/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <sstream>
#include <QLocale>

#include "primitive.hxx"
#include "serialization.hxx"
#include "core/number.hxx"
#include "core/util.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

NodeReal::~NodeReal() = default;

NodeReal::NodeReal() : value(FP::undefined())
{ }

NodeReal::NodeReal(const NodeReal &other) = default;

NodeReal::NodeReal(double value) : value(value)
{ }

NodeReal::NodeReal(QDataStream &stream)
{ stream >> value; }

void NodeReal::serialize(QDataStream &stream) const
{ stream << static_cast<quint8>(Serialization::ID_Real) << value; }

Node::Type NodeReal::type() const
{ return Node::Type::Real; }

void NodeReal::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeRealConstant>(*this); }

bool NodeReal::compatible(Node::WriteGoal goal) const
{ return goal == Node::WriteGoal::Real; }

double NodeReal::toReal() const
{ return value; }

void NodeReal::setReal(double value)
{
    Q_ASSERT(type() != Node::Type::RealConstant);
    this->value = value;
}

QString NodeReal::toDisplayString(const QLocale &locale) const
{
    if (!FP::defined(value))
        return Node::toDisplayString(locale);
    QLocale temp = locale;
    temp.setNumberOptions(temp.numberOptions() | QLocale::OmitGroupSeparator);
    return temp.toString(value);
}

std::string NodeReal::describe() const
{
    if (!FP::defined(value))
        return "Real(UNDEF)";
    std::ostringstream str;
    str << "Real(" << value << ")";
    return str.str();
}

void NodeReal::clear()
{ value = FP::undefined(); }

bool NodeReal::identicalTo(const Node &other) const
{
    switch (other.type()) {
    case Type::Real:
    case Type::RealConstant:
        break;
    default:
        return false;
    }
    return FP::equal(value, static_cast<const NodeReal &>(other).value);
}

std::size_t NodeReal::hash() const
{ return INTEGER::mix(Serialization::ID_Real, std::hash<double>()(value)); }


NodeRealConstant::~NodeRealConstant() = default;

NodeRealConstant::NodeRealConstant() = default;

NodeRealConstant::NodeRealConstant(const NodeReal &other) : NodeReal(other)
{ }

NodeRealConstant::NodeRealConstant(double value) : NodeReal(value)
{ }

NodeRealConstant::NodeRealConstant(QDataStream &stream) : NodeReal(stream)
{ }

Node::Type NodeRealConstant::type() const
{ return Node::Type::RealConstant; }

void NodeRealConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeRealConstant::compatible(Node::WriteGoal) const
{ return false; }

void NodeRealConstant::clear()
{ Q_ASSERT(false); }


NodeInteger::~NodeInteger() = default;

NodeInteger::NodeInteger() : value(INTEGER::undefined())
{ }

NodeInteger::NodeInteger(const NodeInteger &other) = default;

NodeInteger::NodeInteger(std::int_fast64_t value) : value(value)
{ }

NodeInteger::NodeInteger(QDataStream &stream)
{
    qint64 v = 0;
    stream >> v;
    value = v;
}

void NodeInteger::serialize(QDataStream &stream) const
{ stream << static_cast<quint8>(Serialization::ID_Integer) << static_cast<qint64>(value); }

Node::Type NodeInteger::type() const
{ return Node::Type::Integer; }

void NodeInteger::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeIntegerConstant>(*this); }

bool NodeInteger::compatible(Node::WriteGoal goal) const
{ return goal == Node::WriteGoal::Integer; }

std::int_fast64_t NodeInteger::toInteger() const
{ return value; }

void NodeInteger::setInteger(std::int_fast64_t value)
{
    Q_ASSERT(type() != Node::Type::IntegerConstant);
    this->value = value;
}

std::string NodeInteger::describe() const
{
    if (!INTEGER::defined(value))
        return "Integer(UNDEF)";
    return std::string("Integer(") + std::to_string(value) + ")";
}

QString NodeInteger::toDisplayString(const QLocale &locale) const
{
    if (!INTEGER::defined(value))
        return Node::toDisplayString(locale);
    QLocale temp = locale;
    temp.setNumberOptions(temp.numberOptions() | QLocale::OmitGroupSeparator);
    return temp.toString(static_cast<qlonglong>(value));
}

void NodeInteger::clear()
{ value = INTEGER::undefined(); }

bool NodeInteger::identicalTo(const Node &other) const
{
    switch (other.type()) {
    case Type::Integer:
    case Type::IntegerConstant:
        break;
    default:
        return false;
    }
    return value == static_cast<const NodeInteger &>(other).value;
}

std::size_t NodeInteger::hash() const
{ return INTEGER::mix(Serialization::ID_Integer, std::hash<std::int_fast64_t>()(value)); }


NodeIntegerConstant::~NodeIntegerConstant() = default;

NodeIntegerConstant::NodeIntegerConstant() = default;

NodeIntegerConstant::NodeIntegerConstant(const NodeInteger &other) : NodeInteger(other)
{ }

NodeIntegerConstant::NodeIntegerConstant(std::int_fast64_t value) : NodeInteger(value)
{ }

NodeIntegerConstant::NodeIntegerConstant(QDataStream &stream) : NodeInteger(stream)
{ }

Node::Type NodeIntegerConstant::type() const
{ return Node::Type::IntegerConstant; }

void NodeIntegerConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeIntegerConstant::compatible(Node::WriteGoal) const
{ return false; }

void NodeIntegerConstant::clear()
{ Q_ASSERT(false); }


NodeBoolean::~NodeBoolean() = default;

NodeBoolean::NodeBoolean() : value(false)
{ }

NodeBoolean::NodeBoolean(const NodeBoolean &other) = default;

NodeBoolean::NodeBoolean(bool value) : value(value)
{ }

NodeBoolean::NodeBoolean(QDataStream &stream)
{ stream >> value; }

void NodeBoolean::serialize(QDataStream &stream) const
{ stream << static_cast<quint8>(Serialization::ID_Boolean) << value; }

Node::Type NodeBoolean::type() const
{ return Node::Type::Boolean; }

void NodeBoolean::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeBooleanConstant>(*this); }

bool NodeBoolean::compatible(Node::WriteGoal goal) const
{ return goal == Node::WriteGoal::Boolean; }

bool NodeBoolean::toBoolean() const
{ return value; }

void NodeBoolean::setBoolean(bool value)
{
    Q_ASSERT(type() != Node::Type::BooleanConstant);
    this->value = value;
}

std::string NodeBoolean::describe() const
{ return value ? "Boolean(TRUE)" : "Boolean(FALSE)"; }

void NodeBoolean::clear()
{ value = false; }

bool NodeBoolean::identicalTo(const Node &other) const
{
    switch (other.type()) {
    case Type::Boolean:
    case Type::BooleanConstant:
        break;
    default:
        return false;
    }
    return value == static_cast<const NodeBoolean &>(other).value;
}

std::size_t NodeBoolean::hash() const
{ return INTEGER::mix(Serialization::ID_Boolean, value ? 0 : 1); }


NodeBooleanConstant::~NodeBooleanConstant() = default;

NodeBooleanConstant::NodeBooleanConstant() = default;

NodeBooleanConstant::NodeBooleanConstant(const NodeBoolean &other) : NodeBoolean(other)
{ }

NodeBooleanConstant::NodeBooleanConstant(bool value) : NodeBoolean(value)
{ }

NodeBooleanConstant::NodeBooleanConstant(QDataStream &stream) : NodeBoolean(stream)
{ }

Node::Type NodeBooleanConstant::type() const
{ return Node::Type::BooleanConstant; }

void NodeBooleanConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeBooleanConstant::compatible(Node::WriteGoal) const
{ return false; }

void NodeBooleanConstant::clear()
{ Q_ASSERT(false); }


NodeBytes::~NodeBytes() = default;

NodeBytes::NodeBytes() = default;

NodeBytes::NodeBytes(const NodeBytes &other) = default;

NodeBytes::NodeBytes(const Bytes &value) : value(value)
{ }

NodeBytes::NodeBytes(Bytes &&value) : value(std::move(value))
{ }

NodeBytes::NodeBytes(QDataStream &stream)
{ stream >> value; }

void NodeBytes::serialize(QDataStream &stream) const
{ stream << static_cast<quint8>(Serialization::ID_Bytes) << value; }

Node::Type NodeBytes::type() const
{ return Node::Type::Bytes; }

void NodeBytes::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeBytesConstant>(*this); }

bool NodeBytes::compatible(Node::WriteGoal goal) const
{ return goal == Node::WriteGoal::Bytes; }

const Bytes &NodeBytes::toBytes() const
{ return value; }

void NodeBytes::setBytes(const Bytes &value)
{
    Q_ASSERT(type() != Node::Type::BytesConstant);
    this->value = value;
}

void NodeBytes::setBytes(Bytes &&value)
{
    Q_ASSERT(type() != Node::Type::BytesConstant);
    this->value = std::move(value);
}

std::string NodeBytes::describe() const
{ return std::string("Bytes(") + value.toBase64().constData() + ")"; }

void NodeBytes::clear()
{ value.clear(); }

bool NodeBytes::identicalTo(const Node &other) const
{
    switch (other.type()) {
    case Type::Bytes:
    case Type::BytesConstant:
        break;
    default:
        return false;
    }
    return value == static_cast<const NodeBytes &>(other).value;
}

std::size_t NodeBytes::hash() const
{
    std::size_t result = Serialization::ID_Bytes;
    for (auto add : value) {
        result = INTEGER::mix(result, std::hash<char>()(add));
    }
    return result;
}


NodeBytesConstant::~NodeBytesConstant() = default;

NodeBytesConstant::NodeBytesConstant() = default;

NodeBytesConstant::NodeBytesConstant(const NodeBytes &other) : NodeBytes(other)
{ }

NodeBytesConstant::NodeBytesConstant(const Bytes &value) : NodeBytes(value)
{ }

NodeBytesConstant::NodeBytesConstant(Bytes &&value) : NodeBytes(std::move(value))
{ }

NodeBytesConstant::NodeBytesConstant(QDataStream &stream) : NodeBytes(stream)
{ }

Node::Type NodeBytesConstant::type() const
{ return Node::Type::BytesConstant; }

void NodeBytesConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeBytesConstant::compatible(Node::WriteGoal) const
{ return false; }

void NodeBytesConstant::clear()
{ Q_ASSERT(false); }


NodeFlags::~NodeFlags() = default;

NodeFlags::NodeFlags() = default;

NodeFlags::NodeFlags(const NodeFlags &other) = default;

NodeFlags::NodeFlags(const Flags &value) : value(value)
{ }

NodeFlags::NodeFlags(Flags &&value) : value(std::move(value))
{ }

NodeFlags::NodeFlags(QDataStream &stream)
{
#if CPD3_CXX < 201700L
    std::vector<char> raw;
#endif

    std::size_t totalFlags = Serialization::deserializeShortLength(stream);
    for (std::size_t flagIndex = 0; flagIndex < totalFlags; ++flagIndex) {
        std::size_t flagLength = Serialization::deserializeShortLength(stream);
        Flag flag;
#if CPD3_CXX >= 201700L
        while (flagLength > 0) {
            if (stream.status() != QDataStream::Ok) {
                flag.clear();
                break;
            }
            std::size_t offset = flag.size();
            std::size_t add = std::min<std::size_t>(flagLength, 65536);
            flag.resize(offset + add);
            stream.readRawData(reinterpret_cast<char *>(flag.data() + offset), static_cast<int>(add));
            flagLength -= add;
        }
#else
        while (flagLength > 0) {
            if (stream.status() != QDataStream::Ok) {
                flag.clear();
                break;
            }
            raw.resize(std::min<std::size_t>(flagLength, 65536));
            stream.readRawData(reinterpret_cast<char *>(raw.data()), static_cast<int>(raw.size()));
            flag.append(reinterpret_cast<const char *>(raw.data()), raw.size());
            flagLength -= raw.size();
        }
#endif

        value.insert(std::move(flag));
    }
}

void NodeFlags::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_Flags_v2);
    Serialization::serializeShortLength(stream, value.size());
    for (const auto &add : value) {
        Q_ASSERT(static_cast<std::size_t>(static_cast<int>(add.size())) == add.size());
        Serialization::serializeShortLength(stream, add.size());
        stream.writeRawData(reinterpret_cast<const char *>(add.data()),
                            static_cast<int>(add.size()));
    }
}

Node::Type NodeFlags::type() const
{ return Node::Type::Flags; }

void NodeFlags::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeFlagsConstant>(*this); }

bool NodeFlags::compatible(Node::WriteGoal goal) const
{ return goal == Node::WriteGoal::Flags; }

const Flags &NodeFlags::toFlags() const
{ return value; }

void NodeFlags::setFlags(const Flags &value)
{
    Q_ASSERT(type() != Node::Type::FlagsConstant);
    this->value = value;
}

void NodeFlags::setFlags(Flags &&value)
{
    Q_ASSERT(type() != Node::Type::FlagsConstant);
    this->value = std::move(value);
}

void NodeFlags::addFlags(const Flags &value)
{
    Q_ASSERT(type() != Node::Type::FlagsConstant);
    Util::merge(value, this->value);
}

void NodeFlags::addFlags(Flags &&value)
{
    Q_ASSERT(type() != Node::Type::FlagsConstant);
    Util::merge(std::move(value), this->value);
}

void NodeFlags::removeFlags(const Flags &value)
{
    Q_ASSERT(type() != Node::Type::FlagsConstant);
    for (const auto &flag : value) {
        this->value.erase(flag);
    }
}

void NodeFlags::removeFlags(Flags &&value)
{ removeFlags(value); }

void NodeFlags::applyFlag(const Flag &value)
{
    Q_ASSERT(type() != Node::Type::FlagsConstant);
    this->value.insert(value);
}

void NodeFlags::applyFlag(Flag &&value)
{
    Q_ASSERT(type() != Node::Type::FlagsConstant);
    this->value.insert(std::move(value));
}

void NodeFlags::clearFlag(const Flag &value)
{
    Q_ASSERT(type() != Node::Type::FlagsConstant);
    this->value.erase(value);
}

void NodeFlags::clearFlag(Flag &&value)
{ clearFlag(value); }

std::string NodeFlags::describe() const
{
    std::string result("Flags(");
    bool first = true;
    std::vector<Flag> sorted;
    Util::append(value, sorted);
    std::sort(sorted.begin(), sorted.end());
    for (const auto &add : sorted) {
        if (!first)
            result += ",";
        first = false;
        result += add;
    }
    result += ")";
    return std::move(result);
}

void NodeFlags::clear()
{ value.clear(); }

bool NodeFlags::identicalTo(const Node &other) const
{
    switch (other.type()) {
    case Type::Flags:
    case Type::FlagsConstant:
        break;
    default:
        return false;
    }
    return value == static_cast<const NodeFlags &>(other).value;
}

std::size_t NodeFlags::hash() const
{
    std::size_t result = Serialization::ID_Flags_v2;
    result = INTEGER::mix(result, value.size());
    /* Just use simple XOR, since the order doesn't matter then */
    for (const auto &add : value) {
        result ^= std::hash<std::string>()(add);
    }
    return result;
}


NodeFlagsConstant::~NodeFlagsConstant() = default;

NodeFlagsConstant::NodeFlagsConstant() = default;

NodeFlagsConstant::NodeFlagsConstant(const NodeFlags &other) : NodeFlags(other)
{ }

NodeFlagsConstant::NodeFlagsConstant(const Flags &value) : NodeFlags(value)
{ }

NodeFlagsConstant::NodeFlagsConstant(Flags &&value) : NodeFlags(std::move(value))
{ }

NodeFlagsConstant::NodeFlagsConstant(QDataStream &stream) : NodeFlags(stream)
{ }

Node::Type NodeFlagsConstant::type() const
{ return Node::Type::FlagsConstant; }

void NodeFlagsConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeFlagsConstant::compatible(Node::WriteGoal) const
{ return false; }

void NodeFlagsConstant::clear()
{ Q_ASSERT(false); }

}
}
}