/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QLoggingCategory>

#include "overlay.hxx"
#include "toplevel.hxx"
#include "serialization.hxx"
#include "core/qtcompat.hxx"


Q_LOGGING_CATEGORY(log_datacore_variant_overlay, "cpd3.datacore.variant.overlay", QtWarningMsg)

namespace CPD3 {
namespace Data {
namespace Variant {

NodeOverlay::~NodeOverlay() = default;

NodeOverlay::NodeOverlay() : value()
{ }

NodeOverlay::NodeOverlay(const NodeOverlay &other) = default;

NodeOverlay::NodeOverlay(const std::string &value) : value(value)
{ }

NodeOverlay::NodeOverlay(std::string &&value) : value(std::move(value))
{ }

NodeOverlay::NodeOverlay(QDataStream &stream)
{ stream >> value; }

void NodeOverlay::serialize(QDataStream &stream) const
{ stream << static_cast<quint8>(Serialization::ID_Overlay_v2) << value; }

Node::Type NodeOverlay::type() const
{ return Node::Type::Overlay; }

void NodeOverlay::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeOverlayConstant>(*this); }

bool NodeOverlay::compatible(Node::WriteGoal goal) const
{ return goal == Node::WriteGoal::Overlay; }

bool NodeOverlay::bypassSecondOverlay() const
{ return false; }

std::string NodeOverlay::describe() const
{ return std::string("Overlay(") + value + ")"; }

const std::string &NodeOverlay::toString() const
{ return value; }

void NodeOverlay::setOverlayPath(const std::string &value)
{
    Q_ASSERT(type() != Node::Type::OverlayConstant);
    this->value = value;
}

void NodeOverlay::setOverlayPath(std::string &&value)
{
    Q_ASSERT(type() != Node::Type::OverlayConstant);
    this->value = std::move(value);
}

void NodeOverlay::clear()
{ value.clear(); }

bool NodeOverlay::identicalTo(const Node &other) const
{
    switch (other.type()) {
    case Type::Overlay:
    case Type::OverlayConstant:
        break;
    default:
        return false;
    }
    return value == static_cast<const NodeOverlay &>(other).value;
}

std::size_t NodeOverlay::hash() const
{ return INTEGER::mix(Serialization::ID_Overlay_v2, std::hash<std::string>()(value)); }


NodeOverlayConstant::~NodeOverlayConstant() = default;

NodeOverlayConstant::NodeOverlayConstant() = default;

NodeOverlayConstant::NodeOverlayConstant(const NodeOverlay &other) : NodeOverlay(other)
{ }

NodeOverlayConstant::NodeOverlayConstant(const std::string &value) : NodeOverlay(value)
{ }

NodeOverlayConstant::NodeOverlayConstant(std::string &&value) : NodeOverlay(std::move(value))
{ }

NodeOverlayConstant::NodeOverlayConstant(QDataStream &stream) : NodeOverlay(stream)
{ }

Node::Type NodeOverlayConstant::type() const
{ return Node::Type::OverlayConstant; }

void NodeOverlayConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeOverlayConstant::compatible(Node::WriteGoal) const
{ return false; }

void NodeOverlayConstant::clear()
{ Q_ASSERT(false); }


namespace Overlay {

static void first(TopLevel &under, const Node &over)
{
    Q_ASSERT(under.node() != &over);

    switch (over.type()) {
    case Node::Type::Empty:
        under.set<NodeEmpty>();
        break;
    case Node::Type::Real:
    case Node::Type::RealConstant:
        under.set<NodeReal>(static_cast<const NodeReal &>(over));
        break;
    case Node::Type::Integer:
    case Node::Type::IntegerConstant:
        under.set<NodeInteger>(static_cast<const NodeInteger &>(over));
        break;
    case Node::Type::Boolean:
    case Node::Type::BooleanConstant:
        under.set<NodeBoolean>(static_cast<const NodeBoolean &>(over));
        break;
    case Node::Type::String:
    case Node::Type::StringConstant:
        under.set<NodeString>(static_cast<const NodeString &>(over));
        break;
    case Node::Type::LocalizedString:
    case Node::Type::LocalizedStringConstant:
        under.set<NodeLocalizedString>(static_cast<const NodeLocalizedString &>(over));
        break;
    case Node::Type::Bytes:
    case Node::Type::BytesConstant:
        under.set<NodeBytes>(static_cast<const NodeBytes &>(over));
        break;
    case Node::Type::Flags:
    case Node::Type::FlagsConstant:
        under.set<NodeFlags>(static_cast<const NodeFlags &>(over));
        break;

    case Node::Type::Hash:
    case Node::Type::HashConstant:
        if (under.node()->type() != Node::Type::Hash)
            under.set<NodeHash>();
        NodeHash::overlayFirst(*under.node<NodeHash>(), static_cast<const NodeHash &>(over));
        break;
    case Node::Type::Array:
    case Node::Type::ArrayConstant:
        if (under.node()->type() != Node::Type::Array)
            under.set<NodeArray>();
        NodeArray::overlayFirst(*under.node<NodeArray>(), static_cast<const NodeArray &>(over));
        break;
    case Node::Type::Matrix:
    case Node::Type::MatrixConstant:
        if (under.node()->type() != Node::Type::Matrix)
            under.set<NodeMatrix>();
        NodeMatrix::overlayFirst(*under.node<NodeMatrix>(), static_cast<const NodeMatrix &>(over));
        break;
    case Node::Type::Keyframe:
    case Node::Type::KeyframeConstant:
        if (under.node()->type() != Node::Type::Keyframe)
            under.set<NodeKeyframe>();
        NodeKeyframe::overlayFirst(*under.node<NodeKeyframe>(),
                                   static_cast<const NodeKeyframe &>(over));
        break;

    case Node::Type::MetadataReal:
    case Node::Type::MetadataRealConstant:
        if (under.node()->type() != Node::Type::MetadataReal)
            under.set<NodeMetadataReal>();
        NodeMetadataReal::overlayFirst(*under.node<NodeMetadataReal>(),
                                       static_cast<const NodeMetadataReal &>(over));
        break;
    case Node::Type::MetadataInteger:
    case Node::Type::MetadataIntegerConstant:
        if (under.node()->type() != Node::Type::MetadataInteger)
            under.set<NodeMetadataInteger>();
        NodeMetadataInteger::overlayFirst(*under.node<NodeMetadataInteger>(),
                                          static_cast<const NodeMetadataInteger &>(over));
        break;
    case Node::Type::MetadataBoolean:
    case Node::Type::MetadataBooleanConstant:
        if (under.node()->type() != Node::Type::MetadataBoolean)
            under.set<NodeMetadataBoolean>();
        NodeMetadataBoolean::overlayFirst(*under.node<NodeMetadataBoolean>(),
                                          static_cast<const NodeMetadataBoolean &>(over));
        break;
    case Node::Type::MetadataString:
    case Node::Type::MetadataStringConstant:
        if (under.node()->type() != Node::Type::MetadataString)
            under.set<NodeMetadataString>();
        NodeMetadataString::overlayFirst(*under.node<NodeMetadataString>(),
                                         static_cast<const NodeMetadataString &>(over));
        break;
    case Node::Type::MetadataBytes:
    case Node::Type::MetadataBytesConstant:
        if (under.node()->type() != Node::Type::MetadataBytes)
            under.set<NodeMetadataBytes>();
        NodeMetadataBytes::overlayFirst(*under.node<NodeMetadataBytes>(),
                                        static_cast<const NodeMetadataBytes &>(over));
        break;
    case Node::Type::MetadataFlags:
    case Node::Type::MetadataFlagsConstant:
        if (under.node()->type() != Node::Type::MetadataFlags)
            under.set<NodeMetadataFlags>();
        NodeMetadataFlags::overlayFirst(*under.node<NodeMetadataFlags>(),
                                        static_cast<const NodeMetadataFlags &>(over));
        break;
    case Node::Type::MetadataHash:
    case Node::Type::MetadataHashConstant:
        if (under.node()->type() != Node::Type::MetadataHash)
            under.set<NodeMetadataHash>();
        NodeMetadataHash::overlayFirst(*under.node<NodeMetadataHash>(),
                                       static_cast<const NodeMetadataHash &>(over));
        break;
    case Node::Type::MetadataArray:
    case Node::Type::MetadataArrayConstant:
        if (under.node()->type() != Node::Type::MetadataArray)
            under.set<NodeMetadataArray>();
        NodeMetadataArray::overlayFirst(*under.node<NodeMetadataArray>(),
                                        static_cast<const NodeMetadataArray &>(over));
        break;
    case Node::Type::MetadataMatrix:
    case Node::Type::MetadataMatrixConstant:
        if (under.node()->type() != Node::Type::MetadataMatrix)
            under.set<NodeMetadataMatrix>();
        NodeMetadataMatrix::overlayFirst(*under.node<NodeMetadataMatrix>(),
                                         static_cast<const NodeMetadataMatrix &>(over));
        break;
    case Node::Type::MetadataKeyframe:
    case Node::Type::MetadataKeyframeConstant:
        if (under.node()->type() != Node::Type::MetadataKeyframe)
            under.set<NodeMetadataKeyframe>();
        NodeMetadataKeyframe::overlayFirst(*under.node<NodeMetadataKeyframe>(),
                                           static_cast<const NodeMetadataKeyframe &>(over));
        break;

    case Node::Type::Overlay:
    case Node::Type::OverlayConstant: {
        auto origin = PathElement::parse(static_cast<const NodeOverlay &>(over).getOverlayPath());
        if (origin.empty())
            break;

        /* Make a copy, since anything we access might trigger its own
         * deletion during the overlay */
        TopLevel copy = under;
        auto node = copy.read(origin);
        if (!node) {
            under.set<NodeEmpty>();
            break;
        }

        /* We don't have to worry about infinite recursion here, since
         * we enforce that the existing under content does not contain
         * any overlays */
        first(under, *node);
        break;
    }
    }
}

void first(TopLevel &under, const TopLevel &over)
{ first(under, *over.node()); }

void first(std::shared_ptr<Node> &under, const std::shared_ptr<Node> &over)
{
    Q_ASSERT(over);

    /* If we have an existing one, and we can directly replace
     * it, then do so.  We do not do the replacement if there is
     * not existing one, since that means we're creating
     * a new one that we want writable. */
    if (under && under->bypassFirstOverlay(*over)) {
        under = over;
        under->detach(under);
        return;
    }

    switch (over->type()) {
    case Node::Type::Empty:
    case Node::Type::RealConstant:
    case Node::Type::IntegerConstant:
    case Node::Type::BooleanConstant:
    case Node::Type::StringConstant:
    case Node::Type::LocalizedStringConstant:
    case Node::Type::BytesConstant:
    case Node::Type::FlagsConstant:
    case Node::Type::Real:
    case Node::Type::Integer:
    case Node::Type::Boolean:
    case Node::Type::String:
    case Node::Type::LocalizedString:
    case Node::Type::Bytes:
    case Node::Type::Flags:
        /* This happens when copying an opaque type from no
         * existing one, so just create a copy */
        under = over;
        under->detach(under);
        break;

    case Node::Type::HashConstant:
    case Node::Type::Hash:
        if (!under || under->type() != Node::Type::Hash)
            under = Node::create(Node::WriteGoal::Hash, under.get());
        NodeHash::overlayFirst(static_cast<NodeHash &>(*under),
                               static_cast<const NodeHash &>(*over));
        break;
    case Node::Type::Array:
    case Node::Type::ArrayConstant:
        if (!under || under->type() != Node::Type::Array)
            under = Node::create(Node::WriteGoal::Array, under.get());
        NodeArray::overlayFirst(static_cast<NodeArray &>(*under),
                                static_cast<const NodeArray &>(*over));
        break;
    case Node::Type::Matrix:
    case Node::Type::MatrixConstant:
        if (!under || under->type() != Node::Type::Matrix)
            under = Node::create(Node::WriteGoal::Matrix, under.get());
        NodeMatrix::overlayFirst(static_cast<NodeMatrix &>(*under),
                                 static_cast<const NodeMatrix &>(*over));
        break;
    case Node::Type::Keyframe:
    case Node::Type::KeyframeConstant:
        if (!under || under->type() != Node::Type::Keyframe)
            under = Node::create(Node::WriteGoal::Keyframe, under.get());
        NodeKeyframe::overlayFirst(static_cast<NodeKeyframe &>(*under),
                                   static_cast<const NodeKeyframe &>(*over));
        break;

    case Node::Type::MetadataReal:
    case Node::Type::MetadataRealConstant:
        if (!under || under->type() != Node::Type::MetadataReal)
            under = Node::create(Node::WriteGoal::MetadataReal, under.get());
        NodeMetadataReal::overlayFirst(static_cast<NodeMetadataReal &>(*under),
                                       static_cast<const NodeMetadataReal &>(*over));
        break;
    case Node::Type::MetadataInteger:
    case Node::Type::MetadataIntegerConstant:
        if (!under || under->type() != Node::Type::MetadataInteger)
            under = Node::create(Node::WriteGoal::MetadataInteger, under.get());
        NodeMetadataInteger::overlayFirst(static_cast<NodeMetadataInteger &>(*under),
                                          static_cast<const NodeMetadataInteger &>(*over));
        break;
    case Node::Type::MetadataBoolean:
    case Node::Type::MetadataBooleanConstant:
        if (!under || under->type() != Node::Type::MetadataBoolean)
            under = Node::create(Node::WriteGoal::MetadataBoolean, under.get());
        NodeMetadataBoolean::overlayFirst(static_cast<NodeMetadataBoolean &>(*under),
                                          static_cast<const NodeMetadataBoolean &>(*over));
        break;
    case Node::Type::MetadataString:
    case Node::Type::MetadataStringConstant:
        if (!under || under->type() != Node::Type::MetadataString)
            under = Node::create(Node::WriteGoal::MetadataString, under.get());
        NodeMetadataString::overlayFirst(static_cast<NodeMetadataString &>(*under),
                                         static_cast<const NodeMetadataString &>(*over));
        break;
    case Node::Type::MetadataBytes:
    case Node::Type::MetadataBytesConstant:
        if (!under || under->type() != Node::Type::MetadataBytes)
            under = Node::create(Node::WriteGoal::MetadataBytes, under.get());
        NodeMetadataBytes::overlayFirst(static_cast<NodeMetadataBytes &>(*under),
                                        static_cast<const NodeMetadataBytes &>(*over));
        break;
    case Node::Type::MetadataFlags:
    case Node::Type::MetadataFlagsConstant:
        if (!under || under->type() != Node::Type::MetadataFlags)
            under = Node::create(Node::WriteGoal::MetadataFlags, under.get());
        NodeMetadataFlags::overlayFirst(static_cast<NodeMetadataFlags &>(*under),
                                        static_cast<const NodeMetadataFlags &>(*over));
        break;
    case Node::Type::MetadataHash:
    case Node::Type::MetadataHashConstant:
        if (!under || under->type() != Node::Type::MetadataHash)
            under = Node::create(Node::WriteGoal::MetadataHash, under.get());
        NodeMetadataHash::overlayFirst(static_cast<NodeMetadataHash &>(*under),
                                       static_cast<const NodeMetadataHash &>(*over));
        break;
    case Node::Type::MetadataArray:
    case Node::Type::MetadataArrayConstant:
        if (!under || under->type() != Node::Type::MetadataArray)
            under = Node::create(Node::WriteGoal::MetadataArray, under.get());
        NodeMetadataArray::overlayFirst(static_cast<NodeMetadataArray &>(*under),
                                        static_cast<const NodeMetadataArray &>(*over));
        break;
    case Node::Type::MetadataMatrix:
    case Node::Type::MetadataMatrixConstant:
        if (!under || under->type() != Node::Type::MetadataMatrix)
            under = Node::create(Node::WriteGoal::MetadataMatrix, under.get());
        NodeMetadataMatrix::overlayFirst(static_cast<NodeMetadataMatrix &>(*under),
                                         static_cast<const NodeMetadataMatrix &>(*over));
        break;
    case Node::Type::MetadataKeyframe:
    case Node::Type::MetadataKeyframeConstant:
        if (!under || under->type() != Node::Type::MetadataKeyframe)
            under = Node::create(Node::WriteGoal::MetadataKeyframe, under.get());
        NodeMetadataKeyframe::overlayFirst(static_cast<NodeMetadataKeyframe &>(*under),
                                           static_cast<const NodeMetadataKeyframe &>(*over));
        break;

    case Node::Type::Overlay:
    case Node::Type::OverlayConstant:
        /* No-op (preserve the existing under content) until later.  Unless there was
         * nothing there, then we have to put in a placeholder, because another part
         * of the tree may also reference this before we complete the second stage
         * (overlay an overlay) and we can't have null nodes in that case.  The placeholder
         * itself has to be the target overlay (not just an invalid node), so that
         * overlay recursion will still follow it. */
        if (!under) {
            under = over;
            under->detach(under);
        }
        break;
    }

    Q_ASSERT(under);
}


void second(TopLevel &under, const TopLevel &over)
{
    switch (over.node()->type()) {
    case Node::Type::RealConstant:
    case Node::Type::IntegerConstant:
    case Node::Type::BooleanConstant:
    case Node::Type::StringConstant:
    case Node::Type::LocalizedStringConstant:
    case Node::Type::BytesConstant:
    case Node::Type::FlagsConstant:
    case Node::Type::HashConstant:
    case Node::Type::ArrayConstant:
    case Node::Type::MatrixConstant:
    case Node::Type::KeyframeConstant:
    case Node::Type::MetadataRealConstant:
    case Node::Type::MetadataIntegerConstant:
    case Node::Type::MetadataBooleanConstant:
    case Node::Type::MetadataStringConstant:
    case Node::Type::MetadataBytesConstant:
    case Node::Type::MetadataFlagsConstant:
    case Node::Type::MetadataHashConstant:
    case Node::Type::MetadataArrayConstant:
    case Node::Type::MetadataMatrixConstant:
    case Node::Type::MetadataKeyframeConstant:
    case Node::Type::OverlayConstant:
        Q_ASSERT(false);
        break;

    case Node::Type::Empty:
        Q_ASSERT(under.node()->type() == Node::Type::Empty);
        break;
    case Node::Type::Real:
        Q_ASSERT(under.node()->type() == Node::Type::Real);
        break;
    case Node::Type::Integer:
        Q_ASSERT(under.node()->type() == Node::Type::Integer);
        break;
    case Node::Type::Boolean:
        Q_ASSERT(under.node()->type() == Node::Type::Boolean);
        break;
    case Node::Type::String:
        Q_ASSERT(under.node()->type() == Node::Type::String);
        break;
    case Node::Type::LocalizedString:
        /* May have been demoted, so allow for that */
        Q_ASSERT(under.node()->type() == Node::Type::LocalizedString ||
                         under.node()->type() == Node::Type::String);
        break;
    case Node::Type::Bytes:
        Q_ASSERT(under.node()->type() == Node::Type::Bytes);
        break;
    case Node::Type::Flags:
        Q_ASSERT(under.node()->type() == Node::Type::Flags);
        break;

    case Node::Type::Hash:
        Q_ASSERT(under.node()->type() == Node::Type::Hash);
        NodeHash::overlaySecond(*under.node<NodeHash>(), *over.node<NodeHash>(), under);
        break;
    case Node::Type::Array:
        Q_ASSERT(under.node()->type() == Node::Type::Array);
        NodeArray::overlaySecond(*under.node<NodeArray>(), *over.node<NodeArray>(), under);
        break;
    case Node::Type::Matrix:
        Q_ASSERT(under.node()->type() == Node::Type::Matrix);
        NodeMatrix::overlaySecond(*under.node<NodeMatrix>(), *over.node<NodeMatrix>(), under);
        break;
    case Node::Type::Keyframe:
        Q_ASSERT(under.node()->type() == Node::Type::Keyframe);
        NodeKeyframe::overlaySecond(*under.node<NodeKeyframe>(), *over.node<NodeKeyframe>(), under);
        break;

    case Node::Type::MetadataReal:
        Q_ASSERT(under.node()->type() == Node::Type::MetadataReal);
        NodeMetadataReal::overlaySecond(*under.node<NodeMetadataReal>(),
                                        *over.node<NodeMetadataReal>(), under);
        break;
    case Node::Type::MetadataInteger:
        Q_ASSERT(under.node()->type() == Node::Type::MetadataInteger);
        NodeMetadataInteger::overlaySecond(*under.node<NodeMetadataInteger>(),
                                           *over.node<NodeMetadataInteger>(), under);
        break;
    case Node::Type::MetadataBoolean:
        Q_ASSERT(under.node()->type() == Node::Type::MetadataBoolean);
        NodeMetadataBoolean::overlaySecond(*under.node<NodeMetadataBoolean>(),
                                           *over.node<NodeMetadataBoolean>(), under);
        break;
    case Node::Type::MetadataString:
        Q_ASSERT(under.node()->type() == Node::Type::MetadataString);
        NodeMetadataString::overlaySecond(*under.node<NodeMetadataString>(),
                                          *over.node<NodeMetadataString>(), under);
        break;
    case Node::Type::MetadataBytes:
        Q_ASSERT(under.node()->type() == Node::Type::MetadataBytes);
        NodeMetadataBytes::overlaySecond(*under.node<NodeMetadataBytes>(),
                                         *over.node<NodeMetadataBytes>(), under);
        break;
    case Node::Type::MetadataFlags:
        Q_ASSERT(under.node()->type() == Node::Type::MetadataFlags);
        NodeMetadataFlags::overlaySecond(*under.node<NodeMetadataFlags>(),
                                         *over.node<NodeMetadataFlags>(), under);
        break;
    case Node::Type::MetadataHash:
        Q_ASSERT(under.node()->type() == Node::Type::MetadataHash);
        NodeMetadataHash::overlaySecond(*under.node<NodeMetadataHash>(),
                                        *over.node<NodeMetadataHash>(), under);
        break;
    case Node::Type::MetadataArray:
        Q_ASSERT(under.node()->type() == Node::Type::MetadataArray);
        NodeMetadataArray::overlaySecond(*under.node<NodeMetadataArray>(),
                                         *over.node<NodeMetadataArray>(), under);
        break;
    case Node::Type::MetadataMatrix:
        Q_ASSERT(under.node()->type() == Node::Type::MetadataMatrix);
        NodeMetadataMatrix::overlaySecond(*under.node<NodeMetadataMatrix>(),
                                          *over.node<NodeMetadataMatrix>(), under);
        break;
    case Node::Type::MetadataKeyframe:
        Q_ASSERT(under.node()->type() == Node::Type::MetadataKeyframe);
        NodeMetadataKeyframe::overlaySecond(*under.node<NodeMetadataKeyframe>(),
                                            *over.node<NodeMetadataKeyframe>(), under);
        break;

    case Node::Type::Overlay:
        /* Top level overlays are already handled in the first stage, so ignore this */
        break;
    }
}

void second(std::shared_ptr<Node> &under,
            const std::shared_ptr<Node> &over,
            const TopLevel &origin,
            const Path &path,
            std::uint_fast16_t depth)
{
    Q_ASSERT(over);

    if (depth >= 255) {
        qCWarning(log_datacore_variant_overlay) << "Recursion overflow at" << path;
        under = NodeEmpty::instance;
        return;
    }

    switch (over->type()) {
    case Node::Type::Empty:
        Q_ASSERT(under);
        Q_ASSERT(under->type() == Node::Type::Empty);
        break;
    case Node::Type::Real:
    case Node::Type::RealConstant:
        Q_ASSERT(under);
        Q_ASSERT(under->type() == Node::Type::Real || under->type() == Node::Type::RealConstant);
        break;
    case Node::Type::Integer:
    case Node::Type::IntegerConstant:
        Q_ASSERT(under);
        Q_ASSERT(under->type() == Node::Type::Integer ||
                         under->type() == Node::Type::IntegerConstant);
        break;
    case Node::Type::Boolean:
    case Node::Type::BooleanConstant:
        Q_ASSERT(under);
        Q_ASSERT(under->type() == Node::Type::Boolean ||
                         under->type() == Node::Type::BooleanConstant);
        break;
    case Node::Type::String:
    case Node::Type::StringConstant:
        Q_ASSERT(under);
        Q_ASSERT(
                under->type() == Node::Type::String || under->type() == Node::Type::StringConstant);
        break;
    case Node::Type::LocalizedString:
    case Node::Type::LocalizedStringConstant:
        Q_ASSERT(under);
        /* May have been demoted, so allow for that */
        Q_ASSERT(under->type() == Node::Type::LocalizedString ||
                         under->type() == Node::Type::LocalizedStringConstant ||
                         under->type() == Node::Type::String ||
                         under->type() == Node::Type::StringConstant);
        break;
    case Node::Type::Bytes:
    case Node::Type::BytesConstant:
        Q_ASSERT(under);
        Q_ASSERT(under->type() == Node::Type::Bytes || under->type() == Node::Type::BytesConstant);
        break;
    case Node::Type::Flags:
    case Node::Type::FlagsConstant:
        Q_ASSERT(under);
        Q_ASSERT(under->type() == Node::Type::Flags || under->type() == Node::Type::FlagsConstant);
        break;

    case Node::Type::HashConstant:
    case Node::Type::Hash:
        Q_ASSERT(under);
        if (under->type() == Node::Type::HashConstant) {
            Q_ASSERT(over->bypassSecondOverlay());
            Q_ASSERT(under->bypassSecondOverlay());
            break;
        }
        Q_ASSERT(under->type() == Node::Type::Hash);
        NodeHash::overlaySecond(static_cast<NodeHash &>(*under),
                                static_cast<const NodeHash &>(*over), origin, path, depth);
        break;
    case Node::Type::Array:
    case Node::Type::ArrayConstant:
        Q_ASSERT(under);
        if (under->type() == Node::Type::ArrayConstant) {
            Q_ASSERT(over->bypassSecondOverlay());
            Q_ASSERT(under->bypassSecondOverlay());
            break;
        }
        Q_ASSERT(under->type() == Node::Type::Array);
        NodeArray::overlaySecond(static_cast<NodeArray &>(*under),
                                 static_cast<const NodeArray &>(*over), origin, path, depth);
        break;
    case Node::Type::Matrix:
    case Node::Type::MatrixConstant:
        Q_ASSERT(under);
        if (under->type() == Node::Type::MatrixConstant) {
            Q_ASSERT(over->bypassSecondOverlay());
            Q_ASSERT(under->bypassSecondOverlay());
            break;
        }
        Q_ASSERT(under->type() == Node::Type::Matrix);
        NodeMatrix::overlaySecond(static_cast<NodeMatrix &>(*under),
                                  static_cast<const NodeMatrix &>(*over), origin, path, depth);
        break;
    case Node::Type::Keyframe:
    case Node::Type::KeyframeConstant:
        Q_ASSERT(under);
        if (under->type() == Node::Type::KeyframeConstant) {
            Q_ASSERT(over->bypassSecondOverlay());
            Q_ASSERT(under->bypassSecondOverlay());
            break;
        }
        Q_ASSERT(under->type() == Node::Type::Keyframe);
        NodeKeyframe::overlaySecond(static_cast<NodeKeyframe &>(*under),
                                    static_cast<const NodeKeyframe &>(*over), origin, path, depth);
        break;

    case Node::Type::MetadataReal:
    case Node::Type::MetadataRealConstant:
        Q_ASSERT(under);
        if (under->type() == Node::Type::MetadataRealConstant) {
            Q_ASSERT(over->bypassSecondOverlay());
            Q_ASSERT(under->bypassSecondOverlay());
            break;
        }
        Q_ASSERT(under->type() == Node::Type::MetadataReal);
        NodeMetadataReal::overlaySecond(static_cast<NodeMetadataReal &>(*under),
                                        static_cast<const NodeMetadataReal &>(*over), origin, path,
                                        depth);
        break;
    case Node::Type::MetadataInteger:
    case Node::Type::MetadataIntegerConstant:
        Q_ASSERT(under);
        if (under->type() == Node::Type::MetadataIntegerConstant) {
            Q_ASSERT(over->bypassSecondOverlay());
            Q_ASSERT(under->bypassSecondOverlay());
            break;
        }
        Q_ASSERT(under->type() == Node::Type::MetadataInteger);
        NodeMetadataInteger::overlaySecond(static_cast<NodeMetadataInteger &>(*under),
                                           static_cast<const NodeMetadataInteger &>(*over), origin,
                                           path, depth);
        break;
    case Node::Type::MetadataBoolean:
    case Node::Type::MetadataBooleanConstant:
        Q_ASSERT(under);
        if (under->type() == Node::Type::MetadataBooleanConstant) {
            Q_ASSERT(over->bypassSecondOverlay());
            Q_ASSERT(under->bypassSecondOverlay());
            break;
        }
        Q_ASSERT(under->type() == Node::Type::MetadataBoolean);
        NodeMetadataBoolean::overlaySecond(static_cast<NodeMetadataBoolean &>(*under),
                                           static_cast<const NodeMetadataBoolean &>(*over), origin,
                                           path, depth);
        break;
    case Node::Type::MetadataString:
    case Node::Type::MetadataStringConstant:
        Q_ASSERT(under);
        if (under->type() == Node::Type::MetadataStringConstant) {
            Q_ASSERT(over->bypassSecondOverlay());
            Q_ASSERT(under->bypassSecondOverlay());
            break;
        }
        Q_ASSERT(under->type() == Node::Type::MetadataString);
        NodeMetadataString::overlaySecond(static_cast<NodeMetadataString &>(*under),
                                          static_cast<const NodeMetadataString &>(*over), origin,
                                          path, depth);
        break;
    case Node::Type::MetadataBytes:
    case Node::Type::MetadataBytesConstant:
        Q_ASSERT(under);
        if (under->type() == Node::Type::MetadataBytesConstant) {
            Q_ASSERT(over->bypassSecondOverlay());
            Q_ASSERT(under->bypassSecondOverlay());
            break;
        }
        Q_ASSERT(under->type() == Node::Type::MetadataBytes);
        NodeMetadataBytes::overlaySecond(static_cast<NodeMetadataBytes &>(*under),
                                         static_cast<const NodeMetadataBytes &>(*over), origin,
                                         path, depth);
        break;
    case Node::Type::MetadataFlags:
    case Node::Type::MetadataFlagsConstant:
        Q_ASSERT(under);
        if (under->type() == Node::Type::MetadataFlagsConstant) {
            Q_ASSERT(over->bypassSecondOverlay());
            Q_ASSERT(under->bypassSecondOverlay());
            break;
        }
        Q_ASSERT(under->type() == Node::Type::MetadataFlags);
        NodeMetadataFlags::overlaySecond(static_cast<NodeMetadataFlags &>(*under),
                                         static_cast<const NodeMetadataFlags &>(*over), origin,
                                         path, depth);
        break;
    case Node::Type::MetadataHash:
    case Node::Type::MetadataHashConstant:
        Q_ASSERT(under);
        if (under->type() == Node::Type::MetadataHashConstant) {
            Q_ASSERT(over->bypassSecondOverlay());
            Q_ASSERT(under->bypassSecondOverlay());
            break;
        }
        Q_ASSERT(under->type() == Node::Type::MetadataHash);
        NodeMetadataHash::overlaySecond(static_cast<NodeMetadataHash &>(*under),
                                        static_cast<const NodeMetadataHash &>(*over), origin, path,
                                        depth);
        break;
    case Node::Type::MetadataArray:
    case Node::Type::MetadataArrayConstant:
        Q_ASSERT(under);
        if (under->type() == Node::Type::MetadataArrayConstant) {
            Q_ASSERT(over->bypassSecondOverlay());
            Q_ASSERT(under->bypassSecondOverlay());
            break;
        }
        Q_ASSERT(under->type() == Node::Type::MetadataArray);
        NodeMetadataArray::overlaySecond(static_cast<NodeMetadataArray &>(*under),
                                         static_cast<const NodeMetadataArray &>(*over), origin,
                                         path, depth);
        break;
    case Node::Type::MetadataMatrix:
    case Node::Type::MetadataMatrixConstant:
        Q_ASSERT(under);
        if (under->type() == Node::Type::MetadataMatrixConstant) {
            Q_ASSERT(over->bypassSecondOverlay());
            Q_ASSERT(under->bypassSecondOverlay());
            break;
        }
        Q_ASSERT(under->type() == Node::Type::MetadataMatrix);
        NodeMetadataMatrix::overlaySecond(static_cast<NodeMetadataMatrix &>(*under),
                                          static_cast<const NodeMetadataMatrix &>(*over), origin,
                                          path, depth);
        break;
    case Node::Type::MetadataKeyframe:
    case Node::Type::MetadataKeyframeConstant:
        Q_ASSERT(under);
        if (under->type() == Node::Type::MetadataKeyframeConstant) {
            Q_ASSERT(over->bypassSecondOverlay());
            Q_ASSERT(under->bypassSecondOverlay());
            break;
        }
        Q_ASSERT(under->type() == Node::Type::MetadataKeyframe);
        NodeMetadataKeyframe::overlaySecond(static_cast<NodeMetadataKeyframe &>(*under),
                                            static_cast<const NodeMetadataKeyframe &>(*over),
                                            origin, path, depth);
        break;

    case Node::Type::Overlay:
    case Node::Type::OverlayConstant: {
        auto nextPath =
                PathElement::parse(static_cast<const NodeOverlay &>(*over).getOverlayPath(), path);
        /* An empty path would make us resolve forever (resolving ourselves), so
         * just ignore it */
        if (nextPath.empty()) {
            under = NodeEmpty::instance;
            break;
        }

        auto nextNode = origin.read(nextPath);
        if (!nextNode) {
            under = NodeEmpty::instance;
            break;
        }

        /* Repeat the first stage, so we get the types set and basic merging done */
        first(under, nextNode);

        /* Now try to flatten the new value, this will continue to follow
         * links if required, so depth limit it */
        second(under, nextNode, origin, nextPath, depth + 1);
        break;
    }
    }
}

}
}
}
}