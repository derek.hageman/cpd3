/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <type_traits>

#include "serialization.hxx"
#include "handle.hxx"


Q_LOGGING_CATEGORY(log_datacore_variant_serialization, "cpd3.datacore.variant.serialization",
                   QtWarningMsg)

namespace CPD3 {
namespace Data {
namespace Variant {
namespace Serialization {

namespace {
struct ConstructHandler {
    template<typename T, typename... Args,
             class = typename std::enable_if<!std::is_same<T, NodeEmpty>::value>::type>
    std::shared_ptr<Node> construct(Args &&... args) const
    { return std::make_shared<T>(std::forward<Args>(args)...); }

    template<typename T, class = typename std::enable_if<std::is_same<T, NodeEmpty>::value>::type>
    std::shared_ptr<Node> construct() const
    { return NodeEmpty::instance; }
};
}

std::shared_ptr<Node> construct(QDataStream &stream)
{ return deserialize(stream, ConstructHandler()); }


void serializeShortLength(QDataStream &stream, std::size_t length)
{
    if (length < 0xFF) {
        stream << static_cast<quint8>(length);
        return;
    }
    Q_ASSERT(static_cast<quint32>(length) == length);
    stream << static_cast<quint8>(0xFF) << static_cast<quint32>(length);
}

std::size_t deserializeShortLength(QDataStream &stream)
{
    quint8 s = 0;
    stream >> s;
    if (s != 0xFF)
        return s;
    quint32 n = 0;
    stream >> n;
    return n;
}

void serializeShortString(QDataStream &stream, const std::string &str)
{
    Q_ASSERT(static_cast<std::size_t>(static_cast<int>(str.size())) == str.size());
    serializeShortLength(stream, str.size());
    stream.writeRawData(reinterpret_cast<const char *>(str.data()), static_cast<int>(str.size()));
}

std::string deserializeShortString(QDataStream &stream)
{
    std::size_t len = deserializeShortLength(stream);
    std::string result;
#if CPD3_CXX >= 201700L
    while (len > 0) {
        if (stream.status() != QDataStream::Ok) {
            result.clear();
            break;
        }
        std::size_t offset = result.size();
        std::size_t add = std::min<std::size_t>(len, 65536);
        result.resize(offset + add);
        stream.readRawData(reinterpret_cast<char *>(result.data() + offset), static_cast<int>(add));
        len -= add;
    }
#else
    std::vector<char> raw;
    while (len > 0) {
        if (stream.status() != QDataStream::Ok) {
            result.clear();
            break;
        }
        raw.resize(std::min<std::size_t>(len, 65536));
        stream.readRawData(reinterpret_cast<char *>(raw.data()), static_cast<int>(raw.size()));
        result.append(reinterpret_cast<const char *>(raw.data()), raw.size());
        len -= raw.size();
    }
#endif
    return std::move(result);
}


static void serializeMetadata(QDataStream &stream, const Container::ReadMetadata &metadata)
{
    stream << static_cast<quint32>(metadata.size());
    for (auto add : metadata) {
        stream << QString::fromStdString(add.first);
        serializeLegacy(stream, add.second);
    }
}

void serializeLegacy(QDataStream &stream, const Read &value)
{
    switch (value.getType()) {
    case Type::Empty:
        stream << static_cast<quint8>(ID_Empty);
        break;
    case Type::Real:
        stream << static_cast<quint8>(ID_Real) << static_cast<double>(value.toDouble());
        break;
    case Type::Integer:
        stream << static_cast<quint8>(ID_Integer) << static_cast<quint64>(value.toInteger());
        break;
    case Type::Boolean:
        stream << static_cast<quint8>(ID_Boolean) << static_cast<bool>(value.toBool());
        break;
    case Type::String: {
        stream << static_cast<quint8>(ID_String_v1);
        stream << QString::fromStdString(value.toString());
        auto localized = value.allLocalizedStrings();
        localized.erase(QString());
        stream << static_cast<quint32>(localized.size());
        for (const auto &add : localized) {
            stream << QString(add.first) << QString(add.second);
        }
        break;
    }
    case Type::Bytes:
        stream << static_cast<quint8>(ID_Bytes) << QByteArray(value.toBytes());
        break;
    case Type::Flags: {
        stream << static_cast<quint8>(ID_Flags_v1);
        const auto &flags = value.toFlags();
        stream << static_cast<quint32>(flags.size());
        for (const auto &add : flags) {
            stream << QString::fromStdString(add);
        }
        break;
    }
    case Type::Hash: {
        stream << static_cast<quint8>(ID_Hash_v1);
        auto children = value.toHash();
        stream << static_cast<quint32>(children.size());
        for (auto add : children) {
            stream << QString::fromStdString(add.first);
            serializeLegacy(stream, add.second);
        }
        break;
    }
    case Type::Array: {
        stream << static_cast<quint8>(ID_Array_v1);
        auto children = value.toArray();
        stream << static_cast<quint32>(children.size());
        for (auto add : children) {
            serializeLegacy(stream, add);
        }
        break;
    }
    case Type::Matrix: {
        stream << static_cast<quint8>(ID_Matrix_v1);
        auto children = value.toMatrix();
        stream << static_cast<quint32>(children.size());
        for (auto add : children) {
            serializeLegacy(stream, add.second);
        }
        auto shape = children.shape();
        stream << static_cast<quint8>(shape.size());
        for (auto add :shape) {
            stream << static_cast<quint32>(add);
        }
        break;
    }
    case Type::Keyframe: {
        stream << static_cast<quint8>(ID_Keyframe_v1);
        auto children = value.toKeyframe();
        stream << static_cast<quint32>(children.size());
        for (auto add : children) {
            stream << static_cast<double>(add.first);
            serializeLegacy(stream, add.second);
        }
        break;
    }
    case Type::MetadataReal:
        stream << static_cast<quint8>(ID_MetadataReal_v1);
        serializeMetadata(stream, value.toMetadata());
        break;
    case Type::MetadataInteger:
        stream << static_cast<quint8>(ID_MetadataInteger_v1);
        serializeMetadata(stream, value.toMetadata());
        break;
    case Type::MetadataBoolean:
        stream << static_cast<quint8>(ID_MetadataBoolean_v1);
        serializeMetadata(stream, value.toMetadata());
        break;
    case Type::MetadataString:
        stream << static_cast<quint8>(ID_MetadataString_v1);
        serializeMetadata(stream, value.toMetadata());
        break;
    case Type::MetadataBytes:
        stream << static_cast<quint8>(ID_MetadataBytes_v1);
        serializeMetadata(stream, value.toMetadata());
        break;
    case Type::MetadataFlags: {
        stream << static_cast<quint8>(ID_MetadataFlags_v1);
        serializeMetadata(stream, value.toMetadata());
        auto children = value.toMetadataSingleFlag();
        stream << static_cast<quint32>(children.size());
        for (auto add : children) {
            stream << QString::fromStdString(add.first);
            serializeLegacy(stream, add.second);
        }
        break;
    }
    case Type::MetadataHash: {
        stream << static_cast<quint8>(ID_MetadataHash_v1);
        serializeMetadata(stream, value.toMetadata());
        auto children = value.toMetadataHashChild();
        stream << static_cast<quint32>(children.size());
        for (auto add : children) {
            stream << QString::fromStdString(add.first);
            serializeLegacy(stream, add.second);
        }
        break;
    }
    case Type::MetadataArray:
        stream << static_cast<quint8>(ID_MetadataArray_v1);
        serializeMetadata(stream, value.toMetadata());
        break;
    case Type::MetadataMatrix:
        stream << static_cast<quint8>(ID_MetadataMatrix_v1);
        serializeMetadata(stream, value.toMetadata());
        break;
    case Type::MetadataKeyframe:
        stream << static_cast<quint8>(ID_MetadataKeyframe_v1);
        serializeMetadata(stream, value.toMetadata());
        break;
    case Type::Overlay:
        stream << static_cast<quint8>(ID_Overlay_v1);
        stream << QString::fromStdString(value.toString());
        break;
    }
}

}
}
}
}