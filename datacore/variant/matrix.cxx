/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "matrix.hxx"
#include "overlay.hxx"
#include "serialization.hxx"
#include "core/util.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

NodeMatrix::~NodeMatrix() = default;

NodeMatrix::NodeMatrix() = default;

NodeMatrix::NodeMatrix(const NodeMatrix &other) : children(other.children), size(other.size)
{
    for (auto &mod : children) {
        mod->detach(mod);
    }
}

NodeMatrix::NodeMatrix(Children &&children, PathElement::MatrixIndex &&size) : children(
        std::move(children)), size(std::move(size))
{ }

NodeMatrix::NodeMatrix(QDataStream &stream)
{
    {
        std::size_t n = Serialization::deserializeShortLength(stream);
        for (std::size_t i = 0; i < n; ++i) {
            children.emplace_back(Serialization::construct(stream));
        }
    }
    {
        quint8 n = 0;
        stream >> n;
        for (quint8 i = 0; i < n; ++i) {
            size.emplace_back(Serialization::deserializeShortLength(stream));
        }
    }
}

void NodeMatrix::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_Matrix_v2);
    Serialization::serializeShortLength(stream, children.size());
    for (const auto &child : children) {
        child->serialize(stream);
    }
    Q_ASSERT(size.size() <= 0xFF);
    stream << static_cast<quint8>(size.size());
    for (auto s : size) {
        Serialization::serializeShortLength(stream, s);
    }
}

Node::Type NodeMatrix::type() const
{ return Node::Type::Matrix; }

void NodeMatrix::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeMatrixConstant>(*this); }

std::size_t NodeMatrix::toIndex(const PathElement::MatrixIndex &index,
                                const PathElement::MatrixIndex &size)
{
    std::size_t i = 0;
    std::size_t pitch = 1;
    for (std::size_t dim = 0, maxDim = std::min(index.size(), size.size()); dim < maxDim; ++dim) {
        i += index[dim] * pitch;
        pitch *= size[dim];
    }
    return i;
}

PathElement::MatrixIndex NodeMatrix::fromIndex(std::size_t index,
                                               const PathElement::MatrixIndex &size)
{
    PathElement::MatrixIndex result(size.size(), 0);
    auto target = result.begin();
    for (auto s : size) {
        *target = (index % s);
        ++target;
        index /= s;
    }
    return result;
}

std::size_t NodeMatrix::toIndex(const PathElement::MatrixIndex &index) const
{ return toIndex(index, size); }

PathElement::MatrixIndex NodeMatrix::fromIndex(std::size_t index) const
{ return fromIndex(index, size); }

bool NodeMatrix::indexInBounds(const PathElement::MatrixIndex &index) const
{
    std::size_t i = 0;
    for (std::size_t max = size.size(); i < max; i++) {
        if (index[i] >= size[i])
            return false;
    }
    for (std::size_t max = index.size(); i < max; i++) {
        if (index[i] != 0)
            return false;
    }
    return true;
}

void NodeMatrix::maybeReshape(const PathElement::MatrixIndex &index)
{
    if (!size.empty() && indexInBounds(index))
        return;

    PathElement::MatrixIndex expanded;
    std::size_t i = 0;
    for (std::size_t max = size.size(); i < max; i++) {
        expanded.emplace_back(std::max(index[i] + 1, size[i]));
    }
    for (std::size_t max = index.size(); i < max; i++) {
        expanded.emplace_back(index[i] + 1);
    }
    reshape(expanded);
}

namespace {
class DimensionBreakdown {
public:
    virtual ~DimensionBreakdown() = default;

    virtual std::vector<std::shared_ptr<Node>> unroll() = 0;

    virtual void resize(const PathElement::MatrixIndex &size) = 0;

    virtual void assign(PathElement::MatrixIndex index, std::shared_ptr<Node> &&value) = 0;
};

class DimensionMiddle : public DimensionBreakdown {
    std::vector<std::unique_ptr<DimensionBreakdown>> dimensions;
public:
    virtual ~DimensionMiddle() = default;

    DimensionMiddle() = default;

    std::vector<std::shared_ptr<Node>> unroll() override
    {
        std::vector<std::shared_ptr<Node>> result;
        for (auto &dim : dimensions) {
            Util::append(dim->unroll(), result);
        }
        dimensions.clear();
        return result;
    }

    void resize(const PathElement::MatrixIndex &size) override;

    void assign(PathElement::MatrixIndex index, std::shared_ptr<Node> &&value) override
    {
        Q_ASSERT(index.size() > 1);
        std::size_t i = index.back();
        /* Happens with extra dimensions */
        if (i >= dimensions.size())
            return;
        index.pop_back();
        return dimensions[i]->assign(std::move(index), std::move(value));
    }
};

class DimensionLast : public DimensionBreakdown {
    std::vector<std::shared_ptr<Node>> nodes;
public:
    virtual ~DimensionLast() = default;

    DimensionLast() = default;

    explicit DimensionLast(std::vector<std::shared_ptr<Node>> &&all) : nodes(std::move(all))
    { }

    std::vector<std::shared_ptr<Node>> unroll() override
    { return std::move(nodes); }

    void resize(const PathElement::MatrixIndex &size) override
    {
        if (size.empty())
            return;
        Q_ASSERT(size.size() == 1);
        nodes.resize(size.front());
    }

    void assign(PathElement::MatrixIndex index, std::shared_ptr<Node> &&value) override
    {
        Q_ASSERT(index.size() == 1);
        std::size_t i = index.front();
        /* Happens with extra dimensions */
        if (i >= nodes.size())
            return;
        nodes[i] = std::move(value);
    }
};

void DimensionMiddle::resize(const PathElement::MatrixIndex &size)
{
    Q_ASSERT(size.size() > 1);
    std::size_t goal = size.back();
    if (goal <= dimensions.size()) {
        dimensions.resize(goal);
    } else {
        if (size.size() > 2) {
            while (dimensions.size() < goal) {
                dimensions.emplace_back(new DimensionMiddle);
            }
        } else {
            Q_ASSERT(size.size() == 2);
            while (dimensions.size() < goal) {
                dimensions.emplace_back(new DimensionLast);
            }
        }
    }
    PathElement::MatrixIndex next = size;
    next.pop_back();
    for (auto &d : dimensions) {
        d->resize(next);
    }
}

}

void NodeMatrix::reshape(const PathElement::MatrixIndex &target, const std::shared_ptr<Node> &fill)
{
    if (target.empty()) {
        children.clear();
        size.clear();
        return;
    }

    Q_ASSERT(fill);

    /* If we're adding dimensions, then pretend we already had them, but
     * they where size 1 */
    while (target.size() > size.size()) {
        size.emplace_back(1);
    }
    /* For removal, just drop them, the extra children (i.e. the ones
     * with the higher order dimensions non-zero), will be discarded
     * by the resize */
    while (target.size() < size.size()) {
        size.pop_back();
    }

    Q_ASSERT(target.size() == size.size());

    std::unique_ptr<DimensionBreakdown> breakdown;
    if (size.size() <= 1) {
        breakdown.reset(new DimensionLast(std::move(children)));
        Q_ASSERT(size.size() == 1);
        /* Discard extras */
        breakdown->resize(size);
    } else {
        breakdown.reset(new DimensionMiddle);
        breakdown->resize(size);
        for (std::size_t index = 0, max = children.size(); index < max; ++index) {
            breakdown->assign(fromIndex(index), std::move(children[index]));
        }
    }

    breakdown->resize(target);
    children = breakdown->unroll();
    breakdown.reset();

    for (auto &check : children) {
        if (!check) {
            check = fill;
            check->detach(check);
        }
    }
    size = target;
}

std::shared_ptr<Node> NodeMatrix::read(const PathElement &path) const
{
    if (children.empty())
        return std::shared_ptr<Node>();
    switch (path.type) {
    case PathElement::Type::Empty:
        return children.front();
    case PathElement::Type::Matrix: {
        auto index = toIndex(path.ref<PathElement::MatrixIndex>());
        if (index >= children.size())
            return std::shared_ptr<Node>();
        return children[index];
    }
    case PathElement::Type::Array: {
        auto index = path.ref<PathElement::ArrayIndex>();
        if (index >= children.size())
            return std::shared_ptr<Node>();
        return children[index];
    }
    default:
        break;
    }
    return std::shared_ptr<Node>();
}

bool NodeMatrix::compatible(Node::WriteGoal goal) const
{ return goal == WriteGoal::Matrix; }

bool NodeMatrix::childrenWritable(PathElement::Type type) const
{
    switch (type) {
    case PathElement::Type::Empty:
    case PathElement::Type::Matrix:
    case PathElement::Type::Array:
        return true;
    default:
        break;
    }
    return false;
}

std::shared_ptr<Node> NodeMatrix::write(const PathElement &path, Node::WriteGoal final)
{
    switch (path.type) {
    case PathElement::Type::Empty:
        if (children.empty()) {
            children.emplace_back(create(final));
        } else if (!children.front()->compatible(final)) {
            children.front() = create(final, children.front().get());
        }
        return children.front();
    case PathElement::Type::Matrix: {
        const auto &mindex = path.ref<PathElement::MatrixIndex>();
        maybeReshape(mindex);
        auto index = toIndex(mindex);
        while (index > children.size()) {
            children.emplace_back(NodeEmpty::instance);
        }
        if (index >= children.size()) {
            children.emplace_back(create(final));
            Q_ASSERT(index == children.size() - 1);
            return children.back();
        }
        auto &child = children[index];
        Q_ASSERT(child.get());
        if (!child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    case PathElement::Type::Array: {
        auto index = path.ref<PathElement::ArrayIndex>();
        while (index > children.size()) {
            children.emplace_back(NodeEmpty::instance);
        }
        if (index >= children.size()) {
            children.emplace_back(create(final));
            Q_ASSERT(index == children.size() - 1);
            return children.back();
        }
        auto &child = children[index];
        Q_ASSERT(child.get());
        if (!child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    default:
        Q_ASSERT(false);
        break;
    }
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMatrix::write(const PathElement &path, PathElement::Type next)
{
    switch (path.type) {
    case PathElement::Type::Empty:
        if (children.empty()) {
            children.emplace_back(create(next));
        } else if (!children.front()->childrenWritable(next)) {
            children.front() = create(next, children.front().get());
        }
        return children.front();
    case PathElement::Type::Matrix: {
        const auto &mindex = path.ref<PathElement::MatrixIndex>();
        maybeReshape(mindex);
        auto index = toIndex(mindex);
        while (index > children.size()) {
            children.emplace_back(NodeEmpty::instance);
        }
        if (index >= children.size()) {
            children.emplace_back(create(next));
            Q_ASSERT(index == children.size() - 1);
            return children.back();
        }
        auto &child = children[index];
        Q_ASSERT(child.get());
        if (!child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    case PathElement::Type::Array: {
        auto index = path.ref<PathElement::ArrayIndex>();
        while (index > children.size()) {
            children.emplace_back(NodeEmpty::instance);
        }
        if (index >= children.size()) {
            children.emplace_back(create(next));
            Q_ASSERT(index == children.size() - 1);
            return children.back();
        }
        auto &child = children[index];
        Q_ASSERT(child.get());
        if (!child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    default:
        Q_ASSERT(false);
        break;
    }
    return std::shared_ptr<Node>();
}

void NodeMatrix::insert(const PathElement &path, const std::shared_ptr<Node> &final)
{
    switch (path.type) {
    case PathElement::Type::Empty:
        if (children.empty()) {
            children.emplace_back(final);
        } else {
            children.front() = final;
        }
        children.front()->detach(children.front());
        break;
    case PathElement::Type::Matrix: {
        const auto &mindex = path.ref<PathElement::MatrixIndex>();
        maybeReshape(mindex);
        auto index = toIndex(mindex);
        while (index > children.size()) {
            children.emplace_back(NodeEmpty::instance);
        }
        if (index >= children.size()) {
            children.emplace_back(final);
            Q_ASSERT(index == children.size() - 1);
            children.back()->detach(children.back());
            break;
        }
        auto &child = children[index];
        child = final;
        child->detach(child);
        break;
    }
    case PathElement::Type::Array: {
        auto index = path.ref<PathElement::ArrayIndex>();
        while (index > children.size()) {
            children.emplace_back(NodeEmpty::instance);
        }
        if (index >= children.size()) {
            children.emplace_back(final);
            Q_ASSERT(index == children.size() - 1);
            children.back()->detach(children.back());
            break;
        }
        auto &child = children[index];
        child = final;
        child->detach(child);
        break;
    }
    default:
        Q_ASSERT(false);
        break;
    }
}

void NodeMatrix::remove(const PathElement &path)
{
    switch (path.type) {
    case PathElement::Type::Empty:
        if (!children.empty())
            children.erase(children.begin());
        break;
    case PathElement::Type::Matrix: {
        const auto &mindex = path.ref<PathElement::MatrixIndex>();
        if (!indexInBounds(mindex))
            break;
        auto index = toIndex(mindex);
        if (index >= children.size())
            break;
        children.erase(children.begin() + index);
        break;
    }
    case PathElement::Type::Array: {
        auto index = path.ref<PathElement::ArrayIndex>();
        if (index >= children.size())
            break;
        children.erase(children.begin() + index);
        break;
    }
    default:
        Q_ASSERT(false);
        break;
    }
}

bool NodeMatrix::bypassFirstOverlay(const Node &over) const
{
    switch (over.type()) {
    case Node::Type::Matrix:
    case Node::Type::MatrixConstant:
        break;
    default:
        return Node::bypassFirstOverlay(over);
    }
    /* Can't bypass if the over needs a flatten */
    if (!over.bypassSecondOverlay())
        return false;
    /* If the sizes aren't equal, then it's a direct replace */
    if (size != static_cast<const NodeMatrix &>(over).size)
        return true;
    /* Otherwise we can bypass if either is
     * opaque (the over just directly replaces
     * us) */
    return isOverlayOpaque() || over.isOverlayOpaque();
}

bool NodeMatrix::bypassSecondOverlay() const
{
    for (const auto &child : children) {
        if (!child->bypassSecondOverlay())
            return false;
    }
    return true;
}

bool NodeMatrix::isOverlayOpaque() const
{
    for (const auto &child : children) {
        if (!child->isOverlayOpaque())
            return false;
    }
    return true;
}

void NodeMatrix::overlayFirst(NodeMatrix &under, const NodeMatrix &over)
{
    Q_ASSERT(under.type() == Node::Type::Matrix);

    /* Either being opaque means just replace the under entirely */
    if (under.size != over.size || under.isOverlayOpaque() || over.isOverlayOpaque()) {
        under.children.clear();
        under.size = over.size;
        for (const auto &add : over.children) {
            under.children.emplace_back();
            Overlay::first(under.children.back(), add);
        }
        return;
    }

    for (std::size_t index = 0, max = over.children.size(); index < max; ++index) {
        if (index >= under.children.size())
            under.children.emplace_back();
        Overlay::first(under.children[index], over.children[index]);
    }
}

void NodeMatrix::overlaySecond(NodeMatrix &under,
                               const NodeMatrix &over,
                               const TopLevel &origin,
                               const Path &path,
                               uint_fast16_t depth)
{
    Q_ASSERT(under.type() == Node::Type::Matrix);

    /* First, recurse the second stage */
    Path childPath = path;
    for (std::size_t index = 0, max = over.children.size(); index < max; ++index) {
        if (index >= under.children.size())
            under.children.emplace_back();

        childPath.emplace_back(PathElement::Type::Matrix, over.fromIndex(index));
        Overlay::second(under.children[index], over.children[index], origin, childPath, depth);
        childPath.pop_back();
    }

    /* Now detach and make constant all children, so
     * future overlays can re-use them */
    for (auto &child : under.children) {
        child->detach(child);
    }
}

std::string NodeMatrix::describe() const
{
    std::string str = "Matrix(";
    bool first = true;
    std::size_t index = 0;
    for (const auto &child : children) {
        if (!first)
            str += ",";
        first = false;
        str += "[";
        {
            auto coords = fromIndex(index++);
            bool cfirst = true;
            for (auto c : coords) {
                if (!cfirst)
                    str += ",";
                cfirst = false;
                str += std::to_string(c);
            }
        }
        str += "]=>";
        str += child->describe();
    }
    str += ")";
    return str;
}

void NodeMatrix::clear()
{ children.clear(); }

bool NodeMatrix::identicalTo(const Node &other) const
{
    switch (other.type()) {
    case Type::Matrix:
    case Type::MatrixConstant:
        break;
    default:
        return false;
    }
    const auto &othera = static_cast<const NodeMatrix &>(other);
    if (children.size() != othera.children.size())
        return false;
    if (size != othera.size)
        return false;
    for (auto child = children.begin(), otherc = othera.children.begin(), end = children.end();
            child != end;
            ++child, ++otherc) {
        if (!(*child)->identicalTo(*otherc->get()))
            return false;
    }
    return true;
}

bool NodeMatrix::equalTo(const Node &other, const Node &metadata) const
{
    switch (other.type()) {
    case Type::Matrix:
    case Type::MatrixConstant:
        break;
    default:
        return false;
    }

    const auto &othera = static_cast<const NodeMatrix &>(other);
    if (children.size() != othera.children.size())
        return false;
    if (size != othera.size)
        return false;

    auto nextMeta = metadata.read({PathElement::Type::MetadataMatrix, "Children"});
    if (!nextMeta)
        nextMeta = NodeEmpty::instance;

    for (auto child = children.begin(), otherc = othera.children.begin(), end = children.end();
            child != end;
            ++child, ++otherc) {
        if (!(*child)->equalTo(*otherc->get(), *nextMeta))
            return false;
    }
    return true;
}

std::size_t NodeMatrix::hash() const
{
    std::size_t result = Serialization::ID_Matrix_v2;
    result = INTEGER::mix(result, size.size());
    for (auto add : size) {
        result = INTEGER::mix(result, add);
    }
    result = INTEGER::mix(result, children.size());
    for (const auto &add : children) {
        result = INTEGER::mix(result, add->hash());
    }
    return result;
}


NodeMatrixConstant::~NodeMatrixConstant() = default;

NodeMatrixConstant::NodeMatrixConstant() = default;

NodeMatrixConstant::NodeMatrixConstant(const NodeMatrix &other) : NodeMatrix(other)
{ }

NodeMatrixConstant::NodeMatrixConstant(NodeMatrix::Children &&children,
                                       PathElement::MatrixIndex &&size) : NodeMatrix(
        std::move(children), std::move(size))
{ }

NodeMatrixConstant::NodeMatrixConstant(QDataStream &stream) : NodeMatrix(stream)
{ }

Node::Type NodeMatrixConstant::type() const
{ return Node::Type::MatrixConstant; }

void NodeMatrixConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeMatrixConstant::compatible(Node::WriteGoal) const
{ return false; }

bool NodeMatrixConstant::childrenWritable(PathElement::Type) const
{ return false; }

std::shared_ptr<Node> NodeMatrixConstant::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeMatrixConstant::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void NodeMatrixConstant::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void NodeMatrixConstant::remove(const PathElement &)
{ Q_ASSERT(false); }

void NodeMatrixConstant::clear()
{ Q_ASSERT(false); }


}
}
}