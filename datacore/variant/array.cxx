/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "array.hxx"
#include "overlay.hxx"
#include "serialization.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

NodeArray::~NodeArray() = default;

NodeArray::NodeArray() = default;

NodeArray::NodeArray(const NodeArray &other) : children(other.children)
{
    for (auto &mod : children) {
        mod->detach(mod);
    }
}

NodeArray::NodeArray(Children &&children) : children(std::move(children))
{ }

NodeArray::NodeArray(QDataStream &stream)
{
    std::size_t n = Serialization::deserializeShortLength(stream);
    for (std::size_t i = 0; i < n; ++i) {
        children.emplace_back(Serialization::construct(stream));
    }
}

void NodeArray::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_Array_v2);
    Serialization::serializeShortLength(stream, children.size());
    for (const auto &child : children) {
        child->serialize(stream);
    }
}

Node::Type NodeArray::type() const
{ return Node::Type::Array; }

void NodeArray::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeArrayConstant>(*this); }

std::shared_ptr<Node> NodeArray::read(const PathElement &path) const
{
    if (children.empty())
        return std::shared_ptr<Node>();
    switch (path.type) {
    case PathElement::Type::Empty:
        return children.front();
    case PathElement::Type::Array: {
        auto index = path.ref<PathElement::ArrayIndex>();
        if (index >= children.size())
            return std::shared_ptr<Node>();
        return children[index];
    }
    default:
        break;
    }
    return std::shared_ptr<Node>();
}

bool NodeArray::compatible(Node::WriteGoal goal) const
{ return goal == WriteGoal::Array; }

bool NodeArray::childrenWritable(PathElement::Type type) const
{
    switch (type) {
    case PathElement::Type::Empty:
    case PathElement::Type::Array:
        return true;
    default:
        break;
    }
    return false;
}

std::shared_ptr<Node> NodeArray::write(const PathElement &path, Node::WriteGoal final)
{
    switch (path.type) {
    case PathElement::Type::Empty:
        if (children.empty()) {
            children.emplace_back(create(final));
        } else if (!children.front()->compatible(final)) {
            children.front() = create(final, children.front().get());
        }
        return children.front();
    case PathElement::Type::Array: {
        auto index = path.ref<PathElement::ArrayIndex>();
        while (index > children.size()) {
            children.emplace_back(NodeEmpty::instance);
        }
        if (index >= children.size()) {
            children.emplace_back(create(final));
            Q_ASSERT(index == children.size() - 1);
            return children.back();
        }
        auto &child = children[index];
        Q_ASSERT(child.get());
        if (!child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    default:
        Q_ASSERT(false);
        break;
    }
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeArray::write(const PathElement &path, PathElement::Type next)
{
    switch (path.type) {
    case PathElement::Type::Empty:
        if (children.empty()) {
            children.emplace_back(create(next));
        } else if (!children.front()->childrenWritable(next)) {
            children.front() = create(next, children.front().get());
        }
        return children.front();
    case PathElement::Type::Array: {
        auto index = path.ref<PathElement::ArrayIndex>();
        while (index > children.size()) {
            children.emplace_back(NodeEmpty::instance);
        }
        if (index >= children.size()) {
            children.emplace_back(create(next));
            Q_ASSERT(index == children.size() - 1);
            return children.back();
        }
        auto &child = children[index];
        Q_ASSERT(child.get());
        if (!child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    default:
        Q_ASSERT(false);
        break;
    }
    return std::shared_ptr<Node>();
}

void NodeArray::insert(const PathElement &path, const std::shared_ptr<Node> &final)
{
    switch (path.type) {
    case PathElement::Type::Empty:
        if (children.empty()) {
            children.emplace_back(final);
        } else {
            children.front() = final;
        }
        children.front()->detach(children.front());
        break;
    case PathElement::Type::Array: {
        auto index = path.ref<PathElement::ArrayIndex>();
        while (index > children.size()) {
            children.emplace_back(NodeEmpty::instance);
        }
        if (index >= children.size()) {
            children.emplace_back(final);
            Q_ASSERT(index == children.size() - 1);
            children.back()->detach(children.back());
            break;
        }
        auto &child = children[index];
        child = final;
        child->detach(child);
        break;
    }
    default:
        Q_ASSERT(false);
        break;
    }
}

void NodeArray::remove(const PathElement &path)
{
    switch (path.type) {
    case PathElement::Type::Empty:
        if (!children.empty())
            children.erase(children.begin());
        break;
    case PathElement::Type::Array: {
        auto index = path.ref<PathElement::ArrayIndex>();
        if (index >= children.size())
            break;
        children.erase(children.begin() + index);
        break;
    }
    default:
        Q_ASSERT(false);
        break;
    }
}

bool NodeArray::bypassFirstOverlay(const Node &over) const
{
    switch (over.type()) {
    case Node::Type::Array:
    case Node::Type::ArrayConstant:
        break;
    default:
        return Node::bypassFirstOverlay(over);
    }
    /* Can't bypass if the over needs a flatten */
    if (!over.bypassSecondOverlay())
        return false;
    /* Otherwise we can bypass if either is
     * opaque (the over just directly replaces
     * us) */
    return isOverlayOpaque() || over.isOverlayOpaque();
}

bool NodeArray::bypassSecondOverlay() const
{
    for (const auto &child : children) {
        if (!child->bypassSecondOverlay())
            return false;
    }
    return true;
}

bool NodeArray::isOverlayOpaque() const
{
    for (const auto &child : children) {
        if (!child->isOverlayOpaque())
            return false;
    }
    return true;
}

void NodeArray::overlayFirst(NodeArray &under, const NodeArray &over)
{
    Q_ASSERT(under.type() == Node::Type::Array);

    /* Either being opaque means just replace the under entirely */
    if (under.isOverlayOpaque() || over.isOverlayOpaque()) {
        under.children.clear();
        for (const auto &add : over.children) {
            under.children.emplace_back();
            Overlay::first(under.children.back(), add);
        }
        return;
    }

    for (std::size_t index = 0, max = over.children.size(); index < max; ++index) {
        if (index >= under.children.size())
            under.children.emplace_back();
        Overlay::first(under.children[index], over.children[index]);
    }
}

void NodeArray::overlaySecond(NodeArray &under,
                              const NodeArray &over,
                              const TopLevel &origin,
                              const Path &path,
                              uint_fast16_t depth)
{
    Q_ASSERT(under.type() == Node::Type::Array);

    /* First, recurse the second stage */
    Path childPath = path;
    for (std::size_t index = 0, max = over.children.size(); index < max; ++index) {
        if (index >= under.children.size())
            under.children.emplace_back();

        childPath.emplace_back(PathElement::Type::Array, index);
        Overlay::second(under.children[index], over.children[index], origin, childPath, depth);
        childPath.pop_back();
    }

    /* Now detach and make constant all children, so
     * future overlays can re-use them */
    for (auto &child : under.children) {
        child->detach(child);
    }
}

std::string NodeArray::describe() const
{
    std::string str = "Array(";
    bool first = true;
    for (const auto &child : children) {
        if (!first)
            str += ",";
        first = false;
        str += child->describe();
    }
    str += ")";
    return str;
}

void NodeArray::clear()
{ children.clear(); }

bool NodeArray::identicalTo(const Node &other) const
{
    switch (other.type()) {
    case Type::Array:
    case Type::ArrayConstant:
        break;
    default:
        return false;
    }
    const auto &othera = static_cast<const NodeArray &>(other);
    if (children.size() != othera.children.size())
        return false;
    for (auto child = children.begin(), otherc = othera.children.begin(), end = children.end();
            child != end;
            ++child, ++otherc) {
        if (!(*child)->identicalTo(*otherc->get()))
            return false;
    }
    return true;
}

bool NodeArray::equalTo(const Node &other, const Node &metadata) const
{
    switch (other.type()) {
    case Type::Array:
    case Type::ArrayConstant:
        break;
    default:
        return false;
    }

    const auto &othera = static_cast<const NodeArray &>(other);
    if (children.size() != othera.children.size())
        return false;

    auto nextMeta = metadata.read({PathElement::Type::MetadataArray, "Children"});
    if (!nextMeta)
        nextMeta = NodeEmpty::instance;

    for (auto child = children.begin(), otherc = othera.children.begin(), end = children.end();
            child != end;
            ++child, ++otherc) {
        if (!(*child)->equalTo(*otherc->get(), *nextMeta))
            return false;
    }
    return true;
}

std::size_t NodeArray::hash() const
{
    std::size_t result = Serialization::ID_Array_v2;
    result = INTEGER::mix(result, children.size());
    for (const auto &add : children) {
        result = INTEGER::mix(result, add->hash());
    }
    return result;
}


NodeArrayConstant::~NodeArrayConstant() = default;

NodeArrayConstant::NodeArrayConstant() = default;

NodeArrayConstant::NodeArrayConstant(const NodeArray &other) : NodeArray(other)
{ }

NodeArrayConstant::NodeArrayConstant(NodeArray::Children &&children) : NodeArray(
        std::move(children))
{ }

NodeArrayConstant::NodeArrayConstant(QDataStream &stream) : NodeArray(stream)
{ }

Node::Type NodeArrayConstant::type() const
{ return Node::Type::ArrayConstant; }

void NodeArrayConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeArrayConstant::compatible(Node::WriteGoal) const
{ return false; }

bool NodeArrayConstant::childrenWritable(PathElement::Type) const
{ return false; }

std::shared_ptr<Node> NodeArrayConstant::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeArrayConstant::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void NodeArrayConstant::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void NodeArrayConstant::remove(const PathElement &)
{ Q_ASSERT(false); }

void NodeArrayConstant::clear()
{ Q_ASSERT(false); }


}
}
}