/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <sstream>
#include <QRegularExpression>

#include "strings.hxx"
#include "serialization.hxx"
#include "hash.hxx"
#include "core/util.hxx"


namespace CPD3 {
namespace Data {
namespace Variant {


NodeString::~NodeString() = default;

NodeString::NodeString() = default;

NodeString::NodeString(const NodeString &other) = default;

NodeString::NodeString(NodeString &&other) : value(std::move(other.value))
{
    Q_ASSERT(other.type() != Node::Type::StringConstant);
    Q_ASSERT(other.type() != Node::Type::LocalizedStringConstant);
}

NodeString::NodeString(const std::string &value) : value(value)
{ }

NodeString::NodeString(std::string &&value) : value(std::move(value))
{ }

NodeString::NodeString(QDataStream &stream)
{
    value = Serialization::deserializeShortString(stream);
}

void NodeString::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_String_v2);
    Serialization::serializeShortString(stream, value);
}

Node::Type NodeString::type() const
{ return Node::Type::String; }

void NodeString::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeStringConstant>(*this); }

bool NodeString::compatible(Node::WriteGoal goal) const
{ return goal == Node::WriteGoal::String; }

std::string NodeString::describe() const
{
    if (value.find_first_of("\"\n\r\t ") == value.npos) {
        return std::string("String(") + value + ")";
    } else {
        std::string escaped = value;
        Util::replace_all(escaped, "\"", "\\\"");
        Util::replace_all(escaped, "\n", "\\n");
        Util::replace_all(escaped, "\r", "\\r");
        Util::replace_all(escaped, "\t", "\\t");
        return std::string("String(\"") + escaped + "\")";
    }
}

const std::string &NodeString::toString() const
{ return value; }

QString NodeString::toDisplayString(const QLocale &) const
{ return QString::fromStdString(value); }

void NodeString::setString(const std::string &value)
{
    Q_ASSERT(type() != Node::Type::StringConstant);
    Q_ASSERT(type() != Node::Type::LocalizedStringConstant);
    this->value = value;
}

void NodeString::setString(std::string &&value)
{
    Q_ASSERT(type() != Node::Type::StringConstant);
    Q_ASSERT(type() != Node::Type::LocalizedStringConstant);
    this->value = std::move(value);
}

void NodeString::clear()
{ value.clear(); }

bool NodeString::identicalTo(const Node &other) const
{
    switch (other.type()) {
    case Type::String:
    case Type::StringConstant:
        break;
    default:
        return false;
    }
    return value == static_cast<const NodeString &>(other).value;
}

static bool matchesEnumValue(const PathElement::HashIndex &key,
                             const std::shared_ptr<Node> &node,
                             const std::string &value)
{
    if (Util::equal_insensitive(value, key))
        return true;
    auto ptr = node->read({PathElement::Type::Hash, "Alias"});
    if (!ptr)
        return false;
    for (const auto &check : ptr->toFlags()) {
        if (Util::equal_insensitive(value, check))
            return true;
    }
    return false;
}

bool NodeString::equalTo(const Node &other, const Node &metadata) const
{
    switch (other.type()) {
    case Type::String:
    case Type::StringConstant:
    case Type::LocalizedString:
    case Type::LocalizedStringConstant:
        break;
    default:
        return identicalTo(other);
    }

    auto values = metadata.read({PathElement::Type::MetadataString, "EnumerationValues"});
    if (!values)
        return identicalTo(other);

    const auto &ovalue = static_cast<const NodeString &>(other).value;
    if (Util::equal_insensitive(value, ovalue))
        return true;

    switch (values->type()) {
    case Type::Hash:
    case Type::HashConstant:
        break;
    default:
        return false;
    }

    const auto &evalues = static_cast<const NodeHash &>(*values);

    bool hit = false;
    evalues.forAllChildren(
            [&](const PathElement::HashIndex &key, const std::shared_ptr<Node> &node) {
                if (!matchesEnumValue(key, node, value)) {
                    if (matchesEnumValue(key, node, ovalue))
                        return true;
                    return false;
                }
                hit = matchesEnumValue(key, node, ovalue);
                return true;
            });
    if (hit)
        return true;

    return false;
}

std::size_t NodeString::hash() const
{ return INTEGER::mix(Serialization::ID_String_v2, std::hash<std::string>()(value)); }


NodeLocalizedString::~NodeLocalizedString() = default;

NodeLocalizedString::NodeLocalizedString() = default;

NodeLocalizedString::NodeLocalizedString(NodeLocalizedString &&other) : NodeString(
        std::move(static_cast<NodeString &>(other))), localized(std::move(other.localized))
{
    Q_ASSERT(other.type() != Node::Type::LocalizedStringConstant);
}

NodeLocalizedString::NodeLocalizedString(const NodeString &other) : NodeString(other)
{ }

NodeLocalizedString::NodeLocalizedString(NodeString &&other) : NodeString(std::move(other))
{ }

NodeLocalizedString::NodeLocalizedString(const NodeLocalizedString &other) = default;

NodeLocalizedString::NodeLocalizedString(const std::string &value,
                                         const NodeLocalizedString::Localization &localized)
        : NodeString(value), localized(localized)
{ }

NodeLocalizedString::NodeLocalizedString(std::string &&value,
                                         NodeLocalizedString::Localization &&localized)
        : NodeString(std::move(value)), localized(std::move(localized))
{ }

NodeLocalizedString::NodeLocalizedString(QDataStream &stream) : NodeString(stream)
{ stream >> localized; }

void NodeLocalizedString::serialize(QDataStream &stream) const
{
    if (canDemote()) {
        NodeString::serialize(stream);
        return;
    }
    stream << static_cast<quint8>(Serialization::ID_LocalizeString_v2);
    Serialization::serializeShortString(stream, value);
    stream << localized;
}

Node::Type NodeLocalizedString::type() const
{ return Node::Type::LocalizedString; }

void NodeLocalizedString::detach(std::shared_ptr<Node> &self) const
{
    if (canDemote()) {
        self = std::make_shared<NodeStringConstant>(*this);
    } else {
        self = std::make_shared<NodeLocalizedStringConstant>(*this);
    }
}

bool NodeLocalizedString::compatible(Node::WriteGoal goal) const
{
    switch (goal) {
    case Node::WriteGoal::String:
    case Node::WriteGoal::LocalizedString:
        return true;
    default:
        break;
    }
    return false;
}

std::string NodeLocalizedString::describe() const
{
    if (localized.empty())
        return NodeString::describe();

    std::stringstream str;
    str << "String(";

    if (value.find_first_of("\"\n\r\t ") == value.npos) {
        str << value;
    } else {
        std::string escaped = value;
        Util::replace_all(escaped, "\"", "\\\"");
        Util::replace_all(escaped, "\n", "\\n");
        Util::replace_all(escaped, "\r", "\\r");
        Util::replace_all(escaped, "\t", "\\t");
        str << "\"" << escaped << "\"";
    }

    std::vector<Localization::const_iterator> sorted;
    for (auto add = localized.begin(), end = localized.end(); add != end; ++add) {
        sorted.emplace_back(add);
    }
    std::sort(sorted.begin(), sorted.end(),
              [](const Localization::const_iterator &a, const Localization::const_iterator &b) {
                  return a->first < b->first;
              });

    static thread_local QRegularExpression reEscape("[\"\n\r\t ]");

    for (const auto &locale : sorted) {
        str << " ";

        str << locale->first.toStdString();

        QString escaped = locale->second;
        escaped.replace('\"', "\\\"");
        escaped.replace('\n', "\\n");
        escaped.replace('\r', "\\r");
        escaped.replace('\t', "\\t");
        str << "\"" << escaped.toStdString() << "\"";
    }
    str << ")";
    return str.str();
}

QString NodeLocalizedString::toDisplayString(const QLocale &locale) const
{
    for (const auto &lang : locale.uiLanguages()) {
        auto check = localized.find(lang);
        if (check != localized.end())
            return check->second;
    }

    QString name = locale.bcp47Name();
    if (!name.isEmpty()) {
        auto check = localized.find(name);
        if (check != localized.end())
            return check->second;
        check = localized.find(name.section('_', 0, 1));
        if (check != localized.end())
            return check->second;
    }
    name = locale.name();
    if (!name.isEmpty()) {
        auto check = localized.find(name);
        if (check != localized.end())
            return check->second;
        check = localized.find(name.section('_', 0, 1));
        if (check != localized.end())
            return check->second;
    }
    return NodeString::toDisplayString(locale);
}

void NodeLocalizedString::setDisplayString(const QString &locale, const QString &value)
{
    Q_ASSERT(type() != Node::Type::LocalizedStringConstant);
    if (locale.isEmpty()) {
        setString(value.toStdString());
        return;
    }
    if (value.isEmpty()) {
        localized.erase(locale);
        return;
    }
    localized[locale] = value;
}

NodeLocalizedString::Localization NodeLocalizedString::getAllStrings() const
{
    Localization result = localized;
    Q_ASSERT(result.count(QString()) == 0);
    result.emplace(QString(), QString::fromStdString(value));
    return std::move(result);
}

void NodeLocalizedString::clear()
{
    NodeString::clear();
    localized.clear();
}

bool NodeLocalizedString::identicalTo(const Node &other) const
{
    switch (other.type()) {
    case Type::String:
    case Type::StringConstant:
        if (!localized.empty())
            return false;
        return value == static_cast<const NodeString &>(other).value;
    case Type::LocalizedString:
    case Type::LocalizedStringConstant:
        break;
    default:
        return false;
    }
    if (value != static_cast<const NodeLocalizedString &>(other).value)
        return false;
    return localized == static_cast<const NodeLocalizedString &>(other).localized;
}

std::size_t NodeLocalizedString::hash() const
{
    std::size_t result = Serialization::ID_LocalizeString_v2;
    result = INTEGER::mix(result, std::hash<std::string>()(value));
    /* Just use simple XOR, since the order doesn't matter then */
    for (const auto &add : localized) {
        result ^= std::hash<QString>()(add.first);
        result ^= std::hash<QString>()(add.second);
    }
    return result;
}


NodeStringConstant::~NodeStringConstant() = default;

NodeStringConstant::NodeStringConstant() = default;

NodeStringConstant::NodeStringConstant(const NodeString &other) : NodeString(other)
{ }

NodeStringConstant::NodeStringConstant(const std::string &value) : NodeString(value)
{ }

NodeStringConstant::NodeStringConstant(std::string &&value) : NodeString(std::move(value))
{ }

NodeStringConstant::NodeStringConstant(QDataStream &stream) : NodeString(stream)
{ }

Node::Type NodeStringConstant::type() const
{ return Node::Type::StringConstant; }

void NodeStringConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeStringConstant::compatible(Node::WriteGoal) const
{ return false; }

void NodeStringConstant::clear()
{ Q_ASSERT(false); }


NodeLocalizedStringConstant::~NodeLocalizedStringConstant() = default;

NodeLocalizedStringConstant::NodeLocalizedStringConstant() = default;

NodeLocalizedStringConstant::NodeLocalizedStringConstant(const NodeLocalizedString &other)
        : NodeLocalizedString(other)
{ }

NodeLocalizedStringConstant::NodeLocalizedStringConstant(const std::string &value,
                                                         const NodeLocalizedString::Localization &localized)
        : NodeLocalizedString(value, localized)
{ }

NodeLocalizedStringConstant::NodeLocalizedStringConstant(std::string &&value,
                                                         NodeLocalizedString::Localization &&localized)
        : NodeLocalizedString(std::move(value), std::move(localized))
{ }

NodeLocalizedStringConstant::NodeLocalizedStringConstant(QDataStream &stream) : NodeLocalizedString(
        stream)
{ }

Node::Type NodeLocalizedStringConstant::type() const
{ return Node::Type::LocalizedStringConstant; }

void NodeLocalizedStringConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeLocalizedStringConstant::compatible(Node::WriteGoal) const
{ return false; }

void NodeLocalizedStringConstant::clear()
{ Q_ASSERT(false); }

}
}
}