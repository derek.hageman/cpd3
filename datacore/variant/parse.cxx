/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <algorithm>

#include "parse.hxx"
#include "core/util.hxx"


namespace CPD3 {
namespace Data {
namespace Variant {
namespace Parse {


static QString escapeInputString(QString input)
{
    input.replace('\\', "\\\\");
    input.replace('"', "\\\"");
    input.replace('\n', "\\n");
    input.replace('\r', "\\r");
    input.replace('\t', "\\t");
    return std::move(input);
}

QString toInputString(const Read &value, const Read &metadata)
{
    switch (value.getType()) {
    case Type::Empty:
        return QString('_');
    case Type::Real: {
        double v = value.toReal();
        if (!FP::defined(v)) {
            auto mvc = metadata.metadataReal("MVC").toDisplayString();
            if (!mvc.isEmpty())
                return mvc;
            mvc = metadata.metadataReal("Format").toDisplayString();
            if (!mvc.isEmpty())
                return NumberFormat(mvc).mvc();
            return QLatin1String("NaN");
        }

        auto format = metadata.metadataReal("Format").toDisplayString();
        if (!format.isEmpty())
            return NumberFormat(format).apply(v, QChar());

        int decimals = 1;
        for (double power = 10.0; decimals < 6; decimals++, power *= 10.0) {
            if (std::floor(v * power) == std::ceil(v * power))
                break;
        }
        return NumberFormat(1, decimals).apply(v, QChar());
    }
    case Type::Integer: {
        auto v = value.toInteger();
        if (!INTEGER::defined(v)) {
            auto mvc = metadata.metadataInteger("MVC").toDisplayString();
            if (!mvc.isEmpty())
                return mvc;
            mvc = metadata.metadataInteger("Format").toDisplayString();
            if (!mvc.isEmpty())
                return NumberFormat(mvc).mvc();
            return QLatin1String("iNaN");
        }

        auto format = metadata.metadataInteger("Format").toDisplayString();
        if (!format.isEmpty())
            return NumberFormat(format).apply(static_cast<qint64>(v), QChar());

        return QString::number(v);
    }
    case Type::Boolean:
        if (value.toBoolean())
            return QLatin1String("TRUE");
        return QLatin1String("FALSE");
    case Type::String: {
        QString result('"');
        result += escapeInputString(QString::fromStdString(value.toString()));
        result += '"';

        std::unordered_map<QString, QString> localized = value.allLocalizedStrings();
        std::vector<std::unordered_map<QString, QString>::const_iterator> sorted;
        for (auto add = localized.cbegin(), endAdd = localized.cend(); add != endAdd; ++add) {
            if (add->first.isEmpty() ||
                    add->first.contains('"') ||
                    add->first.contains('\n') ||
                    add->first.contains('\r') ||
                    add->first.contains(' '))
                continue;
            sorted.emplace_back(add);
        }
        std::sort(sorted.begin(), sorted.end(),
                  [](const std::unordered_map<QString, QString>::const_iterator &a,
                     const std::unordered_map<QString, QString>::const_iterator &b) {
                      return a->first < b->first;
                  });
        for (const auto &locale : sorted) {
            result += ' ';
            result += locale->first;
            result += '"';
            result += escapeInputString(locale->second);
            result += '"';
        }
        return result;
    }
    case Type::Bytes: {
        QString result('{');
        result += QString::fromLatin1(value.toBytes().toBase64());
        result += '}';
        return result;
    }
    case Type::Flags: {
        QStringList sorted;
        for (const auto &add : value.toFlags()) {
            sorted.push_back(escapeInputString(QString::fromStdString(add)));
        }
        std::sort(sorted.begin(), sorted.end());
        QString result('|');
        result += sorted.join('|');
        return result;
    }
    case Type::Hash:
        return QLatin1String("=HASH");
    case Type::Array:
        return QLatin1String("=ARRAY");
    case Type::Matrix:
        return QLatin1String("=MATRIX");
    case Type::Keyframe:
        return QLatin1String("=KEYFRAME");
    case Type::MetadataReal:
        return QLatin1String("=METAREAL");
    case Type::MetadataInteger:
        return QLatin1String("=METAINTEGER");
    case Type::MetadataBoolean:
        return QLatin1String("=METABOOLEAN");
    case Type::MetadataString:
        return QLatin1String("=METASTRING");
    case Type::MetadataBytes:
        return QLatin1String("=METABYTES");
    case Type::MetadataFlags:
        return QLatin1String("=METAFLAGS");
    case Type::MetadataHash:
        return QLatin1String("=METAHASH");
    case Type::MetadataArray:
        return QLatin1String("=METAARRAY");
    case Type::MetadataMatrix:
        return QLatin1String("=METAMATRIX");
    case Type::MetadataKeyframe:
        return QLatin1String("=METAKEYFRAME");
    case Type::Overlay: {
        QString result('~');
        result += escapeInputString(QString::fromStdString(value.toString()));
        return result;
    }
    }

    Q_ASSERT(false);
    return QString();
}

QString toInputString(const Read &value)
{ return toInputString(value, Variant::Read::empty()); }

static bool unescapeInputString(const QString &input, int &index, QString &output)
{
    for (int max = input.length(); index < max; ++index) {
        QChar check(input.at(index));
        if (check == '\"') {
            ++index;
            return true;
        } else if (check == '\\') {
            ++index;
            if (index >= max)
                return false;
            check = input.at(index);
            if (check == 'n') {
                check = '\n';
            } else if (check == 'r') {
                check = '\r';
            } else if (check == 't') {
                check = '\t';
            }
            /* Otherwise, leave it so \X produces X */
        }
        output.append(check);
    }
    return false;
}

static bool parseInputString(const QString &input, Write &output)
{
    Q_ASSERT(input.startsWith('"'));

    QString base;
    int index = 1;
    if (!unescapeInputString(input, index, base))
        return false;

    std::unordered_map<QString, QString> localized;
    QString locale;
    for (int max = input.length(); index < max; ++index) {
        QChar check(input.at(index));
        if (check.isSpace())
            continue;
        if (check == '\"') {
            ++index;
            if (locale.isEmpty())
                return false;
            QString add;
            if (!unescapeInputString(input, index, add))
                return false;
            localized.emplace(std::move(locale), std::move(add));
            locale = QString();
            continue;
        }
        locale.append(check);
    }
    if (!locale.isEmpty())
        return false;

    for (const auto &add : localized) {
        output.setDisplayString(add.first, add.second);
    }
    output.setString(base.toStdString());
    return true;
}

bool fromInputString(Write &value,
                     const QString &input,
                     const Read &metadata,
                     const Read &reference,
                     bool acceptAmbiguous)
{
    /* Overlays always take priority */
    if (input.startsWith('~') && input.length() > 1) {
        value.setOverlay(input.mid(1));
        return true;
    }

    /* First try parsing based on the metadata type */
    if (acceptAmbiguous || !metadata.metadata("ArbitraryStructure").toBoolean()) {
        bool metadataParseMismatch = false;

        switch (metadata.getType()) {
        case Type::MetadataReal: {
            {
                auto compare = input.toLower();
                if (compare == "nan" ||
                        compare == "undef" ||
                        compare == "undefined" ||
                        compare == "inf" ||
                        compare == "infinity" ||
                        compare == "0dnan" ||
                        compare == "0dundef" ||
                        compare == "0dundefined" ||
                        compare == "0dinf" ||
                        compare == "0dinfinity") {
                    value.setReal(FP::undefined());
                    return true;
                }
                if (compare == "inan") {
                    if (!acceptAmbiguous)
                        return false;
                    value.setReal(FP::undefined());
                    return true;
                }
            }

            {
                auto mvc = metadata.metadataReal("MVC").toDisplayString();
                if (!mvc.isEmpty()) {
                    if (input == mvc) {
                        value.setReal(FP::undefined());
                        return true;
                    }
                }
            }

            {
                auto format = metadata.metadataReal("Format").toDisplayString();
                if (!format.isEmpty()) {
                    auto mvc = NumberFormat(format).mvc();
                    if (input == mvc) {
                        value.setReal(FP::undefined());
                        return true;
                    }
                }
            }

            bool ok = false;
            double v = input.toDouble(&ok);
            if (ok) {
                value.setReal(v);
                return true;
            }
            metadataParseMismatch = true;
            break;
        }
        case Type::MetadataInteger: {
            {
                auto compare = input.toLower();
                if (compare == "inan" ||
                        compare == "undef" ||
                        compare == "undefined" ||
                        compare == "inf" ||
                        compare == "infinity" ||
                        compare == "0inan" ||
                        compare == "0iundef" ||
                        compare == "0iundefined" ||
                        compare == "0iinf" ||
                        compare == "0iinfinity") {
                    value.setInteger(INTEGER::undefined());
                    return true;
                }
                if (compare == "nan") {
                    if (!acceptAmbiguous)
                        return false;
                    value.setInteger(INTEGER::undefined());
                    return true;
                }
            }

            {
                auto mvc = metadata.metadataInteger("MVC").toDisplayString();
                if (!mvc.isEmpty()) {
                    if (input == mvc) {
                        value.setReal(FP::undefined());
                        return true;
                    }
                }
            }

            auto format = metadata.metadataInteger("Format").toDisplayString();
            if (!format.isEmpty()) {
                auto mvc = NumberFormat(format).mvc();
                if (input == mvc) {
                    value.setReal(FP::undefined());
                    return true;
                }
            }

            bool ok = false;
            qint64 v;
            if (input.startsWith("0i", Qt::CaseInsensitive)) {
                v = input.mid(2).toLongLong(&ok);
            } else if (input.startsWith("0x", Qt::CaseInsensitive)) {
                v = input.mid(2).toLongLong(&ok, 16);
            } else if (input.startsWith("0o", Qt::CaseInsensitive)) {
                v = input.mid(2).toLongLong(&ok, 8);
            } else if (input.startsWith("0b", Qt::CaseInsensitive)) {
                v = input.mid(2).toLongLong(&ok, 2);
            } else {
                int base = 10;
                if (!format.isEmpty()) {
                    NumberFormat nf(format);
                    switch (nf.getMode()) {
                    case NumberFormat::Decimal:
                    case NumberFormat::Scientific:
                        break;
                    case NumberFormat::Hex:
                        base = 16;
                        break;
                    case NumberFormat::Octal:
                        base = 8;
                        break;
                    }
                }
                v = input.toLongLong(&ok, base);
            }
            if (ok) {
                value.setInteger(v);
                return true;
            }

            double dv = input.toDouble(&ok);
            if (ok && std::floor(dv) == std::ceil(dv)) {
                value.setInteger(static_cast<std::int_fast64_t>(dv));
                return true;
            }

            metadataParseMismatch = true;
            break;
        }
        case Type::MetadataBoolean: {
            {
                auto compare = input.toLower();
                if (compare == "true" || compare == "t" || compare == "on" || compare == "yes") {
                    value.setBoolean(true);
                    return true;
                }
                if (compare == "false" || compare == "f" || compare == "off" || compare == "no") {
                    value.setBoolean(false);
                    return true;
                }
            }

            bool ok = false;
            int v = input.toInt(&ok);
            if (ok && v >= 0) {
                value.setBoolean(v > 0);
                return true;
            }
            metadataParseMismatch = true;
            break;
        }
        case Type::MetadataBytes: {
            QString str = input;
            if (str.startsWith('{'))
                str = str.mid(1);
            if (str.endsWith('}'))
                str.chop(1);

            QByteArray data = QByteArray::fromBase64(str.toLatin1());
            if (data.isEmpty() && !str.isEmpty()) {
                metadataParseMismatch = true;
                break;
            }
            value.setBytes(std::move(data));
            return true;
        }
        case Type::MetadataFlags: {
            Flags flags;
            for (const auto &f : input.split('|', QString::SkipEmptyParts)) {
                if (f.isEmpty())
                    continue;
                flags.insert(f.toStdString());
            }
            value.setFlags(std::move(flags));
            return true;
        }
        case Type::MetadataString: {
            if (!input.startsWith('"')) {
                if (!acceptAmbiguous)
                    return false;
                value.setString(input.toStdString());
                return true;
            }

            if (parseInputString(input, value))
                return true;

            metadataParseMismatch = true;
            break;
        }
        default:
            break;
        }

        if (metadataParseMismatch && !acceptAmbiguous)
            return false;
    }

    /* No metadata or parsing failed for the type in the metadata, so try some unambiguous
     * parsing types */

    if (input.startsWith('"')) {
        if (parseInputString(input, value))
            return true;
    } else if (input.startsWith('|')) {
        Flags flags;
        for (const auto &f : input.split('|', QString::SkipEmptyParts)) {
            if (f.isEmpty())
                continue;
            flags.insert(f.toStdString());
        }
        value.setFlags(std::move(flags));
        return true;
    } else if (input.startsWith('{')) {
        QString str(input.mid(1));
        str.chop(1);
        if (str.endsWith('}'))
            str.chop(1);

        QByteArray data = QByteArray::fromBase64(str.toLatin1());
        if (!data.isEmpty() || str.isEmpty()) {
            value.setBytes(std::move(data));
            return true;
        }
    } else if (input.startsWith('=') && input.length() > 1) {
        auto compare = input.mid(1).toLower();
        if (compare == "d" ||
                compare == "r" ||
                compare == "double" ||
                compare == "real" ||
                compare == "number") {
            value.setType(Type::Real);
            return true;
        } else if (compare == "i" || compare == "integer") {
            value.setType(Type::Integer);
            return true;
        } else if (compare == "b" || compare == "boolean" || compare == "bool") {
            value.setType(Type::Boolean);
            return true;
        } else if (compare == "s" || compare == "string" || compare == "str" || compare == "text") {
            value.setType(Type::String);
            return true;
        } else if (compare == "n" || compare == "bytes" || compare == "binary") {
            value.setType(Type::Bytes);
            return true;
        } else if (compare == "a" || compare == "array") {
            value.setType(Type::Array);
            return true;
        } else if (compare == "t" || compare == "matrix") {
            value.setType(Type::Matrix);
            return true;
        } else if (compare == "e" || compare == "keyframe") {
            value.setType(Type::Keyframe);
            return true;
        } else if (compare == "h" || compare == "hash") {
            value.setType(Type::Hash);
            return true;
        } else if (compare == "f" || compare == "flags") {
            value.setType(Type::Flags);
            return true;
        } else if (compare == "*d" ||
                compare == "*r" ||
                compare == "md" ||
                compare == "mr" ||
                compare == "metadouble" ||
                compare == "metareal" ||
                compare == "metanumber") {
            value.setType(Type::MetadataReal);
            return true;
        } else if (compare == "*i" ||
                compare == "mi" ||
                compare == "metainteger" ||
                compare == "metaint") {
            value.setType(Type::MetadataInteger);
            return true;
        } else if (compare == "*b" ||
                compare == "mb" ||
                compare == "metaboolean" ||
                compare == "metabool") {
            value.setType(Type::MetadataBoolean);
            return true;
        } else if (compare == "*s" ||
                compare == "ms" ||
                compare == "metastring" ||
                compare == "metatext") {
            value.setType(Type::MetadataString);
            return true;
        } else if (compare == "*n" ||
                compare == "mn" ||
                compare == "metabytes" ||
                compare == "metabinary") {
            value.setType(Type::MetadataBytes);
            return true;
        } else if (compare == "*a" || compare == "ma" || compare == "metaarray") {
            value.setType(Type::MetadataArray);
            return true;
        } else if (compare == "*e" || compare == "me" || compare == "metakeyframe") {
            value.setType(Type::MetadataKeyframe);
            return true;
        } else if (compare == "*h" || compare == "mh" || compare == "*c" || compare == "metahash") {
            value.setType(Type::MetadataHash);
            return true;
        } else if (compare == "*f" ||
                compare == "mf" ||
                compare == "*l" ||
                compare == "metaflags") {
            value.setType(Type::MetadataFlags);
            return true;
        } else if (compare == "invalid" ||
                compare == "undefined" ||
                compare == "undef" ||
                compare == "empty") {
            value.setType(Type::Empty);
            return true;
        }
    } else if (input == "_") {
        value.setEmpty();
        return true;
    } else {
        auto compare = input.toLower();
        if (compare == "true" || compare == "t" || compare == "on" || compare == "yes") {
            value.setBoolean(true);
            return true;
        }
        if (compare == "false" || compare == "f" || compare == "off" || compare == "no") {
            value.setBoolean(false);
            return true;
        }
        if (compare == "0dnan" ||
                compare == "0dundef" ||
                compare == "0dundefined" ||
                compare == "0dinf" ||
                compare == "0dinfinity") {
            value.setReal(FP::undefined());
            return true;
        }
        if (compare == "0inan" ||
                compare == "0iundef" ||
                compare == "0iundefined" ||
                compare == "0iinf" ||
                compare == "0iinfinity") {
            value.setInteger(INTEGER::undefined());
            return true;
        }

        if (input.startsWith("0d", Qt::CaseInsensitive)) {
            bool ok = false;
            double v = input.mid(2).toDouble(&ok);
            if (ok) {
                value.setReal(v);
                return true;
            }
        }

        bool ok = false;
        std::int_fast64_t v = 0;
        if (input.startsWith("0i", Qt::CaseInsensitive)) {
            v = input.mid(2).toLongLong(&ok);
        } else if (input.startsWith("0x", Qt::CaseInsensitive)) {
            v = input.mid(2).toLongLong(&ok, 16);
        } else if (input.startsWith("0o", Qt::CaseInsensitive)) {
            v = input.mid(2).toLongLong(&ok, 8);
        } else if (input.startsWith("0b", Qt::CaseInsensitive)) {
            v = input.mid(2).toLongLong(&ok, 2);
        }
        if (ok) {
            value.setInteger(v);
            return true;
        }
    }

    /* Anything further and we're making assumptions, so bail out if disabled */
    if (!acceptAmbiguous)
        return false;

    /* Now try some parsing based on the current type */
    switch (reference.getType()) {
    case Type::Real: {
        auto compare = input.toLower();
        if (compare == "nan" ||
                compare == "inan" ||
                compare == "undef" ||
                compare == "undefined" ||
                compare == "inf" ||
                compare == "infinity" ||
                compare == "0dnan" ||
                compare == "0dundef" ||
                compare == "0dundefined" ||
                compare == "0dinf" ||
                compare == "0dinfinity") {
            value.setReal(FP::undefined());
            return true;
        }

        bool ok = false;
        double v = input.toDouble(&ok);
        if (ok) {
            value.setReal(v);
            return true;
        }
        break;
    }
    case Type::Integer: {
        auto compare = input.toLower();
        if (compare == "nan" ||
                compare == "inan" ||
                compare == "undef" ||
                compare == "undefined" ||
                compare == "inf" ||
                compare == "infinity" ||
                compare == "0dnan" ||
                compare == "0dundef" ||
                compare == "0dundefined" ||
                compare == "0dinf" ||
                compare == "0dinfinity") {
            value.setInteger(INTEGER::undefined());
            return true;
        }

        bool ok = false;
        std::int_fast64_t v = input.toLongLong(&ok);
        if (ok) {
            value.setInteger(v);
            return true;
        }
        break;
    }
    case Type::Boolean: {
        bool ok = false;
        int v = input.toInt(&ok);
        if (ok && v >= 0) {
            value.setBoolean(v > 0);
            return true;
        }
        break;
    }
    case Type::String:
        value.setString(input.toStdString());
        return true;
    case Type::Bytes: {
        QByteArray data = QByteArray::fromBase64(input.toLatin1());
        if (!data.isEmpty() || input.isEmpty()) {
            value.setBytes(std::move(data));
            return true;
        }
        break;
    }
    case Type::Flags: {
        Flags flags;
        for (const auto &f : input.split('|', QString::SkipEmptyParts)) {
            if (f.isEmpty())
                continue;
            flags.insert(f.toStdString());
        }
        value.setFlags(std::move(flags));
        return true;
    }

    case Type::Overlay:
        if (!input.isEmpty()) {
            value.setOverlay(input.toStdString());
            return true;
        }
        break;

    default:
        break;
    }

    if (input.contains('|')) {
        Flags flags;
        for (const auto &f : input.split('|', QString::SkipEmptyParts)) {
            if (f.isEmpty())
                continue;
            flags.insert(f.toStdString());
        }
        value.setFlags(std::move(flags));
        return true;
    }

    {
        bool ok = false;
        std::int_fast64_t v = input.toLongLong(&ok);
        if (ok) {
            if (reference.getType() == Type::Real)
                value.setReal(static_cast<double>(v));
            else
                value.setInteger(v);
            return true;
        }
    }

    {
        bool ok = false;
        double v = input.toDouble(&ok);
        if (ok) {
            if (reference.getType() == Type::Integer && std::floor(v) == std::ceil(v))
                value.setInteger(static_cast<int_fast64_t>(v));
            else
                value.setReal(v);
            return true;
        }
    }

    value.setString(input.toStdString());
    return true;
}

bool fromInputString(Write &&value,
                     const QString &input,
                     const Read &metadata,
                     const Read &reference,
                     bool acceptAmbiguous)
{ return fromInputString(value, input, metadata, reference, acceptAmbiguous); }

bool fromInputString(Write &value, const QString &input, bool acceptAmbiguous)
{
    return fromInputString(std::move(value), input, Variant::Read::empty(), Variant::Read::empty(),
                           acceptAmbiguous);
}

bool fromInputString(Write &&value, const QString &input, bool acceptAmbiguous)
{ return fromInputString(value, input, acceptAmbiguous); }

static bool overlayMatches(const Read &metadata, const Read &data, const Read &contents)
{
    return Root::overlay(Root(data), Root(contents)).read().equivalentTo(data, metadata);
}

static void evaluateMetadataOverlays(const Read &metadata,
                                     const Read &data,
                                     std::vector<Root> &mergeContents)
{
    for (auto check : metadata.metadata("Overlay").toArray()) {
        {
            auto require = check.hash("Require");
            if (require.exists()) {
                if (!overlayMatches(metadata, data, require))
                    continue;
            }
        }
        {
            auto exclude = check.hash("Exclude");
            if (exclude.exists()) {
                if (overlayMatches(metadata, data, exclude))
                    continue;
            }
        }

        mergeContents.emplace_back(check.hash("Data"));

        if (check.hash("Final").toBool())
            break;
    }
}

static void makeArbitraryStructureMetadata(Variant::Write &target)
{
    target.metadata("ArbitraryStructure").setBool(true);
    switch (target.getType()) {
    case Variant::Type::MetadataHash:
        for (auto child : target.toMetadataHashChild()) {
            makeArbitraryStructureMetadata(child.second);
        }
        break;
    case Variant::Type::MetadataFlags:
        for (auto child : target.toMetadataSingleFlag()) {
            makeArbitraryStructureMetadata(child.second);
        }
        break;
    default:
        break;
    }
}

static void makeArbitraryStructureMetadata(Variant::Write &&target)
{ return makeArbitraryStructureMetadata(target); }

static Read maybeEvaluateMetadata(const Read &metadata, const Read &data)
{
    if (metadata.metadata("ArbitraryStructure").toBool()) {
        if (metadata.currentPath().empty())
            return metadata;
        Root root(metadata);
        makeArbitraryStructureMetadata(root.write());
        return root.read();
    }

    std::vector<Root> mergeContents;

    evaluateMetadataOverlays(metadata, data, mergeContents);

    {
        const auto &templatePath = metadata.metadata("TemplatePath").toString();
        if (!templatePath.empty()) {
            auto path = PathElement::toMetadata(
                    PathElement::parse(templatePath, metadata.currentPath()));
            auto templateMetadata = metadata.getRoot().getPath(std::move(path));
            if (templateMetadata.exists()) {
                mergeContents.emplace_back(templateMetadata);
                evaluateMetadataOverlays(templateMetadata, data, mergeContents);
            }
        }
    }

    if (mergeContents.empty())
        return metadata;

    mergeContents.emplace(mergeContents.begin(), metadata);

    return Root::overlay(mergeContents.begin(), mergeContents.end());
}

Root evaluateMetadata(const Read &metadata, const Read &data)
{
    if (metadata.metadata("ArbitraryStructure").toBool()) {
        Root root(metadata);
        makeArbitraryStructureMetadata(root.write());
        return root;
    }

    std::vector<Root> mergeContents;
    mergeContents.emplace_back(metadata);

    evaluateMetadataOverlays(metadata, data, mergeContents);

    {
        const auto &templatePath = metadata.metadata("TemplatePath").toString();
        if (!templatePath.empty()) {
            auto path = PathElement::toMetadata(
                    PathElement::parse(templatePath, metadata.currentPath()));
            auto templateMetadata = metadata.getRoot().getPath(std::move(path));
            if (templateMetadata.exists()) {
                mergeContents.emplace_back(templateMetadata);
                evaluateMetadataOverlays(templateMetadata, data, mergeContents);
            }
        }
    }

    return Root::overlay(mergeContents.begin(), mergeContents.end());
}

Root evaluateMetadata(const Read &metadata)
{ return evaluateMetadata(metadata, Variant::Read::empty()); }

Root evaluateMetadataTree(const Read &metadata, const Read &data)
{
    const auto &fullPath = metadata.currentPath();
    if (fullPath.empty())
        return evaluateMetadata(metadata, data);

    std::vector<Read> dataContext;
    dataContext.emplace_back(data);
    for (std::size_t i = 0, max = fullPath.size(); i < max; ++i) {
        auto dataPath = dataContext.back().currentPath();
        if (dataPath.empty()) {
            dataContext.emplace_back();
            dataContext.back().detachFromRoot();
            continue;
        }

        dataPath.pop_back();
        dataContext.emplace_back(data.getRoot().getPath(std::move(dataPath)));
    }

    Root currentMetadata = evaluateMetadata(metadata.getRoot(), dataContext.back());
    dataContext.pop_back();

    for (const auto &pe : fullPath) {
        Q_ASSERT(!dataContext.empty());

        if (currentMetadata.read().metadata("ArbitraryStructure").toBool()) {
            if (!currentMetadata.read().getPath(pe).exists()) {
                Variant::Root root(currentMetadata.read().getType());
                root.write().metadata("ArbitraryStructure").setBoolean(true);
                return root;
            }
        }

        currentMetadata = evaluateMetadata(currentMetadata.read().getPath(pe), dataContext.back());
        dataContext.pop_back();
    }

    return currentMetadata;
}

Root evaluateMetadataTree(const Read &metadata)
{ return evaluateMetadataTree(metadata, Variant::Read::empty()); }


InputNode::InputNode(const Path &path) : path(path)
{ }

InputNode::InputNode(Path &&path) : path(std::move(path))
{ }

InputNode::InputNode(const InputNode &) = default;

InputNode &InputNode::operator=(const InputNode &) = default;

InputNode::InputNode(InputNode &&) = default;

InputNode &InputNode::operator=(InputNode &&) = default;

InputNode::~InputNode() = default;

Root InputNode::parse(std::vector<std::reference_wrapper<InputNode>> &&nodes,
                      const Read &metadata,
                      const Read &reference)
{
    class Node {
        std::unordered_map<PathElement, std::unique_ptr<Node>> children;
        InputNode *node;
    public:
        Node() : children(), node(nullptr)
        { }

        void assign(const Path &path, std::size_t index, InputNode *target)
        {
            if (index >= path.size()) {
                node = target;
                return;
            }
            auto &child = children[path[index]];
            if (!child)
                child.reset(new Node);
            child->assign(path, index + 1, target);
        }

        bool firstPass(Write &&target, const Read &metadata, const Read &reference)
        {
            if (node) {
                if (node->attemptWrite(target, metadata, reference))
                    node = nullptr;
            }
            for (auto c = children.begin(); c != children.end();) {
                auto childTarget = target.getPath(c->first);
                auto childMetadata =
                        maybeEvaluateMetadata(metadata.getPath(PathElement::toMetadata(c->first)),
                                              childTarget);
                auto childReference = reference.getPath(c->first);
                if (c->second->firstPass(std::move(childTarget), childMetadata, childReference)) {
                    ++c;
                    continue;
                }

                c = children.erase(c);
            }

            return node || !children.empty();
        }

        bool secondPass(Write &&target, const Read &metadata, const Read &reference)
        {
            if (node) {
                node->fallbackWrite(target, metadata, reference);
                node = nullptr;
            }

            for (auto c = children.begin(); c != children.end();) {
                auto childTarget = target.getPath(c->first);
                auto childMetadata =
                        maybeEvaluateMetadata(metadata.getPath(PathElement::toMetadata(c->first)),
                                              childTarget);
                auto childReference = reference.getPath(c->first);

                if (c->second->secondPass(std::move(childTarget), childMetadata, childReference)) {
                    ++c;
                    continue;
                }

                c = children.erase(c);
            }

            return node || !children.empty();
        }
    };
    Node parseRoot;
    for (const auto &add : nodes) {
        parseRoot.assign(add.get().getPath(), 0, &add.get());
    }

    Root result;
    parseRoot.firstPass(result.write(), evaluateMetadata(metadata, result), reference);
    parseRoot.secondPass(result.write(), evaluateMetadata(metadata, result), reference);
    return result;
}

InputNodeString::InputNodeString(const Path &path, const QString &value) : InputNode(path),
                                                                           value(value)
{ }

InputNodeString::InputNodeString(Path &&path, const QString &value) : InputNode(std::move(path)),
                                                                      value(value)
{ }

InputNodeString::InputNodeString(const InputNodeString &) = default;

InputNodeString &InputNodeString::operator=(const InputNodeString &) = default;

InputNodeString::InputNodeString(InputNodeString &&) = default;

InputNodeString &InputNodeString::operator=(InputNodeString &&) = default;

InputNodeString::~InputNodeString() = default;

bool InputNodeString::attemptWrite(Write &value, const Read &metadata, const Read &reference)
{ return fromInputString(value, this->value, metadata, reference, false); }

void InputNodeString::fallbackWrite(Write &value, const Read &metadata, const Read &reference)
{ fromInputString(value, this->value, metadata, reference, true); }

}
}
}
}