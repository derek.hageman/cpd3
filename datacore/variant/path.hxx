/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_PATH_HXX
#define CPD3DATACOREVARIANT_PATH_HXX

#include "core/first.hxx"

#include <string>
#include <cstdint>
#include <vector>
#include <type_traits>

#include <QString>
#include <QDebug>
#include <QDataStream>

#include "datacore/datacore.hxx"
#include "core/qtcompat.hxx"
#include "core/number.hxx"
#include "core/util.hxx"
#include "common.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

class TopLevel;

class NodeHash;

class NodeArray;

class NodeMatrix;

class NodeKeyframe;

class NodeMetadataBase;

class NodeMetadataFlags;

class NodeMetadataHash;

class PathElement;

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const PathElement &value);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, PathElement &value);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const PathElement &value);


/**
 * A single element in a path to lookup variants
 * and their children.
 */
class CPD3DATACORE_EXPORT PathElement final {
    friend class TopLevel;

    friend class NodeHash;

    friend class NodeArray;

    friend class NodeMatrix;

    friend class NodeKeyframe;

    friend class NodeMetadataBase;

    friend class NodeMetadataFlags;

    friend class NodeMetadataHash;

public:
    /**
     * The types of possible path elements.
     */
    enum class Type {
        Empty,

        AnyString,
        AnyMetadata,

        Hash,
        Array,
        Matrix,
        Keyframe,

        MetadataReal,
        MetadataInteger,
        MetadataBoolean,
        MetadataString,
        MetadataBytes,
        MetadataFlags,
        MetadataArray,
        MetadataMatrix,
        MetadataHash,
        MetadataKeyframe,

        MetadataHashChild,
        MetadataSingleFlag,
    };

    typedef std::string StringIndex;
    typedef std::string HashIndex;
    typedef std::string MetadataIndex;
    typedef HashIndex MetadataHashChildIndex;
    typedef Flag MetadataSingleFlagIndex;
    typedef std::size_t ArrayIndex;
    typedef std::vector<std::size_t> MatrixIndex;
    typedef double KeyframeIndex;

    typedef std::pair<std::string, std::string> Encoded;

private:
    Util::ExplicitUnion<StringIndex, HashIndex, MetadataIndex, MetadataHashChildIndex,
                        MetadataSingleFlagIndex, ArrayIndex, MatrixIndex, KeyframeIndex> data;
    Type type;

    template<typename T>
    inline T &ref()
    { return data.ref<T>(); }

    template<typename T>
    inline const T &ref() const
    { return data.ref<T>(); }

    void destructData();

    void constructFrom(const PathElement &other);

    void constructFrom(PathElement &&other);

    bool equal(const PathElement &other) const;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const PathElement &value);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, PathElement &value);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const PathElement &value);

    static void parsePath(const std::string &string, std::vector<PathElement> &path);

public:
    PathElement();

    ~PathElement();

    PathElement(const PathElement &other);

    PathElement &operator=(const PathElement &other);

    PathElement(PathElement &&other);

    PathElement &operator=(PathElement &&other);


    /**
     * Construct a path element.
     *
     * @param type      the type, which must be compatible with the value
     * @param value     the value of the path element
     */
    PathElement(Type type, const char *value);

    /**
     * Construct a path element.
     *
     * @param type      the type, which must be compatible with the value
     * @param value     the value of the path element
     */
    PathElement(Type type, const std::string &value);

    /**
     * Construct a path element.
     *
     * @param type      the type, which must be compatible with the value
     * @param value     the value of the path element
     */
    PathElement(Type type, std::string &&value);

    /**
     * Construct a path element.
     *
     * @param type      the type, which must be compatible with the value
     * @param value     the value of the path element
     */
    PathElement(Type type, const QString &value);

    /**
     * Construct a path element.
     *
     * @param type      the type, which must be compatible with the value
     * @param value     the value of the path element
     */
    PathElement(Type type, QString &&value);

    /**
     * Construct a path element.
     *
     * @param type      the type, which must be compatible with the value
     * @param value     the value of the path element
     */
    PathElement(Type type, const MatrixIndex &value);

    /**
     * Construct a path element.
     *
     * @param type      the type, which must be compatible with the value
     * @param value     the value of the path element
     */
    PathElement(Type type, MatrixIndex &&value);

    /**
     * Construct a path element.
     *
     * @param type      the type, which must be compatible with the value
     * @param value     the value of the path element
     */
    PathElement(Type type, double value);

    /**
     * Construct a path element.
     *
     * @param type      the type, which must be compatible with the value
     * @param value     the value of the path element
     */
    PathElement(Type type, std::size_t value);

    /**
     * Construct a path element.
     *
     * @param type      the type, which must be compatible with the value
     * @param value     the value of the path element
     */
    template<class Integer, class = typename std::enable_if<((std::is_same<Integer, int>::value) &&
            !std::is_same<std::size_t, Integer>::value)>::type>
    inline PathElement(Type type, Integer value) : PathElement(type,
                                                               static_cast<std::size_t>(value))
    { Q_ASSERT(value >= 0); }


    /**
     * Get the string representation of the element.
     *
     * @param escape    true to perform escaping
     * @param explicitHash use explicit hash typing, instead of generic any string
     * @return          the string representation of the element
     */
    std::string toString(bool escape = true, bool explicitHash = false) const;

    /**
     * Parse a string into a path element.  This does NOT
     * reverse escaping.
     *
     * @param string    the string representation of the element
     * @return          the parsed path element
     */
    static PathElement fromString(const std::string &string);

    /**
     * Parse a string into a path element.  This does NOT
     * reverse escaping.
     *
     * @param string    the string representation of the element
     * @return          the parsed path element
     */
    static PathElement fromString(std::string &&string);

    /**
     * Parse a full path into a sequence of path elements.
     *
     * @param string    the string to parse
     * @param existing  the existing path for relative lookups
     * @return          a parsed path
     */
    static std::vector<PathElement> parse(const std::string &string,
                                          std::vector<PathElement> existing = std::vector<
                                                  PathElement>());

    /**
     * Parse a full path into a sequence of path elements.
     *
     * @param string    the string to parse
     * @param existing  the existing path for relative lookups
     * @return          a parsed path
     */
    static std::vector<PathElement> parse(std::string &&string,
                                          std::vector<PathElement> existing = std::vector<
                                                  PathElement>());

    /**
     * Get the path element's array index, if it is an array.
     *
     * @return the array index or -1 if it is not an array
     */
    std::size_t toArrayIndex() const;

    /**
     * Encode the element as a pair of strings.
     *
     * @return  the encoded path
     */
    Encoded encode() const;

    /**
     * Decode a path element from an encoded pair.
     *
     * @param encoded   the encoded pair
     * @return          a path element
     */
    static PathElement decode(const Encoded &encoded);

    /**
     * Decode a path element from an encoded pair.
     *
     * @param encoded   the encoded pair
     * @return          a path element
     */
    static PathElement decode(Encoded &&encoded);


    /**
     * Convert a path element into an accessor for metadata.
     *
     * @param data      the data path element
     * @return          the path element to access metadata
     */
    static PathElement toMetadata(const PathElement &data);

    /**
     * Convert a path element into an accessor for metadata.
     *
     * @param data      the data path element
     * @return          the path element to access metadata
     */
    static PathElement toMetadata(PathElement &&data);

    /**
     * Convert a sequence of path elements into a metadata access
     * sequence.
     *
     * @param data      the data path
     * @return          the metadata path
     */
    static std::vector<PathElement> toMetadata(const std::vector<PathElement> &data);

    /**
     * Convert a sequence of path elements into a metadata access
     * sequence.
     *
     * @param data      the data path
     * @return          the metadata path
     */
    static std::vector<PathElement> toMetadata(std::vector<PathElement> &&data);

    /**
     * Swap two path elements.
     *
     * @param a     the path element name
     * @param b     the path element name
     */
    static void swap(PathElement &a, PathElement &b);

    /**
     * Get the hash of the element.
     *
     * @return      a hash value
     */
    std::size_t hash() const;

    inline bool operator==(const PathElement &other) const
    { return equal(other); }

    inline bool operator!=(const PathElement &other) const
    { return !equal(other); }

    /**
     * A generic ordering function.
     */
    struct CPD3DATACORE_EXPORT OrderLogical {
        bool operator()(const PathElement &a, const PathElement &b) const;
    };

    friend struct OrderLogical;

    using OrderDisplay = OrderLogical;
};

CPD3DATACORE_EXPORT inline void swap(PathElement &lhs, PathElement &rhs)
{ PathElement::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(PathElement &lhs, PathElement &rhs)
{ PathElement::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline uint qHash(const PathElement &key)
{ return static_cast<uint>(key.hash()); }

using Path = std::vector<PathElement>;

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const Path &value);

}
}
}

namespace std {

template<>
struct hash<CPD3::Data::Variant::PathElement> {
    inline std::size_t operator()(const CPD3::Data::Variant::PathElement &e) const
    { return e.hash(); }
};

template<>
struct hash<CPD3::Data::Variant::Path> {
    inline std::size_t operator()(const CPD3::Data::Variant::Path &p) const
    {
        std::size_t result = 0;
        for (const auto &add : p) {
            result = CPD3::INTEGER::mix(result, hash<CPD3::Data::Variant::PathElement>()(add));
        }
        return result;
    }
};

}


Q_DECLARE_METATYPE(CPD3::Data::Variant::PathElement);

Q_DECLARE_METATYPE(CPD3::Data::Variant::Path);


#ifdef CPD3_QTEST
namespace CPD3 {
namespace Data {
namespace Variant {

inline char *toString(const CPD3::Data::Variant::PathElement &element)
{
    QByteArray ba = "Variant::PathEelement(";
    ba += element.toString(true, true).data();
    ba += ")";
    return ::qstrdup(ba.data());
}

inline char *toString(const CPD3::Data::Variant::Path &path)
{
    QByteArray ba = "Variant::Path(";
    for (const auto &e : path) {
        ba += "/";
        ba += e.toString().data();
    }
    ba += ")";
    return ::qstrdup(ba.data());
}

}
}
}
#endif


#endif //CPD3DATACOREVARIANT_PATH_HXX
