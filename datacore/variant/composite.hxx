/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_COMPOSITE_HXX
#define CPD3DATACOREVARIANT_COMPOSITE_HXX

#include "core/first.hxx"

#include <QString>
#include <QStringList>

#include "datacore/datacore.hxx"
#include "handle.hxx"
#include "root.hxx"
#include "core/textsubstitution.hxx"
#include "core/timeutils.hxx"
#include "core/number.hxx"

class QVariant;

namespace CPD3 {
namespace Data {
namespace Variant {
namespace Composite {

/**
 * Apply display formatting to a value.
 *
 * @param value     the value
 * @param elements  the format context elements
 * @return          the formatted value
 */
CPD3DATACORE_EXPORT QString applyFormat(const Read &value,
                                        const QStringList &elements = QStringList());

/**
 * A text substitution stack that implements replacement for general data
 * value keys.
 */
class CPD3DATACORE_EXPORT SubstitutionStack : public CPD3::TextSubstitutionStack {
public:
    SubstitutionStack();

    virtual ~SubstitutionStack();

    SubstitutionStack(const SubstitutionStack &);

    SubstitutionStack &operator=(const SubstitutionStack &);

    SubstitutionStack(SubstitutionStack &&);

    SubstitutionStack &operator=(SubstitutionStack &&);

    /**
     * Set a variant value in the substitutions.
     *
     * @param key   the key
     * @param value the variant value
     */
    void setVariant(const QString &key, const Read &value);
};

/**
 * Convert a value to a time interval.
 *
 * @param value the input value
 * @param unit  the output unit if any
 * @param count the output count if any
 * @param align the output alignment if any
 * @return      true if all conversions where ok
 */
CPD3DATACORE_EXPORT bool toTimeInterval(const Read &value,
                                        Time::LogicalTimeUnit *unit,
                                        int *count = nullptr,
                                        bool *align = nullptr);

/**
 * Convert a value to a time interval.
 *
 * @param value the input value
 * @param count the output count if any
 * @param align the output alignment if any
 * @return      the output units
 */
CPD3DATACORE_EXPORT Time::LogicalTimeUnit toTimeInterval(const Read &value,
                                                         int *count = nullptr,
                                                         bool *align = nullptr);

/**
 * Convert a value to a time interval and then apply it as an offset.
 *
 * @param value         the input value
 * @param origin        the input time origin
 * @param forward       true if offsetting forward in time
 * @param defaultUnit   the default time units
 * @param defaultCount  the default count
 * @param defaultAlign  the default alignment
 * @return              the origin with the offset applied
 */
CPD3DATACORE_EXPORT double offsetTimeInterval(const Read &value,
                                              double origin,
                                              bool forward = true,
                                              Time::LogicalTimeUnit defaultUnit = Time::Second,
                                              int defaultCount = 0,
                                              bool defaultAlign = false);

/**
 * Set a value with a time unit.
 *
 * @param value the value to set
 * @param unit  the unit to set
 */
CPD3DATACORE_EXPORT void fromTimeInterval(Write &value, Time::LogicalTimeUnit unit);

CPD3DATACORE_EXPORT void fromTimeInterval(Write &&value, Time::LogicalTimeUnit unit);

/**
 * Set a value with a time interval.
 *
 * @param value the value to set
 * @param unit  the unit to set
 * @param count the number of units
 */
CPD3DATACORE_EXPORT void fromTimeInterval(Write &value, Time::LogicalTimeUnit unit, int count);

CPD3DATACORE_EXPORT void fromTimeInterval(Write &&value, Time::LogicalTimeUnit unit, int count);

/**
 * Set a value with a time interval.
 *
 * @param value the value to set
 * @param unit  the unit to set
 * @param count the number of units
 * @param align the alignment
 */
CPD3DATACORE_EXPORT void fromTimeInterval(Write &value,
                                          Time::LogicalTimeUnit unit,
                                          int count,
                                          bool align);

CPD3DATACORE_EXPORT void fromTimeInterval(Write &&value,
                                          Time::LogicalTimeUnit unit,
                                          int count,
                                          bool align);


/**
 * Convert a value to a calibration.
 *
 * @param value the input value
 * @return      a calibration
 */
CPD3DATACORE_EXPORT Calibration toCalibration(const Read &value);

/**
 * Set a value with a calibration.
 *
 * @param value         the value to set
 * @param calibration   the calibration
 * @param includeRoot   always include the requested root
 */
CPD3DATACORE_EXPORT void fromCalibration(Write &value,
                                         const Calibration &calibration,
                                         bool includeRoot = false);

CPD3DATACORE_EXPORT void fromCalibration(Write &&value,
                                         const Calibration &calibration,
                                         bool includeRoot = false);

/**
 * Set a value based on a variant input.
 *
 * @param value     the output value
 * @param variant   the input variant
 */
CPD3DATACORE_EXPORT void fromVariant(Write &value, const QVariant &variant);

CPD3DATACORE_EXPORT void fromVariant(Write &&value, const QVariant &variant);

/**
 * Apply a transform to an input and return the output.  This is normally
 * used to apply a real number change to all children as required.
 *
 * @tparam Functor  the transformer type of void(const Read &input, Write &output)
 * @param input     the input value
 * @param f         the transformer functor
 * @return          the transformed value
 */
template<typename Functor>
Root applyTransform(const Read &input, Functor f)
{
    auto type = input.getType();
    switch (type) {
    case Type::Array: {
        Root result(Type::Array);
        auto write = result.write();
        for (std::size_t i = 0, max = input.toArray().size(); i < max; i++) {
            auto output = write.array(i);
            f(input.array(i), output);
        }
        return result;
    }
    case Type::Matrix: {
        Root result(Type::Matrix);
        auto write = result.write();
        for (auto in : input.toMatrix()) {
            auto output = write.matrix(in.first);
            f(in.second, output);
        }
        return result;
    }
    default:
        break;
    }

    Root result(type);
    {
        auto output = result.write();
        f(input, output);
    }
    return result;
}

/**
 * Apply a transform to a value.  This alters the value in place.  his is normally
 * used to apply a real number change to all children as required.
 *
 * @tparam Functor  the transformer type of void(Write &output)
 * @param data      the data to transform
 * @param f         the transformer functor
 */
template<typename Functor>
void applyInplace(Write &data, Functor f)
{
    auto type = data.getType();
    switch (type) {
    case Type::Array:
        for (auto v : data.toArray()) {
            f(v);
        }
        return;
    case Type::Matrix:
        for (auto v : data.toMatrix()) {
            f(v.second);
        }
        return;
    default:
        break;
    }

    f(data);
}

template<typename Functor>
void applyInplace(Write &&data, Functor f)
{ return applyInplace(data, std::forward<Functor>(f)); }

/**
 * Invalidate a value.  For types with an undefined state, this
 * sets it to that value.  For others it sets the type to empty.
 *
 * @param value     the value to invalidate
 */
CPD3DATACORE_EXPORT void invalidate(Write &value);

CPD3DATACORE_EXPORT void invalidate(Write &&value);

/**
 * Get a path element that represents a unique key under a parent value.
 *
 * @param parent    the parent value
 * @return          a path element to a key that does not currently exist
 */
CPD3DATACORE_EXPORT PathElement uniqueKey(const Read &parent,
                                          const Read &metadata = Read::empty(),
                                          bool *ok = nullptr);


/**
 * Test if the specified flag is a contamination flag.
 *
 * @param flag      the flag to check
 * @return          true if the flag is a contamination flag
 */
CPD3DATACORE_EXPORT bool isContaminationFlag(const Variant::Flag &flag);

/**
 * Test if any of the specified flags are contamination flags.
 *
 * @param flags     the flags to check
 * @return          true if the flags contain a contamination flag
 */
CPD3DATACORE_EXPORT bool isContaminationFlag(const Variant::Flags &flags);

/**
 * Test if a value is logically "defined".  That is not equal
 * to the missing value code.
 *
 * @param value             the value to check
 * @param convertStrings    parse strings and convert them
 * @return                  true if the value is defined
 */
CPD3DATACORE_EXPORT bool isDefined(const Read &value);

/**
 * Convert a value to a number, using "normal" casting mechanics.  For
 * example, this will convert integers.
 *
 * @param value             the input value
 * @param convertStrings    parse strings and convert them
 * @return                  the number value of it
 */
CPD3DATACORE_EXPORT double toNumber(const Read &value, bool convertStrings = false);

/**
 * Convert a value to an integer, using "normal" casting mechanics.  For
 * example, this will convert reals.
 *
 * @param value     the input value
 * @return          the number value of it
 */
CPD3DATACORE_EXPORT std::int_fast64_t toInteger(const Read &value, bool convertStrings = false);

}
}
}
}

#endif //CPD3DATACOREVARIANT_COMPOSITE_HXX
