/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_HANDLE_HXX
#define CPD3DATACOREVARIANT_HANDLE_HXX

#include "core/first.hxx"

#include <memory>
#include <string>
#include <unordered_map>
#include <type_traits>
#include <QString>
#include <QDebug>
#include <QDataStream>

#include "datacore/datacore.hxx"
#include "core/qtcompat.hxx"
#include "path.hxx"
#include "common.hxx"
#include "containers.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

class Root;

class TopLevel;

class Read;

class Write;

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Read &h);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, Read &h);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const Read &h);

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Write &h);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, Write &h);


/**
 * A read handle in a variant hierarchy.  This provides
 * read only access to the values in the variant.
 */
class CPD3DATACORE_EXPORT Read {
    std::shared_ptr<TopLevel> top;
    Path path;

    friend class Root;

    friend class Write;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Read &h);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, Read &h);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const Read &h);

    Read(const Read &other, const PathElement &add);

    Read(const Read &other, PathElement &&add);

    bool equal(const Read &other) const;

    friend class Container::ReadHash;

    friend class Container::WriteHash;

    friend class Container::WriteFlags;

    friend class Container::ReadArray;

    friend class Container::WriteArray;

    friend class Container::ReadMatrix;

    friend class Container::WriteMatrix;

    friend class Container::ReadKeyframe;

    friend class Container::WriteKeyframe;

    friend class Container::ReadMetadata;

    friend class Container::WriteMetadataReal;

    friend class Container::WriteMetadataInteger;

    friend class Container::WriteMetadataBoolean;

    friend class Container::WriteMetadataString;

    friend class Container::WriteMetadataBytes;

    friend class Container::WriteMetadataFlags;

    friend class Container::WriteMetadataArray;

    friend class Container::WriteMetadataMatrix;

    friend class Container::WriteMetadataHash;

    friend class Container::WriteMetadataKeyframe;

    friend class Container::ReadMetadataSingleFlag;

    friend class Container::WriteMetadataSingleFlag;

    friend class Container::ReadMetadataHashChild;

    friend class Container::WriteMetadataHashChild;

    friend class Container::ReadGeneric;

public:
    /**
     * Create an empty handle.  This cannot be used until
     * it is either assigned to a root, or detached with
     * detachFromRoot().
     */
    Read();

    ~Read();

    Read(const Read &);

    Read &operator=(const Read &);

    Read(Read &&);

    Read &operator=(Read &&);


    /**
     * Get a static instance of an always empty read handle.
     * Most commonly used as a default argument.
     *
     * @return an empty read handle
     */
    static const Read &empty();


    /**
     * Create a handle at a variant root.
     *
     * @param root the root to read from
     */
    explicit Read(Root &&root);

    /**
     * Create a handle at a variant root.
     *
     * @param root the root to read from
     */
    Read(const Root &root);

    /**
     * Create a handle at a path within a variant root.
     *
     * @param root  the variant root
     * @param path  the path of the handle
     */
    Read(const Root &root, const Path &path);

    /**
     * Create a handle at a path within a variant root.
     *
     * @param root  the variant root
     * @param path  the path of the handle
     */
    Read(const Root &root, Path &&path);


    /**
     * Swap two variant handles.
     *
     * @param a     the first variant handle
     * @param b     the second variant handle
     */
    static void swap(Read &a, Read &b);

    /**
     * A hash function for values.
     */
    struct CPD3DATACORE_EXPORT ValueHash {
        std::size_t operator()(const Read &value) const;
    };

    friend struct ValueHash;

    inline bool operator==(const Read &other) const
    { return equal(other); }

    inline bool operator!=(const Read &other) const
    { return !equal(other); }


    /**
     * Get the current path within the root the handle represents.
     *
     * @return  the handle path
     */
    inline const Path &currentPath() const
    { return path; }


    /**
     * Get a read reference located at the variant root.
     *
     * @return  a root handle
     */
    Read getRoot() const;

    /**
     * Lookup a path starting from the location of the handle
     * and return a read reference.
     *
     * @param path  the path to lookup
     * @return      a handle located at the path
     * @see PathElement::parse(const std::string &, std::vector<PathElement>)
     */
    Read getPath(const std::string &path) const;

    /** @see getPath(const std::string &) const */
    Read getPath(std::string &&path) const;

    /** @see getPath(const std::string &) const */
    Read getPath(const char *path) const;

    /** @see getPath(const std::string &) const */
    Read getPath(const QString &path) const;

    /** @see getPath(const std::string &) const */
    inline Read operator[](const std::string &path) const
    { return getPath(path); }

    /** @see getPath(const std::string &) const */
    inline Read operator[](std::string &&path) const
    { return getPath(std::move(path)); }

    /** @see getPath(const std::string &) const */
    inline Read operator[](const char *path) const
    { return getPath(path); }

    /** @see getPath(const std::string &) const */
    inline Read operator[](const QString &path) const
    { return getPath(path); }


    /**
     * Get a handle by adding an exact path element to
     * the current one.
     *
     * @param add   the path element to add
     * @return      a handle with the added path element
     */
    inline Read getPath(const PathElement &add) const
    { return Read(*this, add); }

    /** @see getPath(const PathElement &) const */
    inline Read getPath(PathElement &&add) const
    { return Read(*this, std::move(add)); }

    /**
     * Get a handle by adding the specified path elements
     * to the current one.
     *
     * @param add   the path elements to add
     * @return      a handle with the added path element
     */
    Read getPath(const Path &add) const;

    /** @see getPath(const Path &) const */
    Read getPath(Path &&add) const;


    /**
     * Lookup a child by any string accessible type.
     *
     * @param index the child index
     * @return      a handle for the child
     */
    inline Read child(const std::string &index) const
    { return Read(*this, {PathElement::Type::AnyString, index}); }

    /** @see child(const std::string &) const */
    inline Read child(std::string &&index) const
    { return Read(*this, {PathElement::Type::AnyString, std::move(index)}); }

    /** @see child(const std::string &) const */
    inline Read child(const char *index) const
    { return Read(*this, {PathElement::Type::AnyString, index}); }

    /** @see child(const std::string &) const */
    inline Read child(const QString &index) const
    { return Read(*this, {PathElement::Type::AnyString, index}); }


    /**
     * Lookup a child for a hash.
     *
     * @param index the child index
     * @return      a handle for the child
     */
    inline Read hash(const std::string &index) const
    { return Read(*this, {PathElement::Type::Hash, index}); }

    /** @see hash(const std::string &) const */
    inline Read hash(std::string &&index) const
    { return Read(*this, {PathElement::Type::Hash, std::move(index)}); }

    /** @see hash(const std::string &) const */
    inline Read hash(const char *index) const
    { return Read(*this, {PathElement::Type::Hash, index}); }

    /** @see hash(const std::string &) const */
    inline Read hash(const QString &index) const
    { return Read(*this, {PathElement::Type::Hash, index}); }

    /**
     * Lookup a child for an array index.
     *
     * @param index the index in the array
     * @return      a handle for the child
     */
    inline Read array(std::size_t index) const
    { return Read(*this, {PathElement::Type::Array, index}); }

    /**
     * Lookup a child for a matrix index.
     *
     * @param index the index in the matrix
     * @return      a handle for the child
     */
    inline Read matrix(const PathElement::MatrixIndex &index) const
    { return Read(*this, {PathElement::Type::Matrix, index}); }

    /** @see matrix(const PathElement::MatrixIndex &) */
    inline Read matrix(PathElement::MatrixIndex &&index) const
    { return Read(*this, {PathElement::Type::Matrix, std::move(index)}); }

    /**
     * Lookup a child for a keyframe index.
     *
     * @param index the keyframe index value
     * @return      a handle for the child
     */
    inline Read keyframe(double index) const
    {
        Q_ASSERT(FP::defined(index));
        return Read(*this, {PathElement::Type::Keyframe, index});
    }


    /**
     * Lookup a child for any type of metadata.
     *
     * @param index the metadata identifier
     * @return      a handle for the metadata
     */
    inline Read metadata(const std::string &index) const
    { return Read(*this, {PathElement::Type::AnyMetadata, index}); }

    /** @see metadata(const std::string &) const */
    inline Read metadata(std::string &&index) const
    { return Read(*this, {PathElement::Type::AnyMetadata, std::move(index)}); }

    /** @see metadata(const std::string &) const */
    inline Read metadata(const char *index) const
    { return Read(*this, {PathElement::Type::AnyMetadata, index}); }

    /** @see metadata(const std::string &) const */
    inline Read metadata(const QString &index) const
    { return Read(*this, {PathElement::Type::AnyMetadata, index}); }


    /**
     * Lookup a child for a real number metadata.
     *
     * @param index the metadata identifier
     * @return      a handle for the metadata
     */
    inline Read metadataReal(const std::string &index) const
    { return Read(*this, {PathElement::Type::MetadataReal, index}); }

    /** @see metadataReal(const std::string &) const */
    inline Read metadataReal(std::string &&index) const
    { return Read(*this, {PathElement::Type::MetadataReal, std::move(index)}); }

    /** @see metadataReal(const std::string &) const */
    inline Read metadataReal(const char *index) const
    { return Read(*this, {PathElement::Type::MetadataReal, index}); }

    /** @see metadataReal(const std::string &) const */
    inline Read metadataReal(const QString &index) const
    { return Read(*this, {PathElement::Type::MetadataReal, index}); }

    /**
     * Lookup a child for a integer metadata.
     *
     * @param index the metadata identifier
     * @return      a handle for the metadata
     */
    inline Read metadataInteger(const std::string &index) const
    { return Read(*this, {PathElement::Type::MetadataInteger, index}); }

    /** @see metadataInteger(const std::string &) const */
    inline Read metadataInteger(std::string &&index) const
    { return Read(*this, {PathElement::Type::MetadataInteger, std::move(index)}); }

    /** @see metadataInteger(const std::string &) const */
    inline Read metadataInteger(const char *index) const
    { return Read(*this, {PathElement::Type::MetadataInteger, index}); }

    /** @see metadataInteger(const std::string &) const */
    inline Read metadataInteger(const QString &index) const
    { return Read(*this, {PathElement::Type::MetadataInteger, index}); }

    /**
     * Lookup a child for a boolean value metadata.
     *
     * @param index the metadata identifier
     * @return      a handle for the metadata
     */
    inline Read metadataBoolean(const std::string &index) const
    { return Read(*this, {PathElement::Type::MetadataBoolean, index}); }

    /** @see metadataBoolean(const std::string &) const */
    inline Read metadataBoolean(std::string &&index) const
    { return Read(*this, {PathElement::Type::MetadataBoolean, std::move(index)}); }

    /** @see metadataBoolean(const std::string &) const */
    inline Read metadataBoolean(const char *index) const
    { return Read(*this, {PathElement::Type::MetadataBoolean, index}); }

    /** @see metadataBoolean(const std::string &) const */
    inline Read metadataBoolean(const QString &index) const
    { return Read(*this, {PathElement::Type::MetadataBoolean, index}); }

    /**
     * Lookup a child for a string metadata.
     *
     * @param index the metadata identifier
     * @return      a handle for the metadata
     */
    inline Read metadataString(const std::string &index) const
    { return Read(*this, {PathElement::Type::MetadataString, index}); }

    /** @see metadataString(const std::string &) const */
    inline Read metadataString(std::string &&index) const
    { return Read(*this, {PathElement::Type::MetadataString, std::move(index)}); }

    /** @see metadataString(const std::string &) const */
    inline Read metadataString(const char *index) const
    { return Read(*this, {PathElement::Type::MetadataString, index}); }

    /** @see metadataString(const std::string &) const */
    inline Read metadataString(const QString &index) const
    { return Read(*this, {PathElement::Type::MetadataString, index}); }

    /**
     * Lookup a child for a byte value metadata.
     *
     * @param index the metadata identifier
     * @return      a handle for the metadata
     */
    inline Read metadataBytes(const std::string &index) const
    { return Read(*this, {PathElement::Type::MetadataBytes, index}); }

    /** @see metadataBytes(const std::string &) const */
    inline Read metadataBytes(std::string &&index) const
    { return Read(*this, {PathElement::Type::MetadataBytes, std::move(index)}); }

    /** @see metadataBytes(const std::string &) const */
    inline Read metadataBytes(const char *index) const
    { return Read(*this, {PathElement::Type::MetadataBytes, index}); }

    /** @see metadataBytes(const std::string &) const */
    inline Read metadataBytes(const QString &index) const
    { return Read(*this, {PathElement::Type::MetadataBytes, index}); }

    /**
     * Lookup a child for a flags metadata.
     *
     * @param index the metadata identifier
     * @return      a handle for the metadata
     */
    inline Read metadataFlags(const std::string &index) const
    { return Read(*this, {PathElement::Type::MetadataFlags, index}); }

    /** @see metadataFlags(const std::string &) const */
    inline Read metadataFlags(std::string &&index) const
    { return Read(*this, {PathElement::Type::MetadataFlags, std::move(index)}); }

    /** @see metadataFlags(const std::string &) const */
    inline Read metadataFlags(const char *index) const
    { return Read(*this, {PathElement::Type::MetadataFlags, index}); }

    /** @see metadataFlags(const std::string &) const */
    inline Read metadataFlags(const QString &index) const
    { return Read(*this, {PathElement::Type::MetadataFlags, index}); }

    /**
     * Lookup a child for array metadata.
     *
     * @param index the metadata identifier
     * @return      a handle for the metadata
     */
    inline Read metadataArray(const std::string &index) const
    { return Read(*this, {PathElement::Type::MetadataArray, index}); }

    /** @see metadataArray(const std::string &) const */
    inline Read metadataArray(std::string &&index) const
    { return Read(*this, {PathElement::Type::MetadataArray, std::move(index)}); }

    /** @see metadataArray(const std::string &) const */
    inline Read metadataArray(const char *index) const
    { return Read(*this, {PathElement::Type::MetadataArray, index}); }

    /** @see metadataArray(const std::string &) const */
    inline Read metadataArray(const QString &index) const
    { return Read(*this, {PathElement::Type::MetadataArray, index}); }

    /**
     * Lookup a child for matrix metadata.
     *
     * @param index the metadata identifier
     * @return      a handle for the metadata
     */
    inline Read metadataMatrix(const std::string &index) const
    { return Read(*this, {PathElement::Type::MetadataMatrix, index}); }

    /** @see metadataMatrix(const std::string &) const */
    inline Read metadataMatrix(std::string &&index) const
    { return Read(*this, {PathElement::Type::MetadataMatrix, std::move(index)}); }

    /** @see metadataMatrix(const std::string &) const */
    inline Read metadataMatrix(const char *index) const
    { return Read(*this, {PathElement::Type::MetadataMatrix, index}); }

    /** @see metadataMatrix(const std::string &) const */
    inline Read metadataMatrix(const QString &index) const
    { return Read(*this, {PathElement::Type::MetadataMatrix, index}); }

    /**
     * Lookup a child for hash map metadata.
     *
     * @param index the metadata identifier
     * @return      a handle for the metadata
     */
    inline Read metadataHash(const std::string &index) const
    { return Read(*this, {PathElement::Type::MetadataHash, index}); }

    /** @see metadataHash(const std::string &) const */
    inline Read metadataHash(std::string &&index) const
    { return Read(*this, {PathElement::Type::MetadataHash, std::move(index)}); }

    /** @see metadataHash(const std::string &) const */
    inline Read metadataHash(const char *index) const
    { return Read(*this, {PathElement::Type::MetadataHash, index}); }

    /** @see metadataHash(const std::string &) const */
    inline Read metadataHash(const QString &index) const
    { return Read(*this, {PathElement::Type::MetadataHash, index}); }

    /**
     * Lookup a child for keyframe map metadata.
     *
     * @param index the metadata identifier
     * @return      a handle for the metadata
     */
    inline Read metadataKeyframe(const std::string &index) const
    { return Read(*this, {PathElement::Type::MetadataKeyframe, index}); }

    /** @see metadataKeyframe(const std::string &) const */
    inline Read metadataKeyframe(std::string &&index) const
    { return Read(*this, {PathElement::Type::MetadataKeyframe, std::move(index)}); }

    /** @see metadataKeyframe(const std::string &) const */
    inline Read metadataKeyframe(const char *index) const
    { return Read(*this, {PathElement::Type::MetadataKeyframe, index}); }

    /** @see metadataKeyframe(const std::string &) const */
    inline Read metadataKeyframe(const QString &index) const
    { return Read(*this, {PathElement::Type::MetadataKeyframe, index}); }

    /**
     * Lookup a child referencing metadata about a single hash key.
     *
     * @param index the metadata identifier
     * @return      a handle for the metadata
     */
    inline Read metadataHashChild(const std::string &index) const
    { return Read(*this, {PathElement::Type::MetadataHashChild, index}); }

    /** @see metadataHashChild(const std::string &) const */
    inline Read metadataHashChild(std::string &&index) const
    { return Read(*this, {PathElement::Type::MetadataHashChild, std::move(index)}); }

    /** @see metadataHashChild(const std::string &) const */
    inline Read metadataHashChild(const char *index) const
    { return Read(*this, {PathElement::Type::MetadataHashChild, index}); }

    /** @see metadataHashChild(const std::string &) const */
    inline Read metadataHashChild(const QString &index) const
    { return Read(*this, {PathElement::Type::MetadataHashChild, index}); }

    /**
     * Lookup a child referencing metadata about a single flag.
     *
     * @param index the metadata identifier
     * @return      a handle for the metadata
     */
    inline Read metadataSingleFlag(const std::string &index) const
    { return Read(*this, {PathElement::Type::MetadataSingleFlag, index}); }

    /** @see metadataSingleFlag(const std::string &) const */
    inline Read metadataSingleFlag(std::string &&index) const
    { return Read(*this, {PathElement::Type::MetadataSingleFlag, std::move(index)}); }

    /** @see metadataSingleFlag(const std::string &) const */
    inline Read metadataSingleFlag(const char *index) const
    { return Read(*this, {PathElement::Type::MetadataSingleFlag, index}); }

    /** @see metadataSingleFlag(const std::string &) const */
    inline Read metadataSingleFlag(const QString &index) const
    { return Read(*this, {PathElement::Type::MetadataSingleFlag, index}); }


    /**
     * Detach the handle from the root.  This makes the handle
     * the only reference to a new root with the value of
     * the handle at the old position.  The old root can then
     * change without affecting the contents of the handle.
     */
    void detachFromRoot();

    /**
     * Get a description of the handle's value.
     *
     * @parma includePath   include the path the handle is located at
     * @return              a description of the value
     */
    std::string describe(bool includePath = true) const;


    /**
     * Get the type contained at the handle's location.
     *
     * @return  the handle's value type
     */
    Type getType() const;

    /**
     * Test if the value at the handle exists and is not explicitly
     * empty.
     *
     * @return  true if the handle's value exists
     */
    bool exists() const;

    /**
     * Test if the value at the handle exists and is a metadata
     * type.
     *
     * @return  true if the handle is metadata
     */
    bool isMetadata() const;

    /**
     * Get a hash access to the handle.
     *
     * @return  a hash container access
     */
    inline Container::ReadHash toHash() const
    { return Container::ReadHash(*this); }

    /** @see toHash() const */
    inline Container::ReadHash toConstHash() const
    { return toHash(); }

    /**
     * Get an array access to the handle.
     *
     * @return  am array container access
     */
    inline Container::ReadArray toArray() const
    { return Container::ReadArray(*this); }

    /** @see toArray() const */
    inline Container::ReadArray toConstArray() const
    { return toArray(); }

    /**
     * Get an matrix access to the handle.
     *
     * @return  am matrix container access
     */
    inline Container::ReadMatrix toMatrix() const
    { return Container::ReadMatrix(*this); }

    /** @see toMatrix() const */
    inline Container::ReadMatrix toConstMatrix() const
    { return toMatrix(); }

    /**
     * Get a keyframe access to the handle.
     *
     * @return  a keyframe container access
     */
    inline Container::ReadKeyframe toKeyframe() const
    { return Container::ReadKeyframe(*this); }

    /** @see toKeyframe() const */
    inline Container::ReadKeyframe toConstKeyframe() const
    { return toKeyframe(); }

    /**
     * Get a generic metadata access to the handle.
     *
     * @return  a metadata container access
     */
    inline Container::ReadMetadata toMetadata() const
    { return Container::ReadMetadata(*this); }

    /**
     * Get a generic metadata access to the handle that
     * uses the specialized type if the value already exists.
     *
     * @return  a metadata container access
     */
    Container::ReadMetadata toMetadataSpecialized() const;

    /**
     * Get a real metadata access to the handle.
     *
     * @return  a real metadata container access
     */
    inline Container::ReadMetadataReal toMetadataReal() const
    { return Container::ReadMetadataReal(*this); }

    /** @see toMetadataReal() const */
    inline Container::ReadMetadataReal toConstMetadataReal() const
    { return toMetadataReal(); }

    /**
     * Get a integer metadata access to the handle.
     *
     * @return  a integer metadata container access
     */
    inline Container::ReadMetadataInteger toMetadataInteger() const
    { return Container::ReadMetadataInteger(*this); }

    /** @see toMetadataInteger() const */
    inline Container::ReadMetadataInteger toConstMetadataInteger() const
    { return toMetadataInteger(); }

    /**
     * Get a boolean metadata access to the handle.
     *
     * @return  a boolean metadata container access
     */
    inline Container::ReadMetadataBoolean toMetadataBoolean() const
    { return Container::ReadMetadataBoolean(*this); }

    /** @see toMetadataBoolean() const */
    inline Container::ReadMetadataBoolean toConstMetadataBoolean() const
    { return toMetadataBoolean(); }

    /**
     * Get a string metadata access to the handle.
     *
     * @return  a string metadata container access
     */
    inline Container::ReadMetadataString toMetadataString() const
    { return Container::ReadMetadataString(*this); }

    /** @see toMetadataString() const */
    inline Container::ReadMetadataString toConstMetadataString() const
    { return toMetadataString(); }

    /**
     * Get a bytes metadata access to the handle.
     *
     * @return  a bytes metadata container access
     */
    inline Container::ReadMetadataBytes toMetadataBytes() const
    { return Container::ReadMetadataBytes(*this); }

    /** @see toMetadataBytes() const */
    inline Container::ReadMetadataBytes toConstMetadataBytes() const
    { return toMetadataBytes(); }

    /**
     * Get a flags metadata access to the handle.
     *
     * @return  a flags metadata container access
     */
    inline Container::ReadMetadataFlags toMetadataFlags() const
    { return Container::ReadMetadataFlags(*this); }

    /** @see toMetadataFlags() const */
    inline Container::ReadMetadataFlags toConstMetadataFlags() const
    { return toMetadataFlags(); }

    /**
     * Get a flags metadata access to the handle.
     *
     * @return  a flags metadata container access
     */
    inline Container::ReadMetadataSingleFlag toMetadataSingleFlag() const
    { return Container::ReadMetadataSingleFlag(*this); }

    /** @see toMetadataFlags() const */
    inline Container::ReadMetadataSingleFlag toConstMetadataSingleFlag() const
    { return toMetadataSingleFlag(); }

    /**
     * Get a array metadata access to the handle.
     *
     * @return  a array metadata container access
     */
    inline Container::ReadMetadataArray toMetadataArray() const
    { return Container::ReadMetadataArray(*this); }

    /** @see toMetadataArray() const */
    inline Container::ReadMetadataArray toConstMetadataArray() const
    { return toMetadataArray(); }

    /**
     * Get a matrix metadata access to the handle.
     *
     * @return  a matrix metadata container access
     */
    inline Container::ReadMetadataMatrix toMetadataMatrix() const
    { return Container::ReadMetadataMatrix(*this); }

    /** @see toMetadataMatrix() const */
    inline Container::ReadMetadataMatrix toConstMetadataMatrix() const
    { return toMetadataMatrix(); }

    /**
     * Get a hash metadata access to the handle.
     *
     * @return  a hash metadata container access
     */
    inline Container::ReadMetadataHash toMetadataHash() const
    { return Container::ReadMetadataHash(*this); }

    /** @see toMetadataHash() const */
    inline Container::ReadMetadataHash toConstMetadataHash() const
    { return toMetadataHash(); }

    /**
     * Get a hash metadata access to the handle.
     *
     * @return  a hash metadata container access
     */
    inline Container::ReadMetadataHashChild toMetadataHashChild() const
    { return Container::ReadMetadataHashChild(*this); }

    /** @see toMetadataHash() const */
    inline Container::ReadMetadataHashChild toConstMetadataHashChild() const
    { return toMetadataHashChild(); }

    /**
     * Get a keyframe metadata access to the handle.
     *
     * @return  a keyframe metadata container access
     */
    inline Container::ReadMetadataKeyframe toMetadataKeyframe() const
    { return Container::ReadMetadataKeyframe(*this); }

    /** @see toMetadataKeyframe() const */
    inline Container::ReadMetadataKeyframe toConstMetadataKeyframe() const
    { return toMetadataKeyframe(); }

    /**
     * Get a generic children access to the handle.
     *
     * @return  a generic children container access
     */
    inline Container::ReadGeneric toChildren() const
    { return Container::ReadGeneric(*this); }

    /** @see toChildren() const */
    inline Container::ReadGeneric toConstChildren() const
    { return toChildren(); }

    /**
     * Get the real number value of the handle.
     *
     * @return  a real number value
     */
    double toReal() const;

    /** @see toReal() const */
    inline double toDouble() const
    { return toReal(); }

    /**
     * Get the integer value of the handle
     *
     * @return  an integer value
     */
    std::int_fast64_t toInteger() const;

    inline qint64 toInt64() const
    { return toInteger(); }

    /**
     * Get the boolean value of the handle
     *
     * @return  the boolean value of the handle
     */
    bool toBoolean() const;

    inline bool toBool() const
    { return toBoolean(); }

    /**
     * Get the string value of the handle.
     *
     * @return  a string value
     */
    const std::string &toString() const;

    /** @see toString() const */
    QString toQString() const
    { return QString::fromStdString(toString()); }

    /**
     * Get the string to display in localized form.
     *
     * @param locale    the locale
     * @return          a display string
     */
    QString toDisplayString(const QString &locale) const;

    /**
     * Get the string to display in the current locale.
     *
     * @return      a display string
     */
    QString toDisplayString() const;

    /**
     * Get all possible localized strings.
     *
     * @return  the mapping of locales to display strings
     */
    std::unordered_map<QString, QString> allLocalizedStrings() const;

    /**
     * Get the binary byte value of the handle.
     *
     * @return  the binary contents of the handle
     */
    const Bytes &toBytes() const;

    /** @see toBytes() const */
    const Bytes &toBinary() const
    { return toBytes(); }

    /**
     * Get the flags value of the handle.
     *
     * @return  the flags
     */
    const Flags &toFlags() const;

    /** @see toFlags() const */
    inline const Flags &toConstFlags() const
    { return toFlags(); }

    /**
     * Test if the handle contains the specified flag.
     *
     * @param flag  the flag to check
     * @return      true of the handle as flags has the flag set
     */
    bool testFlag(const Flag &flag) const;

    /**
     * Test if the handle contains all the specified flags.
     *
     * @param flags the flags to check
     * @return      true if the handle as flags contains all the flags
     */
    bool testFlags(const Flags &flags) const;

    /**
     * Test if the handle contains any of the specified flags.
     *
     * @param flags the flags to check
     * @return      true if the handle as flags contains any of the flags
     */
    bool testAnyFlags(const Flags &flags) const;

    /**
     * Test if another handle is equivalent to this one, considering metadata.
     *
     * @param other     the handle to compare with
     * @param metadata  the metadata
     * @return          true if the two handles are equivalent
     */
    bool equivalentTo(const Read &other, const Read &metadata) const;
};

CPD3DATACORE_EXPORT inline void swap(Read &lhs, Read &rhs) noexcept
{ Read::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(Read &lhs, Read &rhs) noexcept
{ Read::swap(lhs, rhs); }


/**
 * A read and write handle in a variant hierarchy.  This
 * allows for modification of the values.
 */
class CPD3DATACORE_EXPORT Write final : public Read {
    Write(const Write &other, const PathElement &add);

    Write(const Write &other, PathElement &&add);

public:
    Write();

    Write(const Write &);

    Write &operator=(const Write &);

    Write(Write &&);

    Write &operator=(Write &&);

    explicit Write(Root &root);

    explicit Write(Root &&root);

    Write(Root &root, Path path);

    Write(Root &&root, Path path);

    explicit Write(Type type);

    static inline Write empty()
    { return Write(Type::Empty); }


    Write getRoot() const;

    Write getPath(const std::string &path) const;

    Write getPath(std::string &&path) const;

    Write getPath(const char *path) const;

    Write getPath(const QString &path) const;

    inline Write operator[](const std::string &path) const
    { return getPath(path); }

    inline Write operator[](std::string &&path) const
    { return getPath(std::move(path)); }

    inline Write operator[](const char *path) const
    { return getPath(path); }

    inline Write operator[](const QString &path) const
    { return getPath(path); }


    inline Write getPath(const PathElement &add) const
    { return Write(*this, add); }

    inline Write getPath(PathElement &&add) const
    { return Write(*this, std::move(add)); }

    Write getPath(const Path &add) const;

    Write getPath(Path &&add) const;


    inline Write child(const std::string &index) const
    { return Write(*this, {PathElement::Type::AnyString, index}); }

    inline Write child(std::string &&index) const
    { return Write(*this, {PathElement::Type::AnyString, std::move(index)}); }

    inline Write child(const char *index) const
    { return Write(*this, {PathElement::Type::AnyString, index}); }

    inline Write child(const QString &index) const
    { return Write(*this, {PathElement::Type::AnyString, index}); }


    inline Write hash(const std::string &index) const
    { return Write(*this, {PathElement::Type::Hash, index}); }

    inline Write hash(std::string &&index) const
    { return Write(*this, {PathElement::Type::Hash, std::move(index)}); }

    inline Write hash(const char *index) const
    { return Write(*this, {PathElement::Type::Hash, index}); }

    inline Write hash(const QString &index) const
    { return Write(*this, {PathElement::Type::Hash, index}); }

    inline Write array(std::size_t index) const
    { return Write(*this, {PathElement::Type::Array, index}); }

    inline Write matrix(const PathElement::MatrixIndex &index) const
    { return Write(*this, {PathElement::Type::Matrix, index}); }

    inline Write matrix(PathElement::MatrixIndex &&index) const
    { return Write(*this, {PathElement::Type::Matrix, std::move(index)}); }

    inline Write keyframe(double index) const
    {
        Q_ASSERT(FP::defined(index));
        return Write(*this, {PathElement::Type::Keyframe, index});
    }


    inline Write metadata(const std::string &index) const
    { return Write(*this, {PathElement::Type::AnyMetadata, index}); }

    inline Write metadata(std::string &&index) const
    { return Write(*this, {PathElement::Type::AnyMetadata, std::move(index)}); }

    inline Write metadata(const char *index) const
    { return Write(*this, {PathElement::Type::AnyMetadata, index}); }

    inline Write metadata(const QString &index) const
    { return Write(*this, {PathElement::Type::AnyMetadata, index}); }

    inline Write metadataReal(const std::string &index) const
    { return Write(*this, {PathElement::Type::MetadataReal, index}); }

    inline Write metadataReal(std::string &&index) const
    { return Write(*this, {PathElement::Type::MetadataReal, std::move(index)}); }

    inline Write metadataReal(const char *index) const
    { return Write(*this, {PathElement::Type::MetadataReal, index}); }

    inline Write metadataReal(const QString &index) const
    { return Write(*this, {PathElement::Type::MetadataReal, index}); }

    inline Write metadataInteger(const std::string &index) const
    { return Write(*this, {PathElement::Type::MetadataInteger, index}); }

    inline Write metadataInteger(std::string &&index) const
    { return Write(*this, {PathElement::Type::MetadataInteger, std::move(index)}); }

    inline Write metadataInteger(const char *index) const
    { return Write(*this, {PathElement::Type::MetadataInteger, index}); }

    inline Write metadataInteger(const QString &index) const
    { return Write(*this, {PathElement::Type::MetadataInteger, index}); }

    inline Write metadataBoolean(const std::string &index) const
    { return Write(*this, {PathElement::Type::MetadataBoolean, index}); }

    inline Write metadataBoolean(std::string &&index) const
    { return Write(*this, {PathElement::Type::MetadataBoolean, std::move(index)}); }

    inline Write metadataBoolean(const char *index) const
    { return Write(*this, {PathElement::Type::MetadataBoolean, index}); }

    inline Write metadataBoolean(const QString &index) const
    { return Write(*this, {PathElement::Type::MetadataBoolean, index}); }

    inline Write metadataString(const std::string &index) const
    { return Write(*this, {PathElement::Type::MetadataString, index}); }

    inline Write metadataString(std::string &&index) const
    { return Write(*this, {PathElement::Type::MetadataString, std::move(index)}); }

    inline Write metadataString(const char *index) const
    { return Write(*this, {PathElement::Type::MetadataString, index}); }

    inline Write metadataString(const QString &index) const
    { return Write(*this, {PathElement::Type::MetadataString, index}); }

    inline Write metadataBytes(const std::string &index) const
    { return Write(*this, {PathElement::Type::MetadataBytes, index}); }

    inline Write metadataBytes(std::string &&index) const
    { return Write(*this, {PathElement::Type::MetadataBytes, std::move(index)}); }

    inline Write metadataBytes(const char *index) const
    { return Write(*this, {PathElement::Type::MetadataBytes, index}); }

    inline Write metadataBytes(const QString &index) const
    { return Write(*this, {PathElement::Type::MetadataBytes, index}); }

    inline Write metadataFlags(const std::string &index) const
    { return Write(*this, {PathElement::Type::MetadataFlags, index}); }

    inline Write metadataFlags(std::string &&index) const
    { return Write(*this, {PathElement::Type::MetadataFlags, std::move(index)}); }

    inline Write metadataFlags(const char *index) const
    { return Write(*this, {PathElement::Type::MetadataFlags, index}); }

    inline Write metadataFlags(const QString &index) const
    { return Write(*this, {PathElement::Type::MetadataFlags, index}); }

    inline Write metadataArray(const std::string &index) const
    { return Write(*this, {PathElement::Type::MetadataArray, index}); }

    inline Write metadataArray(std::string &&index) const
    { return Write(*this, {PathElement::Type::MetadataArray, std::move(index)}); }

    inline Write metadataArray(const char *index) const
    { return Write(*this, {PathElement::Type::MetadataArray, index}); }

    inline Write metadataArray(const QString &index) const
    { return Write(*this, {PathElement::Type::MetadataArray, index}); }

    inline Write metadataMatrix(const std::string &index) const
    { return Write(*this, {PathElement::Type::MetadataMatrix, index}); }

    inline Write metadataMatrix(std::string &&index) const
    { return Write(*this, {PathElement::Type::MetadataMatrix, std::move(index)}); }

    inline Write metadataMatrix(const char *index) const
    { return Write(*this, {PathElement::Type::MetadataMatrix, index}); }

    inline Write metadataMatrix(const QString &index) const
    { return Write(*this, {PathElement::Type::MetadataMatrix, index}); }

    inline Write metadataHash(const std::string &index) const
    { return Write(*this, {PathElement::Type::MetadataHash, index}); }

    inline Write metadataHash(std::string &&index) const
    { return Write(*this, {PathElement::Type::MetadataHash, std::move(index)}); }

    inline Write metadataHash(const char *index) const
    { return Write(*this, {PathElement::Type::MetadataHash, index}); }

    inline Write metadataHash(const QString &index) const
    { return Write(*this, {PathElement::Type::MetadataHash, index}); }

    inline Write metadataKeyframe(const std::string &index) const
    { return Write(*this, {PathElement::Type::MetadataKeyframe, index}); }

    inline Write metadataKeyframe(std::string &&index) const
    { return Write(*this, {PathElement::Type::MetadataKeyframe, std::move(index)}); }

    inline Write metadataKeyframe(const char *index) const
    { return Write(*this, {PathElement::Type::MetadataKeyframe, index}); }

    inline Write metadataKeyframe(const QString &index) const
    { return Write(*this, {PathElement::Type::MetadataKeyframe, index}); }

    inline Write metadataHashChild(const std::string &index) const
    { return Write(*this, {PathElement::Type::MetadataHashChild, index}); }

    inline Write metadataHashChild(std::string &&index) const
    { return Write(*this, {PathElement::Type::MetadataHashChild, std::move(index)}); }

    inline Write metadataHashChild(const char *index) const
    { return Write(*this, {PathElement::Type::MetadataHashChild, index}); }

    inline Write metadataHashChild(const QString &index) const
    { return Write(*this, {PathElement::Type::MetadataHashChild, index}); }

    inline Write metadataSingleFlag(const std::string &index) const
    { return Write(*this, {PathElement::Type::MetadataSingleFlag, index}); }

    inline Write metadataSingleFlag(std::string &&index) const
    { return Write(*this, {PathElement::Type::MetadataSingleFlag, std::move(index)}); }

    inline Write metadataSingleFlag(const char *index) const
    { return Write(*this, {PathElement::Type::MetadataSingleFlag, index}); }

    inline Write metadataSingleFlag(const QString &index) const
    { return Write(*this, {PathElement::Type::MetadataSingleFlag, index}); }


    void setType(Type type);


    inline void setEmpty()
    { setType(Type::Empty); }


    /**
     * Clear the contents of the handle.  This removes and children
     * and sets the value to the invalid default.  It will preserve
     * the type of an existing value, but will not create one
     * if one doesn't already exist.
     */
    void clear();

    /**
     * Remove the value referenced by the handle, optionally
     * creating the parents that would be required to access
     * it as well.
     *
     * @param createParents create the parents needed to access the value if they do not exist
     */
    void remove(bool createParents = true);


    /**
     * Replace the contents of the variant referenced by the
     * handle with the value stored in another handle.
     *
     * @param value     the value to assign
     */
    void set(const Read &value);

    /**
     * Get a flags access to the handle.
     *
     * @return  a flags container access
     */
    inline Container::WriteFlags writeFlags()
    { return Container::WriteFlags(*this); }

    inline Container::ReadHash toHash() const
    { return Read::toHash(); }

    /**
     * Get a hash access to the handle.
     *
     * @return  a hash container access
     */
    inline Container::WriteHash toHash()
    { return Container::WriteHash(*this); }

    inline Container::ReadArray toArray() const
    { return Read::toArray(); }

    /**
     * Get an array access to the handle.
     *
     * @return  an array container access
     */
    inline Container::WriteArray toArray()
    { return Container::WriteArray(*this); }

    inline Container::ReadMatrix toMatrix() const
    { return Read::toMatrix(); }

    /**
     * Get an matrix access to the handle.
     *
     * @return  an matrix container access
     */
    inline Container::WriteMatrix toMatrix()
    { return Container::WriteMatrix(*this); }

    inline Container::ReadKeyframe toKeyframe() const
    { return Read::toKeyframe(); }

    /**
     * Get a keyframe access to the handle.
     *
     * @return  a keyframe container access
     */
    inline Container::WriteKeyframe toKeyframe()
    { return Container::WriteKeyframe(*this); }

    inline Container::ReadMetadataReal toMetadataReal() const
    { return Read::toMetadataReal(); }

    /**
     * Get a real metadata access to the handle.
     *
     * @return  a real metadata container access
     */
    inline Container::WriteMetadataReal toMetadataReal()
    { return Container::WriteMetadataReal(*this); }

    inline Container::ReadMetadataInteger toMetadataInteger() const
    { return Read::toMetadataInteger(); }

    /**
     * Get a integer metadata access to the handle.
     *
     * @return  a integer metadata container access
     */
    inline Container::WriteMetadataInteger toMetadataInteger()
    { return Container::WriteMetadataInteger(*this); }

    inline Container::ReadMetadataBoolean toMetadataBoolean() const
    { return Read::toMetadataBoolean(); }

    /**
     * Get a boolean metadata access to the handle.
     *
     * @return  a boolean metadata container access
     */
    inline Container::WriteMetadataBoolean toMetadataBoolean()
    { return Container::WriteMetadataBoolean(*this); }

    inline Container::ReadMetadataString toMetadataString() const
    { return Read::toMetadataString(); }

    /**
     * Get a string metadata access to the handle.
     *
     * @return  a string metadata container access
     */
    inline Container::WriteMetadataString toMetadataString()
    { return Container::WriteMetadataString(*this); }

    inline Container::ReadMetadataBytes toMetadataBytes() const
    { return Read::toMetadataBytes(); }

    /**
     * Get a bytes metadata access to the handle.
     *
     * @return  a bytes metadata container access
     */
    inline Container::WriteMetadataBytes toMetadataBytes()
    { return Container::WriteMetadataBytes(*this); }

    inline Container::ReadMetadataFlags toMetadataFlags() const
    { return Read::toMetadataFlags(); }

    /**
     * Get a flags metadata access to the handle.
     *
     * @return  a flags metadata container access
     */
    inline Container::WriteMetadataFlags toMetadataFlags()
    { return Container::WriteMetadataFlags(*this); }

    inline Container::ReadMetadataSingleFlag toMetadataSingleFlag() const
    { return Read::toMetadataSingleFlag(); }

    /**
     * Get a flags metadata access to the handle.
     *
     * @return  a flags metadata container access
     */
    inline Container::WriteMetadataSingleFlag toMetadataSingleFlag()
    { return Container::WriteMetadataSingleFlag(*this); }

    inline Container::ReadMetadataArray toMetadataArray() const
    { return Read::toMetadataArray(); }

    /**
     * Get a array metadata access to the handle.
     *
     * @return  a array metadata container access
     */
    inline Container::WriteMetadataArray toMetadataArray()
    { return Container::WriteMetadataArray(*this); }

    inline Container::ReadMetadataMatrix toMetadataMatrix() const
    { return Read::toMetadataMatrix(); }

    /**
     * Get a matrix metadata access to the handle.
     *
     * @return  a matrix metadata container access
     */
    inline Container::WriteMetadataMatrix toMetadataMatrix()
    { return Container::WriteMetadataMatrix(*this); }

    inline Container::ReadMetadataHash toMetadataHash() const
    { return Read::toMetadataHash(); }

    /**
     * Get a hash metadata access to the handle.
     *
     * @return  a hash metadata container access
     */
    inline Container::WriteMetadataHash toMetadataHash()
    { return Container::WriteMetadataHash(*this); }

    inline Container::ReadMetadataHashChild toMetadataHashChild() const
    { return Read::toMetadataHashChild(); }

    /**
     * Get a hash metadata access to the handle.
     *
     * @return  a hash metadata container access
     */
    inline Container::WriteMetadataHashChild toMetadataHashChild()
    { return Container::WriteMetadataHashChild(*this); }

    inline Container::ReadMetadataKeyframe toMetadataKeyframe() const
    { return Read::toMetadataKeyframe(); }

    /**
     * Get a keyframe metadata access to the handle.
     *
     * @return  a keyframe metadata container access
     */
    inline Container::WriteMetadataKeyframe toMetadataKeyframe()
    { return Container::WriteMetadataKeyframe(*this); }


    /**
     * Set the handle to a real number.
     *
     * @param value the value to set
     */
    void setReal(double value);

    /** @see setReal(double) */
    inline void setDouble(double value)
    { setReal(value); }

    inline Write &operator=(double value)
    {
        setReal(value);
        return *this;
    }

    /**
     * Set the handle to an integer.
     *
     * @param value the value to set
     */
    void setInteger(std::int_fast64_t value);

    /** @see setInteger(std::int_fast64_t) */
    inline void setInt64(std::int_fast64_t value)
    { setInteger(value); }

    inline Write &operator=(std::int_fast64_t value)
    {
        setInteger(value);
        return *this;
    }

    template<class Integer, class = typename std::enable_if<
            ((std::is_same<Integer, int>::value || std::is_same<Integer, qint64>::value) &&
                    !std::is_same<std::int_fast64_t, Integer>::value)>::type>
    inline Write &operator=(Integer value)
    {
        setInteger(value);
        return *this;
    }

    /**
     * Set the handle to a boolean true or false value.
     *
     * @param value the value to set
     */
    void setBoolean(bool value);

    /** @see setBoolean(bool) */
    inline void setBool(bool value)
    { setBoolean(value); }

    inline Write &operator=(bool value)
    {
        setBoolean(value);
        return *this;
    }

    /**
     * Set the handle to a string.
     *
     * @param value the value to set
     */
    void setString(const std::string &value);

    inline Write &operator=(const std::string &value)
    {
        setString(value);
        return *this;
    }

    /** @see setString(const std::string &) */
    void setString(std::string &&value);

    inline Write &operator=(std::string &&value)
    {
        setString(std::move(value));
        return *this;
    }

    /** @see setString(const std::string &) */
    inline void setString(const QString &value)
    { setString(value.toStdString()); }

    inline Write &operator=(const QString &value)
    {
        setString(value);
        return *this;
    }

    /** @see setString(const std::string &) */
    inline void setString(const char *value)
    { setString(std::string(value)); }

    inline Write &operator=(const char *value)
    {
        setString(value);
        return *this;
    }

    /**
     * Set the display localized string for a specific locale.
     *
     * @param locale    the locale to set in
     * @param value     the display string
     */
    void setDisplayString(const QString &locale, const QString &value);

    /**
     * Set the handle to a set of bytes.
     *
     * @param value the value to set
     */
    void setBytes(const Bytes &value);

    /** @see setBytes(const Bytes &) */
    inline void setBinary(const Bytes &value)
    { setBytes(value); }

    /** @see setBytes(const Bytes &) */
    void setBytes(Bytes &&value);

    /** @see setBytes(const Bytes &) */
    inline void setBinary(Bytes &&value)
    { setBytes(std::move(value)); }

    /** @see setBytes(const Bytes &) */
    inline void setBytes(const Util::ByteView &value)
    { setBytes(value.toQByteArray()); }

    /** @see setBytes(const Bytes &) */
    inline void setBinary(const Util::ByteView &value)
    { setBytes(value); }

    /**
     * Set the handle to a set of flags.
     *
     * @param flags the flats to set
     */
    void setFlags(const Flags &flags);

    /** @see setFlags(const Flags &) */
    void setFlags(Flags &&flags);

    /**
     * Set the handle to a set of flags.
     *
     * @param flags the flats to set
     */
    void setFlags(const Container::WriteFlags &flags);

    /**
     * Set the handle to flags if required then add
     * additional ones to any already present.
     *
     * @param flags     the flags to add
     */
    void addFlags(const Flags &flags);

    /** @see addFlags(const Flags &) */
    void addFlags(Flags &&flags);

    /**
     * Set the handle to flags if required then remove
     * additional ones to any already present.
     *
     * @param flags     the flags to remove
     */
    void removeFlags(const Flags &flags);

    /** @see addFlags(const Flags &) */
    void removeFlags(Flags &&flags);

    /**
     * Set the handle to flags if required then add
     * one to any already present.
     *
     * @param flag  the flag to add
     */
    void applyFlag(const Flag &flag);

    /** @see applyFlag(const Flag ) */
    void applyFlag(Flag &&flag);

    /**
     * Set the handle to flags if required then remove
     * one to any already present.
     *
     * @param flag  the flag to remove
     */
    void clearFlag(const Flag &flag);

    /** @see clearFlag(const Flag &) */
    void clearFlag(Flag &&flag);


    /**
     * Set the handle to an overlay path.
     *
     * @param value the overlay path
     */
    void setOverlay(const std::string &value);

    /** @see setOverlay(const std::string &) */
    void setOverlay(std::string &&value);

    /** @see setOverlay(const std::string &) */
    inline void setOverlay(const char *value)
    { setOverlay(std::string(value)); }

    /** @see setOverlay(const std::string &) */
    void setOverlay(const QString &value)
    { setOverlay(value.toStdString()); }
};


}
}
}


#ifdef CPD3_QTEST

#include "core/util.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

inline char *toString(const CPD3::Data::Variant::Read &read)
{
    QByteArray ba = "Variant::Read(";
    auto s = read.describe();
    Util::replace_all(s, "\n", "\\n");
    Util::replace_all(s, "\r", "\\r");
    Util::replace_all(s, "\t", "\\t");
    ba += s.data();
    ba += ")";
    return ::qstrdup(ba.data());
}

inline char *toString(const CPD3::Data::Variant::Write &write)
{
    QByteArray ba = "Variant::Write(";
    auto s = write.describe();
    Util::replace_all(s, "\n", "\\n");
    Util::replace_all(s, "\r", "\\r");
    Util::replace_all(s, "\t", "\\t");
    ba += s.data();
    ba += ")";
    return ::qstrdup(ba.data());
}

}
}
}
#endif

Q_DECLARE_METATYPE(CPD3::Data::Variant::Read);

Q_DECLARE_METATYPE(CPD3::Data::Variant::Write);

#endif //CPD3DATACOREVARIANT_HANDLE_HXX
