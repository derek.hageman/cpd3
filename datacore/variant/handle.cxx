/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QLocale>

#include "core/util.hxx"
#include "core/number.hxx"
#include "handle.hxx"
#include "root.hxx"
#include "toplevel.hxx"


namespace CPD3 {
namespace Data {
namespace Variant {


CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Read &h)
{
    Q_ASSERT(h.top);
    if (h.path.empty()) {
        h.top->node()->serialize(stream);
    } else if (auto ptr = h.top->read(h.path)) {
        ptr->serialize(stream);
    } else {
        NodeEmpty::instance->serialize(stream);
    }
    return stream;
}

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, Read &h)
{
    h.top = std::make_shared<TopLevel>(stream);
    h.path.clear();
    return stream;
}

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const Read &h)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    if (!h.top) {
        stream << "Null";
    } else if (h.path.empty()) {
        stream << h.top->node()->describe().data();
    } else if (auto ptr = h.top->read(h.path)) {
        stream << ptr->describe().data();
    } else {
        stream << "Empty";
    }

    return stream;
}

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Write &h)
{
    stream << static_cast<const Read &>(h);
    return stream;
}

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, Write &h)
{
    stream >> static_cast<Read &>(h);
    return stream;
}


Read::Read() = default;

Read::~Read() = default;

Read::Read(const Read &) = default;

Read &Read::operator=(const Read &) = default;

Read::Read(Read &&) = default;

Read &Read::operator=(Read &&) = default;

static const Read emptyInstance = Read(Root());

const Read &Read::empty()
{ return emptyInstance; }

Read::Read(Root &&root) : top(std::move(root.top))
{ }

Read::Read(const Root &root) : top(root.top)
{ }

Read::Read(const Root &root, const Path &path) : top(root.top), path(path)
{ }

Read::Read(const Root &root, Path &&path) : top(root.top), path(std::move(path))
{ }

Read::Read(const Read &other, const PathElement &add) : top(other.top), path(other.path)
{ path.emplace_back(add); }

Read::Read(const Read &other, PathElement &&add) : top(other.top), path(other.path)
{ path.emplace_back(std::move(add)); }

void Read::swap(Read &a, Read &b)
{
    using std::swap;
    swap(a.top, b.top);
    swap(a.path, b.path);
}

Read Read::getRoot() const
{
    Read result;
    result.top = top;
    return result;
}

Read Read::getPath(const std::string &path) const
{
    Read result;
    result.top = top;
    result.path = PathElement::parse(path, this->path);
    return result;
}

Read Read::getPath(std::string &&path) const
{
    Read result;
    result.top = top;
    result.path = PathElement::parse(std::move(path), this->path);
    return result;
}

Read Read::getPath(const char *path) const
{
    Read result;
    result.top = top;
    result.path = PathElement::parse(path, this->path);
    return result;
}

Read Read::getPath(const QString &path) const
{
    Read result;
    result.top = top;
    result.path = PathElement::parse(path.toStdString(), this->path);
    return result;
}

Read Read::getPath(const Path &add) const
{
    Read result = *this;
    Util::append(add, result.path);
    return result;
}

Read Read::getPath(Path &&add) const
{
    Read result = *this;
    Util::append(std::move(add), result.path);
    return result;
}

void Read::detachFromRoot()
{
    if (!top) {
        path.clear();
        top = std::make_shared<TopLevel>();
        return;
    }

    if (path.empty()) {
        top = std::make_shared<TopLevel>(*top);
        return;
    }

    auto ptr = top->read(path);
    path.clear();

    if (!ptr) {
        top = std::make_shared<TopLevel>();
        return;
    }

    top = std::make_shared<TopLevel>(*ptr);
}

std::string Read::describe(bool includePath) const
{
    if (!top)
        return std::string("Null");
    if (path.empty())
        return top->node()->describe();
    std::string result;
    if (includePath) {
        result += "{";
        for (const auto &add : path) {
            result += "/";
            result += add.toString();
        }
        result += "}";
    }
    if (auto ptr = top->read(path)) {
        result += ptr->describe();
    } else {
        result += "Empty";
    }
    return result;
}

bool Read::equal(const Read &other) const
{
    if (!top)
        return !other.top;
    const Node *a = nullptr;
    std::shared_ptr<Node> pinA;
    if (path.empty()) {
        a = top->node();
    } else {
        pinA = top->read(path);
        if (!pinA)
            pinA = NodeEmpty::instance;
        a = pinA.get();
    }
    Q_ASSERT(a);

    if (!other.top)
        return false;
    const Node *b = nullptr;
    std::shared_ptr<Node> pinB;
    if (other.path.empty()) {
        b = other.top->node();
    } else {
        pinB = other.top->read(other.path);
        if (!pinB)
            pinB = NodeEmpty::instance;
        b = pinB.get();
    }
    Q_ASSERT(b);

    return a->identicalTo(*b);
}

bool Read::equivalentTo(const Read &other, const Read &metadata) const
{
    if (!top)
        return !other.top;
    const Node *a = nullptr;
    std::shared_ptr<Node> pinA;
    if (path.empty()) {
        a = top->node();
    } else {
        pinA = top->read(path);
        if (!pinA)
            pinA = NodeEmpty::instance;
        a = pinA.get();
    }
    Q_ASSERT(a);

    if (!other.top)
        return false;
    const Node *b = nullptr;
    std::shared_ptr<Node> pinB;
    if (other.path.empty()) {
        b = other.top->node();
    } else {
        pinB = other.top->read(other.path);
        if (!pinB)
            pinB = NodeEmpty::instance;
        b = pinB.get();
    }
    Q_ASSERT(b);

    const Node *m = nullptr;
    std::shared_ptr<Node> pinM;
    if (!metadata.top) {
        pinM = NodeEmpty::instance;
        m = pinM.get();
    } else if (metadata.path.empty()) {
        m = metadata.top->node();
    } else {
        pinM = metadata.top->read(metadata.path);
        if (!pinM)
            pinM = NodeEmpty::instance;
        m = pinM.get();
    }
    Q_ASSERT(m);

    return a->equalTo(*b, *m);
}

std::size_t Read::ValueHash::operator()(const Read &value) const
{
    if (!value.top)
        return 0;
    if (value.path.empty())
        return value.top->node()->hash();

    auto ptr = value.top->read(value.path);
    if (!ptr)
        return NodeEmpty::instance->hash();
    return ptr->hash();
}

Type Read::getType() const
{
    Q_ASSERT(top);
    Node::Type type;
    if (path.empty()) {
        type = top->node()->type();
    } else {
        auto ptr = top->read(path);
        if (!ptr)
            return Type::Empty;
        type = ptr->type();
    }
    switch (type) {
    case Node::Type::Empty:
        return Type::Empty;

    case Node::Type::Real:
    case Node::Type::RealConstant:
        return Type::Real;
    case Node::Type::Integer:
    case Node::Type::IntegerConstant:
        return Type::Integer;
    case Node::Type::Boolean:
    case Node::Type::BooleanConstant:
        return Type::Boolean;
    case Node::Type::String:
    case Node::Type::StringConstant:
    case Node::Type::LocalizedString:
    case Node::Type::LocalizedStringConstant:
        return Type::String;
    case Node::Type::Bytes:
    case Node::Type::BytesConstant:
        return Type::Bytes;
    case Node::Type::Flags:
    case Node::Type::FlagsConstant:
        return Type::Flags;

    case Node::Type::Hash:
    case Node::Type::HashConstant:
        return Type::Hash;
    case Node::Type::Array:
    case Node::Type::ArrayConstant:
        return Type::Array;
    case Node::Type::Matrix:
    case Node::Type::MatrixConstant:
        return Type::Matrix;
    case Node::Type::Keyframe:
    case Node::Type::KeyframeConstant:
        return Type::Keyframe;

    case Node::Type::MetadataReal:
    case Node::Type::MetadataRealConstant:
        return Type::MetadataReal;
    case Node::Type::MetadataInteger:
    case Node::Type::MetadataIntegerConstant:
        return Type::MetadataInteger;
    case Node::Type::MetadataBoolean:
    case Node::Type::MetadataBooleanConstant:
        return Type::MetadataBoolean;
    case Node::Type::MetadataString:
    case Node::Type::MetadataStringConstant:
        return Type::MetadataString;
    case Node::Type::MetadataBytes:
    case Node::Type::MetadataBytesConstant:
        return Type::MetadataBytes;
    case Node::Type::MetadataFlags:
    case Node::Type::MetadataFlagsConstant:
        return Type::MetadataFlags;
    case Node::Type::MetadataHash:
    case Node::Type::MetadataHashConstant:
        return Type::MetadataHash;
    case Node::Type::MetadataArray:
    case Node::Type::MetadataArrayConstant:
        return Type::MetadataArray;
    case Node::Type::MetadataMatrix:
    case Node::Type::MetadataMatrixConstant:
        return Type::MetadataMatrix;
    case Node::Type::MetadataKeyframe:
    case Node::Type::MetadataKeyframeConstant:
        return Type::MetadataKeyframe;

    case Node::Type::Overlay:
    case Node::Type::OverlayConstant:
        return Type::Overlay;
    }

    Q_ASSERT(false);
    return Type::Empty;
}

bool Read::exists() const
{
    Q_ASSERT(top);
    if (path.empty())
        return top->node()->type() != Node::Type::Empty;
    auto ptr = top->read(path);
    if (!ptr)
        return false;
    return ptr->type() != Node::Type::Empty;
}

bool Read::isMetadata() const
{
    Q_ASSERT(top);
    Node::Type type;
    if (path.empty()) {
        type = top->node()->type();
    } else {
        auto ptr = top->read(path);
        if (!ptr)
            return false;
        type = ptr->type();
    }
    switch (type) {
    case Node::Type::Empty:
    case Node::Type::Real:
    case Node::Type::RealConstant:
    case Node::Type::Integer:
    case Node::Type::IntegerConstant:
    case Node::Type::Boolean:
    case Node::Type::BooleanConstant:
    case Node::Type::String:
    case Node::Type::StringConstant:
    case Node::Type::LocalizedString:
    case Node::Type::LocalizedStringConstant:
    case Node::Type::Bytes:
    case Node::Type::BytesConstant:
    case Node::Type::Flags:
    case Node::Type::FlagsConstant:
    case Node::Type::Hash:
    case Node::Type::HashConstant:
    case Node::Type::Array:
    case Node::Type::ArrayConstant:
    case Node::Type::Matrix:
    case Node::Type::MatrixConstant:
    case Node::Type::Keyframe:
    case Node::Type::KeyframeConstant:
    case Node::Type::Overlay:
    case Node::Type::OverlayConstant:
        return false;

    case Node::Type::MetadataReal:
    case Node::Type::MetadataRealConstant:
    case Node::Type::MetadataInteger:
    case Node::Type::MetadataIntegerConstant:
    case Node::Type::MetadataBoolean:
    case Node::Type::MetadataBooleanConstant:
    case Node::Type::MetadataString:
    case Node::Type::MetadataStringConstant:
    case Node::Type::MetadataBytes:
    case Node::Type::MetadataBytesConstant:
    case Node::Type::MetadataFlags:
    case Node::Type::MetadataFlagsConstant:
    case Node::Type::MetadataHash:
    case Node::Type::MetadataHashConstant:
    case Node::Type::MetadataArray:
    case Node::Type::MetadataArrayConstant:
    case Node::Type::MetadataMatrix:
    case Node::Type::MetadataMatrixConstant:
    case Node::Type::MetadataKeyframe:
    case Node::Type::MetadataKeyframeConstant:
        return true;
    }

    Q_ASSERT(false);
    return false;
}

Container::ReadMetadata Read::toMetadataSpecialized() const
{
    Q_ASSERT(top);
    Node::Type type;
    if (path.empty()) {
        type = top->node()->type();
    } else {
        auto ptr = top->read(path);
        if (!ptr)
            return toMetadata();
        type = ptr->type();
    }
    switch (type) {
    case Node::Type::Empty:
    case Node::Type::Real:
    case Node::Type::RealConstant:
    case Node::Type::Integer:
    case Node::Type::IntegerConstant:
    case Node::Type::Boolean:
    case Node::Type::BooleanConstant:
    case Node::Type::String:
    case Node::Type::StringConstant:
    case Node::Type::LocalizedString:
    case Node::Type::LocalizedStringConstant:
    case Node::Type::Bytes:
    case Node::Type::BytesConstant:
    case Node::Type::Flags:
    case Node::Type::FlagsConstant:
    case Node::Type::Hash:
    case Node::Type::HashConstant:
    case Node::Type::Array:
    case Node::Type::ArrayConstant:
    case Node::Type::Matrix:
    case Node::Type::MatrixConstant:
    case Node::Type::Keyframe:
    case Node::Type::KeyframeConstant:
    case Node::Type::Overlay:
    case Node::Type::OverlayConstant:
        return toMetadata();

    case Node::Type::MetadataReal:
    case Node::Type::MetadataRealConstant:
        return toMetadataReal();
    case Node::Type::MetadataInteger:
    case Node::Type::MetadataIntegerConstant:
        return toMetadataInteger();
    case Node::Type::MetadataBoolean:
    case Node::Type::MetadataBooleanConstant:
        return toMetadataBoolean();
    case Node::Type::MetadataString:
    case Node::Type::MetadataStringConstant:
        return toMetadataString();
    case Node::Type::MetadataBytes:
    case Node::Type::MetadataBytesConstant:
        return toMetadataBytes();
    case Node::Type::MetadataFlags:
    case Node::Type::MetadataFlagsConstant:
        return toMetadataFlags();
    case Node::Type::MetadataHash:
    case Node::Type::MetadataHashConstant:
        return toMetadataHash();
    case Node::Type::MetadataArray:
    case Node::Type::MetadataArrayConstant:
        return toMetadataArray();
    case Node::Type::MetadataMatrix:
    case Node::Type::MetadataMatrixConstant:
        return toMetadataMatrix();
    case Node::Type::MetadataKeyframe:
    case Node::Type::MetadataKeyframeConstant:
        return toMetadataKeyframe();
    }
    return toMetadata();
}

double Read::toReal() const
{
    Q_ASSERT(top);
    if (path.empty())
        return top->node()->toReal();
    auto ptr = top->read(path);
    if (!ptr)
        return FP::undefined();
    return ptr->toReal();
}

std::int_fast64_t Read::toInteger() const
{
    Q_ASSERT(top);
    if (path.empty())
        return top->node()->toInteger();
    auto ptr = top->read(path);
    if (!ptr)
        return INTEGER::undefined();
    return ptr->toInteger();
}

bool Read::toBoolean() const
{
    Q_ASSERT(top);
    if (path.empty())
        return top->node()->toBoolean();
    auto ptr = top->read(path);
    if (!ptr)
        return false;
    return ptr->toBoolean();
}

const std::string &Read::toString() const
{
    Q_ASSERT(top);
    if (path.empty())
        return top->node()->toString();
    auto ptr = top->read(path);
    if (!ptr)
        return Node::invalidString;
    return ptr->toString();
}

QString Read::toDisplayString(const QString &locale) const
{
    Q_ASSERT(top);
    if (path.empty())
        return top->node()->toDisplayString(QLocale(locale));
    auto ptr = top->read(path);
    if (!ptr)
        return QString();
    return ptr->toDisplayString(QLocale(locale));
}

QString Read::toDisplayString() const
{
    Q_ASSERT(top);
    if (path.empty())
        return top->node()->toDisplayString(QLocale());
    auto ptr = top->read(path);
    if (!ptr)
        return QString();
    return ptr->toDisplayString(QLocale());
}

std::unordered_map<QString, QString> Read::allLocalizedStrings() const
{
    Q_ASSERT(top);
    if (path.empty()) {
        switch (top->node()->type()) {
        case Node::Type::LocalizedString:
        case Node::Type::LocalizedStringConstant:
            return top->node<NodeLocalizedString>()->getAllStrings();
        default:
            break;
        }
        return std::unordered_map<QString, QString>();
    }
    auto ptr = top->read(path);
    if (!ptr)
        return std::unordered_map<QString, QString>();

    switch (ptr->type()) {
    case Node::Type::LocalizedString:
    case Node::Type::LocalizedStringConstant:
        return std::static_pointer_cast<NodeLocalizedString>(ptr)->getAllStrings();
    default:
        break;
    }
    return std::unordered_map<QString, QString>();
}

const Bytes &Read::toBytes() const
{
    Q_ASSERT(top);
    if (path.empty())
        return top->node()->toBytes();
    auto ptr = top->read(path);
    if (!ptr)
        return Node::invalidBytes;
    return ptr->toBytes();
}

const Flags &Read::toFlags() const
{
    Q_ASSERT(top);
    if (path.empty())
        return top->node()->toFlags();
    auto ptr = top->read(path);
    if (!ptr)
        return Node::invalidFlags;
    return ptr->toFlags();
}

bool Read::testFlag(const Flag &flag) const
{ return toFlags().count(flag) != 0; }

bool Read::testFlags(const Flags &flags) const
{
    const auto &existing = toFlags();
    for (const auto &flag : flags) {
        if (existing.count(flag) == 0)
            return false;
    }
    return true;
}

bool Read::testAnyFlags(const Flags &flags) const
{
    const auto &existing = toFlags();
    for (const auto &flag : flags) {
        if (existing.count(flag) != 0)
            return true;
    }
    return false;
}


Write::Write() = default;

Write::Write(const Write &) = default;

Write &Write::operator=(const Write &) = default;

Write::Write(Write &&) = default;

Write &Write::operator=(Write &&) = default;

Write::Write(Root &root) : Read(root)
{ }

Write::Write(Root &&root) : Read(std::move(root))
{ }

Write::Write(Root &root, Path path) : Read(root, std::move(path))
{ }

Write::Write(Root &&root, Path path) : Read(root, std::move(path))
{ }

Write::Write(const Write &other, const PathElement &add) : Read(other, add)
{ }

Write::Write(const Write &other, PathElement &&add) : Read(other, std::move(add))
{ }

Write::Write(Type type) : Write(Root(type))
{ }

Write Write::getRoot() const
{
    Write result;
    result.top = top;
    return result;
}

Write Write::getPath(const std::string &path) const
{
    Write result;
    result.top = top;
    result.path = PathElement::parse(path, this->path);
    return result;
}

Write Write::getPath(std::string &&path) const
{
    Write result;
    result.top = top;
    result.path = PathElement::parse(std::move(path), this->path);
    return result;
}

Write Write::getPath(const char *path) const
{
    Write result;
    result.top = top;
    result.path = PathElement::parse(path, this->path);
    return result;
}

Write Write::getPath(const QString &path) const
{
    Write result;
    result.top = top;
    result.path = PathElement::parse(path.toStdString(), this->path);
    return result;
}

Write Write::getPath(const Path &add) const
{
    Write result = *this;
    Util::append(add, result.path);
    return result;
}

Write Write::getPath(Path &&add) const
{
    Write result = *this;
    Util::append(std::move(add), result.path);
    return result;
}

void Write::setType(Type type)
{
    Q_ASSERT(top);
    if (path.empty()) {
        top->write(Node::goalFromGenericType(type));
    } else {
        top->write(path, Node::goalFromGenericType(type));
    }
}

void Write::clear()
{
    if (!top) {
        path.clear();
        top = std::make_shared<TopLevel>();
        return;
    }
    if (path.empty()) {
        top->node()->clear();
        return;
    }
    auto existing = top->read(path);
    if (!existing)
        return;
    top->write(path, Node::goalFromNodeType(existing->type()))->clear();
}

void Write::remove(bool createParents)
{
    Q_ASSERT(top);
    if (path.empty()) {
        setEmpty();
        return;
    }
    if (!createParents && !top->read(path))
        return;
    top->remove(path);
}

void Write::set(const Read &value)
{
    Q_ASSERT(top);
    Q_ASSERT(value.top);
    if (path.empty()) {
        if (value.path.empty()) {
            *top = *value.top;
            return;
        }
        auto ptr = value.top->read(value.path);
        if (!ptr) {
            top->set<NodeEmpty>();
            return;
        }
        /* Safe to assign, since we know we have a reference via
         * the shared pointer, so even if this frees the root
         * the node itself will remain */
        *top = *ptr;
        return;
    }

    top->assign(path, value.top->detached(value.path));
}

void Write::setReal(double value)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeReal *>(top->write(Node::WriteGoal::Real))->setReal(value);
    } else {
        std::static_pointer_cast<NodeReal>(top->write(path, Node::WriteGoal::Real))->setReal(value);
    }
}

void Write::setInteger(std::int_fast64_t value)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeInteger *>(top->write(Node::WriteGoal::Integer))->setInteger(value);
    } else {
        std::static_pointer_cast<NodeInteger>(
                top->write(path, Node::WriteGoal::Integer))->setInteger(value);
    }
}

void Write::setBoolean(bool value)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeBoolean *>(top->write(Node::WriteGoal::Boolean))->setBoolean(value);
    } else {
        std::static_pointer_cast<NodeBoolean>(
                top->write(path, Node::WriteGoal::Boolean))->setBoolean(value);
    }
}

void Write::setOverlay(const std::string &value)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeOverlay *>(top->write(Node::WriteGoal::Overlay))->setOverlayPath(value);
    } else {
        std::static_pointer_cast<NodeOverlay>(
                top->write(path, Node::WriteGoal::Overlay))->setOverlayPath(value);
    }
}

void Write::setOverlay(std::string &&value)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeOverlay *>(top->write(Node::WriteGoal::Overlay))->setOverlayPath(
                std::move(value));
    } else {
        std::static_pointer_cast<NodeOverlay>(
                top->write(path, Node::WriteGoal::Overlay))->setOverlayPath(std::move(value));
    }
}

void Write::setString(const std::string &value)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeString *>(top->write(Node::WriteGoal::String))->setString(value);
    } else {
        std::static_pointer_cast<NodeString>(top->write(path, Node::WriteGoal::String))->setString(
                value);
    }
}

void Write::setString(std::string &&value)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeString *>(top->write(Node::WriteGoal::String))->setString(std::move(value));
    } else {
        std::static_pointer_cast<NodeString>(top->write(path, Node::WriteGoal::String))->setString(
                std::move(value));
    }
}

void Write::setDisplayString(const QString &locale, const QString &value)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeLocalizedString *>(top->write(
                Node::WriteGoal::LocalizedString))->setDisplayString(locale, value);
    } else {
        std::static_pointer_cast<NodeLocalizedString>(
                top->write(path, Node::WriteGoal::LocalizedString))->setDisplayString(locale,
                                                                                      value);
    }
}

void Write::setBytes(const Bytes &value)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeBytes *>(top->write(Node::WriteGoal::Bytes))->setBytes(value);
    } else {
        std::static_pointer_cast<NodeBytes>(top->write(path, Node::WriteGoal::Bytes))->setBytes(
                value);
    }
}

void Write::setBytes(Bytes &&value)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeBytes *>(top->write(Node::WriteGoal::Bytes))->setBytes(std::move(value));
    } else {
        std::static_pointer_cast<NodeBytes>(top->write(path, Node::WriteGoal::Bytes))->setBytes(
                std::move(value));
    }
}

void Write::setFlags(const Flags &flags)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeFlags *>(top->write(Node::WriteGoal::Flags))->setFlags(flags);
    } else {
        std::static_pointer_cast<NodeFlags>(top->write(path, Node::WriteGoal::Flags))->setFlags(
                flags);
    }
}

void Write::setFlags(Flags &&flags)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeFlags *>(top->write(Node::WriteGoal::Flags))->setFlags(std::move(flags));
    } else {
        std::static_pointer_cast<NodeFlags>(top->write(path, Node::WriteGoal::Flags))->setFlags(
                std::move(flags));
    }
}

void Write::setFlags(const Container::WriteFlags &flags)
{
    Q_ASSERT(top);
    NodeFlags *target = nullptr;
    if (path.empty()) {
        target = static_cast<NodeFlags *>(top->write(Node::WriteGoal::Flags));
    } else {
        target =
                std::static_pointer_cast<NodeFlags>(top->write(path, Node::WriteGoal::Flags)).get();
    }
    target->setFlags(flags);
}

void Write::addFlags(const Flags &flags)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeFlags *>(top->write(Node::WriteGoal::Flags))->addFlags(flags);
    } else {
        std::static_pointer_cast<NodeFlags>(top->write(path, Node::WriteGoal::Flags))->addFlags(
                flags);
    }
}

void Write::addFlags(Flags &&flags)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeFlags *>(top->write(Node::WriteGoal::Flags))->addFlags(std::move(flags));
    } else {
        std::static_pointer_cast<NodeFlags>(top->write(path, Node::WriteGoal::Flags))->addFlags(
                std::move(flags));
    }
}

void Write::removeFlags(const Flags &flags)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeFlags *>(top->write(Node::WriteGoal::Flags))->removeFlags(flags);
    } else {
        std::static_pointer_cast<NodeFlags>(top->write(path, Node::WriteGoal::Flags))->removeFlags(
                flags);
    }
}

void Write::removeFlags(Flags &&flags)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeFlags *>(top->write(Node::WriteGoal::Flags))->removeFlags(std::move(flags));
    } else {
        std::static_pointer_cast<NodeFlags>(top->write(path, Node::WriteGoal::Flags))->removeFlags(
                std::move(flags));
    }
}

void Write::applyFlag(const Flag &flag)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeFlags *>(top->write(Node::WriteGoal::Flags))->applyFlag(flag);
    } else {
        std::static_pointer_cast<NodeFlags>(top->write(path, Node::WriteGoal::Flags))->applyFlag(
                flag);
    }
}

void Write::applyFlag(Flag &&flag)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeFlags *>(top->write(Node::WriteGoal::Flags))->applyFlag(std::move(flag));
    } else {
        std::static_pointer_cast<NodeFlags>(top->write(path, Node::WriteGoal::Flags))->applyFlag(
                std::move(flag));
    }
}

void Write::clearFlag(const Flag &flag)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeFlags *>(top->write(Node::WriteGoal::Flags))->clearFlag(flag);
    } else {
        std::static_pointer_cast<NodeFlags>(top->write(path, Node::WriteGoal::Flags))->clearFlag(
                flag);
    }
}

void Write::clearFlag(Flag &&flag)
{
    Q_ASSERT(top);
    if (path.empty()) {
        static_cast<NodeFlags *>(top->write(Node::WriteGoal::Flags))->clearFlag(std::move(flag));
    } else {
        std::static_pointer_cast<NodeFlags>(top->write(path, Node::WriteGoal::Flags))->clearFlag(
                std::move(flag));
    }
}


}
}
}