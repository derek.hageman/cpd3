/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "core/number.hxx"
#include "node.hxx"
#include "primitive.hxx"
#include "strings.hxx"
#include "overlay.hxx"
#include "hash.hxx"
#include "array.hxx"
#include "matrix.hxx"
#include "serialization.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

Node::~Node() = default;

Node::Node() noexcept = default;

Node::Node(const Node &) noexcept = default;

std::shared_ptr<Node> Node::read(const PathElement &) const
{ return std::shared_ptr<Node>(); }

bool Node::compatible(Node::WriteGoal) const
{ return false; }

bool Node::childrenWritable(PathElement::Type) const
{ return false; }

Node::WriteGoal Node::goalFromGenericType(CPD3::Data::Variant::Type type)
{
    switch (type) {
    case CPD3::Data::Variant::Type::Empty:
        return WriteGoal::Empty;
    case CPD3::Data::Variant::Type::Real:
        return WriteGoal::Real;
    case CPD3::Data::Variant::Type::Integer:
        return WriteGoal::Integer;
    case CPD3::Data::Variant::Type::Boolean:
        return WriteGoal::Boolean;
    case CPD3::Data::Variant::Type::String:
        return WriteGoal::String;
    case CPD3::Data::Variant::Type::Bytes:
        return WriteGoal::Bytes;
    case CPD3::Data::Variant::Type::Flags:
        return WriteGoal::Flags;
    case CPD3::Data::Variant::Type::Hash:
        return WriteGoal::Hash;
    case CPD3::Data::Variant::Type::Array:
        return WriteGoal::Array;
    case CPD3::Data::Variant::Type::Matrix:
        return WriteGoal::Matrix;
    case CPD3::Data::Variant::Type::Keyframe:
        return WriteGoal::Keyframe;
    case CPD3::Data::Variant::Type::MetadataReal:
        return WriteGoal::MetadataReal;
    case CPD3::Data::Variant::Type::MetadataInteger:
        return WriteGoal::MetadataInteger;
    case CPD3::Data::Variant::Type::MetadataBoolean:
        return WriteGoal::MetadataBoolean;
    case CPD3::Data::Variant::Type::MetadataString:
        return WriteGoal::MetadataString;
    case CPD3::Data::Variant::Type::MetadataBytes:
        return WriteGoal::MetadataBytes;
    case CPD3::Data::Variant::Type::MetadataFlags:
        return WriteGoal::MetadataFlags;
    case CPD3::Data::Variant::Type::MetadataHash:
        return WriteGoal::MetadataHash;
    case CPD3::Data::Variant::Type::MetadataArray:
        return WriteGoal::MetadataArray;
    case CPD3::Data::Variant::Type::MetadataMatrix:
        return WriteGoal::MetadataMatrix;
    case CPD3::Data::Variant::Type::MetadataKeyframe:
        return WriteGoal::MetadataKeyframe;
    case CPD3::Data::Variant::Type::Overlay:
        return WriteGoal::Overlay;
    }
    Q_ASSERT(false);
    return WriteGoal::Empty;
}

Node::WriteGoal Node::goalFromNodeType(Type type)
{
    switch (type) {
    case Type::Empty:
        return WriteGoal::Empty;
    case Type::Real:
    case Type::RealConstant:
        return WriteGoal::Real;
    case Type::Integer:
    case Type::IntegerConstant:
        return WriteGoal::Integer;
    case Type::Boolean:
    case Type::BooleanConstant:
        return WriteGoal::Boolean;
    case Type::String:
    case Type::StringConstant:
        return WriteGoal::String;
    case Type::LocalizedString:
    case Type::LocalizedStringConstant:
        return WriteGoal::LocalizedString;
    case Type::Bytes:
    case Type::BytesConstant:
        return WriteGoal::Bytes;
    case Type::Flags:
    case Type::FlagsConstant:
        return WriteGoal::Flags;
    case Type::Hash:
    case Type::HashConstant:
        return WriteGoal::Hash;
    case Type::Array:
    case Type::ArrayConstant:
        return WriteGoal::Array;
    case Type::Matrix:
    case Type::MatrixConstant:
        return WriteGoal::Matrix;
    case Type::Keyframe:
    case Type::KeyframeConstant:
        return WriteGoal::Keyframe;
    case Type::MetadataReal:
    case Type::MetadataRealConstant:
        return WriteGoal::MetadataReal;
    case Type::MetadataInteger:
    case Type::MetadataIntegerConstant:
        return WriteGoal::MetadataInteger;
    case Type::MetadataBoolean:
    case Type::MetadataBooleanConstant:
        return WriteGoal::MetadataBoolean;
    case Type::MetadataString:
    case Type::MetadataStringConstant:
        return WriteGoal::MetadataString;
    case Type::MetadataBytes:
    case Type::MetadataBytesConstant:
        return WriteGoal::MetadataBytes;
    case Type::MetadataFlags:
    case Type::MetadataFlagsConstant:
        return WriteGoal::MetadataFlags;
    case Type::MetadataHash:
    case Type::MetadataHashConstant:
        return WriteGoal::MetadataHash;
    case Type::MetadataArray:
    case Type::MetadataArrayConstant:
        return WriteGoal::MetadataArray;
    case Type::MetadataMatrix:
    case Type::MetadataMatrixConstant:
        return WriteGoal::MetadataMatrix;
    case Type::MetadataKeyframe:
    case Type::MetadataKeyframeConstant:
        return WriteGoal::MetadataKeyframe;
    case Type::Overlay:
    case Type::OverlayConstant:
        return WriteGoal::Overlay;
    }
    Q_ASSERT(false);
    return WriteGoal::Empty;
}

std::shared_ptr<Node> Node::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> Node::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void Node::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void Node::remove(const PathElement &)
{ }

std::shared_ptr<Node> Node::create(Node::WriteGoal final, const Node *existing)
{
    switch (final) {
    case Node::WriteGoal::Empty:
        return NodeEmpty::instance;
    case Node::WriteGoal::Real:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Real:
                return std::make_shared<NodeReal>(*static_cast<const NodeReal *>(existing));
            case Node::Type::RealConstant:
                return std::make_shared<NodeReal>(*static_cast<const NodeRealConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeReal>();
    case Node::WriteGoal::Integer:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Integer:
                return std::make_shared<NodeInteger>(
                        *static_cast<const NodeIntegerConstant *>(existing));
            case Node::Type::IntegerConstant:
                return std::make_shared<NodeInteger>(*static_cast<const NodeInteger *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeInteger>();
    case Node::WriteGoal::Boolean:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Boolean:
                return std::make_shared<NodeBoolean>(*static_cast<const NodeBoolean *>(existing));
            case Node::Type::BooleanConstant:
                return std::make_shared<NodeBoolean>(
                        *static_cast<const NodeBooleanConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeBoolean>();
    case Node::WriteGoal::String:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::String:
                return std::make_shared<NodeString>(*static_cast<const NodeString *>(existing));
            case Node::Type::StringConstant:
                return std::make_shared<NodeString>(
                        *static_cast<const NodeStringConstant *>(existing));
            case Node::Type::LocalizedString:
                if (static_cast<const NodeLocalizedString *>(existing)->canDemote()) {
                    return std::make_shared<NodeString>(
                            *static_cast<const NodeLocalizedString *>(existing));
                }
                return std::make_shared<NodeLocalizedString>(
                        *static_cast<const NodeLocalizedString *>(existing));
            case Node::Type::LocalizedStringConstant:
                if (static_cast<const NodeLocalizedStringConstant *>(existing)->canDemote()) {
                    return std::make_shared<NodeString>(
                            *static_cast<const NodeLocalizedStringConstant *>(existing));
                }
                return std::make_shared<NodeLocalizedString>(
                        *static_cast<const NodeLocalizedStringConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeString>();
    case Node::WriteGoal::LocalizedString:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::LocalizedString:
                return std::make_shared<NodeLocalizedString>(
                        *static_cast<const NodeLocalizedString *>(existing));
            case Node::Type::LocalizedStringConstant:
                return std::make_shared<NodeLocalizedString>(
                        *static_cast<const NodeLocalizedStringConstant *>(existing));
            case Node::Type::String:
                return std::make_shared<NodeLocalizedString>(
                        *static_cast<const NodeString *>(existing));
            case Node::Type::StringConstant:
                return std::make_shared<NodeLocalizedString>(
                        *static_cast<const NodeStringConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeLocalizedString>();
    case Node::WriteGoal::Bytes:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Bytes:
                return std::make_shared<NodeBytes>(*static_cast<const NodeBytes *>(existing));
            case Node::Type::BytesConstant:
                return std::make_shared<NodeBytes>(
                        *static_cast<const NodeBytesConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeBytes>();
    case Node::WriteGoal::Flags:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Flags:
                return std::make_shared<NodeFlags>(*static_cast<const NodeFlags *>(existing));
            case Node::Type::FlagsConstant:
                return std::make_shared<NodeFlags>(
                        *static_cast<const NodeFlagsConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeFlags>();
    case Node::WriteGoal::Hash:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Hash:
                return std::make_shared<NodeHash>(*static_cast<const NodeHash *>(existing));
            case Node::Type::HashConstant:
                return std::make_shared<NodeHash>(*static_cast<const NodeHashConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeHash>();
    case Node::WriteGoal::Array:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Array:
                return std::make_shared<NodeArray>(*static_cast<const NodeArray *>(existing));
            case Node::Type::ArrayConstant:
                return std::make_shared<NodeArray>(
                        *static_cast<const NodeArrayConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeArray>();
    case Node::WriteGoal::Matrix:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Matrix:
                return std::make_shared<NodeMatrix>(*static_cast<const NodeMatrix *>(existing));
            case Node::Type::MatrixConstant:
                return std::make_shared<NodeMatrix>(
                        *static_cast<const NodeMatrixConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMatrix>();
    case Node::WriteGoal::Keyframe:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Keyframe:
                return std::make_shared<NodeKeyframe>(*static_cast<const NodeKeyframe *>(existing));
            case Node::Type::KeyframeConstant:
                return std::make_shared<NodeKeyframe>(
                        *static_cast<const NodeKeyframeConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeKeyframe>();
    case Node::WriteGoal::MetadataReal:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataReal:
                return std::make_shared<NodeMetadataReal>(
                        *static_cast<const NodeMetadataReal *>(existing));
            case Node::Type::MetadataRealConstant:
                return std::make_shared<NodeMetadataReal>(
                        *static_cast<const NodeMetadataRealConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataReal>();
    case Node::WriteGoal::MetadataInteger:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataInteger:
                return std::make_shared<NodeMetadataInteger>(
                        *static_cast<const NodeMetadataInteger *>(existing));
            case Node::Type::MetadataIntegerConstant:
                return std::make_shared<NodeMetadataInteger>(
                        *static_cast<const NodeMetadataIntegerConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataInteger>();
    case Node::WriteGoal::MetadataBoolean:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataBoolean:
                return std::make_shared<NodeMetadataBoolean>(
                        *static_cast<const NodeMetadataBoolean *>(existing));
            case Node::Type::MetadataBooleanConstant:
                return std::make_shared<NodeMetadataBoolean>(
                        *static_cast<const NodeMetadataBooleanConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataBoolean>();
    case Node::WriteGoal::MetadataString:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataString:
                return std::make_shared<NodeMetadataString>(
                        *static_cast<const NodeMetadataString *>(existing));
            case Node::Type::MetadataStringConstant:
                return std::make_shared<NodeMetadataString>(
                        *static_cast<const NodeMetadataStringConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataString>();
    case Node::WriteGoal::MetadataBytes:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataBytes:
                return std::make_shared<NodeMetadataBytes>(
                        *static_cast<const NodeMetadataBytes *>(existing));
            case Node::Type::MetadataBytesConstant:
                return std::make_shared<NodeMetadataBytes>(
                        *static_cast<const NodeMetadataBytesConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataBytes>();
    case Node::WriteGoal::MetadataFlags:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataFlags:
                return std::make_shared<NodeMetadataFlags>(
                        *static_cast<const NodeMetadataFlags *>(existing));
            case Node::Type::MetadataFlagsConstant:
                return std::make_shared<NodeMetadataFlags>(
                        *static_cast<const NodeMetadataFlagsConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataFlags>();
    case Node::WriteGoal::MetadataHash:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataHash:
                return std::make_shared<NodeMetadataHash>(
                        *static_cast<const NodeMetadataHash *>(existing));
            case Node::Type::MetadataHashConstant:
                return std::make_shared<NodeMetadataHash>(
                        *static_cast<const NodeMetadataHashConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataHash>();
    case Node::WriteGoal::MetadataArray:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataArray:
                return std::make_shared<NodeMetadataArray>(
                        *static_cast<const NodeMetadataArray *>(existing));
            case Node::Type::MetadataArrayConstant:
                return std::make_shared<NodeMetadataArray>(
                        *static_cast<const NodeMetadataArrayConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataArray>();
    case Node::WriteGoal::MetadataMatrix:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataMatrix:
                return std::make_shared<NodeMetadataMatrix>(
                        *static_cast<const NodeMetadataMatrix *>(existing));
            case Node::Type::MetadataMatrixConstant:
                return std::make_shared<NodeMetadataMatrix>(
                        *static_cast<const NodeMetadataMatrixConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataMatrix>();
    case Node::WriteGoal::MetadataKeyframe:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataKeyframe:
                return std::make_shared<NodeMetadataKeyframe>(
                        *static_cast<const NodeMetadataKeyframe *>(existing));
            case Node::Type::MetadataKeyframeConstant:
                return std::make_shared<NodeMetadataKeyframe>(
                        *static_cast<const NodeMetadataKeyframeConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataKeyframe>();
    case Node::WriteGoal::Overlay:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Overlay:
                return std::make_shared<NodeOverlay>(*static_cast<const NodeOverlay *>(existing));
            case Node::Type::OverlayConstant:
                return std::make_shared<NodeOverlay>(
                        *static_cast<const NodeOverlayConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeOverlay>();
    }

    Q_ASSERT(false);
    return std::make_shared<NodeEmpty>();
}

std::shared_ptr<Node> Node::create(PathElement::Type next, const Node *existing)
{
    switch (next) {
    case PathElement::Type::Empty:
        return std::make_shared<NodeHash>();
    case PathElement::Type::AnyString:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Hash:
                return std::make_shared<NodeHash>(*static_cast<const NodeHash *>(existing));
            case Node::Type::HashConstant:
                return std::make_shared<NodeHash>(*static_cast<const NodeHashConstant *>(existing));
            case Node::Type::MetadataReal:
                return std::make_shared<NodeMetadataReal>(
                        *static_cast<const NodeMetadataReal *>(existing));
            case Node::Type::MetadataRealConstant:
                return std::make_shared<NodeMetadataReal>(
                        *static_cast<const NodeMetadataRealConstant *>(existing));
            case Node::Type::MetadataInteger:
                return std::make_shared<NodeMetadataInteger>(
                        *static_cast<const NodeMetadataInteger *>(existing));
            case Node::Type::MetadataIntegerConstant:
                return std::make_shared<NodeMetadataInteger>(
                        *static_cast<const NodeMetadataIntegerConstant *>(existing));
            case Node::Type::MetadataBoolean:
                return std::make_shared<NodeMetadataBoolean>(
                        *static_cast<const NodeMetadataBoolean *>(existing));
            case Node::Type::MetadataBooleanConstant:
                return std::make_shared<NodeMetadataBoolean>(
                        *static_cast<const NodeMetadataBooleanConstant *>(existing));
            case Node::Type::MetadataString:
                return std::make_shared<NodeMetadataString>(
                        *static_cast<const NodeMetadataString *>(existing));
            case Node::Type::MetadataStringConstant:
                return std::make_shared<NodeMetadataString>(
                        *static_cast<const NodeMetadataStringConstant *>(existing));
            case Node::Type::MetadataBytes:
                return std::make_shared<NodeMetadataBytes>(
                        *static_cast<const NodeMetadataBytes *>(existing));
            case Node::Type::MetadataBytesConstant:
                return std::make_shared<NodeMetadataBytes>(
                        *static_cast<const NodeMetadataBytesConstant *>(existing));
            case Node::Type::MetadataFlags:
                return std::make_shared<NodeMetadataFlags>(
                        *static_cast<const NodeMetadataFlags *>(existing));
            case Node::Type::MetadataFlagsConstant:
                return std::make_shared<NodeMetadataFlags>(
                        *static_cast<const NodeMetadataFlagsConstant *>(existing));
            case Node::Type::MetadataHash:
                return std::make_shared<NodeMetadataHash>(
                        *static_cast<const NodeMetadataHash *>(existing));
            case Node::Type::MetadataHashConstant:
                return std::make_shared<NodeMetadataHash>(
                        *static_cast<const NodeMetadataHashConstant *>(existing));
            case Node::Type::MetadataArray:
                return std::make_shared<NodeMetadataArray>(
                        *static_cast<const NodeMetadataArray *>(existing));
            case Node::Type::MetadataArrayConstant:
                return std::make_shared<NodeMetadataArray>(
                        *static_cast<const NodeMetadataArrayConstant *>(existing));
            case Node::Type::MetadataMatrix:
                return std::make_shared<NodeMetadataMatrix>(
                        *static_cast<const NodeMetadataMatrix *>(existing));
            case Node::Type::MetadataMatrixConstant:
                return std::make_shared<NodeMetadataMatrix>(
                        *static_cast<const NodeMetadataMatrixConstant *>(existing));
            case Node::Type::MetadataKeyframe:
                return std::make_shared<NodeMetadataKeyframe>(
                        *static_cast<const NodeMetadataKeyframe *>(existing));
            case Node::Type::MetadataKeyframeConstant:
                return std::make_shared<NodeMetadataKeyframe>(
                        *static_cast<const NodeMetadataKeyframeConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeHash>();
    case PathElement::Type::AnyMetadata:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataReal:
                return std::make_shared<NodeMetadataReal>(
                        *static_cast<const NodeMetadataReal *>(existing));
            case Node::Type::MetadataRealConstant:
                return std::make_shared<NodeMetadataReal>(
                        *static_cast<const NodeMetadataRealConstant *>(existing));
            case Node::Type::MetadataInteger:
                return std::make_shared<NodeMetadataInteger>(
                        *static_cast<const NodeMetadataInteger *>(existing));
            case Node::Type::MetadataIntegerConstant:
                return std::make_shared<NodeMetadataInteger>(
                        *static_cast<const NodeMetadataIntegerConstant *>(existing));
            case Node::Type::MetadataBoolean:
                return std::make_shared<NodeMetadataBoolean>(
                        *static_cast<const NodeMetadataBoolean *>(existing));
            case Node::Type::MetadataBooleanConstant:
                return std::make_shared<NodeMetadataBoolean>(
                        *static_cast<const NodeMetadataBooleanConstant *>(existing));
            case Node::Type::MetadataString:
                return std::make_shared<NodeMetadataString>(
                        *static_cast<const NodeMetadataString *>(existing));
            case Node::Type::MetadataStringConstant:
                return std::make_shared<NodeMetadataString>(
                        *static_cast<const NodeMetadataStringConstant *>(existing));
            case Node::Type::MetadataBytes:
                return std::make_shared<NodeMetadataBytes>(
                        *static_cast<const NodeMetadataBytes *>(existing));
            case Node::Type::MetadataBytesConstant:
                return std::make_shared<NodeMetadataBytes>(
                        *static_cast<const NodeMetadataBytesConstant *>(existing));
            case Node::Type::MetadataFlags:
                return std::make_shared<NodeMetadataFlags>(
                        *static_cast<const NodeMetadataFlags *>(existing));
            case Node::Type::MetadataFlagsConstant:
                return std::make_shared<NodeMetadataFlags>(
                        *static_cast<const NodeMetadataFlagsConstant *>(existing));
            case Node::Type::MetadataHash:
                return std::make_shared<NodeMetadataHash>(
                        *static_cast<const NodeMetadataHash *>(existing));
            case Node::Type::MetadataHashConstant:
                return std::make_shared<NodeMetadataHash>(
                        *static_cast<const NodeMetadataHashConstant *>(existing));
            case Node::Type::MetadataArray:
                return std::make_shared<NodeMetadataArray>(
                        *static_cast<const NodeMetadataArray *>(existing));
            case Node::Type::MetadataArrayConstant:
                return std::make_shared<NodeMetadataArray>(
                        *static_cast<const NodeMetadataArrayConstant *>(existing));
            case Node::Type::MetadataMatrix:
                return std::make_shared<NodeMetadataMatrix>(
                        *static_cast<const NodeMetadataMatrix *>(existing));
            case Node::Type::MetadataMatrixConstant:
                return std::make_shared<NodeMetadataMatrix>(
                        *static_cast<const NodeMetadataMatrixConstant *>(existing));
            case Node::Type::MetadataKeyframe:
                return std::make_shared<NodeMetadataKeyframe>(
                        *static_cast<const NodeMetadataKeyframe *>(existing));
            case Node::Type::MetadataKeyframeConstant:
                return std::make_shared<NodeMetadataKeyframe>(
                        *static_cast<const NodeMetadataKeyframeConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataReal>();
    case PathElement::Type::Hash:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Hash:
                return std::make_shared<NodeHash>(*static_cast<const NodeHash *>(existing));
            case Node::Type::HashConstant:
                return std::make_shared<NodeHash>(*static_cast<const NodeHashConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeHash>();
    case PathElement::Type::Array:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Array:
                return std::make_shared<NodeArray>(*static_cast<const NodeArray *>(existing));
            case Node::Type::ArrayConstant:
                return std::make_shared<NodeArray>(
                        *static_cast<const NodeArrayConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeArray>();
    case PathElement::Type::Matrix:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Matrix:
                return std::make_shared<NodeMatrix>(*static_cast<const NodeMatrix *>(existing));
            case Node::Type::MatrixConstant:
                return std::make_shared<NodeMatrix>(
                        *static_cast<const NodeMatrixConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMatrix>();
    case PathElement::Type::Keyframe:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::Keyframe:
                return std::make_shared<NodeKeyframe>(*static_cast<const NodeKeyframe *>(existing));
            case Node::Type::KeyframeConstant:
                return std::make_shared<NodeKeyframe>(
                        *static_cast<const NodeKeyframeConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeKeyframe>();
    case PathElement::Type::MetadataReal:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataReal:
                return std::make_shared<NodeMetadataReal>(
                        *static_cast<const NodeMetadataReal *>(existing));
            case Node::Type::MetadataRealConstant:
                return std::make_shared<NodeMetadataReal>(
                        *static_cast<const NodeMetadataRealConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataReal>();
    case PathElement::Type::MetadataInteger:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataInteger:
                return std::make_shared<NodeMetadataInteger>(
                        *static_cast<const NodeMetadataInteger *>(existing));
            case Node::Type::MetadataIntegerConstant:
                return std::make_shared<NodeMetadataInteger>(
                        *static_cast<const NodeMetadataIntegerConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataInteger>();
    case PathElement::Type::MetadataBoolean:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataBoolean:
                return std::make_shared<NodeMetadataBoolean>(
                        *static_cast<const NodeMetadataBoolean *>(existing));
            case Node::Type::MetadataBooleanConstant:
                return std::make_shared<NodeMetadataBoolean>(
                        *static_cast<const NodeMetadataBooleanConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataBoolean>();
    case PathElement::Type::MetadataString:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataString:
                return std::make_shared<NodeMetadataString>(
                        *static_cast<const NodeMetadataString *>(existing));
            case Node::Type::MetadataStringConstant:
                return std::make_shared<NodeMetadataString>(
                        *static_cast<const NodeMetadataStringConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataString>();
    case PathElement::Type::MetadataBytes:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataBytes:
                return std::make_shared<NodeMetadataBytes>(
                        *static_cast<const NodeMetadataBytes *>(existing));
            case Node::Type::MetadataBytesConstant:
                return std::make_shared<NodeMetadataBytes>(
                        *static_cast<const NodeMetadataBytesConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataBytes>();
    case PathElement::Type::MetadataFlags:
    case PathElement::Type::MetadataSingleFlag:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataFlags:
                return std::make_shared<NodeMetadataFlags>(
                        *static_cast<const NodeMetadataFlags *>(existing));
            case Node::Type::MetadataFlagsConstant:
                return std::make_shared<NodeMetadataFlags>(
                        *static_cast<const NodeMetadataFlagsConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataFlags>();
    case PathElement::Type::MetadataArray:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataArray:
                return std::make_shared<NodeMetadataArray>(
                        *static_cast<const NodeMetadataArray *>(existing));
            case Node::Type::MetadataArrayConstant:
                return std::make_shared<NodeMetadataArray>(
                        *static_cast<const NodeMetadataArrayConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataArray>();
    case PathElement::Type::MetadataMatrix:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataMatrix:
                return std::make_shared<NodeMetadataMatrix>(
                        *static_cast<const NodeMetadataMatrix *>(existing));
            case Node::Type::MetadataMatrixConstant:
                return std::make_shared<NodeMetadataMatrix>(
                        *static_cast<const NodeMetadataMatrixConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataMatrix>();
    case PathElement::Type::MetadataHash:
    case PathElement::Type::MetadataHashChild:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataHash:
                return std::make_shared<NodeMetadataHash>(
                        *static_cast<const NodeMetadataHash *>(existing));
            case Node::Type::MetadataHashConstant:
                return std::make_shared<NodeMetadataHash>(
                        *static_cast<const NodeMetadataHashConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataHash>();
    case PathElement::Type::MetadataKeyframe:
        if (existing) {
            switch (existing->type()) {
            case Node::Type::MetadataKeyframe:
                return std::make_shared<NodeMetadataKeyframe>(
                        *static_cast<const NodeMetadataKeyframe *>(existing));
            case Node::Type::MetadataKeyframeConstant:
                return std::make_shared<NodeMetadataKeyframe>(
                        *static_cast<const NodeMetadataKeyframeConstant *>(existing));
            default:
                break;
            }
        }
        return std::make_shared<NodeMetadataKeyframe>();
    }

    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

bool Node::bypassFirstOverlay(const Node &over) const
{
    switch (over.type()) {
    case Type::Empty:
    case Type::Real:
    case Type::RealConstant:
    case Type::Integer:
    case Type::IntegerConstant:
    case Type::Boolean:
    case Type::BooleanConstant:
    case Type::String:
    case Type::StringConstant:
    case Type::LocalizedString:
    case Type::LocalizedStringConstant:
    case Type::Bytes:
    case Type::BytesConstant:
    case Type::Flags:
    case Type::FlagsConstant:
        return true;
    default:
        return false;
    }
}

bool Node::bypassSecondOverlay() const
{ return true; }

bool Node::isOverlayOpaque() const
{ return true; }

bool Node::equalTo(const Node &other, const Node &metadata) const
{ return identicalTo(other); }

double Node::toReal() const
{ return FP::undefined(); }

std::int_fast64_t Node::toInteger() const
{ return INTEGER::undefined(); }

bool Node::toBoolean() const
{ return false; }

const std::string Node::invalidString;

const std::string &Node::toString() const
{ return invalidString; }

QString Node::toDisplayString(const QLocale &) const
{ return QString(); }

const Bytes Node::invalidBytes;

const Bytes &Node::toBytes() const
{ return invalidBytes; }

const Flags Node::invalidFlags;

const Flags &Node::toFlags() const
{ return invalidFlags; }


const std::shared_ptr<NodeEmpty> NodeEmpty::instance = std::make_shared<NodeEmpty>();

NodeEmpty::~NodeEmpty() = default;

NodeEmpty::NodeEmpty() noexcept = default;

NodeEmpty::NodeEmpty(const NodeEmpty &) noexcept = default;

Node::Type NodeEmpty::type() const
{ return Node::Type::Empty; }

void NodeEmpty::detach(std::shared_ptr<Node> &self) const
{ self = instance; }

bool NodeEmpty::compatible(Node::WriteGoal goal) const
{ return goal == Node::WriteGoal::Empty; }

std::string NodeEmpty::describe() const
{ return "Empty"; }

void NodeEmpty::serialize(QDataStream &stream) const
{ stream << static_cast<quint8>(Serialization::ID_Empty); }

void NodeEmpty::clear()
{ }

bool NodeEmpty::identicalTo(const Node &other) const
{
    switch (other.type()) {
    case Type::Empty:
        break;
    default:
        return false;
    }
    return true;
}

std::size_t NodeEmpty::hash() const
{ return Serialization::ID_Empty; }

}
}
}