/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "toplevel.hxx"
#include "serialization.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

TopLevel::TopLevel()
{ data.construct<NodeEmpty>(); }

TopLevel::~TopLevel()
{ data.destruct<Node>(); }

TopLevel::TopLevel(const TopLevel &other)
{ constructFrom(*other.node(), ConstructHandler(this)); }

TopLevel &TopLevel::operator=(const TopLevel &other)
{
    if (&other != this)
        constructFrom(*other.node(), AssignHandler(this));
    return *this;
}

TopLevel::TopLevel(const Node &other)
{ constructFrom(other, ConstructHandler(this)); }

TopLevel &TopLevel::operator=(const Node &other)
{
    if (&other != node())
        constructFrom(other, AssignHandler(this));
    return *this;
}

TopLevel::TopLevel(double value)
{ data.construct<NodeReal>(value); }

TopLevel &TopLevel::operator=(double value)
{
    set<NodeReal>(value);
    return *this;
}

TopLevel::TopLevel(std::int_fast64_t value)
{ data.construct<NodeInteger>(value); }

TopLevel &TopLevel::operator=(std::int_fast64_t value)
{
    set<NodeInteger>(value);
    return *this;
}

TopLevel::TopLevel(bool value)
{ data.construct<NodeBoolean>(value); }

TopLevel &TopLevel::operator=(bool value)
{
    set<NodeBoolean>(value);
    return *this;
}

TopLevel::TopLevel(const std::string &value)
{ data.construct<NodeString>(value); }

TopLevel::TopLevel(std::string &&value)
{ data.construct<NodeString>(std::move(value)); }

TopLevel &TopLevel::operator=(const std::string &value)
{
    set<NodeString>(value);
    return *this;
}

TopLevel &TopLevel::operator=(std::string &&value)
{
    set<NodeString>(std::move(value));
    return *this;
}

TopLevel::TopLevel(const Bytes &value)
{ data.construct<NodeBytes>(value); }

TopLevel::TopLevel(Bytes &&value)
{ data.construct<NodeBytes>(std::move(value)); }

TopLevel &TopLevel::operator=(const Bytes &value)
{
    set<NodeBytes>(value);
    return *this;
}

TopLevel &TopLevel::operator=(Bytes &&value)
{
    set<NodeBytes>(std::move(value));
    return *this;
}

TopLevel::TopLevel(const Flags &value)
{ data.construct<NodeFlags>(value); }

TopLevel::TopLevel(Flags &&value)
{ data.construct<NodeFlags>(std::move(value)); }

TopLevel &TopLevel::operator=(const Flags &value)
{
    set<NodeFlags>(value);
    return *this;
}

TopLevel &TopLevel::operator=(Flags &&value)
{
    set<NodeFlags>(std::move(value));
    return *this;
}

TopLevel::TopLevel(Node::WriteGoal type)
{
    switch (type) {
    case Node::WriteGoal::Empty:
        data.construct<NodeEmpty>();
        break;

    case Node::WriteGoal::Real:
        data.construct<NodeReal>();
        break;
    case Node::WriteGoal::Integer:
        data.construct<NodeInteger>();
        break;
    case Node::WriteGoal::Boolean:
        data.construct<NodeBoolean>();
        break;
    case Node::WriteGoal::String:
        data.construct<NodeString>();
        break;
    case Node::WriteGoal::LocalizedString:
        data.construct<NodeLocalizedString>();
        break;
    case Node::WriteGoal::Bytes:
        data.construct<NodeBytes>();
        break;
    case Node::WriteGoal::Flags:
        data.construct<NodeFlags>();
        break;

    case Node::WriteGoal::Hash:
        data.construct<NodeHash>();
        break;
    case Node::WriteGoal::Array:
        data.construct<NodeArray>();
        break;
    case Node::WriteGoal::Matrix:
        data.construct<NodeMatrix>();
        break;
    case Node::WriteGoal::Keyframe:
        data.construct<NodeKeyframe>();
        break;

    case Node::WriteGoal::MetadataReal:
        data.construct<NodeMetadataReal>();
        break;
    case Node::WriteGoal::MetadataInteger:
        data.construct<NodeMetadataInteger>();
        break;
    case Node::WriteGoal::MetadataBoolean:
        data.construct<NodeMetadataBoolean>();
        break;
    case Node::WriteGoal::MetadataString:
        data.construct<NodeMetadataString>();
        break;
    case Node::WriteGoal::MetadataBytes:
        data.construct<NodeMetadataBytes>();
        break;
    case Node::WriteGoal::MetadataFlags:
        data.construct<NodeMetadataFlags>();
        break;
    case Node::WriteGoal::MetadataHash:
        data.construct<NodeMetadataHash>();
        break;
    case Node::WriteGoal::MetadataArray:
        data.construct<NodeMetadataArray>();
        break;
    case Node::WriteGoal::MetadataMatrix:
        data.construct<NodeMetadataMatrix>();
        break;
    case Node::WriteGoal::MetadataKeyframe:
        data.construct<NodeMetadataKeyframe>();
        break;
    case Node::WriteGoal::Overlay:
        data.construct<NodeOverlay>();
        break;
    }
}

TopLevel::TopLevel(QDataStream &stream)
{ Serialization::deserialize(stream, ConstructHandler(this), false); }

std::shared_ptr<Node> TopLevel::read(const Path &path) const
{
    Q_ASSERT(!path.empty());

    auto apply = path.begin();
    auto result = node()->read(*apply);
    auto end = path.end();
    for (++apply; apply != end && result; ++apply) {
        result = result->read(*apply);
    }
    return std::move(result);
}

Node *TopLevel::write(Node::WriteGoal goal)
{
    if (node()->compatible(goal))
        return node();

    switch (goal) {
    case Node::WriteGoal::Empty:
        set<NodeEmpty>();
        break;
    case Node::WriteGoal::Real:
        Q_ASSERT(node()->type() != Node::Type::RealConstant);
        set<NodeReal>();
        break;
    case Node::WriteGoal::Integer:
        Q_ASSERT(node()->type() != Node::Type::IntegerConstant);
        set<NodeInteger>();
        break;
    case Node::WriteGoal::Boolean:
        Q_ASSERT(node()->type() != Node::Type::BooleanConstant);
        set<NodeBoolean>();
        break;
    case Node::WriteGoal::String:
        Q_ASSERT(node()->type() != Node::Type::StringConstant);
        Q_ASSERT(node()->type() != Node::Type::LocalizedString);
        Q_ASSERT(node()->type() != Node::Type::LocalizedStringConstant);
        set<NodeString>();
        break;
    case Node::WriteGoal::LocalizedString:
        Q_ASSERT(node()->type() != Node::Type::StringConstant);
        Q_ASSERT(node()->type() != Node::Type::LocalizedStringConstant);
        if (node()->type() == Node::Type::String) {
            NodeString copy = std::move(*static_cast<NodeString *>(node()));
            set<NodeLocalizedString>(std::move(copy));
            break;
        }
        set<NodeLocalizedString>();
        break;
    case Node::WriteGoal::Bytes:
        Q_ASSERT(node()->type() != Node::Type::BytesConstant);
        set<NodeBytes>();
        break;
    case Node::WriteGoal::Flags:
        Q_ASSERT(node()->type() != Node::Type::FlagsConstant);
        set<NodeFlags>();
        break;
    case Node::WriteGoal::Hash:
        Q_ASSERT(node()->type() != Node::Type::HashConstant);
        set<NodeHash>();
        break;
    case Node::WriteGoal::Array:
        Q_ASSERT(node()->type() != Node::Type::ArrayConstant);
        set<NodeArray>();
        break;
    case Node::WriteGoal::Matrix:
        Q_ASSERT(node()->type() != Node::Type::MatrixConstant);
        set<NodeMatrix>();
        break;
    case Node::WriteGoal::Keyframe:
        Q_ASSERT(node()->type() != Node::Type::KeyframeConstant);
        set<NodeKeyframe>();
        break;
    case Node::WriteGoal::MetadataReal:
        Q_ASSERT(node()->type() != Node::Type::MetadataRealConstant);
        set<NodeMetadataReal>();
        break;
    case Node::WriteGoal::MetadataInteger:
        Q_ASSERT(node()->type() != Node::Type::MetadataIntegerConstant);
        set<NodeMetadataInteger>();
        break;
    case Node::WriteGoal::MetadataBoolean:
        Q_ASSERT(node()->type() != Node::Type::MetadataBooleanConstant);
        set<NodeMetadataBoolean>();
        break;
    case Node::WriteGoal::MetadataString:
        Q_ASSERT(node()->type() != Node::Type::MetadataStringConstant);
        set<NodeMetadataString>();
        break;
    case Node::WriteGoal::MetadataBytes:
        Q_ASSERT(node()->type() != Node::Type::MetadataBytesConstant);
        set<NodeMetadataBytes>();
        break;
    case Node::WriteGoal::MetadataFlags:
        Q_ASSERT(node()->type() != Node::Type::MetadataFlagsConstant);
        set<NodeMetadataFlags>();
        break;
    case Node::WriteGoal::MetadataHash:
        Q_ASSERT(node()->type() != Node::Type::MetadataHashConstant);
        set<NodeMetadataHash>();
        break;
    case Node::WriteGoal::MetadataArray:
        Q_ASSERT(node()->type() != Node::Type::MetadataArrayConstant);
        set<NodeMetadataArray>();
        break;
    case Node::WriteGoal::MetadataMatrix:
        Q_ASSERT(node()->type() != Node::Type::MetadataMatrixConstant);
        set<NodeMetadataMatrix>();
        break;
    case Node::WriteGoal::MetadataKeyframe:
        Q_ASSERT(node()->type() != Node::Type::MetadataKeyframeConstant);
        set<NodeMetadataKeyframe>();
        break;
    case Node::WriteGoal::Overlay:
        Q_ASSERT(node()->type() != Node::Type::OverlayConstant);
        set<NodeOverlay>();
        break;
    }

    Q_ASSERT(node()->compatible(goal));

    return node();
}

void TopLevel::convertData(PathElement::Type access)
{
    if (node()->childrenWritable(access))
        return;

    switch (access) {
    case PathElement::Type::Empty:
        set<NodeHash>();
        break;
    case PathElement::Type::AnyString:
        set<NodeHash>();
        break;
    case PathElement::Type::AnyMetadata:
        set<NodeMetadataReal>();
        break;
    case PathElement::Type::Hash:
        Q_ASSERT(node()->type() != Node::Type::HashConstant);
        set<NodeHash>();
        break;
    case PathElement::Type::Array:
        Q_ASSERT(node()->type() != Node::Type::ArrayConstant);
        set<NodeArray>();
        break;
    case PathElement::Type::Matrix:
        Q_ASSERT(node()->type() != Node::Type::MatrixConstant);
        set<NodeMatrix>();
        break;
    case PathElement::Type::Keyframe:
        Q_ASSERT(node()->type() != Node::Type::KeyframeConstant);
        set<NodeKeyframe>();
        break;
    case PathElement::Type::MetadataReal:
        Q_ASSERT(node()->type() != Node::Type::MetadataRealConstant);
        set<NodeMetadataReal>();
        break;
    case PathElement::Type::MetadataInteger:
        Q_ASSERT(node()->type() != Node::Type::MetadataIntegerConstant);
        set<NodeMetadataInteger>();
        break;
    case PathElement::Type::MetadataBoolean:
        Q_ASSERT(node()->type() != Node::Type::MetadataBooleanConstant);
        set<NodeMetadataBoolean>();
        break;
    case PathElement::Type::MetadataString:
        Q_ASSERT(node()->type() != Node::Type::MetadataRealConstant);
        set<NodeMetadataString>();
        break;
    case PathElement::Type::MetadataBytes:
        Q_ASSERT(node()->type() != Node::Type::MetadataBytesConstant);
        set<NodeMetadataBytes>();
        break;
    case PathElement::Type::MetadataFlags:
    case PathElement::Type::MetadataSingleFlag:
        Q_ASSERT(node()->type() != Node::Type::MetadataFlagsConstant);
        set<NodeMetadataFlags>();
        break;
    case PathElement::Type::MetadataArray:
        Q_ASSERT(node()->type() != Node::Type::MetadataArrayConstant);
        set<NodeMetadataArray>();
        break;
    case PathElement::Type::MetadataMatrix:
        Q_ASSERT(node()->type() != Node::Type::MetadataMatrixConstant);
        set<NodeMetadataMatrix>();
        break;
    case PathElement::Type::MetadataHash:
    case PathElement::Type::MetadataHashChild:
        Q_ASSERT(node()->type() != Node::Type::MetadataHashConstant);
        set<NodeMetadataHash>();
        break;
    case PathElement::Type::MetadataKeyframe:
        Q_ASSERT(node()->type() != Node::Type::MetadataKeyframeConstant);
        set<NodeMetadataKeyframe>();
        break;
    }

    Q_ASSERT(node()->childrenWritable(access));
}

std::shared_ptr<Node> TopLevel::write(const Path &path, Node::WriteGoal goal)
{
    Q_ASSERT(!path.empty());

    auto apply = path.begin();
    convertData(apply->type);

    auto next = apply;
    ++next;
    auto end = path.end();
    if (next == end)
        return node()->write(*apply, goal);

    auto result = node()->write(*apply, next->type);
    for (apply = next, ++next;; apply = next, ++next) {
        Q_ASSERT(result);
        Q_ASSERT(result->childrenWritable(apply->type));

        if (next == end)
            return result->write(*apply, goal);
        result = result->write(*apply, next->type);
    }

    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> TopLevel::detached(const Path &path) const
{
    std::shared_ptr<Node> copy;
    if (path.empty()) {
        node()->detach(copy);
        /* Since we can't store constant data, we know this will
         * always make a copy of the data */
    } else {
        copy = read(path);
        if (!copy)
            copy = NodeEmpty::instance;
        else
            copy->detach(copy);
    }
    Q_ASSERT(copy);
    return std::move(copy);
}

void TopLevel::assign(const Path &path, const std::shared_ptr<Node> &other)
{
    Q_ASSERT(!path.empty());

    auto apply = path.begin();
    convertData(apply->type);

    auto next = apply;
    ++next;
    auto end = path.end();
    if (next == end)
        return node()->insert(*apply, other);

    auto result = node()->write(*apply, next->type);
    for (apply = next, ++next;; apply = next, ++next) {
        Q_ASSERT(result);
        Q_ASSERT(result->childrenWritable(apply->type));

        if (next == end)
            return result->insert(*apply, other);
        result = result->write(*apply, next->type);
    }

    Q_ASSERT(false);
}

void TopLevel::remove(const Path &path)
{
    Q_ASSERT(!path.empty());

    auto apply = path.begin();
    convertData(apply->type);

    auto next = apply;
    ++next;
    auto end = path.end();
    if (next == end) {
        node()->remove(*apply);
        return;
    }

    auto result = node()->write(*apply, next->type);
    for (apply = next, ++next;; apply = next, ++next) {
        Q_ASSERT(result);
        Q_ASSERT(result->childrenWritable(apply->type));

        if (next == end) {
            result->remove(*apply);
            return;
        }
        result = result->write(*apply, next->type);
    }
}


}
}
}
