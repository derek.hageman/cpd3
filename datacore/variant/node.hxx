/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_NODE_HXX
#define CPD3DATACOREVARIANT_NODE_HXX

#include "core/first.hxx"

#include <memory>
#include <cstdint>
#include <string>
#include <QString>

#include "datacore/datacore.hxx"
#include "path.hxx"
#include "common.hxx"

class QDataStream;

class QLocale;

namespace CPD3 {
namespace Data {
namespace Variant {

class PathElement;

/**
 * The base node type for all elements of the variant tree.
 */
class CPD3DATACORE_EXPORT Node {
public:
    virtual ~Node();

    Node() noexcept;

    Node(const Node &) noexcept;


    /**
     * The unique type identifier of the node.  Primarily used
     * for type checking and casting.
     */
    enum class Type {
        Empty,

        Real,
        RealConstant,
        Integer,
        IntegerConstant,
        Boolean,
        BooleanConstant,
        String,
        StringConstant,
        LocalizedString,
        LocalizedStringConstant,
        Bytes,
        BytesConstant,
        Flags,
        FlagsConstant,

        Hash,
        HashConstant,
        Array,
        ArrayConstant,
        Matrix,
        MatrixConstant,
        Keyframe,
        KeyframeConstant,

        MetadataReal,
        MetadataRealConstant,
        MetadataInteger,
        MetadataIntegerConstant,
        MetadataBoolean,
        MetadataBooleanConstant,
        MetadataString,
        MetadataStringConstant,
        MetadataBytes,
        MetadataBytesConstant,
        MetadataFlags,
        MetadataFlagsConstant,
        MetadataHash,
        MetadataHashConstant,
        MetadataArray,
        MetadataArrayConstant,
        MetadataMatrix,
        MetadataMatrixConstant,
        MetadataKeyframe,
        MetadataKeyframeConstant,

        Overlay,
        OverlayConstant,
    };

    /**
     * Get the unique type identifier of the node.
     * This is used when an explict cast is required.
     *
     * @return  the node type identifier
     */
    virtual Type type() const = 0;

    /**
     * Serialize the contents of the node to a data stream.
     *
     * @param stream    the target stream
     */
    virtual void serialize(QDataStream &stream) const = 0;

    /**
     * Modify a pointer to the node to detach it if required.
     * This is a no-op for non-modifiable types, but the
     * modifiable ones set the pointer to a non-modifiable
     * copy.  Note that the pointer may be the only remaining
     * reference, so no access to the node may be performed
     * if it is changed (i.e. this may be deleted).  The
     * copy should be made from *this, because the pointer
     * may be null (for cloning the top level).
     *
     * @param self  the output pointer, modified if required
     */
    virtual void detach(std::shared_ptr<Node> &self) const = 0;

    /**
     * Get a child element of the node for reading.
     * <br>
     * The default implementation always returns null.
     *
     * @param path  the path to lookup in the node
     * @return      the child element or null when it does not exist
     */
    virtual std::shared_ptr<Node> read(const PathElement &path) const;

    /**
     * The types of write targets available.  These map the the
     * non-constant types that can be instantiated.
     */
    enum class WriteGoal {
        Empty,

        Real,
        Integer,
        Boolean,
        String,
        LocalizedString,
        Bytes,
        Flags,

        Hash,
        Array,
        Matrix,
        Keyframe,

        MetadataReal,
        MetadataInteger,
        MetadataBoolean,
        MetadataString,
        MetadataBytes,
        MetadataFlags,
        MetadataHash,
        MetadataArray,
        MetadataMatrix,
        MetadataKeyframe,

        Overlay,
    };

    /**
     * Convert a generic type identifier to a write goal type.
     *
     * @param type      the generic type identifier
     * @return          the write goal type
     */
    static WriteGoal goalFromGenericType(CPD3::Data::Variant::Type type);

    /**
     * Convert a node type into the goal type used to access it.
     *
     * @param type      the node type
     * @return          the write goal type
     */
    static WriteGoal goalFromNodeType(Type type);

    /**
     * Test if a node is compatible with a write type.
     *
     * @param goal      the write goal
     * @return          true if the node can be written as the specified type
     */
    virtual bool compatible(WriteGoal goal) const;

    /**
     * Test if a node has children writable with a specified access
     * type.  This is used by the parent node to see if the node
     * needs to be replaced before further access can be performed
     *
     * @param type      the access type
     * @return          true if the node can write children with the type
     */
    virtual bool childrenWritable(PathElement::Type type) const;

    /**
     * Access a child node for writing.  The node itself will already
     * have been converted to an access type suitable for the path,
     * but this should alter the child, if required.
     *
     * @param path      the path in the node to access
     * @param final     the final write goal for the child
     * @return          the child element
     */
    virtual std::shared_ptr<Node> write(const PathElement &path, WriteGoal final);

    /**
     * Access a child for writing.  The node itself will already
     * have been converted to an access type suitable for the path,
     * but this should alter the child, if required.
     *
     * @param path      the path in the node to access
     * @param next      the next access type to determine the type the child must be
     * @return          the child element
     */
    virtual std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next);

    /**
     * Insert a child node, directly replacing any existing contents.
     * The node itself will already have been converted to an access type
     * suitable for the path, so this should simply replace or insert
     * the child.
     *
     * @param path      the path in the node to access
     * @param final     the the value to set it to
     */
    virtual void insert(const PathElement &path, const std::shared_ptr<Node> &final);

    /**
     * Remove the specified child from the node.  The node itself will already
     * have been converted to an access type suitable for the path,
     * but this should alter the child, if required.
     *
     * @param path    the child to remove
     */
    virtual void remove(const PathElement &path);

    /**
     * Create a node of the specified type.  This can
     * be used when a write access needs to create a new child
     * type (i.e. it is not compatible).
     *
     * @param final     the final write goal
     * @param existing  the existing node to copy from if any
     * @return          a new node instance
     */
    static std::shared_ptr<Node> create(WriteGoal final, const Node *existing = nullptr);

    /**
     * Create a node of the specified type.  This can
     * be used when a write access needs to create a new child
     * type (i.e. it is not compatible).
     *
     * @param next      the next path access type
     * @param existing  the existing node to copy from if any
     * @return          a new node instance
     */
    static std::shared_ptr<Node> create(PathElement::Type next, const Node *existing = nullptr);


    /**
     * Test if it is safe for the node to bypass the first overlay step and
     * use a direct replacement with the overlaid node.  This is true
     * if the overlay is opaque and it and all children can bypass
     * the second overlay step.
     *
     * @param over      the node being overlaid
     * @return          true if the first overlay can be bypassed
     */
    virtual bool bypassFirstOverlay(const Node &over) const;

    /**
     * Test if it is safe for the node to bypass the second overlay step.
     * This is true if all child nodes can bypass the step and the
     * node itself is not an overlay point.
     *
     * @return          true if the second overlay can be bypassed
     */
    virtual bool bypassSecondOverlay() const;


    /**
     * Generate a description of the node.
     *
     * @return  a description of the node
     */
    virtual std::string describe() const = 0;

    /**
     * Perform a logical "clear" on the node.  This will remove
     * or invalidate any data it stores and remove any
     * children.
     */
    virtual void clear() = 0;

    /**
     * Test if the node is considered opaque for overlaying.
     * This means that the value will always replace the one
     * below it in the even of an overlay, hiding the lower
     * one entirely.
     *
     * @return      true if the node is opaque when overlaying
     */
    virtual bool isOverlayOpaque() const;

    /**
     * Test if the node is exactly identical to another one.
     *
     * @param other the other node to compare with
     * @return      true if the nodes are equal
     */
    virtual bool identicalTo(const Node &other) const = 0;

    /**
     * Test if the node is equal to another one, considering
     * metadata.
     *
     * @param other     the node to compare with
     * @param metadata  the metadata in effect
     * @return          true if the nodes are equal in light of the metadata
     */
    virtual bool equalTo(const Node &other, const Node &metadata) const;

    /**
     * Get the hash value of the node.  The hash value of
     * two identical nodes must be the same.
     *
     * @return      the hashed value of the node
     */
    virtual std::size_t hash() const = 0;

    /**
     * Get the real number value of the node.
     *
     * @return  the real number value
     */
    virtual double toReal() const;

    /**
     * Get the integer value of the node.
     *
     * @return  the integer value
     */
    virtual std::int_fast64_t toInteger() const;

    /**
     * Get the boolean value of the node.
     *
     * @return  the boolean value
     */
    virtual bool toBoolean() const;

    /**
     * A constant empty string, used as the return
     * value when a node is not string convertible.
     */
    static const std::string invalidString;

    /**
     * Get the simple string value of the node.
     *
     * @return  the string value
     */
    virtual const std::string &toString() const;

    /**
     * Get the display string of the node, this is
     * a localized version of the string value.
     *
     * @param locale    the locale to query for
     * @return          the localized string
     */
    virtual QString toDisplayString(const QLocale &locale) const;

    /**
     * A constant empty byte data, used as the return
     * value when a node is not bytes convertible.
     */
    static const Bytes invalidBytes;

    /**
     * Get the byte value of the node.
     *
     * @return  the byte value
     */
    virtual const Bytes &toBytes() const;

    /**
     * A constant empty flags, used as the return
     * value when a node is not flags convertible.
     */
    static const Flags invalidFlags;

    /**
     * Get the flags value of the node.
     *
     * @return  the flags value
     */
    virtual const Flags &toFlags() const;
};

/**
 * An empty and invalid node.
 */
class CPD3DATACORE_EXPORT NodeEmpty : public Node {
public:
    /**
     * All empty nodes are immutable, so a single static
     * instance may be used for all of them.
     */
    static const std::shared_ptr<NodeEmpty> instance;

    virtual ~NodeEmpty();

    NodeEmpty() noexcept;

    NodeEmpty(const NodeEmpty &) noexcept;

    Type type() const override;

    void serialize(QDataStream &stream) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    std::string describe() const override;

    void clear() override;

    bool identicalTo(const Node &other) const override;

    std::size_t hash() const override;
};

}
}
}

#endif //CPD3DATACOREVARIANT_NODE_HXX
