/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_TOPLEVEL_HXX
#define CPD3DATACOREVARIANT_TOPLEVEL_HXX

#include "core/first.hxx"

#include <cstdint>
#include <algorithm>

#include "datacore/datacore.hxx"
#include "core/util.hxx"
#include "node.hxx"
#include "primitive.hxx"
#include "strings.hxx"
#include "overlay.hxx"
#include "hash.hxx"
#include "array.hxx"
#include "matrix.hxx"
#include "keyframe.hxx"
#include "metadata.hxx"
#include "path.hxx"
#include "common.hxx"

class QDataStream;

namespace CPD3 {
namespace Data {
namespace Variant {

/**
 * The internal storage for the top level root node of the variant
 * tree.  This provides a fixed size point that can store
 * any node type.
 */
class CPD3DATACORE_EXPORT TopLevel final {
    Util::ExplicitUnion<Node, NodeEmpty,

                        NodeReal, NodeInteger, NodeBoolean, NodeString, NodeLocalizedString,
                        NodeBytes, NodeFlags,

                        NodeArray, NodeMatrix, NodeKeyframe, NodeHash,

                        NodeMetadataReal, NodeMetadataInteger, NodeMetadataBoolean,
                        NodeMetadataString, NodeMetadataBytes, NodeMetadataFlags, NodeMetadataHash,
                        NodeMetadataArray, NodeMetadataMatrix, NodeMetadataKeyframe, NodeOverlay>
            data;

    struct ConstructHandler {
        TopLevel *top;

        inline ConstructHandler(TopLevel *top) : top(top)
        { }

        template<typename T, typename... Args>
        void construct(Args &&... args) const
        { top->data.construct<T>(std::forward<Args>(args)...); }
    };

    friend struct ConstructHandler;

    struct AssignHandler {
        TopLevel *top;

        inline AssignHandler(TopLevel *top) : top(top)
        { }

        template<typename T, typename... Args>
        void construct(Args &&... args) const
        { top->set<T>(std::forward<Args>(args)...); }
    };

    friend struct AssignHandler;

    template<typename Operator>
    static auto constructFrom(const Node &other, Operator op) -> decltype(op.template construct<
            NodeEmpty>(static_cast<const NodeEmpty &>(other)))
    {
        switch (other.type()) {
        case Node::Type::Empty:
            return op.template construct<NodeEmpty>(static_cast<const NodeEmpty &>(other));

        case Node::Type::Real:
        case Node::Type::RealConstant:
            return op.template construct<NodeReal>(static_cast<const NodeReal &>(other));
        case Node::Type::Integer:
        case Node::Type::IntegerConstant:
            return op.template construct<NodeInteger>(static_cast<const NodeInteger &>(other));
        case Node::Type::Boolean:
        case Node::Type::BooleanConstant:
            return op.template construct<NodeBoolean>(static_cast<const NodeBoolean &>(other));
        case Node::Type::String:
        case Node::Type::StringConstant:
            return op.template construct<NodeString>(static_cast<const NodeString &>(other));
        case Node::Type::LocalizedString:
        case Node::Type::LocalizedStringConstant:
            if (static_cast<const NodeLocalizedString &>(other).canDemote()) {
                return op.template construct<NodeString>(
                        static_cast<const NodeLocalizedString &>(other));
            } else {
                return op.template construct<NodeLocalizedString>(
                        static_cast<const NodeLocalizedString &>(other));
            }
        case Node::Type::Bytes:
        case Node::Type::BytesConstant:
            return op.template construct<NodeBytes>(static_cast<const NodeBytes &>(other));
        case Node::Type::Flags:
        case Node::Type::FlagsConstant:
            return op.template construct<NodeFlags>(static_cast<const NodeFlags &>(other));
        case Node::Type::Hash:
        case Node::Type::HashConstant:
            return op.template construct<NodeHash>(static_cast<const NodeHash &>(other));
        case Node::Type::Array:
        case Node::Type::ArrayConstant:
            return op.template construct<NodeArray>(static_cast<const NodeArray &>(other));
        case Node::Type::Matrix:
        case Node::Type::MatrixConstant:
            return op.template construct<NodeMatrix>(static_cast<const NodeMatrix &>(other));
        case Node::Type::Keyframe:
        case Node::Type::KeyframeConstant:
            return op.template construct<NodeKeyframe>(static_cast<const NodeKeyframe &>(other));

        case Node::Type::MetadataReal:
        case Node::Type::MetadataRealConstant:
            return op.template construct<NodeMetadataReal>(
                    static_cast<const NodeMetadataReal &>(other));
        case Node::Type::MetadataInteger:
        case Node::Type::MetadataIntegerConstant:
            return op.template construct<NodeMetadataInteger>(
                    static_cast<const NodeMetadataInteger &>(other));
        case Node::Type::MetadataBoolean:
        case Node::Type::MetadataBooleanConstant:
            return op.template construct<NodeMetadataBoolean>(
                    static_cast<const NodeMetadataBoolean &>(other));
        case Node::Type::MetadataString:
        case Node::Type::MetadataStringConstant:
            return op.template construct<NodeMetadataString>(
                    static_cast<const NodeMetadataString &>(other));
        case Node::Type::MetadataBytes:
        case Node::Type::MetadataBytesConstant:
            return op.template construct<NodeMetadataBytes>(
                    static_cast<const NodeMetadataBytes &>(other));
        case Node::Type::MetadataFlags:
        case Node::Type::MetadataFlagsConstant:
            return op.template construct<NodeMetadataFlags>(
                    static_cast<const NodeMetadataFlags &>(other));
        case Node::Type::MetadataHash:
        case Node::Type::MetadataHashConstant:
            return op.template construct<NodeMetadataHash>(
                    static_cast<const NodeMetadataHash &>(other));
        case Node::Type::MetadataArray:
        case Node::Type::MetadataArrayConstant:
            return op.template construct<NodeMetadataArray>(
                    static_cast<const NodeMetadataArray &>(other));
        case Node::Type::MetadataMatrix:
        case Node::Type::MetadataMatrixConstant:
            return op.template construct<NodeMetadataMatrix>(
                    static_cast<const NodeMetadataMatrix &>(other));
        case Node::Type::MetadataKeyframe:
        case Node::Type::MetadataKeyframeConstant:
            return op.template construct<NodeMetadataKeyframe>(
                    static_cast<const NodeMetadataKeyframe &>(other));

        case Node::Type::Overlay:
        case Node::Type::OverlayConstant:
            return op.template construct<NodeOverlay>(static_cast<const NodeOverlay &>(other));
        }

        Q_ASSERT(false);
        return op.template construct<NodeEmpty>(static_cast<const NodeEmpty &>(other));
    }

    void convertData(PathElement::Type access);

public:
    TopLevel();

    ~TopLevel();

    TopLevel(const TopLevel &other);

    TopLevel &operator=(const TopLevel &other);

    explicit TopLevel(Node::WriteGoal type);

    explicit TopLevel(const Node &other);

    TopLevel &operator=(const Node &other);

    explicit TopLevel(QDataStream &stream);

    explicit TopLevel(double value);

    TopLevel &operator=(double value);

    explicit TopLevel(std::int_fast64_t value);

    TopLevel &operator=(std::int_fast64_t value);

    explicit TopLevel(bool value);

    TopLevel &operator=(bool value);

    explicit TopLevel(const std::string &value);

    explicit TopLevel(std::string &&value);

    TopLevel &operator=(const std::string &value);

    TopLevel &operator=(std::string &&value);

    explicit TopLevel(const Bytes &value);

    explicit TopLevel(Bytes &&value);

    TopLevel &operator=(const Bytes &value);

    TopLevel &operator=(Bytes &&value);

    explicit TopLevel(const Flags &value);

    explicit TopLevel(Flags &&value);

    TopLevel &operator=(const Flags &value);

    TopLevel &operator=(Flags &&value);

    /**
     * Get the top level node data.
     *
     * @tparam T    the type to cast to
     * @return      the top level node
     */
    template<typename T = Node>
    inline const T *node() const
    { return data.ptr<T>(); }

    /**
     * Get the top level node data.
     *
     * @tparam T    the type to cast to
     * @return      the top level node
     */
    template<typename T = Node>
    inline T *node()
    { return data.ptr<T>(); }

    /**
     * Construct the top level node with the specified
     * arguments.
     *
     * @tparam T        the type to construct
     * @tparam Args     the arguments types
     * @param args      the argument data
     */
    template<typename T, typename... Args>
    void set(Args &&... args)
    {
        data.destruct<Node>();
        data.construct<T>(std::forward<Args>(args)...);
    }

    /**
     * Get a read path from the top level node.  The path must not
     * be empty (use node() for that case).
     *
     * @param path      the path to read from
     * @return          a read node or empty if not accessible
     */
    std::shared_ptr<Node> read(const Path &path) const;

    /**
     * Make the top level writable for the specified goal.
     *
     * @param goal      the write type
     * @return          the top level node pointer
     */
    Node *write(Node::WriteGoal goal);

    /**
     * Get a write path from the top level node.  The
     * path must not be empty (use write(Node::WriteGoal)
     * for that case).
     *
     * @param path      the path to write to
     * @param goal      the final write type
     * @return          a writable node of the goal type
     */
    std::shared_ptr<Node> write(const Path &path, Node::WriteGoal goal);

    /**
     * Get a detached instance of the node at the specified path,
     * which may be empty.  This is usually used with assigment.
     *
     * @param path      the path to read from
     * @return          a detached node
     */
    std::shared_ptr<Node> detached(const Path &path) const;

    /**
     * Create a path and assign it to the contents to a node value.
     * The path must not be empty (use the equal
     * operator for that case).
     *
     * @param path      the path to assign at
     * @param other     the source to assign to
     */
    void assign(const Path &path, const std::shared_ptr<Node> &other);

    /**
     * Remove the node at the specified path.  This will
     * also create the parents that would be required to access
     * it.
     *
     * @param path              the path
     */
    void remove(const Path &path);
};

}
}
}

#endif //CPD3DATACOREVARIANT_TOPLEVEL_HXX
