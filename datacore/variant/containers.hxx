/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_CONTAINERS_HXX
#define CPD3DATACOREVARIANT_CONTAINERS_HXX

#include "core/first.hxx"

#include <utility>
#include <memory>
#include <string>
#include <iterator>
#include <unordered_map>
#include <unordered_set>
#include <initializer_list>
#include <type_traits>
#include <QString>

#include "datacore/datacore.hxx"
#include "path.hxx"
#include "common.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

class TopLevel;

class Read;

class Write;

class Node;

namespace Container {

/**
 * A writable access to a variant handle as a flags.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteFlags final {
public:
    using backend_set = Flags;
    using key_type = typename backend_set::key_type;
    using value_type = typename backend_set::value_type;
    using size_type = typename backend_set::size_type;
    using difference_type = typename backend_set::difference_type;
    using pointer = const value_type *;
    using const_pointer = const value_type *;
    using reference = const value_type &;
    using const_reference = const value_type &;
    using iterator = typename backend_set::iterator;
    using const_iterator = typename backend_set::const_iterator;

private:
    std::shared_ptr<TopLevel> top;
    Path path;

    backend_set &backend();

    const backend_set &backend() const;

public:
    WriteFlags();

    ~WriteFlags();

    WriteFlags(const WriteFlags &);

    WriteFlags &operator=(const WriteFlags &);

    WriteFlags(WriteFlags &&);

    WriteFlags &operator=(WriteFlags &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteFlags(Write &read);

    /**
     * Swap two handles.
     *
     * @param a     the first handle
     * @param b     the second handle
     */
    static void swap(WriteFlags &a, WriteFlags &b);


    /**
     * Test if the container is empty.
     *
     * @return      true if the container is empty
     */
    bool empty() const;

    /**
     * Get the total size of the container.
     *
     * @return      the number of elements in the container
     */
    size_type size() const;

    /**
     * Get the number of elements that match a key.
     *
     * @param key   the key to find
     * @return      the number of matching elements
     */
    size_type count(const key_type &key) const;

    /** @see count(const key_type &) const */
    inline size_type count(const char *key) const
    { return count(key_type(key)); }

    /** @see count(const key_type &) const */
    inline size_type count(const QString &key) const
    { return count(key.toStdString()); }

    bool operator==(const WriteFlags &other) const;

    bool operator!=(const WriteFlags &other) const;

    bool operator==(const Flags &other) const;

    bool operator!=(const Flags &other) const;

    operator const Flags &() const;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    const_iterator begin() const;

    /** @see begin() const */
    inline const_iterator cbegin() const
    { return begin(); }

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    const_iterator end() const;

    /** @see end() const */
    inline const_iterator cend() const
    { return end(); }


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    const_iterator find(const key_type &key) const;

    /** @see find(const key_type &) const */
    inline const_iterator find(const char *key) const
    { return find(key_type(key)); }

    /** @see find(const key_type &) const */
    inline const_iterator find(const QString &key) const
    { return find(key.toStdString()); }


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(const char *value)
    { return insert(value_type(value)); };

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(const QString &value)
    { return insert(value.toStdString()); };

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @return          a pair of the inserted iterator and a boolean if the insert was performed
     */
    inline std::pair<iterator, bool> emplace(const key_type &key)
    { return insert(key); };

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(key_type &&key)
    { return insert(std::move(key)); };

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const char *key)
    { return emplace(key_type(key)); }

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const QString &key)
    { return emplace(key.toStdString()); }

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const WriteFlags &other);

    /** @see merge(const WriteFlags &) */
    void merge(const Flags &other);

    /** @see merge(const Flags &) */
    void merge(Flags &&other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(iterator pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(iterator first, iterator last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(const key_type &key);

private:
    bool requiresIteratorDetach() const;
};

CPD3DATACORE_EXPORT inline void swap(WriteFlags &lhs, WriteFlags &rhs)
{ WriteFlags::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(WriteFlags &lhs, WriteFlags &rhs)
{ WriteFlags::swap(lhs, rhs); }


class WriteHash;

/**
 * A read only access to a variant handle as a hash map.  This roughly
 * follows the STL associative container API.
 */
class CPD3DATACORE_EXPORT ReadHash {
public:
    using backend_map = std::unordered_map<PathElement::HashIndex, std::shared_ptr<Node>>;
    using key_type = typename backend_map::key_type;
    using mapped_type = Read;
    using value_type = std::pair<const key_type, mapped_type>;
    using size_type = typename backend_map::size_type;
    using difference_type = typename backend_map::difference_type;
    using pointer = const value_type *;
    using const_pointer = const value_type *;
    using reference = const value_type &;
    using const_reference = const value_type &;

private:
    std::shared_ptr<TopLevel> top;
    Path path;

    friend class WriteHash;

    const backend_map &backend() const;

public:
    ReadHash();

    ~ReadHash();

    ReadHash(const ReadHash &);

    ReadHash &operator=(const ReadHash &);

    ReadHash(ReadHash &&);

    ReadHash &operator=(ReadHash &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     */
    explicit ReadHash(const Read &read);

    /**
     * Swap two handles.
     *
     * @param a     the first handle
     * @param b     the second handle
     */
    static void swap(ReadHash &a, ReadHash &b);


    /**
     * Test if the container is empty.
     *
     * @return      true if the container is empty
     */
    bool empty() const;

    /**
     * Get the total size of the container.
     *
     * @return      the number of elements in the container
     */
    size_type size() const;

    /**
     * Get the number of elements that match a key.
     *
     * @param key   the key to find
     * @return      the number of matching elements
     */
    size_type count(const key_type &key) const;

    /** @see count(const key_type &) const */
    inline size_type count(const char *key) const
    { return count(key_type(key)); }

    /** @see count(const key_type &) const */
    inline size_type count(const QString &key) const
    { return count(key.toStdString()); }

    bool operator==(const ReadHash &other) const;

    bool operator!=(const ReadHash &other) const;


    mapped_type operator[](const key_type &key) const;

    mapped_type operator[](key_type &&key) const;

    mapped_type operator[](const char *key) const;

    mapped_type operator[](const QString &key) const;


    /**
     * A read only iterator.
     */
    class CPD3DATACORE_EXPORT const_iterator final {
    public:
        using backing_iterator = typename backend_map::const_iterator;
        using difference_type = ReadHash::difference_type;
        using value_type = ReadHash::value_type;
        using pointer = const std::pair<ReadHash::key_type, ReadHash::mapped_type> *;
        using reference = ReadHash::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class ReadHash;

        const ReadHash *parent;
        backing_iterator i;

    public:
        /**
         * Construct an iterator.
         *
         * @param parent    the parent container
         * @param i         the iterator location
         */
        const_iterator(const ReadHash &parent, backing_iterator &&i);

        const_iterator();

        ~const_iterator();

        const_iterator(const const_iterator &);

        const_iterator &operator=(const const_iterator &);

        const_iterator(const_iterator &&);

        const_iterator &operator=(const_iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        const_iterator &operator++();

        const_iterator operator++(int);

        bool operator==(const const_iterator &other) const;

        bool operator!=(const const_iterator &other) const;

        /**
         * Get the backing iterator in used.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        ReadHash::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class const_iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    const_iterator begin() const;

    /** @see begin() const */
    inline const_iterator cbegin() const
    { return begin(); }

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    const_iterator end() const;

    /** @see end() const */
    inline const_iterator cend() const
    { return end(); }


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    const_iterator find(const key_type &key) const;

    /** @see find(const key_type &) const */
    inline const_iterator find(const char *key) const
    { return find(key_type(key)); }

    /** @see find(const key_type &) const */
    inline const_iterator find(const QString &key) const
    { return find(key.toStdString()); }


    /**
     * Get all unique keys in the container.
     *
     * @return      the set of unique keys
     */
    std::unordered_set<key_type> keys() const;
};

CPD3DATACORE_EXPORT inline void swap(ReadHash &lhs, ReadHash &rhs)
{ ReadHash::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(ReadHash &lhs, ReadHash &rhs)
{ ReadHash::swap(lhs, rhs); }


/**
 * A writable access to a variant handle as a hash map.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteHash final : public ReadHash {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_map &backend();

public:
    WriteHash();

    ~WriteHash();

    WriteHash(const WriteHash &);

    WriteHash &operator=(const WriteHash &);

    WriteHash(WriteHash &&);

    WriteHash &operator=(WriteHash &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteHash(Write &write);


    mapped_type operator[](const key_type &key);

    mapped_type operator[](key_type &&key);

    mapped_type operator[](const char *key);

    mapped_type operator[](const QString &key);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using backing_iterator = typename backend_map::iterator;
        using difference_type = WriteHash::difference_type;
        using value_type = WriteHash::value_type;
        using pointer = const std::pair<WriteHash::key_type, WriteHash::mapped_type> *;
        using reference = WriteHash::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class WriteHash;

        WriteHash *parent;
        backing_iterator i;

    public:
        /**
         * Create the iterator.
         *
         * @param parent    the parent container
         * @param i         the backing iterator
         */
        iterator(WriteHash &parent, backing_iterator &&i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        iterator &operator++();

        iterator operator++(int);

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        /**
         * Get the backing iterator.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        bool operator==(const ReadHash::const_iterator &other) const;

        bool operator!=(const ReadHash::const_iterator &other) const;

        operator ReadHash::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteHash::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    iterator find(const key_type &key);

    /** @see find(const key_type &) */
    inline iterator find(const char *key)
    { return find(key_type(key)); }

    /** @see find(const key_type &) */
    inline iterator find(const QString &key)
    { return find(key.toStdString()); }


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @param value     the value to insert
     * @return          a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> emplace(const key_type &key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    std::pair<iterator, bool> emplace(key_type &&key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const char *key, const Read &value)
    { return emplace(key_type(key), value); }

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const QString &key, const Read &value)
    { return emplace(key.toStdString(), value); }

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const ReadHash &other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &first, const iterator &last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(const key_type &key);
};


class WriteArray;

/**
 * A read only access to a variant handle as an array.  This roughly
 * follows the STL sequential container API.
 */
class CPD3DATACORE_EXPORT ReadArray {
public:
    using backend_array = std::vector<std::shared_ptr<Node>>;
    using value_type = Read;
    using size_type = typename backend_array::size_type;
    using difference_type = typename backend_array::difference_type;
    using pointer = const value_type *;
    using const_pointer = const value_type *;
    using reference = const value_type &;
    using const_reference = const value_type &;

private:
    std::shared_ptr<TopLevel> top;
    Path path;

    friend class WriteArray;

    const backend_array &backend() const;

public:
    ReadArray();

    ~ReadArray();

    ReadArray(const ReadArray &);

    ReadArray &operator=(const ReadArray &);

    ReadArray(ReadArray &&);

    ReadArray &operator=(ReadArray &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     */
    explicit ReadArray(const Read &read);

    /**
     * Swap two handles.
     *
     * @param a     the first handle
     * @param b     the second handle
     */
    static void swap(ReadArray &a, ReadArray &b);


    /**
     * Test if the container is empty.
     *
     * @return      true if the container is empty
     */
    bool empty() const;

    /**
     * Get the total size of the container.
     *
     * @return      the number of elements in the container
     */
    size_type size() const;

    /**
     * Get the first element in the array.
     * 
     * @return      the first element in the array 
     */
    value_type front() const;

    /**
     * Get the last element in the array.
     * 
     * @return      the last element in the array 
     */
    value_type back() const;


    bool operator==(const ReadArray &other) const;

    bool operator!=(const ReadArray &other) const;

    value_type operator[](size_type index) const;


    /**
     * A read only iterator.
     */
    class CPD3DATACORE_EXPORT const_iterator final {
    public:
        using difference_type = ReadArray::difference_type;
        using value_type = ReadArray::value_type;
        using pointer = const value_type *;
        using reference = ReadArray::const_reference;
        using iterator_category = std::random_access_iterator_tag;
    private:
        friend class ReadArray;

        const ReadArray *parent;
        ReadArray::size_type i;

    public:
        /**
         * Construct an iterator.
         *
         * @param parent    the parent container
         * @param i         the iterator location
         */
        const_iterator(const ReadArray &parent, ReadArray::size_type i);

        const_iterator();

        ~const_iterator();

        const_iterator(const const_iterator &);

        const_iterator &operator=(const const_iterator &);

        const_iterator(const_iterator &&);

        const_iterator &operator=(const_iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        value_type operator[](difference_type index) const;

        const_iterator &operator++();

        const_iterator operator++(int);

        const_iterator &operator--();

        const_iterator operator--(int);

        const_iterator &operator+=(difference_type op);

        const_iterator &operator-=(difference_type op);

        const_iterator operator+(difference_type op) const;

        const_iterator operator-(difference_type op) const;

        difference_type operator-(const const_iterator &other) const;

        bool operator==(const const_iterator &other) const;

        bool operator!=(const const_iterator &other) const;

        bool operator<(const const_iterator &other) const;

        bool operator<=(const const_iterator &other) const;

        bool operator>(const const_iterator &other) const;

        bool operator>=(const const_iterator &other) const;

        /**
         * Get the backing iterator in used.
         *
         * @return  the backing iterator
         */
        ReadArray::size_type backing() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class const_iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    const_iterator begin() const;

    /** @see begin() const */
    inline const_iterator cbegin() const
    { return begin(); }

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    const_iterator end() const;

    /** @see end() const */
    inline const_iterator cend() const
    { return end(); }
};

CPD3DATACORE_EXPORT inline void swap(ReadArray &lhs, ReadArray &rhs)
{ ReadArray::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(ReadArray &lhs, ReadArray &rhs)
{ ReadArray::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline ReadArray::const_iterator operator+(ReadArray::const_iterator::difference_type op,
                                                               const ReadArray::const_iterator &it)
{
    ReadArray::const_iterator temp = it;
    return temp += op;
}

CPD3DATACORE_EXPORT inline ReadArray::const_iterator operator-(ReadArray::const_iterator::difference_type op,
                                                               const ReadArray::const_iterator &it)
{
    ReadArray::const_iterator temp = it;
    return temp -= op;
}


/**
 * A writable access to a variant handle as an arra.  This roughly
 * follows the STL sequential container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed.
 */
class CPD3DATACORE_EXPORT WriteArray final : public ReadArray {
public:
    using value_type = Write;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_array &backend();

public:
    WriteArray();

    ~WriteArray();

    WriteArray(const WriteArray &);

    WriteArray &operator=(const WriteArray &);

    WriteArray(WriteArray &&);

    WriteArray &operator=(WriteArray &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteArray(Write &write);


    /**
     * Get the first element in the array.
     * 
     * @return      the first element in the array 
     */
    value_type front();

    /**
     * Get the last element in the array.
     * 
     * @return      the last element in the array 
     */
    value_type back();

    value_type operator[](size_type index);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using difference_type = ReadArray::difference_type;
        using value_type = WriteArray::value_type;
        using pointer = const value_type *;
        using reference = ReadArray::const_reference;
        using iterator_category = std::random_access_iterator_tag;
    private:
        friend class WriteArray;

        WriteArray *parent;
        WriteArray::size_type i;

    public:
        /**
         * Construct an iterator.
         *
         * @param parent    the parent container
         * @param i         the iterator location
         */
        iterator(WriteArray &parent, WriteArray::size_type i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        value_type operator[](difference_type index) const;

        iterator &operator++();

        iterator operator++(int);

        iterator &operator--();

        iterator operator--(int);

        iterator &operator+=(difference_type op);

        iterator &operator-=(difference_type op);

        iterator operator+(difference_type op) const;

        iterator operator-(difference_type op) const;

        difference_type operator-(const iterator &other) const;

        difference_type operator-(const ReadArray::const_iterator &other) const;

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        bool operator<(const iterator &other) const;

        bool operator<=(const iterator &other) const;

        bool operator>(const iterator &other) const;

        bool operator>=(const iterator &other) const;

        bool operator==(const ReadArray::const_iterator &other) const;

        bool operator!=(const ReadArray::const_iterator &other) const;

        bool operator<(const ReadArray::const_iterator &other) const;

        bool operator<=(const ReadArray::const_iterator &other) const;

        bool operator>(const ReadArray::const_iterator &other) const;

        bool operator>=(const ReadArray::const_iterator &other) const;

        /**
         * Get the backing iterator in used.
         *
         * @return  the backing iterator
         */
        WriteArray::size_type backing() const;

        operator ReadArray::const_iterator() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();

    /**
     * Change the array size.  Newly created elements will be
     * initialized empty.
     *
     * @param count the new size
     */
    void resize(size_type count);

    /**
     * Change the array size.  Newly created elements will be
     * initialized with the specified value.
     *
     * @param count the new size
     * @param value the value to initialize with
     */
    void resize(size_type count, const Read &value);


    /**
     * Add an element to the back of the array.
     *
     * @param value the value to add
     */
    void push_back(const Read &value);

    /**
     * Get an access handle after the end of the array.  This is used
     * to instantiate a new element.
     *
     * @return  an access handle at end()
     */
    value_type after_back();

    /**
     * Construct an element on the back of the array.
     *
     * @tparam Args the argument types
     * @param args  the arguments forwarded
     */
    template<typename... Args>
    inline void emplace_back(Args &&... args)
    { return push_back(std::move(Read(std::forward<Args>(args)...))); }

    /**
     * Add an element to the front of the array.
     *
     * @param value the value to add
     */
    void push_front(const Read &value);

    /**
     * Construct an element on the front of the array.
     *
     * @tparam Args the argument types
     * @param args  the arguments forwarded
     */
    template<typename... Args>
    inline void emplace_front(Args &&... args)
    { return push_front(std::move(Read(std::forward<Args>(args)...))); }

    /**
     * Insert an element at a specific position.
     *
     * @param pos       the position to insert at (shifting to the back)
     * @param value     the value to insert
     * @return          an iterator at the inserted position
     */
    iterator insert(const const_iterator &pos, const Read &value);

    /**
     * Insert multiple elements at a position.
     *
     * @param pos       the position to insert (shifting to the back)
     * @param count     the number of copies to insert
     * @param value     the value to insert
     * @return          an iterator at the first inserted position
     */
    iterator insert(const const_iterator &pos, size_type count, const Read &value);

    /**
     * Insert multiple elements at a position.
     *
     * @tparam InputIt  the input iterator type
     * @param pos       the position to insert (shifting to the back)
     * @param first     the first element to insert
     * @param last      the after the last element to insert
     * @return          an iterator at the first inserted position
     */
    template<class InputIt>
    iterator insert(const_iterator pos,
                    typename std::enable_if<std::is_base_of<std::input_iterator_tag,
                                                            typename std::iterator_traits<
                                                                    InputIt>::iterator_category>::value,
                                            InputIt>::type first,
                    InputIt last)
    {
        auto index = pos - cbegin();
        for (; first != last; ++first, ++pos) {
            pos = insert(pos, *first);
        }
        return begin() + index;
    }

    /**
     * Insert multiple elements at a position.
     *
     * @param pos       the position to insert at (shifting to the back)
     * @param ilist     the elements to insert
     * @return          an iterator at the first inserted position
     */
    iterator insert(const const_iterator &pos, std::initializer_list<Read> ilist);

    /**
     * Remove all elements of the array.
     */
    void clear();

    /**
     * Remove the first element of the array.
     */
    void pop_back();

    /**
     * Remove the last element of the array.
     */
    void pop_front();

    /**
     * Erase an element of the array.
     *
     * @param pos   the position to erase
     */
    iterator erase(const const_iterator &pos);

    /**
     * Erase an element of the array.
     *
     * @param pos   the position to erase
     */
    iterator erase(size_type pos);

    /**
     * Erase all elements within a range.
     *
     * @param first the first element to erase
     * @param last  the end (past the last) to erase
     */
    iterator erase(const const_iterator &first, const const_iterator &last);
};

CPD3DATACORE_EXPORT inline WriteArray::iterator operator+(WriteArray::iterator::difference_type op,
                                                          const WriteArray::iterator &it)
{
    WriteArray::iterator temp = it;
    return temp += op;
}

CPD3DATACORE_EXPORT inline WriteArray::iterator operator-(WriteArray::iterator::difference_type op,
                                                          const WriteArray::iterator &it)
{
    WriteArray::iterator temp = it;
    return temp -= op;
}


class WriteMatrix;

/**
 * A read only access to a variant handle as a matrix.  This roughly
 * follows the STL sequential container API and some of the
 * associative API.
 */
class CPD3DATACORE_EXPORT ReadMatrix {
public:
    using backend_array = std::vector<std::shared_ptr<Node>>;
    using key_type = PathElement::MatrixIndex;
    using mapped_type = Read;
    using value_type = std::pair<const key_type, mapped_type>;
    using size_type = typename backend_array::size_type;
    using difference_type = typename backend_array::difference_type;
    using pointer = const value_type *;
    using const_pointer = const value_type *;
    using reference = const value_type &;
    using const_reference = const value_type &;

private:
    std::shared_ptr<TopLevel> top;
    Path path;

    friend class WriteMatrix;

    const backend_array &backend() const;

public:
    ReadMatrix();

    ~ReadMatrix();

    ReadMatrix(const ReadMatrix &);

    ReadMatrix &operator=(const ReadMatrix &);

    ReadMatrix(ReadMatrix &&);

    ReadMatrix &operator=(ReadMatrix &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     */
    explicit ReadMatrix(const Read &read);

    /**
     * Swap two handles.
     *
     * @param a     the first handle
     * @param b     the second handle
     */
    static void swap(ReadMatrix &a, ReadMatrix &b);


    /**
     * Test if the container is empty.
     *
     * @return      true if the container is empty
     */
    bool empty() const;

    /**
     * Get the total size of the container.
     *
     * @return      the number of elements in the container
     */
    size_type size() const;

    /**
     * Get the first element in the matrix.
     * 
     * @return      the first element in the matrix 
     */
    mapped_type front() const;

    /**
     * Get the last element in the matrix.
     * 
     * @return      the last element in the matrix 
     */
    mapped_type back() const;

    /**
     * Convert an offset size to a key type.
     *
     * @param origin    the offset origin
     * @return          the index
     */
    key_type key(size_type origin) const;

    /**
     * Convert a key to an index in the matrix.
     *
     * @param key       the matrix key
     * @return          the offset
     */
    size_type index(const key_type &key) const;

    /**
     * Convert an offset size to a key type.
     *
     * @param origin    the offset origin
     * @param shape     the matrix shape
     * @return          the index
     */
    static key_type key(size_type origin, const key_type &shape);

    /**
     * Convert a key to an index in the matrix.
     *
     * @param key       the matrix key
     * @param shape     the matrix shape
     * @return          the offset
     */
    static size_type index(const key_type &key, const key_type &shape);

    /**
     * Get the shape of the matrix.
     *
     * @return  the matrix shape
     */
    key_type shape() const;


    bool operator==(const ReadMatrix &other) const;

    bool operator!=(const ReadMatrix &other) const;

    mapped_type operator[](size_type index) const;

    mapped_type operator[](const key_type &index) const;

    mapped_type operator[](key_type &&index) const;


    /**
     * A read only iterator.
     */
    class CPD3DATACORE_EXPORT const_iterator final {
    public:
        using difference_type = ReadMatrix::difference_type;
        using value_type = ReadMatrix::value_type;
        using pointer = const std::pair<ReadMatrix::key_type, ReadMatrix::mapped_type> *;
        using reference = ReadMatrix::const_reference;
        using iterator_category = std::random_access_iterator_tag;
    private:
        friend class ReadMatrix;

        const ReadMatrix *parent;
        ReadMatrix::size_type i;

    public:
        /**
         * Construct an iterator.
         *
         * @param parent    the parent container
         * @param i         the iterator location
         */
        const_iterator(const ReadMatrix &parent, ReadMatrix::size_type i);

        const_iterator();

        ~const_iterator();

        const_iterator(const const_iterator &);

        const_iterator &operator=(const const_iterator &);

        const_iterator(const_iterator &&);

        const_iterator &operator=(const_iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        ReadMatrix::mapped_type operator[](difference_type index) const;

        const_iterator &operator++();

        const_iterator operator++(int);

        const_iterator &operator--();

        const_iterator operator--(int);

        const_iterator &operator+=(difference_type op);

        const_iterator &operator-=(difference_type op);

        const_iterator operator+(difference_type op) const;

        const_iterator operator-(difference_type op) const;

        difference_type operator-(const const_iterator &other) const;

        bool operator==(const const_iterator &other) const;

        bool operator!=(const const_iterator &other) const;

        bool operator<(const const_iterator &other) const;

        bool operator<=(const const_iterator &other) const;

        bool operator>(const const_iterator &other) const;

        bool operator>=(const const_iterator &other) const;

        /**
         * Get the backing iterator in used.
         *
         * @return  the backing iterator
         */
        ReadMatrix::size_type backing() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        key_type key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        ReadMatrix::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class const_iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    const_iterator begin() const;

    /** @see begin() const */
    inline const_iterator cbegin() const
    { return begin(); }

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    const_iterator end() const;

    /** @see end() const */
    inline const_iterator cend() const
    { return end(); }

    /**
     * Get an iterator at a specific index.
     *
     * @param index the target index
     * @return      the iterator or end() if out of bounds
     */
    const_iterator find(const key_type &index) const;
};

CPD3DATACORE_EXPORT inline void swap(ReadMatrix &lhs, ReadMatrix &rhs)
{ ReadMatrix::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(ReadMatrix &lhs, ReadMatrix &rhs)
{ ReadMatrix::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline ReadMatrix::const_iterator operator+(ReadMatrix::const_iterator::difference_type op,
                                                                const ReadMatrix::const_iterator &it)
{
    ReadMatrix::const_iterator temp = it;
    return temp += op;
}

CPD3DATACORE_EXPORT inline ReadMatrix::const_iterator operator-(ReadMatrix::const_iterator::difference_type op,
                                                                const ReadMatrix::const_iterator &it)
{
    ReadMatrix::const_iterator temp = it;
    return temp -= op;
}


/**
 * A writable access to a variant handle as an array.  This roughly
 * follows the STL sequential container API with some elements from
 * the associative ones.  This does not actually trigger a write to
 * the variant until a non-const operation is performed.
 */
class CPD3DATACORE_EXPORT WriteMatrix final : public ReadMatrix {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_array &backend();

public:
    WriteMatrix();

    ~WriteMatrix();

    WriteMatrix(const WriteMatrix &);

    WriteMatrix &operator=(const WriteMatrix &);

    WriteMatrix(WriteMatrix &&);

    WriteMatrix &operator=(WriteMatrix &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteMatrix(Write &write);


    /**
     * Get the first element in the matrix.
     * 
     * @return      the first element in the matrix 
     */
    mapped_type front();

    /**
     * Get the last element in the matrix.
     * 
     * @return      the last element in the matrix 
     */
    mapped_type back();

    /**
     * Get an access handle after the end of the array.  This is used
     * to instantiate a new element.
     *
     * @return  an access handle at end()
     */
    mapped_type after_back();

    mapped_type operator[](size_type index);

    mapped_type operator[](const PathElement::MatrixIndex &index);

    mapped_type operator[](PathElement::MatrixIndex &&index);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using difference_type = WriteMatrix::difference_type;
        using value_type = WriteMatrix::value_type;
        using pointer = const std::pair<WriteMatrix::key_type, WriteMatrix::mapped_type> *;
        using reference = WriteMatrix::const_reference;
        using iterator_category = std::random_access_iterator_tag;
    private:
        friend class WriteMatrix;

        WriteMatrix *parent;
        WriteMatrix::size_type i;

    public:
        /**
         * Construct an iterator.
         *
         * @param parent    the parent container
         * @param i         the iterator location
         */
        iterator(WriteMatrix &parent, WriteMatrix::size_type i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        mapped_type operator[](difference_type index) const;

        iterator &operator++();

        iterator operator++(int);

        iterator &operator--();

        iterator operator--(int);

        iterator &operator+=(difference_type op);

        iterator &operator-=(difference_type op);

        iterator operator+(difference_type op) const;

        iterator operator-(difference_type op) const;

        difference_type operator-(const iterator &other) const;

        difference_type operator-(const ReadMatrix::const_iterator &other) const;

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        bool operator<(const iterator &other) const;

        bool operator<=(const iterator &other) const;

        bool operator>(const iterator &other) const;

        bool operator>=(const iterator &other) const;

        bool operator==(const ReadMatrix::const_iterator &other) const;

        bool operator!=(const ReadMatrix::const_iterator &other) const;

        bool operator<(const ReadMatrix::const_iterator &other) const;

        bool operator<=(const ReadMatrix::const_iterator &other) const;

        bool operator>(const ReadMatrix::const_iterator &other) const;

        bool operator>=(const ReadMatrix::const_iterator &other) const;

        /**
         * Get the backing iterator in used.
         *
         * @return  the backing iterator
         */
        WriteMatrix::size_type backing() const;

        operator ReadMatrix::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        key_type key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteMatrix::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();

    /**
     * Change the matrix size.  Newly created elements will be
     * initialized empty.
     *
     * @param count the new size
     */
    void resize(size_type count);

    /**
     * Change the matrix size.  Newly created elements will be
     * initialized with the specified value.
     *
     * @param count the new size
     * @param value the value to initialize with
     */
    void resize(size_type count, const Read &value);

    /**
     * Change the matrix shape.  Newly created elements will be
     * initialized empty.
     *
     * @param shape the new shape
     */
    void reshape(const key_type &shape);

    /**
     * Change the matrix shape.  Newly created elements will be
     * initialized with the specified value.
     *
     * @param shape the new shape
     * @param value the value to initialize with
     */
    void reshape(const key_type &shape, const Read &value);


    /**
     * Add an element to the back of the matrix.
     *
     * @param value the value to add
     */
    void push_back(const Read &value);

    /**
     * Construct an element on the back of the matrix.
     *
     * @tparam Args the argument types
     * @param args  the arguments forwarded
     */
    template<typename... Args>
    inline void emplace_back(Args &&... args)
    { return push_back(std::move(Read(std::forward<Args>(args)...))); }

    /**
     * Add an element to the front of the matrix.
     *
     * @param value the value to add
     */
    void push_front(const Read &value);

    /**
     * Construct an element on the front of the matrix.
     *
     * @tparam Args the argument types
     * @param args  the arguments forwarded
     */
    template<typename... Args>
    inline void emplace_front(Args &&... args)
    { return push_front(std::move(Read(std::forward<Args>(args)...))); }

    /**
     * Insert an element at a specific position.
     *
     * @param pos       the position to insert at (shifting to the back)
     * @param value     the value to insert
     * @return          an iterator at the inserted position
     */
    iterator insert(const const_iterator &pos, const Read &value);

    /**
     * Insert multiple elements at a position.
     *
     * @param pos       the position to insert (shifting to the back)
     * @param count     the number of copies to insert
     * @param value     the value to insert
     * @return          an iterator at the first inserted position
     */
    iterator insert(const const_iterator &pos, size_type count, const Read &value);

    /**
     * Insert multiple elements at a position.
     *
     * @tparam InputIt  the input iterator type
     * @param pos       the position to insert (shifting to the back)
     * @param first     the first element to insert
     * @param last      the after the last element to insert
     * @return          an iterator at the first inserted position
     */
    template<class InputIt>
    iterator insert(const_iterator pos,
                    typename std::enable_if<std::is_base_of<std::input_iterator_tag,
                                                            typename std::iterator_traits<
                                                                    InputIt>::iterator_category>::value,
                                            InputIt>::type first,
                    InputIt last)
    {
        auto index = pos - cbegin();
        for (; first != last; ++first, ++pos) {
            pos = insert(pos, *first);
        }
        return begin() + index;
    }

    /**
     * Insert multiple elements at a position.
     *
     * @param pos       the position to insert at (shifting to the back)
     * @param ilist     the elements to insert
     * @return          an iterator at the first inserted position
     */
    iterator insert(const const_iterator &pos, std::initializer_list<Read> ilist);

    /**
     * Remove all elements of the matrix.
     */
    void clear();

    /**
     * Remove the first element of the matrix.
     */
    void pop_back();

    /**
     * Remove the last element of the matrix.
     */
    void pop_front();

    /**
     * Erase an element of the matrix.
     *
     * @param pos   the position to erase
     */
    iterator erase(const const_iterator &pos);

    /**
     * Erase an element of the matrix.
     *
     * @param pos   the position to erase
     */
    iterator erase(size_type pos);

    /**
     * Erase all elements within a range.
     *
     * @param first the first element to erase
     * @param last  the end (past the last) to erase
     */
    iterator erase(const const_iterator &first, const const_iterator &last);
};

CPD3DATACORE_EXPORT inline WriteMatrix::iterator operator+(WriteMatrix::iterator::difference_type op,
                                                           const WriteMatrix::iterator &it)
{
    WriteMatrix::iterator temp = it;
    return temp += op;
}

CPD3DATACORE_EXPORT inline WriteMatrix::iterator operator-(WriteMatrix::iterator::difference_type op,
                                                           const WriteMatrix::iterator &it)
{
    WriteMatrix::iterator temp = it;
    return temp -= op;
}


class WriteKeyframe;

/**
 * A read only access to a variant handle as a keyframe map.  This roughly
 * follows the STL associative container API and is ordered.
 */
class CPD3DATACORE_EXPORT ReadKeyframe {
public:
    using backend_map = std::map<PathElement::KeyframeIndex, std::shared_ptr<Node>>;
    using key_type = typename backend_map::key_type;
    using mapped_type = Read;
    using value_type = std::pair<const key_type, mapped_type>;
    using size_type = typename backend_map::size_type;
    using difference_type = typename backend_map::difference_type;
    using pointer = const value_type *;
    using const_pointer = const value_type *;
    using reference = const value_type &;
    using const_reference = const value_type &;

private:
    std::shared_ptr<TopLevel> top;
    Path path;

    friend class WriteKeyframe;

    const backend_map &backend() const;

public:
    ReadKeyframe();

    ~ReadKeyframe();

    ReadKeyframe(const ReadKeyframe &);

    ReadKeyframe &operator=(const ReadKeyframe &);

    ReadKeyframe(ReadKeyframe &&);

    ReadKeyframe &operator=(ReadKeyframe &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     */
    explicit ReadKeyframe(const Read &read);

    /**
     * Swap two handles.
     *
     * @param a     the first handle
     * @param b     the second handle
     */
    static void swap(ReadKeyframe &a, ReadKeyframe &b);


    /**
     * Test if the container is empty.
     *
     * @return      true if the container is empty
     */
    bool empty() const;

    /**
     * Get the total size of the container.
     *
     * @return      the number of elements in the container
     */
    size_type size() const;

    /**
     * Get the number of elements that match a key.
     *
     * @param key   the key to find
     * @return      the number of matching elements
     */
    size_type count(key_type key) const;

    bool operator==(const ReadKeyframe &other) const;

    bool operator!=(const ReadKeyframe &other) const;


    mapped_type operator[](key_type key) const;


    /**
     * A read only iterator.
     */
    class CPD3DATACORE_EXPORT const_iterator final {
    public:
        using backing_iterator = typename backend_map::const_iterator;
        using difference_type = ReadKeyframe::difference_type;
        using value_type = ReadKeyframe::value_type;
        using pointer = const std::pair<ReadKeyframe::key_type, ReadKeyframe::mapped_type> *;
        using reference = ReadKeyframe::const_reference;
        using iterator_category = std::bidirectional_iterator_tag;
    private:
        friend class ReadKeyframe;

        const ReadKeyframe *parent;
        backing_iterator i;

    public:
        /**
         * Construct an iterator.
         *
         * @param parent    the parent container
         * @param i         the iterator location
         */
        const_iterator(const ReadKeyframe &parent, backing_iterator &&i);

        const_iterator();

        ~const_iterator();

        const_iterator(const const_iterator &);

        const_iterator &operator=(const const_iterator &);

        const_iterator(const_iterator &&);

        const_iterator &operator=(const_iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        const_iterator &operator++();

        const_iterator operator++(int);

        const_iterator &operator--();

        const_iterator operator--(int);

        bool operator==(const const_iterator &other) const;

        bool operator!=(const const_iterator &other) const;

        /**
         * Get the backing iterator in used.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        key_type key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        ReadKeyframe::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class const_iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    const_iterator begin() const;

    /** @see begin() const */
    inline const_iterator cbegin() const
    { return begin(); }

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    const_iterator end() const;

    /** @see end() const */
    inline const_iterator cend() const
    { return end(); }


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    const_iterator find(key_type key) const;

    /**
     * Get the first element not less than the key.
     *
     * @param key   the key to locate
     * @return      an iterator pointing to the first element not less than the key
     */
    const_iterator lower_bound(key_type key) const;

    /**
     * Get the first element not greater than the key.
     *
     * @param key   the key to locate
     * @return      an iterator pointing to the first element greater than the key
     */
    const_iterator upper_bound(key_type key) const;

    /**
     * Get all unique keys in the container.
     *
     * @return      the set of unique keys
     */
    std::vector<key_type> keys() const;
};

CPD3DATACORE_EXPORT inline void swap(ReadKeyframe &lhs, ReadKeyframe &rhs)
{ ReadKeyframe::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(ReadKeyframe &lhs, ReadKeyframe &rhs)
{ ReadKeyframe::swap(lhs, rhs); }


/**
 * A writable access to a variant handle as a keyframe map.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteKeyframe final : public ReadKeyframe {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_map &backend();

public:
    WriteKeyframe();

    ~WriteKeyframe();

    WriteKeyframe(const WriteKeyframe &);

    WriteKeyframe &operator=(const WriteKeyframe &);

    WriteKeyframe(WriteKeyframe &&);

    WriteKeyframe &operator=(WriteKeyframe &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteKeyframe(Write &write);


    mapped_type operator[](key_type key);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using backing_iterator = typename backend_map::iterator;
        using difference_type = WriteKeyframe::difference_type;
        using value_type = WriteKeyframe::value_type;
        using pointer = const std::pair<WriteKeyframe::key_type, WriteKeyframe::mapped_type> *;
        using reference = WriteKeyframe::const_reference;
        using iterator_category = std::bidirectional_iterator_tag;
    private:
        friend class WriteKeyframe;

        WriteKeyframe *parent;
        backing_iterator i;

    public:
        /**
         * Create the iterator.
         *
         * @param parent    the parent container
         * @param i         the backing iterator
         */
        iterator(WriteKeyframe &parent, backing_iterator &&i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        iterator &operator++();

        iterator operator++(int);

        iterator &operator--();

        iterator operator--(int);

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        /**
         * Get the backing iterator.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        bool operator==(const ReadKeyframe::const_iterator &other) const;

        bool operator!=(const ReadKeyframe::const_iterator &other) const;

        operator ReadKeyframe::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        key_type key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteKeyframe::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    iterator find(key_type key);

    /**
     * Get the first element not less than the key.
     *
     * @param key   the key to locate
     * @return      an iterator pointing to the first element not less than the key
     */
    iterator lower_bound(key_type key);

    /**
     * Get the first element not greater than the key.
     *
     * @param key   the key to locate
     * @return      an iterator pointing to the first element greater than the key
     */
    iterator upper_bound(key_type key);


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @param value     the value to insert
     * @return          a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> emplace(key_type key, const Read &value);

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const ReadKeyframe &other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &first, const iterator &last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(key_type key);
};


class WriteMetadataReal;

class WriteMetadataInteger;

class WriteMetadataBoolean;

class WriteMetadataString;

class WriteMetadataBytes;

class WriteMetadataArray;

class WriteMetadataMatrix;

class WriteMetadataKeyframe;

class WriteMetadataFlags;

class WriteMetadataHash;

/**
 * A read only access to a variant handle as a generic metadata map.  This roughly
 * follows the STL associative container API.
 */
class CPD3DATACORE_EXPORT ReadMetadata {
public:
    using backend_map = std::unordered_map<PathElement::MetadataIndex, std::shared_ptr<Node>>;
    using key_type = typename backend_map::key_type;
    using mapped_type = Read;
    using value_type = std::pair<const key_type, mapped_type>;
    using size_type = typename backend_map::size_type;
    using difference_type = typename backend_map::difference_type;
    using pointer = const value_type *;
    using const_pointer = const value_type *;
    using reference = const value_type &;
    using const_reference = const value_type &;

private:
    std::shared_ptr<TopLevel> top;
    Path path;
    PathElement::Type type;

    const backend_map &backend() const;

    friend class WriteMetadataReal;

    friend class WriteMetadataInteger;

    friend class WriteMetadataBoolean;

    friend class WriteMetadataString;

    friend class WriteMetadataBytes;

    friend class WriteMetadataArray;

    friend class WriteMetadataMatrix;

    friend class WriteMetadataKeyframe;

    friend class WriteMetadataFlags;

    friend class WriteMetadataHash;

public:
    ReadMetadata();

    ~ReadMetadata();

    ReadMetadata(const ReadMetadata &);

    ReadMetadata &operator=(const ReadMetadata &);

    ReadMetadata(ReadMetadata &&);

    ReadMetadata &operator=(ReadMetadata &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     * @aram type   the
     */
    explicit ReadMetadata(const Read &read);

    /**
     * Create a container accessor.
     *
     * @param read      the read handle to access
     * @param type      the type to force read access to
     */
    ReadMetadata(const Read &read, PathElement::Type type);

    /**
     * Swap two handles.
     *
     * @param a     the first handle
     * @param b     the second handle
     */
    static void swap(ReadMetadata &a, ReadMetadata &b);


    /**
     * Test if the container is empty.
     *
     * @return      true if the container is empty
     */
    bool empty() const;

    /**
     * Get the total size of the container.
     *
     * @return      the number of elements in the container
     */
    size_type size() const;

    /**
     * Get the number of elements that match a key.
     *
     * @param key   the key to find
     * @return      the number of matching elements
     */
    size_type count(const key_type &key) const;

    /** @see count(const key_type &) const */
    inline size_type count(const char *key) const
    { return count(key_type(key)); }

    /** @see count(const key_type &) const */
    inline size_type count(const QString &key) const
    { return count(key.toStdString()); }

    bool operator==(const ReadMetadata &other) const;

    bool operator!=(const ReadMetadata &other) const;


    mapped_type operator[](const key_type &key) const;

    mapped_type operator[](key_type &&key) const;

    mapped_type operator[](const char *key) const;

    mapped_type operator[](const QString &key) const;


    /**
     * A read only iterator.
     */
    class CPD3DATACORE_EXPORT const_iterator final {
    public:
        using backing_iterator = typename backend_map::const_iterator;
        using difference_type = ReadMetadata::difference_type;
        using value_type = ReadMetadata::value_type;
        using pointer = const std::pair<ReadMetadata::key_type, ReadMetadata::mapped_type> *;
        using reference = ReadMetadata::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class ReadMetadata;

        const ReadMetadata *parent;
        backing_iterator i;

    public:
        /**
         * Construct an iterator.
         *
         * @param parent    the parent container
         * @param i         the iterator location
         */
        const_iterator(const ReadMetadata &parent, backing_iterator &&i);

        const_iterator();

        ~const_iterator();

        const_iterator(const const_iterator &);

        const_iterator &operator=(const const_iterator &);

        const_iterator(const_iterator &&);

        const_iterator &operator=(const_iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        const_iterator &operator++();

        const_iterator operator++(int);

        bool operator==(const const_iterator &other) const;

        bool operator!=(const const_iterator &other) const;

        /**
         * Get the backing iterator in used.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        ReadMetadata::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class const_iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    const_iterator begin() const;

    /** @see begin() const */
    inline const_iterator cbegin() const
    { return begin(); }

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    const_iterator end() const;

    /** @see end() const */
    inline const_iterator cend() const
    { return end(); }


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    const_iterator find(const key_type &key) const;

    /** @see find(const key_type &) const */
    inline const_iterator find(const char *key) const
    { return find(key_type(key)); }

    /** @see find(const key_type &) const */
    inline const_iterator find(const QString &key) const
    { return find(key.toStdString()); }


    /**
     * Get all unique keys in the container.
     *
     * @return      the set of unique keys
     */
    std::unordered_set<key_type> keys() const;
};

CPD3DATACORE_EXPORT inline void swap(ReadMetadata &lhs, ReadMetadata &rhs)
{ ReadMetadata::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(ReadMetadata &lhs, ReadMetadata &rhs)
{ ReadMetadata::swap(lhs, rhs); }

/**
 * A read only access to a variant handle as a real number metadata map.  This roughly
 * follows the STL associative container API.
 */
class CPD3DATACORE_EXPORT ReadMetadataReal : public ReadMetadata {
public:
    ReadMetadataReal();

    ~ReadMetadataReal();

    ReadMetadataReal(const ReadMetadataReal &);

    ReadMetadataReal &operator=(const ReadMetadataReal &);

    ReadMetadataReal(ReadMetadataReal &&);

    ReadMetadataReal &operator=(ReadMetadataReal &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     * @aram type   the
     */
    explicit ReadMetadataReal(const Read &read);
};

/**
 * A writable access to a variant handle as a real metadata map.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteMetadataReal final : public ReadMetadataReal {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_map &backend();

public:
    WriteMetadataReal();

    ~WriteMetadataReal();

    WriteMetadataReal(const WriteMetadataReal &);

    WriteMetadataReal &operator=(const WriteMetadataReal &);

    WriteMetadataReal(WriteMetadataReal &&);

    WriteMetadataReal &operator=(WriteMetadataReal &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteMetadataReal(Write &write);


    mapped_type operator[](const key_type &key);

    mapped_type operator[](key_type &&key);

    mapped_type operator[](const char *key);

    mapped_type operator[](const QString &key);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using backing_iterator = typename backend_map::iterator;
        using difference_type = WriteMetadataReal::difference_type;
        using value_type = WriteMetadataReal::value_type;
        using pointer = const std::pair<WriteMetadataReal::key_type,
                                        WriteMetadataReal::mapped_type> *;
        using reference = WriteMetadataReal::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class WriteMetadataReal;

        WriteMetadataReal *parent;
        backing_iterator i;

    public:
        /**
         * Create the iterator.
         *
         * @param parent    the parent container
         * @param i         the backing iterator
         */
        iterator(WriteMetadataReal &parent, backing_iterator &&i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        iterator &operator++();

        iterator operator++(int);

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        /**
         * Get the backing iterator.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        bool operator==(const ReadMetadataReal::const_iterator &other) const;

        bool operator!=(const ReadMetadataReal::const_iterator &other) const;

        operator ReadMetadataReal::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteMetadataReal::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    iterator find(const key_type &key);

    /** @see find(const key_type &) */
    inline iterator find(const char *key)
    { return find(key_type(key)); }

    /** @see find(const key_type &) */
    inline iterator find(const QString &key)
    { return find(key.toStdString()); }


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @param value     the value to insert
     * @return          a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> emplace(const key_type &key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    std::pair<iterator, bool> emplace(key_type &&key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const char *key, const Read &value)
    { return emplace(key_type(key), value); }

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const QString &key, const Read &value)
    { return emplace(key.toStdString(), value); }

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const ReadMetadataReal &other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &first, const iterator &last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(const key_type &key);
};

/**
 * A read only access to a variant handle as a integer number metadata map.  This roughly
 * follows the STL associative container API.
 */
class CPD3DATACORE_EXPORT ReadMetadataInteger : public ReadMetadata {
public:
    ReadMetadataInteger();

    ~ReadMetadataInteger();

    ReadMetadataInteger(const ReadMetadataInteger &);

    ReadMetadataInteger &operator=(const ReadMetadataInteger &);

    ReadMetadataInteger(ReadMetadataInteger &&);

    ReadMetadataInteger &operator=(ReadMetadataInteger &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     * @aram type   the
     */
    explicit ReadMetadataInteger(const Read &read);
};

/**
 * A writable access to a variant handle as an integer metadata map.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteMetadataInteger final : public ReadMetadataInteger {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_map &backend();

public:
    WriteMetadataInteger();

    ~WriteMetadataInteger();

    WriteMetadataInteger(const WriteMetadataInteger &);

    WriteMetadataInteger &operator=(const WriteMetadataInteger &);

    WriteMetadataInteger(WriteMetadataInteger &&);

    WriteMetadataInteger &operator=(WriteMetadataInteger &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteMetadataInteger(Write &write);


    mapped_type operator[](const key_type &key);

    mapped_type operator[](key_type &&key);

    mapped_type operator[](const char *key);

    mapped_type operator[](const QString &key);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using backing_iterator = typename backend_map::iterator;
        using difference_type = WriteMetadataInteger::difference_type;
        using value_type = WriteMetadataInteger::value_type;
        using pointer = const std::pair<WriteMetadataInteger::key_type,
                                        WriteMetadataInteger::mapped_type> *;
        using reference = WriteMetadataInteger::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class WriteMetadataInteger;

        WriteMetadataInteger *parent;
        backing_iterator i;

    public:
        /**
         * Create the iterator.
         *
         * @param parent    the parent container
         * @param i         the backing iterator
         */
        iterator(WriteMetadataInteger &parent, backing_iterator &&i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        iterator &operator++();

        iterator operator++(int);

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        /**
         * Get the backing iterator.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        bool operator==(const ReadMetadataInteger::const_iterator &other) const;

        bool operator!=(const ReadMetadataInteger::const_iterator &other) const;

        operator ReadMetadataInteger::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteMetadataInteger::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    iterator find(const key_type &key);

    /** @see find(const key_type &) */
    inline iterator find(const char *key)
    { return find(key_type(key)); }

    /** @see find(const key_type &) */
    inline iterator find(const QString &key)
    { return find(key.toStdString()); }


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @param value     the value to insert
     * @return          a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> emplace(const key_type &key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    std::pair<iterator, bool> emplace(key_type &&key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const char *key, const Read &value)
    { return emplace(key_type(key), value); }

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const QString &key, const Read &value)
    { return emplace(key.toStdString(), value); }

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const ReadMetadataInteger &other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &first, const iterator &last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(const key_type &key);
};

/**
 * A read only access to a variant handle as a boolean metadata map.  This roughly
 * follows the STL associative container API.
 */
class CPD3DATACORE_EXPORT ReadMetadataBoolean : public ReadMetadata {
public:
    ReadMetadataBoolean();

    ~ReadMetadataBoolean();

    ReadMetadataBoolean(const ReadMetadataBoolean &);

    ReadMetadataBoolean &operator=(const ReadMetadataBoolean &);

    ReadMetadataBoolean(ReadMetadataBoolean &&);

    ReadMetadataBoolean &operator=(ReadMetadataBoolean &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     * @aram type   the
     */
    explicit ReadMetadataBoolean(const Read &read);
};

/**
 * A writable access to a variant handle as a boolean metadata map.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteMetadataBoolean final : public ReadMetadataBoolean {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_map &backend();

public:
    WriteMetadataBoolean();

    ~WriteMetadataBoolean();

    WriteMetadataBoolean(const WriteMetadataBoolean &);

    WriteMetadataBoolean &operator=(const WriteMetadataBoolean &);

    WriteMetadataBoolean(WriteMetadataBoolean &&);

    WriteMetadataBoolean &operator=(WriteMetadataBoolean &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteMetadataBoolean(Write &write);


    mapped_type operator[](const key_type &key);

    mapped_type operator[](key_type &&key);

    mapped_type operator[](const char *key);

    mapped_type operator[](const QString &key);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using backing_iterator = typename backend_map::iterator;
        using difference_type = WriteMetadataBoolean::difference_type;
        using value_type = WriteMetadataBoolean::value_type;
        using pointer = const std::pair<WriteMetadataBoolean::key_type,
                                        WriteMetadataBoolean::mapped_type> *;
        using reference = WriteMetadataBoolean::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class WriteMetadataBoolean;

        WriteMetadataBoolean *parent;
        backing_iterator i;

    public:
        /**
         * Create the iterator.
         *
         * @param parent    the parent container
         * @param i         the backing iterator
         */
        iterator(WriteMetadataBoolean &parent, backing_iterator &&i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        iterator &operator++();

        iterator operator++(int);

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        /**
         * Get the backing iterator.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        bool operator==(const ReadMetadataBoolean::const_iterator &other) const;

        bool operator!=(const ReadMetadataBoolean::const_iterator &other) const;

        operator ReadMetadataBoolean::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteMetadataBoolean::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    iterator find(const key_type &key);

    /** @see find(const key_type &) */
    inline iterator find(const char *key)
    { return find(key_type(key)); }

    /** @see find(const key_type &) */
    inline iterator find(const QString &key)
    { return find(key.toStdString()); }


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @param value     the value to insert
     * @return          a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> emplace(const key_type &key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    std::pair<iterator, bool> emplace(key_type &&key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const char *key, const Read &value)
    { return emplace(key_type(key), value); }

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const QString &key, const Read &value)
    { return emplace(key.toStdString(), value); }

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const ReadMetadataBoolean &other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &first, const iterator &last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(const key_type &key);
};

/**
 * A read only access to a variant handle as a string metadata map.  This roughly
 * follows the STL associative container API.
 */
class CPD3DATACORE_EXPORT ReadMetadataString : public ReadMetadata {
public:
    ReadMetadataString();

    ~ReadMetadataString();

    ReadMetadataString(const ReadMetadataString &);

    ReadMetadataString &operator=(const ReadMetadataString &);

    ReadMetadataString(ReadMetadataString &&);

    ReadMetadataString &operator=(ReadMetadataString &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     * @aram type   the
     */
    explicit ReadMetadataString(const Read &read);
};

/**
 * A writable access to a variant handle as a string metadata map.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteMetadataString final : public ReadMetadataString {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_map &backend();

public:
    WriteMetadataString();

    ~WriteMetadataString();

    WriteMetadataString(const WriteMetadataString &);

    WriteMetadataString &operator=(const WriteMetadataString &);

    WriteMetadataString(WriteMetadataString &&);

    WriteMetadataString &operator=(WriteMetadataString &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteMetadataString(Write &write);


    mapped_type operator[](const key_type &key);

    mapped_type operator[](key_type &&key);

    mapped_type operator[](const char *key);

    mapped_type operator[](const QString &key);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using backing_iterator = typename backend_map::iterator;
        using difference_type = WriteMetadataString::difference_type;
        using value_type = WriteMetadataString::value_type;
        using pointer = const std::pair<WriteMetadataString::key_type,
                                        WriteMetadataString::mapped_type> *;
        using reference = WriteMetadataString::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class WriteMetadataString;

        WriteMetadataString *parent;
        backing_iterator i;

    public:
        /**
         * Create the iterator.
         *
         * @param parent    the parent container
         * @param i         the backing iterator
         */
        iterator(WriteMetadataString &parent, backing_iterator &&i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        iterator &operator++();

        iterator operator++(int);

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        /**
         * Get the backing iterator.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        bool operator==(const ReadMetadataString::const_iterator &other) const;

        bool operator!=(const ReadMetadataString::const_iterator &other) const;

        operator ReadMetadataString::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteMetadataString::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    iterator find(const key_type &key);

    /** @see find(const key_type &) */
    inline iterator find(const char *key)
    { return find(key_type(key)); }

    /** @see find(const key_type &) */
    inline iterator find(const QString &key)
    { return find(key.toStdString()); }


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a string if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @param value     the value to insert
     * @return          a pair of the inserted iterator and a string if the insert was performed
     */
    std::pair<iterator, bool> emplace(const key_type &key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    std::pair<iterator, bool> emplace(key_type &&key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const char *key, const Read &value)
    { return emplace(key_type(key), value); }

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const QString &key, const Read &value)
    { return emplace(key.toStdString(), value); }

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const ReadMetadataString &other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &first, const iterator &last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(const key_type &key);
};

/**
 * A read only access to a variant handle as a bytes value metadata map.  This roughly
 * follows the STL associative container API.
 */
class CPD3DATACORE_EXPORT ReadMetadataBytes : public ReadMetadata {
public:
    ReadMetadataBytes();

    ~ReadMetadataBytes();

    ReadMetadataBytes(const ReadMetadataBytes &);

    ReadMetadataBytes &operator=(const ReadMetadataBytes &);

    ReadMetadataBytes(ReadMetadataBytes &&);

    ReadMetadataBytes &operator=(ReadMetadataBytes &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     * @aram type   the
     */
    explicit ReadMetadataBytes(const Read &read);
};

/**
 * A writable access to a variant handle as a bytes value metadata map.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteMetadataBytes final : public ReadMetadataBytes {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_map &backend();

public:
    WriteMetadataBytes();

    ~WriteMetadataBytes();

    WriteMetadataBytes(const WriteMetadataBytes &);

    WriteMetadataBytes &operator=(const WriteMetadataBytes &);

    WriteMetadataBytes(WriteMetadataBytes &&);

    WriteMetadataBytes &operator=(WriteMetadataBytes &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteMetadataBytes(Write &write);


    mapped_type operator[](const key_type &key);

    mapped_type operator[](key_type &&key);

    mapped_type operator[](const char *key);

    mapped_type operator[](const QString &key);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using backing_iterator = typename backend_map::iterator;
        using difference_type = WriteMetadataBytes::difference_type;
        using value_type = WriteMetadataBytes::value_type;
        using pointer = const std::pair<WriteMetadataBytes::key_type,
                                        WriteMetadataBytes::mapped_type> *;
        using reference = WriteMetadataBytes::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class WriteMetadataBytes;

        WriteMetadataBytes *parent;
        backing_iterator i;

    public:
        /**
         * Create the iterator.
         *
         * @param parent    the parent container
         * @param i         the backing iterator
         */
        iterator(WriteMetadataBytes &parent, backing_iterator &&i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        iterator &operator++();

        iterator operator++(int);

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        /**
         * Get the backing iterator.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        bool operator==(const ReadMetadataBytes::const_iterator &other) const;

        bool operator!=(const ReadMetadataBytes::const_iterator &other) const;

        operator ReadMetadataBytes::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteMetadataBytes::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    iterator find(const key_type &key);

    /** @see find(const key_type &) */
    inline iterator find(const char *key)
    { return find(key_type(key)); }

    /** @see find(const key_type &) */
    inline iterator find(const QString &key)
    { return find(key.toStdString()); }


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a bytes if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @param value     the value to insert
     * @return          a pair of the inserted iterator and a bytes if the insert was performed
     */
    std::pair<iterator, bool> emplace(const key_type &key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    std::pair<iterator, bool> emplace(key_type &&key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const char *key, const Read &value)
    { return emplace(key_type(key), value); }

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const QString &key, const Read &value)
    { return emplace(key.toStdString(), value); }

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const ReadMetadataBytes &other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &first, const iterator &last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(const key_type &key);
};

/**
 * A read only access to a variant handle as a flags value metadata map.  This roughly
 * follows the STL associative container API.
 */
class CPD3DATACORE_EXPORT ReadMetadataFlags : public ReadMetadata {
public:
    ReadMetadataFlags();

    ~ReadMetadataFlags();

    ReadMetadataFlags(const ReadMetadataFlags &);

    ReadMetadataFlags &operator=(const ReadMetadataFlags &);

    ReadMetadataFlags(ReadMetadataFlags &&);

    ReadMetadataFlags &operator=(ReadMetadataFlags &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     * @aram type   the
     */
    explicit ReadMetadataFlags(const Read &read);
};

/**
 * A writable access to a variant handle as a flags value metadata map.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteMetadataFlags final : public ReadMetadataFlags {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_map &backend();

public:
    WriteMetadataFlags();

    ~WriteMetadataFlags();

    WriteMetadataFlags(const WriteMetadataFlags &);

    WriteMetadataFlags &operator=(const WriteMetadataFlags &);

    WriteMetadataFlags(WriteMetadataFlags &&);

    WriteMetadataFlags &operator=(WriteMetadataFlags &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteMetadataFlags(Write &write);


    mapped_type operator[](const key_type &key);

    mapped_type operator[](key_type &&key);

    mapped_type operator[](const char *key);

    mapped_type operator[](const QString &key);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using backing_iterator = typename backend_map::iterator;
        using difference_type = WriteMetadataFlags::difference_type;
        using value_type = WriteMetadataFlags::value_type;
        using pointer = const std::pair<WriteMetadataFlags::key_type,
                                        WriteMetadataFlags::mapped_type> *;
        using reference = WriteMetadataFlags::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class WriteMetadataFlags;

        WriteMetadataFlags *parent;
        backing_iterator i;

    public:
        /**
         * Create the iterator.
         *
         * @param parent    the parent container
         * @param i         the backing iterator
         */
        iterator(WriteMetadataFlags &parent, backing_iterator &&i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        iterator &operator++();

        iterator operator++(int);

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        /**
         * Get the backing iterator.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        bool operator==(const ReadMetadataFlags::const_iterator &other) const;

        bool operator!=(const ReadMetadataFlags::const_iterator &other) const;

        operator ReadMetadataFlags::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteMetadataFlags::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    iterator find(const key_type &key);

    /** @see find(const key_type &) */
    inline iterator find(const char *key)
    { return find(key_type(key)); }

    /** @see find(const key_type &) */
    inline iterator find(const QString &key)
    { return find(key.toStdString()); }


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a flags if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @param value     the value to insert
     * @return          a pair of the inserted iterator and a flags if the insert was performed
     */
    std::pair<iterator, bool> emplace(const key_type &key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    std::pair<iterator, bool> emplace(key_type &&key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const char *key, const Read &value)
    { return emplace(key_type(key), value); }

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const QString &key, const Read &value)
    { return emplace(key.toStdString(), value); }

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const ReadMetadataFlags &other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &first, const iterator &last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(const key_type &key);
};

/**
 * A read only access to a variant handle as an array metadata map.  This roughly
 * follows the STL associative container API.
 */
class CPD3DATACORE_EXPORT ReadMetadataArray : public ReadMetadata {
public:
    ReadMetadataArray();

    ~ReadMetadataArray();

    ReadMetadataArray(const ReadMetadataArray &);

    ReadMetadataArray &operator=(const ReadMetadataArray &);

    ReadMetadataArray(ReadMetadataArray &&);

    ReadMetadataArray &operator=(ReadMetadataArray &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     * @aram type   the
     */
    explicit ReadMetadataArray(const Read &read);
};

/**
 * A writable access to a variant handle as an array metadata map.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteMetadataArray final : public ReadMetadataArray {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_map &backend();

public:
    WriteMetadataArray();

    ~WriteMetadataArray();

    WriteMetadataArray(const WriteMetadataArray &);

    WriteMetadataArray &operator=(const WriteMetadataArray &);

    WriteMetadataArray(WriteMetadataArray &&);

    WriteMetadataArray &operator=(WriteMetadataArray &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteMetadataArray(Write &write);


    mapped_type operator[](const key_type &key);

    mapped_type operator[](key_type &&key);

    mapped_type operator[](const char *key);

    mapped_type operator[](const QString &key);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using backing_iterator = typename backend_map::iterator;
        using difference_type = WriteMetadataArray::difference_type;
        using value_type = WriteMetadataArray::value_type;
        using pointer = const std::pair<WriteMetadataArray::key_type,
                                        WriteMetadataArray::mapped_type> *;
        using reference = WriteMetadataArray::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class WriteMetadataArray;

        WriteMetadataArray *parent;
        backing_iterator i;

    public:
        /**
         * Create the iterator.
         *
         * @param parent    the parent container
         * @param i         the backing iterator
         */
        iterator(WriteMetadataArray &parent, backing_iterator &&i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        iterator &operator++();

        iterator operator++(int);

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        /**
         * Get the backing iterator.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        bool operator==(const ReadMetadataArray::const_iterator &other) const;

        bool operator!=(const ReadMetadataArray::const_iterator &other) const;

        operator ReadMetadataArray::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteMetadataArray::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    iterator find(const key_type &key);

    /** @see find(const key_type &) */
    inline iterator find(const char *key)
    { return find(key_type(key)); }

    /** @see find(const key_type &) */
    inline iterator find(const QString &key)
    { return find(key.toStdString()); }


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a array if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @param value     the value to insert
     * @return          a pair of the inserted iterator and a array if the insert was performed
     */
    std::pair<iterator, bool> emplace(const key_type &key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    std::pair<iterator, bool> emplace(key_type &&key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const char *key, const Read &value)
    { return emplace(key_type(key), value); }

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const QString &key, const Read &value)
    { return emplace(key.toStdString(), value); }

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const ReadMetadataArray &other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &first, const iterator &last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(const key_type &key);
};


/**
 * A read only access to a variant handle as a matrix metadata map.  This roughly
 * follows the STL associative container API.
 */
class CPD3DATACORE_EXPORT ReadMetadataMatrix : public ReadMetadata {
public:
    ReadMetadataMatrix();

    ~ReadMetadataMatrix();

    ReadMetadataMatrix(const ReadMetadataMatrix &);

    ReadMetadataMatrix &operator=(const ReadMetadataMatrix &);

    ReadMetadataMatrix(ReadMetadataMatrix &&);

    ReadMetadataMatrix &operator=(ReadMetadataMatrix &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     * @aram type   the
     */
    explicit ReadMetadataMatrix(const Read &read);
};

/**
 * A writable access to a variant handle as a matrix metadata map.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteMetadataMatrix final : public ReadMetadataMatrix {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_map &backend();

public:
    WriteMetadataMatrix();

    ~WriteMetadataMatrix();

    WriteMetadataMatrix(const WriteMetadataMatrix &);

    WriteMetadataMatrix &operator=(const WriteMetadataMatrix &);

    WriteMetadataMatrix(WriteMetadataMatrix &&);

    WriteMetadataMatrix &operator=(WriteMetadataMatrix &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteMetadataMatrix(Write &write);


    mapped_type operator[](const key_type &key);

    mapped_type operator[](key_type &&key);

    mapped_type operator[](const char *key);

    mapped_type operator[](const QString &key);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using backing_iterator = typename backend_map::iterator;
        using difference_type = WriteMetadataMatrix::difference_type;
        using value_type = WriteMetadataMatrix::value_type;
        using pointer = const std::pair<WriteMetadataMatrix::key_type,
                                        WriteMetadataMatrix::mapped_type> *;
        using reference = WriteMetadataMatrix::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class WriteMetadataMatrix;

        WriteMetadataMatrix *parent;
        backing_iterator i;

    public:
        /**
         * Create the iterator.
         *
         * @param parent    the parent container
         * @param i         the backing iterator
         */
        iterator(WriteMetadataMatrix &parent, backing_iterator &&i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        iterator &operator++();

        iterator operator++(int);

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        /**
         * Get the backing iterator.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        bool operator==(const ReadMetadataMatrix::const_iterator &other) const;

        bool operator!=(const ReadMetadataMatrix::const_iterator &other) const;

        operator ReadMetadataMatrix::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteMetadataMatrix::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    iterator find(const key_type &key);

    /** @see find(const key_type &) */
    inline iterator find(const char *key)
    { return find(key_type(key)); }

    /** @see find(const key_type &) */
    inline iterator find(const QString &key)
    { return find(key.toStdString()); }


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a matrix if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @param value     the value to insert
     * @return          a pair of the inserted iterator and a matrix if the insert was performed
     */
    std::pair<iterator, bool> emplace(const key_type &key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    std::pair<iterator, bool> emplace(key_type &&key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const char *key, const Read &value)
    { return emplace(key_type(key), value); }

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const QString &key, const Read &value)
    { return emplace(key.toStdString(), value); }

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const ReadMetadataMatrix &other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &first, const iterator &last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(const key_type &key);
};


/**
 * A read only access to a variant handle as a hash metadata map.  This roughly
 * follows the STL associative container API.
 */
class CPD3DATACORE_EXPORT ReadMetadataHash : public ReadMetadata {
public:
    ReadMetadataHash();

    ~ReadMetadataHash();

    ReadMetadataHash(const ReadMetadataHash &);

    ReadMetadataHash &operator=(const ReadMetadataHash &);

    ReadMetadataHash(ReadMetadataHash &&);

    ReadMetadataHash &operator=(ReadMetadataHash &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     * @aram type   the
     */
    explicit ReadMetadataHash(const Read &read);
};

/**
 * A writable access to a variant handle as a hash metadata map.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteMetadataHash final : public ReadMetadataHash {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_map &backend();

public:
    WriteMetadataHash();

    ~WriteMetadataHash();

    WriteMetadataHash(const WriteMetadataHash &);

    WriteMetadataHash &operator=(const WriteMetadataHash &);

    WriteMetadataHash(WriteMetadataHash &&);

    WriteMetadataHash &operator=(WriteMetadataHash &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteMetadataHash(Write &write);


    mapped_type operator[](const key_type &key);

    mapped_type operator[](key_type &&key);

    mapped_type operator[](const char *key);

    mapped_type operator[](const QString &key);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using backing_iterator = typename backend_map::iterator;
        using difference_type = WriteMetadataHash::difference_type;
        using value_type = WriteMetadataHash::value_type;
        using pointer = const std::pair<WriteMetadataHash::key_type,
                                        WriteMetadataHash::mapped_type> *;
        using reference = WriteMetadataHash::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class WriteMetadataHash;

        WriteMetadataHash *parent;
        backing_iterator i;

    public:
        /**
         * Create the iterator.
         *
         * @param parent    the parent container
         * @param i         the backing iterator
         */
        iterator(WriteMetadataHash &parent, backing_iterator &&i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        iterator &operator++();

        iterator operator++(int);

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        /**
         * Get the backing iterator.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        bool operator==(const ReadMetadataHash::const_iterator &other) const;

        bool operator!=(const ReadMetadataHash::const_iterator &other) const;

        operator ReadMetadataHash::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteMetadataHash::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    iterator find(const key_type &key);

    /** @see find(const key_type &) */
    inline iterator find(const char *key)
    { return find(key_type(key)); }

    /** @see find(const key_type &) */
    inline iterator find(const QString &key)
    { return find(key.toStdString()); }


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a hash if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @param value     the value to insert
     * @return          a pair of the inserted iterator and a hash if the insert was performed
     */
    std::pair<iterator, bool> emplace(const key_type &key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    std::pair<iterator, bool> emplace(key_type &&key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const char *key, const Read &value)
    { return emplace(key_type(key), value); }

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const QString &key, const Read &value)
    { return emplace(key.toStdString(), value); }

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const ReadMetadataHash &other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &first, const iterator &last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(const key_type &key);
};


/**
 * A read only access to a variant handle as a keyframe metadata map.  This roughly
 * follows the STL associative container API.
 */
class CPD3DATACORE_EXPORT ReadMetadataKeyframe : public ReadMetadata {
public:
    ReadMetadataKeyframe();

    ~ReadMetadataKeyframe();

    ReadMetadataKeyframe(const ReadMetadataKeyframe &);

    ReadMetadataKeyframe &operator=(const ReadMetadataKeyframe &);

    ReadMetadataKeyframe(ReadMetadataKeyframe &&);

    ReadMetadataKeyframe &operator=(ReadMetadataKeyframe &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     * @aram type   the
     */
    explicit ReadMetadataKeyframe(const Read &read);
};

/**
 * A writable access to a variant handle as a keyframe metadata map.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteMetadataKeyframe final : public ReadMetadataKeyframe {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_map &backend();

public:
    WriteMetadataKeyframe();

    ~WriteMetadataKeyframe();

    WriteMetadataKeyframe(const WriteMetadataKeyframe &);

    WriteMetadataKeyframe &operator=(const WriteMetadataKeyframe &);

    WriteMetadataKeyframe(WriteMetadataKeyframe &&);

    WriteMetadataKeyframe &operator=(WriteMetadataKeyframe &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteMetadataKeyframe(Write &write);


    mapped_type operator[](const key_type &key);

    mapped_type operator[](key_type &&key);

    mapped_type operator[](const char *key);

    mapped_type operator[](const QString &key);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using backing_iterator = typename backend_map::iterator;
        using difference_type = WriteMetadataKeyframe::difference_type;
        using value_type = WriteMetadataKeyframe::value_type;
        using pointer = const std::pair<WriteMetadataKeyframe::key_type,
                                        WriteMetadataKeyframe::mapped_type> *;
        using reference = WriteMetadataKeyframe::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class WriteMetadataKeyframe;

        WriteMetadataKeyframe *parent;
        backing_iterator i;

    public:
        /**
         * Create the iterator.
         *
         * @param parent    the parent container
         * @param i         the backing iterator
         */
        iterator(WriteMetadataKeyframe &parent, backing_iterator &&i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        iterator &operator++();

        iterator operator++(int);

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        /**
         * Get the backing iterator.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        bool operator==(const ReadMetadataKeyframe::const_iterator &other) const;

        bool operator!=(const ReadMetadataKeyframe::const_iterator &other) const;

        operator ReadMetadataKeyframe::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteMetadataKeyframe::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    iterator find(const key_type &key);

    /** @see find(const key_type &) */
    inline iterator find(const char *key)
    { return find(key_type(key)); }

    /** @see find(const key_type &) */
    inline iterator find(const QString &key)
    { return find(key.toStdString()); }


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a keyframe if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @param value     the value to insert
     * @return          a pair of the inserted iterator and a keyframe if the insert was performed
     */
    std::pair<iterator, bool> emplace(const key_type &key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    std::pair<iterator, bool> emplace(key_type &&key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const char *key, const Read &value)
    { return emplace(key_type(key), value); }

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const QString &key, const Read &value)
    { return emplace(key.toStdString(), value); }

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const ReadMetadataKeyframe &other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &first, const iterator &last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(const key_type &key);
};


class WriteMetadataSingleFlag;

/**
 * A read only access to a variant handle as a hash map.  This roughly
 * follows the STL associative container API.
 */
class CPD3DATACORE_EXPORT ReadMetadataSingleFlag {
public:
    using backend_map = std::unordered_map<PathElement::MetadataSingleFlagIndex,
                                           std::shared_ptr<Node>>;
    using key_type = typename backend_map::key_type;
    using mapped_type = Read;
    using value_type = std::pair<const key_type, mapped_type>;
    using size_type = typename backend_map::size_type;
    using difference_type = typename backend_map::difference_type;
    using pointer = const value_type *;
    using const_pointer = const value_type *;
    using reference = const value_type &;
    using const_reference = const value_type &;

private:
    std::shared_ptr<TopLevel> top;
    Path path;

    friend class WriteMetadataSingleFlag;

    const backend_map &backend() const;

public:
    ReadMetadataSingleFlag();

    ~ReadMetadataSingleFlag();

    ReadMetadataSingleFlag(const ReadMetadataSingleFlag &);

    ReadMetadataSingleFlag &operator=(const ReadMetadataSingleFlag &);

    ReadMetadataSingleFlag(ReadMetadataSingleFlag &&);

    ReadMetadataSingleFlag &operator=(ReadMetadataSingleFlag &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     */
    explicit ReadMetadataSingleFlag(const Read &read);

    /**
     * Swap two handles.
     *
     * @param a     the first handle
     * @param b     the second handle
     */
    static void swap(ReadMetadataSingleFlag &a, ReadMetadataSingleFlag &b);


    /**
     * Test if the container is empty.
     *
     * @return      true if the container is empty
     */
    bool empty() const;

    /**
     * Get the total size of the container.
     *
     * @return      the number of elements in the container
     */
    size_type size() const;

    /**
     * Get the number of elements that match a key.
     *
     * @param key   the key to find
     * @return      the number of matching elements
     */
    size_type count(const key_type &key) const;

    /** @see count(const key_type &) const */
    inline size_type count(const char *key) const
    { return count(key_type(key)); }

    /** @see count(const key_type &) const */
    inline size_type count(const QString &key) const
    { return count(key.toStdString()); }

    bool operator==(const ReadMetadataSingleFlag &other) const;

    bool operator!=(const ReadMetadataSingleFlag &other) const;


    mapped_type operator[](const key_type &key) const;

    mapped_type operator[](key_type &&key) const;

    mapped_type operator[](const char *key) const;

    mapped_type operator[](const QString &key) const;


    /**
     * A read only iterator.
     */
    class CPD3DATACORE_EXPORT const_iterator final {
    public:
        using backing_iterator = typename backend_map::const_iterator;
        using difference_type = ReadMetadataSingleFlag::difference_type;
        using value_type = ReadMetadataSingleFlag::value_type;
        using pointer = const std::pair<ReadMetadataSingleFlag::key_type,
                                        ReadMetadataSingleFlag::mapped_type> *;
        using reference = ReadMetadataSingleFlag::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class ReadMetadataSingleFlag;

        const ReadMetadataSingleFlag *parent;
        backing_iterator i;

    public:
        /**
         * Construct an iterator.
         *
         * @param parent    the parent container
         * @param i         the iterator location
         */
        const_iterator(const ReadMetadataSingleFlag &parent, backing_iterator &&i);

        const_iterator();

        ~const_iterator();

        const_iterator(const const_iterator &);

        const_iterator &operator=(const const_iterator &);

        const_iterator(const_iterator &&);

        const_iterator &operator=(const_iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        const_iterator &operator++();

        const_iterator operator++(int);

        bool operator==(const const_iterator &other) const;

        bool operator!=(const const_iterator &other) const;

        /**
         * Get the backing iterator in used.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        ReadMetadataSingleFlag::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class const_iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    const_iterator begin() const;

    /** @see begin() const */
    inline const_iterator cbegin() const
    { return begin(); }

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    const_iterator end() const;

    /** @see end() const */
    inline const_iterator cend() const
    { return end(); }


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    const_iterator find(const key_type &key) const;

    /** @see find(const key_type &) const */
    inline const_iterator find(const char *key) const
    { return find(key_type(key)); }

    /** @see find(const key_type &) const */
    inline const_iterator find(const QString &key) const
    { return find(key.toStdString()); }


    /**
     * Get all unique keys in the container.
     *
     * @return      the set of unique keys
     */
    std::unordered_set<key_type> keys() const;
};

CPD3DATACORE_EXPORT inline void swap(ReadMetadataSingleFlag &lhs, ReadMetadataSingleFlag &rhs)
{ ReadMetadataSingleFlag::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(ReadMetadataSingleFlag &lhs, ReadMetadataSingleFlag &rhs)
{ ReadMetadataSingleFlag::swap(lhs, rhs); }


/**
 * A writable access to a variant handle as a hash map.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteMetadataSingleFlag final : public ReadMetadataSingleFlag {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_map &backend();

public:
    WriteMetadataSingleFlag();

    ~WriteMetadataSingleFlag();

    WriteMetadataSingleFlag(const WriteMetadataSingleFlag &);

    WriteMetadataSingleFlag &operator=(const WriteMetadataSingleFlag &);

    WriteMetadataSingleFlag(WriteMetadataSingleFlag &&);

    WriteMetadataSingleFlag &operator=(WriteMetadataSingleFlag &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteMetadataSingleFlag(Write &write);


    mapped_type operator[](const key_type &key);

    mapped_type operator[](key_type &&key);

    mapped_type operator[](const char *key);

    mapped_type operator[](const QString &key);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using backing_iterator = typename backend_map::iterator;
        using difference_type = WriteMetadataSingleFlag::difference_type;
        using value_type = WriteMetadataSingleFlag::value_type;
        using pointer = const std::pair<WriteMetadataSingleFlag::key_type,
                                        WriteMetadataSingleFlag::mapped_type> *;
        using reference = WriteMetadataSingleFlag::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class WriteMetadataSingleFlag;

        WriteMetadataSingleFlag *parent;
        backing_iterator i;

    public:
        /**
         * Create the iterator.
         *
         * @param parent    the parent container
         * @param i         the backing iterator
         */
        iterator(WriteMetadataSingleFlag &parent, backing_iterator &&i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        iterator &operator++();

        iterator operator++(int);

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        /**
         * Get the backing iterator.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        bool operator==(const ReadMetadataSingleFlag::const_iterator &other) const;

        bool operator!=(const ReadMetadataSingleFlag::const_iterator &other) const;

        operator ReadMetadataSingleFlag::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteMetadataSingleFlag::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    iterator find(const key_type &key);

    /** @see find(const key_type &) */
    inline iterator find(const char *key)
    { return find(key_type(key)); }

    /** @see find(const key_type &) */
    inline iterator find(const QString &key)
    { return find(key.toStdString()); }


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @param value     the value to insert
     * @return          a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> emplace(const key_type &key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    std::pair<iterator, bool> emplace(key_type &&key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const char *key, const Read &value)
    { return emplace(key_type(key), value); }

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const QString &key, const Read &value)
    { return emplace(key.toStdString(), value); }

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const ReadMetadataSingleFlag &other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &first, const iterator &last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(const key_type &key);
};


class WriteMetadataHashChild;

/**
 * A read only access to a variant handle as a hash map.  This roughly
 * follows the STL associative container API.
 */
class CPD3DATACORE_EXPORT ReadMetadataHashChild {
public:
    using backend_map = std::unordered_map<PathElement::MetadataHashChildIndex,
                                           std::shared_ptr<Node>>;
    using key_type = typename backend_map::key_type;
    using mapped_type = Read;
    using value_type = std::pair<const key_type, mapped_type>;
    using size_type = typename backend_map::size_type;
    using difference_type = typename backend_map::difference_type;
    using pointer = const value_type *;
    using const_pointer = const value_type *;
    using reference = const value_type &;
    using const_reference = const value_type &;

private:
    std::shared_ptr<TopLevel> top;
    Path path;

    friend class WriteMetadataHashChild;

    const backend_map &backend() const;

public:
    ReadMetadataHashChild();

    ~ReadMetadataHashChild();

    ReadMetadataHashChild(const ReadMetadataHashChild &);

    ReadMetadataHashChild &operator=(const ReadMetadataHashChild &);

    ReadMetadataHashChild(ReadMetadataHashChild &&);

    ReadMetadataHashChild &operator=(ReadMetadataHashChild &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     */
    explicit ReadMetadataHashChild(const Read &read);

    /**
     * Swap two handles.
     *
     * @param a     the first handle
     * @param b     the second handle
     */
    static void swap(ReadMetadataHashChild &a, ReadMetadataHashChild &b);


    /**
     * Test if the container is empty.
     *
     * @return      true if the container is empty
     */
    bool empty() const;

    /**
     * Get the total size of the container.
     *
     * @return      the number of elements in the container
     */
    size_type size() const;

    /**
     * Get the number of elements that match a key.
     *
     * @param key   the key to find
     * @return      the number of matching elements
     */
    size_type count(const key_type &key) const;

    /** @see count(const key_type &) const */
    inline size_type count(const char *key) const
    { return count(key_type(key)); }

    /** @see count(const key_type &) const */
    inline size_type count(const QString &key) const
    { return count(key.toStdString()); }

    bool operator==(const ReadMetadataHashChild &other) const;

    bool operator!=(const ReadMetadataHashChild &other) const;


    mapped_type operator[](const key_type &key) const;

    mapped_type operator[](key_type &&key) const;

    mapped_type operator[](const char *key) const;

    mapped_type operator[](const QString &key) const;


    /**
     * A read only iterator.
     */
    class CPD3DATACORE_EXPORT const_iterator final {
    public:
        using backing_iterator = typename backend_map::const_iterator;
        using difference_type = ReadMetadataHashChild::difference_type;
        using value_type = ReadMetadataHashChild::value_type;
        using pointer = const std::pair<ReadMetadataHashChild::key_type,
                                        ReadMetadataHashChild::mapped_type> *;
        using reference = ReadMetadataHashChild::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class ReadMetadataHashChild;

        const ReadMetadataHashChild *parent;
        backing_iterator i;

    public:
        /**
         * Construct an iterator.
         *
         * @param parent    the parent container
         * @param i         the iterator location
         */
        const_iterator(const ReadMetadataHashChild &parent, backing_iterator &&i);

        const_iterator();

        ~const_iterator();

        const_iterator(const const_iterator &);

        const_iterator &operator=(const const_iterator &);

        const_iterator(const_iterator &&);

        const_iterator &operator=(const_iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        const_iterator &operator++();

        const_iterator operator++(int);

        bool operator==(const const_iterator &other) const;

        bool operator!=(const const_iterator &other) const;

        /**
         * Get the backing iterator in used.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        ReadMetadataHashChild::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class const_iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    const_iterator begin() const;

    /** @see begin() const */
    inline const_iterator cbegin() const
    { return begin(); }

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    const_iterator end() const;

    /** @see end() const */
    inline const_iterator cend() const
    { return end(); }


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    const_iterator find(const key_type &key) const;

    /** @see find(const key_type &) const */
    inline const_iterator find(const char *key) const
    { return find(key_type(key)); }

    /** @see find(const key_type &) const */
    inline const_iterator find(const QString &key) const
    { return find(key.toStdString()); }


    /**
     * Get all unique keys in the container.
     *
     * @return      the set of unique keys
     */
    std::unordered_set<key_type> keys() const;
};

CPD3DATACORE_EXPORT inline void swap(ReadMetadataHashChild &lhs, ReadMetadataHashChild &rhs)
{ ReadMetadataHashChild::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(ReadMetadataHashChild &lhs, ReadMetadataHashChild &rhs)
{ ReadMetadataHashChild::swap(lhs, rhs); }


/**
 * A writable access to a variant handle as a hash map.  This roughly
 * follows the STL associative container API.  This does not actually
 * trigger a write to the variant until a non-const operation is
 * performed, but doing so may invalidate any prior const iterators.
 */
class CPD3DATACORE_EXPORT WriteMetadataHashChild final : public ReadMetadataHashChild {
public:
    using mapped_type = Write;
    using value_type = std::pair<const key_type, mapped_type>;
    using pointer = value_type *;
    using reference = value_type &;

private:
    backend_map &backend();

public:
    WriteMetadataHashChild();

    ~WriteMetadataHashChild();

    WriteMetadataHashChild(const WriteMetadataHashChild &);

    WriteMetadataHashChild &operator=(const WriteMetadataHashChild &);

    WriteMetadataHashChild(WriteMetadataHashChild &&);

    WriteMetadataHashChild &operator=(WriteMetadataHashChild &&);

    /**
     * Create a container accessor.
     *
     * @param write the write handle to access
     */
    explicit WriteMetadataHashChild(Write &write);


    mapped_type operator[](const key_type &key);

    mapped_type operator[](key_type &&key);

    mapped_type operator[](const char *key);

    mapped_type operator[](const QString &key);


    /**
     * A writable iterator.
     */
    class CPD3DATACORE_EXPORT iterator final {
    public:
        using backing_iterator = typename backend_map::iterator;
        using difference_type = WriteMetadataHashChild::difference_type;
        using value_type = WriteMetadataHashChild::value_type;
        using pointer = const std::pair<WriteMetadataHashChild::key_type,
                                        WriteMetadataHashChild::mapped_type> *;
        using reference = WriteMetadataHashChild::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class WriteMetadataHashChild;

        WriteMetadataHashChild *parent;
        backing_iterator i;

    public:
        /**
         * Create the iterator.
         *
         * @param parent    the parent container
         * @param i         the backing iterator
         */
        iterator(WriteMetadataHashChild &parent, backing_iterator &&i);

        iterator();

        ~iterator();

        iterator(const iterator &);

        iterator &operator=(const iterator &);

        iterator(iterator &&);

        iterator &operator=(iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        iterator &operator++();

        iterator operator++(int);

        bool operator==(const iterator &other) const;

        bool operator!=(const iterator &other) const;

        /**
         * Get the backing iterator.
         *
         * @return  the backing iterator
         */
        const backing_iterator &backing() const;

        bool operator==(const ReadMetadataHashChild::const_iterator &other) const;

        bool operator!=(const ReadMetadataHashChild::const_iterator &other) const;

        operator ReadMetadataHashChild::const_iterator() const;

        /**
         * Get the key of the iterator.
         *
         * @return  the key referenced by the iterator
         */
        const key_type &key() const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        WriteMetadataHashChild::mapped_type value() const;

        /**
         * Get the path element of the iterator.
         *
         * @return
         */
        PathElement path() const;
    };

    friend class iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    iterator begin();

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    iterator end();


    /**
     * Find an element in the container.
     *
     * @param key   the key to locate
     * @return      the located iterator or end() if not found
     */
    iterator find(const key_type &key);

    /** @see find(const key_type &) */
    inline iterator find(const char *key)
    { return find(key_type(key)); }

    /** @see find(const key_type &) */
    inline iterator find(const QString &key)
    { return find(key.toStdString()); }


    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param value the pair value to insert
     * @return      a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> insert(const value_type &value);

    /** @see insert(const value_type &) */
    std::pair<iterator, bool> insert(value_type &&value);

    /**
     * Insert multiple elements into the container.
     *
     * @tparam InputIt  the iterator type
     * @param first     the first element to insert
     * @param last      the end of the insertion range
     */
    template<class InputIt>
    void insert(InputIt first, InputIt last)
    {
        for (; first != last; ++first) {
            insert(*first);
        }
    }

    /**
     * Insert a value into the container if it does not already exist.
     *
     * @param key       the key to insert
     * @param value     the value to insert
     * @return          a pair of the inserted iterator and a boolean if the insert was performed
     */
    std::pair<iterator, bool> emplace(const key_type &key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    std::pair<iterator, bool> emplace(key_type &&key, const Read &value);

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const char *key, const Read &value)
    { return emplace(key_type(key), value); }

    /** @see emplace(const key_type &, const Read &) */
    inline std::pair<iterator, bool> emplace(const QString &key, const Read &value)
    { return emplace(key.toStdString(), value); }

    /**
     * Insert all elements in another container that are not currently in this one.
     *
     * @param other the other container
     */
    void merge(const ReadMetadataHashChild &other);


    /**
     * Erase a single element in the container.
     *
     * @param pos   the element to remove
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &pos);

    /**
     * Erase elements in a container.
     *
     * @param first the the first element to remove
     * @param last  the end of the removal range
     * @return      an iterator at the removal point (the new next)
     */
    iterator erase(const iterator &first, const iterator &last);

    /**
     * Erase all elements matching a key.
     *
     * @param key   the key to remove
     * @return      the number of elements removed
     */
    size_type erase(const key_type &key);
};


/**
 * A generic interface for iterating over children of a handle.
 */
class CPD3DATACORE_EXPORT ReadGeneric {
public:
    using value_type = Read;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using pointer = const value_type *;
    using const_pointer = const value_type *;
    using reference = const value_type &;
    using const_reference = const value_type &;

    /**
     * Control parameters for the iteration
     */
    struct CPD3DATACORE_EXPORT Control {
        /**
         * Sort string types before iteration.
         */
        bool sortStrings;

        /**
         * Include the default key in iteration.
         */
        bool includeDefaultKey;

        Control();
    };

private:
    class Converter;

    template<typename WrapType, typename BackendType>
    class Converter_Wrap;

    template<typename WrapType, typename BackendType>
    class Converter_SortedWrap;

    template<typename WrapType, typename BackendType>
    class Converter_StringKeyWrap;

    class Backend;

    template<typename MapType>
    class Backend_Map;

    template<typename MapType>
    class Backend_SortedMap;

    std::unique_ptr<Converter> converter;

    static value_type fetch(const std::shared_ptr<TopLevel> &top, Path path);

public:
    ReadGeneric();

    ~ReadGeneric();

    ReadGeneric(const ReadGeneric &other);

    ReadGeneric &operator=(const ReadGeneric &other);

    ReadGeneric(ReadGeneric &&);

    ReadGeneric &operator=(ReadGeneric &&);

    /**
     * Create a container accessor.
     *
     * @param read  the read handle to access
     */
    explicit ReadGeneric(const Read &read);

    /**
     * Create a container accessor.
     *
     * @param read      the read handle to access
     * @param control   the control parameters
     */
    ReadGeneric(const Read &read, const Control &control);

    /**
     * Swap two handles.
     *
     * @param a     the first handle
     * @param b     the second handle
     */
    static void swap(ReadGeneric &a, ReadGeneric &b);


    /**
     * A read only iterator.
     */
    class CPD3DATACORE_EXPORT const_iterator final {
    public:
        using difference_type = ReadGeneric::difference_type;
        using value_type = ReadGeneric::value_type;
        using pointer = ReadGeneric::pointer;
        using reference = ReadGeneric::const_reference;
        using iterator_category = std::forward_iterator_tag;
    private:
        friend class ReadGeneric;

        friend class ReadGeneric::Converter;

        std::unique_ptr<Backend> backend;

        const_iterator(std::unique_ptr<Backend> &&backend);

    public:
        const_iterator();

        ~const_iterator();

        const_iterator(const const_iterator &other);

        const_iterator &operator=(const const_iterator &other);

        const_iterator(const_iterator &&);

        const_iterator &operator=(const_iterator &&);

        value_type operator*() const;

        pointer operator->() const;

        const_iterator &operator++();

        const_iterator operator++(int);

        bool operator==(const const_iterator &other) const;

        bool operator!=(const const_iterator &other) const;

        /**
         * Get the value of the iterator.
         *
         * @return  the value referenced by the iterator
         */
        value_type value() const;


        /**
         * Get the key path element of the iterator.
         *
         * @return
         */
        PathElement key() const;

        /**
         * Get the key path element of the iterator.
         *
         * @return
         */
        inline PathElement path() const
        { return key(); }

        /**
         * Get the element key as a string.
         *
         * @return  the string representation of the iterator key
         */
        std::string stringKey() const;

        /**
         * Get the integer key as a string.
         *
         * @return  the integer representation of the iterator key
         */
        std::int_fast64_t integerKey() const;


        /**
         * Get the key index into a hash.
         *
         * @return  the hash index
         */
        PathElement::HashIndex hashIndex() const;

        /**
         * Get the key index into an array.
         *
         * @return  the array index
         */
        PathElement::ArrayIndex arrayIndex() const;

        /**
         * Get the key index into a matrix.
         *
         * @return  the matrix index
         */
        PathElement::MatrixIndex matrixIndex() const;

        /**
         * Get the key index into a keyframe map.
         *
         * @return  the keyframe index
         */
        PathElement::KeyframeIndex keyframeIndex() const;

        /**
         * Get the key index into metadata.
         *
         * @return  the metadata index
         */
        PathElement::MetadataIndex metadataIndex() const;

        /**
         * Get the key index into metadata about a single flag.
         *
         * @return  the metadata index
         */
        PathElement::MetadataSingleFlagIndex metadataSingleFlagIndex() const;

        /**
         * Get the key index into metadata about a hash child.
         *
         * @return  the metadata index
         */
        PathElement::MetadataHashChildIndex metadataHashChildIndex() const;
    };

    friend class const_iterator;

    typedef const_iterator iterator;

    /**
     * Get the begin iterator of the container.
     *
     * @return  the begin iterator
     */
    const_iterator begin() const;

    /** @see begin() const */
    inline const_iterator cbegin() const
    { return begin(); }

    /**
     * Get the end iterator of the container.
     *
     * @return  the end iterator
     */
    const_iterator end() const;

    /** @see end() const */
    inline const_iterator cend() const
    { return end(); }

    /**
     * Get the unique string keys described by the container.
     *
     * @return  the set of unique strings
     */
    std::unordered_set<std::string> keys() const;
};

CPD3DATACORE_EXPORT inline void swap(ReadGeneric &lhs, ReadGeneric &rhs)
{ ReadGeneric::swap(lhs, rhs); }

CPD3DATACORE_EXPORT inline void qSwap(ReadGeneric &lhs, ReadGeneric &rhs)
{ ReadGeneric::swap(lhs, rhs); }

}
}
}
}

#endif //CPD3DATACOREVARIANT_CONTAINERS_HXX
