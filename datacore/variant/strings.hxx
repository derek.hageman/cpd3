/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_STRINGS_HXX
#define CPD3DATACOREVARIANT_STRINGS_HXX

#include "core/first.hxx"

#include <string>
#include <unordered_map>
#include <QString>
#include <QDataStream>

#include "datacore/datacore.hxx"
#include "node.hxx"
#include "core/qtcompat.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

class NodeLocalizedString;

/**
 * A node containing a single string value.
 */
class CPD3DATACORE_EXPORT NodeString : public Node {
    std::string value;

    friend class NodeLocalizedString;

public:
    virtual ~NodeString();

    NodeString();

    NodeString(const NodeString &other);

    NodeString(NodeString &&other);

    explicit NodeString(const std::string &value);

    explicit NodeString(std::string &&value);

    explicit NodeString(QDataStream &stream);

    Type type() const override;

    void serialize(QDataStream &stream) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    std::string describe() const override;

    const std::string &toString() const override;

    QString toDisplayString(const QLocale &locale) const override;

    void setString(const std::string &value);

    void setString(std::string &&value);

    void clear() override;

    bool identicalTo(const Node &other) const override;

    bool equalTo(const Node &other, const Node &metadata) const override;

    std::size_t hash() const override;
};

/**
 * A node that contains a string and potentially other versions
 * of that string for other locales.
 */
class CPD3DATACORE_EXPORT NodeLocalizedString : public NodeString {
public:
    using Localization = std::unordered_map<QString, QString>;
private:
    Localization localized;
public:
    virtual ~NodeLocalizedString();

    NodeLocalizedString();

    NodeLocalizedString(const NodeLocalizedString &other);

    NodeLocalizedString(NodeLocalizedString &&other);

    explicit NodeLocalizedString(const NodeString &other);

    explicit NodeLocalizedString(NodeString &&other);

    NodeLocalizedString(const std::string &value, const Localization &localized);

    NodeLocalizedString(std::string &&value, Localization &&localized);

    explicit NodeLocalizedString(QDataStream &stream);

    Type type() const override;

    void serialize(QDataStream &stream) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    std::string describe() const override;

    QString toDisplayString(const QLocale &locale) const override;

    void setDisplayString(const QString &locale, const QString &value);

    Localization getAllStrings() const;

    inline bool canDemote() const
    { return localized.empty(); }

    void clear() override;

    bool identicalTo(const Node &other) const override;

    std::size_t hash() const override;
};

/**
 * A string node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeStringConstant : public NodeString {
public:
    virtual ~NodeStringConstant();

    NodeStringConstant();

    explicit NodeStringConstant(const NodeString &other);

    explicit NodeStringConstant(const std::string &value);

    explicit NodeStringConstant(std::string &&value);

    explicit NodeStringConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    void clear() override;
};

/**
 * A localized string node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeLocalizedStringConstant : public NodeLocalizedString {
public:
    virtual ~NodeLocalizedStringConstant();

    NodeLocalizedStringConstant();

    explicit NodeLocalizedStringConstant(const NodeLocalizedString &other);

    explicit NodeLocalizedStringConstant(const std::string &value, const Localization &localized);

    explicit NodeLocalizedStringConstant(std::string &&value, Localization &&localized);

    explicit NodeLocalizedStringConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    void clear() override;
};

}
}
}

#endif //CPD3DATACOREVARIANT_STRINGS_HXX
