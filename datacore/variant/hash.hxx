/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_HASH_HXX
#define CPD3DATACOREVARIANT_HASH_HXX

#include "core/first.hxx"

#include <string>
#include <memory>
#include <unordered_map>

#include "node.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

namespace Container {
class ReadHash;

class WriteHash;
}

/**
 * A node containing a hash map of strings to other nodes.
 */
class CPD3DATACORE_EXPORT NodeHash : public Node {
public:
    using Children = std::unordered_map<PathElement::HashIndex, std::shared_ptr<Node>>;
private:
    Children children;

    friend class Container::ReadHash;

    friend class Container::WriteHash;

public:
    virtual ~NodeHash();

    NodeHash();

    NodeHash(const NodeHash &other);

    explicit NodeHash(Children &&children);

    explicit NodeHash(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    std::shared_ptr<Node> read(const PathElement &path) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &path) override;

    bool bypassFirstOverlay(const Node &over) const override;

    bool bypassSecondOverlay() const override;

    bool isOverlayOpaque() const override;

    static void overlayFirst(NodeHash &under, const NodeHash &over);

    static void overlaySecond(NodeHash &under,
                              const NodeHash &over,
                              const TopLevel &origin,
                              const Path &path = Path(),
                              std::uint_fast16_t depth = 0);

    std::string describe() const override;

    void clear() override;

    bool identicalTo(const Node &other) const override;

    bool equalTo(const Node &other, const Node &metadata) const override;

    std::size_t hash() const override;

    template<typename Functor>
    bool forAllChildren(Functor f) const
    {
        for (const auto &child : children) {
            if (f(child.first, child.second))
                return true;
        }
        return false;
    }
};

/**
 * A hash node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeHashConstant : public NodeHash {
public:
    virtual ~NodeHashConstant();

    NodeHashConstant();

    explicit NodeHashConstant(const NodeHash &other);

    explicit NodeHashConstant(Children &&children);

    explicit NodeHashConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &child) override;

    void clear() override;
};

}
}
}

#endif //CPD3DATACOREVARIANT_HASH_HXX
