/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_PRIMITIVE_HXX
#define CPD3DATACOREVARIANT_PRIMITIVE_HXX

#include "core/first.hxx"

#include <cstdint>
#include <QString>
#include <QDataStream>

#include "node.hxx"
#include "common.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

namespace Container {
class WriteFlags;
}

/**
 * A node containing a single real number value.
 */
class CPD3DATACORE_EXPORT NodeReal : public Node {
    double value;
public:
    virtual ~NodeReal();

    NodeReal();

    NodeReal(const NodeReal &other);

    explicit NodeReal(double value);

    explicit NodeReal(QDataStream &stream);

    Type type() const override;

    void serialize(QDataStream &stream) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    std::string describe() const override;

    double toReal() const override;

    void setReal(double value);

    QString toDisplayString(const QLocale &locale) const override;

    void clear() override;

    bool identicalTo(const Node &other) const override;

    std::size_t hash() const override;
};

/**
 * A real node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeRealConstant : public NodeReal {
public:
    virtual ~NodeRealConstant();

    NodeRealConstant();

    explicit NodeRealConstant(const NodeReal &other);

    explicit NodeRealConstant(double value);

    explicit NodeRealConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    void clear() override;
};

/**
 * A node containing a single integer value.
 */
class CPD3DATACORE_EXPORT NodeInteger : public Node {
    std::int_fast64_t value;
public:
    virtual ~NodeInteger();

    NodeInteger();

    NodeInteger(const NodeInteger &other);

    explicit NodeInteger(std::int_fast64_t value);

    explicit NodeInteger(QDataStream &stream);

    Type type() const override;

    void serialize(QDataStream &stream) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    std::string describe() const override;

    std::int_fast64_t toInteger() const override;

    void setInteger(std::int_fast64_t value);

    QString toDisplayString(const QLocale &locale) const override;

    void clear() override;

    bool identicalTo(const Node &other) const override;

    std::size_t hash() const override;
};

/**
 * A integer node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeIntegerConstant : public NodeInteger {
public:
    virtual ~NodeIntegerConstant();

    NodeIntegerConstant();

    explicit NodeIntegerConstant(const NodeInteger &other);

    explicit NodeIntegerConstant(std::int_fast64_t value);

    explicit NodeIntegerConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    void clear() override;
};

/**
 * A node containing a single boolean value.
 */
class CPD3DATACORE_EXPORT NodeBoolean : public Node {
    bool value;
public:
    virtual ~NodeBoolean();

    NodeBoolean();

    NodeBoolean(const NodeBoolean &other);

    explicit NodeBoolean(bool value);

    explicit NodeBoolean(QDataStream &stream);

    Type type() const override;

    void serialize(QDataStream &stream) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    std::string describe() const override;

    bool toBoolean() const override;

    void setBoolean(bool value);

    void clear() override;

    bool identicalTo(const Node &other) const override;

    std::size_t hash() const override;
};

/**
 * A boolean node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeBooleanConstant : public NodeBoolean {
public:
    virtual ~NodeBooleanConstant();

    NodeBooleanConstant();

    explicit NodeBooleanConstant(const NodeBoolean &other);

    explicit NodeBooleanConstant(bool value);

    explicit NodeBooleanConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    void clear() override;
};

/**
 * A node containing a byte array value.
 */
class CPD3DATACORE_EXPORT NodeBytes : public Node {
    Bytes value;
public:
    virtual ~NodeBytes();

    NodeBytes();

    NodeBytes(const NodeBytes &other);

    explicit NodeBytes(const Bytes &value);

    explicit NodeBytes(Bytes &&value);

    explicit NodeBytes(QDataStream &stream);

    Type type() const override;

    void serialize(QDataStream &stream) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    std::string describe() const override;

    const Bytes &toBytes() const override;

    void setBytes(const Bytes &value);

    void setBytes(Bytes &&value);

    void clear() override;

    bool identicalTo(const Node &other) const override;

    std::size_t hash() const override;
};

/**
 * A byte node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeBytesConstant : public NodeBytes {
public:
    virtual ~NodeBytesConstant();

    NodeBytesConstant();

    explicit NodeBytesConstant(const NodeBytes &other);

    explicit NodeBytesConstant(const Bytes &value);

    explicit NodeBytesConstant(Bytes &&value);

    explicit NodeBytesConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    void clear() override;
};

/**
 * A node containing a flags value.
 */
class CPD3DATACORE_EXPORT NodeFlags : public Node {
    Flags value;

    friend class Container::WriteFlags;

public:
    virtual ~NodeFlags();

    NodeFlags();

    NodeFlags(const NodeFlags &other);

    explicit NodeFlags(const Flags &value);

    explicit NodeFlags(Flags &&value);

    explicit NodeFlags(QDataStream &stream);

    Type type() const override;

    void serialize(QDataStream &stream) const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    std::string describe() const override;

    const Flags &toFlags() const override;

    void setFlags(const Flags &value);

    void setFlags(Flags &&value);

    void addFlags(const Flags &value);

    void addFlags(Flags &&value);

    void removeFlags(const Flags &value);

    void removeFlags(Flags &&value);

    void applyFlag(const Flag &value);

    void applyFlag(Flag &&value);

    void clearFlag(const Flag &value);

    void clearFlag(Flag &&value);

    void clear() override;

    bool identicalTo(const Node &other) const override;

    std::size_t hash() const override;
};

/**
 * A flags node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeFlagsConstant : public NodeFlags {
public:
    virtual ~NodeFlagsConstant();

    NodeFlagsConstant();

    explicit NodeFlagsConstant(const NodeFlags &other);

    explicit NodeFlagsConstant(const Flags &value);

    explicit NodeFlagsConstant(Flags &&value);

    explicit NodeFlagsConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    void clear() override;
};

}
}
}

#endif //CPD3DATACOREVARIANT_PRIMITIVE_HXX
