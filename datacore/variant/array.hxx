/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_ARRAY_HXX
#define CPD3DATACOREVARIANT_ARRAY_HXX

#include "core/first.hxx"

#include <vector>
#include <memory>

#include "datacore/datacore.hxx"
#include "node.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

namespace Container {
class ReadArray;

class WriteArray;
}

/**
 * A node containing a array map of strings to other nodes.
 */
class CPD3DATACORE_EXPORT NodeArray : public Node {
public:
    using Children = std::vector<std::shared_ptr<Node>>;
private:
    Children children;

    friend class Container::ReadArray;

    friend class Container::WriteArray;

public:
    virtual ~NodeArray();

    NodeArray();

    NodeArray(const NodeArray &other);

    explicit NodeArray(Children &&children);

    explicit NodeArray(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    std::shared_ptr<Node> read(const PathElement &path) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &path) override;

    bool bypassFirstOverlay(const Node &over) const override;

    bool bypassSecondOverlay() const override;

    bool isOverlayOpaque() const override;

    static void overlayFirst(NodeArray &under, const NodeArray &over);

    static void overlaySecond(NodeArray &under,
                              const NodeArray &over,
                              const TopLevel &origin,
                              const Path &path = Path(),
                              std::uint_fast16_t depth = 0);

    std::string describe() const override;

    void clear() override;

    bool identicalTo(const Node &other) const override;

    bool equalTo(const Node &other, const Node &metadata) const override;

    std::size_t hash() const override;
};

/**
 * A array node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeArrayConstant : public NodeArray {
public:
    virtual ~NodeArrayConstant();

    NodeArrayConstant();

    explicit NodeArrayConstant(const NodeArray &other);

    explicit NodeArrayConstant(Children &&children);

    explicit NodeArrayConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &child) override;

    void clear() override;
};

}
}
}

#endif //CPD3DATACOREVARIANT_ARRAY_HXX
