/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "root.hxx"
#include "toplevel.hxx"
#include "overlay.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

Root::Root() : top(std::make_shared<TopLevel>())
{ }

Root::~Root() = default;

Root::Root(Root &&) noexcept = default;

Root &Root::operator=(Root &&) noexcept = default;

Root::Root(const Root &other) : top(std::make_shared<TopLevel>(*other.top))
{ }

Root &Root::operator=(const Root &other)
{
    top = std::make_shared<TopLevel>(*other.top);
    return *this;
}

Root::Root(const Read &from)
{
    if (!from.top) {
        top = std::make_shared<TopLevel>();
        return;
    }

    if (from.path.empty()) {
        top = std::make_shared<TopLevel>(*from.top);
        return;
    }

    auto ptr = from.top->read(from.path);
    if (!ptr) {
        top = std::make_shared<TopLevel>();
        return;
    }

    top = std::make_shared<TopLevel>(*ptr);
}

Root::Root(Type type) : top(std::make_shared<TopLevel>(Node::goalFromGenericType(type)))
{ }

Root::Root(double value) : top(std::make_shared<TopLevel>(value))
{ }

Root::Root(std::int_fast64_t value) : top(std::make_shared<TopLevel>(value))
{ }

Root::Root(bool value) : top(std::make_shared<TopLevel>(value))
{ }

Root::Root(const std::string &value) : top(std::make_shared<TopLevel>(value))
{ }

Root::Root(std::string &&value) : top(std::make_shared<TopLevel>(std::move(value)))
{ }

Root::Root(const char *value) : Root(std::string(value))
{ }

Root::Root(const QString &value) : Root(value.toStdString())
{ }

Root::Root(const Bytes &value) : top(std::make_shared<TopLevel>(value))
{ }

Root::Root(Bytes &&value) : top(std::make_shared<TopLevel>(std::move(value)))
{ }

Root::Root(const Flags &value) : top(std::make_shared<TopLevel>(value))
{ }

Root::Root(Flags &&value) : top(std::make_shared<TopLevel>(std::move(value)))
{ }

void Root::swap(Root &a, Root &b) noexcept
{
    using std::swap;
    swap(a.top, b.top);
}

Read Root::operator[](const std::string &path) const
{ return Read(*this, PathElement::parse(path)); }

Read Root::operator[](std::string &&path) const
{ return Read(*this, PathElement::parse(std::move(path))); }

Write Root::operator[](const std::string &path)
{ return Write(*this, PathElement::parse(path)); }

Write Root::operator[](std::string &&path)
{ return Write(*this, PathElement::parse(std::move(path))); }

bool Root::isOverlayOpaque() const
{ return top->node()->isOverlayOpaque(); }

bool Root::isOverlayOpaque(const Read &base)
{
    Q_ASSERT(base.top);
    if (base.path.empty())
        return base.top->node()->isOverlayOpaque();
    auto ptr = base.top->read(base.path);
    if (!ptr)
        return true;
    return ptr->isOverlayOpaque();
}

void Root::overlayStep(const Root &over)
{
    Q_ASSERT(top);
    Q_ASSERT(over.top);

    Overlay::first(*top, *over.top);
    Overlay::second(*top, *over.top);
}

void Root::overlayUnroll(Root &output, const Root &first)
{ output.overlayStep(first); }

Root::Root(QDataStream &stream) : top(std::make_shared<TopLevel>(stream))
{ }

Root Root::deserialize(QDataStream &stream)
{ return Root(stream); }


QDataStream &operator<<(QDataStream &stream, const Root &root)
{
    root.top->node()->serialize(stream);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, Root &root)
{
    root.top = std::make_shared<TopLevel>(stream);
    return stream;
}

QDebug operator<<(QDebug stream, const Root &root)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << root.top->node()->describe().data();
    return stream;
}

}
}
}