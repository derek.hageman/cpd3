/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <sstream>

#include "hash.hxx"
#include "overlay.hxx"
#include "serialization.hxx"
#include "core/util.hxx"


namespace CPD3 {
namespace Data {
namespace Variant {

NodeHash::~NodeHash() = default;

NodeHash::NodeHash() = default;

NodeHash::NodeHash(const NodeHash &other) : children(other.children)
{
    for (auto &mod : children) {
        mod.second->detach(mod.second);
    }
}

NodeHash::NodeHash(Children &&children) : children(std::move(children))
{ }

NodeHash::NodeHash(QDataStream &stream)
{
    std::size_t n = Serialization::deserializeShortLength(stream);
    for (std::size_t i = 0; i < n; ++i) {
        std::string key = Serialization::deserializeShortString(stream);
        children.emplace(std::move(key), Serialization::construct(stream));
    }
}

void NodeHash::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(Serialization::ID_Hash_v2);
    Serialization::serializeShortLength(stream, children.size());
    for (const auto &child : children) {
        Serialization::serializeShortString(stream, child.first);
        child.second->serialize(stream);
    }
}

Node::Type NodeHash::type() const
{ return Node::Type::Hash; }

void NodeHash::detach(std::shared_ptr<Node> &self) const
{ self = std::make_shared<NodeHashConstant>(*this); }

std::shared_ptr<Node> NodeHash::read(const PathElement &path) const
{
    auto point = children.end();
    switch (path.type) {
    case PathElement::Type::Empty:
        break;
    case PathElement::Type::AnyString:
        point = children.find(path.ref<PathElement::StringIndex>());
        break;
    case PathElement::Type::Hash:
        point = children.find(path.ref<PathElement::HashIndex>());
        break;
    default:
        break;
    }
    if (point == children.end()) {
        point = children.find(PathElement::HashIndex());
        if (point == children.end())
            return std::shared_ptr<Node>();
    }
    return point->second;
}

bool NodeHash::compatible(Node::WriteGoal goal) const
{ return goal == WriteGoal::Hash; }

bool NodeHash::childrenWritable(PathElement::Type type) const
{
    switch (type) {
    case PathElement::Type::Empty:
    case PathElement::Type::AnyString:
    case PathElement::Type::Hash:
        return true;
    default:
        break;
    }
    return false;
}

std::shared_ptr<Node> NodeHash::write(const PathElement &path, Node::WriteGoal final)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto &child = children[PathElement::HashIndex()];
        if (!child || !child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    case PathElement::Type::AnyString: {
        auto &child = children[path.ref<PathElement::StringIndex>()];
        if (!child || !child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    case PathElement::Type::Hash: {
        auto &child = children[path.ref<PathElement::HashIndex>()];
        if (!child || !child->compatible(final))
            child = create(final, child.get());
        return child;
    }
    default:
        Q_ASSERT(false);
        break;
    }
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeHash::write(const PathElement &path, PathElement::Type next)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto &child = children[PathElement::HashIndex()];
        if (!child || !child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    case PathElement::Type::AnyString: {
        auto &child = children[path.ref<PathElement::StringIndex>()];
        if (!child || !child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    case PathElement::Type::Hash: {
        auto &child = children[path.ref<PathElement::HashIndex>()];
        if (!child || !child->childrenWritable(next))
            child = create(next, child.get());
        return child;
    }
    default:
        Q_ASSERT(false);
        break;
    }
    return std::shared_ptr<Node>();
}

void NodeHash::insert(const PathElement &path, const std::shared_ptr<Node> &final)
{
    switch (path.type) {
    case PathElement::Type::Empty: {
        auto &child = children[PathElement::HashIndex()];
        child = final;
        child->detach(child);
        break;
    }
    case PathElement::Type::AnyString: {
        auto &child = children[path.ref<PathElement::StringIndex>()];
        child = final;
        child->detach(child);
        break;
    }
    case PathElement::Type::Hash: {
        auto &child = children[path.ref<PathElement::HashIndex>()];
        child = final;
        child->detach(child);
        break;
    }
    default:
        Q_ASSERT(false);
        break;
    }
}

void NodeHash::remove(const PathElement &path)
{
    switch (path.type) {
    case PathElement::Type::Empty:
        children.erase(PathElement::HashIndex());
        break;
    case PathElement::Type::AnyString:
        children.erase(path.ref<PathElement::StringIndex>());
        break;
    case PathElement::Type::Hash:
        children.erase(path.ref<PathElement::HashIndex>());
        break;
    default:
        Q_ASSERT(false);
        break;
    }
}

bool NodeHash::bypassFirstOverlay(const Node &over) const
{
    switch (over.type()) {
    case Node::Type::Hash:
    case Node::Type::HashConstant:
        break;
    default:
        return Node::bypassFirstOverlay(over);
    }
    if (!children.empty())
        return false;
    return over.bypassSecondOverlay();
}

bool NodeHash::bypassSecondOverlay() const
{
    for (const auto &child : children) {
        if (!child.second->bypassSecondOverlay())
            return false;
    }
    return true;
}

bool NodeHash::isOverlayOpaque() const
{ return false; }

void NodeHash::overlayFirst(NodeHash &under, const NodeHash &over)
{
    Q_ASSERT(under.type() == Node::Type::Hash);

    for (const auto &add : over.children) {
        Overlay::first(under.children[add.first], add.second);
    }
}

void NodeHash::overlaySecond(NodeHash &under,
                             const NodeHash &over,
                             const TopLevel &origin,
                             const Path &path,
                             uint_fast16_t depth)
{
    Q_ASSERT(under.type() == Node::Type::Hash);

    /* First, recurse the second stage */
    Path childPath = path;
    for (const auto &add : over.children) {
        childPath.emplace_back(PathElement::Type::Hash, add.first);
        Overlay::second(under.children[add.first], add.second, origin, childPath, depth);
        childPath.pop_back();
    }

    /* Now detach and make constant all children, so
     * future overlays can re-use them */
    for (auto &child : under.children) {
        child.second->detach(child.second);
    }
}

std::string NodeHash::describe() const
{
    std::stringstream str;
    str << "Hash(";
    bool first = true;

    std::vector<Children::const_iterator> sorted;
    for (auto add = children.begin(), end = children.end(); add != end; ++add) {
        sorted.emplace_back(add);
    }
    std::sort(sorted.begin(), sorted.end(),
              [](const Children::const_iterator &a, const Children::const_iterator &b) {
                  return a->first < b->first;
              });

    for (const auto &child : sorted) {
        if (!first)
            str << ",";
        first = false;

        if (child->first.find_first_of("\"\n\r\t ") == child->first.npos) {
            str << child->first << "=" << child->second->describe();
        } else {
            std::string key = child->first;
            Util::replace_all(key, "\"", "\\\"");
            Util::replace_all(key, "\n", "\\n");
            Util::replace_all(key, "\r", "\\r");
            Util::replace_all(key, "\t", "\\t");
            str << "\"" << key << "\"=" << child->second->describe();
        }
    }
    str << ")";
    return str.str();
}

void NodeHash::clear()
{ children.clear(); }

bool NodeHash::identicalTo(const Node &other) const
{
    switch (other.type()) {
    case Type::Hash:
    case Type::HashConstant:
        break;
    default:
        return false;
    }
    const auto &otherh = static_cast<const NodeHash &>(other);
    if (children.size() != otherh.children.size())
        return false;
    for (const auto &child : children) {
        auto check = otherh.children.find(child.first);
        if (check == otherh.children.end())
            return false;
        if (!child.second->identicalTo(*check->second))
            return false;
    }
    return true;
}

bool NodeHash::equalTo(const Node &other, const Node &metadata) const
{
    switch (other.type()) {
    case Type::Hash:
    case Type::HashConstant:
        break;
    default:
        return false;
    }

    const auto &otherh = static_cast<const NodeHash &>(other);
    if (children.size() != otherh.children.size())
        return false;

    for (const auto &child : children) {
        auto check = otherh.children.find(child.first);
        if (check == otherh.children.end())
            return false;

        auto ptr = metadata.read({PathElement::Type::MetadataHashChild, child.first});
        if (!ptr)
            ptr = NodeEmpty::instance;

        if (!child.second->equalTo(*check->second, *ptr))
            return false;
    }
    return true;
}

std::size_t NodeHash::hash() const
{
    std::size_t result = Serialization::ID_Hash_v2;
    result = INTEGER::mix(result, children.size());
    /* Just use simple XOR, since the order doesn't matter then */
    for (const auto &add : children) {
        result ^= std::hash<PathElement::HashIndex>()(add.first);
        result ^= add.second->hash();
    }
    return result;
}


NodeHashConstant::~NodeHashConstant() = default;

NodeHashConstant::NodeHashConstant() = default;

NodeHashConstant::NodeHashConstant(const NodeHash &other) : NodeHash(other)
{ }

NodeHashConstant::NodeHashConstant(Children &&children) : NodeHash(std::move(children))
{ }

NodeHashConstant::NodeHashConstant(QDataStream &stream) : NodeHash(stream)
{ }

Node::Type NodeHashConstant::type() const
{ return Node::Type::HashConstant; }

void NodeHashConstant::detach(std::shared_ptr<Node> &) const
{ }

bool NodeHashConstant::compatible(Node::WriteGoal) const
{ return false; }

bool NodeHashConstant::childrenWritable(PathElement::Type) const
{ return false; }

std::shared_ptr<Node> NodeHashConstant::write(const PathElement &, Node::WriteGoal)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

std::shared_ptr<Node> NodeHashConstant::write(const PathElement &, PathElement::Type)
{
    Q_ASSERT(false);
    return std::shared_ptr<Node>();
}

void NodeHashConstant::insert(const PathElement &, const std::shared_ptr<Node> &)
{ Q_ASSERT(false); }

void NodeHashConstant::remove(const PathElement &)
{ Q_ASSERT(false); }

void NodeHashConstant::clear()
{ Q_ASSERT(false); }


}
}
}