/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_COMMON_HXX
#define CPD3DATACOREVARIANT_COMMON_HXX

#include "core/first.hxx"

#include <string>
#include <unordered_set>
#include <QByteArray>

#include "datacore/datacore.hxx"
#include "core/qtcompat.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

enum class Type {
    Empty,

    Real,
    Integer,
    Boolean,
    String,
    Bytes,
    Flags,

    Hash,
    Array,
    Matrix,
    Keyframe,

    MetadataReal,
    MetadataInteger,
    MetadataBoolean,
    MetadataString,
    MetadataBytes,
    MetadataFlags,
    MetadataHash,
    MetadataArray,
    MetadataMatrix,
    MetadataKeyframe,

    Overlay,
};

typedef QByteArray Bytes;

typedef std::string Flag;
typedef std::unordered_set<Flag> Flags;

}
}
}

#ifdef CPD3_QTEST
namespace CPD3 {
namespace Data {
namespace Variant {

inline char *toString(const CPD3::Data::Variant::Type &type)
{
    switch(type) {
    case Type::Empty:
        return ::qstrdup("Variant::Type::Empty");
    case Type::Real:
        return ::qstrdup("Variant::Type::Real");
    case Type::Integer:
        return ::qstrdup("Variant::Type::Integer");
    case Type::Boolean:
        return ::qstrdup("Variant::Type::Boolean");
    case Type::String:
        return ::qstrdup("Variant::Type::String");
    case Type::Bytes:
        return ::qstrdup("Variant::Type::Bytes");
    case Type::Flags:
        return ::qstrdup("Variant::Type::Flags");
    case Type::Hash:
        return ::qstrdup("Variant::Type::Hash");
    case Type::Array:
        return ::qstrdup("Variant::Type::Array");
    case Type::Matrix:
        return ::qstrdup("Variant::Type::Matrix");
    case Type::Keyframe:
        return ::qstrdup("Variant::Type::Keyframe");
    case Type::MetadataReal:
        return ::qstrdup("Variant::Type::MetadataReal");
    case Type::MetadataInteger:
        return ::qstrdup("Variant::Type::MetadataInteger");
    case Type::MetadataBoolean:
        return ::qstrdup("Variant::Type::MetadataBoolean");
    case Type::MetadataString:
        return ::qstrdup("Variant::Type::MetadataString");
    case Type::MetadataBytes:
        return ::qstrdup("Variant::Type::MetadataBytes");
    case Type::MetadataFlags:
        return ::qstrdup("Variant::Type::MetadataFlags");
    case Type::MetadataHash:
        return ::qstrdup("Variant::Type::MetadataHash");
    case Type::MetadataArray:
        return ::qstrdup("Variant::Type::MetadataArray");
    case Type::MetadataMatrix:
        return ::qstrdup("Variant::Type::MetadataMatrix");
    case Type::MetadataKeyframe:
        return ::qstrdup("Variant::Type::MetadataKeyframe");
    case Type::Overlay:
        return ::qstrdup("Variant::Type::Overlay");
    }
    Q_ASSERT(false);
    return nullptr;
}

}
}
}
#endif

Q_DECLARE_TYPEINFO(CPD3::Data::Variant::Type, Q_PRIMITIVE_TYPE);

Q_DECLARE_METATYPE(CPD3::Data::Variant::Type);

#endif //CPD3DATACOREVARIANT_COMMON_HXX
