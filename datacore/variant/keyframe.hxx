/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACOREVARIANT_KEYFRAME_HXX
#define CPD3DATACOREVARIANT_KEYFRAME_HXX

#include "core/first.hxx"

#include <string>
#include <memory>
#include <map>

#include "node.hxx"

namespace CPD3 {
namespace Data {
namespace Variant {

namespace Container {
class ReadKeyframe;

class WriteKeyframe;
}

/**
 * A node containing a keyframe map of strings to other nodes.
 */
class CPD3DATACORE_EXPORT NodeKeyframe : public Node {
public:
    using Children = std::map<PathElement::KeyframeIndex, std::shared_ptr<Node>>;
private:
    Children children;

    friend class Container::ReadKeyframe;

    friend class Container::WriteKeyframe;

public:
    virtual ~NodeKeyframe();

    NodeKeyframe();

    NodeKeyframe(const NodeKeyframe &other);

    explicit NodeKeyframe(Children &&children);

    explicit NodeKeyframe(QDataStream &stream);

    void serialize(QDataStream &stream) const override;

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    std::shared_ptr<Node> read(const PathElement &path) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &path) override;

    bool bypassFirstOverlay(const Node &over) const override;

    bool bypassSecondOverlay() const override;

    static void overlayFirst(NodeKeyframe &under, const NodeKeyframe &over);

    static void overlaySecond(NodeKeyframe &under,
                              const NodeKeyframe &over,
                              const TopLevel &origin,
                              const Path &path = Path(),
                              std::uint_fast16_t depth = 0);

    std::string describe() const override;

    void clear() override;

    bool identicalTo(const Node &other) const override;

    bool equalTo(const Node &other, const Node &metadata) const override;

    std::size_t hash() const override;
};

/**
 * A keyframe node that is immutable and can be shared.
 */
class CPD3DATACORE_EXPORT NodeKeyframeConstant : public NodeKeyframe {
public:
    virtual ~NodeKeyframeConstant();

    NodeKeyframeConstant();

    explicit NodeKeyframeConstant(const NodeKeyframe &other);

    explicit NodeKeyframeConstant(Children &&children);

    explicit NodeKeyframeConstant(QDataStream &stream);

    Type type() const override;

    void detach(std::shared_ptr<Node> &self) const override;

    bool compatible(WriteGoal goal) const override;

    bool childrenWritable(PathElement::Type type) const override;

    std::shared_ptr<Node> write(const PathElement &path, WriteGoal final) override;

    std::shared_ptr<Node> write(const PathElement &path, PathElement::Type next) override;

    void insert(const PathElement &path, const std::shared_ptr<Node> &final) override;

    void remove(const PathElement &child) override;

    void clear() override;
};

}
}
}

#endif //CPD3DATACOREVARIANT_KEYFRAME_HXX
