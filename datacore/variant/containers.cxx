/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <sstream>

#include "containers.hxx"
#include "handle.hxx"
#include "toplevel.hxx"
#include "core/util.hxx"


namespace CPD3 {
namespace Data {
namespace Variant {
namespace Container {


WriteFlags::WriteFlags() = default;

WriteFlags::~WriteFlags() = default;

WriteFlags::WriteFlags(const WriteFlags &) = default;

WriteFlags &WriteFlags::operator=(const WriteFlags &) = default;

WriteFlags::WriteFlags(WriteFlags &&) = default;

WriteFlags &WriteFlags::operator=(WriteFlags &&) = default;

WriteFlags::WriteFlags(Write &write) : top(write.top), path(write.path)
{ }

const WriteFlags::backend_set &WriteFlags::backend() const
{
    static const backend_set invalid;
    if (path.empty()) {
        switch (top->node()->type()) {
        case Node::Type::Flags:
        case Node::Type::FlagsConstant:
            break;
        default:
            return invalid;
        }
        return top->node<NodeFlags>()->value;
    }

    auto ptr = top->read(path);
    if (!ptr)
        return invalid;

    switch (ptr->type()) {
    case Node::Type::Flags:
    case Node::Type::FlagsConstant:
        break;
    default:
        return invalid;
    }

    /* Safe to keep the pointer as long as the root remains unchanged, since it will
     * have a pointer to it regardless.  Any write access invalidates this,
     * however. */
    return static_cast<const NodeFlags *>(ptr.get())->value;
}

WriteFlags::backend_set &WriteFlags::backend()
{
    if (path.empty())
        return static_cast<NodeFlags *>(top->write(Node::WriteGoal::Flags))->value;
    else
        return std::static_pointer_cast<NodeFlags>(top->write(path, Node::WriteGoal::Flags))->value;
}

void WriteFlags::swap(WriteFlags &a, WriteFlags &b)
{
    using std::swap;
    swap(a.top, b.top);
    swap(a.path, b.path);
}

bool WriteFlags::empty() const
{ return backend().empty(); }

WriteFlags::size_type WriteFlags::size() const
{ return backend().size(); }

WriteFlags::size_type WriteFlags::count(const key_type &key) const
{ return backend().count(key); }

bool WriteFlags::operator==(const WriteFlags &other) const
{ return backend() == other.backend(); }

bool WriteFlags::operator!=(const WriteFlags &other) const
{ return backend() != other.backend(); }

bool WriteFlags::operator==(const Flags &other) const
{ return backend() == other; }

bool WriteFlags::operator!=(const Flags &other) const
{ return backend() != other; }

WriteFlags::operator const Flags &() const
{ return backend(); }

WriteFlags::const_iterator WriteFlags::begin() const
{ return backend().begin(); }

WriteFlags::const_iterator WriteFlags::end() const
{ return backend().end(); }

WriteFlags::const_iterator WriteFlags::find(const key_type &key) const
{ return backend().find(key); }

std::pair<WriteFlags::iterator, bool> WriteFlags::insert(const value_type &value)
{ return backend().insert(value); }

std::pair<WriteFlags::iterator, bool> WriteFlags::insert(value_type &&value)
{ return backend().insert(std::move(value)); }

void WriteFlags::merge(const WriteFlags &other)
{
    auto &target = backend();
    const auto &source = other.backend();
    Util::merge(source, target);
}

void WriteFlags::merge(const Flags &other)
{
    Q_ASSERT(&other != &static_cast<const WriteFlags &>(*this).backend());
    Util::merge(other, backend());
}

void WriteFlags::merge(Flags &&other)
{
    Q_ASSERT(&other != &static_cast<const WriteFlags &>(*this).backend());
    Util::merge(std::move(other), backend());
}

bool WriteFlags::requiresIteratorDetach() const
{
    /* Top level can never be constant, so an iterator into it is either
     * one into the always available invalid, or the final target */
    if (path.empty())
        return false;

    auto ptr = top->read(path);
    /* No existing node, so the iterator is always in the invalid constant */
    if (!ptr)
        return false;

    switch (ptr->type()) {
    case Node::Type::FlagsConstant:
        break;
    default:
        /* Either already non-constant, or in the invalid */
        return false;
    }

    return true;
}

WriteFlags::iterator WriteFlags::erase(iterator pos)
{
    if (requiresIteratorDetach()) {
        key_type save = *pos;
        auto &target = std::static_pointer_cast<NodeFlags>(top->write(path, Node::WriteGoal::Flags))
                ->value;
        pos = target.find(save);
    }

    return backend().erase(pos);
}

WriteFlags::iterator WriteFlags::erase(iterator first, iterator last)
{
    if (requiresIteratorDetach()) {
        key_type save1 = *first;
        key_type save2 = *last;
        auto &target = std::static_pointer_cast<NodeFlags>(top->write(path, Node::WriteGoal::Flags))
                ->value;
        /* Assume it stays in the same order */
        first = target.find(save1);
        last = target.find(save2);
    }

    return backend().erase(first, last);
}

WriteFlags::size_type WriteFlags::erase(const key_type &key)
{
    /* Be safe and allow erase by an iterator value */
    if (requiresIteratorDetach()) {
        key_type save = key;
        return backend().erase(save);
    }
    return backend().erase(key);
}


ReadHash::ReadHash() = default;

ReadHash::~ReadHash() = default;

ReadHash::ReadHash(const ReadHash &) = default;

ReadHash &ReadHash::operator=(const ReadHash &) = default;

ReadHash::ReadHash(ReadHash &&) = default;

ReadHash &ReadHash::operator=(ReadHash &&) = default;

ReadHash::ReadHash(const Read &read) : top(read.top), path(read.path)
{ }

void ReadHash::swap(ReadHash &a, ReadHash &b)
{
    using std::swap;
    swap(a.top, b.top);
    swap(a.path, b.path);
}

const ReadHash::backend_map &ReadHash::backend() const
{
    static const ReadHash::backend_map invalid;
    if (path.empty()) {
        switch (top->node()->type()) {
        case Node::Type::Hash:
        case Node::Type::HashConstant:
            break;
        default:
            return invalid;
        }
        return top->node<NodeHash>()->children;
    }

    auto ptr = top->read(path);
    if (!ptr)
        return invalid;

    switch (ptr->type()) {
    case Node::Type::Hash:
    case Node::Type::HashConstant:
        break;
    default:
        return invalid;
    }

    /* Safe to keep the pointer as long as the root remains unchanged, since it will
     * have a pointer to it regardless.  Any write access invalidates this,
     * however. */
    return static_cast<const NodeHash *>(ptr.get())->children;
}

bool ReadHash::empty() const
{ return backend().empty(); }

ReadHash::size_type ReadHash::size() const
{ return backend().size(); }

ReadHash::size_type ReadHash::count(const key_type &key) const
{ return backend().count(key); }

bool ReadHash::operator==(const ReadHash &other) const
{
    const auto &a = backend();
    const auto &b = other.backend();
    if (a.size() != b.size())
        return false;
    for (const auto &check : a) {
        auto i = b.find(check.first);
        if (i == b.end())
            return false;
        if (!check.second->identicalTo(*i->second))
            return false;
    }
    return true;
}

bool ReadHash::operator!=(const ReadHash &other) const
{ return !(*this == other); }

ReadHash::mapped_type ReadHash::operator[](const key_type &key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Hash, key);
    return result;
}

ReadHash::mapped_type ReadHash::operator[](key_type &&key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Hash, std::move(key));
    return result;
}

ReadHash::mapped_type ReadHash::operator[](const char *key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Hash, key);
    return result;
}

ReadHash::mapped_type ReadHash::operator[](const QString &key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Hash, key);
    return result;
}

ReadHash::const_iterator ReadHash::begin() const
{ return ReadHash::const_iterator(*this, backend().begin()); }

ReadHash::const_iterator ReadHash::end() const
{ return ReadHash::const_iterator(*this, backend().end()); }

ReadHash::const_iterator ReadHash::find(const key_type &key) const
{ return ReadHash::const_iterator(*this, backend().find(key)); }

std::unordered_set<ReadHash::key_type> ReadHash::keys() const
{
    std::unordered_set<ReadHash::key_type> result;
    for (const auto &add : backend()) {
        if (add.first.empty())
            continue;
        result.insert(add.first);
    }
    return result;
}

ReadHash::const_iterator::const_iterator() = default;

ReadHash::const_iterator::~const_iterator() = default;

ReadHash::const_iterator::const_iterator(const const_iterator &) = default;

ReadHash::const_iterator &ReadHash::const_iterator::operator=(const const_iterator &) = default;

ReadHash::const_iterator::const_iterator(const_iterator &&) = default;

ReadHash::const_iterator &ReadHash::const_iterator::operator=(const_iterator &&) = default;

ReadHash::const_iterator::const_iterator(const ReadHash &parent, backing_iterator &&i) : parent(
        &parent), i(std::move(i))
{ }

ReadHash::const_iterator::value_type ReadHash::const_iterator::operator*() const
{ return ReadHash::value_type(i->first, (*parent)[i->first]); }

ReadHash::const_iterator::pointer ReadHash::const_iterator::operator->() const
{
    static thread_local std::pair<ReadHash::key_type, ReadHash::mapped_type> ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

ReadHash::const_iterator &ReadHash::const_iterator::operator++()
{
    ++i;
    return *this;
}

ReadHash::const_iterator ReadHash::const_iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool ReadHash::const_iterator::operator==(const const_iterator &other) const
{ return i == other.i; }

bool ReadHash::const_iterator::operator!=(const const_iterator &other) const
{ return i != other.i; }

const ReadHash::const_iterator::backing_iterator &ReadHash::const_iterator::backing() const
{ return i; }

const ReadHash::key_type &ReadHash::const_iterator::key() const
{ return i->first; }

ReadHash::mapped_type ReadHash::const_iterator::value() const
{ return (*parent)[i->first]; }

PathElement ReadHash::const_iterator::path() const
{ return PathElement(PathElement::Type::Hash, i->first); }


WriteHash::WriteHash() = default;

WriteHash::~WriteHash() = default;

WriteHash::WriteHash(const WriteHash &) = default;

WriteHash &WriteHash::operator=(const WriteHash &) = default;

WriteHash::WriteHash(WriteHash &&) = default;

WriteHash &WriteHash::operator=(WriteHash &&) = default;

WriteHash::WriteHash(Write &write) : ReadHash(write)
{ }

WriteHash::backend_map &WriteHash::backend()
{
    if (path.empty())
        return static_cast<NodeHash *>(top->write(Node::WriteGoal::Hash))->children;
    else
        return std::static_pointer_cast<NodeHash>(
                top->write(path, Node::WriteGoal::Hash))->children;
}

WriteHash::mapped_type WriteHash::operator[](const key_type &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Hash, key);
    return result;
}

WriteHash::mapped_type WriteHash::operator[](key_type &&key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Hash, std::move(key));
    return result;
}

WriteHash::mapped_type WriteHash::operator[](const char *key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Hash, key);
    return result;
}

WriteHash::mapped_type WriteHash::operator[](const QString &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Hash, key);
    return result;
}

WriteHash::iterator WriteHash::begin()
{ return WriteHash::iterator(*this, backend().begin()); }

WriteHash::iterator WriteHash::end()
{ return WriteHash::iterator(*this, backend().end()); }

WriteHash::iterator WriteHash::find(const key_type &key)
{ return WriteHash::iterator(*this, backend().find(key)); }

std::pair<WriteHash::iterator, bool> WriteHash::insert(const value_type &value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(value.first, std::move(source));
    return std::pair<WriteHash::iterator, bool>(WriteHash::iterator(*this, std::move(result.first)),
                                                result.second);
}

std::pair<WriteHash::iterator, bool> WriteHash::insert(value_type &&value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(std::move(value.first), std::move(source));
    return std::pair<WriteHash::iterator, bool>(WriteHash::iterator(*this, std::move(result.first)),
                                                result.second);
}

std::pair<WriteHash::iterator, bool> WriteHash::emplace(const key_type &key, const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(key, std::move(source));
    return std::pair<WriteHash::iterator, bool>(WriteHash::iterator(*this, std::move(result.first)),
                                                result.second);
}

std::pair<WriteHash::iterator, bool> WriteHash::emplace(key_type &&key, const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(std::move(key), std::move(source));
    return std::pair<WriteHash::iterator, bool>(WriteHash::iterator(*this, std::move(result.first)),
                                                result.second);
}

void WriteHash::merge(const ReadHash &other)
{
    auto &target = backend();
    for (const auto &node : other.backend()) {
        auto add = node.second;
        add->detach(add);
        target.emplace(node.first, std::move(add));
    }
}

WriteHash::iterator WriteHash::erase(const iterator &pos)
{ return WriteHash::iterator(*this, backend().erase(pos.i)); }

WriteHash::iterator WriteHash::erase(const iterator &first, const iterator &last)
{ return WriteHash::iterator(*this, backend().erase(first.i, last.i)); }

ReadHash::size_type WriteHash::erase(const key_type &key)
{ return backend().erase(key); }

WriteHash::iterator::iterator() = default;

WriteHash::iterator::~iterator() = default;

WriteHash::iterator::iterator(const iterator &) = default;

WriteHash::iterator &WriteHash::iterator::operator=(const iterator &) = default;

WriteHash::iterator::iterator(iterator &&) = default;

WriteHash::iterator &WriteHash::iterator::operator=(iterator &&) = default;

WriteHash::iterator::iterator(WriteHash &parent, backing_iterator &&i) : parent(&parent),
                                                                         i(std::move(i))
{ }

WriteHash::iterator::value_type WriteHash::iterator::operator*() const
{ return WriteHash::value_type(i->first, (*parent)[i->first]); }

WriteHash::iterator::pointer WriteHash::iterator::operator->() const
{
    static thread_local std::pair<WriteHash::key_type, WriteHash::mapped_type> ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

WriteHash::iterator &WriteHash::iterator::operator++()
{
    ++i;
    return *this;
}

WriteHash::iterator WriteHash::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool WriteHash::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteHash::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteHash::iterator::operator==(const ReadHash::const_iterator &other) const
{ return i == other.backing(); }

bool WriteHash::iterator::operator!=(const ReadHash::const_iterator &other) const
{ return i != other.backing(); }

WriteHash::iterator::operator ReadHash::const_iterator() const
{ return ReadHash::const_iterator(*parent, ReadHash::const_iterator::backing_iterator(i)); }

const WriteHash::iterator::backing_iterator &WriteHash::iterator::backing() const
{ return i; }

const WriteHash::key_type &WriteHash::iterator::key() const
{ return i->first; }

WriteHash::mapped_type WriteHash::iterator::value() const
{ return (*parent)[i->first]; }

PathElement WriteHash::iterator::path() const
{ return PathElement(PathElement::Type::Hash, i->first); }


ReadArray::ReadArray() = default;

ReadArray::~ReadArray() = default;

ReadArray::ReadArray(const ReadArray &) = default;

ReadArray &ReadArray::operator=(const ReadArray &) = default;

ReadArray::ReadArray(ReadArray &&) = default;

ReadArray &ReadArray::operator=(ReadArray &&) = default;

ReadArray::ReadArray(const Read &read) : top(read.top), path(read.path)
{ }

void ReadArray::swap(ReadArray &a, ReadArray &b)
{
    using std::swap;
    swap(a.top, b.top);
    swap(a.path, b.path);
}

const ReadArray::backend_array &ReadArray::backend() const
{
    static const ReadArray::backend_array invalid;
    if (path.empty()) {
        switch (top->node()->type()) {
        case Node::Type::Array:
        case Node::Type::ArrayConstant:
            break;
        default:
            return invalid;
        }
        return top->node<NodeArray>()->children;
    }

    auto ptr = top->read(path);
    if (!ptr)
        return invalid;

    switch (ptr->type()) {
    case Node::Type::Array:
    case Node::Type::ArrayConstant:
        break;
    default:
        return invalid;
    }

    /* Safe to keep the pointer as long as the root remains unchanged, since it will
     * have a pointer to it regardless.  Any write access invalidates this,
     * however. */
    return static_cast<const NodeArray *>(ptr.get())->children;
}

bool ReadArray::empty() const
{ return backend().empty(); }

ReadArray::size_type ReadArray::size() const
{ return backend().size(); }

ReadArray::value_type ReadArray::front() const
{
    value_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Array, 0);
    return result;
}

ReadArray::value_type ReadArray::back() const
{
    value_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Array, size() - 1);
    return result;
}

ReadArray::value_type ReadArray::operator[](size_type index) const
{
    value_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Array, index);
    return result;
}

bool ReadArray::operator==(const ReadArray &other) const
{
    const auto &a = backend();
    const auto &b = other.backend();
    if (a.size() != b.size())
        return false;
    for (auto ca = a.begin(), cb = b.begin(), endA = a.end(); ca != endA; ++ca, ++cb) {
        if (!(*ca)->identicalTo(*cb->get()))
            return false;
    }
    return true;
}

bool ReadArray::operator!=(const ReadArray &other) const
{ return !(*this == other); }

ReadArray::const_iterator ReadArray::begin() const
{ return ReadArray::const_iterator(*this, 0); }

ReadArray::const_iterator ReadArray::end() const
{ return ReadArray::const_iterator(*this, backend().size()); }


ReadArray::const_iterator::const_iterator() = default;

ReadArray::const_iterator::~const_iterator() = default;

ReadArray::const_iterator::const_iterator(const ReadArray::const_iterator &) = default;

ReadArray::const_iterator &ReadArray::const_iterator::operator=(const ReadArray::const_iterator &) = default;

ReadArray::const_iterator::const_iterator(ReadArray::const_iterator &&) = default;

ReadArray::const_iterator &ReadArray::const_iterator::operator=(ReadArray::const_iterator &&) = default;

ReadArray::const_iterator::const_iterator(const ReadArray &parent, ReadArray::size_type i) : parent(
        &parent), i(i)
{ }

ReadArray::const_iterator::value_type ReadArray::const_iterator::operator*() const
{ return (*parent)[i]; }

ReadArray::const_iterator::pointer ReadArray::const_iterator::operator->() const
{
    static thread_local value_type ref;
    ref = (*parent)[i];
    return &ref;
}

ReadArray::const_iterator::value_type ReadArray::const_iterator::operator[](difference_type index) const
{ return (*parent)[i + index]; }

ReadArray::const_iterator &ReadArray::const_iterator::operator++()
{
    ++i;
    return *this;
}

ReadArray::const_iterator ReadArray::const_iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

ReadArray::const_iterator &ReadArray::const_iterator::operator--()
{
    ++i;
    return *this;
}

ReadArray::const_iterator ReadArray::const_iterator::operator--(int)
{
    auto save = *this;
    ++i;
    return save;
}

ReadArray::const_iterator &ReadArray::const_iterator::operator+=(difference_type op)
{
    i += op;
    return *this;
}

ReadArray::const_iterator &ReadArray::const_iterator::operator-=(difference_type op)
{
    i -= op;
    return *this;
}

ReadArray::const_iterator ReadArray::const_iterator::operator+(difference_type op) const
{
    auto result = *this;
    result.i += op;
    return result;
}

ReadArray::const_iterator ReadArray::const_iterator::operator-(difference_type op) const
{
    auto result = *this;
    result.i -= op;
    return result;
}

ReadArray::difference_type ReadArray::const_iterator::operator-(const const_iterator &other) const
{ return i - other.i; }

bool ReadArray::const_iterator::operator==(const ReadArray::const_iterator &other) const
{ return i == other.i; }

bool ReadArray::const_iterator::operator!=(const ReadArray::const_iterator &other) const
{ return i != other.i; }

bool ReadArray::const_iterator::operator<(const const_iterator &other) const
{ return i < other.i; }

bool ReadArray::const_iterator::operator<=(const const_iterator &other) const
{ return i <= other.i; }

bool ReadArray::const_iterator::operator>(const const_iterator &other) const
{ return i > other.i; }

bool ReadArray::const_iterator::operator>=(const const_iterator &other) const
{ return i >= other.i; }

ReadArray::size_type ReadArray::const_iterator::backing() const
{ return i; }

PathElement ReadArray::const_iterator::path() const
{ return PathElement(PathElement::Type::Array, i); }


WriteArray::WriteArray() = default;

WriteArray::~WriteArray() = default;

WriteArray::WriteArray(const WriteArray &) = default;

WriteArray &WriteArray::operator=(const WriteArray &) = default;

WriteArray::WriteArray(WriteArray &&) = default;

WriteArray &WriteArray::operator=(WriteArray &&) = default;

WriteArray::WriteArray(Write &write) : ReadArray(write)
{ }

WriteArray::backend_array &WriteArray::backend()
{
    if (path.empty())
        return static_cast<NodeArray *>(top->write(Node::WriteGoal::Array))->children;
    else
        return std::static_pointer_cast<NodeArray>(
                top->write(path, Node::WriteGoal::Array))->children;
}

WriteArray::value_type WriteArray::front()
{
    value_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Array, 0);
    return result;
}

WriteArray::value_type WriteArray::back()
{
    value_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Array, size() - 1);
    return result;
}

WriteArray::value_type WriteArray::operator[](size_type index)
{
    value_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Array, index);
    return result;
}

WriteArray::iterator WriteArray::begin()
{ return WriteArray::iterator(*this, 0); }

WriteArray::iterator WriteArray::end()
{ return WriteArray::iterator(*this, backend().size()); }

void WriteArray::resize(size_type count)
{ backend().resize(count, NodeEmpty::instance); }

void WriteArray::resize(size_type count, const Read &value)
{
    auto &target = backend();
    if (count <= target.size()) {
        target.resize(count);
        return;
    }
    /* Detached values are constant, so we can duplicate it */
    target.resize(count, value.top->detached(value.path));
}

void WriteArray::push_back(const Read &value)
{
    auto &target = backend();
    target.push_back(value.top->detached(value.path));
}

WriteArray::value_type WriteArray::after_back()
{
    value_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Array, size());
    return result;
}

void WriteArray::push_front(const Read &value)
{
    auto &target = backend();
    target.insert(target.begin(), value.top->detached(value.path));
}

WriteArray::iterator WriteArray::insert(const const_iterator &pos, const Read &value)
{
    auto &target = backend();
    target.insert(target.begin() + pos.backing(), value.top->detached(value.path));
    return WriteArray::iterator(*this, pos.backing());
}

WriteArray::iterator WriteArray::insert(const const_iterator &pos,
                                        size_type count,
                                        const Read &value)
{
    auto &target = backend();
    /* Detached values are constant, so we can duplicate it */
    target.insert(target.begin() + pos.backing(), count, value.top->detached(value.path));
    return WriteArray::iterator(*this, pos.backing());
}

WriteArray::iterator WriteArray::insert(const ReadArray::const_iterator &pos,
                                        std::initializer_list<Read> ilist)
{
    return insert(pos, ilist.begin(), ilist.end());
}

void WriteArray::clear()
{ backend().clear(); }

void WriteArray::pop_back()
{ backend().pop_back(); }

void WriteArray::pop_front()
{
    auto &target = backend();
    target.erase(target.begin());
}

WriteArray::iterator WriteArray::erase(const const_iterator &pos)
{
    auto &target = backend();
    target.erase(target.begin() + pos.backing());
    return WriteArray::iterator(*this, pos.backing());
}

WriteArray::iterator WriteArray::erase(size_type pos)
{
    auto &target = backend();
    target.erase(target.begin() + pos);
    return WriteArray::iterator(*this, pos);
}

WriteArray::iterator WriteArray::erase(const const_iterator &first, const const_iterator &last)
{
    auto &target = backend();
    target.erase(target.begin() + first.backing(), target.begin() + last.backing());
    return WriteArray::iterator(*this, first.backing());
}


WriteArray::iterator::iterator() = default;

WriteArray::iterator::~iterator() = default;

WriteArray::iterator::iterator(const WriteArray::iterator &) = default;

WriteArray::iterator &WriteArray::iterator::operator=(const WriteArray::iterator &) = default;

WriteArray::iterator::iterator(WriteArray::iterator &&) = default;

WriteArray::iterator &WriteArray::iterator::operator=(WriteArray::iterator &&) = default;

WriteArray::iterator::iterator(WriteArray &parent, WriteArray::size_type i) : parent(&parent), i(i)
{ }

WriteArray::iterator::value_type WriteArray::iterator::operator*() const
{ return (*parent)[i]; }

WriteArray::iterator::pointer WriteArray::iterator::operator->() const
{
    static thread_local value_type ref;
    ref = (*parent)[i];
    return &ref;
}

WriteArray::iterator::value_type WriteArray::iterator::operator[](difference_type index) const
{ return (*parent)[i + index]; }

WriteArray::iterator &WriteArray::iterator::operator++()
{
    ++i;
    return *this;
}

WriteArray::iterator WriteArray::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

WriteArray::iterator &WriteArray::iterator::operator--()
{
    ++i;
    return *this;
}

WriteArray::iterator WriteArray::iterator::operator--(int)
{
    auto save = *this;
    ++i;
    return save;
}

WriteArray::iterator &WriteArray::iterator::operator+=(difference_type op)
{
    i += op;
    return *this;
}

WriteArray::iterator &WriteArray::iterator::operator-=(difference_type op)
{
    i -= op;
    return *this;
}

WriteArray::iterator WriteArray::iterator::operator+(difference_type op) const
{
    auto result = *this;
    result.i += op;
    return result;
}

WriteArray::iterator WriteArray::iterator::operator-(difference_type op) const
{
    auto result = *this;
    result.i -= op;
    return result;
}

WriteArray::difference_type WriteArray::iterator::operator-(const iterator &other) const
{ return i - other.i; }

WriteArray::difference_type WriteArray::iterator::operator-(const ReadArray::const_iterator &other) const
{ return i - other.backing(); }

bool WriteArray::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteArray::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteArray::iterator::operator<(const iterator &other) const
{ return i < other.i; }

bool WriteArray::iterator::operator<=(const iterator &other) const
{ return i <= other.i; }

bool WriteArray::iterator::operator>(const iterator &other) const
{ return i > other.i; }

bool WriteArray::iterator::operator>=(const iterator &other) const
{ return i >= other.i; }

bool WriteArray::iterator::operator==(const ReadArray::const_iterator &other) const
{ return i == other.backing(); }

bool WriteArray::iterator::operator!=(const ReadArray::const_iterator &other) const
{ return i != other.backing(); }

bool WriteArray::iterator::operator<(const ReadArray::const_iterator &other) const
{ return i < other.backing(); }

bool WriteArray::iterator::operator<=(const ReadArray::const_iterator &other) const
{ return i <= other.backing(); }

bool WriteArray::iterator::operator>(const ReadArray::const_iterator &other) const
{ return i > other.backing(); }

bool WriteArray::iterator::operator>=(const ReadArray::const_iterator &other) const
{ return i >= other.backing(); }

WriteArray::size_type WriteArray::iterator::backing() const
{ return i; }

PathElement WriteArray::iterator::path() const
{ return PathElement(PathElement::Type::Array, i); }

WriteArray::iterator::operator ReadArray::const_iterator() const
{ return ReadArray::const_iterator(*parent, i); }


ReadMatrix::ReadMatrix() = default;

ReadMatrix::~ReadMatrix() = default;

ReadMatrix::ReadMatrix(const ReadMatrix &) = default;

ReadMatrix &ReadMatrix::operator=(const ReadMatrix &) = default;

ReadMatrix::ReadMatrix(ReadMatrix &&) = default;

ReadMatrix &ReadMatrix::operator=(ReadMatrix &&) = default;

ReadMatrix::ReadMatrix(const Read &read) : top(read.top), path(read.path)
{ }

void ReadMatrix::swap(ReadMatrix &a, ReadMatrix &b)
{
    using std::swap;
    swap(a.top, b.top);
    swap(a.path, b.path);
}

const ReadMatrix::backend_array &ReadMatrix::backend() const
{
    static const ReadMatrix::backend_array invalid;
    if (path.empty()) {
        switch (top->node()->type()) {
        case Node::Type::Matrix:
        case Node::Type::MatrixConstant:
            break;
        default:
            return invalid;
        }
        return top->node<NodeMatrix>()->children;
    }

    auto ptr = top->read(path);
    if (!ptr)
        return invalid;

    switch (ptr->type()) {
    case Node::Type::Matrix:
    case Node::Type::MatrixConstant:
        break;
    default:
        return invalid;
    }

    /* Safe to keep the pointer as long as the root remains unchanged, since it will
     * have a pointer to it regardless.  Any write access invalidates this,
     * however. */
    return static_cast<const NodeMatrix *>(ptr.get())->children;
}

bool ReadMatrix::empty() const
{ return backend().empty(); }

ReadMatrix::size_type ReadMatrix::size() const
{ return backend().size(); }

ReadMatrix::mapped_type ReadMatrix::front() const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Array, 0);
    return result;
}

ReadMatrix::mapped_type ReadMatrix::back() const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Array, size() - 1);
    return result;
}

ReadMatrix::key_type ReadMatrix::key(size_type origin) const
{
    if (path.empty()) {
        switch (top->node()->type()) {
        case Node::Type::Matrix:
        case Node::Type::MatrixConstant:
            break;
        default:
            return key_type();
        }
        return top->node<NodeMatrix>()->fromIndex(origin);
    }

    auto ptr = top->read(path);
    if (!ptr)
        return key_type();

    switch (ptr->type()) {
    case Node::Type::Matrix:
    case Node::Type::MatrixConstant:
        break;
    default:
        return key_type();
    }

    return static_cast<const NodeMatrix *>(ptr.get())->fromIndex(origin);
}

ReadMatrix::size_type ReadMatrix::index(const key_type &key) const
{
    if (path.empty()) {
        switch (top->node()->type()) {
        case Node::Type::Matrix:
        case Node::Type::MatrixConstant:
            break;
        default:
            return 0;
        }
        return top->node<NodeMatrix>()->toIndex(key);
    }

    auto ptr = top->read(path);
    if (!ptr)
        return 0;

    switch (ptr->type()) {
    case Node::Type::Matrix:
    case Node::Type::MatrixConstant:
        break;
    default:
        return 0;
    }

    return static_cast<const NodeMatrix *>(ptr.get())->toIndex(key);
}

ReadMatrix::key_type ReadMatrix::key(size_type origin, const key_type &shape)
{ return NodeMatrix::fromIndex(origin, shape); }

ReadMatrix::size_type ReadMatrix::index(const key_type &key, const key_type &shape)
{ return NodeMatrix::toIndex(key, shape); }

ReadMatrix::key_type ReadMatrix::shape() const
{
    if (path.empty()) {
        switch (top->node()->type()) {
        case Node::Type::Matrix:
        case Node::Type::MatrixConstant:
            break;
        default:
            return key_type();
        }
        return top->node<NodeMatrix>()->size;
    }

    auto ptr = top->read(path);
    if (!ptr)
        return key_type();

    switch (ptr->type()) {
    case Node::Type::Matrix:
    case Node::Type::MatrixConstant:
        break;
    default:
        return key_type();
    }

    return static_cast<const NodeMatrix *>(ptr.get())->size;
}

ReadMatrix::mapped_type ReadMatrix::operator[](size_type index) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Array, index);
    return result;
}

ReadMatrix::mapped_type ReadMatrix::operator[](const key_type &index) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Matrix, index);
    return result;
}

ReadMatrix::mapped_type ReadMatrix::operator[](key_type &&index) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Matrix, std::move(index));
    return result;
}

bool ReadMatrix::operator==(const ReadMatrix &other) const
{
    const auto &a = backend();
    const auto &b = other.backend();
    if (a.size() != b.size())
        return false;
    for (auto ca = a.begin(), cb = b.begin(), endA = a.end(); ca != endA; ++ca, ++cb) {
        if (!(*ca)->identicalTo(*cb->get()))
            return false;
    }
    return true;
}

bool ReadMatrix::operator!=(const ReadMatrix &other) const
{ return !(*this == other); }

ReadMatrix::const_iterator ReadMatrix::begin() const
{ return ReadMatrix::const_iterator(*this, 0); }

ReadMatrix::const_iterator ReadMatrix::end() const
{ return ReadMatrix::const_iterator(*this, backend().size()); }

ReadMatrix::const_iterator ReadMatrix::find(const key_type &index) const
{
    if (path.empty()) {
        switch (top->node()->type()) {
        case Node::Type::Matrix:
        case Node::Type::MatrixConstant:
            break;
        default:
            return const_iterator(*this, size() - 1);
        }
        const auto *node = top->node<NodeMatrix>();
        if (!node->indexInBounds(index))
            return const_iterator(*this, size() - 1);
        return const_iterator(*this, node->toIndex(index));
    }

    auto ptr = top->read(path);
    if (!ptr)
        return const_iterator(*this, size() - 1);

    switch (ptr->type()) {
    case Node::Type::Matrix:
    case Node::Type::MatrixConstant:
        break;
    default:
        return const_iterator(*this, size() - 1);
    }

    const auto *node = static_cast<const NodeMatrix *>(ptr.get());
    if (!node->indexInBounds(index))
        return const_iterator(*this, size() - 1);
    return const_iterator(*this, node->toIndex(index));
}

ReadMatrix::const_iterator::const_iterator() = default;

ReadMatrix::const_iterator::~const_iterator() = default;

ReadMatrix::const_iterator::const_iterator(const ReadMatrix::const_iterator &) = default;

ReadMatrix::const_iterator &ReadMatrix::const_iterator::operator=(const ReadMatrix::const_iterator &) = default;

ReadMatrix::const_iterator::const_iterator(ReadMatrix::const_iterator &&) = default;

ReadMatrix::const_iterator &ReadMatrix::const_iterator::operator=(ReadMatrix::const_iterator &&) = default;

ReadMatrix::const_iterator::const_iterator(const ReadMatrix &parent, ReadMatrix::size_type i)
        : parent(&parent), i(i)
{ }

ReadMatrix::const_iterator::value_type ReadMatrix::const_iterator::operator*() const
{
    auto key = parent->key(i);
    auto v = (*parent)[key];
    return ReadMatrix::value_type(std::move(key), std::move(v));
}

ReadMatrix::const_iterator::pointer ReadMatrix::const_iterator::operator->() const
{
    static thread_local std::pair<ReadMatrix::key_type, ReadMatrix::mapped_type> ref;
    ref.first = parent->key(i);
    ref.second = (*parent)[ref.first];
    return &ref;
}

ReadMatrix::mapped_type ReadMatrix::const_iterator::operator[](difference_type index) const
{ return (*parent)[i + index]; }

ReadMatrix::const_iterator &ReadMatrix::const_iterator::operator++()
{
    ++i;
    return *this;
}

ReadMatrix::const_iterator ReadMatrix::const_iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

ReadMatrix::const_iterator &ReadMatrix::const_iterator::operator--()
{
    ++i;
    return *this;
}

ReadMatrix::const_iterator ReadMatrix::const_iterator::operator--(int)
{
    auto save = *this;
    ++i;
    return save;
}

ReadMatrix::const_iterator &ReadMatrix::const_iterator::operator+=(difference_type op)
{
    i += op;
    return *this;
}

ReadMatrix::const_iterator &ReadMatrix::const_iterator::operator-=(difference_type op)
{
    i -= op;
    return *this;
}

ReadMatrix::const_iterator ReadMatrix::const_iterator::operator+(difference_type op) const
{
    auto result = *this;
    result.i += op;
    return result;
}

ReadMatrix::const_iterator ReadMatrix::const_iterator::operator-(difference_type op) const
{
    auto result = *this;
    result.i -= op;
    return result;
}

ReadMatrix::difference_type ReadMatrix::const_iterator::operator-(const const_iterator &other) const
{ return i - other.i; }

bool ReadMatrix::const_iterator::operator==(const ReadMatrix::const_iterator &other) const
{ return i == other.i; }

bool ReadMatrix::const_iterator::operator!=(const ReadMatrix::const_iterator &other) const
{ return i != other.i; }

bool ReadMatrix::const_iterator::operator<(const const_iterator &other) const
{ return i < other.i; }

bool ReadMatrix::const_iterator::operator<=(const const_iterator &other) const
{ return i <= other.i; }

bool ReadMatrix::const_iterator::operator>(const const_iterator &other) const
{ return i > other.i; }

bool ReadMatrix::const_iterator::operator>=(const const_iterator &other) const
{ return i >= other.i; }

ReadMatrix::size_type ReadMatrix::const_iterator::backing() const
{ return i; }

ReadMatrix::key_type ReadMatrix::const_iterator::key() const
{ return parent->key(i); }

ReadMatrix::mapped_type ReadMatrix::const_iterator::value() const
{ return (*parent)[parent->key(i)]; }

PathElement ReadMatrix::const_iterator::path() const
{ return PathElement(PathElement::Type::Matrix, parent->key(i)); }


WriteMatrix::WriteMatrix() = default;

WriteMatrix::~WriteMatrix() = default;

WriteMatrix::WriteMatrix(const WriteMatrix &) = default;

WriteMatrix &WriteMatrix::operator=(const WriteMatrix &) = default;

WriteMatrix::WriteMatrix(WriteMatrix &&) = default;

WriteMatrix &WriteMatrix::operator=(WriteMatrix &&) = default;

WriteMatrix::WriteMatrix(Write &write) : ReadMatrix(write)
{ }

WriteMatrix::backend_array &WriteMatrix::backend()
{
    if (path.empty())
        return static_cast<NodeMatrix *>(top->write(Node::WriteGoal::Matrix))->children;
    else
        return std::static_pointer_cast<NodeMatrix>(
                top->write(path, Node::WriteGoal::Matrix))->children;
}

WriteMatrix::mapped_type WriteMatrix::front()
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Array, 0);
    return result;
}

WriteMatrix::mapped_type WriteMatrix::back()
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Array, size() - 1);
    return result;
}

WriteMatrix::mapped_type WriteMatrix::after_back()
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Array, size() - 1);
    return result;
}

WriteMatrix::mapped_type WriteMatrix::operator[](size_type index)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Array, index);
    return result;
}

WriteMatrix::mapped_type WriteMatrix::operator[](const key_type &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Matrix, key);
    return result;
}

WriteMatrix::mapped_type WriteMatrix::operator[](key_type &&key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Matrix, std::move(key));
    return result;
}

WriteMatrix::iterator WriteMatrix::begin()
{ return WriteMatrix::iterator(*this, 0); }

WriteMatrix::iterator WriteMatrix::end()
{ return WriteMatrix::iterator(*this, backend().size()); }

void WriteMatrix::resize(size_type count)
{ backend().resize(count, NodeEmpty::instance); }

void WriteMatrix::resize(size_type count, const Read &value)
{
    auto &target = backend();
    if (count <= target.size()) {
        target.resize(count);
        return;
    }
    /* Detached values are constant, so we can duplicate it */
    target.resize(count, value.top->detached(value.path));
}

void WriteMatrix::reshape(const key_type &shape)
{
    if (path.empty()) {
        static_cast<NodeMatrix *>(top->write(Node::WriteGoal::Matrix))->reshape(shape);
    } else {
        std::static_pointer_cast<NodeMatrix>(top->write(path, Node::WriteGoal::Matrix))->reshape(
                shape);
    }
}

void WriteMatrix::reshape(const key_type &shape, const Read &value)
{
    NodeMatrix *target = nullptr;
    if (path.empty()) {
        target = static_cast<NodeMatrix *>(top->write(Node::WriteGoal::Matrix));
    } else {
        target = std::static_pointer_cast<NodeMatrix>(
                top->write(path, Node::WriteGoal::Matrix)).get();
    }

    /* Detached values are constant, so we can duplicate it */
    target->reshape(shape, value.top->detached(value.path));
}

void WriteMatrix::push_back(const Read &value)
{
    auto &target = backend();
    target.push_back(value.top->detached(value.path));
}

void WriteMatrix::push_front(const Read &value)
{
    auto &target = backend();
    target.insert(target.begin(), value.top->detached(value.path));
}

WriteMatrix::iterator WriteMatrix::insert(const const_iterator &pos, const Read &value)
{
    auto &target = backend();
    target.insert(target.begin() + pos.backing(), value.top->detached(value.path));
    return WriteMatrix::iterator(*this, pos.backing());
}

WriteMatrix::iterator WriteMatrix::insert(const const_iterator &pos,
                                          size_type count,
                                          const Read &value)
{
    auto &target = backend();
    /* Detached values are constant, so we can duplicate it */
    target.insert(target.begin() + pos.backing(), count, value.top->detached(value.path));
    return WriteMatrix::iterator(*this, pos.backing());
}

WriteMatrix::iterator WriteMatrix::insert(const ReadMatrix::const_iterator &pos,
                                          std::initializer_list<Read> ilist)
{
    return insert(pos, ilist.begin(), ilist.end());
}

void WriteMatrix::clear()
{ backend().clear(); }

void WriteMatrix::pop_back()
{ backend().pop_back(); }

void WriteMatrix::pop_front()
{
    auto &target = backend();
    target.erase(target.begin());
}

WriteMatrix::iterator WriteMatrix::erase(const const_iterator &pos)
{
    auto &target = backend();
    target.erase(target.begin() + pos.backing());
    return WriteMatrix::iterator(*this, pos.backing());
}

WriteMatrix::iterator WriteMatrix::erase(size_type pos)
{
    auto &target = backend();
    target.erase(target.begin() + pos);
    return WriteMatrix::iterator(*this, pos);
}

WriteMatrix::iterator WriteMatrix::erase(const const_iterator &first, const const_iterator &last)
{
    auto &target = backend();
    target.erase(target.begin() + first.backing(), target.begin() + last.backing());
    return WriteMatrix::iterator(*this, first.backing());
}

WriteMatrix::iterator::iterator() = default;

WriteMatrix::iterator::~iterator() = default;

WriteMatrix::iterator::iterator(const WriteMatrix::iterator &) = default;

WriteMatrix::iterator &WriteMatrix::iterator::operator=(const WriteMatrix::iterator &) = default;

WriteMatrix::iterator::iterator(WriteMatrix::iterator &&) = default;

WriteMatrix::iterator &WriteMatrix::iterator::operator=(WriteMatrix::iterator &&) = default;

WriteMatrix::iterator::iterator(WriteMatrix &parent, WriteMatrix::size_type i) : parent(&parent),
                                                                                 i(i)
{ }

WriteMatrix::iterator::value_type WriteMatrix::iterator::operator*() const
{
    auto key = parent->key(i);
    auto v = (*parent)[key];
    return WriteMatrix::value_type(std::move(key), std::move(v));
}

WriteMatrix::iterator::pointer WriteMatrix::iterator::operator->() const
{
    static thread_local std::pair<WriteMatrix::key_type, WriteMatrix::mapped_type> ref;
    ref.first = parent->key(i);
    ref.second = (*parent)[ref.first];
    return &ref;
}

WriteMatrix::mapped_type WriteMatrix::iterator::operator[](difference_type index) const
{ return (*parent)[i + index]; }

WriteMatrix::iterator &WriteMatrix::iterator::operator++()
{
    ++i;
    return *this;
}

WriteMatrix::iterator WriteMatrix::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

WriteMatrix::iterator &WriteMatrix::iterator::operator--()
{
    ++i;
    return *this;
}

WriteMatrix::iterator WriteMatrix::iterator::operator--(int)
{
    auto save = *this;
    ++i;
    return save;
}

WriteMatrix::iterator &WriteMatrix::iterator::operator+=(difference_type op)
{
    i += op;
    return *this;
}

WriteMatrix::iterator &WriteMatrix::iterator::operator-=(difference_type op)
{
    i -= op;
    return *this;
}

WriteMatrix::iterator WriteMatrix::iterator::operator+(difference_type op) const
{
    auto result = *this;
    result.i += op;
    return result;
}

WriteMatrix::iterator WriteMatrix::iterator::operator-(difference_type op) const
{
    auto result = *this;
    result.i -= op;
    return result;
}

WriteMatrix::difference_type WriteMatrix::iterator::operator-(const iterator &other) const
{ return i - other.i; }

WriteMatrix::difference_type WriteMatrix::iterator::operator-(const ReadMatrix::const_iterator &other) const
{ return i - other.backing(); }

bool WriteMatrix::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteMatrix::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteMatrix::iterator::operator<(const iterator &other) const
{ return i < other.i; }

bool WriteMatrix::iterator::operator<=(const iterator &other) const
{ return i <= other.i; }

bool WriteMatrix::iterator::operator>(const iterator &other) const
{ return i > other.i; }

bool WriteMatrix::iterator::operator>=(const iterator &other) const
{ return i >= other.i; }

bool WriteMatrix::iterator::operator==(const ReadMatrix::const_iterator &other) const
{ return i == other.backing(); }

bool WriteMatrix::iterator::operator!=(const ReadMatrix::const_iterator &other) const
{ return i != other.backing(); }

bool WriteMatrix::iterator::operator<(const ReadMatrix::const_iterator &other) const
{ return i < other.backing(); }

bool WriteMatrix::iterator::operator<=(const ReadMatrix::const_iterator &other) const
{ return i <= other.backing(); }

bool WriteMatrix::iterator::operator>(const ReadMatrix::const_iterator &other) const
{ return i > other.backing(); }

bool WriteMatrix::iterator::operator>=(const ReadMatrix::const_iterator &other) const
{ return i >= other.backing(); }

WriteMatrix::size_type WriteMatrix::iterator::backing() const
{ return i; }

WriteMatrix::iterator::operator ReadMatrix::const_iterator() const
{ return ReadMatrix::const_iterator(*parent, i); }

ReadMatrix::key_type WriteMatrix::iterator::key() const
{ return parent->key(i); }

WriteMatrix::mapped_type WriteMatrix::iterator::value() const
{ return (*parent)[parent->key(i)]; }

PathElement WriteMatrix::iterator::path() const
{ return PathElement(PathElement::Type::Matrix, parent->key(i)); }


ReadKeyframe::ReadKeyframe() = default;

ReadKeyframe::~ReadKeyframe() = default;

ReadKeyframe::ReadKeyframe(const ReadKeyframe &) = default;

ReadKeyframe &ReadKeyframe::operator=(const ReadKeyframe &) = default;

ReadKeyframe::ReadKeyframe(ReadKeyframe &&) = default;

ReadKeyframe &ReadKeyframe::operator=(ReadKeyframe &&) = default;

ReadKeyframe::ReadKeyframe(const Read &read) : top(read.top), path(read.path)
{ }

void ReadKeyframe::swap(ReadKeyframe &a, ReadKeyframe &b)
{
    using std::swap;
    swap(a.top, b.top);
    swap(a.path, b.path);
}

const ReadKeyframe::backend_map &ReadKeyframe::backend() const
{
    static const ReadKeyframe::backend_map invalid;
    if (path.empty()) {
        switch (top->node()->type()) {
        case Node::Type::Keyframe:
        case Node::Type::KeyframeConstant:
            break;
        default:
            return invalid;
        }
        return top->node<NodeKeyframe>()->children;
    }

    auto ptr = top->read(path);
    if (!ptr)
        return invalid;

    switch (ptr->type()) {
    case Node::Type::Keyframe:
    case Node::Type::KeyframeConstant:
        break;
    default:
        return invalid;
    }

    /* Safe to keep the pointer as long as the root remains unchanged, since it will
     * have a pointer to it regardless.  Any write access invalidates this,
     * however. */
    return static_cast<const NodeKeyframe *>(ptr.get())->children;
}

bool ReadKeyframe::empty() const
{ return backend().empty(); }

ReadKeyframe::size_type ReadKeyframe::size() const
{ return backend().size(); }

ReadKeyframe::size_type ReadKeyframe::count(key_type key) const
{ return backend().count(key); }

bool ReadKeyframe::operator==(const ReadKeyframe &other) const
{
    const auto &a = backend();
    const auto &b = other.backend();
    if (a.size() != b.size())
        return false;
    for (const auto &check : a) {
        auto i = b.find(check.first);
        if (i == b.end())
            return false;
        if (!check.second->identicalTo(*i->second))
            return false;
    }
    return true;
}

bool ReadKeyframe::operator!=(const ReadKeyframe &other) const
{ return !(*this == other); }

ReadKeyframe::mapped_type ReadKeyframe::operator[](key_type key) const
{
    Q_ASSERT(FP::defined(key));
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Keyframe, key);
    return result;
}

ReadKeyframe::const_iterator ReadKeyframe::begin() const
{ return ReadKeyframe::const_iterator(*this, backend().begin()); }

ReadKeyframe::const_iterator ReadKeyframe::end() const
{ return ReadKeyframe::const_iterator(*this, backend().end()); }

ReadKeyframe::const_iterator ReadKeyframe::find(key_type key) const
{
    Q_ASSERT(FP::defined(key));
    return ReadKeyframe::const_iterator(*this, backend().find(key));
}

ReadKeyframe::const_iterator ReadKeyframe::lower_bound(key_type key) const
{
    Q_ASSERT(FP::defined(key));
    return ReadKeyframe::const_iterator(*this, backend().lower_bound(key));
}

ReadKeyframe::const_iterator ReadKeyframe::upper_bound(key_type key) const
{
    Q_ASSERT(FP::defined(key));
    return ReadKeyframe::const_iterator(*this, backend().upper_bound(key));
}

std::vector<ReadKeyframe::key_type> ReadKeyframe::keys() const
{
    std::vector<ReadKeyframe::key_type> result;
    for (const auto &add : backend()) {
        result.emplace_back(add.first);
    }
    return result;
}

ReadKeyframe::const_iterator::const_iterator() = default;

ReadKeyframe::const_iterator::~const_iterator() = default;

ReadKeyframe::const_iterator::const_iterator(const const_iterator &) = default;

ReadKeyframe::const_iterator &ReadKeyframe::const_iterator::operator=(const const_iterator &) = default;

ReadKeyframe::const_iterator::const_iterator(const_iterator &&) = default;

ReadKeyframe::const_iterator &ReadKeyframe::const_iterator::operator=(const_iterator &&) = default;

ReadKeyframe::const_iterator::const_iterator(const ReadKeyframe &parent, backing_iterator &&i)
        : parent(&parent), i(std::move(i))
{ }

ReadKeyframe::const_iterator::value_type ReadKeyframe::const_iterator::operator*() const
{ return ReadKeyframe::value_type(i->first, (*parent)[i->first]); }

ReadKeyframe::const_iterator::pointer ReadKeyframe::const_iterator::operator->() const
{
    static thread_local std::pair<ReadKeyframe::key_type, ReadKeyframe::mapped_type> ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

ReadKeyframe::const_iterator &ReadKeyframe::const_iterator::operator++()
{
    ++i;
    return *this;
}

ReadKeyframe::const_iterator ReadKeyframe::const_iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

ReadKeyframe::const_iterator &ReadKeyframe::const_iterator::operator--()
{
    --i;
    return *this;
}

ReadKeyframe::const_iterator ReadKeyframe::const_iterator::operator--(int)
{
    auto save = *this;
    --i;
    return save;
}

bool ReadKeyframe::const_iterator::operator==(const const_iterator &other) const
{ return i == other.i; }

bool ReadKeyframe::const_iterator::operator!=(const const_iterator &other) const
{ return i != other.i; }

const ReadKeyframe::const_iterator::backing_iterator &ReadKeyframe::const_iterator::backing() const
{ return i; }

ReadKeyframe::key_type ReadKeyframe::const_iterator::key() const
{ return i->first; }

ReadKeyframe::mapped_type ReadKeyframe::const_iterator::value() const
{ return (*parent)[i->first]; }

PathElement ReadKeyframe::const_iterator::path() const
{ return PathElement(PathElement::Type::Keyframe, i->first); }


WriteKeyframe::WriteKeyframe() = default;

WriteKeyframe::~WriteKeyframe() = default;

WriteKeyframe::WriteKeyframe(const WriteKeyframe &) = default;

WriteKeyframe &WriteKeyframe::operator=(const WriteKeyframe &) = default;

WriteKeyframe::WriteKeyframe(WriteKeyframe &&) = default;

WriteKeyframe &WriteKeyframe::operator=(WriteKeyframe &&) = default;

WriteKeyframe::WriteKeyframe(Write &write) : ReadKeyframe(write)
{ }

WriteKeyframe::backend_map &WriteKeyframe::backend()
{
    if (path.empty())
        return static_cast<NodeKeyframe *>(top->write(Node::WriteGoal::Keyframe))->children;
    else
        return std::static_pointer_cast<NodeKeyframe>(
                top->write(path, Node::WriteGoal::Keyframe))->children;
}

WriteKeyframe::mapped_type WriteKeyframe::operator[](key_type key)
{
    Q_ASSERT(FP::defined(key));
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::Keyframe, key);
    return result;
}

WriteKeyframe::iterator WriteKeyframe::begin()
{ return WriteKeyframe::iterator(*this, backend().begin()); }

WriteKeyframe::iterator WriteKeyframe::end()
{ return WriteKeyframe::iterator(*this, backend().end()); }

WriteKeyframe::iterator WriteKeyframe::find(key_type key)
{
    Q_ASSERT(FP::defined(key));
    return WriteKeyframe::iterator(*this, backend().find(key));
}

WriteKeyframe::iterator WriteKeyframe::lower_bound(key_type key)
{
    Q_ASSERT(FP::defined(key));
    return WriteKeyframe::iterator(*this, backend().lower_bound(key));
}

WriteKeyframe::iterator WriteKeyframe::upper_bound(key_type key)
{
    Q_ASSERT(FP::defined(key));
    return WriteKeyframe::iterator(*this, backend().upper_bound(key));
}

std::pair<WriteKeyframe::iterator, bool> WriteKeyframe::insert(const value_type &value)
{
    Q_ASSERT(FP::defined(value.first));
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(value.first, std::move(source));
    return std::pair<WriteKeyframe::iterator, bool>(
            WriteKeyframe::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteKeyframe::iterator, bool> WriteKeyframe::insert(value_type &&value)
{
    Q_ASSERT(FP::defined(value.first));
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(std::move(value.first), std::move(source));
    return std::pair<WriteKeyframe::iterator, bool>(
            WriteKeyframe::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteKeyframe::iterator, bool> WriteKeyframe::emplace(key_type key, const Read &value)
{
    Q_ASSERT(FP::defined(key));
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(key, std::move(source));
    return std::pair<WriteKeyframe::iterator, bool>(
            WriteKeyframe::iterator(*this, std::move(result.first)), result.second);
}

void WriteKeyframe::merge(const ReadKeyframe &other)
{
    auto &target = backend();
    for (const auto &node : other.backend()) {
        auto add = node.second;
        add->detach(add);
        target.emplace(node.first, std::move(add));
    }
}

WriteKeyframe::iterator WriteKeyframe::erase(const iterator &pos)
{ return WriteKeyframe::iterator(*this, backend().erase(pos.i)); }

WriteKeyframe::iterator WriteKeyframe::erase(const iterator &first, const iterator &last)
{ return WriteKeyframe::iterator(*this, backend().erase(first.i, last.i)); }

ReadKeyframe::size_type WriteKeyframe::erase(key_type key)
{ return backend().erase(key); }

WriteKeyframe::iterator::iterator() = default;

WriteKeyframe::iterator::~iterator() = default;

WriteKeyframe::iterator::iterator(const iterator &) = default;

WriteKeyframe::iterator &WriteKeyframe::iterator::operator=(const iterator &) = default;

WriteKeyframe::iterator::iterator(iterator &&) = default;

WriteKeyframe::iterator &WriteKeyframe::iterator::operator=(iterator &&) = default;

WriteKeyframe::iterator::iterator(WriteKeyframe &parent, backing_iterator &&i) : parent(&parent),
                                                                                 i(std::move(i))
{ }

WriteKeyframe::iterator::value_type WriteKeyframe::iterator::operator*() const
{ return WriteKeyframe::value_type(i->first, (*parent)[i->first]); }

WriteKeyframe::iterator::pointer WriteKeyframe::iterator::operator->() const
{
    static thread_local std::pair<WriteKeyframe::key_type, WriteKeyframe::mapped_type> ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

WriteKeyframe::iterator &WriteKeyframe::iterator::operator++()
{
    ++i;
    return *this;
}

WriteKeyframe::iterator WriteKeyframe::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

WriteKeyframe::iterator &WriteKeyframe::iterator::operator--()
{
    --i;
    return *this;
}

WriteKeyframe::iterator WriteKeyframe::iterator::operator--(int)
{
    auto save = *this;
    --i;
    return save;
}

bool WriteKeyframe::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteKeyframe::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteKeyframe::iterator::operator==(const ReadKeyframe::const_iterator &other) const
{ return i == other.backing(); }

bool WriteKeyframe::iterator::operator!=(const ReadKeyframe::const_iterator &other) const
{ return i != other.backing(); }

WriteKeyframe::iterator::operator ReadKeyframe::const_iterator() const
{ return ReadKeyframe::const_iterator(*parent, ReadKeyframe::const_iterator::backing_iterator(i)); }

const WriteKeyframe::iterator::backing_iterator &WriteKeyframe::iterator::backing() const
{ return i; }

WriteKeyframe::key_type WriteKeyframe::iterator::key() const
{ return i->first; }

WriteKeyframe::mapped_type WriteKeyframe::iterator::value() const
{ return (*parent)[i->first]; }

PathElement WriteKeyframe::iterator::path() const
{ return PathElement(PathElement::Type::Keyframe, i->first); }


ReadMetadata::ReadMetadata() = default;

ReadMetadata::~ReadMetadata() = default;

ReadMetadata::ReadMetadata(const ReadMetadata &) = default;

ReadMetadata &ReadMetadata::operator=(const ReadMetadata &) = default;

ReadMetadata::ReadMetadata(ReadMetadata &&) = default;

ReadMetadata &ReadMetadata::operator=(ReadMetadata &&) = default;

ReadMetadata::ReadMetadata(const Read &read) : top(read.top),
                                               path(read.path),
                                               type(PathElement::Type::AnyMetadata)
{ }

ReadMetadata::ReadMetadata(const Read &read, PathElement::Type type) : top(read.top),
                                                                       path(read.path),
                                                                       type(type)
{ }

void ReadMetadata::swap(ReadMetadata &a, ReadMetadata &b)
{
    using std::swap;
    swap(a.top, b.top);
    swap(a.path, b.path);
    swap(a.type, b.type);
}

const ReadMetadata::backend_map &ReadMetadata::backend() const
{
    static const ReadMetadata::backend_map invalid;
    if (path.empty()) {
        switch (top->node()->type()) {
        case Node::Type::MetadataReal:
        case Node::Type::MetadataRealConstant:
        case Node::Type::MetadataInteger:
        case Node::Type::MetadataIntegerConstant:
        case Node::Type::MetadataBoolean:
        case Node::Type::MetadataBooleanConstant:
        case Node::Type::MetadataString:
        case Node::Type::MetadataStringConstant:
        case Node::Type::MetadataBytes:
        case Node::Type::MetadataBytesConstant:
        case Node::Type::MetadataFlags:
        case Node::Type::MetadataFlagsConstant:
        case Node::Type::MetadataHash:
        case Node::Type::MetadataHashConstant:
        case Node::Type::MetadataArray:
        case Node::Type::MetadataArrayConstant:
        case Node::Type::MetadataMatrix:
        case Node::Type::MetadataMatrixConstant:
        case Node::Type::MetadataKeyframe:
        case Node::Type::MetadataKeyframeConstant:
            break;
        default:
            return invalid;
        }
        return top->node<NodeMetadataBase>()->children;
    }

    auto ptr = top->read(path);
    if (!ptr)
        return invalid;

    switch (ptr->type()) {
    case Node::Type::MetadataReal:
    case Node::Type::MetadataRealConstant:
    case Node::Type::MetadataInteger:
    case Node::Type::MetadataIntegerConstant:
    case Node::Type::MetadataBoolean:
    case Node::Type::MetadataBooleanConstant:
    case Node::Type::MetadataString:
    case Node::Type::MetadataStringConstant:
    case Node::Type::MetadataBytes:
    case Node::Type::MetadataBytesConstant:
    case Node::Type::MetadataFlags:
    case Node::Type::MetadataFlagsConstant:
    case Node::Type::MetadataHash:
    case Node::Type::MetadataHashConstant:
    case Node::Type::MetadataArray:
    case Node::Type::MetadataArrayConstant:
    case Node::Type::MetadataMatrix:
    case Node::Type::MetadataMatrixConstant:
    case Node::Type::MetadataKeyframe:
    case Node::Type::MetadataKeyframeConstant:
        break;
    default:
        return invalid;
    }

    /* Safe to keep the pointer as long as the root remains unchanged, since it will
     * have a pointer to it regardless.  Any write access invalidates this,
     * however. */
    return static_cast<const NodeMetadataBase *>(ptr.get())->children;
}

bool ReadMetadata::empty() const
{ return backend().empty(); }

ReadMetadata::size_type ReadMetadata::size() const
{ return backend().size(); }

ReadMetadata::size_type ReadMetadata::count(const key_type &key) const
{ return backend().count(key); }

bool ReadMetadata::operator==(const ReadMetadata &other) const
{
    const auto &a = backend();
    const auto &b = other.backend();
    if (a.size() != b.size())
        return false;
    for (const auto &check : a) {
        auto i = b.find(check.first);
        if (i == b.end())
            return false;
        if (!check.second->identicalTo(*i->second))
            return false;
    }
    return true;
}

bool ReadMetadata::operator!=(const ReadMetadata &other) const
{ return !(*this == other); }

ReadMetadata::mapped_type ReadMetadata::operator[](const key_type &key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(type, key);
    return result;
}

ReadMetadata::mapped_type ReadMetadata::operator[](key_type &&key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(type, std::move(key));
    return result;
}

ReadMetadata::mapped_type ReadMetadata::operator[](const char *key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(type, key);
    return result;
}

ReadMetadata::mapped_type ReadMetadata::operator[](const QString &key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(type, key);
    return result;
}

ReadMetadata::const_iterator ReadMetadata::begin() const
{ return ReadMetadata::const_iterator(*this, backend().begin()); }

ReadMetadata::const_iterator ReadMetadata::end() const
{ return ReadMetadata::const_iterator(*this, backend().end()); }

ReadMetadata::const_iterator ReadMetadata::find(const key_type &key) const
{ return ReadMetadata::const_iterator(*this, backend().find(key)); }

std::unordered_set<ReadMetadata::key_type> ReadMetadata::keys() const
{
    std::unordered_set<ReadMetadata::key_type> result;
    for (const auto &add : backend()) {
        result.insert(add.first);
    }
    return result;
}

ReadMetadata::const_iterator::const_iterator() = default;

ReadMetadata::const_iterator::~const_iterator() = default;

ReadMetadata::const_iterator::const_iterator(const const_iterator &) = default;

ReadMetadata::const_iterator &ReadMetadata::const_iterator::operator=(const const_iterator &) = default;

ReadMetadata::const_iterator::const_iterator(const_iterator &&) = default;

ReadMetadata::const_iterator &ReadMetadata::const_iterator::operator=(const_iterator &&) = default;

ReadMetadata::const_iterator::const_iterator(const ReadMetadata &parent, backing_iterator &&i)
        : parent(&parent), i(std::move(i))
{ }

ReadMetadata::const_iterator::value_type ReadMetadata::const_iterator::operator*() const
{ return ReadMetadata::value_type(i->first, (*parent)[i->first]); }

ReadMetadata::const_iterator::pointer ReadMetadata::const_iterator::operator->() const
{
    static thread_local std::pair<ReadMetadata::key_type, ReadMetadata::mapped_type> ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

ReadMetadata::const_iterator &ReadMetadata::const_iterator::operator++()
{
    ++i;
    return *this;
}

ReadMetadata::const_iterator ReadMetadata::const_iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool ReadMetadata::const_iterator::operator==(const const_iterator &other) const
{ return i == other.i; }

bool ReadMetadata::const_iterator::operator!=(const const_iterator &other) const
{ return i != other.i; }

const ReadMetadata::const_iterator::backing_iterator &ReadMetadata::const_iterator::backing() const
{ return i; }

const ReadMetadata::key_type &ReadMetadata::const_iterator::key() const
{ return i->first; }

ReadMetadata::mapped_type ReadMetadata::const_iterator::value() const
{ return (*parent)[i->first]; }

PathElement ReadMetadata::const_iterator::path() const
{ return PathElement(parent->type, i->first); }


ReadMetadataReal::ReadMetadataReal() = default;

ReadMetadataReal::~ReadMetadataReal() = default;

ReadMetadataReal::ReadMetadataReal(const ReadMetadataReal &) = default;

ReadMetadataReal &ReadMetadataReal::operator=(const ReadMetadataReal &) = default;

ReadMetadataReal::ReadMetadataReal(ReadMetadataReal &&) = default;

ReadMetadataReal &ReadMetadataReal::operator=(ReadMetadataReal &&) = default;

ReadMetadataReal::ReadMetadataReal(const Read &read) : ReadMetadata(read,
                                                                    PathElement::Type::MetadataReal)
{ }


WriteMetadataReal::WriteMetadataReal() = default;

WriteMetadataReal::~WriteMetadataReal() = default;

WriteMetadataReal::WriteMetadataReal(const WriteMetadataReal &) = default;

WriteMetadataReal &WriteMetadataReal::operator=(const WriteMetadataReal &) = default;

WriteMetadataReal::WriteMetadataReal(WriteMetadataReal &&) = default;

WriteMetadataReal &WriteMetadataReal::operator=(WriteMetadataReal &&) = default;

WriteMetadataReal::WriteMetadataReal(Write &write) : ReadMetadataReal(write)
{ }

WriteMetadataReal::backend_map &WriteMetadataReal::backend()
{
    if (path.empty())
        return static_cast<NodeMetadataBase *>(top->write(Node::WriteGoal::MetadataReal))->children;
    else
        return std::static_pointer_cast<NodeMetadataBase>(
                top->write(path, Node::WriteGoal::MetadataReal))->children;
}

WriteMetadataReal::mapped_type WriteMetadataReal::operator[](const key_type &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataReal, key);
    return result;
}

WriteMetadataReal::mapped_type WriteMetadataReal::operator[](key_type &&key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataReal, std::move(key));
    return result;
}

WriteMetadataReal::mapped_type WriteMetadataReal::operator[](const char *key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataReal, key);
    return result;
}

WriteMetadataReal::mapped_type WriteMetadataReal::operator[](const QString &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataReal, key);
    return result;
}

WriteMetadataReal::iterator WriteMetadataReal::begin()
{ return WriteMetadataReal::iterator(*this, backend().begin()); }

WriteMetadataReal::iterator WriteMetadataReal::end()
{ return WriteMetadataReal::iterator(*this, backend().end()); }

WriteMetadataReal::iterator WriteMetadataReal::find(const key_type &key)
{ return WriteMetadataReal::iterator(*this, backend().find(key)); }

std::pair<WriteMetadataReal::iterator, bool> WriteMetadataReal::insert(const value_type &value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(value.first, std::move(source));
    return std::pair<WriteMetadataReal::iterator, bool>(
            WriteMetadataReal::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataReal::iterator, bool> WriteMetadataReal::insert(value_type &&value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(std::move(value.first), std::move(source));
    return std::pair<WriteMetadataReal::iterator, bool>(
            WriteMetadataReal::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataReal::iterator, bool> WriteMetadataReal::emplace(const key_type &key,
                                                                        const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(key, std::move(source));
    return std::pair<WriteMetadataReal::iterator, bool>(
            WriteMetadataReal::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataReal::iterator, bool> WriteMetadataReal::emplace(key_type &&key,
                                                                        const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(std::move(key), std::move(source));
    return std::pair<WriteMetadataReal::iterator, bool>(
            WriteMetadataReal::iterator(*this, std::move(result.first)), result.second);
}

void WriteMetadataReal::merge(const ReadMetadataReal &other)
{
    auto &target = backend();
    for (const auto &node : other.backend()) {
        auto add = node.second;
        add->detach(add);
        target.emplace(node.first, std::move(add));
    }
}

WriteMetadataReal::iterator WriteMetadataReal::erase(const iterator &pos)
{ return WriteMetadataReal::iterator(*this, backend().erase(pos.i)); }

WriteMetadataReal::iterator WriteMetadataReal::erase(const iterator &first, const iterator &last)
{ return WriteMetadataReal::iterator(*this, backend().erase(first.i, last.i)); }

ReadMetadataReal::size_type WriteMetadataReal::erase(const key_type &key)
{ return backend().erase(key); }

WriteMetadataReal::iterator::iterator() = default;

WriteMetadataReal::iterator::~iterator() = default;

WriteMetadataReal::iterator::iterator(const iterator &) = default;

WriteMetadataReal::iterator &WriteMetadataReal::iterator::operator=(const iterator &) = default;

WriteMetadataReal::iterator::iterator(iterator &&) = default;

WriteMetadataReal::iterator &WriteMetadataReal::iterator::operator=(iterator &&) = default;

WriteMetadataReal::iterator::iterator(WriteMetadataReal &parent, backing_iterator &&i) : parent(
        &parent), i(std::move(i))
{ }

WriteMetadataReal::iterator::value_type WriteMetadataReal::iterator::operator*() const
{ return WriteMetadataReal::value_type(i->first, (*parent)[i->first]); }

WriteMetadataReal::iterator::pointer WriteMetadataReal::iterator::operator->() const
{
    static thread_local std::pair<WriteMetadataReal::key_type, WriteMetadataReal::mapped_type> ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

WriteMetadataReal::iterator &WriteMetadataReal::iterator::operator++()
{
    ++i;
    return *this;
}

WriteMetadataReal::iterator WriteMetadataReal::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool WriteMetadataReal::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteMetadataReal::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteMetadataReal::iterator::operator==(const ReadMetadataReal::const_iterator &other) const
{ return i == other.backing(); }

bool WriteMetadataReal::iterator::operator!=(const ReadMetadataReal::const_iterator &other) const
{ return i != other.backing(); }

WriteMetadataReal::iterator::operator ReadMetadataReal::const_iterator() const
{
    return ReadMetadataReal::const_iterator(*parent,
                                            ReadMetadataReal::const_iterator::backing_iterator(i));
}

const WriteMetadataReal::iterator::backing_iterator &WriteMetadataReal::iterator::backing() const
{ return i; }

const WriteMetadataReal::key_type &WriteMetadataReal::iterator::key() const
{ return i->first; }

WriteMetadataReal::mapped_type WriteMetadataReal::iterator::value() const
{ return (*parent)[i->first]; }

PathElement WriteMetadataReal::iterator::path() const
{ return PathElement(PathElement::Type::MetadataReal, i->first); }


ReadMetadataInteger::ReadMetadataInteger() = default;

ReadMetadataInteger::~ReadMetadataInteger() = default;

ReadMetadataInteger::ReadMetadataInteger(const ReadMetadataInteger &) = default;

ReadMetadataInteger &ReadMetadataInteger::operator=(const ReadMetadataInteger &) = default;

ReadMetadataInteger::ReadMetadataInteger(ReadMetadataInteger &&) = default;

ReadMetadataInteger &ReadMetadataInteger::operator=(ReadMetadataInteger &&) = default;

ReadMetadataInteger::ReadMetadataInteger(const Read &read) : ReadMetadata(read,
                                                                          PathElement::Type::MetadataInteger)
{ }


WriteMetadataInteger::WriteMetadataInteger() = default;

WriteMetadataInteger::~WriteMetadataInteger() = default;

WriteMetadataInteger::WriteMetadataInteger(const WriteMetadataInteger &) = default;

WriteMetadataInteger &WriteMetadataInteger::operator=(const WriteMetadataInteger &) = default;

WriteMetadataInteger::WriteMetadataInteger(WriteMetadataInteger &&) = default;

WriteMetadataInteger &WriteMetadataInteger::operator=(WriteMetadataInteger &&) = default;

WriteMetadataInteger::WriteMetadataInteger(Write &write) : ReadMetadataInteger(write)
{ }

WriteMetadataInteger::backend_map &WriteMetadataInteger::backend()
{
    if (path.empty())
        return static_cast<NodeMetadataBase *>(top->write(
                Node::WriteGoal::MetadataInteger))->children;
    else
        return std::static_pointer_cast<NodeMetadataBase>(
                top->write(path, Node::WriteGoal::MetadataInteger))->children;
}

WriteMetadataInteger::mapped_type WriteMetadataInteger::operator[](const key_type &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataInteger, key);
    return result;
}

WriteMetadataInteger::mapped_type WriteMetadataInteger::operator[](key_type &&key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataInteger, std::move(key));
    return result;
}

WriteMetadataInteger::mapped_type WriteMetadataInteger::operator[](const char *key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataInteger, key);
    return result;
}

WriteMetadataInteger::mapped_type WriteMetadataInteger::operator[](const QString &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataInteger, key);
    return result;
}

WriteMetadataInteger::iterator WriteMetadataInteger::begin()
{ return WriteMetadataInteger::iterator(*this, backend().begin()); }

WriteMetadataInteger::iterator WriteMetadataInteger::end()
{ return WriteMetadataInteger::iterator(*this, backend().end()); }

WriteMetadataInteger::iterator WriteMetadataInteger::find(const key_type &key)
{ return WriteMetadataInteger::iterator(*this, backend().find(key)); }

std::pair<WriteMetadataInteger::iterator,
          bool> WriteMetadataInteger::insert(const value_type &value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(value.first, std::move(source));
    return std::pair<WriteMetadataInteger::iterator, bool>(
            WriteMetadataInteger::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataInteger::iterator, bool> WriteMetadataInteger::insert(value_type &&value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(std::move(value.first), std::move(source));
    return std::pair<WriteMetadataInteger::iterator, bool>(
            WriteMetadataInteger::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataInteger::iterator, bool> WriteMetadataInteger::emplace(const key_type &key,
                                                                              const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(key, std::move(source));
    return std::pair<WriteMetadataInteger::iterator, bool>(
            WriteMetadataInteger::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataInteger::iterator, bool> WriteMetadataInteger::emplace(key_type &&key,
                                                                              const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(std::move(key), std::move(source));
    return std::pair<WriteMetadataInteger::iterator, bool>(
            WriteMetadataInteger::iterator(*this, std::move(result.first)), result.second);
}

void WriteMetadataInteger::merge(const ReadMetadataInteger &other)
{
    auto &target = backend();
    for (const auto &node : other.backend()) {
        auto add = node.second;
        add->detach(add);
        target.emplace(node.first, std::move(add));
    }
}

WriteMetadataInteger::iterator WriteMetadataInteger::erase(const iterator &pos)
{ return WriteMetadataInteger::iterator(*this, backend().erase(pos.i)); }

WriteMetadataInteger::iterator WriteMetadataInteger::erase(const iterator &first,
                                                           const iterator &last)
{ return WriteMetadataInteger::iterator(*this, backend().erase(first.i, last.i)); }

ReadMetadataInteger::size_type WriteMetadataInteger::erase(const key_type &key)
{ return backend().erase(key); }

WriteMetadataInteger::iterator::iterator() = default;

WriteMetadataInteger::iterator::~iterator() = default;

WriteMetadataInteger::iterator::iterator(const iterator &) = default;

WriteMetadataInteger::iterator &WriteMetadataInteger::iterator::operator=(const iterator &) = default;

WriteMetadataInteger::iterator::iterator(iterator &&) = default;

WriteMetadataInteger::iterator &WriteMetadataInteger::iterator::operator=(iterator &&) = default;

WriteMetadataInteger::iterator::iterator(WriteMetadataInteger &parent, backing_iterator &&i)
        : parent(&parent), i(std::move(i))
{ }

WriteMetadataInteger::iterator::value_type WriteMetadataInteger::iterator::operator*() const
{ return WriteMetadataInteger::value_type(i->first, (*parent)[i->first]); }

WriteMetadataInteger::iterator::pointer WriteMetadataInteger::iterator::operator->() const
{
    static thread_local std::pair<WriteMetadataInteger::key_type, WriteMetadataInteger::mapped_type>
            ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

WriteMetadataInteger::iterator &WriteMetadataInteger::iterator::operator++()
{
    ++i;
    return *this;
}

WriteMetadataInteger::iterator WriteMetadataInteger::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool WriteMetadataInteger::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteMetadataInteger::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteMetadataInteger::iterator::operator==(const ReadMetadataInteger::const_iterator &other) const
{ return i == other.backing(); }

bool WriteMetadataInteger::iterator::operator!=(const ReadMetadataInteger::const_iterator &other) const
{ return i != other.backing(); }

WriteMetadataInteger::iterator::operator ReadMetadataInteger::const_iterator() const
{
    return ReadMetadataInteger::const_iterator(*parent,
                                               ReadMetadataInteger::const_iterator::backing_iterator(
                                                       i));
}

const WriteMetadataInteger::iterator::backing_iterator &WriteMetadataInteger::iterator::backing() const
{ return i; }

const WriteMetadataInteger::key_type &WriteMetadataInteger::iterator::key() const
{ return i->first; }

WriteMetadataInteger::mapped_type WriteMetadataInteger::iterator::value() const
{ return (*parent)[i->first]; }

PathElement WriteMetadataInteger::iterator::path() const
{ return PathElement(PathElement::Type::MetadataInteger, i->first); }


ReadMetadataBoolean::ReadMetadataBoolean() = default;

ReadMetadataBoolean::~ReadMetadataBoolean() = default;

ReadMetadataBoolean::ReadMetadataBoolean(const ReadMetadataBoolean &) = default;

ReadMetadataBoolean &ReadMetadataBoolean::operator=(const ReadMetadataBoolean &) = default;

ReadMetadataBoolean::ReadMetadataBoolean(ReadMetadataBoolean &&) = default;

ReadMetadataBoolean &ReadMetadataBoolean::operator=(ReadMetadataBoolean &&) = default;

ReadMetadataBoolean::ReadMetadataBoolean(const Read &read) : ReadMetadata(read,
                                                                          PathElement::Type::MetadataBoolean)
{ }


WriteMetadataBoolean::WriteMetadataBoolean() = default;

WriteMetadataBoolean::~WriteMetadataBoolean() = default;

WriteMetadataBoolean::WriteMetadataBoolean(const WriteMetadataBoolean &) = default;

WriteMetadataBoolean &WriteMetadataBoolean::operator=(const WriteMetadataBoolean &) = default;

WriteMetadataBoolean::WriteMetadataBoolean(WriteMetadataBoolean &&) = default;

WriteMetadataBoolean &WriteMetadataBoolean::operator=(WriteMetadataBoolean &&) = default;

WriteMetadataBoolean::WriteMetadataBoolean(Write &write) : ReadMetadataBoolean(write)
{ }

WriteMetadataBoolean::backend_map &WriteMetadataBoolean::backend()
{
    if (path.empty())
        return static_cast<NodeMetadataBase *>(top->write(
                Node::WriteGoal::MetadataBoolean))->children;
    else
        return std::static_pointer_cast<NodeMetadataBase>(
                top->write(path, Node::WriteGoal::MetadataBoolean))->children;
}

WriteMetadataBoolean::mapped_type WriteMetadataBoolean::operator[](const key_type &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataBoolean, key);
    return result;
}

WriteMetadataBoolean::mapped_type WriteMetadataBoolean::operator[](key_type &&key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataBoolean, std::move(key));
    return result;
}

WriteMetadataBoolean::mapped_type WriteMetadataBoolean::operator[](const char *key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataBoolean, key);
    return result;
}

WriteMetadataBoolean::mapped_type WriteMetadataBoolean::operator[](const QString &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataBoolean, key);
    return result;
}

WriteMetadataBoolean::iterator WriteMetadataBoolean::begin()
{ return WriteMetadataBoolean::iterator(*this, backend().begin()); }

WriteMetadataBoolean::iterator WriteMetadataBoolean::end()
{ return WriteMetadataBoolean::iterator(*this, backend().end()); }

WriteMetadataBoolean::iterator WriteMetadataBoolean::find(const key_type &key)
{ return WriteMetadataBoolean::iterator(*this, backend().find(key)); }

std::pair<WriteMetadataBoolean::iterator,
          bool> WriteMetadataBoolean::insert(const value_type &value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(value.first, std::move(source));
    return std::pair<WriteMetadataBoolean::iterator, bool>(
            WriteMetadataBoolean::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataBoolean::iterator, bool> WriteMetadataBoolean::insert(value_type &&value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(std::move(value.first), std::move(source));
    return std::pair<WriteMetadataBoolean::iterator, bool>(
            WriteMetadataBoolean::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataBoolean::iterator, bool> WriteMetadataBoolean::emplace(const key_type &key,
                                                                              const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(key, std::move(source));
    return std::pair<WriteMetadataBoolean::iterator, bool>(
            WriteMetadataBoolean::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataBoolean::iterator, bool> WriteMetadataBoolean::emplace(key_type &&key,
                                                                              const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(std::move(key), std::move(source));
    return std::pair<WriteMetadataBoolean::iterator, bool>(
            WriteMetadataBoolean::iterator(*this, std::move(result.first)), result.second);
}

void WriteMetadataBoolean::merge(const ReadMetadataBoolean &other)
{
    auto &target = backend();
    for (const auto &node : other.backend()) {
        auto add = node.second;
        add->detach(add);
        target.emplace(node.first, std::move(add));
    }
}

WriteMetadataBoolean::iterator WriteMetadataBoolean::erase(const iterator &pos)
{ return WriteMetadataBoolean::iterator(*this, backend().erase(pos.i)); }

WriteMetadataBoolean::iterator WriteMetadataBoolean::erase(const iterator &first,
                                                           const iterator &last)
{ return WriteMetadataBoolean::iterator(*this, backend().erase(first.i, last.i)); }

ReadMetadataBoolean::size_type WriteMetadataBoolean::erase(const key_type &key)
{ return backend().erase(key); }

WriteMetadataBoolean::iterator::iterator() = default;

WriteMetadataBoolean::iterator::~iterator() = default;

WriteMetadataBoolean::iterator::iterator(const iterator &) = default;

WriteMetadataBoolean::iterator &WriteMetadataBoolean::iterator::operator=(const iterator &) = default;

WriteMetadataBoolean::iterator::iterator(iterator &&) = default;

WriteMetadataBoolean::iterator &WriteMetadataBoolean::iterator::operator=(iterator &&) = default;

WriteMetadataBoolean::iterator::iterator(WriteMetadataBoolean &parent, backing_iterator &&i)
        : parent(&parent), i(std::move(i))
{ }

WriteMetadataBoolean::iterator::value_type WriteMetadataBoolean::iterator::operator*() const
{ return WriteMetadataBoolean::value_type(i->first, (*parent)[i->first]); }

WriteMetadataBoolean::iterator::pointer WriteMetadataBoolean::iterator::operator->() const
{
    static thread_local std::pair<WriteMetadataBoolean::key_type, WriteMetadataBoolean::mapped_type>
            ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

WriteMetadataBoolean::iterator &WriteMetadataBoolean::iterator::operator++()
{
    ++i;
    return *this;
}

WriteMetadataBoolean::iterator WriteMetadataBoolean::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool WriteMetadataBoolean::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteMetadataBoolean::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteMetadataBoolean::iterator::operator==(const ReadMetadataBoolean::const_iterator &other) const
{ return i == other.backing(); }

bool WriteMetadataBoolean::iterator::operator!=(const ReadMetadataBoolean::const_iterator &other) const
{ return i != other.backing(); }

WriteMetadataBoolean::iterator::operator ReadMetadataBoolean::const_iterator() const
{
    return ReadMetadataBoolean::const_iterator(*parent,
                                               ReadMetadataBoolean::const_iterator::backing_iterator(
                                                       i));
}

const WriteMetadataBoolean::iterator::backing_iterator &WriteMetadataBoolean::iterator::backing() const
{ return i; }

const WriteMetadataBoolean::key_type &WriteMetadataBoolean::iterator::key() const
{ return i->first; }

WriteMetadataBoolean::mapped_type WriteMetadataBoolean::iterator::value() const
{ return (*parent)[i->first]; }

PathElement WriteMetadataBoolean::iterator::path() const
{ return PathElement(PathElement::Type::MetadataBoolean, i->first); }


ReadMetadataString::ReadMetadataString() = default;

ReadMetadataString::~ReadMetadataString() = default;

ReadMetadataString::ReadMetadataString(const ReadMetadataString &) = default;

ReadMetadataString &ReadMetadataString::operator=(const ReadMetadataString &) = default;

ReadMetadataString::ReadMetadataString(ReadMetadataString &&) = default;

ReadMetadataString &ReadMetadataString::operator=(ReadMetadataString &&) = default;

ReadMetadataString::ReadMetadataString(const Read &read) : ReadMetadata(read,
                                                                        PathElement::Type::MetadataString)
{ }


WriteMetadataString::WriteMetadataString() = default;

WriteMetadataString::~WriteMetadataString() = default;

WriteMetadataString::WriteMetadataString(const WriteMetadataString &) = default;

WriteMetadataString &WriteMetadataString::operator=(const WriteMetadataString &) = default;

WriteMetadataString::WriteMetadataString(WriteMetadataString &&) = default;

WriteMetadataString &WriteMetadataString::operator=(WriteMetadataString &&) = default;

WriteMetadataString::WriteMetadataString(Write &write) : ReadMetadataString(write)
{ }

WriteMetadataString::backend_map &WriteMetadataString::backend()
{
    if (path.empty())
        return static_cast<NodeMetadataBase *>(top->write(
                Node::WriteGoal::MetadataString))->children;
    else
        return std::static_pointer_cast<NodeMetadataBase>(
                top->write(path, Node::WriteGoal::MetadataString))->children;
}

WriteMetadataString::mapped_type WriteMetadataString::operator[](const key_type &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataString, key);
    return result;
}

WriteMetadataString::mapped_type WriteMetadataString::operator[](key_type &&key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataString, std::move(key));
    return result;
}

WriteMetadataString::mapped_type WriteMetadataString::operator[](const char *key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataString, key);
    return result;
}

WriteMetadataString::mapped_type WriteMetadataString::operator[](const QString &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataString, key);
    return result;
}

WriteMetadataString::iterator WriteMetadataString::begin()
{ return WriteMetadataString::iterator(*this, backend().begin()); }

WriteMetadataString::iterator WriteMetadataString::end()
{ return WriteMetadataString::iterator(*this, backend().end()); }

WriteMetadataString::iterator WriteMetadataString::find(const key_type &key)
{ return WriteMetadataString::iterator(*this, backend().find(key)); }

std::pair<WriteMetadataString::iterator, bool> WriteMetadataString::insert(const value_type &value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(value.first, std::move(source));
    return std::pair<WriteMetadataString::iterator, bool>(
            WriteMetadataString::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataString::iterator, bool> WriteMetadataString::insert(value_type &&value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(std::move(value.first), std::move(source));
    return std::pair<WriteMetadataString::iterator, bool>(
            WriteMetadataString::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataString::iterator, bool> WriteMetadataString::emplace(const key_type &key,
                                                                            const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(key, std::move(source));
    return std::pair<WriteMetadataString::iterator, bool>(
            WriteMetadataString::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataString::iterator, bool> WriteMetadataString::emplace(key_type &&key,
                                                                            const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(std::move(key), std::move(source));
    return std::pair<WriteMetadataString::iterator, bool>(
            WriteMetadataString::iterator(*this, std::move(result.first)), result.second);
}

void WriteMetadataString::merge(const ReadMetadataString &other)
{
    auto &target = backend();
    for (const auto &node : other.backend()) {
        auto add = node.second;
        add->detach(add);
        target.emplace(node.first, std::move(add));
    }
}

WriteMetadataString::iterator WriteMetadataString::erase(const iterator &pos)
{ return WriteMetadataString::iterator(*this, backend().erase(pos.i)); }

WriteMetadataString::iterator WriteMetadataString::erase(const iterator &first,
                                                         const iterator &last)
{ return WriteMetadataString::iterator(*this, backend().erase(first.i, last.i)); }

ReadMetadataString::size_type WriteMetadataString::erase(const key_type &key)
{ return backend().erase(key); }

WriteMetadataString::iterator::iterator() = default;

WriteMetadataString::iterator::~iterator() = default;

WriteMetadataString::iterator::iterator(const iterator &) = default;

WriteMetadataString::iterator &WriteMetadataString::iterator::operator=(const iterator &) = default;

WriteMetadataString::iterator::iterator(iterator &&) = default;

WriteMetadataString::iterator &WriteMetadataString::iterator::operator=(iterator &&) = default;

WriteMetadataString::iterator::iterator(WriteMetadataString &parent, backing_iterator &&i) : parent(
        &parent), i(std::move(i))
{ }

WriteMetadataString::iterator::value_type WriteMetadataString::iterator::operator*() const
{ return WriteMetadataString::value_type(i->first, (*parent)[i->first]); }

WriteMetadataString::iterator::pointer WriteMetadataString::iterator::operator->() const
{
    static thread_local std::pair<WriteMetadataString::key_type, WriteMetadataString::mapped_type>
            ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

WriteMetadataString::iterator &WriteMetadataString::iterator::operator++()
{
    ++i;
    return *this;
}

WriteMetadataString::iterator WriteMetadataString::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool WriteMetadataString::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteMetadataString::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteMetadataString::iterator::operator==(const ReadMetadataString::const_iterator &other) const
{ return i == other.backing(); }

bool WriteMetadataString::iterator::operator!=(const ReadMetadataString::const_iterator &other) const
{ return i != other.backing(); }

WriteMetadataString::iterator::operator ReadMetadataString::const_iterator() const
{
    return ReadMetadataString::const_iterator(*parent,
                                              ReadMetadataString::const_iterator::backing_iterator(
                                                      i));
}

const WriteMetadataString::iterator::backing_iterator &WriteMetadataString::iterator::backing() const
{ return i; }

const WriteMetadataString::key_type &WriteMetadataString::iterator::key() const
{ return i->first; }

WriteMetadataString::mapped_type WriteMetadataString::iterator::value() const
{ return (*parent)[i->first]; }

PathElement WriteMetadataString::iterator::path() const
{ return PathElement(PathElement::Type::MetadataString, i->first); }


ReadMetadataBytes::ReadMetadataBytes() = default;

ReadMetadataBytes::~ReadMetadataBytes() = default;

ReadMetadataBytes::ReadMetadataBytes(const ReadMetadataBytes &) = default;

ReadMetadataBytes &ReadMetadataBytes::operator=(const ReadMetadataBytes &) = default;

ReadMetadataBytes::ReadMetadataBytes(ReadMetadataBytes &&) = default;

ReadMetadataBytes &ReadMetadataBytes::operator=(ReadMetadataBytes &&) = default;

ReadMetadataBytes::ReadMetadataBytes(const Read &read) : ReadMetadata(read,
                                                                      PathElement::Type::MetadataBytes)
{ }


WriteMetadataBytes::WriteMetadataBytes() = default;

WriteMetadataBytes::~WriteMetadataBytes() = default;

WriteMetadataBytes::WriteMetadataBytes(const WriteMetadataBytes &) = default;

WriteMetadataBytes &WriteMetadataBytes::operator=(const WriteMetadataBytes &) = default;

WriteMetadataBytes::WriteMetadataBytes(WriteMetadataBytes &&) = default;

WriteMetadataBytes &WriteMetadataBytes::operator=(WriteMetadataBytes &&) = default;

WriteMetadataBytes::WriteMetadataBytes(Write &write) : ReadMetadataBytes(write)
{ }

WriteMetadataBytes::backend_map &WriteMetadataBytes::backend()
{
    if (path.empty())
        return static_cast<NodeMetadataBase *>(top->write(
                Node::WriteGoal::MetadataBytes))->children;
    else
        return std::static_pointer_cast<NodeMetadataBase>(
                top->write(path, Node::WriteGoal::MetadataBytes))->children;
}

WriteMetadataBytes::mapped_type WriteMetadataBytes::operator[](const key_type &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataBytes, key);
    return result;
}

WriteMetadataBytes::mapped_type WriteMetadataBytes::operator[](key_type &&key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataBytes, std::move(key));
    return result;
}

WriteMetadataBytes::mapped_type WriteMetadataBytes::operator[](const char *key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataBytes, key);
    return result;
}

WriteMetadataBytes::mapped_type WriteMetadataBytes::operator[](const QString &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataBytes, key);
    return result;
}

WriteMetadataBytes::iterator WriteMetadataBytes::begin()
{ return WriteMetadataBytes::iterator(*this, backend().begin()); }

WriteMetadataBytes::iterator WriteMetadataBytes::end()
{ return WriteMetadataBytes::iterator(*this, backend().end()); }

WriteMetadataBytes::iterator WriteMetadataBytes::find(const key_type &key)
{ return WriteMetadataBytes::iterator(*this, backend().find(key)); }

std::pair<WriteMetadataBytes::iterator, bool> WriteMetadataBytes::insert(const value_type &value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(value.first, std::move(source));
    return std::pair<WriteMetadataBytes::iterator, bool>(
            WriteMetadataBytes::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataBytes::iterator, bool> WriteMetadataBytes::insert(value_type &&value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(std::move(value.first), std::move(source));
    return std::pair<WriteMetadataBytes::iterator, bool>(
            WriteMetadataBytes::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataBytes::iterator, bool> WriteMetadataBytes::emplace(const key_type &key,
                                                                          const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(key, std::move(source));
    return std::pair<WriteMetadataBytes::iterator, bool>(
            WriteMetadataBytes::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataBytes::iterator, bool> WriteMetadataBytes::emplace(key_type &&key,
                                                                          const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(std::move(key), std::move(source));
    return std::pair<WriteMetadataBytes::iterator, bool>(
            WriteMetadataBytes::iterator(*this, std::move(result.first)), result.second);
}

void WriteMetadataBytes::merge(const ReadMetadataBytes &other)
{
    auto &target = backend();
    for (const auto &node : other.backend()) {
        auto add = node.second;
        add->detach(add);
        target.emplace(node.first, std::move(add));
    }
}

WriteMetadataBytes::iterator WriteMetadataBytes::erase(const iterator &pos)
{ return WriteMetadataBytes::iterator(*this, backend().erase(pos.i)); }

WriteMetadataBytes::iterator WriteMetadataBytes::erase(const iterator &first, const iterator &last)
{ return WriteMetadataBytes::iterator(*this, backend().erase(first.i, last.i)); }

ReadMetadataBytes::size_type WriteMetadataBytes::erase(const key_type &key)
{ return backend().erase(key); }

WriteMetadataBytes::iterator::iterator() = default;

WriteMetadataBytes::iterator::~iterator() = default;

WriteMetadataBytes::iterator::iterator(const iterator &) = default;

WriteMetadataBytes::iterator &WriteMetadataBytes::iterator::operator=(const iterator &) = default;

WriteMetadataBytes::iterator::iterator(iterator &&) = default;

WriteMetadataBytes::iterator &WriteMetadataBytes::iterator::operator=(iterator &&) = default;

WriteMetadataBytes::iterator::iterator(WriteMetadataBytes &parent, backing_iterator &&i) : parent(
        &parent), i(std::move(i))
{ }

WriteMetadataBytes::iterator::value_type WriteMetadataBytes::iterator::operator*() const
{ return WriteMetadataBytes::value_type(i->first, (*parent)[i->first]); }

WriteMetadataBytes::iterator::pointer WriteMetadataBytes::iterator::operator->() const
{
    static thread_local std::pair<WriteMetadataBytes::key_type, WriteMetadataBytes::mapped_type>
            ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

WriteMetadataBytes::iterator &WriteMetadataBytes::iterator::operator++()
{
    ++i;
    return *this;
}

WriteMetadataBytes::iterator WriteMetadataBytes::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool WriteMetadataBytes::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteMetadataBytes::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteMetadataBytes::iterator::operator==(const ReadMetadataBytes::const_iterator &other) const
{ return i == other.backing(); }

bool WriteMetadataBytes::iterator::operator!=(const ReadMetadataBytes::const_iterator &other) const
{ return i != other.backing(); }

WriteMetadataBytes::iterator::operator ReadMetadataBytes::const_iterator() const
{
    return ReadMetadataBytes::const_iterator(*parent,
                                             ReadMetadataBytes::const_iterator::backing_iterator(
                                                     i));
}

const WriteMetadataBytes::iterator::backing_iterator &WriteMetadataBytes::iterator::backing() const
{ return i; }

const WriteMetadataBytes::key_type &WriteMetadataBytes::iterator::key() const
{ return i->first; }

WriteMetadataBytes::mapped_type WriteMetadataBytes::iterator::value() const
{ return (*parent)[i->first]; }

PathElement WriteMetadataBytes::iterator::path() const
{ return PathElement(PathElement::Type::MetadataBytes, i->first); }


ReadMetadataFlags::ReadMetadataFlags() = default;

ReadMetadataFlags::~ReadMetadataFlags() = default;

ReadMetadataFlags::ReadMetadataFlags(const ReadMetadataFlags &) = default;

ReadMetadataFlags &ReadMetadataFlags::operator=(const ReadMetadataFlags &) = default;

ReadMetadataFlags::ReadMetadataFlags(ReadMetadataFlags &&) = default;

ReadMetadataFlags &ReadMetadataFlags::operator=(ReadMetadataFlags &&) = default;

ReadMetadataFlags::ReadMetadataFlags(const Read &read) : ReadMetadata(read,
                                                                      PathElement::Type::MetadataFlags)
{ }


WriteMetadataFlags::WriteMetadataFlags() = default;

WriteMetadataFlags::~WriteMetadataFlags() = default;

WriteMetadataFlags::WriteMetadataFlags(const WriteMetadataFlags &) = default;

WriteMetadataFlags &WriteMetadataFlags::operator=(const WriteMetadataFlags &) = default;

WriteMetadataFlags::WriteMetadataFlags(WriteMetadataFlags &&) = default;

WriteMetadataFlags &WriteMetadataFlags::operator=(WriteMetadataFlags &&) = default;

WriteMetadataFlags::WriteMetadataFlags(Write &write) : ReadMetadataFlags(write)
{ }

WriteMetadataFlags::backend_map &WriteMetadataFlags::backend()
{
    if (path.empty())
        return static_cast<NodeMetadataBase *>(top->write(
                Node::WriteGoal::MetadataFlags))->children;
    else
        return std::static_pointer_cast<NodeMetadataBase>(
                top->write(path, Node::WriteGoal::MetadataFlags))->children;
}

WriteMetadataFlags::mapped_type WriteMetadataFlags::operator[](const key_type &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataFlags, key);
    return result;
}

WriteMetadataFlags::mapped_type WriteMetadataFlags::operator[](key_type &&key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataFlags, std::move(key));
    return result;
}

WriteMetadataFlags::mapped_type WriteMetadataFlags::operator[](const char *key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataFlags, key);
    return result;
}

WriteMetadataFlags::mapped_type WriteMetadataFlags::operator[](const QString &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataFlags, key);
    return result;
}

WriteMetadataFlags::iterator WriteMetadataFlags::begin()
{ return WriteMetadataFlags::iterator(*this, backend().begin()); }

WriteMetadataFlags::iterator WriteMetadataFlags::end()
{ return WriteMetadataFlags::iterator(*this, backend().end()); }

WriteMetadataFlags::iterator WriteMetadataFlags::find(const key_type &key)
{ return WriteMetadataFlags::iterator(*this, backend().find(key)); }

std::pair<WriteMetadataFlags::iterator, bool> WriteMetadataFlags::insert(const value_type &value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(value.first, std::move(source));
    return std::pair<WriteMetadataFlags::iterator, bool>(
            WriteMetadataFlags::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataFlags::iterator, bool> WriteMetadataFlags::insert(value_type &&value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(std::move(value.first), std::move(source));
    return std::pair<WriteMetadataFlags::iterator, bool>(
            WriteMetadataFlags::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataFlags::iterator, bool> WriteMetadataFlags::emplace(const key_type &key,
                                                                          const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(key, std::move(source));
    return std::pair<WriteMetadataFlags::iterator, bool>(
            WriteMetadataFlags::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataFlags::iterator, bool> WriteMetadataFlags::emplace(key_type &&key,
                                                                          const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(std::move(key), std::move(source));
    return std::pair<WriteMetadataFlags::iterator, bool>(
            WriteMetadataFlags::iterator(*this, std::move(result.first)), result.second);
}

void WriteMetadataFlags::merge(const ReadMetadataFlags &other)
{
    auto &target = backend();
    for (const auto &node : other.backend()) {
        auto add = node.second;
        add->detach(add);
        target.emplace(node.first, std::move(add));
    }
}

WriteMetadataFlags::iterator WriteMetadataFlags::erase(const iterator &pos)
{ return WriteMetadataFlags::iterator(*this, backend().erase(pos.i)); }

WriteMetadataFlags::iterator WriteMetadataFlags::erase(const iterator &first, const iterator &last)
{ return WriteMetadataFlags::iterator(*this, backend().erase(first.i, last.i)); }

ReadMetadataFlags::size_type WriteMetadataFlags::erase(const key_type &key)
{ return backend().erase(key); }

WriteMetadataFlags::iterator::iterator() = default;

WriteMetadataFlags::iterator::~iterator() = default;

WriteMetadataFlags::iterator::iterator(const iterator &) = default;

WriteMetadataFlags::iterator &WriteMetadataFlags::iterator::operator=(const iterator &) = default;

WriteMetadataFlags::iterator::iterator(iterator &&) = default;

WriteMetadataFlags::iterator &WriteMetadataFlags::iterator::operator=(iterator &&) = default;

WriteMetadataFlags::iterator::iterator(WriteMetadataFlags &parent, backing_iterator &&i) : parent(
        &parent), i(std::move(i))
{ }

WriteMetadataFlags::iterator::value_type WriteMetadataFlags::iterator::operator*() const
{ return WriteMetadataFlags::value_type(i->first, (*parent)[i->first]); }

WriteMetadataFlags::iterator::pointer WriteMetadataFlags::iterator::operator->() const
{
    static thread_local std::pair<WriteMetadataFlags::key_type, WriteMetadataFlags::mapped_type>
            ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

WriteMetadataFlags::iterator &WriteMetadataFlags::iterator::operator++()
{
    ++i;
    return *this;
}

WriteMetadataFlags::iterator WriteMetadataFlags::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool WriteMetadataFlags::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteMetadataFlags::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteMetadataFlags::iterator::operator==(const ReadMetadataFlags::const_iterator &other) const
{ return i == other.backing(); }

bool WriteMetadataFlags::iterator::operator!=(const ReadMetadataFlags::const_iterator &other) const
{ return i != other.backing(); }

WriteMetadataFlags::iterator::operator ReadMetadataFlags::const_iterator() const
{
    return ReadMetadataFlags::const_iterator(*parent,
                                             ReadMetadataFlags::const_iterator::backing_iterator(
                                                     i));
}

const WriteMetadataFlags::iterator::backing_iterator &WriteMetadataFlags::iterator::backing() const
{ return i; }

const WriteMetadataFlags::key_type &WriteMetadataFlags::iterator::key() const
{ return i->first; }

WriteMetadataFlags::mapped_type WriteMetadataFlags::iterator::value() const
{ return (*parent)[i->first]; }

PathElement WriteMetadataFlags::iterator::path() const
{ return PathElement(PathElement::Type::MetadataFlags, i->first); }


ReadMetadataArray::ReadMetadataArray() = default;

ReadMetadataArray::~ReadMetadataArray() = default;

ReadMetadataArray::ReadMetadataArray(const ReadMetadataArray &) = default;

ReadMetadataArray &ReadMetadataArray::operator=(const ReadMetadataArray &) = default;

ReadMetadataArray::ReadMetadataArray(ReadMetadataArray &&) = default;

ReadMetadataArray &ReadMetadataArray::operator=(ReadMetadataArray &&) = default;

ReadMetadataArray::ReadMetadataArray(const Read &read) : ReadMetadata(read,
                                                                      PathElement::Type::MetadataArray)
{ }


WriteMetadataArray::WriteMetadataArray() = default;

WriteMetadataArray::~WriteMetadataArray() = default;

WriteMetadataArray::WriteMetadataArray(const WriteMetadataArray &) = default;

WriteMetadataArray &WriteMetadataArray::operator=(const WriteMetadataArray &) = default;

WriteMetadataArray::WriteMetadataArray(WriteMetadataArray &&) = default;

WriteMetadataArray &WriteMetadataArray::operator=(WriteMetadataArray &&) = default;

WriteMetadataArray::WriteMetadataArray(Write &write) : ReadMetadataArray(write)
{ }

WriteMetadataArray::backend_map &WriteMetadataArray::backend()
{
    if (path.empty())
        return static_cast<NodeMetadataBase *>(top->write(
                Node::WriteGoal::MetadataArray))->children;
    else
        return std::static_pointer_cast<NodeMetadataBase>(
                top->write(path, Node::WriteGoal::MetadataArray))->children;
}

WriteMetadataArray::mapped_type WriteMetadataArray::operator[](const key_type &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataArray, key);
    return result;
}

WriteMetadataArray::mapped_type WriteMetadataArray::operator[](key_type &&key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataArray, std::move(key));
    return result;
}

WriteMetadataArray::mapped_type WriteMetadataArray::operator[](const char *key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataArray, key);
    return result;
}

WriteMetadataArray::mapped_type WriteMetadataArray::operator[](const QString &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataArray, key);
    return result;
}

WriteMetadataArray::iterator WriteMetadataArray::begin()
{ return WriteMetadataArray::iterator(*this, backend().begin()); }

WriteMetadataArray::iterator WriteMetadataArray::end()
{ return WriteMetadataArray::iterator(*this, backend().end()); }

WriteMetadataArray::iterator WriteMetadataArray::find(const key_type &key)
{ return WriteMetadataArray::iterator(*this, backend().find(key)); }

std::pair<WriteMetadataArray::iterator, bool> WriteMetadataArray::insert(const value_type &value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(value.first, std::move(source));
    return std::pair<WriteMetadataArray::iterator, bool>(
            WriteMetadataArray::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataArray::iterator, bool> WriteMetadataArray::insert(value_type &&value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(std::move(value.first), std::move(source));
    return std::pair<WriteMetadataArray::iterator, bool>(
            WriteMetadataArray::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataArray::iterator, bool> WriteMetadataArray::emplace(const key_type &key,
                                                                          const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(key, std::move(source));
    return std::pair<WriteMetadataArray::iterator, bool>(
            WriteMetadataArray::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataArray::iterator, bool> WriteMetadataArray::emplace(key_type &&key,
                                                                          const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(std::move(key), std::move(source));
    return std::pair<WriteMetadataArray::iterator, bool>(
            WriteMetadataArray::iterator(*this, std::move(result.first)), result.second);
}

void WriteMetadataArray::merge(const ReadMetadataArray &other)
{
    auto &target = backend();
    for (const auto &node : other.backend()) {
        auto add = node.second;
        add->detach(add);
        target.emplace(node.first, std::move(add));
    }
}

WriteMetadataArray::iterator WriteMetadataArray::erase(const iterator &pos)
{ return WriteMetadataArray::iterator(*this, backend().erase(pos.i)); }

WriteMetadataArray::iterator WriteMetadataArray::erase(const iterator &first, const iterator &last)
{ return WriteMetadataArray::iterator(*this, backend().erase(first.i, last.i)); }

ReadMetadataArray::size_type WriteMetadataArray::erase(const key_type &key)
{ return backend().erase(key); }

WriteMetadataArray::iterator::iterator() = default;

WriteMetadataArray::iterator::~iterator() = default;

WriteMetadataArray::iterator::iterator(const iterator &) = default;

WriteMetadataArray::iterator &WriteMetadataArray::iterator::operator=(const iterator &) = default;

WriteMetadataArray::iterator::iterator(iterator &&) = default;

WriteMetadataArray::iterator &WriteMetadataArray::iterator::operator=(iterator &&) = default;

WriteMetadataArray::iterator::iterator(WriteMetadataArray &parent, backing_iterator &&i) : parent(
        &parent), i(std::move(i))
{ }

WriteMetadataArray::iterator::value_type WriteMetadataArray::iterator::operator*() const
{ return WriteMetadataArray::value_type(i->first, (*parent)[i->first]); }

WriteMetadataArray::iterator::pointer WriteMetadataArray::iterator::operator->() const
{
    static thread_local std::pair<WriteMetadataArray::key_type, WriteMetadataArray::mapped_type>
            ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

WriteMetadataArray::iterator &WriteMetadataArray::iterator::operator++()
{
    ++i;
    return *this;
}

WriteMetadataArray::iterator WriteMetadataArray::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool WriteMetadataArray::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteMetadataArray::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteMetadataArray::iterator::operator==(const ReadMetadataArray::const_iterator &other) const
{ return i == other.backing(); }

bool WriteMetadataArray::iterator::operator!=(const ReadMetadataArray::const_iterator &other) const
{ return i != other.backing(); }

WriteMetadataArray::iterator::operator ReadMetadataArray::const_iterator() const
{
    return ReadMetadataArray::const_iterator(*parent,
                                             ReadMetadataArray::const_iterator::backing_iterator(
                                                     i));
}

const WriteMetadataArray::iterator::backing_iterator &WriteMetadataArray::iterator::backing() const
{ return i; }

const WriteMetadataArray::key_type &WriteMetadataArray::iterator::key() const
{ return i->first; }

WriteMetadataArray::mapped_type WriteMetadataArray::iterator::value() const
{ return (*parent)[i->first]; }

PathElement WriteMetadataArray::iterator::path() const
{ return PathElement(PathElement::Type::MetadataArray, i->first); }


ReadMetadataMatrix::ReadMetadataMatrix() = default;

ReadMetadataMatrix::~ReadMetadataMatrix() = default;

ReadMetadataMatrix::ReadMetadataMatrix(const ReadMetadataMatrix &) = default;

ReadMetadataMatrix &ReadMetadataMatrix::operator=(const ReadMetadataMatrix &) = default;

ReadMetadataMatrix::ReadMetadataMatrix(ReadMetadataMatrix &&) = default;

ReadMetadataMatrix &ReadMetadataMatrix::operator=(ReadMetadataMatrix &&) = default;

ReadMetadataMatrix::ReadMetadataMatrix(const Read &read) : ReadMetadata(read,
                                                                        PathElement::Type::MetadataMatrix)
{ }


WriteMetadataMatrix::WriteMetadataMatrix() = default;

WriteMetadataMatrix::~WriteMetadataMatrix() = default;

WriteMetadataMatrix::WriteMetadataMatrix(const WriteMetadataMatrix &) = default;

WriteMetadataMatrix &WriteMetadataMatrix::operator=(const WriteMetadataMatrix &) = default;

WriteMetadataMatrix::WriteMetadataMatrix(WriteMetadataMatrix &&) = default;

WriteMetadataMatrix &WriteMetadataMatrix::operator=(WriteMetadataMatrix &&) = default;

WriteMetadataMatrix::WriteMetadataMatrix(Write &write) : ReadMetadataMatrix(write)
{ }

WriteMetadataMatrix::backend_map &WriteMetadataMatrix::backend()
{
    if (path.empty())
        return static_cast<NodeMetadataBase *>(top->write(
                Node::WriteGoal::MetadataMatrix))->children;
    else
        return std::static_pointer_cast<NodeMetadataBase>(
                top->write(path, Node::WriteGoal::MetadataMatrix))->children;
}

WriteMetadataMatrix::mapped_type WriteMetadataMatrix::operator[](const key_type &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataMatrix, key);
    return result;
}

WriteMetadataMatrix::mapped_type WriteMetadataMatrix::operator[](key_type &&key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataMatrix, std::move(key));
    return result;
}

WriteMetadataMatrix::mapped_type WriteMetadataMatrix::operator[](const char *key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataMatrix, key);
    return result;
}

WriteMetadataMatrix::mapped_type WriteMetadataMatrix::operator[](const QString &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataMatrix, key);
    return result;
}

WriteMetadataMatrix::iterator WriteMetadataMatrix::begin()
{ return WriteMetadataMatrix::iterator(*this, backend().begin()); }

WriteMetadataMatrix::iterator WriteMetadataMatrix::end()
{ return WriteMetadataMatrix::iterator(*this, backend().end()); }

WriteMetadataMatrix::iterator WriteMetadataMatrix::find(const key_type &key)
{ return WriteMetadataMatrix::iterator(*this, backend().find(key)); }

std::pair<WriteMetadataMatrix::iterator, bool> WriteMetadataMatrix::insert(const value_type &value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(value.first, std::move(source));
    return std::pair<WriteMetadataMatrix::iterator, bool>(
            WriteMetadataMatrix::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataMatrix::iterator, bool> WriteMetadataMatrix::insert(value_type &&value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(std::move(value.first), std::move(source));
    return std::pair<WriteMetadataMatrix::iterator, bool>(
            WriteMetadataMatrix::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataMatrix::iterator, bool> WriteMetadataMatrix::emplace(const key_type &key,
                                                                            const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(key, std::move(source));
    return std::pair<WriteMetadataMatrix::iterator, bool>(
            WriteMetadataMatrix::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataMatrix::iterator, bool> WriteMetadataMatrix::emplace(key_type &&key,
                                                                            const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(std::move(key), std::move(source));
    return std::pair<WriteMetadataMatrix::iterator, bool>(
            WriteMetadataMatrix::iterator(*this, std::move(result.first)), result.second);
}

void WriteMetadataMatrix::merge(const ReadMetadataMatrix &other)
{
    auto &target = backend();
    for (const auto &node : other.backend()) {
        auto add = node.second;
        add->detach(add);
        target.emplace(node.first, std::move(add));
    }
}

WriteMetadataMatrix::iterator WriteMetadataMatrix::erase(const iterator &pos)
{ return WriteMetadataMatrix::iterator(*this, backend().erase(pos.i)); }

WriteMetadataMatrix::iterator WriteMetadataMatrix::erase(const iterator &first,
                                                         const iterator &last)
{ return WriteMetadataMatrix::iterator(*this, backend().erase(first.i, last.i)); }

ReadMetadataMatrix::size_type WriteMetadataMatrix::erase(const key_type &key)
{ return backend().erase(key); }

WriteMetadataMatrix::iterator::iterator() = default;

WriteMetadataMatrix::iterator::~iterator() = default;

WriteMetadataMatrix::iterator::iterator(const iterator &) = default;

WriteMetadataMatrix::iterator &WriteMetadataMatrix::iterator::operator=(const iterator &) = default;

WriteMetadataMatrix::iterator::iterator(iterator &&) = default;

WriteMetadataMatrix::iterator &WriteMetadataMatrix::iterator::operator=(iterator &&) = default;

WriteMetadataMatrix::iterator::iterator(WriteMetadataMatrix &parent, backing_iterator &&i) : parent(
        &parent), i(std::move(i))
{ }

WriteMetadataMatrix::iterator::value_type WriteMetadataMatrix::iterator::operator*() const
{ return WriteMetadataMatrix::value_type(i->first, (*parent)[i->first]); }

WriteMetadataMatrix::iterator::pointer WriteMetadataMatrix::iterator::operator->() const
{
    static thread_local std::pair<WriteMetadataMatrix::key_type, WriteMetadataMatrix::mapped_type>
            ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

WriteMetadataMatrix::iterator &WriteMetadataMatrix::iterator::operator++()
{
    ++i;
    return *this;
}

WriteMetadataMatrix::iterator WriteMetadataMatrix::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool WriteMetadataMatrix::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteMetadataMatrix::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteMetadataMatrix::iterator::operator==(const ReadMetadataMatrix::const_iterator &other) const
{ return i == other.backing(); }

bool WriteMetadataMatrix::iterator::operator!=(const ReadMetadataMatrix::const_iterator &other) const
{ return i != other.backing(); }

WriteMetadataMatrix::iterator::operator ReadMetadataMatrix::const_iterator() const
{
    return ReadMetadataMatrix::const_iterator(*parent,
                                              ReadMetadataMatrix::const_iterator::backing_iterator(
                                                      i));
}

const WriteMetadataMatrix::iterator::backing_iterator &WriteMetadataMatrix::iterator::backing() const
{ return i; }

const WriteMetadataMatrix::key_type &WriteMetadataMatrix::iterator::key() const
{ return i->first; }

WriteMetadataMatrix::mapped_type WriteMetadataMatrix::iterator::value() const
{ return (*parent)[i->first]; }

PathElement WriteMetadataMatrix::iterator::path() const
{ return PathElement(PathElement::Type::MetadataMatrix, i->first); }


ReadMetadataHash::ReadMetadataHash() = default;

ReadMetadataHash::~ReadMetadataHash() = default;

ReadMetadataHash::ReadMetadataHash(const ReadMetadataHash &) = default;

ReadMetadataHash &ReadMetadataHash::operator=(const ReadMetadataHash &) = default;

ReadMetadataHash::ReadMetadataHash(ReadMetadataHash &&) = default;

ReadMetadataHash &ReadMetadataHash::operator=(ReadMetadataHash &&) = default;

ReadMetadataHash::ReadMetadataHash(const Read &read) : ReadMetadata(read,
                                                                    PathElement::Type::MetadataHash)
{ }


WriteMetadataHash::WriteMetadataHash() = default;

WriteMetadataHash::~WriteMetadataHash() = default;

WriteMetadataHash::WriteMetadataHash(const WriteMetadataHash &) = default;

WriteMetadataHash &WriteMetadataHash::operator=(const WriteMetadataHash &) = default;

WriteMetadataHash::WriteMetadataHash(WriteMetadataHash &&) = default;

WriteMetadataHash &WriteMetadataHash::operator=(WriteMetadataHash &&) = default;

WriteMetadataHash::WriteMetadataHash(Write &write) : ReadMetadataHash(write)
{ }

WriteMetadataHash::backend_map &WriteMetadataHash::backend()
{
    if (path.empty())
        return static_cast<NodeMetadataBase *>(top->write(Node::WriteGoal::MetadataHash))->children;
    else
        return std::static_pointer_cast<NodeMetadataBase>(
                top->write(path, Node::WriteGoal::MetadataHash))->children;
}

WriteMetadataHash::mapped_type WriteMetadataHash::operator[](const key_type &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataHash, key);
    return result;
}

WriteMetadataHash::mapped_type WriteMetadataHash::operator[](key_type &&key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataHash, std::move(key));
    return result;
}

WriteMetadataHash::mapped_type WriteMetadataHash::operator[](const char *key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataHash, key);
    return result;
}

WriteMetadataHash::mapped_type WriteMetadataHash::operator[](const QString &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataHash, key);
    return result;
}

WriteMetadataHash::iterator WriteMetadataHash::begin()
{ return WriteMetadataHash::iterator(*this, backend().begin()); }

WriteMetadataHash::iterator WriteMetadataHash::end()
{ return WriteMetadataHash::iterator(*this, backend().end()); }

WriteMetadataHash::iterator WriteMetadataHash::find(const key_type &key)
{ return WriteMetadataHash::iterator(*this, backend().find(key)); }

std::pair<WriteMetadataHash::iterator, bool> WriteMetadataHash::insert(const value_type &value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(value.first, std::move(source));
    return std::pair<WriteMetadataHash::iterator, bool>(
            WriteMetadataHash::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataHash::iterator, bool> WriteMetadataHash::insert(value_type &&value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(std::move(value.first), std::move(source));
    return std::pair<WriteMetadataHash::iterator, bool>(
            WriteMetadataHash::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataHash::iterator, bool> WriteMetadataHash::emplace(const key_type &key,
                                                                        const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(key, std::move(source));
    return std::pair<WriteMetadataHash::iterator, bool>(
            WriteMetadataHash::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataHash::iterator, bool> WriteMetadataHash::emplace(key_type &&key,
                                                                        const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(std::move(key), std::move(source));
    return std::pair<WriteMetadataHash::iterator, bool>(
            WriteMetadataHash::iterator(*this, std::move(result.first)), result.second);
}

void WriteMetadataHash::merge(const ReadMetadataHash &other)
{
    auto &target = backend();
    for (const auto &node : other.backend()) {
        auto add = node.second;
        add->detach(add);
        target.emplace(node.first, std::move(add));
    }
}

WriteMetadataHash::iterator WriteMetadataHash::erase(const iterator &pos)
{ return WriteMetadataHash::iterator(*this, backend().erase(pos.i)); }

WriteMetadataHash::iterator WriteMetadataHash::erase(const iterator &first, const iterator &last)
{ return WriteMetadataHash::iterator(*this, backend().erase(first.i, last.i)); }

ReadMetadataHash::size_type WriteMetadataHash::erase(const key_type &key)
{ return backend().erase(key); }

WriteMetadataHash::iterator::iterator() = default;

WriteMetadataHash::iterator::~iterator() = default;

WriteMetadataHash::iterator::iterator(const iterator &) = default;

WriteMetadataHash::iterator &WriteMetadataHash::iterator::operator=(const iterator &) = default;

WriteMetadataHash::iterator::iterator(iterator &&) = default;

WriteMetadataHash::iterator &WriteMetadataHash::iterator::operator=(iterator &&) = default;

WriteMetadataHash::iterator::iterator(WriteMetadataHash &parent, backing_iterator &&i) : parent(
        &parent), i(std::move(i))
{ }

WriteMetadataHash::iterator::value_type WriteMetadataHash::iterator::operator*() const
{ return WriteMetadataHash::value_type(i->first, (*parent)[i->first]); }

WriteMetadataHash::iterator::pointer WriteMetadataHash::iterator::operator->() const
{
    static thread_local std::pair<WriteMetadataHash::key_type, WriteMetadataHash::mapped_type> ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

WriteMetadataHash::iterator &WriteMetadataHash::iterator::operator++()
{
    ++i;
    return *this;
}

WriteMetadataHash::iterator WriteMetadataHash::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool WriteMetadataHash::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteMetadataHash::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteMetadataHash::iterator::operator==(const ReadMetadataHash::const_iterator &other) const
{ return i == other.backing(); }

bool WriteMetadataHash::iterator::operator!=(const ReadMetadataHash::const_iterator &other) const
{ return i != other.backing(); }

WriteMetadataHash::iterator::operator ReadMetadataHash::const_iterator() const
{
    return ReadMetadataHash::const_iterator(*parent,
                                            ReadMetadataHash::const_iterator::backing_iterator(i));
}

const WriteMetadataHash::iterator::backing_iterator &WriteMetadataHash::iterator::backing() const
{ return i; }

const WriteMetadataHash::key_type &WriteMetadataHash::iterator::key() const
{ return i->first; }

WriteMetadataHash::mapped_type WriteMetadataHash::iterator::value() const
{ return (*parent)[i->first]; }

PathElement WriteMetadataHash::iterator::path() const
{ return PathElement(PathElement::Type::MetadataHash, i->first); }


ReadMetadataKeyframe::ReadMetadataKeyframe() = default;

ReadMetadataKeyframe::~ReadMetadataKeyframe() = default;

ReadMetadataKeyframe::ReadMetadataKeyframe(const ReadMetadataKeyframe &) = default;

ReadMetadataKeyframe &ReadMetadataKeyframe::operator=(const ReadMetadataKeyframe &) = default;

ReadMetadataKeyframe::ReadMetadataKeyframe(ReadMetadataKeyframe &&) = default;

ReadMetadataKeyframe &ReadMetadataKeyframe::operator=(ReadMetadataKeyframe &&) = default;

ReadMetadataKeyframe::ReadMetadataKeyframe(const Read &read) : ReadMetadata(read,
                                                                            PathElement::Type::MetadataKeyframe)
{ }


WriteMetadataKeyframe::WriteMetadataKeyframe() = default;

WriteMetadataKeyframe::~WriteMetadataKeyframe() = default;

WriteMetadataKeyframe::WriteMetadataKeyframe(const WriteMetadataKeyframe &) = default;

WriteMetadataKeyframe &WriteMetadataKeyframe::operator=(const WriteMetadataKeyframe &) = default;

WriteMetadataKeyframe::WriteMetadataKeyframe(WriteMetadataKeyframe &&) = default;

WriteMetadataKeyframe &WriteMetadataKeyframe::operator=(WriteMetadataKeyframe &&) = default;

WriteMetadataKeyframe::WriteMetadataKeyframe(Write &write) : ReadMetadataKeyframe(write)
{ }

WriteMetadataKeyframe::backend_map &WriteMetadataKeyframe::backend()
{
    if (path.empty())
        return static_cast<NodeMetadataBase *>(top->write(
                Node::WriteGoal::MetadataKeyframe))->children;
    else
        return std::static_pointer_cast<NodeMetadataBase>(
                top->write(path, Node::WriteGoal::MetadataKeyframe))->children;
}

WriteMetadataKeyframe::mapped_type WriteMetadataKeyframe::operator[](const key_type &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataKeyframe, key);
    return result;
}

WriteMetadataKeyframe::mapped_type WriteMetadataKeyframe::operator[](key_type &&key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataKeyframe, std::move(key));
    return result;
}

WriteMetadataKeyframe::mapped_type WriteMetadataKeyframe::operator[](const char *key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataKeyframe, key);
    return result;
}

WriteMetadataKeyframe::mapped_type WriteMetadataKeyframe::operator[](const QString &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataKeyframe, key);
    return result;
}

WriteMetadataKeyframe::iterator WriteMetadataKeyframe::begin()
{ return WriteMetadataKeyframe::iterator(*this, backend().begin()); }

WriteMetadataKeyframe::iterator WriteMetadataKeyframe::end()
{ return WriteMetadataKeyframe::iterator(*this, backend().end()); }

WriteMetadataKeyframe::iterator WriteMetadataKeyframe::find(const key_type &key)
{ return WriteMetadataKeyframe::iterator(*this, backend().find(key)); }

std::pair<WriteMetadataKeyframe::iterator,
          bool> WriteMetadataKeyframe::insert(const value_type &value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(value.first, std::move(source));
    return std::pair<WriteMetadataKeyframe::iterator, bool>(
            WriteMetadataKeyframe::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataKeyframe::iterator, bool> WriteMetadataKeyframe::insert(value_type &&value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(std::move(value.first), std::move(source));
    return std::pair<WriteMetadataKeyframe::iterator, bool>(
            WriteMetadataKeyframe::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataKeyframe::iterator, bool> WriteMetadataKeyframe::emplace(const key_type &key,
                                                                                const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(key, std::move(source));
    return std::pair<WriteMetadataKeyframe::iterator, bool>(
            WriteMetadataKeyframe::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataKeyframe::iterator, bool> WriteMetadataKeyframe::emplace(key_type &&key,
                                                                                const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(std::move(key), std::move(source));
    return std::pair<WriteMetadataKeyframe::iterator, bool>(
            WriteMetadataKeyframe::iterator(*this, std::move(result.first)), result.second);
}

void WriteMetadataKeyframe::merge(const ReadMetadataKeyframe &other)
{
    auto &target = backend();
    for (const auto &node : other.backend()) {
        auto add = node.second;
        add->detach(add);
        target.emplace(node.first, std::move(add));
    }
}

WriteMetadataKeyframe::iterator WriteMetadataKeyframe::erase(const iterator &pos)
{ return WriteMetadataKeyframe::iterator(*this, backend().erase(pos.i)); }

WriteMetadataKeyframe::iterator WriteMetadataKeyframe::erase(const iterator &first,
                                                             const iterator &last)
{ return WriteMetadataKeyframe::iterator(*this, backend().erase(first.i, last.i)); }

ReadMetadataKeyframe::size_type WriteMetadataKeyframe::erase(const key_type &key)
{ return backend().erase(key); }

WriteMetadataKeyframe::iterator::iterator() = default;

WriteMetadataKeyframe::iterator::~iterator() = default;

WriteMetadataKeyframe::iterator::iterator(const iterator &) = default;

WriteMetadataKeyframe::iterator &WriteMetadataKeyframe::iterator::operator=(const iterator &) = default;

WriteMetadataKeyframe::iterator::iterator(iterator &&) = default;

WriteMetadataKeyframe::iterator &WriteMetadataKeyframe::iterator::operator=(iterator &&) = default;

WriteMetadataKeyframe::iterator::iterator(WriteMetadataKeyframe &parent, backing_iterator &&i)
        : parent(&parent), i(std::move(i))
{ }

WriteMetadataKeyframe::iterator::value_type WriteMetadataKeyframe::iterator::operator*() const
{ return WriteMetadataKeyframe::value_type(i->first, (*parent)[i->first]); }

WriteMetadataKeyframe::iterator::pointer WriteMetadataKeyframe::iterator::operator->() const
{
    static thread_local std::pair<WriteMetadataKeyframe::key_type,
                                  WriteMetadataKeyframe::mapped_type> ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

WriteMetadataKeyframe::iterator &WriteMetadataKeyframe::iterator::operator++()
{
    ++i;
    return *this;
}

WriteMetadataKeyframe::iterator WriteMetadataKeyframe::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool WriteMetadataKeyframe::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteMetadataKeyframe::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteMetadataKeyframe::iterator::operator==(const ReadMetadataKeyframe::const_iterator &other) const
{ return i == other.backing(); }

bool WriteMetadataKeyframe::iterator::operator!=(const ReadMetadataKeyframe::const_iterator &other) const
{ return i != other.backing(); }

WriteMetadataKeyframe::iterator::operator ReadMetadataKeyframe::const_iterator() const
{
    return ReadMetadataKeyframe::const_iterator(*parent,
                                                ReadMetadataKeyframe::const_iterator::backing_iterator(
                                                        i));
}

const WriteMetadataKeyframe::iterator::backing_iterator &WriteMetadataKeyframe::iterator::backing() const
{ return i; }

const WriteMetadataKeyframe::key_type &WriteMetadataKeyframe::iterator::key() const
{ return i->first; }

WriteMetadataKeyframe::mapped_type WriteMetadataKeyframe::iterator::value() const
{ return (*parent)[i->first]; }

PathElement WriteMetadataKeyframe::iterator::path() const
{ return PathElement(PathElement::Type::MetadataKeyframe, i->first); }


ReadMetadataSingleFlag::ReadMetadataSingleFlag() = default;

ReadMetadataSingleFlag::~ReadMetadataSingleFlag() = default;

ReadMetadataSingleFlag::ReadMetadataSingleFlag(const ReadMetadataSingleFlag &) = default;

ReadMetadataSingleFlag &ReadMetadataSingleFlag::operator=(const ReadMetadataSingleFlag &) = default;

ReadMetadataSingleFlag::ReadMetadataSingleFlag(ReadMetadataSingleFlag &&) = default;

ReadMetadataSingleFlag &ReadMetadataSingleFlag::operator=(ReadMetadataSingleFlag &&) = default;

ReadMetadataSingleFlag::ReadMetadataSingleFlag(const Read &read) : top(read.top), path(read.path)
{ }

void ReadMetadataSingleFlag::swap(ReadMetadataSingleFlag &a, ReadMetadataSingleFlag &b)
{
    using std::swap;
    swap(a.top, b.top);
    swap(a.path, b.path);
}

const ReadMetadataSingleFlag::backend_map &ReadMetadataSingleFlag::backend() const
{
    static const ReadMetadataSingleFlag::backend_map invalid;
    if (path.empty()) {
        switch (top->node()->type()) {
        case Node::Type::MetadataFlags:
        case Node::Type::MetadataFlagsConstant:
            break;
        default:
            return invalid;
        }
        return top->node<NodeMetadataFlags>()->flags;
    }

    auto ptr = top->read(path);
    if (!ptr)
        return invalid;

    switch (ptr->type()) {
    case Node::Type::MetadataFlags:
    case Node::Type::MetadataFlagsConstant:
        break;
    default:
        return invalid;
    }

    /* Safe to keep the pointer as long as the root remains unchanged, since it will
     * have a pointer to it regardless.  Any write access invalidates this,
     * however. */
    return static_cast<const NodeMetadataFlags *>(ptr.get())->flags;
}

bool ReadMetadataSingleFlag::empty() const
{ return backend().empty(); }

ReadMetadataSingleFlag::size_type ReadMetadataSingleFlag::size() const
{ return backend().size(); }

ReadMetadataSingleFlag::size_type ReadMetadataSingleFlag::count(const key_type &key) const
{ return backend().count(key); }

bool ReadMetadataSingleFlag::operator==(const ReadMetadataSingleFlag &other) const
{
    const auto &a = backend();
    const auto &b = other.backend();
    if (a.size() != b.size())
        return false;
    for (const auto &check : a) {
        auto i = b.find(check.first);
        if (i == b.end())
            return false;
        if (!check.second->identicalTo(*i->second))
            return false;
    }
    return true;
}

bool ReadMetadataSingleFlag::operator!=(const ReadMetadataSingleFlag &other) const
{ return !(*this == other); }

ReadMetadataSingleFlag::mapped_type ReadMetadataSingleFlag::operator[](const key_type &key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataSingleFlag, key);
    return result;
}

ReadMetadataSingleFlag::mapped_type ReadMetadataSingleFlag::operator[](key_type &&key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataSingleFlag, std::move(key));
    return result;
}

ReadMetadataSingleFlag::mapped_type ReadMetadataSingleFlag::operator[](const char *key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataSingleFlag, key);
    return result;
}

ReadMetadataSingleFlag::mapped_type ReadMetadataSingleFlag::operator[](const QString &key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataSingleFlag, key);
    return result;
}

ReadMetadataSingleFlag::const_iterator ReadMetadataSingleFlag::begin() const
{ return ReadMetadataSingleFlag::const_iterator(*this, backend().begin()); }

ReadMetadataSingleFlag::const_iterator ReadMetadataSingleFlag::end() const
{ return ReadMetadataSingleFlag::const_iterator(*this, backend().end()); }

ReadMetadataSingleFlag::const_iterator ReadMetadataSingleFlag::find(const key_type &key) const
{ return ReadMetadataSingleFlag::const_iterator(*this, backend().find(key)); }

std::unordered_set<ReadMetadataSingleFlag::key_type> ReadMetadataSingleFlag::keys() const
{
    std::unordered_set<ReadMetadataSingleFlag::key_type> result;
    for (const auto &add : backend()) {
        result.insert(add.first);
    }
    return result;
}

ReadMetadataSingleFlag::const_iterator::const_iterator() = default;

ReadMetadataSingleFlag::const_iterator::~const_iterator() = default;

ReadMetadataSingleFlag::const_iterator::const_iterator(const const_iterator &) = default;

ReadMetadataSingleFlag::const_iterator &ReadMetadataSingleFlag::const_iterator::operator=(const const_iterator &) = default;

ReadMetadataSingleFlag::const_iterator::const_iterator(const_iterator &&) = default;

ReadMetadataSingleFlag::const_iterator &ReadMetadataSingleFlag::const_iterator::operator=(
        const_iterator &&) = default;

ReadMetadataSingleFlag::const_iterator::const_iterator(const ReadMetadataSingleFlag &parent,
                                                       backing_iterator &&i) : parent(&parent),
                                                                               i(std::move(i))
{ }

ReadMetadataSingleFlag::const_iterator::value_type ReadMetadataSingleFlag::const_iterator::operator*() const
{ return ReadMetadataSingleFlag::value_type(i->first, (*parent)[i->first]); }

ReadMetadataSingleFlag::const_iterator::pointer ReadMetadataSingleFlag::const_iterator::operator->() const
{
    static thread_local std::pair<ReadMetadataSingleFlag::key_type,
                                  ReadMetadataSingleFlag::mapped_type> ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

ReadMetadataSingleFlag::const_iterator &ReadMetadataSingleFlag::const_iterator::operator++()
{
    ++i;
    return *this;
}

ReadMetadataSingleFlag::const_iterator ReadMetadataSingleFlag::const_iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool ReadMetadataSingleFlag::const_iterator::operator==(const const_iterator &other) const
{ return i == other.i; }

bool ReadMetadataSingleFlag::const_iterator::operator!=(const const_iterator &other) const
{ return i != other.i; }

const ReadMetadataSingleFlag::const_iterator::backing_iterator &ReadMetadataSingleFlag::const_iterator::backing() const
{ return i; }

const ReadMetadataSingleFlag::key_type &ReadMetadataSingleFlag::const_iterator::key() const
{ return i->first; }

ReadMetadataSingleFlag::mapped_type ReadMetadataSingleFlag::const_iterator::value() const
{ return (*parent)[i->first]; }

PathElement ReadMetadataSingleFlag::const_iterator::path() const
{ return PathElement(PathElement::Type::MetadataSingleFlag, i->first); }


WriteMetadataSingleFlag::WriteMetadataSingleFlag() = default;

WriteMetadataSingleFlag::~WriteMetadataSingleFlag() = default;

WriteMetadataSingleFlag::WriteMetadataSingleFlag(const WriteMetadataSingleFlag &) = default;

WriteMetadataSingleFlag &WriteMetadataSingleFlag::operator=(const WriteMetadataSingleFlag &) = default;

WriteMetadataSingleFlag::WriteMetadataSingleFlag(WriteMetadataSingleFlag &&) = default;

WriteMetadataSingleFlag &WriteMetadataSingleFlag::operator=(WriteMetadataSingleFlag &&) = default;

WriteMetadataSingleFlag::WriteMetadataSingleFlag(Write &write) : ReadMetadataSingleFlag(write)
{ }

WriteMetadataSingleFlag::backend_map &WriteMetadataSingleFlag::backend()
{
    if (path.empty())
        return static_cast<NodeMetadataFlags *>(top->write(Node::WriteGoal::MetadataFlags))->flags;
    else
        return std::static_pointer_cast<NodeMetadataFlags>(
                top->write(path, Node::WriteGoal::MetadataFlags))->flags;
}

WriteMetadataSingleFlag::mapped_type WriteMetadataSingleFlag::operator[](const key_type &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataSingleFlag, key);
    return result;
}

WriteMetadataSingleFlag::mapped_type WriteMetadataSingleFlag::operator[](key_type &&key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataSingleFlag, std::move(key));
    return result;
}

WriteMetadataSingleFlag::mapped_type WriteMetadataSingleFlag::operator[](const char *key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataSingleFlag, key);
    return result;
}

WriteMetadataSingleFlag::mapped_type WriteMetadataSingleFlag::operator[](const QString &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataSingleFlag, key);
    return result;
}

WriteMetadataSingleFlag::iterator WriteMetadataSingleFlag::begin()
{ return WriteMetadataSingleFlag::iterator(*this, backend().begin()); }

WriteMetadataSingleFlag::iterator WriteMetadataSingleFlag::end()
{ return WriteMetadataSingleFlag::iterator(*this, backend().end()); }

WriteMetadataSingleFlag::iterator WriteMetadataSingleFlag::find(const key_type &key)
{ return WriteMetadataSingleFlag::iterator(*this, backend().find(key)); }

std::pair<WriteMetadataSingleFlag::iterator,
          bool> WriteMetadataSingleFlag::insert(const value_type &value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(value.first, std::move(source));
    return std::pair<WriteMetadataSingleFlag::iterator, bool>(
            WriteMetadataSingleFlag::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataSingleFlag::iterator,
          bool> WriteMetadataSingleFlag::insert(value_type &&value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(std::move(value.first), std::move(source));
    return std::pair<WriteMetadataSingleFlag::iterator, bool>(
            WriteMetadataSingleFlag::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataSingleFlag::iterator,
          bool> WriteMetadataSingleFlag::emplace(const key_type &key, const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(key, std::move(source));
    return std::pair<WriteMetadataSingleFlag::iterator, bool>(
            WriteMetadataSingleFlag::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataSingleFlag::iterator, bool> WriteMetadataSingleFlag::emplace(key_type &&key,
                                                                                    const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(std::move(key), std::move(source));
    return std::pair<WriteMetadataSingleFlag::iterator, bool>(
            WriteMetadataSingleFlag::iterator(*this, std::move(result.first)), result.second);
}

void WriteMetadataSingleFlag::merge(const ReadMetadataSingleFlag &other)
{
    auto &target = backend();
    for (const auto &node : other.backend()) {
        auto add = node.second;
        add->detach(add);
        target.emplace(node.first, std::move(add));
    }
}

WriteMetadataSingleFlag::iterator WriteMetadataSingleFlag::erase(const iterator &pos)
{ return WriteMetadataSingleFlag::iterator(*this, backend().erase(pos.i)); }

WriteMetadataSingleFlag::iterator WriteMetadataSingleFlag::erase(const iterator &first,
                                                                 const iterator &last)
{ return WriteMetadataSingleFlag::iterator(*this, backend().erase(first.i, last.i)); }

ReadMetadataSingleFlag::size_type WriteMetadataSingleFlag::erase(const key_type &key)
{ return backend().erase(key); }

WriteMetadataSingleFlag::iterator::iterator() = default;

WriteMetadataSingleFlag::iterator::~iterator() = default;

WriteMetadataSingleFlag::iterator::iterator(const iterator &) = default;

WriteMetadataSingleFlag::iterator &WriteMetadataSingleFlag::iterator::operator=(const iterator &) = default;

WriteMetadataSingleFlag::iterator::iterator(iterator &&) = default;

WriteMetadataSingleFlag::iterator &WriteMetadataSingleFlag::iterator::operator=(iterator &&) = default;

WriteMetadataSingleFlag::iterator::iterator(WriteMetadataSingleFlag &parent, backing_iterator &&i)
        : parent(&parent), i(std::move(i))
{ }

WriteMetadataSingleFlag::iterator::value_type WriteMetadataSingleFlag::iterator::operator*() const
{ return WriteMetadataSingleFlag::value_type(i->first, (*parent)[i->first]); }

WriteMetadataSingleFlag::iterator::pointer WriteMetadataSingleFlag::iterator::operator->() const
{
    static thread_local std::pair<WriteMetadataSingleFlag::key_type,
                                  WriteMetadataSingleFlag::mapped_type> ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

WriteMetadataSingleFlag::iterator &WriteMetadataSingleFlag::iterator::operator++()
{
    ++i;
    return *this;
}

WriteMetadataSingleFlag::iterator WriteMetadataSingleFlag::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool WriteMetadataSingleFlag::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteMetadataSingleFlag::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteMetadataSingleFlag::iterator::operator==(const ReadMetadataSingleFlag::const_iterator &other) const
{ return i == other.backing(); }

bool WriteMetadataSingleFlag::iterator::operator!=(const ReadMetadataSingleFlag::const_iterator &other) const
{ return i != other.backing(); }

WriteMetadataSingleFlag::iterator::operator ReadMetadataSingleFlag::const_iterator() const
{
    return ReadMetadataSingleFlag::const_iterator(*parent,
                                                  ReadMetadataSingleFlag::const_iterator::backing_iterator(
                                                          i));
}

const WriteMetadataSingleFlag::iterator::backing_iterator &WriteMetadataSingleFlag::iterator::backing() const
{ return i; }

const WriteMetadataSingleFlag::key_type &WriteMetadataSingleFlag::iterator::key() const
{ return i->first; }

WriteMetadataSingleFlag::mapped_type WriteMetadataSingleFlag::iterator::value() const
{ return (*parent)[i->first]; }

PathElement WriteMetadataSingleFlag::iterator::path() const
{ return PathElement(PathElement::Type::MetadataSingleFlag, i->first); }


ReadMetadataHashChild::ReadMetadataHashChild() = default;

ReadMetadataHashChild::~ReadMetadataHashChild() = default;

ReadMetadataHashChild::ReadMetadataHashChild(const ReadMetadataHashChild &) = default;

ReadMetadataHashChild &ReadMetadataHashChild::operator=(const ReadMetadataHashChild &) = default;

ReadMetadataHashChild::ReadMetadataHashChild(ReadMetadataHashChild &&) = default;

ReadMetadataHashChild &ReadMetadataHashChild::operator=(ReadMetadataHashChild &&) = default;

ReadMetadataHashChild::ReadMetadataHashChild(const Read &read) : top(read.top), path(read.path)
{ }

void ReadMetadataHashChild::swap(ReadMetadataHashChild &a, ReadMetadataHashChild &b)
{
    using std::swap;
    swap(a.top, b.top);
    swap(a.path, b.path);
}

const ReadMetadataHashChild::backend_map &ReadMetadataHashChild::backend() const
{
    static const ReadMetadataHashChild::backend_map invalid;
    if (path.empty()) {
        switch (top->node()->type()) {
        case Node::Type::MetadataHash:
        case Node::Type::MetadataHashConstant:
            break;
        default:
            return invalid;
        }
        return top->node<NodeMetadataHash>()->keys;
    }

    auto ptr = top->read(path);
    if (!ptr)
        return invalid;

    switch (ptr->type()) {
    case Node::Type::MetadataHash:
    case Node::Type::MetadataHashConstant:
        break;
    default:
        return invalid;
    }

    /* Safe to keep the pointer as long as the root remains unchanged, since it will
     * have a pointer to it regardless.  Any write access invalidates this,
     * however. */
    return static_cast<const NodeMetadataHash *>(ptr.get())->keys;
}

bool ReadMetadataHashChild::empty() const
{ return backend().empty(); }

ReadMetadataHashChild::size_type ReadMetadataHashChild::size() const
{ return backend().size(); }

ReadMetadataHashChild::size_type ReadMetadataHashChild::count(const key_type &key) const
{ return backend().count(key); }

bool ReadMetadataHashChild::operator==(const ReadMetadataHashChild &other) const
{
    const auto &a = backend();
    const auto &b = other.backend();
    if (a.size() != b.size())
        return false;
    for (const auto &check : a) {
        auto i = b.find(check.first);
        if (i == b.end())
            return false;
        if (!check.second->identicalTo(*i->second))
            return false;
    }
    return true;
}

bool ReadMetadataHashChild::operator!=(const ReadMetadataHashChild &other) const
{ return !(*this == other); }

ReadMetadataHashChild::mapped_type ReadMetadataHashChild::operator[](const key_type &key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataHashChild, key);
    return result;
}

ReadMetadataHashChild::mapped_type ReadMetadataHashChild::operator[](key_type &&key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataHashChild, std::move(key));
    return result;
}

ReadMetadataHashChild::mapped_type ReadMetadataHashChild::operator[](const char *key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataHashChild, key);
    return result;
}

ReadMetadataHashChild::mapped_type ReadMetadataHashChild::operator[](const QString &key) const
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataHashChild, key);
    return result;
}

ReadMetadataHashChild::const_iterator ReadMetadataHashChild::begin() const
{ return ReadMetadataHashChild::const_iterator(*this, backend().begin()); }

ReadMetadataHashChild::const_iterator ReadMetadataHashChild::end() const
{ return ReadMetadataHashChild::const_iterator(*this, backend().end()); }

ReadMetadataHashChild::const_iterator ReadMetadataHashChild::find(const key_type &key) const
{ return ReadMetadataHashChild::const_iterator(*this, backend().find(key)); }

std::unordered_set<ReadMetadataHashChild::key_type> ReadMetadataHashChild::keys() const
{
    std::unordered_set<ReadMetadataHashChild::key_type> result;
    for (const auto &add : backend()) {
        result.insert(add.first);
    }
    return result;
}

ReadMetadataHashChild::const_iterator::const_iterator() = default;

ReadMetadataHashChild::const_iterator::~const_iterator() = default;

ReadMetadataHashChild::const_iterator::const_iterator(const const_iterator &) = default;

ReadMetadataHashChild::const_iterator &ReadMetadataHashChild::const_iterator::operator=(const const_iterator &) = default;

ReadMetadataHashChild::const_iterator::const_iterator(const_iterator &&) = default;

ReadMetadataHashChild::const_iterator &ReadMetadataHashChild::const_iterator::operator=(
        const_iterator &&) = default;

ReadMetadataHashChild::const_iterator::const_iterator(const ReadMetadataHashChild &parent,
                                                      backing_iterator &&i) : parent(&parent),
                                                                              i(std::move(i))
{ }

ReadMetadataHashChild::const_iterator::value_type ReadMetadataHashChild::const_iterator::operator*() const
{ return ReadMetadataHashChild::value_type(i->first, (*parent)[i->first]); }

ReadMetadataHashChild::const_iterator::pointer ReadMetadataHashChild::const_iterator::operator->() const
{
    static thread_local std::pair<ReadMetadataHashChild::key_type,
                                  ReadMetadataHashChild::mapped_type> ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

ReadMetadataHashChild::const_iterator &ReadMetadataHashChild::const_iterator::operator++()
{
    ++i;
    return *this;
}

ReadMetadataHashChild::const_iterator ReadMetadataHashChild::const_iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool ReadMetadataHashChild::const_iterator::operator==(const const_iterator &other) const
{ return i == other.i; }

bool ReadMetadataHashChild::const_iterator::operator!=(const const_iterator &other) const
{ return i != other.i; }

const ReadMetadataHashChild::const_iterator::backing_iterator &ReadMetadataHashChild::const_iterator::backing() const
{ return i; }

const ReadMetadataHashChild::key_type &ReadMetadataHashChild::const_iterator::key() const
{ return i->first; }

ReadMetadataHashChild::mapped_type ReadMetadataHashChild::const_iterator::value() const
{ return (*parent)[i->first]; }

PathElement ReadMetadataHashChild::const_iterator::path() const
{ return PathElement(PathElement::Type::MetadataHashChild, i->first); }


WriteMetadataHashChild::WriteMetadataHashChild() = default;

WriteMetadataHashChild::~WriteMetadataHashChild() = default;

WriteMetadataHashChild::WriteMetadataHashChild(const WriteMetadataHashChild &) = default;

WriteMetadataHashChild &WriteMetadataHashChild::operator=(const WriteMetadataHashChild &) = default;

WriteMetadataHashChild::WriteMetadataHashChild(WriteMetadataHashChild &&) = default;

WriteMetadataHashChild &WriteMetadataHashChild::operator=(WriteMetadataHashChild &&) = default;

WriteMetadataHashChild::WriteMetadataHashChild(Write &write) : ReadMetadataHashChild(write)
{ }

WriteMetadataHashChild::backend_map &WriteMetadataHashChild::backend()
{
    if (path.empty())
        return static_cast<NodeMetadataHash *>(top->write(Node::WriteGoal::MetadataHash))->keys;
    else
        return std::static_pointer_cast<NodeMetadataHash>(
                top->write(path, Node::WriteGoal::MetadataHash))->keys;
}

WriteMetadataHashChild::mapped_type WriteMetadataHashChild::operator[](const key_type &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataHashChild, key);
    return result;
}

WriteMetadataHashChild::mapped_type WriteMetadataHashChild::operator[](key_type &&key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataHashChild, std::move(key));
    return result;
}

WriteMetadataHashChild::mapped_type WriteMetadataHashChild::operator[](const char *key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataHashChild, key);
    return result;
}

WriteMetadataHashChild::mapped_type WriteMetadataHashChild::operator[](const QString &key)
{
    mapped_type result;
    result.top = top;
    result.path = path;
    result.path.emplace_back(PathElement::Type::MetadataHashChild, key);
    return result;
}

WriteMetadataHashChild::iterator WriteMetadataHashChild::begin()
{ return WriteMetadataHashChild::iterator(*this, backend().begin()); }

WriteMetadataHashChild::iterator WriteMetadataHashChild::end()
{ return WriteMetadataHashChild::iterator(*this, backend().end()); }

WriteMetadataHashChild::iterator WriteMetadataHashChild::find(const key_type &key)
{ return WriteMetadataHashChild::iterator(*this, backend().find(key)); }

std::pair<WriteMetadataHashChild::iterator,
          bool> WriteMetadataHashChild::insert(const value_type &value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(value.first, std::move(source));
    return std::pair<WriteMetadataHashChild::iterator, bool>(
            WriteMetadataHashChild::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataHashChild::iterator, bool> WriteMetadataHashChild::insert(value_type &&value)
{
    auto &target = backend();
    auto source = value.second.top->detached(value.second.path);
    auto result = target.emplace(std::move(value.first), std::move(source));
    return std::pair<WriteMetadataHashChild::iterator, bool>(
            WriteMetadataHashChild::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataHashChild::iterator,
          bool> WriteMetadataHashChild::emplace(const key_type &key, const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(key, std::move(source));
    return std::pair<WriteMetadataHashChild::iterator, bool>(
            WriteMetadataHashChild::iterator(*this, std::move(result.first)), result.second);
}

std::pair<WriteMetadataHashChild::iterator, bool> WriteMetadataHashChild::emplace(key_type &&key,
                                                                                  const Read &value)
{
    auto &target = backend();
    auto source = value.top->detached(value.path);
    auto result = target.emplace(std::move(key), std::move(source));
    return std::pair<WriteMetadataHashChild::iterator, bool>(
            WriteMetadataHashChild::iterator(*this, std::move(result.first)), result.second);
}

void WriteMetadataHashChild::merge(const ReadMetadataHashChild &other)
{
    auto &target = backend();
    for (const auto &node : other.backend()) {
        auto add = node.second;
        add->detach(add);
        target.emplace(node.first, std::move(add));
    }
}

WriteMetadataHashChild::iterator WriteMetadataHashChild::erase(const iterator &pos)
{ return WriteMetadataHashChild::iterator(*this, backend().erase(pos.i)); }

WriteMetadataHashChild::iterator WriteMetadataHashChild::erase(const iterator &first,
                                                               const iterator &last)
{ return WriteMetadataHashChild::iterator(*this, backend().erase(first.i, last.i)); }

ReadMetadataHashChild::size_type WriteMetadataHashChild::erase(const key_type &key)
{ return backend().erase(key); }

WriteMetadataHashChild::iterator::iterator() = default;

WriteMetadataHashChild::iterator::~iterator() = default;

WriteMetadataHashChild::iterator::iterator(const iterator &) = default;

WriteMetadataHashChild::iterator &WriteMetadataHashChild::iterator::operator=(const iterator &) = default;

WriteMetadataHashChild::iterator::iterator(iterator &&) = default;

WriteMetadataHashChild::iterator &WriteMetadataHashChild::iterator::operator=(iterator &&) = default;

WriteMetadataHashChild::iterator::iterator(WriteMetadataHashChild &parent, backing_iterator &&i)
        : parent(&parent), i(std::move(i))
{ }

WriteMetadataHashChild::iterator::value_type WriteMetadataHashChild::iterator::operator*() const
{ return WriteMetadataHashChild::value_type(i->first, (*parent)[i->first]); }

WriteMetadataHashChild::iterator::pointer WriteMetadataHashChild::iterator::operator->() const
{
    static thread_local std::pair<WriteMetadataHashChild::key_type,
                                  WriteMetadataHashChild::mapped_type> ref;
    ref.first = i->first;
    ref.second = (*parent)[i->first];
    return &ref;
}

WriteMetadataHashChild::iterator &WriteMetadataHashChild::iterator::operator++()
{
    ++i;
    return *this;
}

WriteMetadataHashChild::iterator WriteMetadataHashChild::iterator::operator++(int)
{
    auto save = *this;
    ++i;
    return save;
}

bool WriteMetadataHashChild::iterator::operator==(const iterator &other) const
{ return i == other.i; }

bool WriteMetadataHashChild::iterator::operator!=(const iterator &other) const
{ return i != other.i; }

bool WriteMetadataHashChild::iterator::operator==(const ReadMetadataHashChild::const_iterator &other) const
{ return i == other.backing(); }

bool WriteMetadataHashChild::iterator::operator!=(const ReadMetadataHashChild::const_iterator &other) const
{ return i != other.backing(); }

WriteMetadataHashChild::iterator::operator ReadMetadataHashChild::const_iterator() const
{
    return ReadMetadataHashChild::const_iterator(*parent,
                                                 ReadMetadataHashChild::const_iterator::backing_iterator(
                                                         i));
}

const WriteMetadataHashChild::iterator::backing_iterator &WriteMetadataHashChild::iterator::backing() const
{ return i; }

const WriteMetadataHashChild::key_type &WriteMetadataHashChild::iterator::key() const
{ return i->first; }

WriteMetadataHashChild::mapped_type WriteMetadataHashChild::iterator::value() const
{ return (*parent)[i->first]; }

PathElement WriteMetadataHashChild::iterator::path() const
{ return PathElement(PathElement::Type::MetadataHashChild, i->first); }


ReadGeneric::ReadGeneric() = default;

ReadGeneric::~ReadGeneric() = default;

class ReadGeneric::Backend {
public:
    Backend() = default;

    virtual ~Backend() = default;

    virtual std::unique_ptr<Backend> clone() const = 0;

    virtual value_type value() const = 0;

    virtual void next() = 0;

    virtual bool equalTo(const Backend &base) const = 0;

    virtual PathElement key() const = 0;

    virtual std::string stringKey() const
    { return std::string(); }

    virtual std::int_fast64_t integerKey() const
    { return INTEGER::undefined(); }

    virtual PathElement::HashIndex hashIndex() const
    { return PathElement::HashIndex(); }

    virtual PathElement::ArrayIndex arrayIndex() const
    { return 0; }

    virtual PathElement::MatrixIndex matrixIndex() const
    { return PathElement::MatrixIndex(); }

    virtual PathElement::KeyframeIndex keyframeIndex() const
    { return FP::undefined(); }

    virtual PathElement::MetadataIndex metadataIndex() const
    { return PathElement::MetadataIndex(); }

    virtual PathElement::MetadataSingleFlagIndex metadataSingleFlagIndex() const
    { return PathElement::MetadataSingleFlagIndex(); }

    virtual PathElement::MetadataHashChildIndex metadataHashChildIndex() const
    { return PathElement::MetadataHashChildIndex(); }
};

class ReadGeneric::Converter {
public:
    Converter() = default;

    virtual ~Converter() = default;

    virtual std::unique_ptr<Backend> begin() const
    { return std::unique_ptr<Backend>(); }

    virtual std::unique_ptr<Backend> end() const
    { return std::unique_ptr<Backend>(); }

    virtual std::unordered_set<std::string> keys() const
    {
        std::unordered_set<std::string> result;
        for (auto c = begin(), e = end(); !c->equalTo(*e); c->next()) {
            auto s = c->stringKey();
            if (s.empty())
                continue;
            result.insert(std::move(s));
        }
        return result;
    }

    virtual std::unique_ptr<Converter> clone() const = 0;
};

ReadGeneric::ReadGeneric(const ReadGeneric &other)
{
    if (other.converter)
        converter = other.converter->clone();
}

ReadGeneric &ReadGeneric::operator=(const ReadGeneric &other)
{
    if (other.converter)
        converter = other.converter->clone();
    else
        converter.reset();
    return *this;
}

ReadGeneric::ReadGeneric(ReadGeneric &&) = default;

ReadGeneric &ReadGeneric::operator=(ReadGeneric &&) = default;

ReadGeneric::ReadGeneric(const Read &read) : ReadGeneric(read, Control())
{ }

ReadGeneric::Control::Control() : sortStrings(false), includeDefaultKey(false)
{ }

template<typename MapType>
class ReadGeneric::Backend_Map : public ReadGeneric::Backend {
public:
    using iterator = typename MapType::const_iterator;
protected:
    iterator i;
public:
    Backend_Map(const MapType &, iterator &&i) : i(std::move(i))
    { }

    virtual ~Backend_Map() = default;

    std::unique_ptr<Backend> clone() const override
    { return std::unique_ptr<Backend>(new Backend_Map<MapType>(*this)); }

    value_type value() const override
    { return i.value(); }

    void next() override
    { ++i; }

    bool equalTo(const Backend &base) const override
    { return i == static_cast<const Backend_Map<MapType> &>(base).i; }

    PathElement key() const override
    { return i.value().currentPath().back(); }

protected:
    Backend_Map(const Backend_Map <MapType> &other) = default;
};

template<typename MapType>
class ReadGeneric::Backend_SortedMap : public ReadGeneric::Backend {
public:
    using iterator = typename std::vector<typename MapType::const_iterator>::const_iterator;
protected:
    iterator i;
public:
    Backend_SortedMap(const MapType &, iterator &&i) : i(std::move(i))
    { }

    virtual ~Backend_SortedMap() = default;

    std::unique_ptr<Backend> clone() const override
    { return std::unique_ptr<Backend>(new Backend_SortedMap<MapType>(*this)); }

    value_type value() const override
    { return (*i).value(); }

    void next() override
    { ++i; }

    bool equalTo(const Backend &base) const override
    { return i == static_cast<const Backend_SortedMap<MapType> &>(base).i; }

    PathElement key() const override
    { return (*i).value().currentPath().back(); }

protected:
    Backend_SortedMap(const Backend_SortedMap <MapType> &other) = default;
};

template<typename WrapType, typename BackendType>
class ReadGeneric::Converter_Wrap : public ReadGeneric::Converter {
    WrapType container;
public:
    Converter_Wrap(WrapType &&container) : container(std::move(container))
    { }

    virtual ~Converter_Wrap() = default;

    std::unique_ptr<Backend> begin() const override
    { return std::unique_ptr<Backend>(new BackendType(container, container.begin())); }

    std::unique_ptr<Backend> end() const override
    { return std::unique_ptr<Backend>(new BackendType(container, container.end())); }

    std::unique_ptr<Converter> clone() const override
    { return std::unique_ptr<Converter>(new Converter_Wrap<WrapType, BackendType>(*this)); }

protected:
    Converter_Wrap(const Converter_Wrap <WrapType, BackendType> &other) = default;
};

template<typename WrapType, typename BackendType>
class ReadGeneric::Converter_SortedWrap : public ReadGeneric::Converter {
    using sort_type = std::vector<typename WrapType::const_iterator>;

    WrapType container;
    sort_type sorted;
public:
    Converter_SortedWrap(WrapType &&container) : container(std::move(container))
    {
        for (auto add = this->container.begin(), endAdd = this->container.end();
                add != endAdd;
                ++add) {
            sorted.emplace_back(add);
        }
    }

    template<typename Functor>
    void eraseFirst(Functor f)
    {
        for (auto c = sorted.begin(), endC = sorted.end(); c != endC; ++c) {
            if (f(*c)) {
                sorted.erase(c);
                break;
            }
        }
    }

    template<typename Compare>
    void sort(Compare c)
    { std::sort(sorted.begin(), sorted.end(), c); }

    virtual ~Converter_SortedWrap() = default;

    std::unique_ptr<Backend> begin() const override
    { return std::unique_ptr<Backend>(new BackendType(container, sorted.begin())); }

    std::unique_ptr<Backend> end() const override
    { return std::unique_ptr<Backend>(new BackendType(container, sorted.end())); }

    std::unique_ptr<Converter> clone() const override
    { return std::unique_ptr<Converter>(new Converter_SortedWrap<WrapType, BackendType>(*this)); }

protected:
    Converter_SortedWrap(const Converter_SortedWrap <WrapType, BackendType> &other) = default;
};

template<typename WrapType, typename BackendType>
class ReadGeneric::Converter_StringKeyWrap : public ReadGeneric::Converter_Wrap<WrapType,
                                                                                BackendType> {
public:
    Converter_StringKeyWrap(WrapType &&container) : Converter_Wrap<WrapType, BackendType>(
            std::move(container))
    { }

    virtual ~Converter_StringKeyWrap() = default;

    std::unique_ptr<Converter> clone() const override
    {
        return std::unique_ptr<Converter>(
                new Converter_StringKeyWrap<WrapType, BackendType>(*this));
    }

    std::unordered_set<std::string> keys() const override
    {
        std::unordered_set<std::string> result;
        for (auto c = this->begin(), e = this->end(); !c->equalTo(*e); c->next()) {
            auto v = c->value();
            switch (v.getType()) {
            case Type::Empty:
                continue;
            case Type::Boolean:
                if (!v.toBool())
                    continue;
                break;
            default:
                break;
            }

            auto s = c->stringKey();
            if (s.empty())
                continue;
            result.insert(std::move(s));
        }
        return result;
    }

protected:
    Converter_StringKeyWrap(const Converter_StringKeyWrap <WrapType, BackendType> &other) = default;
};

ReadGeneric::ReadGeneric(const Read &read, const Control &control)
{
    class ConvertStatic : public Converter {
        std::unordered_set<std::string> staticKeys;

        ConvertStatic(const ConvertStatic &other) = default;

    public:
        explicit ConvertStatic(const std::unordered_set<std::string> &staticKeys) : staticKeys(
                staticKeys)
        {
            this->staticKeys.erase(std::string());
        }

        explicit ConvertStatic(std::unordered_set<std::string> &&staticKeys) : staticKeys(
                std::move(staticKeys))
        {
            this->staticKeys.erase(std::string());
        }

        virtual ~ConvertStatic() = default;

        std::unordered_set<std::string> keys() const override
        { return staticKeys; }

        std::unique_ptr<Converter> clone() const override
        { return std::unique_ptr<Converter>(new ConvertStatic(*this)); }
    };

    switch (read.getType()) {
    case Type::Empty:
    case Type::Boolean:
    case Type::Bytes:
    case Type::Overlay:
    case Type::Real:
    case Type::Integer:
        break;
    case Type::String:
        converter.reset(new ConvertStatic({read.toString()}));
        break;
    case Type::Flags:
        converter.reset(new ConvertStatic(read.toFlags()));
        break;
    case Type::Hash:
        if (control.sortStrings) {
            class SortedHashBackend : public Backend_SortedMap<ReadHash> {
            public:
                SortedHashBackend(const ReadHash &container, iterator &&i) : Backend_SortedMap<
                        ReadHash>(container, std::move(i))
                { }

                virtual ~SortedHashBackend() = default;

                std::string stringKey() const override
                { return i->key(); }

                PathElement::HashIndex hashIndex() const override
                { return i->key(); }
            };

            typedef Converter_SortedWrap<ReadHash, SortedHashBackend> TypeConverter;
            converter.reset(new TypeConverter(read.toHash()));
            if (!control.includeDefaultKey) {
                static_cast<TypeConverter *>(converter.get())->eraseFirst(
                        [](const ReadHash::const_iterator &i) {
                            return i.key().empty();
                        });
            }
            static_cast<TypeConverter *>(converter.get())->sort(
                    [](const ReadHash::const_iterator &a, const ReadHash::const_iterator &b) {
                        return a.key() < b.key();
                    });
        } else {
            class HashBackend : public Backend_Map<ReadHash> {
            public:
                HashBackend(const ReadHash &container, iterator &&i) : Backend_Map<ReadHash>(
                        container, std::move(i))
                { }

                virtual ~HashBackend() = default;

                std::string stringKey() const override
                { return i.key(); }

                PathElement::HashIndex hashIndex() const override
                { return i.key(); }
            };
            class HashBackendNoDefault : public HashBackend {
                iterator end;

                bool accept() const
                {
                    if (i.key().empty())
                        return false;
                    return true;
                }

            public:
                HashBackendNoDefault(const ReadHash &container, iterator &&i) : HashBackend(
                        container, std::move(i))
                {
                    end = container.end();
                    for (; this->i != end && !accept(); ++(this->i)) { }
                }

                virtual ~HashBackendNoDefault() = default;

                void next() override
                {
                    for (++i; i != end && !accept(); ++i) { }
                }
            };

            if (control.includeDefaultKey) {
                converter.reset(new Converter_StringKeyWrap<ReadHash, HashBackend>(read.toHash()));
            } else {
                converter.reset(
                        new Converter_StringKeyWrap<ReadHash, HashBackendNoDefault>(read.toHash()));
            }
        }
        break;
    case Type::Array: {
        class Backend_Array : public ReadGeneric::Backend {
        public:
            using iterator = typename ReadArray::const_iterator;
        protected:
            iterator i;
        public:
            Backend_Array(const ReadArray &, iterator &&i) : i(std::move(i))
            { }

            virtual ~Backend_Array() = default;

            std::unique_ptr<Backend> clone() const override
            { return std::unique_ptr<Backend>(new Backend_Array(*this)); }

            value_type value() const override
            { return *i; }

            void next() override
            { ++i; }

            bool equalTo(const Backend &base) const override
            { return i == static_cast<const Backend_Array &>(base).i; }

            PathElement key() const override
            { return PathElement(PathElement::Type::Array, i.backing()); }

            std::string stringKey() const override
            { return (*i).toString(); }

            std::int_fast64_t integerKey() const override
            { return i.backing(); }

            PathElement::ArrayIndex arrayIndex() const override
            { return i.backing(); }

        protected:
            Backend_Array(const Backend_Array &other) = default;
        };
        converter.reset(new Converter_Wrap<ReadArray, Backend_Array>(read.toArray()));
        break;
    }
    case Type::Matrix: {
        class MatrixBackend : public Backend_Map<ReadMatrix> {
        public:
            MatrixBackend(const ReadMatrix &container, iterator &&i) : Backend_Map<ReadMatrix>(
                    container, std::move(i))
            { }

            virtual ~MatrixBackend() = default;

            std::string stringKey() const override
            { return i.value().toString(); }

            std::int_fast64_t integerKey() const override
            { return i.backing(); }

            PathElement::MatrixIndex matrixIndex() const override
            { return i.key(); }
        };
        converter.reset(new Converter_Wrap<ReadMatrix, MatrixBackend>(read.toMatrix()));
        break;
    }
    case Type::Keyframe: {
        class KeyframeBackend : public Backend_Map<ReadKeyframe> {
        public:
            KeyframeBackend(const ReadKeyframe &container, iterator &&i) : Backend_Map<
                    ReadKeyframe>(container, std::move(i))
            { }

            virtual ~KeyframeBackend() = default;

            std::string stringKey() const override
            { return i.value().toString(); }

            std::int_fast64_t integerKey() const override
            { return static_cast<std::int_fast64_t>(std::round(i.key())); }

            PathElement::KeyframeIndex keyframeIndex() const override
            { return i.key(); }
        };
        converter.reset(new Converter_Wrap<ReadKeyframe, KeyframeBackend>(read.toKeyframe()));
        break;
    }
    case Type::MetadataReal:
    case Type::MetadataInteger:
    case Type::MetadataBoolean:
    case Type::MetadataString:
    case Type::MetadataBytes:
    case Type::MetadataArray:
    case Type::MetadataMatrix:
    case Type::MetadataKeyframe:
    case Type::MetadataFlags:
    case Type::MetadataHash:
        if (control.sortStrings) {
            class SortedMetadataBackend : public Backend_SortedMap<ReadMetadata> {
            public:
                SortedMetadataBackend(const ReadMetadata &container, iterator &&i)
                        : Backend_SortedMap<ReadMetadata>(container, std::move(i))
                { }

                virtual ~SortedMetadataBackend() = default;

                std::string stringKey() const override
                { return i->key(); }

                PathElement::MetadataIndex metadataIndex() const override
                { return i->key(); }
            };

            typedef Converter_SortedWrap<ReadMetadata, SortedMetadataBackend> TypeConverter;
            converter.reset(new TypeConverter(read.toMetadata()));
            if (!control.includeDefaultKey) {
                static_cast<TypeConverter *>(converter.get())->eraseFirst(
                        [](const ReadMetadata::const_iterator &i) {
                            return i.key().empty();
                        });
            }
            static_cast<TypeConverter *>(converter.get())->sort(
                    [](const ReadMetadata::const_iterator &a,
                       const ReadMetadata::const_iterator &b) {
                        return a.key() < b.key();
                    });
        } else {
            class MetadataBackend : public Backend_Map<ReadMetadata> {
            public:
                MetadataBackend(const ReadMetadata &container, iterator &&i) : Backend_Map<
                        ReadMetadata>(container, std::move(i))
                { }

                virtual ~MetadataBackend() = default;

                std::string stringKey() const override
                { return i.key(); }

                PathElement::MetadataIndex metadataIndex() const override
                { return i.key(); }
            };
            class MetadataBackendNoDefault : public MetadataBackend {
                iterator end;
            public:
                MetadataBackendNoDefault(const ReadMetadata &container, iterator &&i)
                        : MetadataBackend(container, std::move(i))
                {
                    end = container.end();
                    if (this->i == end)
                        return;
                    if (this->i.key().empty())
                        ++(this->i);
                }

                virtual ~MetadataBackendNoDefault() = default;

                void next() override
                {
                    ++i;
                    if (i != end && i.key().empty())
                        ++i;
                }
            };
            if (control.includeDefaultKey) {
                converter.reset(
                        new Converter_Wrap<ReadMetadata, MetadataBackend>(read.toMetadata()));
            } else {
                converter.reset(new Converter_Wrap<ReadMetadata, MetadataBackendNoDefault>(
                        read.toMetadata()));
            }
        }
        break;
    }
}

void ReadGeneric::swap(ReadGeneric &a, ReadGeneric &b)
{
    using std::swap;
    swap(a.converter, b.converter);
}

ReadGeneric::const_iterator ReadGeneric::begin() const
{
    if (!converter)
        return const_iterator();
    return const_iterator(converter->begin());
}

ReadGeneric::const_iterator ReadGeneric::end() const
{
    if (!converter)
        return const_iterator();
    return const_iterator(converter->end());
}

std::unordered_set<std::string> ReadGeneric::keys() const
{
    if (!converter)
        return std::unordered_set<std::string>();
    return converter->keys();
}

ReadGeneric::value_type ReadGeneric::fetch(const std::shared_ptr<TopLevel> &top, Path path)
{
    value_type result;
    result.top = top;
    result.path = std::move(path);
    return result;
}

ReadGeneric::const_iterator::const_iterator(std::unique_ptr<Backend> &&backend) : backend(
        std::move(backend))
{ }

ReadGeneric::const_iterator::const_iterator() = default;

ReadGeneric::const_iterator::~const_iterator() = default;

ReadGeneric::const_iterator::const_iterator(const const_iterator &other)
{
    if (other.backend)
        backend = other.backend->clone();
}

ReadGeneric::const_iterator &ReadGeneric::const_iterator::operator=(const const_iterator &other)
{
    if (other.backend)
        backend = other.backend->clone();
    else
        backend.reset();
    return *this;
}

ReadGeneric::const_iterator::const_iterator(const_iterator &&) = default;

ReadGeneric::const_iterator &ReadGeneric::const_iterator::operator=(const_iterator &&) = default;

ReadGeneric::const_iterator::value_type ReadGeneric::const_iterator::operator*() const
{ return backend->value(); }

ReadGeneric::const_iterator::pointer ReadGeneric::const_iterator::operator->() const
{
    static thread_local value_type ref;
    ref = backend->value();
    return &ref;
}

ReadGeneric::const_iterator &ReadGeneric::const_iterator::operator++()
{
    backend->next();
    return *this;
}

ReadGeneric::const_iterator ReadGeneric::const_iterator::operator++(int)
{
    auto save = *this;
    backend->next();
    return save;
}

bool ReadGeneric::const_iterator::operator==(const const_iterator &other) const
{
    if (!backend)
        return !other.backend;
    if (!other.backend)
        return false;
    return backend->equalTo(*other.backend);
}

bool ReadGeneric::const_iterator::operator!=(const const_iterator &other) const
{ return !(*this == other); }

ReadGeneric::const_iterator::value_type ReadGeneric::const_iterator::value() const
{ return backend->value(); }

PathElement ReadGeneric::const_iterator::key() const
{ return backend->key(); }

std::string ReadGeneric::const_iterator::stringKey() const
{ return backend->stringKey(); }

std::int_fast64_t ReadGeneric::const_iterator::integerKey() const
{ return backend->integerKey(); }

PathElement::HashIndex ReadGeneric::const_iterator::hashIndex() const
{ return backend->hashIndex(); }

PathElement::ArrayIndex ReadGeneric::const_iterator::arrayIndex() const
{ return backend->arrayIndex(); }

PathElement::MatrixIndex ReadGeneric::const_iterator::matrixIndex() const
{ return backend->matrixIndex(); }

PathElement::KeyframeIndex ReadGeneric::const_iterator::keyframeIndex() const
{ return backend->keyframeIndex(); }

PathElement::MetadataIndex ReadGeneric::const_iterator::metadataIndex() const
{ return backend->metadataIndex(); }

PathElement::MetadataSingleFlagIndex ReadGeneric::const_iterator::metadataSingleFlagIndex() const
{ return backend->metadataSingleFlagIndex(); }

PathElement::MetadataHashChildIndex ReadGeneric::const_iterator::metadataHashChildIndex() const
{ return backend->metadataHashChildIndex(); }


}
}
}
}