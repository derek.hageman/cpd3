/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <cctype>

#include <QVariant>
#include <QMetaType>
#include <QUrl>
#include <QRegExp>

#include "composite.hxx"
#include "core/util.hxx"


namespace CPD3 {
namespace Data {
namespace Variant {
namespace Composite {

QString applyFormat(const Read &value, const QStringList &elements)
{
    int index = 0;
    QString type;
    if (index < elements.size())
        type = elements.at(index++).toLower();

    QString path;
    if (index < elements.size())
        path = elements.at(index++);

    if (type == "string" || type == "str") {
        auto v = value.getPath(path);
        return TextSubstitution::formatString(v.toDisplayString(), elements.mid(index),
                                              v.getType() == Type::String);
    } else if (type == "real" || type == "double" || type == "number") {
        return TextSubstitution::formatDouble(value.getPath(path).toDouble(), elements.mid(index));
    } else if (type == "integer" || type == "int") {
        return TextSubstitution::formatInteger(value.getPath(path).toInt64(), elements.mid(index));
    } else if (type == "time") {
        return TextSubstitution::formatTime(value.getPath(path).toReal(), elements.mid(index));
    } else if (type == "duration" || type == "interval") {
        return TextSubstitution::formatDuration(value.getPath(path).toReal(), elements.mid(index));
    } else if (type == "exists") {
        QString def;
        if (index < elements.size())
            def = elements.at(index++);
        QString undef;
        if (index < elements.size())
            undef = elements.at(index++);
        if (!value.getPath(path).exists()) {
            return TextSubstitution::formatString(undef, elements.mid(index));
        } else {
            return TextSubstitution::formatString(def, elements.mid(index));
        }
    } else if (type == "ifdef" || type == "defined") {
        QString def;
        if (index < elements.size())
            def = elements.at(index++);
        QString undef;
        if (index < elements.size())
            undef = elements.at(index++);
        if (!isDefined(value.getPath(path))) {
            return TextSubstitution::formatString(undef, elements.mid(index));
        } else {
            return TextSubstitution::formatString(def, elements.mid(index));
        }
    } else if (type == "flags") {
        QString join(',');
        if (index < elements.size())
            join = elements.at(index++);

        QStringList sorted;
        for (const auto &add : value.getPath(path).toFlags()) {
            sorted.push_back(QString::fromStdString(add));
        }
        std::sort(sorted.begin(), sorted.end());
        return TextSubstitution::formatString(sorted.join(join), elements.mid(index));
    } else if (type == "join") {
        QString join(',');
        if (index < elements.size())
            join = elements.at(index++);

        QStringList flags;
        if (index < elements.size())
            flags = elements.at(index++).toLower().split(QRegExp("[,;:]+"));
        bool skipUndefined = true;
        if (flags.contains("undefined"))
            skipUndefined = false;

        Container::ReadGeneric::Control ctl;
        ctl.sortStrings = true;
        QStringList parts;
        Container::ReadGeneric children(value.getPath(path), ctl);
        if (children.begin() != children.end()) {
            for (auto c : children) {
                if (skipUndefined && !c.exists())
                    continue;
                parts.append(applyFormat(c, elements.mid(index)));
            }
        } else {
            for (const auto &add : children.keys()) {
                parts.append(TextSubstitution::formatString(QString::fromStdString(add),
                                                            elements.mid(index)));
            }
        }
        return parts.join(join);
    } else if (type == "lookup") {
        QString join(',');
        if (index < elements.size())
            join = elements.at(index++);

        QStringList paths;
        if (index < elements.size())
            paths = elements.at(index++).split(QRegExp("[,;:]+"));

        QStringList flags;
        if (index < elements.size())
            flags = elements.at(index++).toLower().split(QRegExp("[,;:]+"));
        bool skipUndefined = true;
        if (flags.contains("undefined"))
            skipUndefined = false;

        auto v = value.getPath(path);
        QStringList parts;
        for (const auto &a : paths) {
            auto av = v.getPath(a);
            if (skipUndefined && !av.exists())
                continue;
            parts.append(applyFormat(av, elements.mid(index)));
        }
        return parts.join(join);
    }

    QString mvc;
    if (index < elements.size())
        mvc = elements.at(index++);

    auto v = value.getPath(path);
    if (!v.exists())
        return mvc;

    switch (v.getType()) {
    case Type::Real: {
        auto check = v.toReal();
        if (!FP::defined(check))
            return mvc;
        return QString::number(v.toDouble());
    }
    case Type::Integer: {
        auto check = v.toInteger();
        if (!FP::defined(check))
            return mvc;
        return QString::number(check);
    }
    case Type::Flags: {
        QStringList sorted;
        for (const auto &add : v.toFlags()) {
            sorted.push_back(QString::fromStdString(add));
        }
        std::sort(sorted.begin(), sorted.end());
        return sorted.join(",");
    }
    default:
        return QString::fromStdString(v.toString());
    }

    return mvc;
}

SubstitutionStack::SubstitutionStack() = default;

SubstitutionStack::~SubstitutionStack() = default;

SubstitutionStack::SubstitutionStack(const SubstitutionStack &other) = default;

SubstitutionStack &SubstitutionStack::operator=(const SubstitutionStack &other) = default;

SubstitutionStack::SubstitutionStack(SubstitutionStack &&other) = default;

SubstitutionStack &SubstitutionStack::operator=(SubstitutionStack &&other) = default;

void SubstitutionStack::setVariant(const QString &key, const Read &value)
{
    class VariantReplacer : public TextSubstitutionStack::Replacer {
        Read value;
    public:
        VariantReplacer(const Read &v) : value(v)
        { }

        virtual ~VariantReplacer() = default;

        virtual QString get(const QStringList &elements) const
        { return applyFormat(value, elements); }
    };

    setReplacement(key, new VariantReplacer(value));
}


static Time::LogicalTimeUnit toUnit(const std::string &unit, bool *ok = nullptr)
{
    if (Util::equal_insensitive(unit, "millisecond", "milliseconds", "msec"))
        return Time::Millisecond;
    if (Util::equal_insensitive(unit, "second", "seconds", "sec", "secs", "s"))
        return Time::Second;
    if (Util::equal_insensitive(unit, "minute", "minutes", "min", "mins", "m", "mi"))
        return Time::Minute;
    if (Util::equal_insensitive(unit, "hour", "hours", "h"))
        return Time::Hour;
    if (Util::equal_insensitive(unit, "day", "days", "d"))
        return Time::Day;
    if (Util::equal_insensitive(unit, "week", "weeks", "w"))
        return Time::Week;
    if (Util::equal_insensitive(unit, "month", "months", "mon", "mons", "mo"))
        return Time::Month;
    if (Util::equal_insensitive(unit, "quarter", "quarters", "qtr", "qtrs", "q"))
        return Time::Quarter;
    if (Util::equal_insensitive(unit, "year", "years", "y"))
        return Time::Year;
    if (ok) {
        *ok = false;
    }
    return Time::Second;
}

bool toTimeInterval(const Read &value, Time::LogicalTimeUnit *unit, int *count, bool *align)
{
    switch (value.getType()) {
    case Type::Integer:
        if (count) {
            auto v = value.toInteger();
            if (INTEGER::defined(v))
                *count = static_cast<int>(v);
            else
                return false;
        }
        return true;
    case Type::Real:
        if (count) {
            auto v = value.toReal();
            if (FP::defined(v))
                *count = static_cast<int>(std::round(v));
            else
                return false;
        }
        return true;
    case Type::String:
        if (unit) {
            bool ok = true;
            *unit = toUnit(value.toString(), &ok);
            return ok;
        }
        return true;
    default:
        break;
    }

    bool ok = true;

    if (count) {
        auto check = value["Count"];
        if (!check.exists())
            check = value["Number"];
        if (!check.exists())
            check = value["Offset"];
        if (!check.exists())
            check = value["N"];
        if (check.getType() == Type::Real) {
            auto v = check.toReal();
            if (FP::defined(v))
                *count = static_cast<int>(std::round(v));
            else
                ok = false;
        } else {
            auto v = check.toInteger();
            if (INTEGER::defined(v))
                *count = static_cast<int>(v);
            else
                ok = false;
        }
    }

    if (align) {
        if (value["Align"].exists()) {
            *align = value["Align"].toBool();
            if (value["Align"].getType() != Variant::Type::Boolean)
                ok = false;
        } else if (value["Aligned"].exists()) {
            *align = value["Aligned"].toBool();
            if (value["Aligned"].getType() != Variant::Type::Boolean)
                ok = false;
        }
    }

    if (unit) {
        const auto &u = value["Units"].toString();
        if (!u.empty()) {
            *unit = toUnit(u, &ok);
        } else {
            const auto &u2 = value["Unit"].toString();
            if (!u2.empty()) {
                *unit = toUnit(u2, &ok);
            }
        }
    }

    return ok;
}

Time::LogicalTimeUnit toTimeInterval(const Read &value, int *count, bool *align)
{
    if (count)
        *count = 1;
    if (align)
        *align = false;
    Time::LogicalTimeUnit unit = Time::Second;
    toTimeInterval(value, &unit, count, align);
    return unit;
}

double offsetTimeInterval(const Read &value,
                          double origin,
                          bool forward,
                          Time::LogicalTimeUnit defaultUnit,
                          int defaultCount,
                          bool defaultAlign)
{
    if (value.exists()) {
        defaultUnit = toTimeInterval(value, &defaultCount, &defaultAlign);
    }
    if (!forward)
        defaultCount = -defaultCount;
    return Time::logical(origin, defaultUnit, defaultCount, defaultAlign, forward);
}

static void fromUnit(Write &&target, Time::LogicalTimeUnit unit)
{
    switch (unit) {
    case Time::Millisecond:
        target.setString("Milliseconds");
        break;
    case Time::Second:
        target.setString("Seconds");
        break;
    case Time::Minute:
        target.setString("Minutes");
        break;
    case Time::Hour:
        target.setString("Hours");
        break;
    case Time::Day:
        target.setString("Days");
        break;
    case Time::Week:
        target.setString("Weeks");
        break;
    case Time::Month:
        target.setString("Months");
        break;
    case Time::Quarter:
        target.setString("Quarters");
        break;
    case Time::Year:
        target.setString("Years");
        break;
    }
}

void fromTimeInterval(Write &value, Time::LogicalTimeUnit unit)
{
    fromUnit(value.hash("Units"), unit);
}

void fromTimeInterval(Write &&value, Time::LogicalTimeUnit unit)
{ return fromTimeInterval(value, unit); }

void fromTimeInterval(Write &value, Time::LogicalTimeUnit unit, int count)
{
    value.hash("Count").setInteger(count);
    fromTimeInterval(value, unit);
}

void fromTimeInterval(Write &&value, Time::LogicalTimeUnit unit, int count)
{ return fromTimeInterval(value, unit, count); }

void fromTimeInterval(Write &value, Time::LogicalTimeUnit unit, int count, bool align)
{
    value.hash("Align").setBoolean(align);
    fromTimeInterval(value, unit, count);
}

void fromTimeInterval(Write &&value, Time::LogicalTimeUnit unit, int count, bool align)
{ return fromTimeInterval(value, unit, count, align); }


Calibration toCalibration(const Read &value)
{
    if (!value.exists())
        return Calibration();

    switch (value.getType()) {
    case Type::Real: {
        Calibration cal;
        cal.clear();
        cal.set(0, value.toReal());
        return cal;
    }
    case Type::Hash: {
        Calibration cal;
        cal.clear();
        for (auto add : value.hash("Coefficients").toChildren()) {
            cal.append(add.toReal());
        }
        const auto &root = value.hash("Root").toString();
        if (Util::equal_insensitive(root, "preferpositive")) {
            cal.setDefaultRootChoice(Calibration::PreferPositive);
        } else if (Util::equal_insensitive(root, "prefernegative")) {
            cal.setDefaultRootChoice(Calibration::PreferNegative);
        } else if (Util::equal_insensitive(root, "positive")) {
            cal.setDefaultRootChoice(Calibration::AlwaysPositive);
        } else if (Util::equal_insensitive(root, "negative")) {
            cal.setDefaultRootChoice(Calibration::AlwaysNegative);
        } else if (Util::equal_insensitive(root, "best", "bestinrange")) {
            cal.setDefaultRootChoice(Calibration::BestInRange);
        } else if (Util::equal_insensitive(root, "preferthird", "preferx1", "preferthirdx1")) {
            cal.setDefaultRootChoice(Calibration::PreferThirdX1);
        } else if (Util::equal_insensitive(root, "third", "x1", "thirdx1")) {
            cal.setDefaultRootChoice(Calibration::AlwaysThirdX1);
        }
        return cal;
    }
    default:
        break;
    }

    Calibration cal;
    cal.clear();
    for (auto add : value.toChildren()) {
        cal.append(add.toReal());
    }
    return cal;
}

void fromCalibration(Write &value, const Calibration &calibration, bool includeRoot)
{
    auto size = calibration.size();
    value.remove();
    if (size < 3 || !includeRoot) {
        auto out = value.toArray();
        for (auto i = 0; i < size; ++i) {
            out[i].setReal(calibration.get(i));
        }
        return;
    }

    {
        auto out = value.hash("Coefficients").toArray();
        for (auto i = 0; i < size; ++i) {
            out[i].setReal(calibration.get(i));
        }
    }

    switch (calibration.getDefaultRootChoice()) {
    case Calibration::PreferPositive:
        value.hash("Root").setString("PreferPositive");
        break;
    case Calibration::PreferNegative:
        value.hash("Root").setString("PreferNegative");
        break;
    case Calibration::AlwaysPositive:
        value.hash("Root").setString("Positive");
        break;
    case Calibration::AlwaysNegative:
        value.hash("Root").setString("Negative");
        break;
    case Calibration::BestInRange:
        value.hash("Root").setString("Best");
        break;
    case Calibration::PreferThirdX1:
        value.hash("Root").setString("PreferThird");
        break;
    case Calibration::AlwaysThirdX1:
        value.hash("Root").setString("Third");
        break;
    }
}

void fromCalibration(Write &&value, const Calibration &calibration, bool includeRoot)
{ return fromCalibration(value, calibration, includeRoot); }

void fromVariant(Write &value, const QVariant &variant)
{
    auto typeID = variant.userType();
    if (typeID == qMetaTypeId<Read>()) {
        value.set(variant.value<Read>());
        return;
    } else if (typeID == qMetaTypeId<Write>()) {
        value.set(variant.value<Write>());
        return;
    } else if (typeID == qMetaTypeId<Root>()) {
        value.set(variant.value<Root>());
        return;
    }

    switch (typeID) {
    case QVariant::Invalid:
        value.setEmpty();
        return;
    case QVariant::Bool:
        value.setBoolean(variant.toBool());
        return;
    case QVariant::Int:
        value.setInteger(variant.toInt());
        return;
    case QVariant::UInt:
        value.setInteger(static_cast<std::int_fast64_t>(variant.toUInt()));
        return;
    case QVariant::LongLong:
        value.setInteger(static_cast<std::int_fast64_t>(variant.toLongLong()));
        return;
    case QVariant::ULongLong:
        value.setInteger(static_cast<std::int_fast64_t>(variant.toULongLong()));
        return;
    case QVariant::Double:
        value.setReal(variant.toDouble());
        return;
    case QVariant::Char:
        value.setString(QString(variant.toChar()));
        return;
    case QVariant::String:
        value.setString(variant.toString());
        return;
    case QVariant::ByteArray:
        value.setBytes(variant.toByteArray());
        return;

    case QVariant::Date:
        value.setReal(Time::fromDateTime(QDateTime(variant.toDate(), QTime(0, 0, 0), Qt::UTC)));
        return;
    case QVariant::DateTime:
        value.setReal(Time::fromDateTime(variant.toDateTime(), true));
        return;
    case QVariant::Url:
        value.setString(variant.toUrl().toString());
        return;
    case QVariant::RegExp:
        value.setString(variant.toRegExp().pattern());
        return;
    case QVariant::List: {
        value.setType(Type::Array);
        auto output = value.toArray();
        for (const auto &add : variant.toList()) {
            fromVariant(output.after_back(), add);
        }
        return;
    }

    case QVariant::StringList: {
        value.setType(Type::Array);
        auto output = value.toArray();
        for (const auto &add : variant.toStringList()) {
            output.after_back().setString(add);
        }
        return;
    }

    case QVariant::Hash: {
        auto hash = variant.toHash();
        value.setType(Type::Hash);
        for (auto add = hash.constBegin(), endAdd = hash.constEnd(); add != endAdd; ++add) {
            fromVariant(value.hash(add.key()), add.value());
        }
        return;
    }

    case QVariant::Map: {
        auto hash = variant.toMap();
        value.setType(Type::Hash);
        for (auto add = hash.constBegin(), endAdd = hash.constEnd(); add != endAdd; ++add) {
            fromVariant(value.hash(add.key()), add.value());
        }
        return;
    }

    default:
        break;
    }

    if (variant.canConvert(QVariant::Double)) {
        value.setDouble(variant.toDouble());
        return;
    }
    if (variant.canConvert(QVariant::LongLong)) {
        value.setInteger(static_cast<std::int_fast64_t>(variant.toLongLong()));
        return;
    }
    if (variant.canConvert(QVariant::String)) {
        value.setString(variant.toString());
        return;
    }
    if (variant.canConvert(QVariant::Bool)) {
        value.setBoolean(variant.toBool());
        return;
    }

    value.setEmpty();
}

void fromVariant(Write &&value, const QVariant &variant)
{ return fromVariant(value, variant); }

template<typename Container>
std::string uniqueStringChild(const Container &input)
{
    if (input.find("Value") == input.end())
        return "Value";
    for (std::size_t index = 1; index != 0; ++index) {
        std::string value = "Value";
        value += std::to_string(index);
        if (input.find(value) == input.end())
            return value;
    }
    return std::string();
}

PathElement uniqueKey(const Read &parent, const Read &metadata, bool *ok)
{
    switch (parent.getType()) {
    case Type::Empty:
    case Type::Real:
    case Type::Integer:
    case Type::Boolean:
    case Type::String:
    case Type::Bytes:
    case Type::Flags:
    case Type::Overlay:
        if (ok)
            *ok = false;
        /* Can't actually have children, so just a default path element */
        return PathElement();

    case Type::Hash: {
        auto hash = parent.toHash();
        auto children = metadata.toMetadataHashChild();
        for (auto check : children) {
            if (check.first.empty())
                continue;
            if (hash.find(check.first) != hash.end())
                continue;
            if (ok)
                *ok = true;
            return PathElement(PathElement::Type::Hash, check.first);
        }
        PathElement e(PathElement::Type::Hash, uniqueStringChild(parent.toHash()));
        if (ok)
            *ok = metadata.getPath(e).exists();
        return e;
    }

    case Type::Array: {
        auto array = parent.toArray();
        std::size_t index = array.size();
        for (std::size_t check = 0; check < index; ++check) {
            if (array[check].exists())
                continue;
            index = check;
            break;
        }
        if (ok) {
            auto max = metadata.metadataArray("Count").toInteger();
            *ok = !INTEGER::defined(max) || index < static_cast<std::size_t>(max);
        }
        return PathElement(PathElement::Type::Array, index);
    }
    case Type::Matrix: {
        auto existing = parent.toMatrix();
        PathElement::MatrixIndex shape;
        for (auto add : metadata.metadataMatrix("Size").toArray()) {
            auto dim = add.toInteger();
            if (!INTEGER::defined(dim) || dim <= 0)
                continue;
            shape.push_back(static_cast<std::size_t>(dim));
        }
        if (shape.empty())
            shape = existing.shape();

        PathElement::MatrixIndex index;
        for (auto check : existing) {
            if (check.second.exists())
                continue;
            index = check.first;
            break;
        }

        if (index.empty()) {
            if (shape.empty()) {
                index.push_back(0);
            } else {
                index.resize(shape.size(), 0);
                index[0] = shape[0] + 1;
            }
        }

        if (ok) {
            auto size = metadata.metadataMatrix("Size").toArray();
            if (index.size() != size.size()) {
                *ok = false;
            } else {
                auto idim = index.begin();
                *ok = true;
                for (auto add : size) {
                    auto dim = add.toInteger();
                    if (INTEGER::defined(dim) && dim > 0) {
                        if (static_cast<std::size_t>(dim) < *idim) {
                            *ok = false;
                            break;
                        }
                    }
                    ++idim;
                }
            }
        }

        return PathElement(PathElement::Type::Matrix, std::move(index));
    }
    case Type::Keyframe: {
        auto existing = parent.toKeyframe();

        auto editor = metadata.metadataKeyframe("Editor");
        double minimum = editor["Minimum"].toDouble();
        bool minimumExclusive = editor["MinimumExclusive"].toBool();
        double maximum = editor["Maximum"].toDouble();
        bool maximumExclusive = editor["MaximumExclusive"].toBool();

        if (ok)
            *ok = true;

        double key = 0.0;
        if (existing.empty()) {
            if (FP::defined(minimum)) {
                if (!minimumExclusive)
                    key = minimum;
                else if (!FP::defined(maximum))
                    key = minimum + 1.0;
                else
                    key = (minimum + maximum) * 0.5;
            } else if (FP::defined(maximum)) {
                if (!maximumExclusive)
                    key = maximum;
                else
                    key = maximum - 1.0;
            }
        } else {
            auto end = existing.end();
            --end;
            key = end.key();
            if (FP::defined(maximum) && key < maximum) {
                key = (key + maximum) * 0.5;
            } else {
                key = key + 1.0;
            }
        }

        if (ok) {
            *ok = true;
            if (FP::defined(minimum)) {
                if (key < minimum || (minimumExclusive && key == minimum))
                    *ok = false;
            }
            if (FP::defined(maximum)) {
                if (key > maximum || (maximumExclusive && key == maximum))
                    *ok = false;
            }
        }

        return PathElement(PathElement::Type::Keyframe, key);
    }

    case Type::MetadataReal:
        if (ok)
            *ok = true;
        return PathElement(PathElement::Type::MetadataReal,
                           uniqueStringChild(parent.toMetadataReal()));
    case Type::MetadataInteger:
        if (ok)
            *ok = true;
        return PathElement(PathElement::Type::MetadataInteger,
                           uniqueStringChild(parent.toMetadataInteger()));
    case Type::MetadataBoolean:
        if (ok)
            *ok = true;
        return PathElement(PathElement::Type::MetadataBoolean,
                           uniqueStringChild(parent.toMetadataBoolean()));
    case Type::MetadataString:
        if (ok)
            *ok = true;
        return PathElement(PathElement::Type::MetadataString,
                           uniqueStringChild(parent.toMetadataString()));
    case Type::MetadataBytes:
        if (ok)
            *ok = true;
        return PathElement(PathElement::Type::MetadataBytes,
                           uniqueStringChild(parent.toMetadataBytes()));
    case Type::MetadataFlags:
        if (ok)
            *ok = true;
        return PathElement(PathElement::Type::MetadataFlags,
                           uniqueStringChild(parent.toMetadataFlags()));
    case Type::MetadataHash:
        if (ok)
            *ok = true;
        return PathElement(PathElement::Type::MetadataHash,
                           uniqueStringChild(parent.toMetadataHash()));
    case Type::MetadataArray:
        if (ok)
            *ok = true;
        return PathElement(PathElement::Type::MetadataArray,
                           uniqueStringChild(parent.toMetadataArray()));
    case Type::MetadataMatrix:
        if (ok)
            *ok = true;
        return PathElement(PathElement::Type::MetadataMatrix,
                           uniqueStringChild(parent.toMetadataMatrix()));
    case Type::MetadataKeyframe:
        if (ok)
            *ok = true;
        return PathElement(PathElement::Type::MetadataKeyframe,
                           uniqueStringChild(parent.toMetadataKeyframe()));

    }

    Q_ASSERT(false);
    return PathElement();
}

bool isContaminationFlag(const Variant::Flag &flag)
{
    static const std::string contaminationPrefix = "contaminat";
    if (flag.length() < contaminationPrefix.length())
        return false;
    return std::equal(contaminationPrefix.begin(), contaminationPrefix.end(), flag.begin(),
                      [](char cA, char cB) -> bool {
                          return cA == std::tolower(cB);
                      });
}

bool isContaminationFlag(const Variant::Flags &flags)
{
    for (const auto &check : flags) {
        if (isContaminationFlag(check))
            return true;
    }
    return false;
}

void invalidate(Write &value)
{
    switch (value.getType()) {
    case Type::Real:
    case Type::Integer:
        value.clear();
        return;
    default:
        break;
    }
    value.setEmpty();
}

void invalidate(Write &&value)
{ return invalidate(value); }

bool isDefined(const Read &value)
{
    switch (value.getType()) {
    case Variant::Type::Empty:
        return false;
    case Variant::Type::Real:
        return FP::defined(value.toReal());
    case Variant::Type::Integer:
        return INTEGER::defined(value.toInteger());
    default:
        break;
    }
    return true;
}

double toNumber(const Read &value, bool convertStrings)
{
    switch (value.getType()) {
    case Variant::Type::Real:
        return value.toReal();
    case Variant::Type::Integer: {
        auto check = value.toInteger();
        if (!INTEGER::defined(check))
            return FP::undefined();
        return static_cast<double>(check);
    }
    case Variant::Type::String: {
        if (!convertStrings)
            break;
        bool ok = false;
        auto v = value.toQString().toDouble(&ok);
        if (!ok)
            return FP::undefined();
        return v;
    }
    default:
        break;
    }
    return FP::undefined();
}

std::int_fast64_t toInteger(const Read &value, bool convertStrings)
{
    switch (value.getType()) {
    case Variant::Type::Real: {
        auto check = value.toReal();
        if (!FP::defined(check))
            return INTEGER::undefined();
        return static_cast<std::int_fast64_t>(std::round(check));
    }
    case Variant::Type::Integer:
        return value.toInteger();
    case Variant::Type::String: {
        if (!convertStrings)
            break;
        bool ok = false;
        auto v = value.toQString().toLongLong(&ok);
        if (!ok)
            return INTEGER::undefined();
        return v;
    }
    default:
        break;
    }
    return INTEGER::undefined();
}

}
}
}
}