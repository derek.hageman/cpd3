/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <memory>
#include <QDebugStateSaver>

#include "dynamicinput.hxx"
#include "variant/composite.hxx"
#include "core/qtcompat.hxx"


namespace CPD3 {
namespace Data {

enum TimeInputType {
    TI_NULL = 0, TI_CONSTANT, TI_VARIABLE, TI_CALIBRATE, TI_VARIABLESELECTION
};

DynamicInput::Constant::Constant() : value(FP::undefined())
{ }

DynamicInput::Constant::Constant(double v) : value(v)
{ }

DynamicInput::Constant::~Constant() = default;

double DynamicInput::Constant::get(double)
{ return value; }

double DynamicInput::Constant::getConst(double) const
{ return value; }

double DynamicInput::Constant::get(SequenceSegment &)
{ return value; }

double DynamicInput::Constant::get(const SequenceSegment &)
{ return value; }

double DynamicInput::Constant::getConst(SequenceSegment &) const
{ return value; }

double DynamicInput::Constant::getConst(const SequenceSegment &) const
{ return value; }

DynamicInput *DynamicInput::Constant::clone() const
{ return new DynamicInput::Constant(value); }

DynamicInput::Constant::Constant(QDataStream &stream)
{ stream >> value; }

void DynamicInput::Constant::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) TI_CONSTANT;
    stream << t << value;
}

void DynamicInput::Constant::printLog(QDebug &stream) const
{
    stream << "DynamicInput::Constant(" << value << ")";
}

QString DynamicInput::Constant::describe(double, double, const QString &whenUndefined) const
{
    if (!FP::defined(value))
        return whenUndefined;
    return FP::decimalFormat(value);
}

double DynamicInput::Constant::constant(double, double) const
{ return value; }

DynamicInput::Basic::Basic(const SequenceName &u) : unit(u)
{ }

DynamicInput::Basic::~Basic() = default;

double DynamicInput::Basic::get(double)
{ return FP::undefined(); }

double DynamicInput::Basic::getConst(double) const
{ return FP::undefined(); }

double DynamicInput::Basic::get(SequenceSegment &in)
{ return in.getValue(unit).toDouble(); }

double DynamicInput::Basic::get(const SequenceSegment &in)
{ return in.getValue(unit).toDouble(); }

double DynamicInput::Basic::getConst(SequenceSegment &in) const
{ return in.getValue(unit).toDouble(); }

double DynamicInput::Basic::getConst(const SequenceSegment &in) const
{ return in.getValue(unit).toDouble(); }

Variant::Read DynamicInput::Basic::getValue(SequenceSegment &in)
{ return in.getValue(unit); }

Variant::Read DynamicInput::Basic::getValue(const SequenceSegment &in)
{ return in.getValue(unit); }

Variant::Read DynamicInput::Basic::getValueConst(SequenceSegment &in) const
{ return in.getValue(unit); }

Variant::Read DynamicInput::Basic::getValueConst(const SequenceSegment &in) const
{ return in.getValue(unit); }

bool DynamicInput::Basic::registerInput(const SequenceName &input)
{ return input == unit; }

SequenceName::Set DynamicInput::Basic::getUsedInputs() const
{ return SequenceName::Set{unit}; }

DynamicInput *DynamicInput::Basic::clone() const
{ return new DynamicInput::Basic(unit); }

DynamicInput::Basic::Basic(QDataStream &stream)
{ stream >> unit; }

void DynamicInput::Basic::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) TI_VARIABLE;
    stream << t << unit;
}

void DynamicInput::Basic::printLog(QDebug &stream) const
{ stream << "DynamicInput::Basic(" << unit << ")"; }

QString DynamicInput::Basic::describe(double, double, const QString &) const
{ return QString::fromStdString(unit.getVariable()); }

DynamicInput::Calibration::Calibration(DynamicInput *source, const CPD3::Calibration &calibration)
        : origin(source), cal(calibration)
{ }

DynamicInput::Calibration::Calibration(std::unique_ptr<DynamicInput> &&source,
                                       const CPD3::Calibration &calibration) : origin(
        std::move(source)), cal(calibration)
{ }

DynamicInput::Calibration::~Calibration() = default;

double DynamicInput::Calibration::get(double time)
{ return cal.apply(origin->get(time)); }

double DynamicInput::Calibration::getConst(double time) const
{ return cal.apply(origin->getConst(time)); }

double DynamicInput::Calibration::get(SequenceSegment &in)
{ return cal.apply(origin->get(in)); }

double DynamicInput::Calibration::get(const SequenceSegment &in)
{ return cal.apply(origin->get(in)); }

double DynamicInput::Calibration::getConst(SequenceSegment &in) const
{ return cal.apply(origin->getConst(in)); }

double DynamicInput::Calibration::getConst(const SequenceSegment &in) const
{ return cal.apply(origin->getConst(in)); }

Variant::Read DynamicInput::Calibration::getValue(SequenceSegment &in)
{
    return Variant::Composite::applyTransform(origin->getValue(in),
                                              [this](const Variant::Read &input,
                                                     Variant::Write &output) {
                                                  output.setReal(cal.apply(input.toReal()));
                                              }).read();
}

Variant::Read DynamicInput::Calibration::getValue(const SequenceSegment &in)
{
    return Variant::Composite::applyTransform(origin->getValue(in),
                                              [this](const Variant::Read &input,
                                                     Variant::Write &output) {
                                                  output.setReal(cal.apply(input.toReal()));
                                              }).read();
}

Variant::Read DynamicInput::Calibration::getValueConst(SequenceSegment &in) const
{
    return Variant::Composite::applyTransform(origin->getValue(in),
                                              [this](const Variant::Read &input,
                                                     Variant::Write &output) {
                                                  output.setReal(cal.apply(input.toReal()));
                                              }).read();
}

Variant::Read DynamicInput::Calibration::getValueConst(const SequenceSegment &in) const
{
    return Variant::Composite::applyTransform(origin->getValue(in),
                                              [this](const Variant::Read &input,
                                                     Variant::Write &output) {
                                                  output.setReal(cal.apply(input.toReal()));
                                              }).read();
}

QString DynamicInput::Calibration::describe(double start,
                                            double end,
                                            const QString &whenUndefined) const
{
    return QString::fromLatin1("Calibration(%1)").arg(origin->describe(start, end, whenUndefined));
}

double DynamicInput::Calibration::constant(double start, double end) const
{ return cal.apply(origin->constant(start, end)); }

bool DynamicInput::Calibration::registerInput(const SequenceName &input)
{ return origin->registerInput(input); }

void DynamicInput::Calibration::registerExpectedFlavorless(const SequenceName::Component &station,
                                                           const SequenceName::Component &archive,
                                                           const SequenceName::Component &variable)
{ return origin->registerExpectedFlavorless(station, archive, variable); }

void DynamicInput::Calibration::registerExpected(const SequenceName::Component &station,
                                                 const SequenceName::Component &archive,
                                                 const SequenceName::Component &variable,
                                                 const SequenceName::Flavors &flavors)
{ return origin->registerExpected(station, archive, variable, flavors); }

void DynamicInput::Calibration::registerExpected(const SequenceName::Component &station,
                                                 const SequenceName::Component &archive,
                                                 const SequenceName::Component &variable,
                                                 const std::unordered_set<
                                                         SequenceName::Flavors> &flavors)
{ return origin->registerExpected(station, archive, variable, flavors); }

SequenceName::Set DynamicInput::Calibration::getUsedInputs() const
{ return origin->getUsedInputs(); }

QSet<double> DynamicInput::Calibration::getChangedPoints() const
{ return origin->getChangedPoints(); }

DynamicInput *DynamicInput::Calibration::clone() const
{ return new DynamicInput::Calibration(origin->clone(), cal); }

DynamicInput::Calibration::Calibration(QDataStream &stream)
{
    DynamicInput *in;
    stream >> in >> cal;
    origin.reset(in);
}

void DynamicInput::Calibration::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(TI_CALIBRATE) << origin.get() << cal;
}

void DynamicInput::Calibration::printLog(QDebug &stream) const
{ stream << "Apply(" << cal << "," << origin.get() << ")"; }


DynamicInput::Variable::InputSegment::InputSegment() : start(FP::undefined()),
                                                       end(FP::undefined()),
                                                       constant(FP::undefined()),
                                                       constantFirst(false),
                                                       variableSelect(),
                                                       path(),
                                                       calibration()
{ }

DynamicInput::Variable::InputSegment::InputSegment(const InputSegment &other) = default;

DynamicInput::Variable::InputSegment &DynamicInput::Variable::InputSegment::operator=(const InputSegment &other) = default;

DynamicInput::Variable::InputSegment::InputSegment(InputSegment &&other) = default;

DynamicInput::Variable::InputSegment &DynamicInput::Variable::InputSegment::operator=(InputSegment &&other) = default;

DynamicInput::Variable::InputSegment::InputSegment(const InputSegment &other,
                                                   double setStart,
                                                   double setEnd) : start(setStart),
                                                                    end(setEnd),
                                                                    constant(other.constant),
                                                                    constantFirst(
                                                                            other.constantFirst),
                                                                    variableSelect(
                                                                            other.variableSelect),
                                                                    path(other.path),
                                                                    calibration(other.calibration)
{ }

DynamicInput::Variable::InputSegment::InputSegment(const InputSegment &,
                                                   const InputSegment &over,
                                                   double setStart,
                                                   double setEnd) : start(setStart),
                                                                    end(setEnd),
                                                                    constant(over.constant),
                                                                    constantFirst(
                                                                            over.constantFirst),
                                                                    variableSelect(
                                                                            over.variableSelect),
                                                                    path(over.path),
                                                                    calibration(over.calibration)
{ }

DynamicInput::Variable::InputSegment::InputSegment(const InputSegment &under,
                                                   const ConstantOverlay &over,
                                                   double setStart,
                                                   double setEnd) : start(setStart),
                                                                    end(setEnd),
                                                                    constant(over.constant),
                                                                    constantFirst(over.first),
                                                                    variableSelect(
                                                                            under.variableSelect),
                                                                    path(under.path),
                                                                    calibration(under.calibration)
{ }

DynamicInput::Variable::InputSegment::InputSegment(const InputSegment &under,
                                                   const VariableOverlay &over,
                                                   double setStart,
                                                   double setEnd) : start(setStart),
                                                                    end(setEnd),
                                                                    constant(under.constant),
                                                                    constantFirst(false),
                                                                    variableSelect(over.selection),
                                                                    path(over.path),
                                                                    calibration(under.calibration)
{ }

DynamicInput::Variable::InputSegment::InputSegment(const InputSegment &under,
                                                   const CalibrationOverlay &over,
                                                   double setStart,
                                                   double setEnd) : start(setStart),
                                                                    end(setEnd),
                                                                    constant(under.constant),
                                                                    constantFirst(
                                                                            under.constantFirst),
                                                                    variableSelect(
                                                                            under.variableSelect),
                                                                    path(under.path),
                                                                    calibration(over.calibration)
{ }

DynamicInput::Variable::InputSegment::InputSegment(const ConstantOverlay &other,
                                                   double setStart,
                                                   double setEnd) : start(setStart),
                                                                    end(setEnd),
                                                                    constant(other.constant),
                                                                    constantFirst(other.first),
                                                                    variableSelect(),
                                                                    path(),
                                                                    calibration()
{ }

DynamicInput::Variable::InputSegment::InputSegment(const VariableOverlay &other,
                                                   double setStart,
                                                   double setEnd) : start(setStart),
                                                                    end(setEnd),
                                                                    constant(FP::undefined()),
                                                                    constantFirst(false),
                                                                    variableSelect(other.selection),
                                                                    path(other.path),
                                                                    calibration()
{ }

DynamicInput::Variable::InputSegment::InputSegment(const CalibrationOverlay &other,
                                                   double setStart,
                                                   double setEnd) : start(setStart),
                                                                    end(setEnd),
                                                                    constant(FP::undefined()),
                                                                    constantFirst(false),
                                                                    variableSelect(),
                                                                    path(),
                                                                    calibration(other.calibration)
{ }

DynamicInput::Variable::Variable() = default;

DynamicInput::Variable::Variable(const DynamicInput::Variable &other) = default;

DynamicInput::Variable &DynamicInput::Variable::operator=(const DynamicInput::Variable &other) = default;

DynamicInput::Variable::Variable(DynamicInput::Variable &&other) = default;

DynamicInput::Variable &DynamicInput::Variable::operator=(DynamicInput::Variable &&other) = default;

DynamicInput::Variable::~Variable() = default;

double DynamicInput::Variable::get(double time)
{
    if (!Range::intersectShift(segments, time))
        return FP::undefined();
    return segments.front().constant;
}

double DynamicInput::Variable::getConst(double time) const
{
    auto i = Range::findIntersecting(segments, time);
    if (i == segments.end())
        return FP::undefined();
    return i->constant;
}

double DynamicInput::Variable::get(SequenceSegment &in)
{
    if (!Range::intersectShift(segments, in))
        return FP::undefined();
    const auto &f = segments.front();
    if (f.constantFirst)
        return f.constant;
    double v = f.variableSelect.lookup(in).getPath(f.path).toDouble();
    if (FP::defined(v))
        return f.calibration.apply(v);
    return f.constant;
}

double DynamicInput::Variable::get(const SequenceSegment &in)
{
    if (!Range::intersectShift(segments, in))
        return FP::undefined();
    const auto &f = segments.front();
    if (f.constantFirst)
        return f.constant;
    double v = f.variableSelect.lookup(in).getPath(f.path).toDouble();
    if (FP::defined(v))
        return f.calibration.apply(v);
    return f.constant;
}

double DynamicInput::Variable::getConst(SequenceSegment &in) const
{
    auto i = Range::findIntersecting(segments, in);
    if (i == segments.end())
        return FP::undefined();
    if (i->constantFirst)
        return i->constant;
    double v = i->variableSelect.lookup(in).getPath(i->path).toDouble();
    if (FP::defined(v))
        return i->calibration.apply(v);
    return i->constant;
}

double DynamicInput::Variable::getConst(const SequenceSegment &in) const
{
    auto i = Range::findIntersecting(segments, in);
    if (i == segments.end())
        return FP::undefined();
    if (i->constantFirst)
        return i->constant;
    double v = i->variableSelect.lookup(in).getPath(i->path).toDouble();
    if (FP::defined(v))
        return i->calibration.apply(v);
    return i->constant;
}

Variant::Read DynamicInput::Variable::getValue(SequenceSegment &in)
{
    Variant::Write result;
    if (!Range::intersectShift(segments, in)) {
        result.detachFromRoot();
        return result;
    }
    const auto &f = segments.front();
    if (f.constantFirst) {
        result.detachFromRoot();
        result.setReal(f.constant);
        return result;
    }
    result = Variant::Root(f.variableSelect.lookup(in).getPath(f.path)).write();
    switch (result.getType()) {
    case Variant::Type::Array:
        for (auto mod : result.toArray()) {
            mod.setDouble(f.calibration.apply(mod.toDouble()));
        }
        break;
    case Variant::Type::Matrix:
        for (auto mod : result.toMatrix()) {
            mod.second.setDouble(f.calibration.apply(mod.second.toDouble()));
        }
        break;
    default: {
        double v = result.toDouble();
        if (FP::defined(v)) {
            result.setReal(f.calibration.apply(v));
        } else if (FP::defined(f.constant)) {
            result.setReal(f.constant);
        } else {
            result.setReal(FP::undefined());
        }
        return result;
    }
    }
    if (!result.exists() && FP::defined(f.constant))
        result.setReal(f.constant);
    return result;
}

Variant::Read DynamicInput::Variable::getValue(const SequenceSegment &in)
{
    Variant::Write result;
    if (!Range::intersectShift(segments, in)) {
        result.detachFromRoot();
        return result;
    }
    const auto &f = segments.front();
    if (f.constantFirst) {
        result.detachFromRoot();
        result.setReal(f.constant);
        return result;
    }
    result = Variant::Root(f.variableSelect.lookup(in).getPath(f.path)).write();
    switch (result.getType()) {
    case Variant::Type::Array:
        for (auto mod : result.toArray()) {
            mod.setDouble(f.calibration.apply(mod.toDouble()));
        }
        break;
    case Variant::Type::Matrix:
        for (auto mod : result.toMatrix()) {
            mod.second.setDouble(f.calibration.apply(mod.second.toDouble()));
        }
        break;
    default: {
        double v = result.toDouble();
        if (FP::defined(v)) {
            result.setReal(f.calibration.apply(v));
        } else if (FP::defined(f.constant)) {
            result.setReal(f.constant);
        } else {
            result.setReal(FP::undefined());
        }
        return result;
    }
    }
    if (!result.exists() && FP::defined(f.constant))
        result.setReal(f.constant);
    return result;
}

Variant::Read DynamicInput::Variable::getValueConst(SequenceSegment &in) const
{
    auto i = Range::findIntersecting(segments, in);
    Variant::Write result;
    if (i == segments.end()) {
        result.detachFromRoot();
        return result;
    }
    if (i->constantFirst) {
        result.detachFromRoot();
        result.setReal(i->constant);
        return result;
    }
    result = Variant::Root(i->variableSelect.lookup(in).getPath(i->path)).write();
    switch (result.getType()) {
    case Variant::Type::Array:
        for (auto mod : result.toArray()) {
            mod.setDouble(i->calibration.apply(mod.toDouble()));
        }
        break;
    case Variant::Type::Matrix:
        for (auto mod : result.toMatrix()) {
            mod.second.setDouble(i->calibration.apply(mod.second.toDouble()));
        }
        break;
    default: {
        double v = result.toDouble();
        if (FP::defined(v)) {
            result.setReal(i->calibration.apply(v));
        } else if (FP::defined(i->constant)) {
            result.setReal(i->constant);
        } else {
            result.setReal(FP::undefined());
        }
        return result;
    }
    }
    if (!result.exists() && FP::defined(i->constant))
        result.setReal(i->constant);
    return result;
}

Variant::Read DynamicInput::Variable::getValueConst(const SequenceSegment &in) const
{
    auto i = Range::findIntersecting(segments, in);
    Variant::Write result;
    if (i == segments.end()) {
        result.detachFromRoot();
        return result;
    }
    if (i->constantFirst) {
        result.detachFromRoot();
        result.setReal(i->constant);
        return result;
    }
    result = Variant::Root(i->variableSelect.lookup(in).getPath(i->path)).write();
    switch (result.getType()) {
    case Variant::Type::Array:
        for (auto mod : result.toArray()) {
            mod.setDouble(i->calibration.apply(mod.toDouble()));
        }
        break;
    case Variant::Type::Matrix:
        for (auto mod : result.toMatrix()) {
            mod.second.setDouble(i->calibration.apply(mod.second.toDouble()));
        }
        break;
    default: {
        double v = result.toDouble();
        if (FP::defined(v)) {
            result.setReal(i->calibration.apply(v));
        } else if (FP::defined(i->constant)) {
            result.setReal(i->constant);
        } else {
            result.setReal(FP::undefined());
        }
        return result;
    }
    }
    if (!result.exists() && FP::defined(i->constant))
        result.setReal(i->constant);
    return result;
}

bool DynamicInput::Variable::registerInput(const SequenceName &input)
{
    bool hit = false;
    for (auto &seg : segments) {
        if (seg.variableSelect.registerInput(input))
            hit = true;
    }
    return hit;
}

void DynamicInput::Variable::registerExpectedFlavorless(const SequenceName::Component &station,
                                                        const SequenceName::Component &archive,
                                                        const SequenceName::Component &variable)
{
    for (auto &seg : segments) {
        seg.variableSelect.registerExpectedFlavorless(station, archive, variable);
    }
}

void DynamicInput::Variable::registerExpected(const SequenceName::Component &station,
                                              const SequenceName::Component &archive,
                                              const SequenceName::Component &variable,
                                              const SequenceName::Flavors &flavors)
{
    for (auto &seg : segments) {
        seg.variableSelect.registerExpected(station, archive, variable, flavors);
    }
}

void DynamicInput::Variable::registerExpected(const SequenceName::Component &station,
                                              const SequenceName::Component &archive,
                                              const SequenceName::Component &variable,
                                              const std::unordered_set<
                                                      SequenceName::Flavors> &flavors)
{
    for (auto &seg : segments) {
        seg.variableSelect.registerExpected(station, archive, variable, flavors);
    }
}

SequenceName::Set DynamicInput::Variable::getUsedInputs() const
{
    SequenceName::Set out;
    for (const auto &seg : segments) {
        Util::merge(seg.variableSelect.knownInputs(), out);
    }
    return out;
}

QSet<double> DynamicInput::Variable::getChangedPoints() const
{
    QSet<double> out;
    for (const auto &seg : segments) {
        if (FP::defined(seg.start))
            out |= seg.start;
        if (FP::defined(seg.end))
            out |= seg.end;
    }
    return out;
}

QString DynamicInput::Variable::describe(double start,
                                         double end,
                                         const QString &whenUndefined) const
{
    auto i = Range::findIntersecting(segments, start, end);
    if (i == segments.end())
        return whenUndefined;
    if (i->constantFirst) {
        if (!FP::defined(i->constant))
            return whenUndefined;
        return FP::decimalFormat(i->constant);
    }
    auto check = i->variableSelect.knownInputs();
    if (check.size() == 1 && i->calibration.isIdentity() && i->path.empty())
        return check.begin()->getVariableQString();
    return QString::fromLatin1("Variable");
}

double DynamicInput::Variable::constant(double start, double end) const
{
    auto i = Range::findIntersecting(segments, start, end);
    if (i == segments.end())
        return FP::undefined();
    if (i->constantFirst)
        return i->constant;
    return FP::undefined();
}

void DynamicInput::Variable::set(double start, double end, const Variant::Read &v,
                                 const SequenceName::Component &defaultStation,
                                 const SequenceName::Component &defaultArchive,
                                 const SequenceName::Component &defaultVariable)
{
    InputSegment seg;
    seg.start = start;
    seg.end = end;

    if (!v.exists()) {
        seg.constantFirst = true;
        seg.constant = FP::undefined();
        Range::overlayFragmenting(segments, seg);
        return;
    }

    switch (v.getType()) {
    case Variant::Type::Real:
        seg.constantFirst = true;
        seg.constant = v.toDouble();
        Range::overlayFragmenting(segments, seg);
        return;
    case Variant::Type::Array:
    case Variant::Type::String:
        seg.variableSelect =
                SequenceMatch::OrderedLookup(v, {QString::fromStdString(defaultStation)},
                                             {QString::fromStdString(defaultArchive)},
                                             {QString::fromStdString(defaultVariable)});
        Range::overlayFragmenting(segments, seg);
        return;
    default:
        break;
    }

    auto vc = v["Constant"];
    if (vc.exists()) {
        seg.constantFirst = true;
        seg.constant = vc.toDouble();
        Range::overlayFragmenting(segments, seg);
        return;
    }

    seg.variableSelect =
            SequenceMatch::OrderedLookup(v["Input"], {QString::fromStdString(defaultStation)},
                                         {QString::fromStdString(defaultArchive)},
                                         {QString::fromStdString(defaultVariable)});
    seg.path = v["Path"].toString();
    seg.calibration = Variant::Composite::toCalibration(v["Calibration"]);
    seg.constant = v["Default"].toDouble();
    Range::overlayFragmenting(segments, seg);
}

void DynamicInput::Variable::set(const Variant::Read &v,
                                 const SequenceName::Component &defaultStation,
                                 const SequenceName::Component &defaultArchive,
                                 const SequenceName::Component &defaultVariable)
{
    segments.clear();
    set(FP::undefined(), FP::undefined(), v, defaultStation, defaultArchive, defaultVariable);
}

void DynamicInput::Variable::clear()
{
    segments.clear();
}

void DynamicInput::Variable::set(double constant)
{
    segments.clear();
    segments.emplace_back();
    auto &s = segments.front();
    s.constantFirst = true;
    s.constant = constant;
}

void DynamicInput::Variable::set(const SequenceMatch::OrderedLookup &variableSelect,
                                 const CPD3::Calibration &calibration,
                                 double failoverConstant,
                                 const std::string &path)
{
    segments.clear();
    segments.emplace_back();
    auto &s = segments.front();
    s.variableSelect = variableSelect;
    s.path = path;
    s.calibration = calibration;
    s.constantFirst = false;
    s.constant = failoverConstant;
}

void DynamicInput::Variable::set(double start, double end, double constant, bool isFailover)
{
    if (!isFailover && !FP::defined(start) && !FP::defined(end)) {
        set(constant);
        return;
    }
    Range::overlayFragmenting(segments, ConstantOverlay(constant, !isFailover, start, end));
}

void DynamicInput::Variable::set(double start,
                                 double end,
                                 const SequenceMatch::OrderedLookup &variableSelect,
                                 const CPD3::Calibration &calibration,
                                 double failoverConstant,
                                 const std::string &path)
{
    Range::overlayFragmenting(segments, VariableOverlay(variableSelect, path, start, end));
    Range::overlayFragmenting(segments, CalibrationOverlay(calibration, start, end));
    Range::overlayFragmenting(segments, ConstantOverlay(failoverConstant, false, start, end));
}

void DynamicInput::Variable::set(double start, double end, const CPD3::Calibration &calibration)
{
    Range::overlayFragmenting(segments, CalibrationOverlay(calibration, start, end));
}

DynamicInput *DynamicInput::Variable::clone() const
{ return new DynamicInput::Variable(*this); }

DynamicInput::Variable::Variable(QDataStream &stream)
{
    Deserialize::container(stream, segments, [&stream] {
        InputSegment s;

        stream >> s.start;
        stream >> s.end;
        stream >> s.constant;
        stream >> s.constantFirst;
        stream >> s.variableSelect;
        stream >> s.path;
        stream >> s.calibration;

        return s;
    });
}

void DynamicInput::Variable::serialize(QDataStream &stream) const
{
    stream << static_cast<quint8>(TI_VARIABLESELECTION);
    Serialize::container(stream, segments, [&stream](const InputSegment &seg) {
        stream << seg.start;
        stream << seg.end;
        stream << seg.constant;
        stream << seg.constantFirst;
        stream << seg.variableSelect;
        stream << seg.path;
        stream << seg.calibration;
    });
}

void DynamicInput::Variable::printLog(QDebug &stream) const
{
    stream << "DynamicInput::Variable(";
    bool first = true;
    for (const auto &seg : segments) {
        if (!first)
            stream << ",";
        first = false;
        stream << "[" << Logging::range(seg.start, seg.end) << ",Constant=" << seg.constant << ","
               << (seg.constantFirst ? "ConstantFirst," : "VariableFirst,") << seg.variableSelect
               << "," << seg.path << "," << seg.calibration << "]";
    }
    stream << ")";
}


DynamicInputOption::DynamicInputOption(const QString &argumentName,
                                       const QString &displayName,
                                       const QString &description,
                                       const QString &defaultBehavior,
                                       int sortPriority) : ComponentOptionBase(argumentName,
                                                                               displayName,
                                                                               description,
                                                                               defaultBehavior,
                                                                               sortPriority),
                                                           input()
{ }

DynamicInputOption::DynamicInputOption(const DynamicInputOption &other) = default;

DynamicInputOption::~DynamicInputOption() = default;

void DynamicInputOption::reset()
{
    optionSet = false;
    input.clear();
}

ComponentOptionBase *DynamicInputOption::clone() const
{ return new DynamicInputOption(*this); }

DynamicInput *DynamicInputOption::getInput() const
{
    if (input.segments.empty())
        return new DynamicInput::Constant();
    if (input.segments.size() == 1) {
        const auto &f = input.segments.front();
        if (!FP::defined(f.start) && !FP::defined(f.end)) {
            if (f.constantFirst || !f.variableSelect.valid())
                return new DynamicInput::Constant(f.constant);
            auto r = f.variableSelect.reduce();
            if (r.isValid() && f.calibration.isIdentity() && f.path.empty() &&
                    !FP::defined(f.constantFirst))
                return new DynamicInput::Basic(std::move(r));
        }
    }
    return input.clone();
}

void DynamicInputOption::clear()
{
    optionSet = true;
    input.clear();
}

void DynamicInputOption::set(double constant)
{
    optionSet = true;
    input.set(constant);
}

void DynamicInputOption::set(const SequenceMatch::OrderedLookup &variableSelect,
                             const CPD3::Calibration &calibration,
                             double failoverConstant,
                             const std::string &path)
{
    optionSet = true;
    input.set(variableSelect, calibration, failoverConstant, path);
}

void DynamicInputOption::set(double start, double end, double constant, bool isFailover)
{
    optionSet = true;
    input.set(start, end, constant, isFailover);
}

void DynamicInputOption::set(double start,
                             double end,
                             const SequenceMatch::OrderedLookup &variableSelect,
                             const CPD3::Calibration &calibration,
                             double failoverConstant,
                             const std::string &path)
{
    optionSet = true;
    input.set(start, end, variableSelect, calibration, failoverConstant, path);
}

void DynamicInputOption::set(double start, double end, const Calibration &calibration)
{
    optionSet = true;
    input.set(start, end, calibration);
}

void DynamicInputOption::set(double start, double end, const Variant::Read &value)
{
    optionSet = true;
    input.set(start, end, value);
}

void DynamicInputOption::set(const Variant::Read &value)
{
    optionSet = true;
    input.set(value);
}


bool DynamicInputOption::parseFromValue(const Variant::Read &value)
{
    if (value.getType() == Variant::Type::Array) {
        for (auto v : value.toArray()) {
            set(v.hash("Start").toDouble(), v.hash("End").toDouble(), v);
        }
    } else {
        set(value);
    }
    return true;
}


DynamicInput *DynamicInput::Variable::reduceConfigurationSegments(const DynamicInput::Variable &icv,
                                                                  double start,
                                                                  double end)
{
    if (icv.segments.empty())
        return new DynamicInput::Constant();
    if (icv.segments.size() == 1) {
        const auto &f = icv.segments.front();
        if (Range::compareStart(f.start, start) <= 0 && Range::compareEnd(f.end, end) >= 0) {
            if (f.constantFirst || !f.variableSelect.valid())
                return new DynamicInput::Constant(f.constant);
            auto r = f.variableSelect.reduce();
            if (r.isValid() && f.calibration.isIdentity() && f.path.empty() &&
                    !FP::defined(f.constant))
                return new DynamicInput::Basic(std::move(r));
        }
    }
    return icv.clone();
}

DynamicInput *DynamicInput::fromConfiguration(SequenceSegment::Transfer &config,
                                              const SequenceName &unit,
                                              const QString &path,
                                              double start,
                                              double end,
                                              const SequenceName::Component &defaultStation,
                                              const SequenceName::Component &defaultArchive,
                                              const SequenceName::Component &defaultVariable)
{
    if (config.empty()) return new DynamicInput::Constant();

    DynamicInput::Variable icv;

    auto i = Range::findLowerBound(config.begin(), config.end(), start);
    if (i == config.end()) {
        --i;
        if (Range::compareStartEnd(start, i->getEnd()) < 0) {
            icv.set(i->getStart(), i->getEnd(), i->getValue(unit).getPath(path), defaultStation,
                    defaultArchive, defaultVariable);
        }
    } else if (i != config.begin() || Range::compareStartEnd(i->getStart(), end) < 0) {
        if (Range::compareStartEnd(start, i->getEnd()) >= 0)
            ++i;
        for (; i != config.end() && Range::compareStartEnd(i->getStart(), end) < 0; ++i) {

            auto configValue = i->getValue(unit).getPath(path);
            double st = i->getStart();
            if (configValue.exists()) {
                for (; i + 1 != config.end() &&
                        Range::compareStartEnd((i + 1)->getStart(), end) < 0 &&
                        Range::compareStartEnd((i + 1)->getStart(), i->getEnd()) == 0 &&
                        (i + 1)->getValue(unit).getPath(path) == configValue; ++i) { }
            } else {
                for (; i + 1 != config.end() &&
                        Range::compareStartEnd((i + 1)->getStart(), end) < 0 &&
                        Range::compareStartEnd((i + 1)->getStart(), i->getEnd()) == 0 &&
                        !(i + 1)->getValue(unit).getPath(path).exists(); ++i) { }
            }
            icv.set(st, i->getEnd(), configValue, defaultStation, defaultArchive, defaultVariable);
        }
    }
    return DynamicInput::Variable::reduceConfigurationSegments(icv, start, end);
}

DynamicInput *DynamicInput::fromConfiguration(const ValueSegment::Transfer &config,
                                              const QString &path,
                                              double start,
                                              double end,
                                              const SequenceName::Component &defaultStation,
                                              const SequenceName::Component &defaultArchive,
                                              const SequenceName::Component &defaultVariable)
{
    if (config.empty()) return new DynamicInput::Constant();

    DynamicInput::Variable icv;

    auto i = Range::findLowerBound(config.begin(), config.end(), start);
    if (i == config.end()) {
        --i;
        if (Range::compareStartEnd(start, i->getEnd()) < 0) {
            icv.set(i->getStart(), i->getEnd(), i->value().getPath(path), defaultStation,
                    defaultArchive, defaultVariable);
        }
    } else if (i != config.begin() || Range::compareStartEnd(i->getStart(), end) < 0) {
        if (Range::compareStartEnd(start, i->getEnd()) >= 0)
            ++i;
        for (; i != config.end() && Range::compareStartEnd(i->getStart(), end) < 0; ++i) {

            auto configValue = i->value().getPath(path);
            double st = i->getStart();
            for (; i + 1 != config.end() &&
                    Range::compareStartEnd((i + 1)->getStart(), end) < 0 &&
                    Range::compareStartEnd((i + 1)->getStart(), i->getEnd()) == 0 &&
                    (i + 1)->value().getPath(path) == configValue; ++i) { }
            icv.set(st, i->getEnd(), configValue, defaultStation, defaultArchive, defaultVariable);
        }
    }
    return DynamicInput::Variable::reduceConfigurationSegments(icv, start, end);
}


DynamicInput::~DynamicInput() = default;

double DynamicInput::get(SequenceSegment &in)
{ return get(in.getStart()); }

double DynamicInput::getConst(SequenceSegment &in) const
{ return getConst(in.getStart()); }

Variant::Read DynamicInput::getValue(SequenceSegment &in)
{ return Variant::Root(get(in.getStart())).read(); }

Variant::Read DynamicInput::getValue(const SequenceSegment &in)
{ return Variant::Root(get(in.getStart())).read(); }

Variant::Read DynamicInput::getValueConst(SequenceSegment &in) const
{ return Variant::Root(getConst(in.getStart())).read(); }

Variant::Read DynamicInput::getValueConst(const SequenceSegment &in) const
{ return Variant::Root(getConst(in.getStart())).read(); }

bool DynamicInput::registerInput(const SequenceName &)
{ return false; }

void DynamicInput::registerExpectedFlavorless(const SequenceName::Component &,
                                              const SequenceName::Component &,
                                              const SequenceName::Component &)
{ }

void DynamicInput::registerExpected(const SequenceName::Component &,
                                    const SequenceName::Component &,
                                    const SequenceName::Component &,
                                    const SequenceName::Flavors &)
{ }

void DynamicInput::registerExpected(const SequenceName::Component &,
                                    const SequenceName::Component &,
                                    const SequenceName::Component &,
                                    const std::unordered_set<SequenceName::Flavors> &)
{ }

SequenceName::Set DynamicInput::getUsedInputs() const
{ return SequenceName::Set(); }

QSet<double> DynamicInput::getChangedPoints() const
{ return QSet<double>(); }

QString DynamicInput::describe(double, double, const QString &) const
{ return QString(); }

double DynamicInput::constant(double, double) const
{ return FP::undefined(); }

void DynamicInput::printLog(QDebug &stream) const
{ stream << "DynamicInput(Unknown)"; }

QDataStream &operator<<(QDataStream &stream, const DynamicInput *tiv)
{
    if (!tiv) {
        auto t = static_cast<quint8>(TI_NULL);
        stream << t;
        return stream;
    }
    tiv->serialize(stream);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, DynamicInput *&tiv)
{
    quint8 tRaw;
    stream >> tRaw;
    switch (static_cast<TimeInputType>(tRaw)) {
    case TI_NULL:
        tiv = nullptr;
        return stream;
    case TI_CONSTANT:
        tiv = new DynamicInput::Constant(stream);
        return stream;
    case TI_VARIABLE:
        tiv = new DynamicInput::Basic(stream);
        return stream;
    case TI_CALIBRATE:
        tiv = new DynamicInput::Calibration(stream);
        return stream;
    case TI_VARIABLESELECTION:
        tiv = new DynamicInput::Variable(stream);
        return stream;
    }
    Q_ASSERT(false);
    tiv = nullptr;
    return stream;
}

QDebug operator<<(QDebug stream, const DynamicInput *tiv)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << reinterpret_cast<const void *>(tiv) << ":";
    if (tiv) {
        tiv->printLog(stream);
    } else {
        stream << "DynamicInput(NULL)";
    }
    return stream;
}

QDataStream &operator<<(QDataStream &stream, const std::unique_ptr<DynamicInput> &tiv)
{
    stream << tiv.get();
    return stream;
}

QDataStream &operator>>(QDataStream &stream, std::unique_ptr<DynamicInput> &tiv)
{
    DynamicInput *temp = nullptr;
    stream >> temp;
    tiv.reset(temp);
    return stream;
}

QDebug operator<<(QDebug stream, const std::unique_ptr<DynamicInput> &tiv)
{
    stream << tiv.get();
    return stream;
}

}
}
