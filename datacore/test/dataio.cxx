/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QByteArray>
#include <QBuffer>
#include <QString>
#include <QLocale>

#include "datacore/externalsource.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/dataioxml.hxx"
#include "datacore/stream.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Data;

Q_DECLARE_METATYPE(StandardDataOutput::OutputType);

class TestPathInpuXMLParser : public QXmlDefaultHandler {
public:
    Variant::Path path;
    XMLDataPathParser parser;
    bool inParsing;

    virtual QString errorString() const
    { return parser.errorString(); }

    virtual bool fatalError(const QXmlParseException &exception)
    {
        qWarning() << QString(
                "Error parsing input XML at line %1, column %2: %3").arg(exception.lineNumber())
                                                                    .arg(exception.columnNumber())
                                                                    .arg(exception.message());
        return false;
    }

    virtual bool startElement(const QString &namespaceURI,
                              const QString &localName,
                              const QString &qName,
                              const QXmlAttributes &atts)
    {
        if (!inParsing) {
            inParsing = true;
            return true;
        } else {
            return parser.startElement(namespaceURI, localName, qName, atts) ==
                    XMLDataPathParser::CONTINUE;
        }
    }

    virtual bool endElement(const QString &namespaceURI,
                            const QString &localName,
                            const QString &qName)
    {
        if (!inParsing)
            return false;
        XMLDataPathParser::ParseStatus st = parser.endElement(namespaceURI, localName, qName);
        if (st == XMLDataPathParser::FINISHED) {
            inParsing = false;
            return true;
        }
        return st == XMLDataPathParser::CONTINUE;
    }

    virtual bool characters(const QString &ch)
    {
        return parser.characters(ch) == XMLDataPathParser::CONTINUE;
    }

    TestPathInpuXMLParser() : path(), parser(&path), inParsing(false)
    { }
};

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestDataIO : public QObject {
Q_OBJECT
private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void direct()
    {
        SequenceValue::Transfer input;
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(), FP::undefined(),
                           123454.0, 1);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(123), 45435.0,
                           123454.0);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root("asdsd"), 45437.0,
                           1234545.0);

        auto data = std::make_shared<Util::ByteArray>();

        StandardDataOutput
                out(IO::Access::buffer(data)->stream(), StandardDataOutput::OutputType::Direct);
        out.setAccelerationKey("AK1");
        out.start();
        out.incomingData(input);
        out.endData();
        out.wait();
        QCoreApplication::processEvents();

        std::string accelerationKey;

        StreamSink::Buffer egress;
        StandardDataInput in;
        in.accelerationKey.connect([&](const std::string &key) { accelerationKey = key; });
        in.start();
        in.incomingData(*data);
        in.endData();
        in.setEgress(&egress);
        QVERIFY(in.wait(30000));
        QCOMPARE(accelerationKey, std::string("AK1"));

        QCOMPARE(egress.values(), input);
    }

    void raw()
    {
        SequenceValue::Transfer input;
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(), FP::undefined(),
                           123454.0, 1);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(123), 45435.0,
                           123454.0);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root("asdsd"), 45437.0,
                           1234545.0);

        auto data = std::make_shared<Util::ByteArray>();

        StandardDataOutput
                out(IO::Access::buffer(data)->stream(), StandardDataOutput::OutputType::Raw);
        out.incomingData(input);
        out.endData();
        out.start();
        out.wait();

        StreamSink::Buffer egress;
        StandardDataInput in;
        in.start();
        in.incomingData(std::move(*data));
        in.endData();
        in.setEgress(&egress);
        QVERIFY(in.wait(30000));

        QCOMPARE(egress.values(), input);
    }

    void directLegacy()
    {
        SequenceValue::Transfer input;
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(), FP::undefined(),
                           123454.0, 1);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(123), 45435.0,
                           123454.0);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root("asdsd"), 45437.0,
                           1234545.0);

        auto data = std::make_shared<Util::ByteArray>();

        StandardDataOutput out
                (IO::Access::buffer(data)->stream(), StandardDataOutput::OutputType::Direct_Legacy);
        out.setAccelerationKey("AK1");
        out.start();
        out.incomingData(input);
        out.endData();
        out.wait();

        std::mutex mutex;
        std::condition_variable cv;
        std::string accelerationKey;

        StreamSink::Buffer egress;
        StandardDataInput in;
        in.accelerationKey.connect([&](const std::string &key) {
            std::lock_guard<std::mutex> lock(mutex);
            accelerationKey = key;
            cv.notify_all();
        });
        in.start();
        in.incomingData(data->toQByteArrayRef());
        in.setEgress(&egress);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return !accelerationKey.empty(); });
            QCOMPARE(accelerationKey, std::string("AK1"));
        }
        in.endData();
        QVERIFY(in.wait(30000));
        QCOMPARE(accelerationKey, std::string("AK1"));

        QCOMPARE(egress.values(), input);
    }

    void rawLegacy()
    {
        SequenceValue::Transfer input;
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(), FP::undefined(),
                           123454.0, 1);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(123), 45435.0,
                           123454.0);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root("asdsd"), 45437.0,
                           1234545.0);

        auto data = std::make_shared<Util::ByteArray>();

        StandardDataOutput
                out(IO::Access::buffer(data)->stream(), StandardDataOutput::OutputType::Raw_Legacy);
        out.start();
        out.incomingData(input);
        out.endData();
        out.wait();

        StreamSink::Buffer egress;
        StandardDataInput in;
        in.start();
        in.incomingData(data->toQByteArray());
        in.endData();
        in.setEgress(&egress);
        QVERIFY(in.wait(30000));

        QCOMPARE(egress.values(), input);
    }

    void xml()
    {
        SequenceValue::Transfer input;
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(), FP::undefined(),
                           123454.0, 1);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(123), 45435.0,
                           123454.0);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root("asdsd"), 45437.0,
                           12345.0);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(true), 45437.0,
                           12345.0);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(false), 45437.0,
                           12345.0);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"),
                           Variant::Root(Variant::Bytes("asda")), 45437.0, 12345.0);
        Variant::Root v;
        v.write().setInt64(156);
        input.emplace_back(SequenceName("brw", "raw", "T_S12"), v, 45437.0, 12345.0);
        v.write().setFlags({"f1", "f2"});
        input.emplace_back(SequenceName("brw", "raw", "T_S12"), v, 45437.0, 12345.0);
        v.write().hash("blag").setString("baz");
        v.write().hash("blag2").setString("baz2");
        input.emplace_back(SequenceName("brw", "raw", "T_S12"), v, 45437.0, 12345.0);
        v.write().array(0).hash("baz").setDouble(2.0);
        v.write().array(1).setInt64(-123);
        input.emplace_back(SequenceName("alt", "raw", "T_S13"), v, 45437.0, 12345.0);
        v.write().setString("abcd");
        v.write().setDisplayString(QLocale().name(), "efgh");
        input.emplace_back(SequenceName("alt", "raw", "F2_S11"), v, 45437.0, 12345.0);
        v.write().hash("Value1").setBool(true);
        v.write().hash("Value2").setBool(false);
        input.emplace_back(SequenceName("alt", "raw", "ZState_S11"), v, 45437.0, 12345.0);
        v.write().setType(Variant::Type::Real);
        v.write().setDouble(FP::undefined());
        input.emplace_back(SequenceName("alt", "raw", "Tu_S11"), v, 45437.0, 12345.0);
        v.write().setType(Variant::Type::Matrix);
        v.write().matrix({1, 3}).setDouble(1.0);
        input.emplace_back(SequenceName("alt", "raw", "N_S11"), v, 45437.0, 12345.0);

        v.write().keyframe(0).setDouble(1.0);
        v.write().keyframe(0.5).setDouble(99.9);
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45437.0, 12345.0);
        v.write().metadataReal("kv").setString("vl");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45437.0, 12345.0);
        v.write().metadataBoolean("kv").setString("vl");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45437.0, 12345.0);
        v.write().metadataString("kv").setString("vl");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45437.0, 12345.0);
        v.write().metadataBytes("kv").setString("vl");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45437.0, 12345.0);
        v.write().metadataInteger("kv").setString("vl");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45437.0, 12345.0);
        v.write().metadataArray("kv").setString("vl");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45437.0, 12345.0);
        v.write().metadataKeyframe("kv").setString("vl");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45439.0, 12345.0);
        v.write().metadataHash("mchild").setString("blarg");
        v.write().metadataHashChild("mvalue").setString("bla2rg");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45439.0, 12345.0);
        v.write().metadataFlags("mchild").setString("blarg");
        v.write().metadataSingleFlag("mvalue").setString("bla2rg");
        v.write().metadataSingleFlag("mvalue").setDisplayString("ko", "badvalue");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45439.0, 12345.0);

        QByteArray data;

        QBuffer outBuffer(&data);
        outBuffer.open(QIODevice::WriteOnly);
        StandardDataOutput out(&outBuffer, StandardDataOutput::OutputType::XML);
        out.incomingData(input);
        out.endData();
        out.start();
        Threading::pollInEventLoop([&] { return !out.isFinished(); });
        out.wait();

        StreamSink::Buffer egress;
        StandardDataInput in;
        in.start();
        in.incomingData(data);
        in.endData();
        in.setEgress(&egress);
        QVERIFY(in.wait(30000));

        QCOMPARE(egress.values(), input);

        {
            StreamSink::Buffer egress2;
            StandardDataInput in2;
            in2.start();
            int idxTag = data.mid(50).indexOf('"') + 50;
            in2.setEgress(&egress2);
            in2.incomingData(data.mid(0, idxTag));
            QTest::qSleep(50);
            in2.incomingData(data.mid(idxTag));
            in2.endData();
            QVERIFY(in2.wait(30000));

            QCOMPARE(egress2.values().size(), input.size());
            QCOMPARE(egress2.values(), input);
        }
    }

    void json()
    {
        SequenceValue::Transfer input;
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(), FP::undefined(),
                           123454.0, 1);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(123), 45435.0,
                           123454.0);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root("asdsd"), 45437.0,
                           12345.0);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(true), 45437.0,
                           12345.0);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(false), 45437.0,
                           12345.0);
        input.emplace_back(SequenceName("brw", "raw", "T_S11"),
                           Variant::Root(Variant::Bytes("asda")), 45437.0, 12345.0);
        Variant::Root v;
        v.write().setInt64(156);
        input.emplace_back(SequenceName("brw", "raw", "T_S12"), v, 45437.0, 12345.0);
        v.write().setFlags({"f1", "f2"});
        input.emplace_back(SequenceName("brw", "raw", "T_S12"), v, 45437.0, 12345.0);
        v.write().hash("blag").setString("baz");
        v.write().hash("blag2").setString("baz2");
        input.emplace_back(SequenceName("brw", "raw", "T_S12"), v, 45437.0, 12345.0);
        v.write().array(0).hash("baz").setDouble(2.0);
        v.write().array(1).setInt64(-123);
        input.emplace_back(SequenceName("alt", "raw", "T_S13"), v, 45437.0, 12345.0);
        v.write().setString("abcd");
        v.write().setDisplayString(QLocale().name(), "efgh");
        input.emplace_back(SequenceName("alt", "raw", "F2_S11"), v, 45437.0, 12345.0);
        v.write().hash("Value1").setBool(true);
        v.write().hash("Value2").setBool(false);
        input.emplace_back(SequenceName("alt", "raw", "ZState_S11"), v, 45437.0, 12345.0);
        v.write().setType(Variant::Type::Real);
        v.write().setDouble(FP::undefined());
        input.emplace_back(SequenceName("alt", "raw", "Tu_S11"), v, 45437.0, 12345.0);
        v.write().setType(Variant::Type::Matrix);
        v.write().matrix({1, 0}).setDouble(3.0);
        v.write().matrix({0, 1}).setDouble(2.0);
        v.write().matrix({1, 3}).setDouble(1.0);
        input.emplace_back(SequenceName("alt", "raw", "N_S11"), v, 45437.0, 12345.0);

        v.write().keyframe(0).setDouble(1.0);
        v.write().keyframe(0.5).setDouble(99.9);
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45437.0, 12345.0);
        v.write().metadataReal("kv").setString("vl");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45437.0, 12345.0);
        v.write().metadataBoolean("kv").setString("vl");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45437.0, 12345.0);
        v.write().metadataString("kv").setString("vl");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45437.0, 12345.0);
        v.write().metadataBytes("kv").setString("vl");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45437.0, 12345.0);
        v.write().metadataInteger("kv").setString("vl");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45437.0, 12345.0);
        v.write().metadataArray("kv").setString("vl");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45437.0, 12345.0);
        v.write().metadataKeyframe("kv").setString("vl");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45439.0, 12345.0);
        v.write().metadataHash("mchild").setString("blarg");
        v.write().metadataHashChild("mvalue").setString("bla2rg");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45439.0, 12345.0);
        v.write().metadataFlags("mchild").setString("blarg");
        v.write().metadataSingleFlag("mvalue").setString("bla2rg");
        v.write().metadataSingleFlag("mvalue").setDisplayString("ko", "bad\n\nvalue");
        input.emplace_back(SequenceName("alt", "raw_meta", "T_S13"), v, 45439.0, 12345.0);

        auto data = std::make_shared<Util::ByteArray>();
        StandardDataOutput
                out(IO::Access::buffer(data)->stream(), StandardDataOutput::OutputType::JSON);
        out.start();
        out.incomingData(input);
        out.endData();
        out.wait();

        StreamSink::Buffer egress;
        StandardDataInput in;
        in.start();
        in.incomingData(*data);
        in.endData();
        in.setEgress(&egress);
        QVERIFY(in.wait(30000));

        QCOMPARE(egress.values(), input);
    }

    void jsonInference()
    {
        QByteArray input("[\n"
                         "{ \"station\": \"nil\", \"archive\": \"raw\", \"variable\": \"T_S11\", \n"
                         " \"value\": { \"v1\": 1.25, \"v2\": [ \"str1\", \"str2\" ] } \n"
                         "} \n"
                         "]");

        Variant::Root v;
        v.write().hash("v1").setDouble(1.25);
        v.write().hash("v2").array(0).setString("str1");
        v.write().hash("v2").array(1).setString("str2");

        SequenceValue::Transfer expected;
        expected.emplace_back(SequenceName("nil", "raw", "T_S11"), v, FP::undefined(),
                              FP::undefined());

        StreamSink::Buffer egress;
        StandardDataInput in;
        in.start();
        in.incomingData(input);
        in.endData();
        in.setEgress(&egress);
        QVERIFY(in.wait(30000));

        QCOMPARE(egress.values(), expected);
    }

    void path()
    {
        Variant::Read testPath;
        testPath = testPath.hash("ph").array(1).matrix({0, 1}).keyframe(3.0).metadataHash("blarg");
        Variant::Path lp(testPath.currentPath());
        QCOMPARE(lp, testPath.currentPath());

        QByteArray data;

        {
            QBuffer outBuffer(&data);
            outBuffer.open(QIODevice::WriteOnly);
            QXmlStreamWriter *xml = new QXmlStreamWriter(&outBuffer);
            xml->writeStartDocument();
            xml->writeDTD("<!DOCTYPE cpd3path>");
            xml->writeStartElement("cpd3path");
            XMLDataOutput::writePath(xml, lp);
            xml->writeEndDocument();
            delete xml;
        }

        {
            QXmlSimpleReader xml;
            QXmlInputSource xmlData;
            TestPathInpuXMLParser handler;
            xml.setContentHandler(&handler);
            xml.setErrorHandler(&handler);

            xmlData.setData(data);
            QVERIFY(xml.parse(&xmlData, true));
            xml.parseContinue();
            lp = handler.path;
        }

        QCOMPARE(lp, testPath.currentPath());
    }

    void bulk()
    {
        QFETCH(StandardDataOutput::OutputType, type);
        QFETCH(int, nTimes);
        QFETCH(int, nUnits);
        QFETCH(int, nDuplicate);

        SequenceValue::Transfer input;
        input.emplace_back(SequenceName("brw", "raw_meta", "T_S11"), Variant::Root(123),
                           1388880000.0, FP::undefined());
        for (int t = 0; t < nTimes; t++) {
            double startTime = 1388880000 + (int) (t / nDuplicate) * 60.0;
            double endTime = startTime + 60.0;
            for (int u = 0; u < nUnits; u++) {
                Variant::Root v;
                if ((t + u) % 30 != 0) {
                    v.write().setDouble(t + u / 20.0);
                }
                input.emplace_back(SequenceName("station" + std::to_string(u / 4 + 1),
                                                "archive" + std::to_string(u / 2 + 1),
                                                "variable" + std::to_string(u + 1)), std::move(v),
                                   startTime, endTime);
            }
        }

        auto data = std::make_shared<Util::ByteArray>();
        StandardDataOutput out(IO::Access::buffer(data)->stream(), type);
        out.start();
        out.incomingData(input);
        out.endData();
        out.wait();

        StreamSink::Buffer egress;
        StandardDataInput in;
        in.start();
        in.incomingData(std::move(*data));
        in.endData();
        in.setEgress(&egress);
        QVERIFY(in.wait(120000));

        QCOMPARE(egress.values(), input);
    }

    void bulk_data()
    {
        QTest::addColumn<StandardDataOutput::OutputType>("type");
        QTest::addColumn<int>("nTimes");
        QTest::addColumn<int>("nUnits");
        QTest::addColumn<int>("nDuplicate");

        QTest::newRow("Direct Times") << StandardDataOutput::OutputType::Direct << 5000 << 20 << 1;
        QTest::newRow("Raw Times") << StandardDataOutput::OutputType::Raw << 5000 << 20 << 1;

        QTest::newRow("Direct Units") << StandardDataOutput::OutputType::Direct << 2 << 0x20000
                                      << 1;
        QTest::newRow("Raw Units") << StandardDataOutput::OutputType::Raw << 2 << 0x20000 << 1;

        QTest::newRow("Direct Packet") << StandardDataOutput::OutputType::Direct << 5000 << 20
                                       << 50;
        QTest::newRow("Raw Packet") << StandardDataOutput::OutputType::Raw << 5000 << 20 << 50;
    }

    void rawOverflow()
    {
        Variant::Root huge;
        for (int i = 0; i < 0xFFFF / 4; i++) {
            huge.write().array(i).setInt64(i);
        }

        SequenceValue::Transfer input;
        input.emplace_back(SequenceName("brw", "raw_meta", "T_S11"), huge, 100, 200);
        input.emplace_back(SequenceName("brw", "raw_meta", "T_S11"), huge, 200, 300);

        auto data = std::make_shared<Util::ByteArray>();
        StandardDataOutput
                out(IO::Access::buffer(data)->stream(), StandardDataOutput::OutputType::Raw);
        out.start();
        out.incomingData(input);
        out.endData();
        out.wait();

        StreamSink::Buffer egress;
        StandardDataInput in;
        in.start();
        in.incomingData(std::move(*data));
        in.endData();
        in.setEgress(&egress);
        QVERIFY(in.wait(30000));

        QCOMPARE(egress.values(), input);
    }
};

QTEST_MAIN(TestDataIO)

#include "dataio.moc"
