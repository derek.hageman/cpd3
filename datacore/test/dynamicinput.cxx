/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtSql>

#include "datacore/dynamicinput.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/variant/composite.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Data;

Q_DECLARE_METATYPE(QList<double>);

#define ICOMPARE(actual, expected) do { \
    double av = actual; double ev = expected; \
    if (FP::defined(ev)) \
        QCOMPARE(av, ev); \
    else \
        QVERIFY(!FP::defined(av)); \
} while (0)

namespace QTest {
template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestDynamicInput : public QObject {
Q_OBJECT
private slots:

    void inputConstant()
    {
        DynamicInput *i = new DynamicInput::Constant(12.0);
        QCOMPARE(i->get(1234.0), 12.0);
        QCOMPARE(i->getConst(1234.0), 12.0);
        SequenceSegment in;
        QCOMPARE(i->get(in), 12.0);
        QCOMPARE(i->getConst(in), 12.0);
        DynamicInput *j = i->clone();
        delete i;
        QCOMPARE(j->get(1234.0), 12.0);
        DynamicInput *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->get(1234.0), 12.0);
        delete k;
    }

    void wrapCalibration()
    {
        DynamicInput *i = new DynamicInput::Calibration(new DynamicInput::Constant(12.0),
                                                        Calibration(QList<double>() << 1.0 << 0.5));
        QCOMPARE(i->get(1234.0), 7.0);
        QCOMPARE(i->getConst(1234.0), 7.0);
        SequenceSegment in;
        QCOMPARE(i->get(in), 7.0);
        QCOMPARE(i->getConst(in), 7.0);
        DynamicInput *j = i->clone();
        delete i;
        QCOMPARE(j->get(1234.0), 7.0);
        DynamicInput *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->get(1234.0), 7.0);
        delete k;
    }

    void inputVariable()
    {
        SequenceName u("brw", "clean", "BaB_A11");
        DynamicInput *i = new DynamicInput::Basic(u);
        QVERIFY(!FP::defined(i->get(1234.0)));
        QVERIFY(!FP::defined(i->getConst(1234.0)));
        SequenceSegment in;
        in.setValue(u, Variant::Root(12.0));
        QCOMPARE(i->get(in), 12.0);
        QCOMPARE(i->getConst(in), 12.0);
        DynamicInput *j = i->clone();
        delete i;
        QVERIFY(!FP::defined(j->get(1234.0)));
        QCOMPARE(j->get(in), 12.0);
        DynamicInput *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->get(in), 12.0);
        delete k;
    }

    void segmentedInput()
    {
        QFETCH(ValueSegment::Transfer, configSingle);
        QFETCH(QList<double>, expectedTime);
        QFETCH(QList<double>, expectedData);

        QList<double> inputTimes;
        inputTimes << 1.0 << 2.0 << 4.0 << 8.0;

        SequenceName u("brw", "clean", "BaB_A11");
        SequenceSegment::Transfer dmus;

        SequenceSegment dms;
        dms.setStart(1.0);
        dms.setEnd(2.0);
        dms.setValue(u, Variant::Root(1.0));
        dmus.push_back(dms);
        dms.setStart(2.0);
        dms.setEnd(3.0);
        dms.setValue(u, Variant::Root(2.0));
        dmus.push_back(dms);
        dms.setStart(4.0);
        dms.setEnd(5.0);
        dms.setValue(u, Variant::Root(3.0));
        dmus.push_back(dms);
        dms.setStart(5.0);
        dms.setEnd(7.0);
        dms.setValue(u, Variant::Root());
        dmus.push_back(dms);

        QSet<double> expectedChanged;
        for (int i = 0, max = configSingle.size(); i < max; i++) {
            ValueSegment seg(configSingle.at(i));
            if (FP::defined(seg.getStart())) {
                if (i == 0 ||
                        seg.getValue() != configSingle.at(i - 1).getValue() ||
                        seg.getStart() != configSingle.at(i - 1).getEnd())
                    expectedChanged |= seg.getStart();
            }
            if (FP::defined(seg.getEnd())) {
                if (i == max - 1 ||
                        seg.getValue() != configSingle.at(i + 1).getValue() ||
                        seg.getEnd() != configSingle.at(i + 1).getStart())
                    expectedChanged |= seg.getEnd();
            }
        }


        DynamicInput *i = DynamicInput::fromConfiguration(configSingle);
        i->registerInput(u);
        QCOMPARE(i->getChangedPoints(), expectedChanged);
        for (int idx = 0; idx < 4; idx++) {
            ICOMPARE(i->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(i->getConst(dmus[idx]), expectedData[idx]);

            DynamicInput *j = i->clone();
            DynamicInput *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            ICOMPARE(j->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(j->getConst(dmus[idx]), expectedData[idx]);
            ICOMPARE(j->get(dmus[idx]), expectedData[idx]);
            ICOMPARE(j->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->getConst(dmus[idx]), expectedData[idx]);
            ICOMPARE(k->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->get(dmus[idx]), expectedData[idx]);

            for (int idx2 = idx + 1; idx2 < 4; idx2++) {
                ICOMPARE(j->getConst(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(j->getConst(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(j->get(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(j->get(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->getConst(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->getConst(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(k->get(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->get(dmus[idx2]), expectedData[idx2]);
            }

            ICOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(i->get(dmus[idx]), expectedData[idx]);
            ICOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);

            delete j;
            delete k;
        }
        delete i;

        SequenceSegment::Transfer configMulti;
        SequenceName cmName("brw", "editing", "input");
        for (int i = 0, max = configSingle.size(); i < max; i++) {
            SequenceSegment ms;
            ms.setStart(configSingle.at(i).getStart());
            ms.setEnd(configSingle.at(i).getEnd());
            ms.setValue(cmName, configSingle.at(i).root());
            configMulti.emplace_back(std::move(ms));
        }
        i = DynamicInput::fromConfiguration(configMulti, cmName);
        i->registerInput(u);
        QCOMPARE(i->getChangedPoints(), expectedChanged);
        for (int idx = 0; idx < 4; idx++) {
            ICOMPARE(i->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(i->getConst(dmus[idx]), expectedData[idx]);

            DynamicInput *j = i->clone();
            DynamicInput *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            ICOMPARE(j->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(j->getConst(dmus[idx]), expectedData[idx]);
            ICOMPARE(j->get(dmus[idx]), expectedData[idx]);
            ICOMPARE(j->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->getConst(dmus[idx]), expectedData[idx]);
            ICOMPARE(k->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->get(dmus[idx]), expectedData[idx]);

            for (int idx2 = idx + 1; idx2 < 4; idx2++) {
                ICOMPARE(j->getConst(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(j->getConst(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(j->get(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(j->get(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->getConst(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->getConst(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(k->get(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->get(dmus[idx2]), expectedData[idx2]);
            }

            ICOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(i->get(dmus[idx]), expectedData[idx]);
            ICOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);

            delete j;
            delete k;
        }
        delete i;


        expectedChanged.clear();
        for (const auto &seg : configSingle) {
            if (FP::defined(seg.getStart()))
                expectedChanged |= seg.getStart();
            if (FP::defined(seg.getEnd()))
                expectedChanged |= seg.getEnd();
        }


        DynamicInputOption si("", "", "", "");
        for (int i = 0, max = configSingle.size(); i < max; i++) {
            ValueSegment cs(configSingle.at(i));
            auto cv = cs.read();
            if (cv.getType() == Variant::Type::Real) {
                si.set(cs.getStart(), cs.getEnd(), cv.toDouble());
            } else {
                if (cv["/Constant"].exists()) {
                    si.set(cs.getStart(), cs.getEnd(), cv["/Constant"].toDouble());
                } else {
                    si.set(cs.getStart(), cs.getEnd(), SequenceMatch::OrderedLookup(cv["/Input"]),
                           Variant::Composite::toCalibration(cv["/Calibration"]),
                           cv["/Default"].toDouble());
                }
            }
        }
        i = si.getInput();
        i->registerInput(u);
        QCOMPARE(i->getChangedPoints(), expectedChanged);
        for (int idx = 0; idx < 4; idx++) {
            ICOMPARE(i->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(i->getConst(dmus[idx]), expectedData[idx]);

            DynamicInput *j = i->clone();
            DynamicInput *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            ICOMPARE(j->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(j->getConst(dmus[idx]), expectedData[idx]);
            ICOMPARE(j->get(dmus[idx]), expectedData[idx]);
            ICOMPARE(j->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->getConst(dmus[idx]), expectedData[idx]);
            ICOMPARE(k->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->get(dmus[idx]), expectedData[idx]);

            for (int idx2 = idx + 1; idx2 < 4; idx2++) {
                ICOMPARE(j->getConst(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(j->getConst(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(j->get(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(j->get(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->getConst(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->getConst(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(k->get(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->get(dmus[idx2]), expectedData[idx2]);
            }

            ICOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(i->get(dmus[idx]), expectedData[idx]);
            ICOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);

            delete j;
            delete k;
        }
        delete i;

        DynamicInputOption *si2 = (DynamicInputOption *) si.clone();
        i = si2->getInput();
        delete si2;
        i->registerInput(u);
        QCOMPARE(i->getChangedPoints(), expectedChanged);
        for (int idx = 0; idx < 4; idx++) {
            ICOMPARE(i->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(i->getConst(dmus[idx]), expectedData[idx]);

            DynamicInput *j = i->clone();
            DynamicInput *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            ICOMPARE(j->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(j->getConst(dmus[idx]), expectedData[idx]);
            ICOMPARE(j->get(dmus[idx]), expectedData[idx]);
            ICOMPARE(j->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->getConst(dmus[idx]), expectedData[idx]);
            ICOMPARE(k->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->get(dmus[idx]), expectedData[idx]);

            for (int idx2 = idx + 1; idx2 < 4; idx2++) {
                ICOMPARE(j->getConst(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(j->getConst(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(j->get(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(j->get(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->getConst(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->getConst(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(k->get(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->get(dmus[idx2]), expectedData[idx2]);
            }

            ICOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(i->get(dmus[idx]), expectedData[idx]);
            ICOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);

            delete j;
            delete k;
        }
        delete i;

        DynamicInput::Variable *ti = new DynamicInput::Variable();
        for (int i = 0, max = configSingle.size(); i < max; i++) {
            ValueSegment cs(configSingle.at(i));
            auto cv = cs.read();
            if (cv.getType() == Variant::Type::Real) {
                ti->set(cs.getStart(), cs.getEnd(), cv.toDouble());
            } else {
                if (cv["/Constant"].exists()) {
                    ti->set(cs.getStart(), cs.getEnd(), cv["/Constant"].toDouble());
                } else {
                    ti->set(cs.getStart(), cs.getEnd(), SequenceMatch::OrderedLookup(cv["/Input"]),
                            Variant::Composite::toCalibration(cv["/Calibration"]),
                            cv["/Default"].toDouble());
                }
            }
        }
        i = ti;
        i->registerInput(u);
        QCOMPARE(i->getChangedPoints(), expectedChanged);
        for (int idx = 0; idx < 4; idx++) {
            ICOMPARE(i->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(i->getConst(dmus[idx]), expectedData[idx]);

            DynamicInput *j = i->clone();
            DynamicInput *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            ICOMPARE(j->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(j->getConst(dmus[idx]), expectedData[idx]);
            ICOMPARE(j->get(dmus[idx]), expectedData[idx]);
            ICOMPARE(j->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->getConst(dmus[idx]), expectedData[idx]);
            ICOMPARE(k->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->get(dmus[idx]), expectedData[idx]);

            for (int idx2 = idx + 1; idx2 < 4; idx2++) {
                ICOMPARE(j->getConst(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(j->getConst(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(j->get(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(j->get(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->getConst(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->getConst(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(k->get(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->get(dmus[idx2]), expectedData[idx2]);
            }

            ICOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(i->get(dmus[idx]), expectedData[idx]);
            ICOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);

            delete j;
            delete k;
        }
        delete i;

        ti = new DynamicInput::Variable();
        for (int i = 0, max = configSingle.size(); i < max; i++) {
            ti->set(configSingle.at(i).getStart(), configSingle.at(i).getEnd(),
                    configSingle.at(i).getValue());
        }
        i = ti;
        i->registerInput(u);
        QCOMPARE(i->getChangedPoints(), expectedChanged);
        for (int idx = 0; idx < 4; idx++) {
            ICOMPARE(i->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(i->getConst(dmus[idx]), expectedData[idx]);

            DynamicInput *j = i->clone();
            DynamicInput *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            ICOMPARE(j->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(j->getConst(dmus[idx]), expectedData[idx]);
            ICOMPARE(j->get(dmus[idx]), expectedData[idx]);
            ICOMPARE(j->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->getConst(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->getConst(dmus[idx]), expectedData[idx]);
            ICOMPARE(k->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(k->get(dmus[idx]), expectedData[idx]);

            for (int idx2 = idx + 1; idx2 < 4; idx2++) {
                ICOMPARE(j->getConst(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(j->getConst(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(j->get(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(j->get(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->getConst(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->getConst(dmus[idx2]), expectedData[idx2]);
                ICOMPARE(k->get(inputTimes[idx2]), expectedTime[idx2]);
                ICOMPARE(k->get(dmus[idx2]), expectedData[idx2]);
            }

            ICOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            ICOMPARE(i->get(dmus[idx]), expectedData[idx]);
            ICOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);

            delete j;
            delete k;
        }
        delete i;
    }

    void segmentedInput_data()
    {
        QTest::addColumn<ValueSegment::Transfer>("configSingle");
        QTest::addColumn<QList<double> >("expectedTime");
        QTest::addColumn<QList<double> >("expectedData");

        ValueSegment::Transfer cs;
        QList<double> et;
        QList<double> ed;
        SequenceName mt("brw", "editing", "input");
        SequenceSegment ms;
        Variant::Root v;

        for (int i = 0; i < 4; i++) {
            et << FP::undefined();
            ed << FP::undefined();
        }

        QTest::newRow("Empty") << cs << et << ed;

        v.write().setDouble(1.0);
        for (int i = 0; i < 4; i++) {
            et[i] = 1.0;
            ed[i] = 1.0;
        }
        cs.emplace_back(FP::undefined(), FP::undefined(), v);
        QTest::newRow("Constant direct") << cs << et << ed;

        cs.clear();
        v.write().setEmpty();
        v["/Constant"].setDouble(1.0);
        cs.emplace_back(FP::undefined(), FP::undefined(), v);
        QTest::newRow("Constant explicit") << cs << et << ed;

        cs.clear();
        v.write().setEmpty();
        v["/Default"].setDouble(1.0);
        cs.emplace_back(FP::undefined(), FP::undefined(), v);
        QTest::newRow("Constant default") << cs << et << ed;

        cs.clear();
        v["/Input"].setString("BaB_A11");
        ed[0] = 1.0;
        ed[1] = 2.0;
        ed[2] = 3.0;
        cs.emplace_back(FP::undefined(), FP::undefined(), v);
        QTest::newRow("Input single default") << cs << et << ed;

        cs.clear();
        v.write().setEmpty();
        v["/Input"].setString("brw:clean:BaB_A11");
        ed[3] = FP::undefined();
        for (int i = 0; i < 4; i++) { et[i] = FP::undefined(); }
        cs.emplace_back(FP::undefined(), FP::undefined(), v);
        QTest::newRow("Input single") << cs << et << ed;

        cs.clear();
        cs.emplace_back(FP::undefined(), 1.0, v);
        cs.emplace_back(1.0, 2.0, v);
        cs.emplace_back(3.0, 10.0, v);
        ed[1] = FP::undefined();
        QTest::newRow("Input combine") << cs << et << ed;

        cs.clear();
        v.write().setEmpty();
        v["/Input"].setString("BaB_A11");
        cs.emplace_back(FP::undefined(), 4.0, v);
        v.write().setEmpty();
        v["/Constant"].setDouble(1.0);
        cs.emplace_back(4.0, FP::undefined(), v);
        ed[1] = 2.0;
        ed[2] = 1.0;
        ed[3] = 1.0;
        et[2] = 1.0;
        et[3] = 1.0;
        QTest::newRow("Input two") << cs << et << ed;

        cs.pop_back();
        cs.emplace_back(4.0, 5.0, v);
        cs.emplace_back(5.0, 6.0, v);
        cs.emplace_back(6.0, 10.0, v);
        QTest::newRow("Input two combine") << cs << et << ed;

        cs.clear();
        v.write().setEmpty();
        v["/Constant"].setDouble(1.0);
        cs.emplace_back(FP::undefined(), 3.0, v);
        v.write().setEmpty();
        v["/Constant"].setDouble(2.0);
        cs.emplace_back(5.0, FP::undefined(), v);
        ed[0] = 1.0;
        ed[1] = 1.0;
        ed[2] = FP::undefined();
        ed[3] = 2.0;
        et[0] = 1.0;
        et[1] = 1.0;
        et[2] = FP::undefined();
        et[3] = 2.0;
        QTest::newRow("Input gap") << cs << et << ed;
    }

    void configurationInputSpecial()
    {
        ValueSegment::Transfer configSingle;
        SequenceSegment::Transfer configMulti;
        SequenceName cmName("brw", "editing", "input");
        SequenceSegment dms;
        DynamicInput *i;

        configSingle.emplace_back(3.0, 4.0, Variant::Root(1.0));
        configSingle.emplace_back(5.0, 6.0, Variant::Root(2.0));
        dms.setStart(3.0);
        dms.setEnd(4.0);
        dms.setValue(cmName, Variant::Root(1.0));
        configMulti.push_back(dms);
        dms.setStart(5.0);
        dms.setEnd(6.0);
        dms.setValue(cmName, Variant::Root(2.0));
        configMulti.push_back(dms);

        i = DynamicInput::fromConfiguration(configSingle, "/", 2.0, FP::undefined());
        QCOMPARE(i->getChangedPoints(), QSet<double>() << 3.0 << 4.0 << 5.0 << 6.0);
        QCOMPARE(i->get(3.5), 1.0);
        QVERIFY(!FP::defined(i->get(4.5)));
        QCOMPARE(i->get(5.5), 2.0);
        delete i;
        i = DynamicInput::fromConfiguration(configMulti, cmName, "/", 2.0, FP::undefined());
        QCOMPARE(i->getChangedPoints(), QSet<double>() << 3.0 << 4.0 << 5.0 << 6.0);
        QCOMPARE(i->get(3.5), 1.0);
        QVERIFY(!FP::defined(i->get(4.5)));
        QCOMPARE(i->get(5.5), 2.0);
        delete i;

        i = DynamicInput::fromConfiguration(configSingle, "/", 2.0, 4.5);
        QCOMPARE(i->getChangedPoints(), QSet<double>() << 3.0 << 4.0);
        QCOMPARE(i->get(3.5), 1.0);
        QVERIFY(!FP::defined(i->get(4.5)));
        QVERIFY(!FP::defined(i->get(5.5)));
        delete i;
        i = DynamicInput::fromConfiguration(configMulti, cmName, "/", 2.0, 4.5);
        QCOMPARE(i->getChangedPoints(), QSet<double>() << 3.0 << 4.0);
        QCOMPARE(i->get(3.5), 1.0);
        QVERIFY(!FP::defined(i->get(4.5)));
        QVERIFY(!FP::defined(i->get(5.5)));
        delete i;

        i = DynamicInput::fromConfiguration(configSingle, "/", 4.5, FP::undefined());
        QCOMPARE(i->getChangedPoints(), QSet<double>() << 5.0 << 6.0);
        QVERIFY(!FP::defined(i->get(3.5)));
        QVERIFY(!FP::defined(i->get(4.5)));
        QCOMPARE(i->get(5.5), 2.0);
        delete i;
        i = DynamicInput::fromConfiguration(configMulti, cmName, "/", 4.5, FP::undefined());
        QCOMPARE(i->getChangedPoints(), QSet<double>() << 5.0 << 6.0);
        QVERIFY(!FP::defined(i->get(3.5)));
        QVERIFY(!FP::defined(i->get(4.5)));
        QCOMPARE(i->get(5.5), 2.0);
        delete i;

        i = DynamicInput::fromConfiguration(configSingle, "/", 7.0, FP::undefined());
        QCOMPARE(i->getChangedPoints(), QSet<double>());
        QVERIFY(!FP::defined(i->get(3.5)));
        QVERIFY(!FP::defined(i->get(4.5)));
        QVERIFY(!FP::defined(i->get(5.5)));
        delete i;
        i = DynamicInput::fromConfiguration(configMulti, cmName, "/", 7.0, FP::undefined());
        QCOMPARE(i->getChangedPoints(), QSet<double>());
        QVERIFY(!FP::defined(i->get(3.5)));
        QVERIFY(!FP::defined(i->get(4.5)));
        QVERIFY(!FP::defined(i->get(5.5)));
        delete i;

        i = DynamicInput::fromConfiguration(configSingle, "/", FP::undefined(), 1.0);
        QCOMPARE(i->getChangedPoints(), QSet<double>());
        QVERIFY(!FP::defined(i->get(3.5)));
        QVERIFY(!FP::defined(i->get(4.5)));
        QVERIFY(!FP::defined(i->get(5.5)));
        delete i;
        i = DynamicInput::fromConfiguration(configMulti, cmName, "/", FP::undefined(), 1.0);
        QCOMPARE(i->getChangedPoints(), QSet<double>());
        QVERIFY(!FP::defined(i->get(3.5)));
        QVERIFY(!FP::defined(i->get(4.5)));
        QVERIFY(!FP::defined(i->get(5.5)));
        delete i;


        SequenceName u("brw", "clean", "BaB_A11");
        configSingle.clear();
        Variant::Root c;
        c["Input/Station"] = "brw";
        c["Input/Archive"] = "clean";
        c["Input/Variable"] = "BaB_A11";
        c["Input/Flavors"].setType(Variant::Type::Array);
        c["Path"] = "SomePath";
        c["Calibration/#0"] = 0.5;
        c["Calibration/#1"] = 1.0;
        c["Default"] = -1.0;
        configSingle.clear();
        configSingle.emplace_back(FP::undefined(), FP::undefined(), c);
        i = DynamicInput::fromConfiguration(configSingle);
        i->registerInput(u);
        dms = SequenceSegment();
        dms.setStart(3.0);
        dms.setEnd(4.0);
        Variant::Root v;
        v["SomePath"] = 2.0;
        v["SomeOtherPath"] = 3.0;
        dms.setValue(u, v);
        QCOMPARE(i->get(3.0), -1.0);
        QCOMPARE(i->get(dms), 2.5);
        delete i;
    }

    void filterInputOptionSpecial()
    {
        DynamicInputOption si("", "", "", "");
        DynamicInput *i;

        SequenceName u("brw", "clean", "BaB_A11");
        SequenceSegment dms;
        dms.setValue(u, Variant::Root(2.0));

        si.set(2.0);
        i = si.getInput();
        QCOMPARE(i->get(3.0), 2.0);
        delete i;

        si.clear();
        i = si.getInput();
        QVERIFY(!FP::defined(i->get(3.0)));
        delete i;

        si.set(SequenceMatch::OrderedLookup(SequenceMatch::Element(u)), Calibration(), 5.0);
        i = si.getInput();
        i->registerInput(u);
        QCOMPARE(i->get(3.0), 5.0);
        QCOMPARE(i->get(dms), 2.0);
        delete i;

        QList<double> cf;
        cf << 0.0 << 2.0;
        si.set(FP::undefined(), FP::undefined(), Calibration(cf));
        i = si.getInput();
        i->registerInput(u);
        QCOMPARE(i->get(3.0), 5.0);
        QCOMPARE(i->get(dms), 4.0);
        delete i;

        si.set(FP::undefined(), 2.0, -1.0, true);
        i = si.getInput();
        i->registerInput(u);
        QCOMPARE(i->get(1.0), -1.0);
        QCOMPARE(i->get(3.0), 5.0);
        QCOMPARE(i->get(dms), 4.0);
        delete i;

        Variant::Root vp;
        vp["SomePath"] = 3.0;
        vp["SomeOtherPath"] = 4.0;
        dms.setValue(u, vp);
        si.set(SequenceMatch::OrderedLookup(SequenceMatch::Element(u)), Calibration(), 5.0,
               "SomePath");
        i = si.getInput();
        i->registerInput(u);
        QCOMPARE(i->get(3.0), 5.0);
        QCOMPARE(i->get(dms), 3.0);
        delete i;

        si.set(SequenceMatch::OrderedLookup(SequenceMatch::Element(u)), Calibration(cf), 5.0,
               "SomeOtherPath");
        i = si.getInput();
        i->registerInput(u);
        QCOMPARE(i->get(3.0), 5.0);
        QCOMPARE(i->get(dms), 8.0);
        delete i;
    }
};

QTEST_APPLESS_MAIN(TestDynamicInput)

#include "dynamicinput.moc"
