/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtSql>

#include "datacore/sequencematch.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::SequenceMatch;

class TestSequenceMatchElement : public QObject {
Q_OBJECT
private slots:

    void always()
    {
        Element i;
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!i.neverMatches());
        QVERIFY(!i.reduce().isValid());
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!j.neverMatches());
        QVERIFY(!j.reduce().isValid());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!i.neverMatches());
        QVERIFY(!i.reduce().isValid());

        i.setMatch(SequenceName("", "", "BsB_S11"));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsB_S11")));
    }

    void never()
    {
        Element i(Element::SpecialMatch::None);
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(i.neverMatches());
        QVERIFY(!i.reduce().isValid());
        Element j = i;
        Element k = std::move(i);
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(j.neverMatches());
        QVERIFY(!j.reduce().isValid());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(i.neverMatches());
        QVERIFY(!i.reduce().isValid());

        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsB_S11")));
        i.clearMatch(true, false, false);
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsB_S11")));

        i.setMatch(SequenceName("", "", "BsB_S11"));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsB_S11")));

        QVERIFY(i.matches(SequenceName("alt", "raw", "BsB_S11")));
        i.clearMatch(true, false, false);
        QVERIFY(i.matches(SequenceName("alt", "raw", "BsB_S11")));
    }

    void exactSingle()
    {
        Element i("bnd", "raW", "BsG_S11");
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsg_S11")));
        QVERIFY(!i.neverMatches());
        QVERIFY(!i.reduce().isValid());
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "Bsg_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "Bsg_S11")));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsg_S11")));
        {
            SequenceName toFill;
            QVERIFY(i.fill(toFill));
            QCOMPARE(toFill, SequenceName("bnd", "raw", "BsG_S11"));
        }

        i.setMatch(SequenceName("", "", "BsB_S11"));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsB_S11")));
        QCOMPARE(i.reduce(), SequenceName("bnd", "raw", "BsB_S11"));

        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsR_S11")));
        i.clearMatch(true, false, true);
        QVERIFY(i.matches(SequenceName("alt", "raw", "BsR_S11")));
    }

    void regexpSingle()
    {
        Element i("bn[dA]", "raw(?:_metA)?", "BsG_S\\d+");
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(i.matches(SequenceName("bna", "raw_meta", "BsG_S12")));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsg_S11")));
        QVERIFY(!i.neverMatches());
        QVERIFY(!i.reduce().isValid());
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(j.matches(SequenceName("bna", "raw_meta", "BsG_S12")));
        QVERIFY(!j.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "Bsg_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(k.matches(SequenceName("bna", "raw_meta", "BsG_S12")));
        QVERIFY(!k.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "Bsg_S11")));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(i.matches(SequenceName("bna", "raw_meta", "BsG_S12")));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsg_S11")));
        {
            SequenceName toFill;
            QVERIFY(!i.fill(toFill));
        }

        auto sel = i.toArchiveSelection();
        QCOMPARE(sel.stations, (Archive::Selection::Match{"bn[dA]"}));
        QCOMPARE(sel.archives, (Archive::Selection::Match{"raw(?:_metA)?"}));
        QCOMPARE(sel.variables, (Archive::Selection::Match{"BsG_S\\d+"}));
        QVERIFY(sel.hasFlavors.empty());
        QVERIFY(sel.lacksFlavors.empty());
        QVERIFY(sel.exactFlavors.empty());

        i.setMatch(SequenceName("", "", "BsB_S11"));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S12")));
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsB_S11")));
        QVERIFY(i.matches(SequenceName("bnd", "raw_meta", "BsB_S11")));
        QVERIFY(i.matches(SequenceName("bna", "raw", "BsB_S11")));
        QVERIFY(!i.reduce().isValid());

        QVERIFY(!i.matches(SequenceName("bna", "clean", "BsB_S11")));
        i.clearMatch(false, true, false);
        QVERIFY(i.matches(SequenceName("bna", "clean", "BsB_S11")));
    }

    void exactSingleFlavors()
    {
        Element i(SequenceName::Component("bnD"), "raW", "BsG_S11", {"pM1"});
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm10"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!i.neverMatches());
        QCOMPARE(i.reduce(), SequenceName("bnd", "raw", "BsG_S11", {"pm1"}));
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!j.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm10"})));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!k.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm10"})));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsG_S11")));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm10"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));

        {
            SequenceName toFill;
            QVERIFY(i.fill(toFill, false));
            QCOMPARE(toFill, SequenceName("bnd", "raw", "BsG_S11", {"pm1"}));
        }

        i.setMatch(SequenceName("", "", "", {"pm10"}));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm10"})));
        QCOMPARE(i.reduce(), SequenceName("bnd", "raw", "BsG_S11", {"pm10"}));
        {
            SequenceName toFill;
            QVERIFY(i.fill(toFill, false));
            QCOMPARE(toFill, SequenceName("bnd", "raw", "BsG_S11", {"pm10"}));
        }

        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm11"})));
        i.clearMatch(false, false, false, true);
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm11"})));
    }

    void regexpSingleFlavors()
    {
        Element i(Element::Pattern("bn[D]"), "raw", "BsG_S11", {"pM[12]"});
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm2"})));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm10"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!i.neverMatches());
        QVERIFY(!i.reduce().isValid());
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm2"})));
        QVERIFY(!j.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm10"})));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm2"})));
        QVERIFY(!k.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm10"})));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsG_S11")));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm2"})));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm10"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));

        {
            SequenceName toFill("bnd", "", "");
            QVERIFY(!i.fill(toFill, false));
            toFill = SequenceName("bnd", "", "");
            toFill.setFlavors({"pm1"});
            QVERIFY(i.fill(toFill, true));
            QCOMPARE(toFill, SequenceName("bnd", "raw", "BsG_S11", {"pm1"}));
            toFill.setFlavors({"pm25"});
            QVERIFY(!i.fill(toFill, true));
        }

        i.setMatch(SequenceName("", "", "BsB_S11", {"pm25"}));
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsB_S11", {"pm25"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
    }

    void exactHasLacksFlavors()
    {
        Element i(Element::Pattern("bnd"), "raw", "BsG_S11", {"PM1"}, {"pm25"});
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11", {"pm25"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm25"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!i.neverMatches());
        QVERIFY(!i.reduce().isValid());
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!j.matches(SequenceName("alt", "raw", "BsG_S11", {"pm25"})));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm25"})));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!k.matches(SequenceName("alt", "raw", "BsG_S11", {"pm25"})));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm25"})));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsG_S11")));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11", {"pm25"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm25"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));

        {
            SequenceName toFill;
            QVERIFY(!i.fill(toFill));
        }
    }

    void regexpHasLacksFlavors()
    {
        Element i(Element::Pattern("bndA?"), "raw(?:_metA)?", "Bs[GB]_S11", {"P(?:m)1"}, {"pM2[5]+"});
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("bnda", "raw_meta", "BsB_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("bnda", "raw_meta", "Bsb_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11", {"pm25"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm25"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!i.neverMatches());
        QVERIFY(!i.reduce().isValid());
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(j.matches(SequenceName("bnda", "raw_meta", "BsB_S11", {"pm1"})));
        QVERIFY(!j.matches(SequenceName("bnda", "raw_meta", "Bsb_S11", {"pm1"})));
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!j.matches(SequenceName("alt", "raw", "BsG_S11", {"pm25"})));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm25"})));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(k.matches(SequenceName("bnda", "raw_meta", "BsB_S11", {"pm1"})));
        QVERIFY(!k.matches(SequenceName("bnda", "raw_meta", "Bsb_S11", {"pm1"})));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!k.matches(SequenceName("alt", "raw", "BsG_S11", {"pm25"})));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm25"})));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsG_S11")));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("bnda", "raw_meta", "BsB_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("bnda", "raw_meta", "Bsb_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm10"})));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11", {"pm25"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1", "pm25"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));

        {
            SequenceName toFill;
            QVERIFY(!i.fill(toFill));
        }
    }

    void exactMultiple()
    {
        Element i({"bnd", "THD"}, {"raw", "AVGH"}, {"BsG_S11", "BsB_S11"});
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(!i.neverMatches());
        QVERIFY(!i.reduce().isValid());
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(j.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!j.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(k.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!k.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsb_S11")));

        auto sel = i.toArchiveSelection();
        QCOMPARE(sel.stations, (Archive::Selection::Match{"bnd", "thd"}));
        QCOMPARE(sel.archives, (Archive::Selection::Match{"raw", "avgh"}));
        QCOMPARE(sel.variables, (Archive::Selection::Match{"BsG_S11", "BsB_S11"}));
        QVERIFY(sel.hasFlavors.empty());
        QVERIFY(sel.lacksFlavors.empty());
        QVERIFY(sel.exactFlavors.empty());

        {
            SequenceName toFill;
            QVERIFY(!i.fill(toFill));
        }

        i.setMatch(SequenceName("alt", "clean", "BsR_S11", {"pm25"}));
        QVERIFY(i.matches(SequenceName("alt", "clean", "BsR_S11", {"pm25"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!i.neverMatches());
        QCOMPARE(i.reduce(), SequenceName("alt", "clean", "BsR_S11", {"pm25"}));
    }

    void regexpMultiple()
    {
        Element i({"bn[dA]", "TH[d]"}, {"raw(?:_metA)?", "avg[hH]"}, {"BsG_S\\d+", "BsB_[SA]11"});
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(i.matches(SequenceName("bna", "raw_meta", "BsG_S12")));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(j.matches(SequenceName("bna", "raw_meta", "BsG_S12")));
        QVERIFY(j.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!j.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(k.matches(SequenceName("bna", "raw_meta", "BsG_S12")));
        QVERIFY(k.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!k.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(i.matches(SequenceName("bna", "raw_meta", "BsG_S12")));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsb_S11")));
    }

    void exactMultipleFlavors()
    {
        Element i(Element::PatternList{"bnd", "THD"}, Element::PatternList{"raw", "AVGH"},
                  Element::PatternList{"BsG_S11", "BsB_S11"}, {"pM1"});
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(!i.neverMatches());
        QVERIFY(!i.reduce().isValid());
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(j.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!j.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(j.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!j.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!j.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(k.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!k.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(k.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!k.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!k.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsb_S11")));

        {
            SequenceName toFill;
            QVERIFY(!i.fill(toFill));
        }
    }

    void regexpMultipleFlavors()
    {
        Element i(Element::PatternList{"bn[dA]", "TH[d]"}, {"raw(?:_metA)?", "avg[hH]"},
                  {"BsG_S\\d+", "BsB_[SA]11"}, {"PM[12]"});
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("bna", "raw_meta", "BsG_S12", {"pm2"})));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(j.matches(SequenceName("bna", "raw_meta", "BsG_S12", {"pm2"})));
        QVERIFY(j.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!j.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(!j.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!j.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(k.matches(SequenceName("bna", "raw_meta", "BsG_S12", {"pm2"})));
        QVERIFY(k.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!k.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(!k.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!k.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("bna", "raw_meta", "BsG_S12", {"pm2"})));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsb_S11")));
    }

    void exactMultipleHasLacksFlavors()
    {
        Element i(Element::PatternList{"bnd", "THD"}, {"raw", "AVGH"}, {"BsG_S11", "BsB_S11"},
                  {"Pm1"}, {"PM10"});
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1", "pm10"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(j.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!j.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1", "pm10"})));
        QVERIFY(!j.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(j.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!j.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!j.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(k.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!k.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1", "pm10"})));
        QVERIFY(!k.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(k.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!k.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!k.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1", "pm10"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsb_S11")));
    }

    void regexpMultipleHasLacksFlavors()
    {
        Element i(Element::PatternList{"bn[dA]", "TH[d]"}, {"raw(?:_metA)?", "avg[hH]"},
                  {"BsG_S\\d+", "BsB_[SA]11"},
                  {"PM[12]"}, {"P(?:M)+10"});
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("bna", "raw_meta", "BsG_S12", {"pm2"})));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1", "pm10"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(j.matches(SequenceName("bna", "raw_meta", "BsG_S12", {"pm2"})));
        QVERIFY(j.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!j.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(!j.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1", "pm10"})));
        QVERIFY(!j.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(!j.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!j.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(k.matches(SequenceName("bna", "raw_meta", "BsG_S12", {"pm2"})));
        QVERIFY(k.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!k.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(!k.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1", "pm10"})));
        QVERIFY(!k.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(!k.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!k.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "Bsb_S11")));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(i.matches(SequenceName("bna", "raw_meta", "BsG_S12", {"pm2"})));
        QVERIFY(i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm1", "pm10"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11", {"pm10"})));
        QVERIFY(!i.matches(SequenceName("thd", "avgh", "BsB_S11")));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsb_S11")));
    }

    void exactName()
    {
        Element i(SequenceName("bnd", "raw", "BsG_S11"));
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsg_S11")));
        QVERIFY(!i.neverMatches());
        QCOMPARE(i.reduce(), SequenceName("bnd", "raw", "BsG_S11"));
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!j.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "Bsg_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!k.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "Bsg_S11")));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsg_S11")));

        auto sel = i.toArchiveSelection();
        QCOMPARE(sel.stations, (Archive::Selection::Match{"bnd"}));
        QCOMPARE(sel.archives, (Archive::Selection::Match{"raw"}));
        QCOMPARE(sel.variables, (Archive::Selection::Match{"BsG_S11"}));
        QVERIFY(sel.hasFlavors.empty());
        QVERIFY(sel.lacksFlavors.empty());
        QCOMPARE(sel.exactFlavors, (Archive::Selection::Match{""}));

        {
            SequenceName toFill;
            QVERIFY(i.fill(toFill));
            QCOMPARE(toFill, SequenceName("bnd", "raw", "BsG_S11"));
        }
    }

    void exactNameFlavors()
    {
        Element i(SequenceName("bnd", "raw", "BsG_S11", {"pm10"}));
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm10"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsg_S11")));
        QVERIFY(!i.neverMatches());
        QCOMPARE(i.reduce(), SequenceName("bnd", "raw", "BsG_S11", {"pm10"}));
        Element j = i;
        Element k = std::move(i);
        QVERIFY(j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm10"})));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!j.matches(SequenceName("bnd", "raw", "Bsg_S11")));
        QVERIFY(k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm10"})));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!k.matches(SequenceName("bnd", "raw", "Bsg_S11")));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm10"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11", {"pm1"})));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("alt", "raw", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "BsR_S11")));
        QVERIFY(!i.matches(SequenceName("bnd", "raw", "Bsg_S11")));

        auto sel = i.toArchiveSelection();
        QCOMPARE(sel.stations, (Archive::Selection::Match{"bnd"}));
        QCOMPARE(sel.archives, (Archive::Selection::Match{"raw"}));
        QCOMPARE(sel.variables, (Archive::Selection::Match{"BsG_S11"}));
        QVERIFY(sel.hasFlavors.empty());
        QVERIFY(sel.lacksFlavors.empty());
        QCOMPARE(sel.exactFlavors, (Archive::Selection::Match{"pm10"}));

        {
            SequenceName toFill;
            QVERIFY(i.fill(toFill, false));
            QCOMPARE(toFill, SequenceName("bnd", "raw", "BsG_S11", {"pm10"}));
        }
    }
};

QTEST_APPLESS_MAIN(TestSequenceMatchElement)

#include "sequencematch_element.moc"
