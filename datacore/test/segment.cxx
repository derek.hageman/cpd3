/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtSql>
#include <QString>

#include "datacore/segment.hxx"
#include "datacore/archive/access.hxx"
#include "database/storage.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Data;

Q_DECLARE_METATYPE(QList<QString>);

namespace CPD3 {
namespace Data {
bool operator==(const ValueSegment &a, const ValueSegment &b)
{
    return FP::equal(a.getStart(), b.getStart()) &&
            FP::equal(a.getEnd(), b.getEnd()) &&
            a.getValue() == b.getValue();
}
}
}

class TestSegment : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        QVERIFY(databaseFile.open());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void testDataSegment()
    {
        ValueSegment s1;
        QVERIFY(!FP::defined(s1.getStart()));
        QVERIFY(!FP::defined(s1.getEnd()));
        QVERIFY(!s1.getValue().exists());
        s1.setStart(12.0);
        QCOMPARE(s1.getStart(), 12.0);
        s1.setEnd(13.0);
        QCOMPARE(s1.getEnd(), 13.0);
        s1.write().setDouble(123.0);
        QCOMPARE(s1.getValue().toDouble(), 123.0);
        s1["/foo"].setDouble(456.0);
        QCOMPARE(s1.value().hash("foo").toDouble(), 456.0);
        s1[QString("/foo")].setDouble(457.0);
        QCOMPARE(s1.value().hash("foo").toDouble(), 457.0);
        s1.write().array(0).setDouble(458.0);
        QCOMPARE(s1.value().array(0).toDouble(), 458.0);

        Variant::Root vc(234.0);
        QVERIFY(s1.read() != vc.read());
        vc.write().array(0).setDouble(458.0);
        QVERIFY(s1.read() == vc.read());

        ValueSegment s2(s1);
        QCOMPARE(s1.getStart(), s2.getStart());
        QCOMPARE(s1.getEnd(), s2.getEnd());
        QCOMPARE(s1.getValue(), s2.getValue());
        s2 = s1;
        QCOMPARE(s1.getStart(), s2.getStart());
        QCOMPARE(s1.getEnd(), s2.getEnd());
        QCOMPARE(s1.getValue(), s2.getValue());

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << s1;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> s2;
            }
            QCOMPARE(s1.getStart(), s2.getStart());
            QCOMPARE(s1.getEnd(), s2.getEnd());
            QCOMPARE(s1.getValue(), s2.getValue());
        }

        ValueSegment s3(s1.getStart(), s1.getEnd(), vc);
        QCOMPARE(s1.getStart(), s3.getStart());
        QCOMPARE(s1.getEnd(), s3.getEnd());
        QCOMPARE(s1.getValue(), s3.getValue());
        s3.write().setDouble(1.0);
        QVERIFY(s1.getValue() != s3.getValue());

        s1.setEnd(15.0);
        QVERIFY(s1.getEnd() != s2.getEnd());
        s2 = s1;
        s1.setStart(10.0);
        QVERIFY(s1.getStart() != s2.getStart());
    }

    void testDataMultiSegment()
    {
        SequenceSegment s1;
        QVERIFY(!FP::defined(s1.getStart()));
        QVERIFY(!FP::defined(s1.getEnd()));
        SequenceSegment s2(s1);

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << s1;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> s2;
            }
        }

        SequenceName l1("brw", "clean", "BsG_S11");
        SequenceName l2("brw", "clean", "BsG_S11", {"pm1"});
        s1[l1] = 123.0;
        s1[l2] = 124.0;
        s2 = s1;
        QCOMPARE(s1.getValue(l1).toDouble(), 123.0);
        QCOMPARE(s1.getValue(l2).toDouble(), 124.0);
        QCOMPARE(s1.getValue("brw", "clean", "BsG_S11").toDouble(), 123.0);
        QCOMPARE(s1.getValue("brw", "clean", "BsG_S11", {"pm1"}).toDouble(),
                 124.0);
        QCOMPARE(s1.getValue(QRegExp("brw"), QRegExp("[c]lean"), QRegExp("Bs[GB]_S11"),
                             QList<QRegExp>(), QList<QRegExp>() << QRegExp("pm1")).toDouble(),
                 123.0);
        QCOMPARE(s1.getValue(QRegExp("brw"), QRegExp("[c]lean"), QRegExp("Bs[GB]_S11"),
                             QList<QRegExp>() << QRegExp("pm1")).toDouble(), 124.0);
        s1 = s2;
        s1[l1] = 124.0;
        QCOMPARE(s1.getValue(l1).toDouble(), 124.0);
        s1.getValue(l1).setDouble(121.0);
        QCOMPARE(s1.getValue(l1).toDouble(), 121.0);
        QCOMPARE(s2.getValue(l1).toDouble(), 123.0);

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << s1;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> s2;
            }
        }

        QCOMPARE(s1.getValue(l1).toDouble(), 121.0);
        QCOMPARE(s2.getValue(l1).toDouble(), 121.0);
    }

    void single()
    {
        QFETCH(SequenceValue::Transfer, inputs);
        QFETCH(ValueSegment::Transfer, expected);

        SequenceSegment::Stream sr;
        SequenceSegment::Transfer l;
        Util::append(sr.add(inputs), l);
        Util::append(sr.finish(), l);

        SequenceName lookup("brw", "raw", "T_S11");

        QCOMPARE(l.size(), expected.size());
        for (int i = 0, max = l.size(); i < max; i++) {
            SequenceSegment re(l.at(i));
            ValueSegment ex(expected.at(i));

            QVERIFY(FP::equal(re.getStart(), ex.getStart()));
            QVERIFY(FP::equal(re.getEnd(), ex.getEnd()));
            QVERIFY(re.getValue(lookup) == ex.value());
        }


        ValueSegment::Stream fsr;
        ValueSegment::Transfer fl;
        Util::append(fsr.add(inputs), fl);
        Util::append(fsr.finish(), fl);

        QCOMPARE(fl, expected);
    }

    void single_data()
    {
        QTest::addColumn<SequenceValue::Transfer>("inputs");
        QTest::addColumn<ValueSegment::Transfer>("expected");

        SequenceValue::Transfer inputs;
        ValueSegment::Transfer expected;

        QTest::newRow("Empty") << inputs << expected;

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(1.0), 1.0, 2.0, 0);
        expected.emplace_back(1.0, 2.0, Variant::Root(1.0));
        QTest::newRow("Single") << inputs << expected;

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(2.0), 2.0, 3.0, 1);
        expected.emplace_back(2.0, 3.0, Variant::Root(2.0));
        QTest::newRow("No overlap") << inputs << expected;

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(3.0), 3.0, 4.0, 0);
        expected.emplace_back(3.0, 4.0, Variant::Root(3.0));
        QTest::newRow("No overlap three") << inputs << expected;

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(4.0), 5.0, 6.0, 2);
        expected.emplace_back(5.0, 6.0, Variant::Root(4.0));
        QTest::newRow("No overlap gap") << inputs << expected;

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(5.0), 6.0, 7.0, 2);
        expected.emplace_back(6.0, 7.0, Variant::Root(5.0));
        QTest::newRow("No overlap gap two") << inputs << expected;

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(6.0), 8.0, 10.0, 2);
        expected.emplace_back(8.0, 10.0, Variant::Root(6.0));
        QTest::newRow("No overlap two gap") << inputs << expected;

        inputs.insert(inputs.begin(),
                      SequenceValue(SequenceName("brw", "raw", "T_S11"), Variant::Root(7.0),
                                    FP::undefined(), 1.0, 2));
        expected.insert(expected.begin(), ValueSegment(FP::undefined(), 1.0, Variant::Root(7.0)));
        QTest::newRow("Infinite start") << inputs << expected;

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(8.0), 10.0,
                            FP::undefined(), 2);
        expected.emplace_back(10.0, FP::undefined(), Variant::Root(8.0));
        QTest::newRow("Infinite end") << inputs << expected;

        inputs.clear();
        expected.clear();

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(1.0), 1.0, 2.0, 0);
        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(2.0), 1.5, 2.0, 0);
        expected.emplace_back(1.0, 1.5, Variant::Root(1.0));
        expected.emplace_back(1.5, 2.0, Variant::Root(2.0));
        QTest::newRow("Overlap two tail only") << inputs << expected;

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(3.0), 1.9, 3.0, 0);
        expected.pop_back();
        expected.emplace_back(1.5, 1.9, Variant::Root(2.0));
        expected.emplace_back(1.9, 2.0, Variant::Root(3.0));
        expected.emplace_back(2.0, 3.0, Variant::Root(3.0));
        QTest::newRow("Overlap three head and tail") << inputs << expected;

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(4.0), 3.0, 4.0, 0);
        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(5.0), 3.5, 5.0, 0);
        expected.emplace_back(3.0, 3.5, Variant::Root(4.0));
        expected.emplace_back(3.5, 4.0, Variant::Root(5.0));
        expected.emplace_back(4.0, 5.0, Variant::Root(5.0));
        QTest::newRow("Overlap two tail") << inputs << expected;

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(6.0), 4.0, 6.0, 0);
        expected.pop_back();
        expected.emplace_back(4.0, 5.0, Variant::Root(6.0));
        expected.emplace_back(5.0, 6.0, Variant::Root(6.0));
        QTest::newRow("Overlap three tail") << inputs << expected;

        inputs.clear();
        expected.clear();

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(1.0),
                            FP::undefined(), 2.0, 0);
        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(2.0), 1.0, 3.0, 0);
        expected.emplace_back(FP::undefined(), 1.0, Variant::Root(1.0));
        expected.emplace_back(1.0, 2.0, Variant::Root(2.0));
        expected.emplace_back(2.0, 3.0, Variant::Root(2.0));
        QTest::newRow("Infinite start") << inputs << expected;

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(3.0), 2.5,
                            FP::undefined(), 0);
        expected.pop_back();
        expected.emplace_back(2.0, 2.5, Variant::Root(2.0));
        expected.emplace_back(2.5, 3.0, Variant::Root(3.0));
        expected.emplace_back(3.0, FP::undefined(), Variant::Root(3.0));
        QTest::newRow("Infinite end") << inputs << expected;

        inputs.clear();
        expected.clear();

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(1.0),
                            FP::undefined(), FP::undefined(), 0);
        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(2.0), 1.0, 3.0, 0);
        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(3.0), 3.0, 5.0, 0);
        expected.emplace_back(FP::undefined(), 1.0, Variant::Root(1.0));
        expected.emplace_back(1.0, 3.0, Variant::Root(2.0));
        expected.emplace_back(3.0, 5.0, Variant::Root(3.0));
        expected.emplace_back(5.0, FP::undefined(), Variant::Root(1.0));
        QTest::newRow("Infinite middle") << inputs << expected;

        inputs.clear();
        expected.clear();

        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(1.0), 1.0, 4.0, 1);
        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(2.0), 1.0, 3.0, 0);
        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(3.0), 1.25, 3.0, -1);
        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(3.1), 1.25, 3.0, -1);
        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(4.0), 1.5, 3.0, 0);
        inputs.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(5.0), 3.5, 5.0, 2);
        expected.emplace_back(1.0, 1.25, Variant::Root(1.0));
        expected.emplace_back(1.25, 1.5, Variant::Root(1.0));
        expected.emplace_back(1.5, 3.0, Variant::Root(1.0));
        expected.emplace_back(3.0, 3.5, Variant::Root(1.0));
        expected.emplace_back(3.5, 4.0, Variant::Root(5.0));
        expected.emplace_back(4.0, 5.0, Variant::Root(5.0));
        QTest::newRow("Priority sorting") << inputs << expected;
    }

    void singleMerge()
    {
        QFETCH(ValueSegment::Transfer, input);
        QFETCH(QList<QString>, paths);
        QFETCH(ValueSegment::Transfer, expected);

        ValueSegment::Transfer result(ValueSegment::merge(input, paths));

        QCOMPARE(result.size(), expected.size());
        for (int i = 0, max = result.size(); i < max; i++) {
            ValueSegment re(result.at(i));
            ValueSegment ex(expected.at(i));

            QVERIFY(FP::equal(re.getStart(), ex.getStart()));
            QVERIFY(FP::equal(re.getEnd(), ex.getEnd()));
            QVERIFY(re.value() == ex.value());
        }
    }

    void singleMerge_data()
    {
        QTest::addColumn<ValueSegment::Transfer>("input");
        QTest::addColumn<QList<QString> >("paths");
        QTest::addColumn<ValueSegment::Transfer>("expected");

        ValueSegment::Transfer input;
        QList<QString> paths;
        ValueSegment::Transfer expected;

        QTest::newRow("Empty") << input << paths << expected;

        input.emplace_back(FP::undefined(), 100.0, Variant::Root(1.0));
        input.emplace_back(100, 200.0, Variant::Root(2.0));
        input.emplace_back(200, 300.0, Variant::Root(3.0));
        expected = input;
        QTest::newRow("Non overlapping") << input << paths << expected;

        input.clear();
        expected.clear();
        input.emplace_back(200, 300.0, Variant::Root(3.0));
        input.emplace_back(100, 210.0, Variant::Root(1.0));
        input.emplace_back(150, 250.0, Variant::Root(2.0));
        expected.emplace_back(100, 150.0, Variant::Root(1.0));
        expected.emplace_back(150, 200.0, Variant::Root(2.0));
        expected.emplace_back(200, 210.0, Variant::Root(2.0));
        expected.emplace_back(210, 250.0, Variant::Root(2.0));
        expected.emplace_back(250, 300.0, Variant::Root(3.0));
        QTest::newRow("Basic overlay") << input << paths << expected;

        Variant::Root v;
        input.clear();
        expected.clear();
        v.write().hash("A").setDouble(1.0);
        v.write().hash("B").setDouble(2.0);
        input.emplace_back(100, 200.0, v);
        paths.append(QString());
        input.emplace_back(150, 200.0, Variant::Root(3.0));
        paths.append("A");
        expected.emplace_back(100, 150.0, v);
        v.write().setEmpty();
        v.write().hash("A").setDouble(3.0);
        v.write().hash("B").setDouble(2.0);
        expected.emplace_back(150, 200.0, v);
        QTest::newRow("Path over") << input << paths << expected;

        input.clear();
        expected.clear();
        paths.clear();
        v.write().setEmpty();
        input.emplace_back(100, 200.0, Variant::Root(1.0));
        paths.append(QString("A"));
        v.write().hash("A").setDouble(1.0);
        expected.emplace_back(100, 200.0, v);
        QTest::newRow("Path single") << input << paths << expected;

        input.clear();
        expected.clear();
        paths.clear();
        v.write().setEmpty();
        input.emplace_back(100, 200.0, Variant::Root(1.0));
        paths.append(QString("A"));
        v.write().hash("A").setDouble(2.0);
        v.write().hash("B").setDouble(3.0);
        input.emplace_back(150, 200.0, v);
        v.write().setEmpty();
        v.write().hash("A").setDouble(1.0);
        expected.emplace_back(100, 150.0, v);
        v.write().setEmpty();
        v.write().hash("A").setDouble(2.0);
        v.write().hash("B").setDouble(3.0);
        expected.emplace_back(150, 200.0, v);
        QTest::newRow("Path under") << input << paths << expected;
    }

    void multiple()
    {
        SequenceSegment::Stream sr;
        SequenceSegment::Transfer l;

        Util::append(
                sr.add(SequenceValue(SequenceName("brw", "raw", "T_S11"), Variant::Root(1.0), 1.0,
                                     2.0, 0)), l);
        Util::append(
                sr.add(SequenceValue(SequenceName("brw", "clean", "T_S11"), Variant::Root(2.0), 1.0,
                                     2.0, 0)), l);
        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "T_S11"), Variant::Root(3.0), 1.0,
                                     2.0, 1)), l);
        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "T_S11"), Variant::Root(4.0), 1.0,
                                     2.5, 0)), l);
        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "Tu_S11"), Variant::Root(5.0),
                                     1.0, 2.0, 0)), l);
        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "T_S11"), Variant::Root(-1.0),
                                     1.5, 2.0, -1)), l);
        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "T_S11"), Variant::Root(-2.0),
                                     1.75, 2.0, 2)), l);
        Util::append(sr.finish(), l);

        QCOMPARE((int) l.size(), 4);

        QCOMPARE(l.at(0).getStart(), 1.0);
        QCOMPARE(l.at(0).getEnd(), 1.5);
        QVERIFY(l[0].getValue(SequenceName("brw", "raw", "T_S11")) == Variant::Root(1.0));
        QVERIFY(l[0].getValue(SequenceName("brw", "clean", "T_S11")) == Variant::Root(2.0));
        QVERIFY(l[0].getValue(SequenceName("alt", "clean", "T_S11")) == Variant::Root(3.0));
        QVERIFY(l[0].getValue(SequenceName("alt", "clean", "Tu_S11")) == Variant::Root(5.0));

        QCOMPARE(l[1].getStart(), 1.5);
        QCOMPARE(l[1].getEnd(), 1.75);
        QVERIFY(l[1].getValue(SequenceName("brw", "raw", "T_S11")) == Variant::Root(1.0));
        QVERIFY(l[1].getValue(SequenceName("brw", "clean", "T_S11")) == Variant::Root(2.0));
        QVERIFY(l[1].getValue(SequenceName("alt", "clean", "T_S11")) == Variant::Root(3.0));
        QVERIFY(l[1].getValue(SequenceName("alt", "clean", "Tu_S11")) == Variant::Root(5.0));

        QCOMPARE(l[2].getStart(), 1.75);
        QCOMPARE(l[2].getEnd(), 2.0);
        QVERIFY(l[2].getValue(SequenceName("brw", "raw", "T_S11")) == Variant::Root(1.0));
        QVERIFY(l[2].getValue(SequenceName("brw", "clean", "T_S11")) == Variant::Root(2.0));
        QVERIFY(l[2].getValue(SequenceName("alt", "clean", "T_S11")) == Variant::Root(-2.0));
        QVERIFY(l[2].getValue(SequenceName("alt", "clean", "Tu_S11")) == Variant::Root(5.0));

        QCOMPARE(l[3].getStart(), 2.0);
        QCOMPARE(l[3].getEnd(), 2.5);
        QVERIFY(l[3].getValue(SequenceName("alt", "clean", "T_S11")) == Variant::Root(4.0));
    }

    void inject()
    {
        SequenceSegment::Transfer l;
        SequenceSegment::Stream sr;
        SequenceName unit("brw", "raw", "T_S11");

        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.0), 1.0, 2.0)), l);
        Util::append(sr.inject(SequenceValue(unit, Variant::Root(1.5), 1.5, 2.0, 1)), l);
        QCOMPARE((int) l.size(), 0);

        Util::append(sr.advance(2.0), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 1.0);
        QCOMPARE(l[0].getEnd(), 2.0);
        QCOMPARE(l[0].getValue(unit).toDouble(), 1.5);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.0), 2.0, 4.0)), l);
        QCOMPARE((int) l.size(), 1);

        Util::append(sr.advance(3.0), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[1].getStart(), 2.0);
        QCOMPARE(l[1].getEnd(), 4.0);
        QCOMPARE(l[1].getValue(unit).toDouble(), 2.0);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(3.5), 3.5, 4.0)), l);
        QCOMPARE((int) l.size(), 2);
        Util::append(sr.inject(SequenceValue(unit, Variant::Root(5.0), 5.0, 9.0)), l);
        QCOMPARE((int) l.size(), 3);
        QCOMPARE(l[2].getStart(), 3.5);
        QCOMPARE(l[2].getEnd(), 4.0);
        QCOMPARE(l[2].getValue(unit).toDouble(), 3.5);

        Util::append(sr.advance(5.5), l);
        QCOMPARE((int) l.size(), 3);

        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 3);
    }

    void injectFlat()
    {
        ValueSegment::Transfer l;
        ValueSegment::Stream sr;
        SequenceName unit("brw", "raw", "T_S11");

        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.0), 1.0, 2.0)), l);
        Util::append(sr.inject(SequenceValue(unit, Variant::Root(1.5), 1.5, 2.0, 1)), l);
        QCOMPARE((int) l.size(), 0);

        Util::append(sr.advance(2.0), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 1.0);
        QCOMPARE(l[0].getEnd(), 2.0);
        QCOMPARE(l[0].getValue().toDouble(), 1.5);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.0), 2.0, 4.0)), l);
        QCOMPARE((int) l.size(), 1);

        Util::append(sr.advance(3.0), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[1].getStart(), 2.0);
        QCOMPARE(l[1].getEnd(), 4.0);
        QCOMPARE(l[1].getValue().toDouble(), 2.0);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(3.5), 3.5, 4.0)), l);
        QCOMPARE((int) l.size(), 2);
        Util::append(sr.inject(SequenceValue(unit, Variant::Root(5.0), 5.0, 9.0)), l);
        QCOMPARE((int) l.size(), 3);
        QCOMPARE(l[2].getStart(), 3.5);
        QCOMPARE(l[2].getEnd(), 4.0);
        QCOMPARE(l[2].getValue().toDouble(), 3.5);

        Util::append(sr.advance(5.5), l);
        QCOMPARE((int) l.size(), 3);

        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 3);
    }

    void advance()
    {
        SequenceSegment::Transfer l;
        SequenceSegment::Stream sr;
        SequenceName unit("brw", "raw", "T_S11");

        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.0), 1.0, 2.0)), l);
        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.5), 1.5, 2.0)), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 1.0);
        QCOMPARE(l[0].getEnd(), 1.5);
        QCOMPARE(l[0].getValue(unit).toDouble(), 1.0);

        Util::append(sr.advance(2.0), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[1].getStart(), 1.5);
        QCOMPARE(l[1].getEnd(), 2.0);
        QCOMPARE(l[1].getValue(unit).toDouble(), 1.5);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.0), 2.0, 4.0)), l);
        QCOMPARE((int) l.size(), 2);

        Util::append(sr.advance(3.0), l);
        QCOMPARE((int) l.size(), 3);
        QCOMPARE(l[2].getStart(), 2.0);
        QCOMPARE(l[2].getEnd(), 4.0);
        QCOMPARE(l[2].getValue(unit).toDouble(), 2.0);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(3.5), 3.5, 4.0)), l);
        QCOMPARE((int) l.size(), 3);
        Util::append(sr.add(SequenceValue(unit, Variant::Root(5.0), 5.0, 9.0)), l);
        QCOMPARE((int) l.size(), 4);
        QCOMPARE(l[3].getStart(), 3.5);
        QCOMPARE(l[3].getEnd(), 4.0);
        QCOMPARE(l[3].getValue(unit).toDouble(), 3.5);

        Util::append(sr.advance(5.5), l);
        QCOMPARE((int) l.size(), 5);
        QCOMPARE(l[4].getStart(), 5.0);
        QCOMPARE(l[4].getEnd(), 9.0);
        QCOMPARE(l[4].getValue(unit).toDouble(), 5.0);

        Util::append(sr.advance(6.5), l);
        QCOMPARE((int) l.size(), 5);
        Util::append(sr.advance(6.9), l);
        QCOMPARE((int) l.size(), 5);
        Util::append(sr.add(SequenceValue(unit, Variant::Root(7.0), 7.0, 8.0)), l);
        QCOMPARE((int) l.size(), 5);
        Util::append(sr.advance(7.0), l);
        QCOMPARE((int) l.size(), 5);

        Util::append(sr.advance(7.5), l);
        QCOMPARE((int) l.size(), 6);
        QCOMPARE(l[5].getStart(), 7.0);
        QCOMPARE(l[5].getEnd(), 8.0);
        QCOMPARE(l[5].getValue(unit).toDouble(), 7.0);

        Util::append(sr.advance(7.8), l);
        QCOMPARE((int) l.size(), 6);
        Util::append(sr.add(SequenceValue(unit, Variant::Root(7.9), 7.9, 8.25)), l);
        QCOMPARE((int) l.size(), 6);

        Util::append(sr.advance(7.95), l);
        QCOMPARE((int) l.size(), 7);
        QCOMPARE(l[6].getStart(), 7.9);
        QCOMPARE(l[6].getEnd(), 8.0);
        QCOMPARE(l[6].getValue(unit).toDouble(), 7.9);

        Util::append(sr.advance(8.0), l);
        QCOMPARE((int) l.size(), 7);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 9);
        QCOMPARE(l[7].getStart(), 8.0);
        QCOMPARE(l[7].getEnd(), 8.25);
        QCOMPARE(l[7].getValue(unit).toDouble(), 7.9);
        QCOMPARE(l[8].getStart(), 8.25);
        QCOMPARE(l[8].getEnd(), 9.0);
        QCOMPARE(l[8].getValue(unit).toDouble(), 5.0);


        sr.clear();
        l.clear();

        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.0), FP::undefined(), 2.0)), l);
        QCOMPARE((int) l.size(), 0);
        Util::append(sr.advance(1.0), l);
        QCOMPARE((int) l.size(), 1);
        QVERIFY(!FP::defined(l[0].getStart()));
        QCOMPARE(l[0].getEnd(), 2.0);
        QCOMPARE(l[0].getValue(unit).toDouble(), 1.0);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.0), 2.0, 5.0)), l);
        QCOMPARE((int) l.size(), 1);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[1].getStart(), 2.0);
        QCOMPARE(l[1].getEnd(), 5.0);
        QCOMPARE(l[1].getValue(unit).toDouble(), 2.0);


        sr.clear();
        l.clear();

        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.0), 1.0, FP::undefined())), l);
        QCOMPARE((int) l.size(), 0);
        Util::append(sr.advance(1.0), l);
        QCOMPARE((int) l.size(), 0);
        Util::append(sr.advance(1.5), l);
        QCOMPARE((int) l.size(), 1);
        Util::append(sr.advance(2.0), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 1.0);
        QVERIFY(!FP::defined(l[0].getEnd()));
        QCOMPARE(l[0].getValue(unit).toDouble(), 1.0);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.0), 2.0, 5.0)), l);
        QCOMPARE((int) l.size(), 1);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 3);
        QCOMPARE(l[1].getStart(), 2.0);
        QCOMPARE(l[1].getEnd(), 5.0);
        QCOMPARE(l[1].getValue(unit).toDouble(), 2.0);
        QCOMPARE(l[2].getStart(), 5.0);
        QVERIFY(!FP::defined(l[2].getEnd()));
        QCOMPARE(l[2].getValue(unit).toDouble(), 1.0);


        SequenceName unit2("brw", "raw", "T_S12");
        sr.clear();
        l.clear();

        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.0), 1.0, 5.0)), l);
        Util::append(sr.advance(2.0), l);
        Util::append(sr.add(SequenceValue(unit2, Variant::Root(3.0), 2.0, 5.0)), l);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 1.0);
        QCOMPARE(l[0].getEnd(), 5.0);
        QCOMPARE(l[0].getValue(unit).toDouble(), 2.0);
        QCOMPARE(l[1].getStart(), 2.0);
        QCOMPARE(l[1].getEnd(), 5.0);
        QCOMPARE(l[1].getValue(unit).toDouble(), 2.0);
        QCOMPARE(l[1].getValue(unit2).toDouble(), 3.0);
    }

    void advanceFlat()
    {
        ValueSegment::Transfer l;
        ValueSegment::Stream sr;
        SequenceName unit("brw", "raw", "T_S11");

        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.0), 1.0, 2.0)), l);
        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.5), 1.5, 2.0)), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 1.0);
        QCOMPARE(l[0].getEnd(), 1.5);
        QCOMPARE(l[0].getValue().toDouble(), 1.0);

        Util::append(sr.advance(2.0), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[1].getStart(), 1.5);
        QCOMPARE(l[1].getEnd(), 2.0);
        QCOMPARE(l[1].getValue().toDouble(), 1.5);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.0), 2.0, 4.0)), l);
        QCOMPARE((int) l.size(), 2);

        Util::append(sr.advance(3.0), l);
        QCOMPARE((int) l.size(), 3);
        QCOMPARE(l[2].getStart(), 2.0);
        QCOMPARE(l[2].getEnd(), 4.0);
        QCOMPARE(l[2].getValue().toDouble(), 2.0);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(3.5), 3.5, 4.0)), l);
        QCOMPARE((int) l.size(), 3);
        Util::append(sr.add(SequenceValue(unit, Variant::Root(5.0), 5.0, 9.0)), l);
        QCOMPARE((int) l.size(), 4);
        QCOMPARE(l[3].getStart(), 3.5);
        QCOMPARE(l[3].getEnd(), 4.0);
        QCOMPARE(l[3].getValue().toDouble(), 3.5);

        Util::append(sr.advance(5.5), l);
        QCOMPARE((int) l.size(), 5);
        QCOMPARE(l[4].getStart(), 5.0);
        QCOMPARE(l[4].getEnd(), 9.0);
        QCOMPARE(l[4].getValue().toDouble(), 5.0);

        Util::append(sr.advance(6.5), l);
        QCOMPARE((int) l.size(), 5);
        Util::append(sr.advance(6.9), l);
        QCOMPARE((int) l.size(), 5);
        Util::append(sr.add(SequenceValue(unit, Variant::Root(7.0), 7.0, 8.0)), l);
        QCOMPARE((int) l.size(), 5);
        Util::append(sr.advance(7.0), l);
        QCOMPARE((int) l.size(), 5);

        Util::append(sr.advance(7.5), l);
        QCOMPARE((int) l.size(), 6);
        QCOMPARE(l[5].getStart(), 7.0);
        QCOMPARE(l[5].getEnd(), 8.0);
        QCOMPARE(l[5].getValue().toDouble(), 7.0);

        Util::append(sr.advance(7.8), l);
        QCOMPARE((int) l.size(), 6);
        Util::append(sr.add(SequenceValue(unit, Variant::Root(7.9), 7.9, 8.25)), l);
        QCOMPARE((int) l.size(), 6);

        Util::append(sr.advance(7.95), l);
        QCOMPARE((int) l.size(), 7);
        QCOMPARE(l[6].getStart(), 7.9);
        QCOMPARE(l[6].getEnd(), 8.0);
        QCOMPARE(l[6].getValue().toDouble(), 7.9);

        Util::append(sr.advance(8.0), l);
        QCOMPARE((int) l.size(), 7);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 9);
        QCOMPARE(l[7].getStart(), 8.0);
        QCOMPARE(l[7].getEnd(), 8.25);
        QCOMPARE(l[7].getValue().toDouble(), 7.9);
        QCOMPARE(l[8].getStart(), 8.25);
        QCOMPARE(l[8].getEnd(), 9.0);
        QCOMPARE(l[8].getValue().toDouble(), 5.0);


        sr.clear();
        l.clear();

        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.0), FP::undefined(), 2.0)), l);
        QCOMPARE((int) l.size(), 0);
        Util::append(sr.advance(1.0), l);
        QCOMPARE((int) l.size(), 1);
        QVERIFY(!FP::defined(l[0].getStart()));
        QCOMPARE(l[0].getEnd(), 2.0);
        QCOMPARE(l[0].getValue().toDouble(), 1.0);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.0), 2.0, 5.0)), l);
        QCOMPARE((int) l.size(), 1);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[1].getStart(), 2.0);
        QCOMPARE(l[1].getEnd(), 5.0);
        QCOMPARE(l[1].getValue().toDouble(), 2.0);
    }

    void advanceCompleted()
    {
        SequenceSegment::Transfer l;
        SequenceSegment::Stream sr;
        SequenceName unit("brw", "raw", "T_S11");
        bool futurePending = false;

        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.0), 1.0, 2.0)), l);
        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.5), 1.5, 2.0)), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 1.0);
        QCOMPARE(l[0].getEnd(), 1.5);
        QCOMPARE(l[0].getValue(unit).toDouble(), 1.0);

        Util::append(sr.completedAdvance(1.75, &futurePending), l);
        QCOMPARE((int) l.size(), 1);
        QVERIFY(futurePending);
        Util::append(sr.completedAdvance(2.0, &futurePending), l);
        QCOMPARE((int) l.size(), 2);
        QVERIFY(!futurePending);
        QCOMPARE(l[1].getStart(), 1.5);
        QCOMPARE(l[1].getEnd(), 2.0);
        QCOMPARE(l[1].getValue(unit).toDouble(), 1.5);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.0), 2.0, 4.0)), l);
        QCOMPARE((int) l.size(), 2);
        Util::append(sr.completedAdvance(3.0, &futurePending), l);
        QCOMPARE((int) l.size(), 2);
        QVERIFY(futurePending);
        Util::append(sr.completedAdvance(4.5, &futurePending), l);
        QCOMPARE((int) l.size(), 3);
        QVERIFY(!futurePending);
        QCOMPARE(l[2].getStart(), 2.0);
        QCOMPARE(l[2].getEnd(), 4.0);
        QCOMPARE(l[2].getValue(unit).toDouble(), 2.0);

        Util::append(sr.completedAdvance(4.75, &futurePending), l);
        QCOMPARE((int) l.size(), 3);
        QVERIFY(!futurePending);
    }

    void advanceCompletedFlat()
    {
        ValueSegment::Transfer l;
        ValueSegment::Stream sr;
        SequenceName unit("brw", "raw", "T_S11");
        bool futurePending = false;

        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.0), 1.0, 2.0)), l);
        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.5), 1.5, 2.0)), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 1.0);
        QCOMPARE(l[0].getEnd(), 1.5);
        QCOMPARE(l[0].getValue().toDouble(), 1.0);

        Util::append(sr.completedAdvance(1.75, &futurePending), l);
        QCOMPARE((int) l.size(), 1);
        QVERIFY(futurePending);
        Util::append(sr.completedAdvance(2.0, &futurePending), l);
        QCOMPARE((int) l.size(), 2);
        QVERIFY(!futurePending);
        QCOMPARE(l[1].getStart(), 1.5);
        QCOMPARE(l[1].getEnd(), 2.0);
        QCOMPARE(l[1].getValue().toDouble(), 1.5);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.0), 2.0, 4.0)), l);
        QCOMPARE((int) l.size(), 2);
        Util::append(sr.completedAdvance(3.0, &futurePending), l);
        QCOMPARE((int) l.size(), 2);
        QVERIFY(futurePending);
        Util::append(sr.completedAdvance(4.5, &futurePending), l);
        QCOMPARE((int) l.size(), 3);
        QVERIFY(!futurePending);
        QCOMPARE(l[2].getStart(), 2.0);
        QCOMPARE(l[2].getEnd(), 4.0);
        QCOMPARE(l[2].getValue().toDouble(), 2.0);

        Util::append(sr.completedAdvance(4.75, &futurePending), l);
        QCOMPARE((int) l.size(), 3);
        QVERIFY(!futurePending);
    }

    void overlay()
    {
        SequenceSegment::Stream s1;
        SequenceSegment::Stream s2;
        SequenceName u1("_", "raw", "T_S11");
        SequenceName u2("brw", "raw", "T_S11");
        SequenceSegment::Transfer l;

        s1.add(SequenceValue(u1, Variant::Root(1.0), 2.0, 10.0));
        Util::append(s2.add(SequenceValue(u2, Variant::Root(2.0), 2.0, 4.0)), l);
        s2.overlay(s1);
        Util::append(s2.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 2.0);
        QCOMPARE(l[1].getStart(), 4.0);
        QCOMPARE(l[1].getEnd(), 10.0);
        QCOMPARE(l[1].getValue(u2).toDouble(), 1.0);

        s1.clear();
        s2.clear();
        l.clear();
        s1.add(SequenceValue(u1, Variant::Root(1.0), 2.0, 4.0));
        Util::append(s2.add(SequenceValue(u2, Variant::Root(2.0), 2.0, 4.0)), l);
        s2.overlay(s1);
        Util::append(s2.finish(), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 2.0);

        s1.clear();
        s2.clear();
        l.clear();
        s1.add(SequenceValue(u1, Variant::Root(1.0), 1.0, 10.0));
        Util::append(s2.add(SequenceValue(u2, Variant::Root(2.0), 2.0, 4.0)), l);
        s2.overlay(s1);
        Util::append(s2.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 2.0);
        QCOMPARE(l[1].getStart(), 4.0);
        QCOMPARE(l[1].getEnd(), 10.0);
        QCOMPARE(l[1].getValue(u2).toDouble(), 1.0);

        s1.clear();
        s2.clear();
        l.clear();
        s1.add(SequenceValue(u1, Variant::Root(1.0), 1.0, 10.0));
        s2.advance(2.0);
        Util::append(s2.add(SequenceValue(u2, Variant::Root(2.0), 2.0, 4.0)), l);
        s2.overlay(s1);
        Util::append(s2.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 2.0);
        QCOMPARE(l[1].getStart(), 4.0);
        QCOMPARE(l[1].getEnd(), 10.0);
        QCOMPARE(l[1].getValue(u2).toDouble(), 1.0);

        s1.clear();
        s2.clear();
        l.clear();
        s1.add(SequenceValue(u1, Variant::Root(1.0), 1.0, 4.0));
        Util::append(s2.add(SequenceValue(u2, Variant::Root(2.0), 2.0, 4.0)), l);
        s2.overlay(s1);
        Util::append(s2.finish(), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 2.0);

        s1.clear();
        s2.clear();
        l.clear();
        s1.add(SequenceValue(u1, Variant::Root(1.0), 1.0, 4.0));
        s1.advance(2.0);
        Util::append(s2.add(SequenceValue(u2, Variant::Root(2.0), 2.0, 4.0)), l);
        s2.overlay(s1);
        Util::append(s2.finish(), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 2.0);
    }

    void overlayFlat()
    {
        ValueSegment::Stream s1;
        ValueSegment::Stream s2;
        SequenceName u1("_", "raw", "T_S11");
        SequenceName u2("brw", "raw", "T_S11");
        ValueSegment::Transfer l;

        s1.add(SequenceValue(u1, Variant::Root(1.0), 2.0, 10.0));
        Util::append(s2.add(SequenceValue(u2, Variant::Root(2.0), 2.0, 4.0)), l);
        s2.overlay(s1);
        Util::append(s2.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue().toDouble(), 2.0);
        QCOMPARE(l[1].getStart(), 4.0);
        QCOMPARE(l[1].getEnd(), 10.0);
        QCOMPARE(l[1].getValue().toDouble(), 1.0);

        s1.clear();
        s2.clear();
        l.clear();
        s1.add(SequenceValue(u1, Variant::Root(1.0), 2.0, 4.0));
        Util::append(s2.add(SequenceValue(u2, Variant::Root(2.0), 2.0, 4.0)), l);
        s2.overlay(s1);
        Util::append(s2.finish(), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue().toDouble(), 2.0);

        s1.clear();
        s2.clear();
        l.clear();
        s1.add(SequenceValue(u1, Variant::Root(1.0), 1.0, 10.0));
        Util::append(s2.add(SequenceValue(u2, Variant::Root(2.0), 2.0, 4.0)), l);
        s2.overlay(s1);
        Util::append(s2.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue().toDouble(), 2.0);
        QCOMPARE(l[1].getStart(), 4.0);
        QCOMPARE(l[1].getEnd(), 10.0);
        QCOMPARE(l[1].getValue().toDouble(), 1.0);

        s1.clear();
        s2.clear();
        l.clear();
        s1.add(SequenceValue(u1, Variant::Root(1.0), 1.0, 10.0));
        s2.advance(2.0);
        Util::append(s2.add(SequenceValue(u2, Variant::Root(2.0), 2.0, 4.0)), l);
        s2.overlay(s1);
        Util::append(s2.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue().toDouble(), 2.0);
        QCOMPARE(l[1].getStart(), 4.0);
        QCOMPARE(l[1].getEnd(), 10.0);
        QCOMPARE(l[1].getValue().toDouble(), 1.0);

        s1.clear();
        s2.clear();
        l.clear();
        s1.add(SequenceValue(u1, Variant::Root(1.0), 1.0, 4.0));
        Util::append(s2.add(SequenceValue(u2, Variant::Root(2.0), 2.0, 4.0)), l);
        s2.overlay(s1);
        Util::append(s2.finish(), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue().toDouble(), 2.0);

        s1.clear();
        s2.clear();
        l.clear();
        s1.add(SequenceValue(u1, Variant::Root(1.0), 1.0, 4.0));
        s1.advance(2.0);
        Util::append(s2.add(SequenceValue(u2, Variant::Root(2.0), 2.0, 4.0)), l);
        s2.overlay(s1);
        Util::append(s2.finish(), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue().toDouble(), 2.0);
    }

    void overlaySingleValue()
    {
        SequenceSegment::Transfer l;
        SequenceSegment::Stream sr;
        SequenceName u1("_", "raw", "T_S11");
        SequenceName u2("brw", "raw", "T_S11");

        sr.overlay(SequenceValue(u1, Variant::Root(1.0), 2.0, 4.0));
        Util::append(sr.add(SequenceValue(u2, Variant::Root(2.0), 2.0, 4.0)), l);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 2.0);

        sr.clear();
        l.clear();
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 2.0, 4.0));
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 1.0);

        sr.clear();
        l.clear();
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 2.0, 10.0));
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 1.0);
        QCOMPARE(l[1].getStart(), 4.0);
        QCOMPARE(l[1].getEnd(), 10.0);
        QCOMPARE(l[1].getValue(u2).toDouble(), 1.0);

        sr.clear();
        l.clear();
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 2.0, 10.0));
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 1.0);
        QCOMPARE(l[1].getStart(), 4.0);
        QCOMPARE(l[1].getEnd(), 10.0);
        QCOMPARE(l[1].getValue(u2).toDouble(), 1.0);

        sr.clear();
        l.clear();
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 1.0, 4.0));
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 1.0);
        QCOMPARE(l[0].getEnd(), 2.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 1.0);
        QCOMPARE(l[1].getStart(), 2.0);
        QCOMPARE(l[1].getEnd(), 4.0);
        QCOMPARE(l[1].getValue(u2).toDouble(), 1.0);

        sr.clear();
        l.clear();
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 1.0, 4.0));
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 1.0);

        sr.clear();
        l.clear();
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 1.0, 10.0));
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 3);
        QCOMPARE(l[0].getStart(), 1.0);
        QCOMPARE(l[0].getEnd(), 2.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 1.0);
        QCOMPARE(l[1].getStart(), 2.0);
        QCOMPARE(l[1].getEnd(), 4.0);
        QCOMPARE(l[1].getValue(u2).toDouble(), 1.0);
        QCOMPARE(l[2].getStart(), 4.0);
        QCOMPARE(l[2].getEnd(), 10.0);
        QCOMPARE(l[2].getValue(u2).toDouble(), 1.0);

        sr.clear();
        l.clear();
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 1.0, 10.0));
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 1.0);
        QCOMPARE(l[1].getStart(), 4.0);
        QCOMPARE(l[1].getEnd(), 10.0);
        QCOMPARE(l[1].getValue(u2).toDouble(), 1.0);

        sr.clear();
        l.clear();
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 1.0, 2.0));
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue(u2).toDouble(), 2.0);
    }

    void overlaySingleValueFlat()
    {
        ValueSegment::Transfer l;
        ValueSegment::Stream sr;
        SequenceName u1("_", "raw", "T_S11");
        SequenceName u2("brw", "raw", "T_S11");

        sr.overlay(SequenceValue(u1, Variant::Root(1.0), 2.0, 4.0));
        Util::append(sr.add(SequenceValue(u2, Variant::Root(2.0), 2.0, 4.0)), l);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue().toDouble(), 2.0);

        sr.clear();
        l.clear();
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 2.0, 4.0));
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue().toDouble(), 1.0);

        sr.clear();
        l.clear();
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 2.0, 10.0));
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue().toDouble(), 1.0);
        QCOMPARE(l[1].getStart(), 4.0);
        QCOMPARE(l[1].getEnd(), 10.0);
        QCOMPARE(l[1].getValue().toDouble(), 1.0);

        sr.clear();
        l.clear();
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 2.0, 10.0));
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue().toDouble(), 1.0);
        QCOMPARE(l[1].getStart(), 4.0);
        QCOMPARE(l[1].getEnd(), 10.0);
        QCOMPARE(l[1].getValue().toDouble(), 1.0);

        sr.clear();
        l.clear();
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 1.0, 4.0));
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 1.0);
        QCOMPARE(l[0].getEnd(), 2.0);
        QCOMPARE(l[0].getValue().toDouble(), 1.0);
        QCOMPARE(l[1].getStart(), 2.0);
        QCOMPARE(l[1].getEnd(), 4.0);
        QCOMPARE(l[1].getValue().toDouble(), 1.0);

        sr.clear();
        l.clear();
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 1.0, 4.0));
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue().toDouble(), 1.0);

        sr.clear();
        l.clear();
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 1.0, 10.0));
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 3);
        QCOMPARE(l[0].getStart(), 1.0);
        QCOMPARE(l[0].getEnd(), 2.0);
        QCOMPARE(l[0].getValue().toDouble(), 1.0);
        QCOMPARE(l[1].getStart(), 2.0);
        QCOMPARE(l[1].getEnd(), 4.0);
        QCOMPARE(l[1].getValue().toDouble(), 1.0);
        QCOMPARE(l[2].getStart(), 4.0);
        QCOMPARE(l[2].getEnd(), 10.0);
        QCOMPARE(l[2].getValue().toDouble(), 1.0);

        sr.clear();
        l.clear();
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 1.0, 10.0));
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue().toDouble(), 1.0);
        QCOMPARE(l[1].getStart(), 4.0);
        QCOMPARE(l[1].getEnd(), 10.0);
        QCOMPARE(l[1].getValue().toDouble(), 1.0);

        sr.clear();
        l.clear();
        Util::append(sr.add(SequenceValue(u1, Variant::Root(2.0), 2.0, 4.0)), l);
        sr.overlay(SequenceValue(u2, Variant::Root(1.0), 1.0, 2.0));
        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 4.0);
        QCOMPARE(l[0].getValue().toDouble(), 2.0);
    }

    void breaks()
    {
        SequenceSegment::Transfer l;
        SequenceSegment::Stream sr;
        SequenceName unit("brw", "raw", "T_S11");

        sr.setBreaks(QSet<double>() << 2.0 << 2.5 << 3.0 << 3.5 << 4.25 << 4.75);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.0), 1.0, 2.0)), l);
        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.5), 1.5, 2.0)), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 1.0);
        QCOMPARE(l[0].getEnd(), 1.5);
        QCOMPARE(l[0].getValue(unit).toDouble(), 1.0);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.0), 2.0, 2.75)), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[1].getStart(), 1.5);
        QCOMPARE(l[1].getEnd(), 2.0);
        QCOMPARE(l[1].getValue(unit).toDouble(), 1.5);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.75), 2.75, 3.0)), l);
        QCOMPARE((int) l.size(), 4);
        QCOMPARE(l[2].getStart(), 2.0);
        QCOMPARE(l[2].getEnd(), 2.5);
        QCOMPARE(l[2].getValue(unit).toDouble(), 2.0);
        QCOMPARE(l[3].getStart(), 2.5);
        QCOMPARE(l[3].getEnd(), 2.75);
        QCOMPARE(l[3].getValue(unit).toDouble(), 2.0);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(3.0), 3.0, 4.1)), l);
        QCOMPARE((int) l.size(), 5);
        QCOMPARE(l[4].getStart(), 2.75);
        QCOMPARE(l[4].getEnd(), 3.0);
        QCOMPARE(l[4].getValue(unit).toDouble(), 2.75);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(4.0), 4.0, 5.0)), l);
        QCOMPARE((int) l.size(), 7);
        QCOMPARE(l[5].getStart(), 3.0);
        QCOMPARE(l[5].getEnd(), 3.5);
        QCOMPARE(l[5].getValue(unit).toDouble(), 3.0);
        QCOMPARE(l[6].getStart(), 3.5);
        QCOMPARE(l[6].getEnd(), 4.0);
        QCOMPARE(l[6].getValue(unit).toDouble(), 3.0);

        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 11);
        QCOMPARE(l[7].getStart(), 4.0);
        QCOMPARE(l[7].getEnd(), 4.1);
        QCOMPARE(l[7].getValue(unit).toDouble(), 4.0);
        QCOMPARE(l[8].getStart(), 4.1);
        QCOMPARE(l[8].getEnd(), 4.25);
        QCOMPARE(l[8].getValue(unit).toDouble(), 4.0);
        QCOMPARE(l[9].getStart(), 4.25);
        QCOMPARE(l[9].getEnd(), 4.75);
        QCOMPARE(l[9].getValue(unit).toDouble(), 4.0);
        QCOMPARE(l[10].getStart(), 4.75);
        QCOMPARE(l[10].getEnd(), 5.0);
        QCOMPARE(l[10].getValue(unit).toDouble(), 4.0);


        sr.clear();
        l.clear();
        sr.setBreaks(QSet<double>() << 2.0 << 2.5 << 3.0 << 3.5 << 4.25 << 4.75);
        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.0), 2.0, 3.0)), l);
        QCOMPARE((int) l.size(), 0);

        Util::append(sr.advance(2.25), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 2.5);
        QCOMPARE(l[0].getValue(unit).toDouble(), 2.0);

        Util::append(sr.advance(2.6), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[1].getStart(), 2.5);
        QCOMPARE(l[1].getEnd(), 3.0);
        QCOMPARE(l[1].getValue(unit).toDouble(), 2.0);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(3.0), 3.0, 5.0)), l);
        QCOMPARE((int) l.size(), 2);
        Util::append(sr.advance(3.25), l);
        QCOMPARE((int) l.size(), 3);
        QCOMPARE(l[2].getStart(), 3.0);
        QCOMPARE(l[2].getEnd(), 3.5);
        QCOMPARE(l[2].getValue(unit).toDouble(), 3.0);

        Util::append(sr.advance(3.5), l);
        QCOMPARE((int) l.size(), 3);
        Util::append(sr.advance(3.75), l);
        QCOMPARE((int) l.size(), 4);
        QCOMPARE(l[3].getStart(), 3.5);
        QCOMPARE(l[3].getEnd(), 4.25);
        QCOMPARE(l[3].getValue(unit).toDouble(), 3.0);

        Util::append(sr.advance(5.0), l);
        QCOMPARE((int) l.size(), 6);
        QCOMPARE(l[4].getStart(), 4.25);
        QCOMPARE(l[4].getEnd(), 4.75);
        QCOMPARE(l[4].getValue(unit).toDouble(), 3.0);
        QCOMPARE(l[5].getStart(), 4.75);
        QCOMPARE(l[5].getEnd(), 5.0);
        QCOMPARE(l[5].getValue(unit).toDouble(), 3.0);

        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 6);

        l.clear();
        sr.clear();
        sr.setBreaks(QSet<double>{0.5, 1.0, 1.5, 3.5, 4.0, 5.0});
        Util::append(sr.add(SequenceIdentity(unit, 1.0, 2.0), Variant::Root(1.0)), l);
        Util::append(sr.advance(1.0), l);
        Util::append(sr.advance(1.0), l);
        QCOMPARE((int) l.size(), 0);

        Util::append(sr.add(SequenceIdentity(unit, 2.0, 3.0), Variant::Root(2.0)), l);
        Util::append(sr.add(SequenceIdentity(unit, 3.0, 4.0), Variant::Root(3.0)), l);
        QCOMPARE((int) l.size(), 3);
        QCOMPARE(l[0].getStart(), 1.0);
        QCOMPARE(l[0].getEnd(), 1.5);
        QCOMPARE(l[0].getValue(unit).toDouble(), 1.0);
        QCOMPARE(l[1].getStart(), 1.5);
        QCOMPARE(l[1].getEnd(), 2.0);
        QCOMPARE(l[1].getValue(unit).toDouble(), 1.0);
        QCOMPARE(l[2].getStart(), 2.0);
        QCOMPARE(l[2].getEnd(), 3.0);
        QCOMPARE(l[2].getValue(unit).toDouble(), 2.0);
        Util::append(sr.advance(3.0), l);
        QCOMPARE((int) l.size(), 3);

        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 5);
        QCOMPARE(l[3].getStart(), 3.0);
        QCOMPARE(l[3].getEnd(), 3.5);
        QCOMPARE(l[3].getValue(unit).toDouble(), 3.0);
        QCOMPARE(l[4].getStart(), 3.5);
        QCOMPARE(l[4].getEnd(), 4.0);
        QCOMPARE(l[4].getValue(unit).toDouble(), 3.0);
    }

    void breaksFlat()
    {
        ValueSegment::Transfer l;
        ValueSegment::Stream sr;
        SequenceName unit("brw", "raw", "T_S11");

        sr.setBreaks(QSet<double>() << 2.0 << 2.5 << 3.0 << 3.5 << 4.25 << 4.75);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.0), 1.0, 2.0)), l);
        Util::append(sr.add(SequenceValue(unit, Variant::Root(1.5), 1.5, 2.0)), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 1.0);
        QCOMPARE(l[0].getEnd(), 1.5);
        QCOMPARE(l[0].getValue().toDouble(), 1.0);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.0), 2.0, 2.75)), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[1].getStart(), 1.5);
        QCOMPARE(l[1].getEnd(), 2.0);
        QCOMPARE(l[1].getValue().toDouble(), 1.5);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.75), 2.75, 3.0)), l);
        QCOMPARE((int) l.size(), 4);
        QCOMPARE(l[2].getStart(), 2.0);
        QCOMPARE(l[2].getEnd(), 2.5);
        QCOMPARE(l[2].getValue().toDouble(), 2.0);
        QCOMPARE(l[3].getStart(), 2.5);
        QCOMPARE(l[3].getEnd(), 2.75);
        QCOMPARE(l[3].getValue().toDouble(), 2.0);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(3.0), 3.0, 4.1)), l);
        QCOMPARE((int) l.size(), 5);
        QCOMPARE(l[4].getStart(), 2.75);
        QCOMPARE(l[4].getEnd(), 3.0);
        QCOMPARE(l[4].getValue().toDouble(), 2.75);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(4.0), 4.0, 5.0)), l);
        QCOMPARE((int) l.size(), 7);
        QCOMPARE(l[5].getStart(), 3.0);
        QCOMPARE(l[5].getEnd(), 3.5);
        QCOMPARE(l[5].getValue().toDouble(), 3.0);
        QCOMPARE(l[6].getStart(), 3.5);
        QCOMPARE(l[6].getEnd(), 4.0);
        QCOMPARE(l[6].getValue().toDouble(), 3.0);

        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 11);
        QCOMPARE(l[7].getStart(), 4.0);
        QCOMPARE(l[7].getEnd(), 4.1);
        QCOMPARE(l[7].getValue().toDouble(), 4.0);
        QCOMPARE(l[8].getStart(), 4.1);
        QCOMPARE(l[8].getEnd(), 4.25);
        QCOMPARE(l[8].getValue().toDouble(), 4.0);
        QCOMPARE(l[9].getStart(), 4.25);
        QCOMPARE(l[9].getEnd(), 4.75);
        QCOMPARE(l[9].getValue().toDouble(), 4.0);
        QCOMPARE(l[10].getStart(), 4.75);
        QCOMPARE(l[10].getEnd(), 5.0);
        QCOMPARE(l[10].getValue().toDouble(), 4.0);


        sr.clear();
        l.clear();
        sr.setBreaks(QSet<double>() << 2.0 << 2.5 << 3.0 << 3.5 << 4.25 << 4.75);
        Util::append(sr.add(SequenceValue(unit, Variant::Root(2.0), 2.0, 3.0)), l);
        QCOMPARE((int) l.size(), 0);

        Util::append(sr.advance(2.25), l);
        QCOMPARE((int) l.size(), 1);
        QCOMPARE(l[0].getStart(), 2.0);
        QCOMPARE(l[0].getEnd(), 2.5);
        QCOMPARE(l[0].getValue().toDouble(), 2.0);

        Util::append(sr.advance(2.6), l);
        QCOMPARE((int) l.size(), 2);
        QCOMPARE(l[1].getStart(), 2.5);
        QCOMPARE(l[1].getEnd(), 3.0);
        QCOMPARE(l[1].getValue().toDouble(), 2.0);

        Util::append(sr.add(SequenceValue(unit, Variant::Root(3.0), 3.0, 5.0)), l);
        QCOMPARE((int) l.size(), 2);
        Util::append(sr.advance(3.25), l);
        QCOMPARE((int) l.size(), 3);
        QCOMPARE(l[2].getStart(), 3.0);
        QCOMPARE(l[2].getEnd(), 3.5);
        QCOMPARE(l[2].getValue().toDouble(), 3.0);

        Util::append(sr.advance(3.5), l);
        QCOMPARE((int) l.size(), 3);
        Util::append(sr.advance(3.75), l);
        QCOMPARE((int) l.size(), 4);
        QCOMPARE(l[3].getStart(), 3.5);
        QCOMPARE(l[3].getEnd(), 4.25);
        QCOMPARE(l[3].getValue().toDouble(), 3.0);

        Util::append(sr.advance(5.0), l);
        QCOMPARE((int) l.size(), 6);
        QCOMPARE(l[4].getStart(), 4.25);
        QCOMPARE(l[4].getEnd(), 4.75);
        QCOMPARE(l[4].getValue().toDouble(), 3.0);
        QCOMPARE(l[5].getStart(), 4.75);
        QCOMPARE(l[5].getEnd(), 5.0);
        QCOMPARE(l[5].getValue().toDouble(), 3.0);

        Util::append(sr.finish(), l);
        QCOMPARE((int) l.size(), 6);
    }

    void archiveRead()
    {
        SequenceValue::Transfer input
                {SequenceValue(SequenceName("brw", "config", "foo"), Variant::Root(1.0), 1.0, 2.0,
                               0),
                 SequenceValue(SequenceName("brw", "config", "foo"), Variant::Root(2.0), 2.0, 3.0,
                               0),
                 SequenceValue(SequenceName("brw", "config", "foo"), Variant::Root(3.0), 3.0, 4.0,
                               0),
                 SequenceValue(SequenceName("_", "config", "foo"), Variant::Root(4.0),
                               FP::undefined(), FP::undefined(), 1),
                 SequenceValue(SequenceName("brw", "config", "foo"), Variant::Root(5.0), 4.0,
                               FP::undefined(), -1),
                 SequenceValue(SequenceName("alt", "config", "foo"), Variant::Root(6.0), 2.0, 5.0,
                               0)};
        Archive::Access reader(databaseFile);
        reader.writeSynchronous(input);

        auto result = ValueSegment::Stream::read(
                Archive::Selection(FP::undefined(), FP::undefined(), {"brw"}, {"config"}, {"foo"}),
                &reader);
        QCOMPARE((int) result.size(), 5);
        QVERIFY(!FP::defined(result.at(0).getStart()));
        QCOMPARE(result.at(0).getEnd(), 1.0);
        QVERIFY(result.at(0).getValue() == Variant::Root(4.0));
        QCOMPARE(result.at(1).getStart(), 1.0);
        QCOMPARE(result.at(1).getEnd(), 2.0);
        QVERIFY(result.at(1).getValue() == Variant::Root(1.0));
        QCOMPARE(result.at(2).getStart(), 2.0);
        QCOMPARE(result.at(2).getEnd(), 3.0);
        QVERIFY(result.at(2).getValue() == Variant::Root(2.0));
        QCOMPARE(result.at(3).getStart(), 3.0);
        QCOMPARE(result.at(3).getEnd(), 4.0);
        QVERIFY(result.at(3).getValue() == Variant::Root(3.0));
        QCOMPARE(result.at(4).getStart(), 4.0);
        QVERIFY(!FP::defined(result.at(4).getEnd()));
        QVERIFY(result.at(4).getValue() == Variant::Root(5.0));


        result = ValueSegment::Stream::read(
                Archive::Selection(FP::undefined(), FP::undefined(), {"alt"}, {"config"}, {"foo"}),
                &reader);
        QCOMPARE((int) result.size(), 3);
        QVERIFY(!FP::defined(result.at(0).getStart()));
        QCOMPARE(result.at(0).getEnd(), 2.0);
        QVERIFY(result.at(0).getValue() == Variant::Root(4.0));
        QCOMPARE(result.at(1).getStart(), 2.0);
        QCOMPARE(result.at(1).getEnd(), 5.0);
        QVERIFY(result.at(1).getValue() == Variant::Root(6.0));
        QCOMPARE(result.at(2).getStart(), 5.0);
        QVERIFY(!FP::defined(result.at(2).getEnd()));
        QVERIFY(result.at(2).getValue() == Variant::Root(4.0));

        auto multiResult = SequenceSegment::Stream::read(Archive::Selection(), &reader);
        SequenceName brw("brw", "config", "foo");
        SequenceName alt("alt", "config", "foo");
        SequenceName aao("aao", "config", "foo");
        QCOMPARE((int) multiResult.size(), 6);
        QVERIFY(!FP::defined(multiResult.at(0).getStart()));
        QCOMPARE(multiResult.at(0).getEnd(), 1.0);
        QVERIFY(multiResult[0][brw] == Variant::Root(4.0));
        QVERIFY(multiResult[0][alt] == Variant::Root(4.0));
        QVERIFY(multiResult[0][aao] == Variant::Root(4.0));
        QCOMPARE(multiResult.at(1).getStart(), 1.0);
        QCOMPARE(multiResult.at(1).getEnd(), 2.0);
        QVERIFY(multiResult[1][brw] == Variant::Root(1.0));
        QVERIFY(multiResult[1][alt] == Variant::Root(4.0));
        QVERIFY(multiResult[1][aao] == Variant::Root(4.0));
        QCOMPARE(multiResult.at(2).getStart(), 2.0);
        QCOMPARE(multiResult.at(2).getEnd(), 3.0);
        QVERIFY(multiResult[2][brw] == Variant::Root(2.0));
        QVERIFY(multiResult[2][alt] == Variant::Root(6.0));
        QVERIFY(multiResult[2][aao] == Variant::Root(4.0));
        QCOMPARE(multiResult.at(3).getStart(), 3.0);
        QCOMPARE(multiResult.at(3).getEnd(), 4.0);
        QVERIFY(multiResult[3][brw] == Variant::Root(3.0));
        QVERIFY(multiResult[3][alt] == Variant::Root(6.0));
        QVERIFY(multiResult[3][aao] == Variant::Root(4.0));
        QCOMPARE(multiResult.at(4).getStart(), 4.0);
        QCOMPARE(multiResult.at(4).getEnd(), 5.0);
        QVERIFY(multiResult[4][brw] == Variant::Root(5.0));
        QVERIFY(multiResult[4][alt] == Variant::Root(6.0));
        QVERIFY(multiResult[4].getValue(QRegExp("aao"), QRegExp("config"), QRegExp("foo")) ==
                        Variant::Root(4.0));
        QVERIFY(multiResult[4][aao] == Variant::Root(4.0));
        QCOMPARE(multiResult.at(5).getStart(), 5.0);
        QVERIFY(!FP::defined(multiResult.at(5).getEnd()));
        QVERIFY(multiResult[5][brw] == Variant::Root(5.0));
        QVERIFY(multiResult[5][alt] == Variant::Root(4.0));
        QVERIFY(multiResult[5][aao] == Variant::Root(4.0));
        QVERIFY(multiResult[5].getValue(QRegExp("[s]gp"), QRegExp("config"), QRegExp("foo")) ==
                        Variant::Root(4.0));
    }

    void serialize()
    {
        SequenceSegment::Stream sr;
        SequenceSegment::Transfer l;

        Util::append(
                sr.add(SequenceValue(SequenceName("brw", "raw", "T_S11"), Variant::Root(1.0), 1.0,
                                     2.0, 0)), l);
        Util::append(sr.inject(
                SequenceValue(SequenceName("brw", "raw", "P_S11"), Variant::Root(1.5), 1.0, 2.0,
                              0)), l);
        Util::append(
                sr.add(SequenceValue(SequenceName("brw", "clean", "T_S11"), Variant::Root(2.0), 1.0,
                                     2.0, 0)), l);
        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "T_S11"), Variant::Root(3.0), 1.0,
                                     2.0, 1)), l);
        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "T_S11"), Variant::Root(4.0), 1.0,
                                     2.5, 0)), l);

        QByteArray data;
        QDataStream write(&data, QIODevice::WriteOnly);
        write << sr;
        sr.finish();
        QDataStream read(&data, QIODevice::ReadOnly);
        read >> sr;

        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "Tu_S11"), Variant::Root(5.0),
                                     1.0, 2.0, 0)), l);
        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "T_S11"), Variant::Root(-1.0),
                                     1.5, 2.0, -1)), l);
        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "T_S11"), Variant::Root(-2.0),
                                     1.75, 2.0, 2)), l);
        Util::append(sr.finish(), l);

        QCOMPARE((int) l.size(), 4);

        QCOMPARE(l.at(0).getStart(), 1.0);
        QCOMPARE(l.at(0).getEnd(), 1.5);
        QVERIFY(l[0].getValue(SequenceName("brw", "raw", "T_S11")) == Variant::Root(1.0));
        QVERIFY(l[0].getValue(SequenceName("brw", "raw", "P_S11")) == Variant::Root(1.5));
        QVERIFY(l[0].getValue(SequenceName("brw", "clean", "T_S11")) == Variant::Root(2.0));
        QVERIFY(l[0].getValue(SequenceName("alt", "clean", "T_S11")) == Variant::Root(3.0));
        QVERIFY(l[0].getValue(SequenceName("alt", "clean", "Tu_S11")) == Variant::Root(5.0));

        QCOMPARE(l[1].getStart(), 1.5);
        QCOMPARE(l[1].getEnd(), 1.75);
        QVERIFY(l[1].getValue(SequenceName("brw", "raw", "T_S11")) == Variant::Root(1.0));
        QVERIFY(l[1].getValue(SequenceName("brw", "clean", "T_S11")) == Variant::Root(2.0));
        QVERIFY(l[1].getValue(SequenceName("alt", "clean", "T_S11")) == Variant::Root(3.0));
        QVERIFY(l[1].getValue(SequenceName("alt", "clean", "Tu_S11")) == Variant::Root(5.0));

        QCOMPARE(l[2].getStart(), 1.75);
        QCOMPARE(l[2].getEnd(), 2.0);
        QVERIFY(l[2].getValue(SequenceName("brw", "raw", "T_S11")) == Variant::Root(1.0));
        QVERIFY(l[2].getValue(SequenceName("brw", "clean", "T_S11")) == Variant::Root(2.0));
        QVERIFY(l[2].getValue(SequenceName("alt", "clean", "T_S11")) == Variant::Root(-2.0));
        QVERIFY(l[2].getValue(SequenceName("alt", "clean", "Tu_S11")) == Variant::Root(5.0));

        QCOMPARE(l[3].getStart(), 2.0);
        QCOMPARE(l[3].getEnd(), 2.5);
        QVERIFY(l[3].getValue(SequenceName("alt", "clean", "T_S11")) == Variant::Root(4.0));
    }

    void serializeFlat()
    {
        ValueSegment::Stream sr;
        ValueSegment::Transfer l;

        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "T_S11"), Variant::Root(2.0), 1.0,
                                     2.0, 0)), l);
        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "T_S11"), Variant::Root(3.0), 1.0,
                                     2.0, 1)), l);
        Util::append(sr.inject(
                SequenceValue(SequenceName("alt", "clean", "T_S11"), Variant::Root(3.5), 1.0, 2.0,
                              -1)), l);
        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "T_S11"), Variant::Root(4.0), 1.0,
                                     2.5, 0)), l);

        QByteArray data;
        QDataStream write(&data, QIODevice::WriteOnly);
        write << sr;
        sr.finish();
        QDataStream read(&data, QIODevice::ReadOnly);
        read >> sr;

        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "T_S11"), Variant::Root(-1.0),
                                     1.5, 2.0, -1)), l);
        Util::append(
                sr.add(SequenceValue(SequenceName("alt", "clean", "T_S11"), Variant::Root(-2.0),
                                     1.75, 2.0, 2)), l);
        Util::append(sr.finish(), l);

        QCOMPARE((int) l.size(), 4);

        QCOMPARE(l.at(0).getStart(), 1.0);
        QCOMPARE(l.at(0).getEnd(), 1.5);
        QVERIFY(l[0].getValue() == Variant::Root(3.0));

        QCOMPARE(l[1].getStart(), 1.5);
        QCOMPARE(l[1].getEnd(), 1.75);
        QVERIFY(l[1].getValue() == Variant::Root(3.0));

        QCOMPARE(l[2].getStart(), 1.75);
        QCOMPARE(l[2].getEnd(), 2.0);
        QVERIFY(l[2].getValue() == Variant::Root(-2.0));

        QCOMPARE(l[3].getStart(), 2.0);
        QCOMPARE(l[3].getEnd(), 2.5);
        QVERIFY(l[3].getValue() == Variant::Root(4.0));
    }
};

QTEST_APPLESS_MAIN(TestSegment)

#include "segment.moc"
