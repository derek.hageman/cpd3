/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtSql>
#include <QString>

#include "datacore/sequencematch.hxx"
#include "datacore/archive/selection.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Data::SequenceMatch;
using namespace CPD3::Data::Archive;

class TestSequenceMatchBasic : public QObject {
Q_OBJECT

    static bool match(const std::unique_ptr<Basic> &check,
                      const SequenceName::Component &station,
                      const SequenceName::Component &archive,
                      const SequenceName::Component &variable)
    { return check->matches(SequenceName(station, archive, variable)); }

    static bool cmatch(const std::unique_ptr<Basic> &check,
                       const SequenceName::Component &station,
                       const SequenceName::Component &archive,
                       const SequenceName::Component &variable,
                       double point)
    {
        return const_cast<const Basic &>(*check).matches(SequenceName(station, archive, variable),
                                                         point);
    }

    static bool match(const std::unique_ptr<Basic> &check,
                      const SequenceName::Component &station,
                      const SequenceName::Component &archive,
                      const SequenceName::Component &variable,
                      double point)
    {
        return const_cast<Basic &>(*check).matches(SequenceName(station, archive, variable), point);
    }

    static bool cmatch(const std::unique_ptr<Basic> &check,
                       const SequenceName::Component &station,
                       const SequenceName::Component &archive,
                       const SequenceName::Component &variable,
                       double start,
                       double end)
    {
        return const_cast<const Basic &>(*check).matches(SequenceName(station, archive, variable),
                                                         start, end);
    }

    static bool match(const std::unique_ptr<Basic> &check,
                      const SequenceName::Component &station,
                      const SequenceName::Component &archive,
                      const SequenceName::Component &variable,
                      double start,
                      double end)
    {
        return const_cast<Basic &>(*check).matches(SequenceName(station, archive, variable), start,
                                                   end);
    }

private slots:

    void compileArchiveSelection()
    {
        auto el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bnD"}, {"raW"}, {"BsB_S11"})});
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("_", "raw_meta", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("_", "clean_meta", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("alt", "clean", "BsG_S11")));

        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bn[D]"}, {"ra+W"}, {"BsB_S\\d+"})});
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("_", "raw_meta", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("_", "clean_meta", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("alt", "clean", "BsG_S11")));

        el = Basic::compileToElements({Selection(FP::undefined(), FP::undefined(), {"bnD"}, {"raW"},
                                                 {"BsB_S11"}).withMetaArchive(false)
                                                             .withDefaultStation(false)});
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw_meta", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("_", "raw", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("alt", "clean", "BsG_S11")));

        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bn[D]"}, {"ra+W"},
                           {"BsB_S\\d+"}).withMetaArchive(false).withDefaultStation(false)});
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw_meta", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("_", "raw", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("alt", "clean", "BsG_S11")));

        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bnd", "THD"}, {"raw", "AVGH"},
                           {"BsB_S11", "BsG_S11"})});
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw_meta", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("thd", "avgh", "BsG_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("_", "avgh_meta", "BsG_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("_", "clean_meta", "BsG_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("alt", "clean", "BsG_S11")));

        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bnd?", "T[H]D"},
                           {"r+aw", "AVGH{1,2}"}, {"BsB_S\\d+", "(BsG)+_S11"})});
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw_meta", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("thd", "avgh", "BsG_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("_", "avgh_meta", "BsG_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("_", "clean_meta", "BsG_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("alt", "clean", "BsG_S11")));

        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bnd", "THD"}, {"raw", "AVGH"},
                           {"BsB_S11", "BsG_S11"}).withMetaArchive(false)
                                                  .withDefaultStation(false)});
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw_meta", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("thd", "avgh", "BsG_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("_", "avgh", "BsG_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("_", "clean_meta", "BsG_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("alt", "clean", "BsG_S11")));

        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bnd?", "T[H]D"},
                           {"r+aw", "AVGH{1,2}"}, {"BsB_S\\d+", "(BsG)+_S11"}).withMetaArchive(
                        false).withDefaultStation(false)});
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw_meta", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("thd", "avgh", "BsG_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("_", "avgh", "BsG_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("_", "clean_meta", "BsG_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "Bsb_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("alt", "clean", "BsG_S11")));

        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bnD"}, {"raW"}, {"BsB_S11"}, {"pM1"},
                           {"PM10"})});
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm1"})));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm10"})));
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm1", "pm25"})));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm1", "pm10"})));

        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bnD"}, {"raW"}, {"BsB_S11"},
                           {"pM1+"}, {"PM1+0"})});
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm1"})));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm10"})));
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm1", "pm25"})));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm1", "pm10"})));

        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bnD"}, {"raW"}, {"BsB_S11"}, {}, {},
                           {"pM1"})});
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm1"})));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm10"})));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm1", "pm10"})));

        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bnD"}, {"raW"}, {"BsB_S11"}, {}, {},
                           {"PM[12]"})});
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm1"})));
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm2"})));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm10"})));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm1", "pm10"})));

        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {""}, {"a"}, {"b"})});
        QVERIFY(Element::matchesAny(el, SequenceName("_", "a", "b")));
        QVERIFY(!Element::matchesAny(el, SequenceName("d", "a", "b")));

        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"a"}, {""}, {"b"})});
        QVERIFY(!Element::matchesAny(el, SequenceName("a", "c", "d")));

        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"a"}, {"b"}, {""})});
        QVERIFY(!Element::matchesAny(el, SequenceName("a", "b", "c")));

        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {}, {}, {}).withDefaultStation(false)
                                                                        .withMetaArchive(false)});
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "clean", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("_", "raw_meta", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "clean_meta", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("_", "clean", "BsB_S11")));
    }

    void ignoreArchiveSelection()
    {
        Basic::CompileOptions options;
        options.ignoreFlavors = true;

        auto el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bnd"}, {"raw"}, {"BsB_S11"}, {}, {},
                           {"pm1"})}, options);
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm1"})));
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11", {"pm10"})));
        QVERIFY(!Element::matchesAny(el, SequenceName("alt", "clean", "BsG_S11")));

        options = Basic::CompileOptions();
        options.ignoreStation = true;
        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bnd"}, {"raw"}, {"BsB_S11"})},
                options);
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("alt", "raw", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("alt", "raw_meta", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("_", "raw", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "clean", "BsG_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("bnd", "clean_meta", "BsG_S11")));

        options = Basic::CompileOptions();
        options.ignoreArchive = true;
        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bnd"}, {"raw"}, {"BsB_S11"})},
                options);
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "clean", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw_meta", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("_", "clean_meta", "BsB_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("alt", "raw", "BsB_S11")));

        options = Basic::CompileOptions();
        options.ignoreVariable = true;
        el = Basic::compileToElements(
                {Selection(FP::undefined(), FP::undefined(), {"bnd"}, {"raw"}, {"BsB_S11"})},
                options);
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsB_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("bnd", "raw", "BsG_S11")));
        QVERIFY(Element::matchesAny(el, SequenceName("_", "raw_meta", "BsG_S11")));
        QVERIFY(!Element::matchesAny(el, SequenceName("alt", "clean", "BsG_S11")));
    }

    void empty()
    {
        auto i = Basic::compile(Selection::List());
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11"));
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 1000.0, 2000.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 1000.0, 2000.0));
        i->advance(1000.0);
        auto j = i->clone();
        i.reset();
        QVERIFY(!match(j, "bnd", "raw", "BsG_S11"));
        QVERIFY(!cmatch(j, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!cmatch(j, "bnd", "raw", "BsG_S11", 1000.0, 2000.0));
        QVERIFY(!match(j, "bnd", "raw", "BsG_S11", 1000.0, 2000.0));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        j.reset();
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11"));
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 1000.0, 2000.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 1000.0, 2000.0));
        i.reset();


        i = Basic::compile(Selection::List{Selection(100, 200, {"bnd"}, {"raw"}, {"BsG_S11"})},
                           1000.0, 2000.0);
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11"));
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 1000.0, 2000.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 1000.0, 2000.0));
        i->advance(1000.0);
        j = i->clone();
        i.reset();
        QVERIFY(!match(j, "bnd", "raw", "BsG_S11"));
        QVERIFY(!cmatch(j, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!match(j, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!cmatch(j, "bnd", "raw", "BsG_S11", 1000.0, 2000.0));
        QVERIFY(!match(j, "bnd", "raw", "BsG_S11", 1000.0, 2000.0));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        j.reset();
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11"));
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 1000.0, 2000.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 1000.0, 2000.0));
        i.reset();


        j.reset();
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        QVERIFY(!i);
    }

    void singleSegmentAlways()
    {
        auto i = Basic::compile(
                Selection::List{Selection(1000.0, 2500.0, {"bnd"}, {"raw"}, {"BsG_S11"})}, 1000.0,
                2500.0);
        QVERIFY(match(i, "bnd", "raw", "BsG_S11"));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11"));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!cmatch(i, "alt", "clean", "BsB_S11", 1000.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11", 1000.0));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1100.0, 2500.0));
        QVERIFY(!cmatch(i, "alt", "clean", "BsB_S11", 1100.0, 2500.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1100.0, 2500.0));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11", 1100.0, 2500.0));
        i->advance(1200.0);
        auto j = i->clone();
        i.reset();
        QVERIFY(match(j, "bnd", "raw", "BsG_S11"));
        QVERIFY(!match(j, "alt", "clean", "BsB_S11"));
        QVERIFY(cmatch(j, "bnd", "raw", "BsG_S11", 1300.0));
        QVERIFY(!cmatch(j, "alt", "clean", "BsB_S11", 1300.0));
        QVERIFY(match(j, "bnd", "raw", "BsG_S11", 1300.0));
        QVERIFY(!match(j, "alt", "clean", "BsB_S11", 1300.0));
        QVERIFY(cmatch(j, "bnd", "raw", "BsG_S11", 1400.0, 2500.0));
        QVERIFY(!cmatch(j, "alt", "clean", "BsB_S11", 1400.0, 2500.0));
        QVERIFY(match(j, "bnd", "raw", "BsG_S11", 1400.0, 2500.0));
        QVERIFY(!match(j, "alt", "clean", "BsB_S11", 1400.0, 2500.0));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        j.reset();
        QVERIFY(match(i, "bnd", "raw", "BsG_S11"));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11"));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1500.0));
        QVERIFY(!cmatch(i, "alt", "clean", "BsB_S11", 1500.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1500.0));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11", 1500.0));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1500.0, 2500.0));
        QVERIFY(!cmatch(i, "alt", "clean", "BsB_S11", 1500.0, 2500.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1500.0, 2500.0));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11", 1500.0, 2500.0));
        i->advance(2500);
    }

    void singleSegmentAlwaysMultiple()
    {
        auto i = Basic::compile(
                Selection::List{Selection(1000.0, 2500.0, {"bnd"}, {"raw"}, {"BsG_S11"}),
                                Selection(1000.0, 2500.0, {"bnd"}, {"clean"}, {"BsG_S11"})}, 1000.0,
                2500.0);
        QVERIFY(match(i, "bnd", "raw", "BsG_S11"));
        QVERIFY(match(i, "bnd", "clean", "BsG_S11"));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11"));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(cmatch(i, "bnd", "clean", "BsG_S11", 1000.0));
        QVERIFY(!cmatch(i, "alt", "clean", "BsB_S11", 1000.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(match(i, "bnd", "clean", "BsG_S11", 1000.0));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11", 1000.0));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1100.0, 2500.0));
        QVERIFY(cmatch(i, "bnd", "clean", "BsG_S11", 1100.0, 2500.0));
        QVERIFY(!cmatch(i, "alt", "clean", "BsB_S11", 1100.0, 2500.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1100.0, 2500.0));
        QVERIFY(match(i, "bnd", "clean", "BsG_S11", 1100.0, 2500.0));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11", 1100.0, 2500.0));
        i->advance(1200.0);
        auto j = i->clone();
        i.reset();
        QVERIFY(match(j, "bnd", "raw", "BsG_S11"));
        QVERIFY(match(j, "bnd", "clean", "BsG_S11"));
        QVERIFY(!match(j, "alt", "clean", "BsB_S11"));
        QVERIFY(cmatch(j, "bnd", "raw", "BsG_S11", 1300.0));
        QVERIFY(cmatch(j, "bnd", "clean", "BsG_S11", 1300.0));
        QVERIFY(!cmatch(j, "alt", "clean", "BsB_S11", 1300.0));
        QVERIFY(match(j, "bnd", "raw", "BsG_S11", 1300.0));
        QVERIFY(match(j, "bnd", "clean", "BsG_S11", 1300.0));
        QVERIFY(!match(j, "alt", "clean", "BsB_S11", 1300.0));
        QVERIFY(cmatch(j, "bnd", "raw", "BsG_S11", 1400.0, 2500.0));
        QVERIFY(cmatch(j, "bnd", "clean", "BsG_S11", 1400.0, 2500.0));
        QVERIFY(!cmatch(j, "alt", "clean", "BsB_S11", 1400.0, 2500.0));
        QVERIFY(match(j, "bnd", "raw", "BsG_S11", 1400.0, 2500.0));
        QVERIFY(match(j, "bnd", "clean", "BsG_S11", 1400.0, 2500.0));
        QVERIFY(!match(j, "alt", "clean", "BsB_S11", 1400.0, 2500.0));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        j.reset();
        QVERIFY(match(i, "bnd", "raw", "BsG_S11"));
        QVERIFY(match(i, "bnd", "clean", "BsG_S11"));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11"));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1500.0));
        QVERIFY(cmatch(i, "bnd", "clean", "BsG_S11", 1500.0));
        QVERIFY(!cmatch(i, "alt", "clean", "BsB_S11", 1500.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1500.0));
        QVERIFY(match(i, "bnd", "clean", "BsG_S11", 1500.0));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11", 1500.0));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1500.0, 2500.0));
        QVERIFY(cmatch(i, "bnd", "clean", "BsG_S11", 1500.0, 2500.0));
        QVERIFY(!cmatch(i, "alt", "clean", "BsB_S11", 1500.0, 2500.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1500.0, 2500.0));
        QVERIFY(match(i, "bnd", "clean", "BsG_S11", 1500.0, 2500.0));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11", 1500.0, 2500.0));
        i->advance(2500);
    }

    void singleSegmentUnbounded()
    {
        auto i = Basic::compile(
                Selection::List{Selection(1000.0, 2500.0, {"bnd"}, {"raw"}, {"BsG_S11"})});
        QVERIFY(match(i, "bnd", "raw", "BsG_S11"));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11"));
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 100.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 100.0));
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 100.0, 200.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 200.0, 300.0));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!cmatch(i, "alt", "clean", "BsB_S11", 1000.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11", 1000.0));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1100.0, 2500.0));
        QVERIFY(!cmatch(i, "alt", "clean", "BsB_S11", 1100.0, 2500.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1100.0, 2500.0));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11", 1100.0, 2500.0));
        i->advance(1200.0);
        auto j = i->clone();
        i.reset();
        QVERIFY(match(j, "bnd", "raw", "BsG_S11"));
        QVERIFY(!match(j, "alt", "clean", "BsB_S11"));
        QVERIFY(cmatch(j, "bnd", "raw", "BsG_S11", 1300.0));
        QVERIFY(!cmatch(j, "alt", "clean", "BsB_S11", 1300.0));
        QVERIFY(match(j, "bnd", "raw", "BsG_S11", 1300.0));
        QVERIFY(!match(j, "alt", "clean", "BsB_S11", 1300.0));
        QVERIFY(cmatch(j, "bnd", "raw", "BsG_S11", 1400.0, 2500.0));
        QVERIFY(!cmatch(j, "alt", "clean", "BsB_S11", 1400.0, 2500.0));
        QVERIFY(match(j, "bnd", "raw", "BsG_S11", 1400.0, 2500.0));
        QVERIFY(!match(j, "alt", "clean", "BsB_S11", 1400.0, 2500.0));
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> i;
            }
        }
        j.reset();
        QVERIFY(match(i, "bnd", "raw", "BsG_S11"));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11"));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1500.0));
        QVERIFY(!cmatch(i, "alt", "clean", "BsB_S11", 1500.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1500.0));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11", 1500.0));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1500.0, 2500.0));
        QVERIFY(!cmatch(i, "alt", "clean", "BsB_S11", 1500.0, 2500.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1500.0, 2500.0));
        QVERIFY(!match(i, "alt", "clean", "BsB_S11", 1500.0, 2500.0));
        i->advance(2500);
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 2500.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 2600.0));
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 2600.0, 2700.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 2700.0, 2800.0));
        i.reset();
    }

    void multipleSegment()
    {
        auto i = Basic::compile(
                Selection::List{Selection(1000.0, 2500.0, {"bnd"}, {"raw"}, {"BsG_S11"}),
                                Selection(1800.0, 3000.0, {"thd"}, {"avgh"}, {"BsB_S11"})});
        QVERIFY(match(i, "bnd", "raw", "BsG_S11"));
        QVERIFY(match(i, "thd", "avgh", "BsB_S11"));
        QVERIFY(!match(i, "thd", "avgh", "BsG_S11"));

        QVERIFY(cmatch(i, "thd", "avgh", "BsB_S11", FP::undefined(), FP::undefined()));
        QVERIFY(match(i, "thd", "avgh", "BsB_S11", FP::undefined(), FP::undefined()));

        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 100.0));
        QVERIFY(!match(i, "thd", "avgh", "BsB_S11", 110.0));
        QVERIFY(!cmatch(i, "thd", "avgh", "BsB_S11", 110.0, 300.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 200.0, 300.0));
        i->advance(400.0);

        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 500.0, 1200.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 500.0, 1200.0));
        QVERIFY(!match(i, "thd", "avgh", "BsB_S11", 500.0, 1200.0));
        QVERIFY(!match(i, "thd", "avgh", "BsB_S11", 600.0));
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 600.0));

        QVERIFY(!cmatch(i, "thd", "avgh", "BsB_S11", 1000.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1000.0));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1100.0, 3000.0));
        QVERIFY(match(i, "thd", "avgh", "BsB_S11", 1100.0, 3000.0));
        QVERIFY(!match(i, "thd", "avgh", "BsB_S11", 1100.0, 1500.0));
        i->advance(1800.0);

        auto j = i->clone();
        i.reset();
        i = std::move(j);
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << i;
        }
        QVERIFY(match(i, "bnd", "raw", "BsG_S11"));
        QVERIFY(match(i, "thd", "avgh", "BsB_S11"));
        QVERIFY(!match(i, "thd", "avgh", "BsG_S11"));

        QVERIFY(cmatch(i, "thd", "avgh", "BsB_S11", 1800.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1800.0));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1800.0, 2500.0));
        QVERIFY(match(i, "thd", "avgh", "BsB_S11", 1800.0, 1900.0));
        i->advance(1800.0);

        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 2500.0, 3100.0));
        QVERIFY(match(i, "thd", "avgh", "BsB_S11", 2500.0, 3100.0));
        QVERIFY(cmatch(i, "thd", "avgh", "BsB_S11", 2500.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 2500.0));

        i->advance(3000.0);
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 3000.0, 3100.0));
        QVERIFY(!match(i, "thd", "avgh", "BsB_S11", 3000.0, 3100.0));
        QVERIFY(!cmatch(i, "thd", "avgh", "BsB_S11", 3200.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 3200.0));

        i.reset();
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            stream >> i;
        }
        QVERIFY(match(i, "bnd", "raw", "BsG_S11"));
        QVERIFY(match(i, "thd", "avgh", "BsB_S11"));
        QVERIFY(!match(i, "thd", "avgh", "BsG_S11"));

        QVERIFY(cmatch(i, "thd", "avgh", "BsB_S11", 1800.0));
        QVERIFY(match(i, "bnd", "raw", "BsG_S11", 1800.0));
        QVERIFY(cmatch(i, "bnd", "raw", "BsG_S11", 1800.0, 2500.0));
        QVERIFY(match(i, "thd", "avgh", "BsB_S11", 1800.0, 1900.0));
        i->advance(1800.0);

        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 2500.0, 3100.0));
        QVERIFY(match(i, "thd", "avgh", "BsB_S11", 2500.0, 3100.0));
        QVERIFY(cmatch(i, "thd", "avgh", "BsB_S11", 2500.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 2500.0));

        i->advance(3000.0);
        QVERIFY(!cmatch(i, "bnd", "raw", "BsG_S11", 3000.0, 3100.0));
        QVERIFY(!match(i, "thd", "avgh", "BsB_S11", 3000.0, 3100.0));
        QVERIFY(!cmatch(i, "thd", "avgh", "BsB_S11", 3200.0));
        QVERIFY(!match(i, "bnd", "raw", "BsG_S11", 3200.0));
    }
};

QTEST_APPLESS_MAIN(TestSequenceMatchBasic)

#include "sequencematch_basic.moc"
