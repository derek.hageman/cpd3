/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "datacore/archive/selection.hxx"
#include "datacore/archive/access.hxx"
#include "core/number.hxx"

#include <QTest>
#include <QtSql>
#include <QString>

#include "datacore/segment.hxx"
#include "datacore/processingtapchain.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestFilterBackend : public ProcessingTapChain::ArchiveBackend {
public:
    Archive::Selection::List selections;
    bool issueComplete;

    TestFilterBackend() : issueComplete(false)
    { }

    void issueArchiveRead(const Archive::Selection::List &selections, StreamSink *target) override
    {
        this->selections = selections;

        target->incomingData(
                SequenceValue(SequenceName("brw", "raw", "T_S11"), Variant::Root(0), 1230768000.0,
                              1230768001.0));
        target->incomingData(
                SequenceValue(SequenceName("brw", "raw", "P_S11"), Variant::Root(0), 1230768000.0,
                              1230768001.0));
        target->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(0), 1230768000.0,
                              1230768001.0));

        SequenceValue::Transfer input;
        for (int i = 1; i < 1000; i++) {
            input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(i),
                               1230768000.0 + i, 1230768001.0 + i);
            input.emplace_back(SequenceName("brw", "raw", "P_S11"), Variant::Root(i * 2),
                               1230768000.0 + i, 1230768001.0 + i);
            input.emplace_back(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(i * 3),
                               1230768000.0 + i, 1230768001.0 + i);
        }
        target->incomingData(input);

        target->endData();
    }

    void archiveReadIssueComplete() override
    { issueComplete = true; }
};

class TestProcessingTapChainChain : public QObject {
Q_OBJECT

    SequenceValue::Transfer values;

    bool compareResult(const SequenceValue::Transfer &result,
                       const SequenceMatch::Composite &select,
                       double start = FP::undefined(),
                       double end = FP::undefined())
    {
        SequenceIdentity::Map<Variant::Read> rs;
        for (const auto &add : result) {
            rs.emplace(add.getIdentity(), add.getValue());
        }
        for (const auto &check : values) {
            if (!select.matches(check.getUnit()))
                continue;
            if (!Range::intersects(start, end, check.getStart(), check.getEnd()))
                continue;
            if (rs.empty()) {
                qDebug() << "Set empty on" << check;
                return false;
            }
            auto it = rs.find(check);
            if (it == rs.end()) {
                qDebug() << "Failed to locate" << check;
                return false;
            }
            if (it->second != check.getValue()) {
                qDebug() << "Value mismatch, expected:" << check << "got:" << it->second;
                return false;
            }
            rs.erase(it);
        }
        return rs.empty();
    }

    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());

        for (int i = 0; i < 1000; i++) {
            values.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(i),
                                1230768000.0 + i, 1230768001.0 + i);
            values.emplace_back(SequenceName("brw", "raw", "P_S11"), Variant::Root(i * 2),
                                1230768000.0 + i, 1230768001.0 + i);
            values.emplace_back(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(i * 3),
                                1230768000.0 + i, 1230768001.0 + i);
        }
        Archive::Access(databaseFile).writeSynchronous(values);
    }

    void simpleGet()
    {
        ProcessingTapChain chain;
        StreamSink::Buffer endpoint;

        chain.add(&endpoint, Variant::Root(),
                  SequenceMatch::Composite(SequenceMatch::Element::SpecialMatch::Data));
        chain.start();
        QVERIFY(chain.wait(30000));
        QVERIFY(endpoint.ended());
        QVERIFY(compareResult(endpoint.values(), SequenceMatch::Composite(
                SequenceMatch::Element::SpecialMatch::Data)));
    }

    void backendGet()
    {
        TestFilterBackend backend;
        ProcessingTapChain chain;
        StreamSink::Buffer endpoint;

        chain.add(&endpoint, Variant::Root(),
                  SequenceMatch::Composite(SequenceMatch::Element::SpecialMatch::Data));
        chain.setBackend(&backend);
        chain.start();
        QVERIFY(chain.wait(30000));
        QVERIFY(endpoint.ended());
        QVERIFY(compareResult(endpoint.values(), SequenceMatch::Composite(
                SequenceMatch::Element::SpecialMatch::Data)));

        QVERIFY(backend.issueComplete);
        QCOMPARE((int) backend.selections.size(), (int) 1);
        QVERIFY(backend.selections[0].stations.empty());
        QVERIFY(backend.selections[0].archives.empty());
        QVERIFY(backend.selections[0].variables.empty());
        QVERIFY(backend.selections[0].hasFlavors.empty());
        {
            std::unordered_set<SequenceName::Component> result
                    (backend.selections[0].lacksFlavors.begin(),
                     backend.selections[0].lacksFlavors.end());
            std::unordered_set<SequenceName::Component> expected
                    (SequenceName::defaultLacksFlavors().begin(),
                     SequenceName::defaultLacksFlavors().end());
            QCOMPARE(result, expected);
        }
        QVERIFY(backend.selections[0].exactFlavors.empty());
        QVERIFY(!FP::defined(backend.selections[0].modifiedAfter));
        QVERIFY(backend.selections[0].includeDefaultStation);
        QVERIFY(backend.selections[0].includeMetaArchive);
    }

    void backendGetAdditional()
    {
        TestFilterBackend backend;
        ProcessingTapChain chain;
        StreamSink::Buffer endpoint;

        Variant::Write config = Variant::Write::empty();
        config["Additional/#0/Variable"] = "BsG_S11";

        chain.add(&endpoint, config,
                  SequenceMatch::Composite(SequenceMatch::Element("", "", "T_S11")));
        chain.setBackend(&backend);
        chain.start();
        QVERIFY(chain.wait(30000));
        QVERIFY(endpoint.ended());
        QVERIFY(compareResult(endpoint.values(),
                              SequenceMatch::Composite(SequenceMatch::Element("", "", "T_S11"))));

        QVERIFY(backend.issueComplete);
        QCOMPARE((int) backend.selections.size(), (int) 2);
        QVERIFY(std::find(backend.selections[0].variables.begin(),
                          backend.selections[0].variables.end(), std::string("T_S11")) !=
                        backend.selections[0].variables.end() ||
                        std::find(backend.selections[0].variables.begin(),
                                  backend.selections[0].variables.end(), std::string("BsG_S11")) !=
                                backend.selections[0].variables.end());
        QVERIFY(std::find(backend.selections[1].variables.begin(),
                          backend.selections[1].variables.end(), std::string("T_S11")) !=
                        backend.selections[1].variables.end() ||
                        std::find(backend.selections[1].variables.begin(),
                                  backend.selections[1].variables.end(), std::string("BsG_S11")) !=
                                backend.selections[1].variables.end());
    }

    void multiExit()
    {
        ProcessingTapChain chain;
        StreamSink::Buffer endpoint1;
        StreamSink::Buffer endpoint2;
        StreamSink::Buffer endpoint3;

        Variant::Root config;
        config["Components/#0/Name"].setString("time_shift");

        chain.add(&endpoint1, Variant::Root(),
                  SequenceMatch::Composite(SequenceMatch::Element::SpecialMatch::Data));
        chain.add(&endpoint2, config,
                  SequenceMatch::Composite(SequenceMatch::Element("", "", "BsG_S11")));
        chain.add(&endpoint3, Variant::Root(),
                  SequenceMatch::Composite(SequenceMatch::Element("", "", "T_S11")));
        chain.start();
        QVERIFY(chain.wait(30000));
        QVERIFY(endpoint1.ended());
        QVERIFY(compareResult(endpoint1.values(), SequenceMatch::Composite(
                SequenceMatch::Element::SpecialMatch::Data)));
        QVERIFY(endpoint2.ended());
        QVERIFY(compareResult(endpoint2.values(),
                              SequenceMatch::Composite(SequenceMatch::Element("", "", "BsG_S11"))));
        QVERIFY(endpoint3.ended());
        QVERIFY(compareResult(endpoint3.values(),
                              SequenceMatch::Composite(SequenceMatch::Element("", "", "T_S11"))));
    }

    void combinedMultiExit()
    {
        ProcessingTapChain chain;
        StreamSink::Buffer endpoint1;
        StreamSink::Buffer endpoint2;
        StreamSink::Buffer endpoint3;
        StreamSink::Buffer endpoint4;

        Variant::Root config;
        config["Components/#0/Name"].setString("time_shift");

        chain.add(&endpoint1, Variant::Root(),
                  SequenceMatch::Composite(SequenceMatch::Element::SpecialMatch::Data));
        chain.add(&endpoint2, config,
                  SequenceMatch::Composite(SequenceMatch::Element("", "", "BsG_S11")));

        config["Components/#1/Name"].setString("time_shift");
        chain.add(QList<QPair<SequenceMatch::Composite, StreamSink *> >()
                          << QPair<SequenceMatch::Composite, StreamSink *>(
                                  SequenceMatch::Composite(SequenceMatch::Element("", "", "T_S11")),
                                  &endpoint3) << QPair<SequenceMatch::Composite, StreamSink *>(
                SequenceMatch::Composite(SequenceMatch::Element("", "", "P_S11")), &endpoint4),
                  config);

        chain.start();
        QVERIFY(chain.wait(30000));
        QVERIFY(endpoint1.ended());
        QVERIFY(compareResult(endpoint1.values(), SequenceMatch::Composite(
                SequenceMatch::Element::SpecialMatch::Data)));
        QVERIFY(endpoint2.ended());
        QVERIFY(compareResult(endpoint2.values(),
                              SequenceMatch::Composite(SequenceMatch::Element("", "", "BsG_S11"))));
        QVERIFY(endpoint3.ended());
        QVERIFY(compareResult(endpoint3.values(),
                              SequenceMatch::Composite(SequenceMatch::Element("", "", "T_S11"))));
        QVERIFY(endpoint4.ended());
        QVERIFY(compareResult(endpoint4.values(),
                              SequenceMatch::Composite(SequenceMatch::Element("", "", "P_S11"))));
    }

    void uncombinedMultiExit()
    {
        ProcessingTapChain chain;
        StreamSink::Buffer endpoint1;
        StreamSink::Buffer endpoint2;
        StreamSink::Buffer endpoint3;
        StreamSink::Buffer endpoint4;

        Variant::Root config;
        config["Components/#0/Name"].setString("time_shift");
        config["Merge"].setBoolean(false);

        chain.add(&endpoint1, Variant::Root(),
                  SequenceMatch::Composite(SequenceMatch::Element::SpecialMatch::Data));
        chain.add(&endpoint2, config,
                  SequenceMatch::Composite(SequenceMatch::Element("", "", "BsG_S11")));

        config["Components/#1/Name"].setString("time_shift");
        chain.add(QList<QPair<SequenceMatch::Composite, StreamSink *> >()
                          << QPair<SequenceMatch::Composite, StreamSink *>(
                                  SequenceMatch::Composite(SequenceMatch::Element("", "", "T_S11")),
                                  &endpoint3) << QPair<SequenceMatch::Composite, StreamSink *>(
                SequenceMatch::Composite(SequenceMatch::Element("", "", "P_S11")), &endpoint4),
                  config);

        chain.start();
        QVERIFY(chain.wait(30000));
        QVERIFY(endpoint1.ended());
        QVERIFY(compareResult(endpoint1.values(), SequenceMatch::Composite(
                SequenceMatch::Element::SpecialMatch::Data)));
        QVERIFY(endpoint2.ended());
        QVERIFY(compareResult(endpoint2.values(),
                              SequenceMatch::Composite(SequenceMatch::Element("", "", "BsG_S11"))));
        QVERIFY(endpoint3.ended());
        QVERIFY(compareResult(endpoint3.values(),
                              SequenceMatch::Composite(SequenceMatch::Element("", "", "T_S11"))));
        QVERIFY(endpoint4.ended());
        QVERIFY(compareResult(endpoint4.values(),
                              SequenceMatch::Composite(SequenceMatch::Element("", "", "P_S11"))));
    }

    void uncombinedMultiExitDefaults()
    {
        ProcessingTapChain chain;
        StreamSink::Buffer endpoint1;
        StreamSink::Buffer endpoint2;
        StreamSink::Buffer endpoint3;
        StreamSink::Buffer endpoint4;

        Variant::Root config;
        config["Components/#0/Name"].setString("time_shift");
        config["Merge"].setBoolean(false);

        chain.setDefaultSelection(1230768000.0, 1230768000.0 + 100.0);

        chain.add(&endpoint2, config,
                  SequenceMatch::Composite(SequenceMatch::Element("", "", "BsG_S11")));
        chain.add(&endpoint1, Variant::Root(),
                  SequenceMatch::Composite(SequenceMatch::Element::SpecialMatch::Data));

        config["Components/#1/Name"].setString("time_shift");
        chain.add(QList<QPair<SequenceMatch::Composite, StreamSink *> >()
                          << QPair<SequenceMatch::Composite, StreamSink *>(
                                  SequenceMatch::Composite(SequenceMatch::Element("", "", "T_S11")),
                                  &endpoint3) << QPair<SequenceMatch::Composite, StreamSink *>(
                SequenceMatch::Composite(SequenceMatch::Element("", "", "P_S11")), &endpoint4),
                  config);

        chain.start();
        QVERIFY(chain.wait(30000));
        QVERIFY(endpoint1.ended());
        QVERIFY(compareResult(endpoint1.values(),
                              SequenceMatch::Composite(SequenceMatch::Element::SpecialMatch::Data),
                              1230768000.0, 1230768000.0 + 100.0));
        QVERIFY(endpoint2.ended());
        QVERIFY(compareResult(endpoint2.values(),
                              SequenceMatch::Composite(SequenceMatch::Element("", "", "BsG_S11")),
                              1230768000.0, 1230768000.0 + 100.0));
        QVERIFY(endpoint3.ended());
        QVERIFY(compareResult(endpoint3.values(),
                              SequenceMatch::Composite(SequenceMatch::Element("", "", "T_S11")),
                              1230768000.0, 1230768000.0 + 100.0));
        QVERIFY(endpoint4.ended());
        QVERIFY(compareResult(endpoint4.values(),
                              SequenceMatch::Composite(SequenceMatch::Element("", "", "P_S11")),
                              1230768000.0, 1230768000.0 + 100.0));
    }

    void chainBypass()
    {
        ProcessingTapChain chain;
        StreamSink::Buffer endpoint1;
        StreamSink::Buffer endpoint2;
        StreamSink::Buffer endpoint3;

        Variant::Root config;
        config["Components/#0/Name"].setString("time_shift");
        config["Components/#1/Name"].setString("time_shift");
        config["Components/#2/Name"].setString("time_shift");
        config["Components/#3/Name"].setString("time_shift");

        chain.add(&endpoint1, config,
                  SequenceMatch::Composite(SequenceMatch::Element::SpecialMatch::Data));
        chain.add(&endpoint2, config,
                  SequenceMatch::Composite(SequenceMatch::Element("", "", "BsG_S11")));

        config.write().setEmpty();
        config["Components/#0/Name"].setString("time_shift");
        config["Components/#1/Name"].setString("time_shift");
        chain.add(&endpoint3, config,
                  SequenceMatch::Composite(SequenceMatch::Element("", "", "T_S11")));

        chain.start();
        QVERIFY(chain.wait(30000));
        QVERIFY(endpoint1.ended());
        QVERIFY(compareResult(endpoint1.values(), SequenceMatch::Composite(
                SequenceMatch::Element::SpecialMatch::Data)));
        QVERIFY(endpoint2.ended());
        QVERIFY(compareResult(endpoint2.values(),
                              SequenceMatch::Composite(SequenceMatch::Element("", "", "BsG_S11"))));
        QVERIFY(endpoint3.ended());
        QVERIFY(compareResult(endpoint3.values(),
                              SequenceMatch::Composite(SequenceMatch::Element("", "", "T_S11"))));
    }
};

QTEST_MAIN(TestProcessingTapChainChain)

#include "processingtapchain.moc"
