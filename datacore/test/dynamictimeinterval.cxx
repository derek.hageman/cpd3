/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtSql>
#include <QString>

#include "datacore/dynamictimeinterval.hxx"
#include "datacore/variant/root.hxx"
#include "core/number.hxx"
#include "core/timeutils.hxx"
#include "core/timeparse.hxx"

using namespace CPD3;
using namespace CPD3::Data;

Q_DECLARE_METATYPE(QList<double>);

class TestDynamicTimeInterval : public QObject {
Q_OBJECT
private slots:

    void intervalUndefined()
    {
        DynamicTimeInterval *i = new DynamicTimeInterval::Undefined;
        QVERIFY(!FP::defined(i->apply(1291739399.0, 1291739399.0)));
        QVERIFY(!FP::defined(i->applyConst(1291739399.0, 1291739399.0)));
        QVERIFY(!FP::defined(i->round(1291739399.0, 1291739399.0)));
        QVERIFY(!FP::defined(i->roundConst(1291739399.0, 1291739399.0)));
        DynamicTimeInterval *j = i->clone();
        delete i;
        QVERIFY(!FP::defined(j->apply(1291739399.0, 1291739399.0)));
        DynamicTimeInterval *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QVERIFY(!FP::defined(k->apply(1291739399.0, 1291739399.0)));
        delete k;
    }

    void intervalNone()
    {
        DynamicTimeInterval *i = new DynamicTimeInterval::None;
        QCOMPARE(i->apply(1291739399.0, 1291739399.0), 1291739399.0);
        QCOMPARE(i->applyConst(1291739399.0, 1291739399.0), 1291739399.0);
        QCOMPARE(i->round(1291739399.0, 1291739399.0), 1291739399.0);
        QCOMPARE(i->roundConst(1291739399.0, 1291739399.0), 1291739399.0);
        DynamicTimeInterval *j = i->clone();
        delete i;
        QCOMPARE(j->apply(1291739399.0, 1291739399.0), 1291739399.0);
        DynamicTimeInterval *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->apply(1291739399.0, 1291739399.0), 1291739399.0);
        delete k;
    }

    void intervalConstant()
    {
        DynamicTimeInterval *i = new DynamicTimeInterval::Constant(Time::Second, 60);
        QCOMPARE(i->apply(1291739399.0, 1291742073.0), 1291742133.0);
        QCOMPARE(i->applyConst(1291739399.0, 1291742073.0), 1291742133.0);
        QCOMPARE(i->round(1291739399.0, 1291742073.0), 1291742073.0);
        QCOMPARE(i->roundConst(1291739399.0, 1291742073.0), 1291742073.0);
        DynamicTimeInterval *j = i->clone();
        delete i;
        QCOMPARE(j->apply(1291739399.0, 1291742073.0), 1291742133.0);
        DynamicTimeInterval *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->apply(1291739399.0, 1291742073.0), 1291742133.0);
        delete k;
    }

    void intervalVariable()
    {
        QFETCH(ValueSegment::Transfer, configSingle);
        QFETCH(QList<double>, inputTimes);
        QFETCH(QList<double>, inputOrigins);
        QFETCH(QList<double>, resultTimes);

        DynamicTimeInterval *i = DynamicTimeInterval::fromConfiguration(configSingle);
        for (int idx = 0, max = inputTimes.size(); idx < max; idx++) {
            QCOMPARE(i->applyConst(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);

            DynamicTimeInterval *j = i->clone();
            DynamicTimeInterval *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            QCOMPARE(j->applyConst(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);
            QCOMPARE(k->applyConst(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);
            QCOMPARE(j->apply(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);
            QCOMPARE(k->apply(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);

            for (int idx2 = idx + 1; idx2 < max; idx2++) {
                QCOMPARE(j->applyConst(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
                QCOMPARE(k->applyConst(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
                QCOMPARE(j->apply(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
                QCOMPARE(k->apply(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
            }

            QCOMPARE(i->apply(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);

            delete j;
            delete k;
        }
        delete i;

        SequenceSegment::Transfer configMulti;
        SequenceName cmName("brw", "editing", "input");
        for (int i = 0, max = configSingle.size(); i < max; i++) {
            SequenceSegment ms;
            ms.setStart(configSingle.at(i).getStart());
            ms.setEnd(configSingle.at(i).getEnd());
            ms.setValue(cmName, configSingle.at(i).root());
            configMulti.emplace_back(std::move(ms));
        }
        i = DynamicTimeInterval::fromConfiguration(configMulti, cmName);
        for (int idx = 0, max = inputTimes.size(); idx < max; idx++) {
            QCOMPARE(i->applyConst(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);

            DynamicTimeInterval *j = i->clone();
            DynamicTimeInterval *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            QCOMPARE(j->applyConst(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);
            QCOMPARE(k->applyConst(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);
            QCOMPARE(j->apply(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);
            QCOMPARE(k->apply(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);

            for (int idx2 = idx + 1; idx2 < max; idx2++) {
                QCOMPARE(j->applyConst(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
                QCOMPARE(k->applyConst(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
                QCOMPARE(j->apply(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
                QCOMPARE(k->apply(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
            }

            QCOMPARE(i->apply(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);

            delete j;
            delete k;
        }
        delete i;

        TimeIntervalSelectionOption so("", "", "", "");
        for (int i = 0, max = configSingle.size(); i < max; i++) {
            ValueSegment cs(configSingle.at(i));
            auto cv = cs.read();
            if (cv.getType() == Variant::Type::Real) {
                so.set(cs.getStart(), cs.getEnd(), Time::Second, cv.toReal());
            } else if (cv.getType() == Variant::Type::Integer) {
                so.set(cs.getStart(), cs.getEnd(), Time::Second, cv.toInteger());
            } else if (cv.getType() == Variant::Type::String) {
                Time::LogicalTimeUnit u = Time::Second;
                int n = 0;
                bool a = false;
                try {
                    TimeParse::parseOffset(cv.toQString(), &u, &n, &a);
                    so.set(cs.getStart(), cs.getEnd(), u, n, a);
                } catch (TimeParsingException tpe) {
                    QFAIL(qPrintable(QString(i) + ":" + tpe.getDescription()));
                }
            } else {
                so.set(cs.getStart(), cs.getEnd(), Time::Second, (int) cv["/Count"].toInt64());
            }
        }
        i = so.getInterval();
        for (int idx = 0, max = inputTimes.size(); idx < max; idx++) {
            QCOMPARE(i->applyConst(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);

            DynamicTimeInterval *j = i->clone();
            DynamicTimeInterval *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            QCOMPARE(j->applyConst(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);
            QCOMPARE(k->applyConst(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);
            QCOMPARE(j->apply(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);
            QCOMPARE(k->apply(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);

            for (int idx2 = idx + 1; idx2 < max; idx2++) {
                QCOMPARE(j->applyConst(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
                QCOMPARE(k->applyConst(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
                QCOMPARE(j->apply(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
                QCOMPARE(k->apply(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
            }

            QCOMPARE(i->apply(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);

            delete j;
            delete k;
        }
        delete i;

        TimeIntervalSelectionOption *so2 = (TimeIntervalSelectionOption *) so.clone();
        i = so2->getInterval();
        delete so2;
        for (int idx = 0, max = inputTimes.size(); idx < max; idx++) {
            QCOMPARE(i->applyConst(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);

            DynamicTimeInterval *j = i->clone();
            DynamicTimeInterval *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            QCOMPARE(j->applyConst(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);
            QCOMPARE(k->applyConst(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);
            QCOMPARE(j->apply(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);
            QCOMPARE(k->apply(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);

            for (int idx2 = idx + 1; idx2 < max; idx2++) {
                QCOMPARE(j->applyConst(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
                QCOMPARE(k->applyConst(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
                QCOMPARE(j->apply(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
                QCOMPARE(k->apply(inputTimes[idx2], inputOrigins[idx2]), resultTimes[idx2]);
            }

            QCOMPARE(i->apply(inputTimes[idx], inputOrigins[idx]), resultTimes[idx]);

            delete j;
            delete k;
        }
        delete i;
    }

    void intervalVariable_data()
    {
        QTest::addColumn<ValueSegment::Transfer>("configSingle");
        QTest::addColumn<QList<double> >("inputTimes");
        QTest::addColumn<QList<double> >("inputOrigins");
        QTest::addColumn<QList<double> >("resultTimes");

        ValueSegment::Transfer config;
        QList<double> inputTimes;
        QList<double> inputOrigins;
        QList<double> resultTimes;

        inputTimes << 100.0 << 200.0 << 300.0;
        inputOrigins << 50.0 << 100.0 << 150.0;
        resultTimes = inputOrigins;

        QTest::newRow("Empty") << config << inputTimes << inputOrigins << resultTimes;

        config.emplace_back(FP::undefined(), FP::undefined(), Variant::Root(1.0));
        resultTimes.clear();
        resultTimes << 51.0 << 101.0 << 151.0;
        QTest::newRow("Constant") << config << inputTimes << inputOrigins << resultTimes;

        config.back().setEnd(150.0);
        config.emplace_back(150.0, FP::undefined(), Variant::Root(2.0));
        resultTimes.clear();
        resultTimes << 51.0 << 102.0 << 152.0;
        QTest::newRow("Two") << config << inputTimes << inputOrigins << resultTimes;

        config.back().setEnd(300.0);
        resultTimes.clear();
        resultTimes << 51.0 << 102.0 << 150.0;
        QTest::newRow("Three") << config << inputTimes << inputOrigins << resultTimes;
    }

    void configurationDefault()
    {
        DynamicTimeInterval::Variable *def = new DynamicTimeInterval::Variable;
        def->set(Variant::Root(10.0));

        DynamicTimeInterval *created = DynamicTimeInterval::fromConfiguration(
                ValueSegment::Transfer{
                        ValueSegment(FP::undefined(), FP::undefined(), Variant::Root())},
                QString(), FP::undefined(), FP::undefined(), true, def);
        delete def;

        double output = created->apply(100.0, 100.0, true);
        QCOMPARE(output, 110.0);

        delete created;
    }
};

QTEST_APPLESS_MAIN(TestDynamicTimeInterval)

#include "dynamictimeinterval.moc"
