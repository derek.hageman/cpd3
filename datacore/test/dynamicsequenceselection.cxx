/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtSql>

#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/variant/root.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Data;

Q_DECLARE_METATYPE(QList<double>);

Q_DECLARE_METATYPE(SequenceName::Set);

Q_DECLARE_METATYPE(QList<SequenceName::Set>);

namespace QTest {
template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}


class TestDynamicSequenceSelection : public QObject {
Q_OBJECT
private slots:

    void operateNever()
    {
        DynamicSequenceSelection *i = new DynamicSequenceSelection::None();
        QVERIFY(i->get(1.0, 2.0).empty());
        QVERIFY(i->getConst(1.0, 2.0).empty());
        QVERIFY(i->get(1.0).empty());
        QVERIFY(i->getConst(1.0).empty());
        QVERIFY(!i->matches(1.0, 2.0, SequenceName()));
        QVERIFY(!i->matchesConst(1.0, 2.0, SequenceName()));
        QVERIFY(!i->matches(1.0, SequenceName()));
        QVERIFY(!i->matchesConst(1.0, SequenceName()));
        DynamicSequenceSelection *j = i->clone();
        delete i;
        QVERIFY(j->get(1.0, 2.0).empty());
        QVERIFY(j->getConst(1.0, 2.0).empty());
        QVERIFY(j->get(1.0).empty());
        QVERIFY(j->getConst(1.0).empty());
        QVERIFY(!j->matches(1.0, 2.0, SequenceName()));
        QVERIFY(!j->matchesConst(1.0, 2.0, SequenceName()));
        QVERIFY(!j->matches(1.0, SequenceName()));
        QVERIFY(!j->matchesConst(1.0, SequenceName()));
        DynamicSequenceSelection *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QVERIFY(k->get(1.0, 2.0).empty());
        QVERIFY(k->getConst(1.0, 2.0).empty());
        QVERIFY(k->get(1.0).empty());
        QVERIFY(k->getConst(1.0).empty());
        QVERIFY(!k->matches(1.0, 2.0, SequenceName()));
        QVERIFY(!k->matchesConst(1.0, 2.0, SequenceName()));
        QVERIFY(!k->matches(1.0, SequenceName()));
        QVERIFY(!k->matchesConst(1.0, SequenceName()));
        delete k;
    }

    void operateAlwaysSingle()
    {
        SequenceName u("brw", "clean", "BaB_A11");
        DynamicSequenceSelection *i = new DynamicSequenceSelection::Single(u);
        SequenceName::Set expected;
        expected.insert(u);
        QCOMPARE(i->get(1.0, 2.0), expected);
        QVERIFY(i->registerInput(u));
        QVERIFY(!i->registerInput(u.withFlavor("pm10")));
        QCOMPARE(i->getConst(1.0, 2.0), expected);
        QCOMPARE(i->get(1.0), expected);
        QCOMPARE(i->getConst(1.0), expected);
        QVERIFY(i->matches(1.0, 2.0, u));
        QVERIFY(!i->matches(1.0, 2.0, u.withFlavor("pm10")));
        QVERIFY(i->matchesConst(1.0, 2.0, u));
        QVERIFY(i->matches(1.0, u));
        QVERIFY(i->matchesConst(1.0, u));
        DynamicSequenceSelection *j = i->clone();
        delete i;
        QCOMPARE(j->get(1.0, 2.0), expected);
        QCOMPARE(j->getConst(1.0, 2.0), expected);
        QCOMPARE(j->get(1.0), expected);
        QCOMPARE(j->getConst(1.0), expected);
        QVERIFY(j->matches(1.0, 2.0, u));
        QVERIFY(!j->matches(1.0, 2.0, u.withFlavor("pm10")));
        QVERIFY(j->matchesConst(1.0, 2.0, u));
        QVERIFY(j->matches(1.0, u));
        QVERIFY(j->matchesConst(1.0, u));
        DynamicSequenceSelection *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->get(1.0, 2.0), expected);
        QCOMPARE(k->getConst(1.0, 2.0), expected);
        QCOMPARE(k->get(1.0), expected);
        QCOMPARE(k->getConst(1.0), expected);
        QVERIFY(k->matches(1.0, 2.0, u));
        QVERIFY(!k->matches(1.0, 2.0, u.withFlavor("pm10")));
        QVERIFY(k->matchesConst(1.0, 2.0, u));
        QVERIFY(k->matches(1.0, u));
        QVERIFY(k->matchesConst(1.0, u));
        delete k;
    }

    void operateComposite()
    {
        SequenceName u1("brw", "clean", "BaB_A11");
        SequenceName u2("brw", "clean", "BaG_A11");
        std::vector<std::unique_ptr<DynamicSequenceSelection>> components;
        components.emplace_back(new DynamicSequenceSelection::Single(u1));
        components.emplace_back(new DynamicSequenceSelection::Single(u2));
        DynamicSequenceSelection
                *i = new DynamicSequenceSelection::Composite(std::move(components));
        SequenceName::Set expected;
        expected.insert(u1);
        expected.insert(u2);
        QCOMPARE(i->get(1.0, 2.0), expected);
        QCOMPARE(i->getConst(1.0, 2.0), expected);
        QCOMPARE(i->get(1.0), expected);
        QCOMPARE(i->getConst(1.0), expected);
        QVERIFY(i->matches(1.0, 2.0, u1));
        QVERIFY(!i->matches(1.0, 2.0, u1.withFlavor("pm1")));
        QVERIFY(i->matchesConst(1.0, 2.0, u2));
        QVERIFY(i->matches(1.0, u1));
        QVERIFY(i->matchesConst(1.0, u1));
        DynamicSequenceSelection *j = i->clone();
        delete i;
        QCOMPARE(j->get(1.0, 2.0), expected);
        QCOMPARE(j->getConst(1.0, 2.0), expected);
        QCOMPARE(j->get(1.0), expected);
        QCOMPARE(j->getConst(1.0), expected);
        QVERIFY(j->matches(1.0, 2.0, u1));
        QVERIFY(!j->matches(1.0, 2.0, u1.withFlavor("pm1")));
        QVERIFY(j->matchesConst(1.0, 2.0, u2));
        QVERIFY(j->matches(1.0, u1));
        QVERIFY(j->matchesConst(1.0, u1));
        DynamicSequenceSelection *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->get(1.0, 2.0), expected);
        QCOMPARE(k->getConst(1.0, 2.0), expected);
        QCOMPARE(k->get(1.0), expected);
        QCOMPARE(k->getConst(1.0), expected);
        QVERIFY(k->matches(1.0, 2.0, u1));
        QVERIFY(!k->matches(1.0, 2.0, u1.withFlavor("pm1")));
        QVERIFY(k->matchesConst(1.0, 2.0, u2));
        QVERIFY(k->matches(1.0, u1));
        QVERIFY(k->matchesConst(1.0, u1));
        delete k;
    }

    void operateAlways()
    {
        SequenceName u1("brw", "clean", "BaB_A11");
        SequenceName u2("brw", "clean", "BaB_A12");
        SequenceName::Set expected;
        expected.insert(u1);
        expected.insert(u2);
        DynamicSequenceSelection *i = new DynamicSequenceSelection::Basic(expected);
        QCOMPARE(i->get(1.0, 2.0), expected);
        QCOMPARE(i->getConst(1.0, 2.0), expected);
        QVERIFY(i->registerInput(u1));
        QVERIFY(i->registerInput(u2));
        QVERIFY(!i->registerInput(u1.withFlavor("pm10")));
        QCOMPARE(i->get(1.0), expected);
        QCOMPARE(i->getConst(1.0), expected);
        QVERIFY(i->matches(1.0, 2.0, u1));
        QVERIFY(!i->matches(1.0, 2.0, u1.withFlavor("pm1")));
        QVERIFY(i->matchesConst(1.0, 2.0, u2));
        QVERIFY(i->matches(1.0, u1));
        QVERIFY(i->matchesConst(1.0, u1));
        DynamicSequenceSelection *j = i->clone();
        delete i;
        QCOMPARE(j->get(1.0, 2.0), expected);
        QCOMPARE(j->getConst(1.0, 2.0), expected);
        QCOMPARE(j->get(1.0), expected);
        QCOMPARE(j->getConst(1.0), expected);
        QVERIFY(j->matches(1.0, 2.0, u1));
        QVERIFY(!j->matches(1.0, 2.0, u1.withFlavor("pm1")));
        QVERIFY(j->matchesConst(1.0, 2.0, u2));
        QVERIFY(j->matches(1.0, u1));
        QVERIFY(j->matchesConst(1.0, u1));
        DynamicSequenceSelection *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->get(1.0, 2.0), expected);
        QCOMPARE(k->getConst(1.0, 2.0), expected);
        QCOMPARE(k->get(1.0), expected);
        QCOMPARE(k->getConst(1.0), expected);
        QVERIFY(k->matches(1.0, 2.0, u1));
        QVERIFY(!k->matches(1.0, 2.0, u1.withFlavor("pm1")));
        QVERIFY(k->matchesConst(1.0, 2.0, u2));
        QVERIFY(k->matches(1.0, u1));
        QVERIFY(k->matchesConst(1.0, u1));
        delete k;
    }

    void operateAlwaysRegex()
    {
        SequenceName u1("brw", "clean", "BaB_A11", {"pm1"});
        SequenceName u2("brw", "clean", "BaB_A12");
        DynamicSequenceSelection::Match ar("brw", "clean", "BaB_A11", {"pm1"});
        DynamicSequenceSelection *i = &ar;
        SequenceName::Set expected;
        QVERIFY(ar.registerInput(u1));
        expected.insert(u1);
        QCOMPARE(i->getAllUnits(), expected);
        QCOMPARE(i->get(1.0, 2.0), expected);
        QCOMPARE(i->getConst(1.0, 2.0), expected);
        QCOMPARE(i->get(1.0), expected);
        QCOMPARE(i->getConst(1.0), expected);
        QVERIFY(!ar.registerInput(u2));
        QCOMPARE(i->getAllUnits(), expected);
        QCOMPARE(i->get(1.0, 2.0), expected);
        QCOMPARE(i->getConst(1.0, 2.0), expected);
        QCOMPARE(i->get(1.0), expected);
        QCOMPARE(i->getConst(1.0), expected);
        QVERIFY(i->matches(1.0, 2.0, u1));
        QVERIFY(!i->matches(1.0, 2.0, u2));
        QVERIFY(i->matchesConst(1.0, 2.0, u1));
        QVERIFY(i->matches(1.0, u1));
        QVERIFY(i->matchesConst(1.0, u1));
        DynamicSequenceSelection *j = i->clone();
        QCOMPARE(j->getAllUnits(), expected);
        QCOMPARE(j->get(1.0, 2.0), expected);
        QCOMPARE(j->getConst(1.0, 2.0), expected);
        QCOMPARE(j->get(1.0), expected);
        QCOMPARE(j->getConst(1.0), expected);
        QVERIFY(j->matches(1.0, 2.0, u1));
        QVERIFY(!j->matches(1.0, 2.0, u2));
        QVERIFY(j->matchesConst(1.0, 2.0, u1));
        QVERIFY(j->matches(1.0, u1));
        QVERIFY(j->matchesConst(1.0, u1));
        DynamicSequenceSelection *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->getAllUnits(), expected);
        QCOMPARE(k->get(1.0, 2.0), expected);
        QCOMPARE(k->getConst(1.0, 2.0), expected);
        QCOMPARE(k->get(1.0), expected);
        QCOMPARE(k->getConst(1.0), expected);
        QVERIFY(k->matches(1.0, 2.0, u1));
        QVERIFY(!k->matches(1.0, 2.0, u2));
        QVERIFY(k->matchesConst(1.0, 2.0, u1));
        QVERIFY(k->matches(1.0, u1));
        QVERIFY(k->matchesConst(1.0, u1));
        delete k;

        ar = DynamicSequenceSelection::Match("brw", "", "BaB_A1[12]", {}, {"pm10"});
        i = &ar;
        expected.clear();
        ar.registerInput(u1);
        expected.insert(u1);
        QCOMPARE(i->getAllUnits(), expected);
        QCOMPARE(i->get(1.0, 2.0), expected);
        QCOMPARE(i->getConst(1.0, 2.0), expected);
        QCOMPARE(i->get(1.0), expected);
        QCOMPARE(i->getConst(1.0), expected);
        QVERIFY(i->matches(1.0, 2.0, u1));
        QVERIFY(i->matches(1.0, 2.0, u2));
        QVERIFY(i->matchesConst(1.0, 2.0, u1));
        QVERIFY(i->matches(1.0, u1));
        QVERIFY(i->matchesConst(1.0, u1));
        ar.registerInput(u2);
        expected.insert(u2);
        QCOMPARE(i->getAllUnits(), expected);
        QCOMPARE(i->get(1.0, 2.0), expected);
        QCOMPARE(i->getConst(1.0, 2.0), expected);
        QCOMPARE(i->get(1.0), expected);
        QCOMPARE(i->getConst(1.0), expected);
        j = i->clone();
        QCOMPARE(j->getAllUnits(), expected);
        QCOMPARE(j->get(1.0, 2.0), expected);
        QCOMPARE(j->getConst(1.0, 2.0), expected);
        QCOMPARE(j->get(1.0), expected);
        QCOMPARE(j->getConst(1.0), expected);
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->getAllUnits(), expected);
        QCOMPARE(k->get(1.0, 2.0), expected);
        QCOMPARE(k->getConst(1.0, 2.0), expected);
        QCOMPARE(k->get(1.0), expected);
        QCOMPARE(k->getConst(1.0), expected);
        delete k;

        ar = DynamicSequenceSelection::Match(std::vector<SequenceMatch::Element>{
                SequenceMatch::Element(SequenceName::Component("brw"), "clean", "BaB_A11", {"pm1"}),
                SequenceMatch::Element("br[w]", "Clean", "BaB_A1[2]",
                                       std::vector<SequenceName::Component>(),
                                       std::vector<SequenceName::Component>())});
        i = &ar;
        expected.clear();
        ar.registerInput(u1);
        expected.insert(u1);
        QCOMPARE(i->getAllUnits(), expected);
        QCOMPARE(i->get(1.0, 2.0), expected);
        QCOMPARE(i->getConst(1.0, 2.0), expected);
        QCOMPARE(i->get(1.0), expected);
        QCOMPARE(i->getConst(1.0), expected);
        ar.registerInput(u2);
        expected.insert(u2);
        QCOMPARE(i->getAllUnits(), expected);
        QCOMPARE(i->get(1.0, 2.0), expected);
        QCOMPARE(i->getConst(1.0, 2.0), expected);
        QCOMPARE(i->get(1.0), expected);
        QCOMPARE(i->getConst(1.0), expected);
        j = i->clone();
        QCOMPARE(j->getAllUnits(), expected);
        QCOMPARE(j->get(1.0, 2.0), expected);
        QCOMPARE(j->getConst(1.0, 2.0), expected);
        QCOMPARE(j->get(1.0), expected);
        QCOMPARE(j->getConst(1.0), expected);
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->getAllUnits(), expected);
        QCOMPARE(k->get(1.0, 2.0), expected);
        QCOMPARE(k->getConst(1.0, 2.0), expected);
        QCOMPARE(k->get(1.0), expected);
        QCOMPARE(k->getConst(1.0), expected);
        delete k;
    }

    void segmentedOperate()
    {
        QFETCH(ValueSegment::Transfer, configSingle);
        QFETCH(QList<SequenceName::Set>, expectedData);
        QFETCH(QList<SequenceName::Set>, expectedTime);
        QFETCH(SequenceName::Set, expectedAll);

        QList<double> inputTimes;
        inputTimes << 1.0 << 2.0 << 4.0 << 8.0;

        SequenceName u1("brw", "clean", "BaB_A11");
        SequenceName u2("brw", "clean", "BaB_A12");
        SequenceName u3("brw", "raw", "BaB_A11");
        SequenceSegment::Transfer dmus;

        SequenceSegment dms;
        dms.setStart(1.0);
        dms.setEnd(2.0);
        dmus.push_back(dms);
        dms.setStart(2.0);
        dms.setEnd(3.0);
        dmus.push_back(dms);
        dms.setStart(4.0);
        dms.setEnd(5.0);
        dmus.push_back(dms);
        dms.setStart(5.0);
        dms.setEnd(7.0);
        dmus.push_back(dms);

        QSet<double> expectedChanged;
        for (const auto &seg : configSingle) {
            if (FP::defined(seg.getStart()))
                expectedChanged |= seg.getStart();
            if (FP::defined(seg.getEnd()))
                expectedChanged |= seg.getEnd();
        }


        DynamicSequenceSelection::Variable *to = new DynamicSequenceSelection::Variable();
        for (const auto &seg : configSingle) {
            to->set(seg.getStart(), seg.getEnd(), seg.getValue());
        }
        DynamicSequenceSelection *i = to;
        i->registerInput(u1);
        i->registerInput(u2);
        i->registerInput(u3);
        i->registerExpected("alt", "raw", "", SequenceName::Flavors{});
        QCOMPARE(i->getChangedPoints(), expectedChanged);
        QCOMPARE(i->getAllUnits(), expectedAll);
        for (int idx = 0; idx < 4; idx++) {
            QCOMPARE(i->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(i->getConst(dmus[idx]), expectedData[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matchesConst(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(i->matchesConst(dmus[idx], u));
            }

            DynamicSequenceSelection *j = i->clone();
            DynamicSequenceSelection *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            QCOMPARE(j->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(j->getConst(dmus[idx]), expectedData[idx]);
            QCOMPARE(j->get(dmus[idx]), expectedData[idx]);
            QCOMPARE(j->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->getConst(dmus[idx]), expectedData[idx]);
            QCOMPARE(k->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->get(dmus[idx]), expectedData[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(j->matchesConst(inputTimes[idx], u));
                QVERIFY(j->matches(inputTimes[idx], u));
                QVERIFY(k->matchesConst(inputTimes[idx], u));
                QVERIFY(k->matches(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(j->matchesConst(dmus[idx], u));
                QVERIFY(j->matches(dmus[idx], u));
                QVERIFY(k->matchesConst(dmus[idx], u));
                QVERIFY(k->matches(dmus[idx], u));
            }

            for (int idx2 = idx + 1; idx2 < 4; idx2++) {
                QCOMPARE(j->getConst(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(j->getConst(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(j->get(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(j->get(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->getConst(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->getConst(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(k->get(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->get(dmus[idx2]), expectedData[idx2]);

                for (const auto &u : expectedTime[idx2]) {
                    QVERIFY(j->matchesConst(inputTimes[idx2], u));
                    QVERIFY(j->matches(inputTimes[idx2], u));
                    QVERIFY(k->matchesConst(inputTimes[idx2], u));
                    QVERIFY(k->matches(inputTimes[idx2], u));
                }
                for (const auto &u : expectedData[idx2]) {
                    QVERIFY(j->matchesConst(dmus[idx2], u));
                    QVERIFY(j->matches(dmus[idx2], u));
                    QVERIFY(k->matchesConst(dmus[idx2], u));
                    QVERIFY(k->matches(dmus[idx2], u));
                }
            }

            QCOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(i->get(dmus[idx]), expectedData[idx]);
            QCOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matches(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(i->matches(dmus[idx], u));
            }
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matches(inputTimes[idx], u));
            }

            delete j;
            delete k;
        }
        delete i;


        DynamicSequenceSelectionOption so("", "", "", "");
        for (const auto &seg : configSingle) {
            QList<SequenceMatch::Element> m;
            auto config = seg.read();
            auto type = config.getType();
            if (type == Variant::Type::Array) {
                for (auto v : config.toArray()) {
                    m << SequenceMatch::Element(v);
                }
            } else {
                m << SequenceMatch::Element(config);
            }
            so.set(seg.getStart(), seg.getEnd(), m);
        }
        i = so.getOperator();
        i->registerInput(u1);
        i->registerInput(u2);
        i->registerInput(u3);
        i->registerExpected("alt", "raw", "", SequenceName::Flavors());
        QCOMPARE(i->getChangedPoints(), expectedChanged);
        QCOMPARE(i->getAllUnits(), expectedAll);
        for (int idx = 0; idx < 4; idx++) {
            QCOMPARE(i->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(i->getConst(dmus[idx]), expectedData[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matchesConst(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(i->matchesConst(dmus[idx], u));
            }

            DynamicSequenceSelection *j = i->clone();
            DynamicSequenceSelection *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            QCOMPARE(j->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(j->getConst(dmus[idx]), expectedData[idx]);
            QCOMPARE(j->get(dmus[idx]), expectedData[idx]);
            QCOMPARE(j->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->getConst(dmus[idx]), expectedData[idx]);
            QCOMPARE(k->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->get(dmus[idx]), expectedData[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(j->matchesConst(inputTimes[idx], u));
                QVERIFY(j->matches(inputTimes[idx], u));
                QVERIFY(k->matchesConst(inputTimes[idx], u));
                QVERIFY(k->matches(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(j->matchesConst(dmus[idx], u));
                QVERIFY(j->matches(dmus[idx], u));
                QVERIFY(k->matchesConst(dmus[idx], u));
                QVERIFY(k->matches(dmus[idx], u));
            }

            for (int idx2 = idx + 1; idx2 < 4; idx2++) {
                QCOMPARE(j->getConst(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(j->getConst(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(j->get(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(j->get(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->getConst(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->getConst(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(k->get(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->get(dmus[idx2]), expectedData[idx2]);

                for (const auto &u : expectedTime[idx2]) {
                    QVERIFY(j->matchesConst(inputTimes[idx2], u));
                    QVERIFY(j->matches(inputTimes[idx2], u));
                    QVERIFY(k->matchesConst(inputTimes[idx2], u));
                    QVERIFY(k->matches(inputTimes[idx2], u));
                }
                for (const auto &u : expectedData[idx2]) {
                    QVERIFY(j->matchesConst(dmus[idx2], u));
                    QVERIFY(j->matches(dmus[idx2], u));
                    QVERIFY(k->matchesConst(dmus[idx2], u));
                    QVERIFY(k->matches(dmus[idx2], u));
                }
            }

            QCOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(i->get(dmus[idx]), expectedData[idx]);
            QCOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matches(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(i->matches(dmus[idx], u));
            }
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matches(inputTimes[idx], u));
            }

            delete j;
            delete k;
        }
        delete i;

        DynamicSequenceSelectionOption *so2 = (DynamicSequenceSelectionOption *) so.clone();
        i = so2->getOperator();
        delete so2;
        i->registerInput(u1);
        i->registerInput(u2);
        i->registerInput(u3);
        i->registerExpected("alt", "raw", "", SequenceName::Flavors());
        QCOMPARE(i->getChangedPoints(), expectedChanged);
        QCOMPARE(i->getAllUnits(), expectedAll);
        for (int idx = 0; idx < 4; idx++) {
            QCOMPARE(i->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(i->getConst(dmus[idx]), expectedData[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matchesConst(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(i->matchesConst(dmus[idx], u));
            }

            DynamicSequenceSelection *j = i->clone();
            DynamicSequenceSelection *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            QCOMPARE(j->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(j->getConst(dmus[idx]), expectedData[idx]);
            QCOMPARE(j->get(dmus[idx]), expectedData[idx]);
            QCOMPARE(j->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->getConst(dmus[idx]), expectedData[idx]);
            QCOMPARE(k->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->get(dmus[idx]), expectedData[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(j->matchesConst(inputTimes[idx], u));
                QVERIFY(j->matches(inputTimes[idx], u));
                QVERIFY(k->matchesConst(inputTimes[idx], u));
                QVERIFY(k->matches(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(j->matchesConst(dmus[idx], u));
                QVERIFY(j->matches(dmus[idx], u));
                QVERIFY(k->matchesConst(dmus[idx], u));
                QVERIFY(k->matches(dmus[idx], u));
            }

            for (int idx2 = idx + 1; idx2 < 4; idx2++) {
                QCOMPARE(j->getConst(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(j->getConst(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(j->get(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(j->get(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->getConst(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->getConst(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(k->get(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->get(dmus[idx2]), expectedData[idx2]);

                for (const auto &u : expectedTime[idx2]) {
                    QVERIFY(j->matchesConst(inputTimes[idx2], u));
                    QVERIFY(j->matches(inputTimes[idx2], u));
                    QVERIFY(k->matchesConst(inputTimes[idx2], u));
                    QVERIFY(k->matches(inputTimes[idx2], u));
                }
                for (const auto &u : expectedData[idx2]) {
                    QVERIFY(j->matchesConst(dmus[idx2], u));
                    QVERIFY(j->matches(dmus[idx2], u));
                    QVERIFY(k->matchesConst(dmus[idx2], u));
                    QVERIFY(k->matches(dmus[idx2], u));
                }
            }

            QCOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(i->get(dmus[idx]), expectedData[idx]);
            QCOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matches(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(i->matches(dmus[idx], u));
            }
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matches(inputTimes[idx], u));
            }

            delete j;
            delete k;
        }
        delete i;


        expectedChanged.clear();
        for (int i = 0, max = configSingle.size(); i < max; i++) {
            ValueSegment seg(configSingle.at(i));
            if (FP::defined(seg.getStart())) {
                if (i == 0 ||
                        seg.getValue() != configSingle.at(i - 1).getValue() ||
                        seg.getStart() != configSingle.at(i - 1).getEnd())
                    expectedChanged |= seg.getStart();
            }
            if (FP::defined(seg.getEnd())) {
                if (i == max - 1 ||
                        seg.getValue() != configSingle.at(i + 1).getValue() ||
                        seg.getEnd() != configSingle.at(i + 1).getStart())
                    expectedChanged |= seg.getEnd();
            }
        }

        i = DynamicSequenceSelection::fromConfiguration(configSingle);
        i->registerInput(u1);
        i->registerInput(u2);
        i->registerInput(u3);
        i->registerExpected("alt", "raw", "", SequenceName::Flavors());
        QCOMPARE(i->getChangedPoints(), expectedChanged);
        QCOMPARE(i->getAllUnits(), expectedAll);
        for (int idx = 0; idx < 4; idx++) {
            QCOMPARE(i->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(i->getConst(dmus[idx]), expectedData[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matchesConst(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(i->matchesConst(dmus[idx], u));
            }

            DynamicSequenceSelection *j = i->clone();
            DynamicSequenceSelection *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            QCOMPARE(j->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(j->getConst(dmus[idx]), expectedData[idx]);
            QCOMPARE(j->get(dmus[idx]), expectedData[idx]);
            QCOMPARE(j->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->getConst(dmus[idx]), expectedData[idx]);
            QCOMPARE(k->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->get(dmus[idx]), expectedData[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(j->matchesConst(inputTimes[idx], u));
                QVERIFY(j->matches(inputTimes[idx], u));
                QVERIFY(k->matchesConst(inputTimes[idx], u));
                QVERIFY(k->matches(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(j->matchesConst(dmus[idx], u));
                QVERIFY(j->matches(dmus[idx], u));
                QVERIFY(k->matchesConst(dmus[idx], u));
                QVERIFY(k->matches(dmus[idx], u));
            }

            for (int idx2 = idx + 1; idx2 < 4; idx2++) {
                QCOMPARE(j->getConst(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(j->getConst(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(j->get(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(j->get(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->getConst(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->getConst(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(k->get(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->get(dmus[idx2]), expectedData[idx2]);

                for (const auto &u : expectedTime[idx2]) {
                    QVERIFY(j->matchesConst(inputTimes[idx2], u));
                    QVERIFY(j->matches(inputTimes[idx2], u));
                    QVERIFY(k->matchesConst(inputTimes[idx2], u));
                    QVERIFY(k->matches(inputTimes[idx2], u));
                }
                for (const auto &u : expectedData[idx2]) {
                    QVERIFY(j->matchesConst(dmus[idx2], u));
                    QVERIFY(j->matches(dmus[idx2], u));
                    QVERIFY(k->matchesConst(dmus[idx2], u));
                    QVERIFY(k->matches(dmus[idx2], u));
                }
            }

            QCOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(i->get(dmus[idx]), expectedData[idx]);
            QCOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matches(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(i->matches(dmus[idx], u));
            }
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matches(inputTimes[idx], u));
            }

            delete j;
            delete k;
        }
        delete i;


        SequenceSegment::Transfer configMulti;
        SequenceName cmName("brw", "editing", "input");
        for (int i = 0, max = configSingle.size(); i < max; i++) {
            SequenceSegment ms;
            ms.setStart(configSingle.at(i).getStart());
            ms.setEnd(configSingle.at(i).getEnd());
            ms.setValue(cmName, configSingle.at(i).root());
            configMulti.emplace_back(std::move(ms));
        }
        i = DynamicSequenceSelection::fromConfiguration(configMulti, cmName);
        i->registerInput(u1);
        i->registerInput(u2);
        i->registerInput(u3);
        i->registerExpected("alt", "raw", "", SequenceName::Flavors());
        QCOMPARE(i->getChangedPoints(), expectedChanged);
        QCOMPARE(i->getAllUnits(), expectedAll);
        for (int idx = 0; idx < 4; idx++) {
            QCOMPARE(i->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(i->getConst(dmus[idx]), expectedData[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matchesConst(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(i->matchesConst(dmus[idx], u));
            }

            DynamicSequenceSelection *j = i->clone();
            DynamicSequenceSelection *k;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << i;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
            QCOMPARE(j->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(j->getConst(dmus[idx]), expectedData[idx]);
            QCOMPARE(j->get(dmus[idx]), expectedData[idx]);
            QCOMPARE(j->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->getConst(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->getConst(dmus[idx]), expectedData[idx]);
            QCOMPARE(k->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(k->get(dmus[idx]), expectedData[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(j->matchesConst(inputTimes[idx], u));
                QVERIFY(j->matches(inputTimes[idx], u));
                QVERIFY(k->matchesConst(inputTimes[idx], u));
                QVERIFY(k->matches(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(j->matchesConst(dmus[idx], u));
                QVERIFY(j->matches(dmus[idx], u));
                QVERIFY(k->matchesConst(dmus[idx], u));
                QVERIFY(k->matches(dmus[idx], u));
            }

            for (int idx2 = idx + 1; idx2 < 4; idx2++) {
                QCOMPARE(j->getConst(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(j->getConst(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(j->get(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(j->get(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->getConst(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->getConst(dmus[idx2]), expectedData[idx2]);
                QCOMPARE(k->get(inputTimes[idx2]), expectedTime[idx2]);
                QCOMPARE(k->get(dmus[idx2]), expectedData[idx2]);

                for (const auto &u : expectedTime[idx2]) {
                    QVERIFY(j->matchesConst(inputTimes[idx2], u));
                    QVERIFY(j->matches(inputTimes[idx2], u));
                    QVERIFY(k->matchesConst(inputTimes[idx2], u));
                    QVERIFY(k->matches(inputTimes[idx2], u));
                }
                for (const auto &u : expectedData[idx2]) {
                    QVERIFY(j->matchesConst(dmus[idx2], u));
                    QVERIFY(j->matches(dmus[idx2], u));
                    QVERIFY(k->matchesConst(dmus[idx2], u));
                    QVERIFY(k->matches(dmus[idx2], u));
                }
            }

            QCOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            QCOMPARE(i->get(dmus[idx]), expectedData[idx]);
            QCOMPARE(i->get(inputTimes[idx]), expectedTime[idx]);
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matches(inputTimes[idx], u));
            }
            for (const auto &u : expectedData[idx]) {
                QVERIFY(i->matches(dmus[idx], u));
            }
            for (const auto &u : expectedTime[idx]) {
                QVERIFY(i->matches(inputTimes[idx], u));
            }

            delete j;
            delete k;
        }
        delete i;
    }

    void segmentedOperate_data()
    {
        QTest::addColumn<ValueSegment::Transfer>("configSingle");
        QTest::addColumn<QList<SequenceName::Set> >("expectedData");
        QTest::addColumn<QList<SequenceName::Set> >("expectedTime");
        QTest::addColumn<SequenceName::Set>("expectedAll");

        ValueSegment::Transfer c;
        QList<SequenceName::Set> d;
        QList<SequenceName::Set> t;
        SequenceName::Set a;
        Variant::Root v;
        for (int i = 0; i < 4; i++) {
            d << SequenceName::Set();
            t << SequenceName::Set();
        }

        QTest::newRow("Empty") << c << d << t << a;

        v.write().setString("INVALID:INVA[L]ID:INVALID");
        c.emplace_back(FP::undefined(), FP::undefined(), v);
        QTest::newRow("Never match") << c << d << t << a;

        c.clear();
        v.write().setEmpty();
        v.write().setString("brw:clean:BaB_A11");
        for (int i = 0; i < 4; i++) {
            d[i].emplace("brw", "clean", "BaB_A11");
            t[i].emplace("brw", "clean", "BaB_A11");
        }
        a.emplace("brw", "clean", "BaB_A11");
        c.emplace_back(FP::undefined(), FP::undefined(), v);
        QTest::newRow("Always single string") << c << d << t << a;

        c.clear();
        v.write().setEmpty();
        v.write().hash("Station").setString("brw");
        v.write().hash("Archive").setString("clean");
        v.write().hash("Variable").setString("BaB_A11");
        c.emplace_back(FP::undefined(), FP::undefined(), v);
        QTest::newRow("Always single hash") << c << d << t << a;

        c.clear();
        v.write().setEmpty();
        v.write().array(0).setString("brw:clean:BaB_A11");
        c.emplace_back(FP::undefined(), FP::undefined(), v);
        QTest::newRow("Always single array") << c << d << t << a;

        c.clear();
        v.write().setEmpty();
        v.write().setString("brw:clean:BaB_A1[1]");
        c.emplace_back(FP::undefined(), FP::undefined(), v);
        QTest::newRow("Always single regex string") << c << d << t << a;

        c.clear();
        v.write().setEmpty();
        v.write().array(0).setString("brw:clean:BaB_A11");
        v.write().array(1).setString("brw:clean:BaB_A12");
        for (int i = 0; i < 4; i++) {
            d[i].emplace("brw", "clean", "BaB_A12");
            t[i].emplace("brw", "clean", "BaB_A12");
        }
        a.emplace("brw", "clean", "BaB_A12");
        c.emplace_back(FP::undefined(), FP::undefined(), v);
        QTest::newRow("Always array multiple") << c << d << t << a;

        c.clear();
        v.write().array(2).setString("::BaB_A13");
        for (int i = 0; i < 4; i++) {
            d[i].emplace("alt", "raw", "BaB_A13");
            t[i].emplace("alt", "raw", "BaB_A13");
        }
        a.emplace("alt", "raw", "BaB_A13");
        c.emplace_back(FP::undefined(), FP::undefined(), v);
        QTest::newRow("Always array multiple expected") << c << d << t << a;

        c.clear();
        c.emplace_back(FP::undefined(), 1.0, v);
        c.emplace_back(1.0, 2.0, v);
        c.emplace_back(3.0, 10.0, v);
        d[1].clear();
        t[1].clear();
        QTest::newRow("Input combine") << c << d << t << a;

        c.clear();
        v.write().setEmpty();
        v.write().setString(":.*:BaB_A11");
        c.emplace_back(FP::undefined(), 4.0, v);
        v.write().setEmpty();
        v.write().setString(":clean:BaB_A[1]2");
        c.emplace_back(4.0, FP::undefined(), v);
        for (int i = 0; i < 2; i++) {
            d[i].clear();
            t[i].clear();
            d[i].emplace("alt", "raw", "BaB_A11");
            t[i].emplace("alt", "raw", "BaB_A11");
            d[i].emplace("brw", "clean", "BaB_A11");
            t[i].emplace("brw", "clean", "BaB_A11");
            d[i].emplace("brw", "raw", "BaB_A11");
            t[i].emplace("brw", "raw", "BaB_A11");
        }
        for (int i = 2; i < 4; i++) {
            d[i].clear();
            t[i].clear();
            d[i].emplace("brw", "clean", "BaB_A12");
            t[i].emplace("brw", "clean", "BaB_A12");
        }
        a.clear();
        a.emplace("alt", "raw", "BaB_A11");
        a.emplace("brw", "clean", "BaB_A11");
        a.emplace("brw", "raw", "BaB_A11");
        a.emplace("brw", "clean", "BaB_A12");
        QTest::newRow("Input two") << c << d << t << a;

        c.pop_back();
        c.emplace_back(4.0, 5.0, v);
        c.emplace_back(5.0, 6.0, v);
        c.emplace_back(6.0, 10.0, v);
        QTest::newRow("Input two combine") << c << d << t << a;

        c.clear();
        v.write().setEmpty();
        v.write().setString("brw:clean:BaB_A11");
        c.emplace_back(FP::undefined(), 3.0, v);
        v.write().setEmpty();
        v.write().setString("brw:clean:BaB_A12");
        c.emplace_back(5.0, FP::undefined(), v);
        for (int i = 0; i < 2; i++) {
            d[i].clear();
            t[i].clear();
            d[i].emplace("brw", "clean", "BaB_A11");
            t[i].emplace("brw", "clean", "BaB_A11");
        }
        d[2].clear();
        t[2].clear();
        d[3].clear();
        t[3].clear();
        d[3].emplace("brw", "clean", "BaB_A12");
        t[3].emplace("brw", "clean", "BaB_A12");
        a.clear();
        a.emplace("brw", "clean", "BaB_A11");
        a.emplace("brw", "clean", "BaB_A12");
        QTest::newRow("Input gap") << c << d << t << a;
    }

    void configurationOperateSpecial()
    {
        ValueSegment::Transfer configSingle;
        SequenceSegment::Transfer configMulti;
        SequenceName cmName("brw", "editing", "input");
        SequenceSegment dms;
        DynamicSequenceSelection *i;

        configSingle.emplace_back(FP::undefined(), FP::undefined(), Variant::Root());
        dms.setStart(FP::undefined());
        dms.setEnd(FP::undefined());
        dms.setValue(cmName, Variant::Root());
        configMulti.push_back(dms);
        i = DynamicSequenceSelection::fromConfiguration(configSingle, "/", FP::undefined(),
                                                        FP::undefined());
        i->registerInput(SequenceName("brw", "raw", "P_A11"));
        QVERIFY(i->getAllUnits().empty());
        delete i;
        i = DynamicSequenceSelection::fromConfiguration(configMulti, cmName, "/", FP::undefined(),
                                                        FP::undefined());
        i->registerInput(SequenceName("brw", "raw", "P_A11"));
        QVERIFY(i->getAllUnits().empty());
        delete i;
        configSingle.clear();
        configMulti.clear();


        configSingle.emplace_back(FP::undefined(), 3.0, Variant::Root());
        configSingle.emplace_back(3.0, FP::undefined(), Variant::Root());
        dms.setStart(FP::undefined());
        dms.setEnd(3.0);
        dms.setValue(cmName, Variant::Root());
        configMulti.push_back(dms);
        dms.setStart(3.0);
        dms.setEnd(FP::undefined());
        dms.setValue(cmName, Variant::Root());
        configMulti.push_back(dms);
        i = DynamicSequenceSelection::fromConfiguration(configSingle, "/", FP::undefined(),
                                                        FP::undefined());
        QVERIFY(i->getChangedPoints().empty());
        i->registerInput(SequenceName("brw", "raw", "P_A11"));
        QVERIFY(i->getAllUnits().empty());
        delete i;
        i = DynamicSequenceSelection::fromConfiguration(configMulti, cmName, "/", FP::undefined(),
                                                        FP::undefined());
        QVERIFY(i->getChangedPoints().empty());
        i->registerInput(SequenceName("brw", "raw", "P_A11"));
        QVERIFY(i->getAllUnits().empty());
        delete i;
        configSingle.clear();
        configMulti.clear();

        configSingle.emplace_back(3.0, 4.0, Variant::Root("brw:raw:P_S11:="));
        configSingle.emplace_back(5.0, 6.0, Variant::Root("brw:raw:P_S12:="));
        dms.setStart(3.0);
        dms.setEnd(4.0);
        dms.setValue(cmName, Variant::Root("brw:raw:P_S11:="));
        configMulti.push_back(dms);
        dms.setStart(5.0);
        dms.setEnd(6.0);
        dms.setValue(cmName, Variant::Root("brw:raw:P_S12:="));
        configMulti.push_back(dms);

        SequenceName::Set u1;
        u1.emplace("brw", "raw", "P_S11");
        SequenceName::Set u2;
        u2.emplace("brw", "raw", "P_S12");
        SequenceName::Set e;

        i = DynamicSequenceSelection::fromConfiguration(configSingle, "/", 2.0, FP::undefined());
        QCOMPARE(i->getChangedPoints(), QSet<double>() << 3.0 << 4.0 << 5.0 << 6.0);
        QCOMPARE(i->get(3.5), u1);
        QCOMPARE(i->get(4.5), e);
        QCOMPARE(i->get(5.5), u2);
        delete i;
        i = DynamicSequenceSelection::fromConfiguration(configMulti, cmName, "/", 2.0,
                                                        FP::undefined());
        QCOMPARE(i->getChangedPoints(), QSet<double>() << 3.0 << 4.0 << 5.0 << 6.0);
        QCOMPARE(i->get(3.5), u1);
        QCOMPARE(i->get(4.5), e);
        QCOMPARE(i->get(5.5), u2);
        delete i;

        i = DynamicSequenceSelection::fromConfiguration(configSingle, "/", 2.0, 4.5);
        QCOMPARE(i->getChangedPoints(), QSet<double>() << 3.0 << 4.0);
        QCOMPARE(i->get(3.5), u1);
        QCOMPARE(i->get(4.5), e);
        QCOMPARE(i->get(5.5), e);
        delete i;
        i = DynamicSequenceSelection::fromConfiguration(configMulti, cmName, "/", 2.0, 4.5);
        QCOMPARE(i->getChangedPoints(), QSet<double>() << 3.0 << 4.0);
        QCOMPARE(i->get(3.5), u1);
        QCOMPARE(i->get(4.5), e);
        QCOMPARE(i->get(5.5), e);
        delete i;

        i = DynamicSequenceSelection::fromConfiguration(configSingle, "/", 4.5, FP::undefined());
        QCOMPARE(i->getChangedPoints(), QSet<double>() << 5.0 << 6.0);
        QCOMPARE(i->get(3.5), e);
        QCOMPARE(i->get(4.5), e);
        QCOMPARE(i->get(5.5), u2);
        delete i;
        i = DynamicSequenceSelection::fromConfiguration(configMulti, cmName, "/", 4.5,
                                                        FP::undefined());
        QCOMPARE(i->getChangedPoints(), QSet<double>() << 5.0 << 6.0);
        QCOMPARE(i->get(3.5), e);
        QCOMPARE(i->get(4.5), e);
        QCOMPARE(i->get(5.5), u2);
        delete i;

        i = DynamicSequenceSelection::fromConfiguration(configSingle, "/", 7.0, FP::undefined());
        QCOMPARE(i->getChangedPoints(), QSet<double>());
        QCOMPARE(i->get(3.5), e);
        QCOMPARE(i->get(4.5), e);
        QCOMPARE(i->get(5.5), e);
        delete i;
        i = DynamicSequenceSelection::fromConfiguration(configMulti, cmName, "/", 7.0,
                                                        FP::undefined());
        QCOMPARE(i->getChangedPoints(), QSet<double>());
        QCOMPARE(i->get(3.5), e);
        QCOMPARE(i->get(4.5), e);
        QCOMPARE(i->get(5.5), e);
        delete i;

        i = DynamicSequenceSelection::fromConfiguration(configSingle, "/", FP::undefined(), 1.0);
        QCOMPARE(i->getChangedPoints(), QSet<double>());
        QCOMPARE(i->get(3.5), e);
        QCOMPARE(i->get(4.5), e);
        QCOMPARE(i->get(5.5), e);
        delete i;
        i = DynamicSequenceSelection::fromConfiguration(configMulti, cmName, "/", FP::undefined(),
                                                        1.0);
        QCOMPARE(i->getChangedPoints(), QSet<double>());
        QCOMPARE(i->get(3.5), e);
        QCOMPARE(i->get(4.5), e);
        QCOMPARE(i->get(5.5), e);
        delete i;


        configSingle.emplace_back(6.0, 7.0, Variant::Root());
        dms.setStart(6.0);
        dms.setEnd(7.0);
        dms.setValue(cmName, Variant::Root());
        configMulti.push_back(dms);
        i = DynamicSequenceSelection::fromConfiguration(configSingle, "/", FP::undefined(),
                                                        FP::undefined());
        QCOMPARE(i->get(3.5), u1);
        QCOMPARE(i->get(4.5), e);
        QCOMPARE(i->get(5.5), u2);
        QCOMPARE(i->get(6.5), e);
        delete i;
        i = DynamicSequenceSelection::fromConfiguration(configMulti, cmName, "/", FP::undefined(),
                                                        FP::undefined());
        QCOMPARE(i->get(3.5), u1);
        QCOMPARE(i->get(4.5), e);
        QCOMPARE(i->get(5.5), u2);
        QCOMPARE(i->get(6.5), e);
        delete i;
    }


    void filterOperateOptionSpecial()
    {
        DynamicSequenceSelectionOption so("", "", "", "");
        DynamicSequenceSelection *i;

        SequenceName::Set u1;
        u1.emplace("brw", "raw", "P_S11");
        SequenceName::Set u2;
        u2.emplace("brw", "raw", "P_S12");
        SequenceName::Set e;

        so.set("brw", "raw", "P_S11", SequenceName::Flavors());
        i = so.getOperator();
        QCOMPARE(i->get(3.0), u1);
        delete i;

        so.clear();
        i = so.getOperator();
        QCOMPARE(i->get(3.0), e);
        delete i;

        so.set(QList<SequenceMatch::Element>()
                       << SequenceMatch::Element(SequenceName::Component("brw"), "raw", "P_S11",
                                                 SequenceName::Flavors()));
        i = so.getOperator();
        QCOMPARE(i->get(3.0), u1);
        delete i;

        so.set(FP::undefined(), 2.0, QList<SequenceMatch::Element>()
                << SequenceMatch::Element(SequenceName::Component("brw"), "raw", "P_S12",
                                          SequenceName::Flavors()));
        so.set(4.0, 10.0, QList<SequenceMatch::Element>()
                << SequenceMatch::Element(SequenceName::Component("brw"), "raw", "P_S1[2]",
                                          SequenceName::Flavors()));
        i = so.getOperator();
        i->registerInput(SequenceName("brw", "raw", "P_S11"));
        i->registerInput(SequenceName("brw", "raw", "P_S12"));
        QCOMPARE(i->get(1.0), u2);
        QCOMPARE(i->get(2.0), u1);
        QCOMPARE(i->get(4.0), u2);
        QCOMPARE(i->get(4.5), u2);
        QCOMPARE(i->get(10.0), u1);
        QCOMPARE(i->get(20.0), u1);
        delete i;
    }
};

QTEST_APPLESS_MAIN(TestDynamicSequenceSelection)

#include "dynamicsequenceselection.moc"
