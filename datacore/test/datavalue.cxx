/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <vector>
#include <QTest>
#include <QtSql>

#include "datacore/stream.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
bool operator<(const SequenceName &a, const SequenceName &b)
{ return SequenceName::OrderLogical()(a, b); }

bool operator>(const SequenceName &a, const SequenceName &b)
{ return SequenceName::OrderLogical()(b, a); }

bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }

bool operator!=(const SequenceValue &a, const SequenceValue &b)
{ return !SequenceValue::ValueEqual()(a, b); }
}
}

class TestSequenceValue : public QObject {
Q_OBJECT

private slots:

    void unit()
    {
        SequenceName empty1;
        QCOMPARE(empty1.getStation(), SequenceName::Component(""));
        QCOMPARE(empty1.getArchive(), SequenceName::Component(""));
        QCOMPARE(empty1.getVariable(), SequenceName::Component(""));

        SequenceName empty2;
        QVERIFY(empty1 == empty2);
        QVERIFY(qHash(empty1) == qHash(empty2));
        QVERIFY(!(empty1 != empty2));
        QVERIFY(!(empty1 < empty2));
        QVERIFY(!(empty1 > empty2));

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << empty1;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> empty2;
            }
            QVERIFY(empty1 == empty2);
            QVERIFY(qHash(empty1) == qHash(empty2));
        }

        SequenceName l1("brw", "clean", "BsG_S11", {"abd"});
        QVERIFY(l1 != empty1);
        QCOMPARE(l1.getStation(), SequenceName::Component("brw"));
        QCOMPARE(l1.getArchive(), SequenceName::Component("clean"));
        QCOMPARE(l1.getVariable(), SequenceName::Component("BsG_S11"));
        QCOMPARE(l1.getFlavorsString(), SequenceName::Component("abd"));
        QVERIFY(l1.hasFlavor("abd"));
        QVERIFY(!l1.hasFlavor("abde"));
        QVERIFY(!l1.hasFlavor("ab"));
        QVERIFY(l1.lacksFlavor("abde"));
        QVERIFY(l1.lacksFlavor("ab"));
        QVERIFY(!l1.lacksFlavor("abd"));
        l1.setFlavorsString("abde");
        QCOMPARE(l1.getFlavorsString(), SequenceName::Component("abde"));
        QVERIFY(!l1.hasFlavor("abd"));
        QVERIFY(l1.hasFlavor("abde"));
        QVERIFY(!l1.hasFlavor("ab"));
        QVERIFY(!l1.lacksFlavor("abde"));
        QVERIFY(l1.lacksFlavor("ab"));
        QVERIFY(l1.lacksFlavor("abd"));
        l1.setFlavorsString("abd");

        QVERIFY(l1.toDefaultStation() == SequenceName("_", "clean", "BsG_S11", {"abd"}));
        QCOMPARE(qHash(l1.toDefaultStation()),
                 qHash(SequenceName("_", "clean", "BsG_S11", {"abd"})));
        QVERIFY(l1.toDefaultStation().isDefaultStation());
        QVERIFY(l1.toMeta() == SequenceName("brw", "clean_meta", "BsG_S11", {"abd"}));
        QCOMPARE(qHash(l1.toMeta()), qHash(SequenceName("brw", "clean_meta", "BsG_S11", {"abd"})));
        QVERIFY(l1.toMeta().isMeta());
        QVERIFY(l1.withFlavor("abd") == l1);
        QVERIFY(l1.withFlavor("abde") == SequenceName("brw", "clean", "BsG_S11", {"abd", "Abde"}));
        QCOMPARE(qHash(l1.withFlavor("abde")),
                 qHash(SequenceName("brw", "clean", "BsG_S11", {"abd", "Abde"})));
        QVERIFY(l1.withoutFlavor("abde") == l1);
        QVERIFY(l1.withoutFlavor("abd") == SequenceName("brw", "clean", "BsG_S11", {}));
        QCOMPARE(qHash(l1.withoutFlavor("abd")),
                 qHash(SequenceName("brw", "clean", "BsG_S11", {})));
        l1.setFlavorsString("");
        QVERIFY(l1 == SequenceName("brw", "clean", "BsG_S11"));
        QCOMPARE(qHash(l1), qHash(SequenceName("brw", "clean", "BsG_S11")));
        l1.setFlavorsString("abd");
        QVERIFY(l1 == SequenceName("brw", "clean", "BsG_S11", {"abd"}));
        QCOMPARE(qHash(l1), qHash(SequenceName("brw", "clean", "BsG_S11", {"abd"})));
        l1.addFlavor("qwe");
        QVERIFY(l1 == SequenceName("brw", "clean", "BsG_S11", {"abd", "qwe"}));
        QCOMPARE(qHash(l1), qHash(SequenceName("brw", "clean", "BsG_S11", {"abd", "qwe"})));
        l1.addFlavor("qwe");
        l1.removeFlavor("abd");
        QVERIFY(l1 == SequenceName("brw", "clean", "BsG_S11", {"qwe"}));
        QCOMPARE(qHash(l1), qHash(SequenceName("brw", "clean", "BsG_S11", {"qwe"})));
        l1.clearFlavors();
        QVERIFY(l1 == SequenceName("brw", "clean", "BsG_S11"));
        QCOMPARE(qHash(l1), qHash(SequenceName("brw", "clean", "BsG_S11")));
        l1.removeFlavor("abd");
        l1.addFlavor("abd");
        QVERIFY(l1 == SequenceName("brw", "clean", "BsG_S11", {"abd"}));
        QCOMPARE(qHash(l1), qHash(SequenceName("brw", "clean", "BsG_S11", {"abd"})));

        SequenceName l2(l1);
        QVERIFY(l1 == l2);
        QVERIFY(!(l1 != l2));
        l2 = l1;
        QVERIFY(l1 == l2);

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << l1;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> l2;
            }
            QVERIFY(l1 == l2);
            QVERIFY(qHash(l1) == qHash(l2));
        }

        SequenceName l3("alt", "clean", "BsG_S11");
        QVERIFY(l3 != l1);
        QVERIFY(l3 < l1);
        QVERIFY(l1 > l3);

        SequenceName l4("alt", "raw", "BsG_S11");
        QVERIFY(l3 < l4);

        SequenceName l5("alt", "raw", "BsZ_S11");
        QVERIFY(l4 < l5);
        QVERIFY(l5 < l1);

        SequenceName l6("_", "raw", "BsZ_S11");
        QVERIFY(l6 < l1);
        QVERIFY(l6 < l2);
        QVERIFY(l6 < l3);
        QVERIFY(l6 < l4);
        QVERIFY(l6 < l5);
        QVERIFY(l6.isDefaultStation());
    }

    void basic()
    {
        SequenceValue v;

        QVERIFY(!FP::defined(v.getStart()));
        QVERIFY(!FP::defined(v.getEnd()));
        QCOMPARE(v.getStation(), SequenceName::Component(""));
        QCOMPARE(v.getArchive(), SequenceName::Component(""));
        QCOMPARE(v.getVariable(), SequenceName::Component(""));
        QVERIFY(!v.getValue().exists());
        QCOMPARE(v.getPriority(), 0);

        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << v;
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            stream >> v;
        }
        QVERIFY(!FP::defined(v.getStart()));
        QVERIFY(!FP::defined(v.getEnd()));
        QCOMPARE(v.getStation(), SequenceName::Component(""));
        QCOMPARE(v.getArchive(), SequenceName::Component(""));
        QCOMPARE(v.getVariable(), SequenceName::Component(""));
        QCOMPARE(v.getPriority(), 0);
        QVERIFY(!v.read().exists());

        v.setStart(123.0);
        QCOMPARE(v.getStart(), 123.0);
        v.setEnd(456.0);
        QCOMPARE(v.getEnd(), 456.0);
        v.setStation("blarg");
        QCOMPARE(v.getStation(), SequenceName::Component("blarg"));
        v.setArchive("foobar");
        QCOMPARE(v.getArchive(), SequenceName::Component("foobar"));
        v.setVariable("bazfdf");
        QCOMPARE(v.getVariable(), SequenceName::Component("bazfdf"));
        v.setFlavors({"fgdf"});
        QCOMPARE(v.getFlavors(), (SequenceName::Flavors{"fgdf"}));
        v.setPriority(-232);
        QCOMPARE(v.getPriority(), -232);

        v.write()["/foo"] = 789.0;
        QCOMPARE(v.read().getPath("/foo").toDouble(), 789.0);

        data.clear();
        SequenceValue check;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << v;
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            stream >> check;
        }
        QCOMPARE(check.getStart(), 123.0);
        QCOMPARE(check.getEnd(), 456.0);
        QCOMPARE(check.getStation(), SequenceName::Component("blarg"));
        QCOMPARE(check.getArchive(), SequenceName::Component("foobar"));
        QCOMPARE(check.getVariable(), SequenceName::Component("bazfdf"));
        QCOMPARE(check.getPriority(), -232);


        ArchiveErasure d;
        QVERIFY(!FP::defined(d.getStart()));
        QVERIFY(!FP::defined(d.getEnd()));
        QCOMPARE(d.getStation(), SequenceName::Component(""));
        QCOMPARE(d.getArchive(), SequenceName::Component(""));
        QCOMPARE(d.getVariable(), SequenceName::Component(""));
        QCOMPARE(d.getPriority(), 0);

        data.clear();
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << d;
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            stream >> d;
        }
        QVERIFY(!FP::defined(d.getStart()));
        QVERIFY(!FP::defined(d.getEnd()));
        QCOMPARE(d.getStation(), SequenceName::Component(""));
        QCOMPARE(d.getArchive(), SequenceName::Component(""));
        QCOMPARE(d.getVariable(), SequenceName::Component(""));
        QCOMPARE(d.getPriority(), 0);

        d.setStart(123.0);
        QCOMPARE(d.getStart(), 123.0);
        d.setEnd(456.0);
        QCOMPARE(d.getEnd(), 456.0);
        d.setStation("blarg");
        QCOMPARE(d.getStation(), SequenceName::Component("blarg"));
        d.setArchive("foobar");
        QCOMPARE(d.getArchive(), SequenceName::Component("foobar"));
        d.setVariable("bazfdf");
        QCOMPARE(d.getVariable(), SequenceName::Component("bazfdf"));
        d.setFlavors({"fgdf"});
        QCOMPARE(d.getFlavors(), (SequenceName::Flavors{"fgdf"}));
        d.setPriority(-232);
        QCOMPARE(d.getPriority(), -232);

        data.clear();
        ArchiveErasure dcheck;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << d;
        }
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            stream >> dcheck;
        }
        QCOMPARE(dcheck.getStart(), 123.0);
        QCOMPARE(dcheck.getEnd(), 456.0);
        QCOMPARE(dcheck.getStation(), SequenceName::Component("blarg"));
        QCOMPARE(dcheck.getArchive(), SequenceName::Component("foobar"));
        QCOMPARE(dcheck.getVariable(), SequenceName::Component("bazfdf"));
        QCOMPARE(dcheck.getPriority(), -232);
    }

    void equality()
    {
        SequenceValue v1;
        SequenceValue v2;
        SequenceValue d1;
        SequenceValue d2;

        QVERIFY(v1 == v2);
        QVERIFY(d1 == d2);
        QVERIFY(v1 == d1);
        QVERIFY(d1 == v1);
        v1.setStart(123.0);
        QVERIFY(v1 != v2);
        QVERIFY(v1 != d1);
        QVERIFY(d1 != v1);
        v1.setEnd(456.0);
        QVERIFY(v1 != v2);
        QVERIFY(v1 != d1);
        QVERIFY(d1 != v1);
        v1.setStation("blarg");
        QVERIFY(v1 != v2);
        QVERIFY(v1 != d1);
        QVERIFY(d1 != v1);
        v1.setArchive("foobar");
        QVERIFY(v1 != v2);
        QVERIFY(v1 != d1);
        QVERIFY(d1 != v1);
        v1.setVariable("bazfdf");
        QVERIFY(v1 != v2);
        QVERIFY(v1 != d1);
        QVERIFY(d1 != v1);
        v1.setFlavors({"asda"});
        QVERIFY(v1 != v2);
        QVERIFY(v1 != d1);
        QVERIFY(d1 != v1);

        d1.setStart(123.0);
        QVERIFY(v1 != d1);
        QVERIFY(d1 != v1);
        d1.setEnd(456.0);
        QVERIFY(v1 != d1);
        QVERIFY(d1 != v1);
        d1.setStation("blarg");
        QVERIFY(v1 != d1);
        QVERIFY(d1 != v1);
        d1.setArchive("foobar");
        QVERIFY(v1 != d1);
        QVERIFY(d1 != v1);
        d1.setVariable("bazfdf");
        QVERIFY(v1 != d1);
        QVERIFY(d1 != v1);
        d1.setFlavors({"asda"});
        QVERIFY(v1 == d1);
        QVERIFY(d1 == v1);

        v2.setStart(123.0);
        d2.setStart(123.0);
        QVERIFY(v1 != v2);
        QVERIFY(d1 != d2);
        v2.setEnd(456.0);
        d2.setEnd(456.0);
        QVERIFY(v1 != v2);
        QVERIFY(d1 != d2);
        v2.setStation("blarg");
        d2.setStation("blarg");
        QVERIFY(v1 != v2);
        QVERIFY(d1 != d2);
        v2.setArchive("foobar");
        d2.setArchive("foobar");
        QVERIFY(v1 != v2);
        QVERIFY(d1 != d2);
        v2.setVariable("bazfdf");
        d2.setVariable("bazfdf");
        QVERIFY(v1 != v2);
        QVERIFY(d1 != d2);
        v2.setFlavors({"asda"});
        d2.setFlavors({"asda"});
        QVERIFY(v1 == v2);
        QVERIFY(d1 == d2);
        v1.setPriority(3);
        QVERIFY(d1 != v1);
        QVERIFY(v1 != d1);
        d1.setPriority(3);
        QVERIFY(v1 != v2);
        QVERIFY(d1 != d2);

        QVERIFY(v1 != d2);
        QVERIFY(d2 != v1);
        d2 = v1;
        QVERIFY(v1 == d2);
        QVERIFY(d2 == v1);

        SequenceValue v3(v1);
        QVERIFY(v1 == v3);
        SequenceValue v4;
        v4 = v1;
        QVERIFY(v1 == v4);
    }

    void defaultRefernce()
    {
        SequenceValue v1;
        SequenceValue v2;

        v1.write() = 12.0;
        v2.write() = 13.0;
        QCOMPARE(v1.getValue().toDouble(), 12.0);
        QCOMPARE(v2.getValue().toDouble(), 13.0);

        v1 = SequenceValue();
        v2 = SequenceValue();
        v1.write() = 14.0;
        v2.write() = 15.0;
        QCOMPARE(v1.write().toDouble(), 14.0);
        QCOMPARE(v2.write().toDouble(), 15.0);

        v1 = SequenceValue();
        v2 = SequenceValue();
        v1.setRoot(Variant::Root(16.0));
        v2.setRoot(Variant::Root(17.0));
        QCOMPARE(v1.root().read().toDouble(), 16.0);
        QCOMPARE(v2.root().read().toDouble(), 17.0);

        v1 = SequenceValue();
        v1.setStation("blah");
        v2 = SequenceValue();
        v2.setArchive("blag");
        v1.setRoot(Variant::Root(18.0));
        v2.setRoot(Variant::Root(19.0));
        QCOMPARE(v1.read().toDouble(), 18.0);
        QCOMPARE(v2.read().toDouble(), 19.0);

        v1 = SequenceValue();
        v1.setVariable("foo");
        v2 = SequenceValue();
        v2.setFlavors({"baz"});
        v1.setRoot(Variant::Root(20.0));
        v2.setRoot(Variant::Root(21.0));
        QCOMPARE(v1.read().toDouble(), 20.0);
        QCOMPARE(v2.read().toDouble(), 21.0);

        v1 = SequenceValue();
        v1.setPriority(1);
        v2 = SequenceValue();
        v1.setRoot(Variant::Root(22.0));
        v2.setRoot(Variant::Root(23.0));
        QCOMPARE(v1.read().toDouble(), 22.0);
        QCOMPARE(v2.read().toDouble(), 23.0);

        SequenceValue v3;
        v3.setRoot(Variant::Root(1.0));
        QCOMPARE(v3.getValue().toDouble(), 1.0);
        SequenceValue v4;
        v4.setRoot(Variant::Root(2.0));
        QCOMPARE(v4.getValue().toDouble(), 2.0);
        QCOMPARE(v3.getValue().toDouble(), 1.0);

        SequenceValue v5;
        QVERIFY(!v5.read().exists());
        QCOMPARE(v4.getValue().toDouble(), 2.0);
        QCOMPARE(v3.getValue().toDouble(), 1.0);
        QCOMPARE(v1.read().toDouble(), 22.0);
        QCOMPARE(v2.read().toDouble(), 23.0);
    }

    void outputOrdering()
    {
        QFETCH(SequenceName, v1);
        QFETCH(SequenceName, v2);
        QFETCH(int, result);
        bool a = SequenceName::OrderDisplay()(v1, v2);
        bool b = SequenceName::OrderDisplay()(v2, v1);
        int test;
        if (a && !b)
            test = -1;
        else if (b && !a)
            test = 1;
        else
            test = 0;
        QCOMPARE(test, result);
    }

    void outputOrdering_data()
    {
        QTest::addColumn<SequenceName>("v1");
        QTest::addColumn<SequenceName>("v2");
        QTest::addColumn<int>("result");

        QTest::newRow("Flags first") << SequenceName("brw", "raw", "F1_S11")
                                     << SequenceName("brw", "raw", "BsG_S11") << -1;
        QTest::newRow("Scattering first") << SequenceName("brw", "raw", "BsB_S11")
                                          << SequenceName("brw", "raw", "BaB_A11") << -1;
        QTest::newRow("Scattering color") << SequenceName("brw", "raw", "BsG_S11")
                                          << SequenceName("brw", "raw", "BsB_S11") << 1;
        QTest::newRow("Back scattering first") << SequenceName("brw", "raw", "BbsR_S11")
                                               << SequenceName("brw", "raw", "BaB_A11") << -1;
        QTest::newRow("Aeth before cut") << SequenceName("brw", "raw", "ZIr1_S11", {})
                                         << SequenceName("brw", "raw", "ZFOOBAR_X1", {"pm10"})
                                         << -1;
    }

#if 0
    void benchQListWrite()
    {
        SequenceName u("a", "b", "c");
        QBENCHMARK {
            SequenceValue::Transfer values;
            for (int i=0; i<10000; i++) {
                values.append(SequenceValue(u, Variant::Root()));
            }
        }
    }
    void benchQListRead()
    {
        SequenceName u("a", "b", "c");
        SequenceValue::Transfer values;
        for (int i=0; i<10000; i++) {
            values.append(SequenceValue(u, Variant::Root(1.0)));
        }
        QBENCHMARK {
            for (int i=0; i<10000; i++) {
                values[i].value().setDouble(2.0);
            }
        }
    }

    void benchSTDVectorWrite()
    {
        SequenceName u("a", "b", "c");
        QBENCHMARK {
            std::vector<SequenceValue> values;
            for (int i=0; i<10000; i++) {
                values.push_back(SequenceValue(u, Variant::Root()));
            }
        }
    }
    void benchSTDVectorRead()
    {
        SequenceName u("a", "b", "c");
        std::vector<SequenceValue> values;
        for (int i=0; i<10000; i++) {
            values.push_back(SequenceValue(u, Variant::Root(1.0)));
        }
        QBENCHMARK {
            for (int i=0; i<10000; i++) {
                values[i].value().setDouble(2.0);
            }
        }
    }
#endif
};

QTEST_APPLESS_MAIN(TestSequenceValue)

#include "datavalue.moc"
