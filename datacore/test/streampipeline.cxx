/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QString>
#include <QTemporaryFile>

#include "datacore/streampipeline.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/archive/access.hxx"
#include "io/drivers/file.hxx"

using namespace CPD3;
using namespace CPD3::Data;


struct IOPipe {
    IO::Access::Handle read;
    IO::Access::Handle write;

    IOPipe()
    {
        struct Shared {
            std::mutex mutex;
            std::condition_variable cv;
            enum class ReadState {
                Initialize, InitializeEnded, Starting, Running, Ended
            } readState = ReadState::Initialize;
            Util::ByteArray readPending;
            Threading::Signal<const Util::ByteArray &> streamRead;
            Threading::Signal<> ended;
        };

        class ReadBacking : public IO::Generic::Backing {
            class Stream : public IO::Generic::Stream {
                std::shared_ptr<Shared> shared;
            public:
                explicit Stream(const std::shared_ptr<Shared> &shared) : shared(shared)
                {
                    read = shared->streamRead;
                    ended = shared->ended;
                }

                virtual ~Stream() = default;

                void start() override
                {
                    Util::ByteArray emitData;
                    {
                        std::unique_lock<std::mutex> lock(shared->mutex);
                        switch (shared->readState) {
                        case Shared::ReadState::Initialize:
                            emitData = std::move(shared->readPending);
                            shared->readPending.clear();
                            if (emitData.empty()) {
                                shared->readState = Shared::ReadState::Running;
                                return;
                            }
                            shared->readState = Shared::ReadState::Starting;
                            break;
                        case Shared::ReadState::InitializeEnded:
                            shared->readState = Shared::ReadState::Ended;
                            lock.unlock();
                            if (!shared->readPending.empty())
                                read(shared->readPending);
                            ended();
                            return;
                        case Shared::ReadState::Starting:
                        case Shared::ReadState::Running:
                            Q_ASSERT(false);
                            return;
                        case Shared::ReadState::Ended:
                            return;
                        }
                    }

                    read(emitData);

                    {
                        std::lock_guard<std::mutex> lock(shared->mutex);
                        Q_ASSERT(shared->readState == Shared::ReadState::Starting);
                        shared->readState = Shared::ReadState::Running;
                    }
                    shared->cv.notify_all();
                }

                bool isEnded() const override
                {
                    std::lock_guard<std::mutex> lock(shared->mutex);
                    switch (shared->readState) {
                    case Shared::ReadState::Initialize:
                    case Shared::ReadState::InitializeEnded:
                    case Shared::ReadState::Starting:
                    case Shared::ReadState::Running:
                        return false;
                    case Shared::ReadState::Ended:
                        return true;
                    }
                    Q_ASSERT(false);
                    return false;
                }

                void write(const Util::ByteView &) override
                { }
            };

        public:
            std::shared_ptr<Shared> shared;

            explicit ReadBacking(const std::shared_ptr<Shared> &shared) : shared(shared)
            { }

            virtual ~ReadBacking() = default;

            std::unique_ptr<IO::Generic::Stream> stream() override
            { return std::unique_ptr<IO::Generic::Stream>(new Stream(shared)); }

            std::unique_ptr<IO::Generic::Block> block() override
            { return {}; }
        };

        class WriteBacking : public IO::Generic::Backing {
            class Stream : public IO::Generic::Stream {
                std::shared_ptr<Shared> shared;
            public:
                explicit Stream(const std::shared_ptr<Shared> &shared) : shared(shared)
                { }

                virtual ~Stream()
                {
                    {
                        std::unique_lock<std::mutex> lock(shared->mutex);
                        switch (shared->readState) {
                        case Shared::ReadState::Initialize:
                            shared->readState = Shared::ReadState::InitializeEnded;
                            return;
                        case Shared::ReadState::Starting:
                            shared->cv.wait(lock, [this] {
                                return shared->readState == Shared::ReadState::Running;
                            });
                            break;
                        case Shared::ReadState::Running:
                            break;
                        case Shared::ReadState::InitializeEnded:
                        case Shared::ReadState::Ended:
                            Q_ASSERT(false);
                            break;
                        }

                        shared->readState = Shared::ReadState::Ended;
                    }

                    shared->ended();
                }

                void start() override
                { }

                bool isEnded() const override
                { return false; }

                void write(const Util::ByteView &data) override
                {
                    {
                        std::unique_lock<std::mutex> lock(shared->mutex);
                        switch (shared->readState) {
                        case Shared::ReadState::Initialize:
                            shared->readPending += data;
                            return;
                        case Shared::ReadState::Starting:
                            shared->cv.wait(lock, [this] {
                                return shared->readState == Shared::ReadState::Running;
                            });
                            break;
                        case Shared::ReadState::Running:
                            break;
                        case Shared::ReadState::InitializeEnded:
                        case Shared::ReadState::Ended:
                            Q_ASSERT(false);
                            break;
                        }
                    }

                    shared->streamRead(Util::ByteArray(data));
                }
            };

        public:
            std::shared_ptr<Shared> shared;

            explicit WriteBacking(const std::shared_ptr<Shared> &shared) : shared(shared)
            { }

            virtual ~WriteBacking() = default;

            std::unique_ptr<IO::Generic::Stream> stream() override
            { return std::unique_ptr<IO::Generic::Stream>(new Stream(shared)); }

            std::unique_ptr<IO::Generic::Block> block() override
            { return {}; }
        };

        auto shared = std::make_shared<Shared>();
        read = std::make_shared<ReadBacking>(shared);
        write = std::make_shared<WriteBacking>(shared);
    }
};

class InterlockGate {
    std::mutex mutex;
    std::condition_variable cv;
    enum class State {
        Released, FutureBlocked, BlockedInCall,
    } state;
    int receiveCounter;
public:
    InterlockGate() : state(State::Released), receiveCounter(0)
    { }

    void blockFuture()
    {
        std::lock_guard<std::mutex> lock(mutex);
        state = State::FutureBlocked;
    }

    void unblock()
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            state = State::Released;
        }
        cv.notify_all();
    }

    void called()
    {
        std::unique_lock<std::mutex> lock(mutex);
        receiveCounter++;

        for (;;) {
            switch (state) {
            case State::Released:
                return;
            case State::FutureBlocked:
                state = State::BlockedInCall;
                cv.notify_all();
                /* Fall through */
            case State::BlockedInCall:
                cv.wait(lock);
                break;
            }
        }
    }

    void waitFor()
    {
        std::unique_lock<std::mutex> lock(mutex);
        cv.wait(lock, [this] { return state == State::BlockedInCall; });
    }

    void waitForAndUnblock()
    {
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [this] { return state == State::BlockedInCall; });
            state = State::Released;
        }
        cv.notify_all();
    }

    void blockUntilReceived()
    {
        {
            std::unique_lock<std::mutex> lock(mutex);
            if (receiveCounter > 0)
                return;
            state = State::FutureBlocked;
            cv.wait(lock, [this] { return state == State::BlockedInCall; });
            state = State::Released;
        }
        cv.notify_all();
    }

    bool wasReceived()
    {
        std::lock_guard<std::mutex> lock(mutex);
        return receiveCounter > 0;
    }

    int countReceived()
    {
        std::lock_guard<std::mutex> lock(mutex);
        return receiveCounter;
    }
};

class StaticDataSource : public ExternalSource {
    bool started = false;
    bool ended = false;
public:
    SequenceValue::Transfer values;

    StaticDataSource() = default;

    explicit StaticDataSource(SequenceValue::Transfer values) : values(std::move(values))
    { }

    virtual ~StaticDataSource() = default;

    void signalTerminate() override
    { }

    bool isFinished() override
    { return started && ended; }

    void start() override
    {
        started = true;
        if (ended) {
            finished();
        }
    }

    void setEgress(StreamSink *sink) override
    {
        if (!sink)
            return;
        if (ended)
            return;

        sink->incomingData(values);
        sink->endData();
        ended = true;
        if (started) {
            finished();
        }
    }
};

class StaticDataConverter : public ExternalSourceProcessor {
public:
    SequenceValue::Transfer values;
    struct Received {
        Util::ByteArray data;
        bool ended = false;
    };
    std::shared_ptr<Received> received;

    StaticDataConverter() : received(std::make_shared<Received>())
    { }

    explicit StaticDataConverter(SequenceValue::Transfer values) : values(std::move(values)),
                                                                   received(std::make_shared<
                                                                           Received>())
    { }

    virtual ~StaticDataConverter() = default;

    bool prepare() override
    {
        received->data += std::move(buffer);
        received->ended = received->ended || dataEnded;
        return true;
    }

    bool process() override
    {
        egress->incomingData(values);
        egress->endData();
        return false;
    }
};

class SimpleDataSink : public ExternalSink {
    std::mutex mutex;
    bool started = false;
public:
    struct Received {
        SequenceValue::Transfer values;
        bool ended = false;
    };
    std::shared_ptr<Received> received;

    SimpleDataSink() : received(std::make_shared<Received>())
    { }

    virtual ~SimpleDataSink() = default;

    bool isFinished() override
    {
        std::lock_guard<std::mutex> lock(mutex);
        return started && received->ended;
    }

    void start() override
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            started = true;
            if (!received->ended)
                return;
        }
        finished();
    }

    void incomingData(const SequenceValue::Transfer &values) override
    { Util::append(values, received->values); }

    void endData() override
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            received->ended = true;
            if (!started)
                return;
        }
        finished();
    }
};

class CaptureStage : public ProcessingStageThread {
public:
    SequenceValue::Transfer prefix;
    SequenceName::Set request;

    struct Received {
        SequenceValue::Transfer values;
        bool ended = false;
    };
    std::shared_ptr<Received> received;

    CaptureStage() : received(std::make_shared<Received>())
    { }

    explicit CaptureStage(std::shared_ptr<Received> received) : received(std::move(received))
    { }

    virtual ~CaptureStage() = default;

    SequenceName::Set requestedInputs() override
    {
        SequenceName::Set result;
        Util::merge(request, result);
        return result;
    }

    void serialize(QDataStream &stream) override
    {
        QByteArray testData("test test test");
        stream << testData;
        received.reset();
    }

    class Component : public ProcessingStageComponent {
        QString name;
    public:
        std::shared_ptr<Received> received;

        Component(const QString &name, std::shared_ptr<Received> received) : name(name),
                                                                             received(std::move(
                                                                                     received))
        { }

        virtual ~Component() = default;

        ProcessingStage *createGeneralFilterDynamic(const ComponentOptions &options = {}) override
        { return nullptr; };

        ProcessingStage *deserializeGeneralFilter(QDataStream &stream) override
        {
            QByteArray testData;
            stream >> testData;
            if (testData != "test test test") {
                Q_ASSERT(false);
                return nullptr;
            }
            return new CaptureStage(received);
        }

        QString getGeneralSerializationName() const override
        { return name; }
    };

protected:
    void process(SequenceValue::Transfer &&incoming) override
    {
        if (!prefix.empty()) {
            egress->incomingData(std::move(prefix));
            prefix.clear();
        }
        if (received) {
            Util::append(incoming, received->values);
        }
        egress->incomingData(std::move(incoming));
    }

    void finish() override
    {
        if (!prefix.empty()) {
            egress->incomingData(std::move(prefix));
            prefix.clear();
        }
        if (received) {
            received->ended = true;
        }
        egress->endData();
    }
};

class PipelineOverride : public StreamPipeline {
public:
    IO::Access::Handle input;
    IO::Access::Handle output;

    InterlockGate interlock_acceleration_deserialization;

    std::unordered_map<QString, std::unique_ptr<CaptureStage::Component>> deserializeLookup;

    InterlockGate interlock_acceleration_client_connection;
    InterlockGate interlock_acceleration_client_detaching;
    InterlockGate interlock_acceleration_client_detachAborted;
    InterlockGate interlock_acceleration_client_forwarded;
    InterlockGate interlock_acceleration_client_detachCompleted;
    InterlockGate interlock_acceleration_client_sendStart;
    InterlockGate interlock_acceleration_client_sendDenied;
    InterlockGate interlock_acceleration_client_sendCompleted;
    InterlockGate interlock_acceleration_client_handoffStart;
    InterlockGate interlock_acceleration_client_handoffComplete;

    InterlockGate interlock_acceleration_server_connection;
    InterlockGate interlock_acceleration_server_forward;
    InterlockGate interlock_acceleration_server_detaching;
    InterlockGate interlock_acceleration_server_detachAborted;
    InterlockGate interlock_acceleration_server_detachFailed;
    InterlockGate interlock_acceleration_server_incomingSend;
    InterlockGate interlock_acceleration_server_handoff;

    explicit PipelineOverride(bool acceleration = false,
                              bool serialization = false,
                              bool forwarding = false) : StreamPipeline(acceleration, serialization,
                                                                        forwarding)
    { }

    virtual ~PipelineOverride() = default;

protected:
    IO::Access::Handle createPipelineInput() override
    { return input; }

    IO::Access::Handle createPipelineOutput() override
    { return output; }

    ProcessingStageComponent *getDeserializationComponent(const QString &name) override
    {
        interlock_acceleration_deserialization.called();

        auto check = deserializeLookup.find(name);
        if (check == deserializeLookup.end())
            return nullptr;
        return check->second.get();
    }


    void acceleration_client_connection() override
    { interlock_acceleration_client_connection.called(); }

    void acceleration_client_detaching() override
    { interlock_acceleration_client_detaching.called(); }

    void acceleration_client_detachAborted() override
    { interlock_acceleration_client_detachAborted.called(); }

    void acceleration_client_forwarded() override
    { interlock_acceleration_client_forwarded.called(); }

    void acceleration_client_detachCompleted() override
    { interlock_acceleration_client_detachCompleted.called(); }

    void acceleration_client_sendStart() override
    { interlock_acceleration_client_sendStart.called(); }

    void acceleration_client_sendDenied() override
    { interlock_acceleration_client_sendDenied.called(); }

    void acceleration_client_sendCompleted() override
    { interlock_acceleration_client_sendCompleted.called(); }

    void acceleration_client_handoffStart() override
    { interlock_acceleration_client_handoffStart.called(); }

    void acceleration_client_handoffComplete() override
    { interlock_acceleration_client_handoffComplete.called(); }

    void acceleration_server_connection() override
    { interlock_acceleration_server_connection.called(); }

    void acceleration_server_forward() override
    { interlock_acceleration_server_forward.called(); }

    void acceleration_server_detaching() override
    { interlock_acceleration_server_detaching.called(); }

    void acceleration_server_detachAborted() override
    { interlock_acceleration_server_detachAborted.called(); }

    void acceleration_server_detachFailed() override
    { interlock_acceleration_server_detachFailed.called(); }

    void acceleration_server_incomingSend() override
    { interlock_acceleration_server_incomingSend.called(); }

    void acceleration_server_handoff() override
    { interlock_acceleration_server_handoff.called(); }
};


namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestStreamPipeline : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void basicExternal()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        PipelineOverride pipeline;
        auto target = pipeline.setInputExternal();
        QVERIFY(target != nullptr);
        QVERIFY(pipeline.setOutputIngress(&sink));
        QVERIFY(pipeline.start());

        target->incomingData(values);
        target->endData();

        QVERIFY(pipeline.wait());

        QCOMPARE(sink.values(), values);
    }

    void immediateExternal()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        PipelineOverride pipeline;
        auto target = pipeline.setInputExternal();
        QVERIFY(target != nullptr);
        QVERIFY(pipeline.setOutputIngress(&sink));

        std::thread([&] {
            target->incomingData(values);
            target->endData();
        }).detach();
        QTest::qSleep(50);
        QVERIFY(pipeline.start());

        QVERIFY(pipeline.waitInEventLoop());

        QCOMPARE(sink.values(), values);
    }

    void fileSource()
    {
        auto file = IO::Access::temporaryFile();
        QVERIFY(file.get() != nullptr);

        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        {
            StandardDataOutput output(file->stream());
            output.start();
            output.incomingData(values);
            output.endData();
            output.wait();
        }

        PipelineOverride pipeline;
        QVERIFY(pipeline.setOutputIngress(&sink));
        QVERIFY(pipeline.setInputFile(file->filename()));

        QVERIFY(pipeline.start());

        QVERIFY(pipeline.wait());

        QCOMPARE(sink.values(), values);
    }

    void archiveSource()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        Archive::Access(databaseFile).writeSynchronous(values);

        PipelineOverride pipeline;
        pipeline.setInputExternal();
        QVERIFY(pipeline.setOutputIngress(&sink));
        QVERIFY(pipeline.setInputArchive(Archive::Selection()));

        QVERIFY(pipeline.start());

        QVERIFY(pipeline.wait());

        QCOMPARE(sink.values(), values);
    }

    void archiveSourceUnowned()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        Archive::Access access(databaseFile);
        access.writeSynchronous(values);

        PipelineOverride pipeline;
        pipeline.setInputExternal();
        QVERIFY(pipeline.setOutputIngress(&sink));
        QVERIFY(pipeline.setInputArchive(Archive::Selection::List{Archive::Selection()},
                                         Time::Bounds(101, 399), &access));

        QVERIFY(pipeline.start());

        QVERIFY(pipeline.wait());

        values.front().setStart(101);
        values.back().setEnd(399);
        QCOMPARE(sink.values(), values);
    }

    void generalSource()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        PipelineOverride pipeline;
        QVERIFY(pipeline.setInputSource(
                std::unique_ptr<ExternalSource>(new StaticDataSource(values))));
        QVERIFY(pipeline.setOutputIngress(&sink));
        QVERIFY(pipeline.start());

        QVERIFY(pipeline.wait());

        QCOMPARE(sink.values(), values);
    }

    void generalConverter()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        PipelineOverride pipeline;
        QVERIFY(pipeline.setInputGeneral(
                std::unique_ptr<ExternalConverter>(new StaticDataConverter(values))));
        QVERIFY(pipeline.setOutputIngress(&sink));
        QVERIFY(pipeline.start());

        QVERIFY(pipeline.wait());

        QCOMPARE(sink.values(), values);
    }

    void generalConverterData()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;
        Util::ByteArray data("data data data");

        PipelineOverride pipeline;
        auto conv = std::unique_ptr<StaticDataConverter>(new StaticDataConverter(values));
        auto received = conv->received;
        QVERIFY(pipeline.setInputGeneral(std::move(conv), IO::Access::buffer(data)->stream()));
        QVERIFY(pipeline.setOutputIngress(&sink));
        QVERIFY(pipeline.start());

        QVERIFY(pipeline.wait());

        QCOMPARE(sink.values(), values);
        QVERIFY(received->ended);
        QCOMPARE(received->data, data);
    }

    void pipelineSource()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;
        Util::ByteArray data("data data data");

        IOPipe inputPipe;
        PipelineOverride pipeline;
        pipeline.input = inputPipe.read;
        QVERIFY(pipeline.setInputPipeline());
        QVERIFY(pipeline.setOutputIngress(&sink));
        QVERIFY(pipeline.start());

        {
            StandardDataOutput output(inputPipe.write->stream());
            output.start();
            output.incomingData(values);
            output.endData();
            output.wait();
        }

        QVERIFY(pipeline.wait());

        QCOMPARE(sink.values(), values);
    }

    void fileSink()
    {
        auto file = IO::Access::temporaryFile();
        QVERIFY(file.get() != nullptr);

        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};

        PipelineOverride pipeline;
        auto target = pipeline.setInputExternal();
        QVERIFY(target != nullptr);
        QVERIFY(pipeline.setOutputFile(file->filename()));
        QVERIFY(pipeline.start());

        target->incomingData(values);
        target->endData();

        QVERIFY(pipeline.wait());

        StreamSink::Buffer sink;
        {
            StandardDataInput input;
            input.start();
            input.setEgress(&sink);
            input.incomingData(file->stream()->readAll());
            input.endData();
            input.wait();
        }
        QCOMPARE(sink.values(), values);
    }

    void generalSink()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};

        PipelineOverride pipeline;
        auto target = pipeline.setInputExternal();
        QVERIFY(target != nullptr);
        auto sink = std::unique_ptr<SimpleDataSink>(new SimpleDataSink);
        auto received = sink->received;
        QVERIFY(pipeline.setOutputGeneral(std::move(sink)));
        QVERIFY(pipeline.start());

        target->incomingData(values);
        target->endData();

        QVERIFY(pipeline.wait());

        QVERIFY(received->ended);
        QCOMPARE(received->values, values);
    }

    void tapChainSink()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};

        PipelineOverride pipeline;
        auto target = pipeline.setInputExternal();
        QVERIFY(target != nullptr);

        StreamSink::Buffer sink;
        ProcessingTapChain tapChain;
        tapChain.add(&sink, Variant::Read::empty(),
                     SequenceMatch::Composite(SequenceMatch::Element("bnd", "raw", "BsG_S11")));
        QVERIFY(pipeline.setOutputFilterChain(&tapChain));
        QVERIFY(pipeline.start());

        target->incomingData(values);
        target->endData();

        QVERIFY(pipeline.wait());

        QCOMPARE(sink.values(), values);
    }

    void pipelineSink()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};

        IOPipe outputPipe;
        PipelineOverride pipeline;
        pipeline.output = outputPipe.write;
        auto target = pipeline.setInputExternal();
        QVERIFY(target != nullptr);
        QVERIFY(pipeline.setOutputPipeline());
        QVERIFY(pipeline.start());

        target->incomingData(values);
        target->endData();

        QVERIFY(pipeline.wait());

        StreamSink::Buffer sink;
        {
            StandardDataInput input;
            input.start();
            input.setEgress(&sink);
            input.incomingData(outputPipe.read->stream()->readAll());
            input.endData();
            input.wait();
        }
        QCOMPARE(sink.values(), values);
    }

    void oneStage()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        PipelineOverride pipeline;
        auto target = pipeline.setInputExternal();
        QVERIFY(target != nullptr);
        QVERIFY(pipeline.setOutputIngress(&sink));
        auto stageOne = std::unique_ptr<CaptureStage>(new CaptureStage);
        auto receivedOne = stageOne->received;
        QVERIFY(pipeline.addProcessingStage(std::move(stageOne)));
        QVERIFY(pipeline.start());

        target->incomingData(values);
        target->endData();

        QVERIFY(pipeline.wait());

        QCOMPARE(sink.values(), values);
        QVERIFY(receivedOne->ended);
        QCOMPARE(receivedOne->values, values);
    }

    void twoStage()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        PipelineOverride pipeline;
        auto target = pipeline.setInputExternal();
        QVERIFY(target != nullptr);
        QVERIFY(pipeline.setOutputIngress(&sink));
        auto stageOne = std::unique_ptr<CaptureStage>(new CaptureStage);
        stageOne->prefix
                .emplace_back(
                        SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(0.5), 50.0, 100.0));
        auto receivedOne = stageOne->received;
        QVERIFY(pipeline.addProcessingStage(std::move(stageOne)));
        auto stageTwo = std::unique_ptr<CaptureStage>(new CaptureStage);
        auto receivedTwo = stageTwo->received;
        QVERIFY(pipeline.addProcessingStage(std::move(stageTwo)));
        QVERIFY(pipeline.start());

        target->incomingData(values);
        target->endData();

        QVERIFY(pipeline.wait());

        QVERIFY(receivedOne->ended);
        QCOMPARE(receivedOne->values, values);

        values.insert(values.begin(),
                      SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(0.5), 50.0, 100.0));
        QVERIFY(receivedTwo->ended);
        QCOMPARE(receivedTwo->values, values);
        QCOMPARE(sink.values(), values);
    }

    void tapChainBypass()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};

        Archive::Access(databaseFile).writeSynchronous(values);

        PipelineOverride pipeline;
        QVERIFY(pipeline.setInputArchive(Archive::Selection()));

        StreamSink::Buffer sink;
        ProcessingTapChain tapChain;
        tapChain.add(&sink, Variant::Read::empty(),
                     SequenceMatch::Composite(SequenceMatch::Element("bnd", "raw", "BsG_S11")));
        QVERIFY(pipeline.setOutputFilterChain(&tapChain));

        QVERIFY(pipeline.start());

        QVERIFY(pipeline.wait());

        QCOMPARE(sink.values(), values);
    }

    void tapChainRestartProcessing()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};

        PipelineOverride pipeline;
        auto target = pipeline.setInputExternal();
        QVERIFY(target != nullptr);
        auto stageOne = std::unique_ptr<CaptureStage>(new CaptureStage);
        stageOne->prefix
                .emplace_back(
                        SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(0.5), 50.0, 100.0));
        auto receivedOne = stageOne->received;
        QVERIFY(pipeline.addProcessingStage(std::move(stageOne)));

        StreamSink::Buffer sink;
        ProcessingTapChain tapChain;
        tapChain.add(&sink, Variant::Read::empty(),
                     SequenceMatch::Composite(SequenceMatch::Element("bnd", "raw", "BsG_S11")));
        QVERIFY(pipeline.setOutputFilterChain(&tapChain, true));

        QVERIFY(pipeline.start());

        target->incomingData(values);
        target->endData();

        QVERIFY(pipeline.wait());

        QVERIFY(receivedOne->ended);
        QCOMPARE(receivedOne->values, values);

        values.insert(values.begin(),
                      SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(0.5), 50.0, 100.0));
        QCOMPARE(sink.values(), values);

        receivedOne->ended = false;
        receivedOne->values.clear();
        sink.reset();
        QVERIFY(pipeline.setOutputFilterChain(&tapChain, true));
        QVERIFY(pipeline.start());
        QVERIFY(pipeline.wait());
        QCOMPARE(sink.values(), values);
    }

    void tapChainRestartArchive()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};

        Archive::Access(databaseFile).writeSynchronous(values);

        PipelineOverride pipeline;
        QVERIFY(pipeline.setInputArchive(Archive::Selection()));

        StreamSink::Buffer sink;
        ProcessingTapChain tapChain;
        tapChain.add(&sink, Variant::Read::empty(),
                     SequenceMatch::Composite(SequenceMatch::Element("bnd", "raw", "BsG_S11")));
        QVERIFY(pipeline.setOutputFilterChain(&tapChain, true));

        QVERIFY(pipeline.start());

        QVERIFY(pipeline.wait());

        QCOMPARE(sink.values(), values);

        sink.reset();
        QVERIFY(pipeline.setOutputFilterChain(&tapChain, true));
        QVERIFY(pipeline.start());
        QVERIFY(pipeline.wait());
        QCOMPARE(sink.values(), values);
    }

    void tapChainRestartFile()
    {
        auto file = IO::Access::temporaryFile();
        QVERIFY(file.get() != nullptr);

        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};

        {
            StandardDataOutput output(file->stream());
            output.start();
            output.incomingData(values);
            output.endData();
            output.wait();
        }

        PipelineOverride pipeline;
        QVERIFY(pipeline.setInputFile(file->filename()));

        StreamSink::Buffer sink;
        ProcessingTapChain tapChain;
        tapChain.add(&sink, Variant::Read::empty(),
                     SequenceMatch::Composite(SequenceMatch::Element("bnd", "raw", "BsG_S11")));
        QVERIFY(pipeline.setOutputFilterChain(&tapChain, true));

        QVERIFY(pipeline.start());

        QVERIFY(pipeline.wait());

        QCOMPARE(sink.values(), values);

        sink.reset();
        QVERIFY(pipeline.setOutputFilterChain(&tapChain, true));
        QVERIFY(pipeline.start());
        QVERIFY(pipeline.wait());
        QCOMPARE(sink.values(), values);
    }

    void stageArchiveRequest()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        Archive::Access(databaseFile).writeSynchronous(values);
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "BsR_S11"}, Variant::Root(1.5), 100.0, 200.0)});

        PipelineOverride pipeline;
        QVERIFY(pipeline.setOutputIngress(&sink));
        auto stageOne = std::unique_ptr<CaptureStage>(new CaptureStage);
        stageOne->request.insert(SequenceName("bnd", "raw", "BsG_S11"));
        auto receivedOne = stageOne->received;
        QVERIFY(pipeline.addProcessingStage(std::move(stageOne)));
        QVERIFY(pipeline.setInputArchiveRequested());
        QVERIFY(pipeline.start());

        QVERIFY(pipeline.wait());

        QCOMPARE(sink.values(), values);
        QVERIFY(receivedOne->ended);
        QCOMPARE(receivedOne->values, values);
    }

    void tapChainRequest()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};

        Archive::Access(databaseFile).writeSynchronous(values);

        PipelineOverride pipeline;

        StreamSink::Buffer sink;
        ProcessingTapChain tapChain;
        tapChain.add(&sink, Variant::Read::empty(),
                     SequenceMatch::Composite(SequenceMatch::Element("bnd", "raw", "BsG_S11")));
        QVERIFY(pipeline.setOutputFilterChain(&tapChain));
        QVERIFY(pipeline.setInputArchiveRequested());

        QVERIFY(pipeline.start());

        QVERIFY(pipeline.wait());

        QCOMPARE(sink.values(), values);
    }

    void nonacceleratedPipeline()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        IOPipe joinOneTwo;

        PipelineOverride firstPipeline;
        auto target = firstPipeline.setInputExternal();
        QVERIFY(target != nullptr);
        firstPipeline.output = joinOneTwo.write;
        QVERIFY(firstPipeline.setOutputPipeline());

        PipelineOverride secondPipeline;
        secondPipeline.input = joinOneTwo.read;
        QVERIFY(secondPipeline.setInputPipeline());
        QVERIFY(secondPipeline.setOutputIngress(&sink));

        QVERIFY(firstPipeline.start());
        QVERIFY(secondPipeline.start());

        target->incomingData(values);
        target->endData();

        QVERIFY(firstPipeline.wait());
        QVERIFY(secondPipeline.wait());

        QCOMPARE(sink.values(), values);

        QVERIFY(!firstPipeline.interlock_acceleration_server_connection.wasReceived());
        QVERIFY(!secondPipeline.interlock_acceleration_client_connection.wasReceived());
    }

    void accelerationSimpleHandoff()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        IOPipe joinOneTwo;

        PipelineOverride firstPipeline(true);
        auto target = firstPipeline.setInputExternal();
        QVERIFY(target != nullptr);
        firstPipeline.output = joinOneTwo.write;
        QVERIFY(firstPipeline.setOutputPipeline());

        PipelineOverride secondPipeline(true);
        secondPipeline.input = joinOneTwo.read;
        QVERIFY(secondPipeline.setInputPipeline());
        QVERIFY(secondPipeline.setOutputIngress(&sink));

        secondPipeline.interlock_acceleration_client_detaching.blockFuture();
        secondPipeline.interlock_acceleration_client_detachCompleted.blockFuture();
        firstPipeline.interlock_acceleration_server_detaching.blockFuture();

        QVERIFY(firstPipeline.start());
        QVERIFY(secondPipeline.start());

        secondPipeline.interlock_acceleration_client_detaching.waitForAndUnblock();
        firstPipeline.interlock_acceleration_server_detaching.waitForAndUnblock();
        secondPipeline.interlock_acceleration_client_detachCompleted.waitForAndUnblock();

        target->incomingData(values);
        target->endData();

        QVERIFY(firstPipeline.wait());
        QVERIFY(secondPipeline.wait());

        QCOMPARE(sink.values(), values);

        QVERIFY(firstPipeline.interlock_acceleration_server_handoff.wasReceived());
        QVERIFY(secondPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
    }

    void accelerationDoubleHandoff()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        IOPipe joinOneTwo;
        IOPipe joinTwoThree;

        PipelineOverride firstPipeline(true);
        auto target = firstPipeline.setInputExternal();
        QVERIFY(target != nullptr);
        firstPipeline.output = joinOneTwo.write;
        QVERIFY(firstPipeline.setOutputPipeline());

        PipelineOverride secondPipeline(true);
        secondPipeline.input = joinOneTwo.read;
        secondPipeline.output = joinTwoThree.write;
        QVERIFY(secondPipeline.setInputPipeline());
        QVERIFY(secondPipeline.setOutputPipeline());

        PipelineOverride thirdPipeline(true);
        thirdPipeline.input = joinTwoThree.read;
        QVERIFY(thirdPipeline.setInputPipeline());
        QVERIFY(thirdPipeline.setOutputIngress(&sink));

        thirdPipeline.interlock_acceleration_client_detaching.blockFuture();
        thirdPipeline.interlock_acceleration_client_detachCompleted.blockFuture();
        secondPipeline.interlock_acceleration_server_detaching.blockFuture();

        QVERIFY(firstPipeline.start());
        QVERIFY(secondPipeline.start());
        QVERIFY(thirdPipeline.start());

        thirdPipeline.interlock_acceleration_client_detaching.waitForAndUnblock();
        secondPipeline.interlock_acceleration_server_detaching.waitForAndUnblock();
        thirdPipeline.interlock_acceleration_client_detachCompleted.waitForAndUnblock();

        firstPipeline.interlock_acceleration_server_detaching.blockUntilReceived();
        secondPipeline.interlock_acceleration_client_detachCompleted.blockUntilReceived();

        target->incomingData(values);
        target->endData();

        QVERIFY(firstPipeline.wait());
        QVERIFY(secondPipeline.wait());
        QVERIFY(thirdPipeline.wait());

        QCOMPARE(sink.values(), values);

        QVERIFY(firstPipeline.interlock_acceleration_server_handoff.wasReceived());
        QVERIFY(secondPipeline.interlock_acceleration_client_handoffComplete.wasReceived());

        QVERIFY(secondPipeline.interlock_acceleration_server_handoff.wasReceived());
        QVERIFY(thirdPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
    }

    void accelerationDataHandoff()
    {
        SequenceValue::Transfer values;
        StreamSink::Iterator sink;

        IOPipe joinOneTwo;

        PipelineOverride firstPipeline(true);
        auto target = firstPipeline.setInputExternal();
        QVERIFY(target != nullptr);
        firstPipeline.output = joinOneTwo.write;
        QVERIFY(firstPipeline.setOutputPipeline());

        PipelineOverride secondPipeline(true);
        secondPipeline.input = joinOneTwo.read;
        QVERIFY(secondPipeline.setInputPipeline());
        QVERIFY(secondPipeline.setOutputIngress(&sink));

        secondPipeline.interlock_acceleration_client_detaching.blockFuture();
        secondPipeline.interlock_acceleration_client_detachCompleted.blockFuture();
        firstPipeline.interlock_acceleration_server_detaching.blockFuture();

        QVERIFY(firstPipeline.start());
        QVERIFY(secondPipeline.start());

        secondPipeline.interlock_acceleration_client_detaching.waitFor();
        target->incomingData(
                SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0));
        values.emplace_back(
                SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0));
        QVERIFY(sink.hasNext());
        secondPipeline.interlock_acceleration_client_detaching.unblock();

        firstPipeline.interlock_acceleration_server_detaching.waitForAndUnblock();
        secondPipeline.interlock_acceleration_client_detachCompleted.waitForAndUnblock();

        target->incomingData(
                SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0));
        values.emplace_back(
                SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0));
        target->endData();

        QVERIFY(firstPipeline.wait());
        QVERIFY(secondPipeline.wait());

        QCOMPARE(sink.all(), values);

        QVERIFY(firstPipeline.interlock_acceleration_server_handoff.wasReceived());
        QVERIFY(secondPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
    }

    void accelerationChainHandoff()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        IOPipe joinOneTwo;

        PipelineOverride firstPipeline(true);
        auto target = firstPipeline.setInputExternal();
        QVERIFY(target != nullptr);
        firstPipeline.output = joinOneTwo.write;
        QVERIFY(firstPipeline.setOutputPipeline());
        auto stageOne = std::unique_ptr<CaptureStage>(new CaptureStage);
        auto receivedOne = stageOne->received;
        QVERIFY(firstPipeline.addProcessingStage(std::move(stageOne)));

        PipelineOverride secondPipeline(true);
        secondPipeline.input = joinOneTwo.read;
        QVERIFY(secondPipeline.setInputPipeline());
        QVERIFY(secondPipeline.setOutputIngress(&sink));
        auto stageTwo = std::unique_ptr<CaptureStage>(new CaptureStage);
        auto receivedTwo = stageTwo->received;
        QVERIFY(secondPipeline.addProcessingStage(std::move(stageTwo)));

        secondPipeline.interlock_acceleration_client_detaching.blockFuture();
        secondPipeline.interlock_acceleration_client_detachCompleted.blockFuture();
        firstPipeline.interlock_acceleration_server_detaching.blockFuture();

        QVERIFY(firstPipeline.start());
        QVERIFY(secondPipeline.start());

        secondPipeline.interlock_acceleration_client_detaching.waitForAndUnblock();
        firstPipeline.interlock_acceleration_server_detaching.waitForAndUnblock();
        secondPipeline.interlock_acceleration_client_detachCompleted.waitForAndUnblock();

        target->incomingData(values);
        target->endData();

        QVERIFY(firstPipeline.wait());
        QVERIFY(secondPipeline.wait());

        QCOMPARE(sink.values(), values);
        QVERIFY(receivedOne->ended);
        QCOMPARE(receivedOne->values, values);
        QVERIFY(receivedTwo->ended);
        QCOMPARE(receivedTwo->values, values);

        QVERIFY(firstPipeline.interlock_acceleration_server_handoff.wasReceived());
        QVERIFY(secondPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
    }

    void accelerationAbortHandoff()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        IOPipe joinOneTwo;

        PipelineOverride firstPipeline(true);
        auto target = firstPipeline.setInputExternal();
        QVERIFY(target != nullptr);
        firstPipeline.output = joinOneTwo.write;
        QVERIFY(firstPipeline.setOutputPipeline());

        PipelineOverride secondPipeline(true);
        secondPipeline.input = joinOneTwo.read;
        QVERIFY(secondPipeline.setInputPipeline());
        QVERIFY(secondPipeline.setOutputIngress(&sink));

        firstPipeline.interlock_acceleration_server_detaching.blockFuture();

        QVERIFY(firstPipeline.start());
        QVERIFY(secondPipeline.start());

        firstPipeline.interlock_acceleration_server_detaching.waitFor();
        target->incomingData(values);
        target->endData();
        firstPipeline.interlock_acceleration_server_detaching.unblock();

        QVERIFY(firstPipeline.wait());
        QVERIFY(secondPipeline.wait());

        QCOMPARE(sink.values(), values);

        QVERIFY(firstPipeline.interlock_acceleration_server_detachAborted.wasReceived());
        QVERIFY(!firstPipeline.interlock_acceleration_server_handoff.wasReceived());
        QVERIFY(!secondPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
    }

    void accelerationForward()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        IOPipe joinOneTwo;
        IOPipe joinTwoThree;

        PipelineOverride firstPipeline(true, false, true);
        auto target = firstPipeline.setInputExternal();
        QVERIFY(target != nullptr);
        firstPipeline.output = joinOneTwo.write;
        QVERIFY(firstPipeline.setOutputPipeline());

        PipelineOverride secondPipeline(true, false, true);
        secondPipeline.input = joinOneTwo.read;
        secondPipeline.output = joinTwoThree.write;
        QVERIFY(secondPipeline.setInputPipeline());
        QVERIFY(secondPipeline.setOutputPipeline());

        PipelineOverride thirdPipeline(true, false, true);
        thirdPipeline.input = joinTwoThree.read;
        QVERIFY(thirdPipeline.setInputPipeline());
        QVERIFY(thirdPipeline.setOutputIngress(&sink));

        firstPipeline.interlock_acceleration_server_handoff.blockFuture();
        secondPipeline.interlock_acceleration_client_handoffComplete.blockFuture();
        thirdPipeline.interlock_acceleration_client_connection.blockFuture();

        QVERIFY(firstPipeline.start());
        QVERIFY(secondPipeline.start());
        QVERIFY(thirdPipeline.start());

        firstPipeline.interlock_acceleration_server_handoff.waitForAndUnblock();
        secondPipeline.interlock_acceleration_client_handoffComplete.waitForAndUnblock();

        secondPipeline.interlock_acceleration_server_connection.blockFuture();
        secondPipeline.interlock_acceleration_server_forward.blockFuture();
        thirdPipeline.interlock_acceleration_client_forwarded.blockFuture();
        firstPipeline.interlock_acceleration_server_detaching.blockFuture();

        thirdPipeline.interlock_acceleration_client_connection.waitForAndUnblock();
        secondPipeline.interlock_acceleration_server_connection.waitForAndUnblock();
        secondPipeline.interlock_acceleration_server_forward.waitForAndUnblock();

        thirdPipeline.interlock_acceleration_client_forwarded.waitFor();
        thirdPipeline.interlock_acceleration_client_detaching.blockFuture();
        thirdPipeline.interlock_acceleration_client_detachCompleted.blockFuture();
        thirdPipeline.interlock_acceleration_client_forwarded.unblock();

        thirdPipeline.interlock_acceleration_client_detaching.waitForAndUnblock();
        firstPipeline.interlock_acceleration_server_detaching.waitForAndUnblock();
        thirdPipeline.interlock_acceleration_client_detachCompleted.waitForAndUnblock();

        target->incomingData(values);
        target->endData();

        QVERIFY(firstPipeline.wait());
        QVERIFY(secondPipeline.wait());
        QVERIFY(thirdPipeline.wait());

        QCOMPARE(sink.values(), values);

        QCOMPARE(firstPipeline.interlock_acceleration_server_handoff.countReceived(), 2);
        QVERIFY(secondPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
        QVERIFY(thirdPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
    }

    void accelerationDenyForward()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        IOPipe joinOneTwo;
        IOPipe joinTwoThree;

        PipelineOverride firstPipeline(true, false, true);
        auto target = firstPipeline.setInputExternal();
        QVERIFY(target != nullptr);
        firstPipeline.output = joinOneTwo.write;
        QVERIFY(firstPipeline.setOutputPipeline());

        PipelineOverride secondPipeline(true, false, false);
        secondPipeline.input = joinOneTwo.read;
        secondPipeline.output = joinTwoThree.write;
        QVERIFY(secondPipeline.setInputPipeline());
        QVERIFY(secondPipeline.setOutputPipeline());

        PipelineOverride thirdPipeline(true, false, true);
        thirdPipeline.input = joinTwoThree.read;
        QVERIFY(thirdPipeline.setInputPipeline());
        QVERIFY(thirdPipeline.setOutputIngress(&sink));

        firstPipeline.interlock_acceleration_server_handoff.blockFuture();
        secondPipeline.interlock_acceleration_client_handoffComplete.blockFuture();
        thirdPipeline.interlock_acceleration_client_connection.blockFuture();

        QVERIFY(firstPipeline.start());
        QVERIFY(secondPipeline.start());
        QVERIFY(thirdPipeline.start());

        firstPipeline.interlock_acceleration_server_handoff.waitForAndUnblock();
        secondPipeline.interlock_acceleration_client_handoffComplete.waitForAndUnblock();

        secondPipeline.interlock_acceleration_server_handoff.blockFuture();
        thirdPipeline.interlock_acceleration_client_handoffComplete.blockFuture();

        thirdPipeline.interlock_acceleration_client_connection.waitForAndUnblock();

        secondPipeline.interlock_acceleration_server_handoff.waitForAndUnblock();
        thirdPipeline.interlock_acceleration_client_handoffComplete.waitForAndUnblock();

        target->incomingData(values);
        target->endData();

        QVERIFY(firstPipeline.wait());
        QVERIFY(secondPipeline.wait());
        QVERIFY(thirdPipeline.wait());

        QCOMPARE(sink.values(), values);

        QCOMPARE(firstPipeline.interlock_acceleration_server_handoff.countReceived(), 1);
        QVERIFY(secondPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
        QVERIFY(secondPipeline.interlock_acceleration_server_handoff.wasReceived());
        QVERIFY(thirdPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
        QVERIFY(!thirdPipeline.interlock_acceleration_client_forwarded.wasReceived());
    }

    void accelerationForwardWhileEnding()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        IOPipe joinOneTwo;
        IOPipe joinTwoThree;

        PipelineOverride firstPipeline(true, false, true);
        auto target = firstPipeline.setInputExternal();
        QVERIFY(target != nullptr);
        firstPipeline.output = joinOneTwo.write;
        QVERIFY(firstPipeline.setOutputPipeline());

        PipelineOverride secondPipeline(true, false, true);
        secondPipeline.input = joinOneTwo.read;
        secondPipeline.output = joinTwoThree.write;
        QVERIFY(secondPipeline.setInputPipeline());
        QVERIFY(secondPipeline.setOutputPipeline());

        PipelineOverride thirdPipeline(true, false, true);
        thirdPipeline.input = joinTwoThree.read;
        QVERIFY(thirdPipeline.setInputPipeline());
        QVERIFY(thirdPipeline.setOutputIngress(&sink));

        secondPipeline.interlock_acceleration_client_handoffComplete.blockFuture();
        thirdPipeline.interlock_acceleration_client_connection.blockFuture();

        QVERIFY(firstPipeline.start());
        QVERIFY(secondPipeline.start());
        QVERIFY(thirdPipeline.start());

        secondPipeline.interlock_acceleration_client_handoffComplete.waitForAndUnblock();

        firstPipeline.interlock_acceleration_server_connection.blockFuture();
        thirdPipeline.interlock_acceleration_client_connection.waitForAndUnblock();
        firstPipeline.interlock_acceleration_server_connection.waitFor();
        firstPipeline.interlock_acceleration_server_detaching.blockFuture();
        firstPipeline.interlock_acceleration_server_connection.unblock();

        firstPipeline.interlock_acceleration_server_detaching.waitFor();
        target->incomingData(values);
        target->endData();
        firstPipeline.interlock_acceleration_server_detaching.unblock();

        QVERIFY(firstPipeline.wait());
        QVERIFY(secondPipeline.wait());
        QVERIFY(thirdPipeline.wait());

        QCOMPARE(sink.values(), values);

        QCOMPARE(firstPipeline.interlock_acceleration_server_handoff.countReceived(), 1);
        QVERIFY(firstPipeline.interlock_acceleration_server_detachAborted.wasReceived());
        QVERIFY(secondPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
        QVERIFY(!thirdPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
    }

    void accelerationSimpleSerialize()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        IOPipe joinOneTwo;

        PipelineOverride firstPipeline(true, true);
        auto target = firstPipeline.setInputExternal();
        QVERIFY(target != nullptr);
        firstPipeline.output = joinOneTwo.write;
        QVERIFY(firstPipeline.setOutputPipeline());

        PipelineOverride secondPipeline(true, true);
        secondPipeline.input = joinOneTwo.read;
        QVERIFY(secondPipeline.setInputPipeline());
        QVERIFY(secondPipeline.setOutputIngress(&sink));
        auto stageOne = std::unique_ptr<CaptureStage>(new CaptureStage);
        auto receivedOne = stageOne->received;
        auto serialOne = firstPipeline.deserializeLookup
                                      .emplace("serial1", std::unique_ptr<CaptureStage::Component>(
                                              new CaptureStage::Component("serial1", receivedOne)))
                                      .first;
        QVERIFY(secondPipeline.addProcessingStage(std::move(stageOne), serialOne->second.get()));

        secondPipeline.interlock_acceleration_client_detaching.blockFuture();
        secondPipeline.interlock_acceleration_client_detachCompleted.blockFuture();
        firstPipeline.interlock_acceleration_server_detaching.blockFuture();

        secondPipeline.interlock_acceleration_client_sendStart.blockFuture();
        firstPipeline.interlock_acceleration_server_incomingSend.blockFuture();
        firstPipeline.interlock_acceleration_deserialization.blockFuture();
        secondPipeline.interlock_acceleration_client_sendCompleted.blockFuture();
        secondPipeline.interlock_acceleration_client_handoffStart.blockFuture();

        QVERIFY(firstPipeline.start());
        QVERIFY(secondPipeline.start());

        secondPipeline.interlock_acceleration_client_detaching.waitForAndUnblock();
        firstPipeline.interlock_acceleration_server_detaching.waitForAndUnblock();
        secondPipeline.interlock_acceleration_client_detachCompleted.waitForAndUnblock();

        secondPipeline.interlock_acceleration_client_sendStart.waitForAndUnblock();
        firstPipeline.interlock_acceleration_server_incomingSend.waitForAndUnblock();
        firstPipeline.interlock_acceleration_deserialization.waitForAndUnblock();
        secondPipeline.interlock_acceleration_client_sendCompleted.waitForAndUnblock();

        secondPipeline.interlock_acceleration_client_handoffStart.waitForAndUnblock();

        target->incomingData(values);
        target->endData();

        QVERIFY(firstPipeline.wait());
        QVERIFY(secondPipeline.wait());

        QCOMPARE(sink.values(), values);
        QVERIFY(receivedOne->ended);
        QCOMPARE(receivedOne->values, values);

        QVERIFY(firstPipeline.interlock_acceleration_server_handoff.wasReceived());
        QVERIFY(secondPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
    }

    void accelerationMultipleSerialize()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        IOPipe joinOneTwo;

        PipelineOverride firstPipeline(true, true);
        auto target = firstPipeline.setInputExternal();
        QVERIFY(target != nullptr);
        firstPipeline.output = joinOneTwo.write;
        QVERIFY(firstPipeline.setOutputPipeline());

        PipelineOverride secondPipeline(true, true);
        secondPipeline.input = joinOneTwo.read;
        QVERIFY(secondPipeline.setInputPipeline());
        QVERIFY(secondPipeline.setOutputIngress(&sink));
        auto stageOne = std::unique_ptr<CaptureStage>(new CaptureStage);
        auto receivedOne = stageOne->received;
        auto serialOne = firstPipeline.deserializeLookup
                                      .emplace("serial1", std::unique_ptr<CaptureStage::Component>(
                                              new CaptureStage::Component("serial1", receivedOne)))
                                      .first;
        QVERIFY(secondPipeline.addProcessingStage(std::move(stageOne), serialOne->second.get()));
        auto stageTwo = std::unique_ptr<CaptureStage>(new CaptureStage);
        auto receivedTwo = stageTwo->received;
        auto serialTwo = firstPipeline.deserializeLookup
                                      .emplace("serial2", std::unique_ptr<CaptureStage::Component>(
                                              new CaptureStage::Component("serial2", receivedTwo)))
                                      .first;
        QVERIFY(secondPipeline.addProcessingStage(std::move(stageTwo), serialTwo->second.get()));

        secondPipeline.interlock_acceleration_client_detaching.blockFuture();
        secondPipeline.interlock_acceleration_client_detachCompleted.blockFuture();
        firstPipeline.interlock_acceleration_server_detaching.blockFuture();

        secondPipeline.interlock_acceleration_client_sendStart.blockFuture();
        firstPipeline.interlock_acceleration_server_incomingSend.blockFuture();
        firstPipeline.interlock_acceleration_deserialization.blockFuture();
        secondPipeline.interlock_acceleration_client_sendCompleted.blockFuture();
        secondPipeline.interlock_acceleration_client_handoffStart.blockFuture();

        QVERIFY(firstPipeline.start());
        QVERIFY(secondPipeline.start());

        secondPipeline.interlock_acceleration_client_detaching.waitForAndUnblock();
        firstPipeline.interlock_acceleration_server_detaching.waitForAndUnblock();
        secondPipeline.interlock_acceleration_client_detachCompleted.waitForAndUnblock();

        secondPipeline.interlock_acceleration_client_sendStart.waitForAndUnblock();
        firstPipeline.interlock_acceleration_server_incomingSend.waitForAndUnblock();
        firstPipeline.interlock_acceleration_deserialization.waitForAndUnblock();
        secondPipeline.interlock_acceleration_client_sendCompleted.waitForAndUnblock();

        secondPipeline.interlock_acceleration_client_handoffStart.waitForAndUnblock();

        target->incomingData(values);
        target->endData();

        QVERIFY(firstPipeline.wait());
        QVERIFY(secondPipeline.wait());

        QCOMPARE(sink.values(), values);
        QVERIFY(receivedOne->ended);
        QCOMPARE(receivedOne->values, values);
        QVERIFY(receivedTwo->ended);
        QCOMPARE(receivedTwo->values, values);

        QVERIFY(firstPipeline.interlock_acceleration_server_handoff.wasReceived());
        QVERIFY(secondPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
        QCOMPARE(firstPipeline.interlock_acceleration_deserialization.countReceived(), 2);
        QCOMPARE(secondPipeline.interlock_acceleration_client_sendCompleted.countReceived(), 2);
    }

    void accelerationForwardSerialize()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        IOPipe joinOneTwo;
        IOPipe joinTwoThree;

        PipelineOverride firstPipeline(true, true, true);
        auto target = firstPipeline.setInputExternal();
        QVERIFY(target != nullptr);
        firstPipeline.output = joinOneTwo.write;
        QVERIFY(firstPipeline.setOutputPipeline());

        PipelineOverride secondPipeline(true, true, true);
        secondPipeline.input = joinOneTwo.read;
        secondPipeline.output = joinTwoThree.write;
        QVERIFY(secondPipeline.setInputPipeline());
        QVERIFY(secondPipeline.setOutputPipeline());

        PipelineOverride thirdPipeline(true, true, true);
        thirdPipeline.input = joinTwoThree.read;
        QVERIFY(thirdPipeline.setInputPipeline());
        QVERIFY(thirdPipeline.setOutputIngress(&sink));
        auto stageOne = std::unique_ptr<CaptureStage>(new CaptureStage);
        auto receivedOne = stageOne->received;
        auto serialOne = firstPipeline.deserializeLookup
                                      .emplace("serial1", std::unique_ptr<CaptureStage::Component>(
                                              new CaptureStage::Component("serial1", receivedOne)))
                                      .first;
        QVERIFY(thirdPipeline.addProcessingStage(std::move(stageOne), serialOne->second.get()));

        secondPipeline.interlock_acceleration_client_handoffComplete.blockFuture();
        thirdPipeline.interlock_acceleration_client_handoffComplete.blockFuture();

        QVERIFY(firstPipeline.start());
        QVERIFY(secondPipeline.start());
        QVERIFY(thirdPipeline.start());

        secondPipeline.interlock_acceleration_client_handoffComplete.waitForAndUnblock();
        thirdPipeline.interlock_acceleration_client_handoffComplete.waitForAndUnblock();

        target->incomingData(values);
        target->endData();

        QVERIFY(firstPipeline.wait());
        QVERIFY(secondPipeline.wait());
        QVERIFY(thirdPipeline.wait());

        QCOMPARE(sink.values(), values);
        QVERIFY(receivedOne->ended);
        QCOMPARE(receivedOne->values, values);

        QVERIFY(firstPipeline.interlock_acceleration_server_handoff.wasReceived());
        QVERIFY(secondPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
        QVERIFY(thirdPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
        QVERIFY(firstPipeline.interlock_acceleration_deserialization.wasReceived());
        QVERIFY(!secondPipeline.interlock_acceleration_deserialization.wasReceived());
    }

    void accelerationDenySerialize()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        IOPipe joinOneTwo;

        PipelineOverride firstPipeline(true, false);
        auto target = firstPipeline.setInputExternal();
        QVERIFY(target != nullptr);
        firstPipeline.output = joinOneTwo.write;
        QVERIFY(firstPipeline.setOutputPipeline());

        PipelineOverride secondPipeline(true, true);
        secondPipeline.input = joinOneTwo.read;
        QVERIFY(secondPipeline.setInputPipeline());
        QVERIFY(secondPipeline.setOutputIngress(&sink));
        auto stageOne = std::unique_ptr<CaptureStage>(new CaptureStage);
        auto receivedOne = stageOne->received;
        auto serialOne = firstPipeline.deserializeLookup
                                      .emplace("serial1", std::unique_ptr<CaptureStage::Component>(
                                              new CaptureStage::Component("serial1", receivedOne)))
                                      .first;
        QVERIFY(secondPipeline.addProcessingStage(std::move(stageOne), serialOne->second.get()));

        secondPipeline.interlock_acceleration_client_detaching.blockFuture();
        secondPipeline.interlock_acceleration_client_detachCompleted.blockFuture();
        firstPipeline.interlock_acceleration_server_detaching.blockFuture();

        secondPipeline.interlock_acceleration_client_sendStart.blockFuture();
        firstPipeline.interlock_acceleration_server_incomingSend.blockFuture();
        secondPipeline.interlock_acceleration_client_sendDenied.blockFuture();

        QVERIFY(firstPipeline.start());
        QVERIFY(secondPipeline.start());

        secondPipeline.interlock_acceleration_client_detaching.waitForAndUnblock();
        firstPipeline.interlock_acceleration_server_detaching.waitForAndUnblock();
        secondPipeline.interlock_acceleration_client_detachCompleted.waitForAndUnblock();

        secondPipeline.interlock_acceleration_client_sendStart.waitForAndUnblock();
        firstPipeline.interlock_acceleration_server_incomingSend.waitForAndUnblock();
        secondPipeline.interlock_acceleration_client_sendDenied.waitForAndUnblock();

        target->incomingData(values);
        target->endData();

        QVERIFY(firstPipeline.wait());
        QVERIFY(secondPipeline.wait());

        QCOMPARE(sink.values(), values);
        QVERIFY(receivedOne->ended);
        QCOMPARE(receivedOne->values, values);

        QVERIFY(firstPipeline.interlock_acceleration_server_handoff.wasReceived());
        QVERIFY(secondPipeline.interlock_acceleration_client_handoffComplete.wasReceived());
        QVERIFY(!firstPipeline.interlock_acceleration_deserialization.wasReceived());
    }

    void accelerationReserialize()
    {
        SequenceValue::Transfer values
                {SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(2.0), 200.0, 300.0),
                 SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(3.0), 300.0, 400.0),};
        StreamSink::Buffer sink;

        IOPipe joinOneTwo;
        IOPipe joinTwoThree;

        PipelineOverride firstPipeline(true, true);
        auto target = firstPipeline.setInputExternal();
        QVERIFY(target != nullptr);
        firstPipeline.output = joinOneTwo.write;
        QVERIFY(firstPipeline.setOutputPipeline());
        auto stageOne = std::unique_ptr<CaptureStage>(new CaptureStage);
        auto receivedOne = stageOne->received;
        QVERIFY(firstPipeline.addProcessingStage(std::move(stageOne)));

        PipelineOverride secondPipeline(true, true);
        secondPipeline.input = joinOneTwo.read;
        secondPipeline.output = joinTwoThree.write;
        QVERIFY(secondPipeline.setInputPipeline());
        QVERIFY(secondPipeline.setOutputPipeline());
        auto stageTwo = std::unique_ptr<CaptureStage>(new CaptureStage);
        auto receivedTwo = stageTwo->received;
        auto serialTwo = firstPipeline.deserializeLookup
                                      .emplace("serial2", std::unique_ptr<CaptureStage::Component>(
                                              new CaptureStage::Component("serial2", receivedTwo)))
                                      .first;
        QVERIFY(secondPipeline.addProcessingStage(std::move(stageTwo), serialTwo->second.get()));

        PipelineOverride thirdPipeline(true, true);
        thirdPipeline.input = joinTwoThree.read;
        QVERIFY(thirdPipeline.setInputPipeline());
        QVERIFY(thirdPipeline.setOutputIngress(&sink));
        auto stageThree = std::unique_ptr<CaptureStage>(new CaptureStage);
        auto receivedThree = stageThree->received;
        auto serialThree = secondPipeline.deserializeLookup
                                         .emplace("serial3",
                                                  std::unique_ptr<CaptureStage::Component>(
                                                          new CaptureStage::Component("serial3",
                                                                                      receivedThree)))
                                         .first;
        firstPipeline.deserializeLookup
                     .emplace("serial3", std::unique_ptr<CaptureStage::Component>(
                             new CaptureStage::Component("serial3", receivedThree)));
        QVERIFY(thirdPipeline.addProcessingStage(std::move(stageThree), serialThree->second.get()));

        thirdPipeline.interlock_acceleration_client_handoffComplete.blockFuture();

        QVERIFY(secondPipeline.start());
        QVERIFY(thirdPipeline.start());

        thirdPipeline.interlock_acceleration_client_handoffComplete.waitFor();
        secondPipeline.interlock_acceleration_client_handoffComplete.blockFuture();
        thirdPipeline.interlock_acceleration_client_handoffComplete.unblock();

        QVERIFY(firstPipeline.start());

        secondPipeline.interlock_acceleration_client_handoffComplete.waitForAndUnblock();

        target->incomingData(values);
        target->endData();

        QVERIFY(firstPipeline.wait());
        QVERIFY(secondPipeline.wait());
        QVERIFY(thirdPipeline.wait());

        QCOMPARE(sink.values(), values);
        QVERIFY(receivedOne->ended);
        QCOMPARE(receivedOne->values, values);
        QVERIFY(receivedOne->ended);
        QCOMPARE(receivedOne->values, values);
        QVERIFY(receivedThree->ended);
        QCOMPARE(receivedThree->values, values);

        QVERIFY(secondPipeline.interlock_acceleration_deserialization.wasReceived());
        QCOMPARE(firstPipeline.interlock_acceleration_deserialization.countReceived(), 2);
    }
};

QTEST_MAIN(TestStreamPipeline)

#include "streampipeline.moc"
