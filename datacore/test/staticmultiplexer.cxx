/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <QTest>
#include <QByteArray>

#include "datacore/staticmultiplexer.hxx"
#include "datacore/stream.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"

using namespace CPD3;
using namespace CPD3::Data;

static bool compareLists(const SequenceValue::Transfer &a, const SequenceValue::Transfer &b)
{
    if (a.size() != b.size())
        return false;

    for (std::size_t i = 0; i < a.size();) {
        std::size_t end = i + 1;
        for (; end < a.size() && FP::equal(a.at(end).getStart(), a.at(i).getStart()); ++end) { }

        std::size_t start = i;
        for (; i < end; i++) {
            bool hit = false;
            for (std::size_t j = start; j < end; j++) {
                if (SequenceValue::ValueEqual()(a.at(i), b.at(j))) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                return false;
        }

    }
    return true;
}

static bool sortResultList(const SequenceValue &a, const SequenceValue &b)
{ return Range::compareStart(a.getStart(), b.getStart()) < 0; }

class TestStaticMultiplexer : public QObject {
Q_OBJECT

    static SequenceValue::Transfer slice(const SequenceValue::Transfer &input,
                                         std::size_t begin,
                                         std::size_t count)
    {
        SequenceValue::Transfer result;
        SequenceValue::Transfer::const_iterator end;

        if (begin + count >= input.size())
            end = input.cend();
        else
            end = input.cbegin() + begin + count;

        std::copy(input.cbegin() + begin, end, Util::back_emplacer(result));
        return result;
    }

private slots:

    void simple()
    {
        SequenceValue::Transfer a;
        SequenceValue::Transfer b;
        SequenceValue::Transfer c;

        a.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.01), FP::undefined(),
                       2.0);
        a.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.02), FP::undefined(),
                       3.0);
        a.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.03), 1.0, 2.0);

        b.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.11), FP::undefined(),
                       2.0);
        b.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.12), 1.0, 3.0);
        b.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.13), 2.0, 3.0);

        c.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.21), 1.0, 2.0);
        c.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.22), 2.0, 3.0);
        c.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.23), 3.0, 4.0);

        for (std::size_t sA = 0; sA < a.size(); sA++) {
            for (std::size_t eA = sA; eA <= a.size(); eA++) {
                if (sA != 0 && eA == sA)
                    continue;
                SequenceValue::Transfer selectedA = slice(a, sA, (a.size() - sA) - eA);

                for (std::size_t sB = 0; sB < b.size(); sB++) {
                    for (std::size_t eB = sB; eB <= b.size(); eB++) {
                        if (sB != 0 && eB == sB)
                            continue;
                        SequenceValue::Transfer selectedB = slice(b, sB, (b.size() - sB) - eB);

                        for (std::size_t sC = 0; sC < c.size(); sC++) {
                            for (std::size_t eC = sC; eC <= c.size(); eC++) {
                                if (sC != 0 && eC == sC)
                                    continue;
                                SequenceValue::Transfer
                                        selectedC = slice(c, sC, (c.size() - sC) - eC);

                                SequenceValue::Transfer expected;
                                Util::append(selectedA, expected);
                                Util::append(selectedB, expected);
                                Util::append(selectedC, expected);
                                std::sort(expected.begin(), expected.end(), sortResultList);

                                StaticMultiplexer mux(3);
                                SequenceValue::Transfer result;

                                QVERIFY(mux.incoming(0, selectedA, false).empty());
                                QVERIFY(mux.incoming(1, selectedB, false).empty());
                                Util::append(mux.incoming(2, selectedC), result);
                                Util::append(mux.finish(), result);
                                QVERIFY(compareLists(expected, result));

                                mux.clear(true);
                                result.clear();
                                QVERIFY(mux.incoming(2, selectedC, false).empty());
                                QVERIFY(mux.incoming(0, selectedA, false).empty());
                                Util::append(mux.incoming(1, selectedB), result);
                                Util::append(mux.finish(), result);
                                QVERIFY(compareLists(expected, result));

                                mux.clear(true);
                                result.clear();
                                QVERIFY(mux.incoming(0, selectedA, false).empty());
                                QVERIFY(mux.finish(0, false).empty());
                                QVERIFY(mux.incoming(1, selectedB, false).empty());
                                QVERIFY(mux.finish(1, false).empty());
                                Util::append(mux.incoming(2, selectedC), result);
                                Util::append(mux.finish(2), result);
                                Util::append(mux.finish(), result);
                                QVERIFY(compareLists(expected, result));

                                mux.clear(true);
                                result.clear();
                                for (const auto &add : selectedB) {
                                    QVERIFY(mux.incoming(1, add, false).empty());
                                }
                                for (const auto &add : selectedC) {
                                    QVERIFY(mux.incoming(2, add, false).empty());
                                }
                                for (const auto &add : selectedA) {
                                    Util::append(mux.incoming(0, add), result);
                                }
                                Util::append(mux.finish(), result);
                                QVERIFY(compareLists(expected, result));

                                expected.clear();
                                Util::append(selectedA, expected);
                                Util::append(selectedB, expected);
                                std::sort(expected.begin(), expected.end(), sortResultList);

                                mux.clear(true);
                                result.clear();
                                QVERIFY(mux.incoming(0, selectedA, false).empty());
                                QVERIFY(mux.incoming(1, selectedB, false).empty());
                                for (const auto &add : selectedC) {
                                    Util::append(mux.advance(2, add.getStart()), result);
                                }
                                Util::append(mux.finish(), result);
                                QVERIFY(compareLists(expected, result));

                                mux.clear(true);
                                result.clear();
                                QVERIFY(mux.incoming(0, selectedA, false).empty());
                                QVERIFY(mux.incoming(1, selectedB, false).empty());
                                Util::append(mux.finish(2), result);
                                Util::append(mux.finish(1), result);
                                Util::append(mux.finish(0), result);
                                Util::append(mux.finish(), result);
                                QVERIFY(compareLists(expected, result));
                            }
                        }

                        SequenceValue::Transfer expected;
                        Util::append(selectedA, expected);
                        Util::append(selectedB, expected);
                        std::sort(expected.begin(), expected.end(), sortResultList);

                        StaticMultiplexer mux(2);
                        SequenceValue::Transfer result;

                        QVERIFY(mux.incoming(0, selectedA, false).empty());
                        Util::append(mux.incoming(1, selectedB), result);
                        Util::append(mux.finish(), result);
                        QVERIFY(compareLists(expected, result));
                    }
                }

                StaticMultiplexer mux(1);
                SequenceValue::Transfer result(mux.incoming(0, selectedA));
                Util::append(mux.finish(), result);
                QVERIFY(compareLists(selectedA, result));
            }
        }
    }

    void normalUsage()
    {

        StaticMultiplexer mux(3);

        SequenceValue meta
                (SequenceName("bnd", "raw_meta", "BsG_S11"), Variant::Root(0.01), FP::undefined(),
                 FP::undefined());
        QVERIFY(mux.incoming(2, meta, false).empty());

        SequenceValue flags
                (SequenceName("bnd", "raw", "F1_S11"), Variant::Root(0.11), 2.0, FP::undefined());
        QVERIFY(mux.incoming(1, flags, false).empty());

        SequenceValue v1(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.0), 10.0, 20.0);
        QVERIFY(mux.incoming(0, v1, false).empty());

        SequenceValue::Transfer expected;
        expected.push_back(meta);
        expected.push_back(flags);
        expected.push_back(v1);
        SequenceValue::Transfer result;
        Util::append(mux.advance(2, 10.0), result);
        Util::append(mux.advance(1, 10.0), result);
        QVERIFY(compareLists(expected, result));

        flags = SequenceValue(SequenceName("bnd", "raw", "F1_S11"), Variant::Root(0.12), 10.0,
                              FP::undefined());
        expected.push_back(flags);
        Util::append(mux.incoming(1, flags), result);
        QVERIFY(compareLists(expected, result));

        SequenceValue v2(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 20.0, 30.0);
        QVERIFY(mux.incoming(0, v2).empty());
        expected.push_back(v2);
        Util::append(mux.advance(1, 20.0), result);
        Util::append(mux.advance(2, 20.0), result);
        QVERIFY(compareLists(expected, result));

        flags = SequenceValue(SequenceName("bnd", "raw", "F1_S11"), Variant::Root(0.12), 25.0,
                              FP::undefined());
        expected.push_back(flags);
        QVERIFY(mux.incoming(1, flags).empty());
        QVERIFY(mux.advance(1, 25.0).empty());
        QVERIFY(mux.advance(2, 25.0).empty());

        SequenceValue v3(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(3.0), 30.0, 40.0);
        Util::append(mux.incoming(0, v3), result);
        QVERIFY(compareLists(expected, result));

        expected.push_back(v3);
        Util::append(mux.finish(), result);
        QVERIFY(compareLists(expected, result));
    }

    void identicalTimeShortCircuit()
    {
        StaticMultiplexer mux(3);

        SequenceValue::Transfer result;

        SequenceValue v1(SequenceName("bnd", "raw", "T1"), Variant::Root(1.0), 1.0, 2.0);
        Util::append(mux.incoming(0, v1), result);
        SequenceValue v2(SequenceName("bnd", "raw", "T2"), Variant::Root(1.1), 1.0, 2.0);
        Util::append(mux.incoming(1, v2), result);
        SequenceValue v3(SequenceName("bnd", "raw", "T3"), Variant::Root(1.2), 1.0, 2.0);
        Util::append(mux.incoming(2, v3), result);

        SequenceValue v4(SequenceName("bnd", "raw", "T1"), Variant::Root(1.3), 5.0, 6.0);
        Util::append(mux.incoming(0, v4), result);

        SequenceValue v5(SequenceName("bnd", "raw", "T2"), Variant::Root(1.4), 1.5, 2.5);
        Util::append(mux.incoming(1, v5), result);

        SequenceValue v6(SequenceName("bnd", "raw", "T3"), Variant::Root(1.5), 1.4, 2.4);
        Util::append(mux.incoming(2, v6), result);

        SequenceValue::Transfer expected;
        Util::append(mux.finish(), result);
        expected.push_back(v1);
        expected.push_back(v2);
        expected.push_back(v3);
        expected.push_back(v6);
        expected.push_back(v5);
        expected.push_back(v4);
        QVERIFY(compareLists(expected, result));
    }

    void streamCrossingShortCircuit()
    {
        StaticMultiplexer mux(2);

        SequenceValue::Transfer result;

        SequenceValue v1(SequenceName("bnd", "raw", "T1"), Variant::Root(1.0), 1.0, 2.0);
        Util::append(mux.incoming(0, v1), result);
        SequenceValue v2(SequenceName("bnd", "raw", "T1"), Variant::Root(1.1), 2.0, 3.0);
        Util::append(mux.incoming(0, v2), result);
        SequenceValue v3(SequenceName("bnd", "raw", "T1"), Variant::Root(1.2), 3.0, 4.0);
        Util::append(mux.incoming(0, v3), result);
        SequenceValue v4(SequenceName("bnd", "raw", "T1"), Variant::Root(1.3), 4.0, 5.0);
        Util::append(mux.incoming(0, v4), result);

        Util::append(mux.advance(1, 1.1), result);

        SequenceValue v5(SequenceName("bnd", "raw", "T2"), Variant::Root(2.0), 1.5, 2.0);
        Util::append(mux.incoming(1, v5), result);

        SequenceValue v6(SequenceName("bnd", "raw", "T2"), Variant::Root(2.1), 1.6, 2.0);
        Util::append(mux.incoming(1, v6), result);

        SequenceValue v7(SequenceName("bnd", "raw", "T2"), Variant::Root(2.3), 4.0, 5.0);
        Util::append(mux.incoming(1, v7), result);

        SequenceValue::Transfer expected;
        Util::append(mux.finish(), result);

        expected.push_back(v1);
        expected.push_back(v5);
        expected.push_back(v6);
        expected.push_back(v2);
        expected.push_back(v3);
        expected.push_back(v4);
        expected.push_back(v7);
        QVERIFY(compareLists(expected, result));
    }

    void advanceOrderChange()
    {
        StaticMultiplexer mux(3);

        SequenceValue::Transfer result;

        SequenceValue v1(SequenceName("bnd", "raw", "T1"), Variant::Root(1.0), 1.0, 2.0);
        Util::append(mux.incoming(0, v1), result);
        Util::append(mux.advance(0, 2.0), result);

        SequenceValue v2(SequenceName("bnd", "raw", "T1"), Variant::Root(1.1), 1.5, 2.5);
        Util::append(mux.incoming(1, v2), result);
        Util::append(mux.advance(1, 2.5), result);

        SequenceValue v3(SequenceName("bnd", "raw", "T1"), Variant::Root(1.2), 1.6, 2.6);
        Util::append(mux.incoming(2, v3), result);
        Util::append(mux.advance(2, 2.6), result);

        SequenceValue v5(SequenceName("bnd", "raw", "T1"), Variant::Root(2.1), 2.5, 3.5);
        Util::append(mux.incoming(1, v5), result);
        Util::append(mux.advance(1, 3.5), result);

        SequenceValue v6(SequenceName("bnd", "raw", "T1"), Variant::Root(2.2), 2.6, 3.6);
        Util::append(mux.incoming(2, v6), result);
        Util::append(mux.advance(2, 3.6), result);

        SequenceValue v4(SequenceName("bnd", "raw", "T1"), Variant::Root(2.0), 2.0, 3.0);
        Util::append(mux.incoming(0, v4), result);
        Util::append(mux.advance(0, 3.0), result);

        SequenceValue::Transfer expected;
        Util::append(mux.finish(), result);

        expected.push_back(v1);
        expected.push_back(v2);
        expected.push_back(v3);
        expected.push_back(v4);
        expected.push_back(v5);
        expected.push_back(v6);
        QVERIFY(compareLists(expected, result));
    }

    void serialize()
    {
        StaticMultiplexer mux(2);

        SequenceValue::Transfer values;
        values.emplace_back(SequenceName("bnd", "raw_meta", "BsG_S11"), Variant::Root(1.0),
                            FP::undefined(),
                            FP::undefined());
        values.emplace_back(SequenceName("bnd", "raw_meta", "BsB_S11"), Variant::Root(1.1),
                            FP::undefined(),
                            FP::undefined());
        values.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(2.0), 100, 200);
        values.emplace_back(SequenceName("bnd", "raw", "BsB_S11"), Variant::Root(2.1), 100, 200);
        values.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(3.0), 200, 300);
        values.emplace_back(SequenceName("bnd", "raw", "BsB_S11"), Variant::Root(3.1), 200, 300);

        for (std::size_t i = 0; i < values.size(); i++) {
            StaticMultiplexer ds;
            {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << mux;
                }
                {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    stream >> ds;
                }
            }
            SequenceValue::Transfer expected(mux.incoming(i % 2, values.at(i)));
            SequenceValue::Transfer result(ds.incoming(i % 2, values.at(i)));

            QVERIFY(compareLists(expected, result));
        }
    }
};

QTEST_MAIN(TestStaticMultiplexer)

#include "staticmultiplexer.moc"
