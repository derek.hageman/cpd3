/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtSql>
#include <QString>

#include "datacore/sequencefilter.hxx"

using namespace CPD3;
using namespace CPD3::Data;


class TestSequenceFilter : public QObject {
Q_OBJECT

    static bool listsEqual(const Archive::Selection::Match &a, const Archive::Selection::Match &b)
    {
        return std::unordered_set<std::string>(a.begin(), a.end()) ==
                std::unordered_set<std::string>(b.begin(), b.end());
    }

    static bool containsSelection(const Archive::Selection::List &as,
                                  const Archive::Selection &search)
    {
        for (const auto &check : as) {
            if (!FP::equal(check.start, search.start))
                continue;
            if (!FP::equal(check.end, search.end))
                continue;
            if (!listsEqual(check.stations, search.stations))
                continue;
            if (!listsEqual(check.archives, search.archives))
                continue;
            if (!listsEqual(check.variables, search.variables))
                continue;
            if (!listsEqual(check.hasFlavors, search.hasFlavors))
                continue;
            if (!listsEqual(check.lacksFlavors, search.lacksFlavors))
                continue;
            if (!listsEqual(check.exactFlavors, search.exactFlavors))
                continue;
            return true;
        }
        return false;
    }

private slots:

    void basic()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;

        cv.write().array(0).setString("T_S11");
        cv.write().array(1).hash("Variable").setString("P_S11");
        cv.write().array(1).hash("Accept").setBool(false);
        cv.write().array(2).hash("Variable").setString("P_S11");
        cv.write().array(2).hash("Station").setString("bnd");
        cv.write().array(2).hash("Priority").setInt64(0);
        config.emplace_back(FP::undefined(), 100, cv);

        cv.write().setEmpty();
        cv.write().array(0).setString("T_S11");
        cv.write().array(1).hash("Variable").setString("P_S11");
        cv.write().array(1).hash("Accept").setBool(true);
        cv.write().array(2).hash("Variable").setString("P_S11");
        cv.write().array(2).hash("Station").setString("bnd");
        cv.write().array(2).hash("Accept").setBool(false);
        cv.write().array(2).hash("Priority").setInt64(0);
        config.emplace_back(100, 200, cv);

        cv.write().setEmpty();
        cv.write().array(0).setString("T_S11");
        cv.write().array(1).hash("Variable").setString("P_S11");
        cv.write().array(1).hash("Accept").setBool(false);
        cv.write().array(2).hash("Variable").setString("P_S11");
        cv.write().array(2).hash("Station").setString("bnd");
        cv.write().array(2).hash("Priority").setInt64(0);
        config.emplace_back(200, FP::undefined(), cv);

        SequenceFilter f;
        f.configure(config);

        Archive::Selection::List as(f.toArchiveSelection({}, {"raw"}));
        QVERIFY(containsSelection(as, Archive::Selection(FP::undefined(), 100.0, {}, {"raw"},
                                                         {"T_S11"})));
        QVERIFY(containsSelection(as, Archive::Selection(100.0, 200.0, {}, {"raw"}, {"T_S11"})));
        QVERIFY(containsSelection(as, Archive::Selection(200.0, FP::undefined(), {}, {"raw"},
                                                         {"T_S11"})));
        QVERIFY(containsSelection(as, Archive::Selection(FP::undefined(), 100.0, {"bnd"}, {"raw"},
                                                         {"P_S11"})));
        QVERIFY(containsSelection(as, Archive::Selection(100.0, 200.0, {}, {"raw"}, {"P_S11"})));
        QVERIFY(containsSelection(as, Archive::Selection(200.0, FP::undefined(), {"bnd"}, {"raw"},
                                                         {"P_S11"})));

        QVERIFY(f.accept(SequenceName("bnd", "raw", "T_S11"), 50, 60));
        QVERIFY(!f.accept(SequenceName("alt", "raw", "P_S11"), 50, 60));
        QVERIFY(f.accept(SequenceName("bnd", "raw", "P_S11"), 50, 60));

        QVERIFY(f.accept(SequenceName("bnd", "raw", "T_S11"), 150, 160));
        QVERIFY(f.accept(SequenceName("alt", "raw", "P_S11"), 150, 160));
        QVERIFY(!f.accept(SequenceName("bnd", "raw", "P_S11"), 150, 160));

        QVERIFY(f.accept(SequenceName("bnd", "raw", "T_S11"), 250, 260));
        QVERIFY(!f.accept(SequenceName("alt", "raw", "P_S11"), 250, 260));
        QVERIFY(f.accept(SequenceName("bnd", "raw", "P_S11"), 250, 260));

        QVERIFY(f.accept(SequenceName("bnd", "raw", "T_S11"), 50, 260));


        QVERIFY(f.accept(SequenceName("bnd", "raw", "T_S11"), 50, 60));
        QVERIFY(!f.accept(SequenceName("alt", "raw", "P_S11"), 50, 60));
        QVERIFY(f.accept(SequenceName("bnd", "raw", "P_S11"), 50, 60));

        QVERIFY(f.accept(SequenceName("bnd", "raw", "T_S11"), 150, 160));
        QVERIFY(f.accept(SequenceName("alt", "raw", "P_S11"), 150, 160));
        QVERIFY(!f.accept(SequenceName("bnd", "raw", "P_S11"), 150, 160));

        QVERIFY(f.accept(SequenceName("bnd", "raw", "T_S11"), 250, 260));
        QVERIFY(!f.accept(SequenceName("alt", "raw", "P_S11"), 250, 260));
        QVERIFY(f.accept(SequenceName("bnd", "raw", "P_S11"), 250, 260));

        QVERIFY(f.accept(SequenceName("bnd", "raw", "T_S11"), 50, 260));


        QVERIFY(f.accept(SequenceName("bnd", "raw", "T_S11"), 50, 60));
        QVERIFY(!f.accept(SequenceName("alt", "raw", "P_S11"), 50, 60));
        QVERIFY(f.accept(SequenceName("bnd", "raw", "P_S11"), 50, 60));
        QVERIFY(f.accept(SequenceName("bnd", "raw", "T_S11"), 50, 260));

        QVERIFY(f.accept(SequenceName("bnd", "raw", "T_S11"), 150, 160));
        QVERIFY(f.accept(SequenceName("alt", "raw", "P_S11"), 150, 160));
        QVERIFY(!f.accept(SequenceName("bnd", "raw", "P_S11"), 150, 160));

        QVERIFY(f.accept(SequenceName("bnd", "raw", "T_S11"), 250, 260));
        QVERIFY(!f.accept(SequenceName("alt", "raw", "P_S11"), 250, 260));
        QVERIFY(f.accept(SequenceName("bnd", "raw", "P_S11"), 250, 260));
    }
};

QTEST_APPLESS_MAIN(TestSequenceFilter)

#include "sequencefilter.moc"
