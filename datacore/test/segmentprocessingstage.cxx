/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QHash>
#include <QList>

#include "datacore/segmentprocessingstage.hxx"
#include "datacore/stream.hxx"
#include "core/range.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class SegmentProcessingStageTestImplementation : public SegmentProcessingStage {
public:

    QHash<int, QSet<double> > breaks;
    QHash<SequenceName, int> filterTargets;
    QHash<SequenceName, int> filterMetaTargets;
    QHash<SequenceName, int> inputTargets;
    QHash<SequenceName, int> injectTargets;
    QHash<SequenceName, QSet<SequenceName> > additionalInputs;
    QSet<SequenceName> bypassed;
    QSet<SequenceName> remainingUnhandled;
    QHash<int, QHash<int, QHash<SequenceName, double> > > setValues;
    QHash<int, QHash<int, QHash<SequenceName, double> > > expectValues;
    QHash<int, int> setCalls;
    QHash<int, int> setMetaCalls;

    QList<int> failures;
#define BVERIFY(c)  do { if (!(c)) { failures.append(__LINE__); return; } } while (0)

    SegmentProcessingStageTestImplementation()
    { reset(); }

    void reset()
    {
        breaks.clear();
        filterTargets.clear();
        filterMetaTargets.clear();
        bypassed.clear();
        inputTargets.clear();
        additionalInputs.clear();
        remainingUnhandled.clear();
        setValues.clear();
        expectValues.clear();
        setCalls.clear();
        setMetaCalls.clear();
        failures.clear();
    }

    virtual void unhandled(const SequenceName &unit,
                           SegmentProcessingStage::SequenceHandlerControl *control)
    {
        QHash<SequenceName, int>::iterator s = filterTargets.find(unit);
        if (s != filterTargets.end()) {
            control->filterUnit(unit, s.value());
            remainingUnhandled.remove(unit);
            QSet<SequenceName> v(additionalInputs[unit]);
            for (QSet<SequenceName>::const_iterator u = v.constBegin(), end = v.constEnd();
                    u != end;
                    ++u) {
                control->inputUnit(*u, s.value());
                remainingUnhandled.remove(*u);
            }
            return;
        }
        s = inputTargets.find(unit);
        if (s != inputTargets.end()) {
            control->inputUnit(unit, s.value());
            remainingUnhandled.remove(unit);
            QSet<SequenceName> v(additionalInputs[unit]);
            for (QSet<SequenceName>::const_iterator u = v.constBegin(), end = v.constEnd();
                    u != end;
                    ++u) {
                control->inputUnit(*u, s.value());
                remainingUnhandled.remove(*u);
            }
            return;
        }
        s = injectTargets.find(unit);
        if (s != injectTargets.end()) {
            control->injectUnit(unit, s.value());
            remainingUnhandled.remove(unit);
            QSet<SequenceName> v(additionalInputs[unit]);
            for (QSet<SequenceName>::const_iterator u = v.constBegin(), end = v.constEnd();
                    u != end;
                    ++u) {
                control->injectUnit(*u, s.value());
                remainingUnhandled.remove(*u);
            }
            return;
        }
        if (bypassed.contains(unit))
            return;
        for (QHash<SequenceName, QSet<SequenceName> >::const_iterator
                us = additionalInputs.constBegin(), end = additionalInputs.constEnd();
                us != end;
                ++us) {
            if (us.value().contains(unit))
                return;
        }
        BVERIFY(false);
    }

    virtual void process(int id, SequenceSegment &data)
    {
        BVERIFY(!filterTargets.keys(id).isEmpty());
        QList<SequenceName> keys(filterTargets.keys(id));
        for (QList<SequenceName>::const_iterator set = keys.constBegin(), end = keys.constEnd();
                set != end;
                ++set) {
            QHash<SequenceName, double> eCheck = expectValues[id][setCalls[id]];
            QHash<SequenceName, double>::const_iterator check = eCheck.constFind(*set);
            if (check != eCheck.constEnd()) {
                BVERIFY(check.value() == data.getValue(*set).toDouble());
            }

            data.setValue(*set, Variant::Root(setValues[id][setCalls[id]][*set]));
        }
        keys = inputTargets.keys(id);
        for (QList<SequenceName>::const_iterator set = keys.constBegin(), end = keys.constEnd();
                set != end;
                ++set) {
            QHash<SequenceName, double> eCheck = expectValues[id][setCalls[id]];
            QHash<SequenceName, double>::const_iterator check = eCheck.constFind(*set);
            if (check != eCheck.constEnd()) {
                BVERIFY(check.value() == data.getValue(*set).toDouble());
            }
        }
        keys = injectTargets.keys(id);
        for (QList<SequenceName>::const_iterator set = keys.constBegin(), end = keys.constEnd();
                set != end;
                ++set) {
            QHash<SequenceName, double> eCheck = expectValues[id][setCalls[id]];
            QHash<SequenceName, double>::const_iterator check = eCheck.constFind(*set);
            if (check != eCheck.constEnd()) {
                BVERIFY(check.value() == data.getValue(*set).toDouble());
            }
        }
        setCalls[id]++;
    }

    virtual void processMeta(int id, SequenceSegment &data)
    {
        BVERIFY(!filterMetaTargets.keys(id).isEmpty());
        QList<SequenceName> keys(filterMetaTargets.keys(id));
        for (QList<SequenceName>::const_iterator set = keys.constBegin(), end = keys.constEnd();
                set != end;
                ++set) {
            QHash<SequenceName, double> eCheck = expectValues[id][setMetaCalls[id]];
            QHash<SequenceName, double>::const_iterator check = eCheck.constFind(*set);
            if (check != eCheck.constEnd()) {
                BVERIFY(check.value() == data.getValue(*set).toDouble());
            }

            data.setValue(*set, Variant::Root(setValues[id][setMetaCalls[id]][*set]));
        }
        keys = inputTargets.keys(id);
        for (QList<SequenceName>::const_iterator set = keys.constBegin(), end = keys.constEnd();
                set != end;
                ++set) {
            QHash<SequenceName, double> eCheck = expectValues[id][setMetaCalls[id]];
            QHash<SequenceName, double>::const_iterator check = eCheck.constFind(*set);
            if (check != eCheck.constEnd()) {
                BVERIFY(check.value() == data.getValue(*set).toDouble());
            }
        }
        keys = injectTargets.keys(id);
        for (QList<SequenceName>::const_iterator set = keys.constBegin(), end = keys.constEnd();
                set != end;
                ++set) {
            QHash<SequenceName, double> eCheck = expectValues[id][setMetaCalls[id]];
            QHash<SequenceName, double>::const_iterator check = eCheck.constFind(*set);
            if (check != eCheck.constEnd()) {
                BVERIFY(check.value() == data.getValue(*set).toDouble());
            }
        }
        setMetaCalls[id]++;
    }

    virtual QSet<double> metadataBreaks(int id)
    { return breaks[id]; }
};

class TestSegmentProcessingStage : public QObject {
Q_OBJECT

    bool verifyValue(const SequenceValue::Transfer &values,
                     double start, double end, const SequenceName &unit,
                     const Variant::Read &expected)
    {
        auto check = Range::findIntersecting(values.cbegin(), values.cend(), start, end);
        for (; check != values.cbegin() && FP::equal((check - 1)->getStart(), start); --check) { }
        for (; check != values.cend(); ++check) {
            if (check->getUnit() != unit)
                continue;
            for (; check != values.cend() && Range::compareStartEnd(check->getStart(), end) < 0;
                    ++check) {
                if (check->getUnit() != unit)
                    continue;
                if (!expected.exists()) {
                    if (check->getValue().exists())
                        return false;
                } else {
                    if (check->getValue() != expected)
                        return false;
                }
            }
            return true;
        }
        return false;
    }

    bool checkAscending(const SequenceValue::Transfer &values)
    {
        if (values.size() <= 1)
            return true;
        for (auto v = values.cbegin(), next = v + 1, end = values.cend();
                next != end;
                v = next, ++next) {
            if (Range::compareStart(v->getStart(), next->getStart()) > 0)
                return false;
        }
        return true;
    }

private slots:

    void singleData()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();


        SequenceName u1("brw", "raw", "T_S11");

        filter.filterTargets[u1] = 1;
        filter.setValues[1][0][u1] = 2.0;
        filter.setValues[1][1][u1] = 2.1;
        filter.setValues[1][2][u1] = 2.2;
        filter.expectValues[1][0][u1] = 1.0;
        filter.expectValues[1][1][u1] = 1.1;
        filter.expectValues[1][2][u1] = 1.2;
        filter.remainingUnhandled << u1;

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(1.1), 2.0, 3.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(1.2), 3.0, 4.0));
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
        for (QList<int>::const_iterator line = filter.failures.constBegin(),
                end = filter.failures.constEnd(); line != end; ++line) {
            QByteArray er("Failure at line: ");
            er.append(QByteArray::number(*line));
            QFAIL(er.data());
        }
        QVERIFY(filter.remainingUnhandled.isEmpty());
        QCOMPARE(filter.setCalls[1], 3);
        QCOMPARE(filter.setMetaCalls[1], 0);
        QCOMPARE((int) results.values().size(), 3);
        QVERIFY(checkAscending(results.values()));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u1, Variant::Root(2.0)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u1, Variant::Root(2.1)));
        QVERIFY(verifyValue(results.values(), 3.0, 4.0, u1, Variant::Root(2.2)));
    }

    void singleDataWait()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();


        SequenceName u1("brw", "raw", "T_S11");

        filter.filterTargets[u1] = 1;
        filter.setValues[1][0][u1] = 2.0;
        filter.setValues[1][1][u1] = 2.1;
        filter.setValues[1][2][u1] = 2.2;
        filter.expectValues[1][0][u1] = 1.0;
        filter.expectValues[1][1][u1] = 1.1;
        filter.expectValues[1][2][u1] = 1.2;
        filter.remainingUnhandled << u1;

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u1, Variant::Root(1.1), 2.0, 3.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u1, Variant::Root(1.2), 3.0, 4.0));
        QTest::qSleep(50);
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
        for (QList<int>::const_iterator line = filter.failures.constBegin(),
                end = filter.failures.constEnd(); line != end; ++line) {
            QByteArray er("Failure at line: ");
            er.append(QByteArray::number(*line));
            QFAIL(er.data());
        }
        QVERIFY(filter.remainingUnhandled.isEmpty());
        QCOMPARE(filter.setCalls[1], 3);
        QCOMPARE(filter.setMetaCalls[1], 0);
        QCOMPARE((int) results.values().size(), 3);
        QVERIFY(checkAscending(results.values()));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u1, Variant::Root(2.0)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u1, Variant::Root(2.1)));
        QVERIFY(verifyValue(results.values(), 3.0, 4.0, u1, Variant::Root(2.2)));
    }

    void singleMetadata()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();


        SequenceName u1("brw", "raw_meta", "T_S11");

        filter.filterTargets[u1.fromMeta()] = 1;
        filter.filterMetaTargets[u1] = 1;
        filter.setValues[1][0][u1] = 2.0;
        filter.setValues[1][1][u1] = 2.1;
        filter.setValues[1][2][u1] = 2.2;
        filter.expectValues[1][0][u1] = 1.0;
        filter.expectValues[1][1][u1] = 1.1;
        filter.expectValues[1][2][u1] = 1.2;
        filter.remainingUnhandled << u1.fromMeta();

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(1.1), 2.0, 3.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(1.2), 3.0, 4.0));
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
        for (QList<int>::const_iterator line = filter.failures.constBegin(),
                end = filter.failures.constEnd(); line != end; ++line) {
            QByteArray er("Failure at line: ");
            er.append(QByteArray::number(*line));
            QFAIL(er.data());
        }
        QVERIFY(filter.remainingUnhandled.isEmpty());
        QCOMPARE(filter.setCalls[1], 0);
        QCOMPARE(filter.setMetaCalls[1], 3);
        QCOMPARE((int) results.values().size(), 3);
        QVERIFY(checkAscending(results.values()));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u1, Variant::Root(2.0)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u1, Variant::Root(2.1)));
        QVERIFY(verifyValue(results.values(), 3.0, 4.0, u1, Variant::Root(2.2)));
    }

    void metadataBreaks()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();


        SequenceName u1("brw", "raw_meta", "T_S11");

        filter.filterTargets[u1.fromMeta()] = 1;
        filter.filterMetaTargets[u1] = 1;
        filter.setValues[1][0][u1] = 2.0;
        filter.setValues[1][1][u1] = 2.1;
        filter.setValues[1][2][u1] = 2.2;
        filter.setValues[1][3][u1] = 2.3;
        filter.setValues[1][4][u1] = 2.4;
        filter.expectValues[1][0][u1] = 1.0;
        filter.expectValues[1][1][u1] = 1.0;
        filter.expectValues[1][2][u1] = 1.1;
        filter.expectValues[1][3][u1] = 1.2;
        filter.expectValues[1][4][u1] = 1.2;
        filter.breaks[1] << 0.5 << 1.0 << 1.5 << 3.5 << 4.0 << 5.0;
        filter.remainingUnhandled << u1.fromMeta();

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(1.1), 2.0, 3.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(1.2), 3.0, 4.0));
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
        for (QList<int>::const_iterator line = filter.failures.constBegin(),
                end = filter.failures.constEnd(); line != end; ++line) {
            QByteArray er("Failure at line: ");
            er.append(QByteArray::number(*line));
            QFAIL(er.data());
        }
        QVERIFY(filter.remainingUnhandled.isEmpty());
        QCOMPARE(filter.setCalls[1], 0);
        QCOMPARE(filter.setMetaCalls[1], 5);
        QCOMPARE((int) results.values().size(), 5);
        QVERIFY(checkAscending(results.values()));
        QVERIFY(verifyValue(results.values(), 1.0, 1.5, u1, Variant::Root(2.0)));
        QVERIFY(verifyValue(results.values(), 1.5, 2.0, u1, Variant::Root(2.1)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u1, Variant::Root(2.2)));
        QVERIFY(verifyValue(results.values(), 3.0, 3.5, u1, Variant::Root(2.3)));
        QVERIFY(verifyValue(results.values(), 3.5, 4.0, u1, Variant::Root(2.4)));
    }

    void bypass()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();


        SequenceName u1("brw", "raw", "T_S11");
        SequenceName u2("brw", "raw", "T_A11");

        filter.filterTargets[u1] = 1;
        filter.setValues[1][0][u1] = 2.0;
        filter.setValues[1][1][u1] = 2.1;
        filter.setValues[1][2][u1] = 2.2;
        filter.expectValues[1][0][u1] = 1.1;
        filter.expectValues[1][1][u1] = 1.3;
        filter.expectValues[1][2][u1] = 1.5;
        filter.remainingUnhandled << u1;
        filter.bypassed << u2;

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u2, Variant::Root(1.0), 0.5, 2.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(1.1), 1.0, 2.0));
        thread->incomingData(SequenceValue(u2, Variant::Root(1.2), 1.0, 2.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(1.3), 2.0, 3.0));
        thread->incomingData(SequenceValue(u2, Variant::Root(1.4), 2.5, 5.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(1.5), 3.0, 4.0));
        thread->incomingData(SequenceValue(u2, Variant::Root(1.6), 5.0, 7.0));
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
        for (QList<int>::const_iterator line = filter.failures.constBegin(),
                end = filter.failures.constEnd(); line != end; ++line) {
            QByteArray er("Failure at line: ");
            er.append(QByteArray::number(*line));
            QFAIL(er.data());
        }
        QVERIFY(filter.remainingUnhandled.isEmpty());
        QCOMPARE(filter.setCalls[1], 3);
        QCOMPARE(filter.setMetaCalls[1], 0);
        QVERIFY(checkAscending(results.values()));
        QVERIFY(verifyValue(results.values(), 0.5, 1.0, u2, Variant::Root(1.0)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u1, Variant::Root(2.0)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u2, Variant::Root(1.2)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u1, Variant::Root(2.1)));
        QVERIFY(verifyValue(results.values(), 2.5, 5.0, u2, Variant::Root(1.4)));
        QVERIFY(verifyValue(results.values(), 3.0, 4.0, u1, Variant::Root(2.2)));
        QVERIFY(verifyValue(results.values(), 5.0, 7.0, u2, Variant::Root(1.6)));
    }

    void bypassWait()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();


        SequenceName u1("brw", "raw", "T_S11");
        SequenceName u2("brw", "raw", "T_A11");

        filter.filterTargets[u1] = 1;
        filter.setValues[1][0][u1] = 2.0;
        filter.setValues[1][1][u1] = 2.1;
        filter.setValues[1][2][u1] = 2.2;
        filter.expectValues[1][0][u1] = 1.1;
        filter.expectValues[1][1][u1] = 1.3;
        filter.expectValues[1][2][u1] = 1.5;
        filter.remainingUnhandled << u1;
        filter.bypassed << u2;

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u2, Variant::Root(1.0), 0.5, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u1, Variant::Root(1.1), 1.0, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u2, Variant::Root(1.2), 1.0, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u1, Variant::Root(1.3), 2.0, 3.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u2, Variant::Root(1.4), 2.5, 5.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u1, Variant::Root(1.5), 3.0, 4.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u2, Variant::Root(1.6), 5.0, 7.0));
        QTest::qSleep(50);
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
        for (QList<int>::const_iterator line = filter.failures.constBegin(),
                end = filter.failures.constEnd(); line != end; ++line) {
            QByteArray er("Failure at line: ");
            er.append(QByteArray::number(*line));
            QFAIL(er.data());
        }
        QVERIFY(filter.remainingUnhandled.isEmpty());
        QCOMPARE(filter.setCalls[1], 3);
        QCOMPARE(filter.setMetaCalls[1], 0);
        QVERIFY(checkAscending(results.values()));
        QVERIFY(verifyValue(results.values(), 0.5, 1.0, u2, Variant::Root(1.0)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u1, Variant::Root(2.0)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u2, Variant::Root(1.2)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u1, Variant::Root(2.1)));
        QVERIFY(verifyValue(results.values(), 2.5, 5.0, u2, Variant::Root(1.4)));
        QVERIFY(verifyValue(results.values(), 3.0, 4.0, u1, Variant::Root(2.2)));
        QVERIFY(verifyValue(results.values(), 5.0, 7.0, u2, Variant::Root(1.6)));
    }

    void interweave()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();


        SequenceName u1("brw", "raw", "T_S11");
        SequenceName u2("brw", "raw_meta", "T_S11");
        SequenceName u3("brw", "raw", "T_S12");
        SequenceName u4("alt", "raw", "T_S11");
        SequenceName u5("alt", "raw", "T_S12");

        filter.filterTargets[u1] = 0;
        filter.filterTargets[u3] = 0;
        filter.filterTargets[u4] = 1;
        filter.inputTargets[u5] = 1;
        filter.filterMetaTargets[u2] = 0;
        filter.setValues[0][0][u1] = 0.1;
        filter.setValues[0][0][u3] = 0.2;
        filter.setValues[0][1][u1] = 0.3;
        filter.setValues[0][1][u3] = 0.4;
        filter.setValues[0][2][u1] = 0.5;
        filter.setValues[0][2][u3] = 0.6;
        filter.setValues[0][3][u1] = 0.7;
        filter.setValues[0][3][u3] = 0.8;

        filter.setValues[0][0][u2] = 0.11;
        filter.setValues[0][1][u2] = 0.21;

        filter.setValues[1][0][u4] = 1.1;
        filter.setValues[1][1][u4] = 1.2;

        filter.expectValues[0][0][u1] = 2.0;
        filter.expectValues[0][1][u1] = 5.0;
        filter.expectValues[0][1][u3] = 7.0;
        filter.expectValues[0][2][u3] = 8.0;
        filter.expectValues[0][3][u3] = 8.0;

        filter.expectValues[0][0][u2] = 1.0;
        filter.expectValues[0][1][u2] = 6.0;

        filter.expectValues[1][0][u4] = 3.0;
        filter.expectValues[1][0][u5] = 3.1;
        filter.expectValues[1][1][u4] = 4.0;
        filter.expectValues[1][1][u5] = 4.1;

        filter.remainingUnhandled << u1 << u3 << u4 << u5;

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u2, Variant::Root(1.0), FP::undefined(), 2.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(2.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u5, Variant::Root(3.1), 1.0, 2.0));
        thread->incomingData(SequenceValue(u4, Variant::Root(3.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u4, Variant::Root(4.0), 1.5, 2.0));
        thread->incomingData(SequenceValue(u5, Variant::Root(4.1), 1.5, 2.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(5.0), 2.0, 4.0));
        thread->incomingData(SequenceValue(u2, Variant::Root(6.0), 2.0, 4.0));
        thread->incomingData(SequenceValue(u3, Variant::Root(7.0), 2.0, 4.5));
        thread->incomingData(SequenceValue(u3, Variant::Root(8.0), 4.0, 5.0));
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
        for (QList<int>::const_iterator line = filter.failures.constBegin(),
                end = filter.failures.constEnd(); line != end; ++line) {
            QByteArray er("Failure at line: ");
            er.append(QByteArray::number(*line));
            QFAIL(er.data());
        }
        QVERIFY(filter.remainingUnhandled.isEmpty());
        QCOMPARE(filter.setCalls[0], 4);
        QCOMPARE(filter.setCalls[1], 2);
        QCOMPARE(filter.setMetaCalls[0], 2);
        QVERIFY(checkAscending(results.values()));
        QVERIFY(verifyValue(results.values(), FP::undefined(), 2.0, u2, Variant::Root(0.11)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u1, Variant::Root(0.1)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u3, Variant::Root(0.2)));
        QVERIFY(verifyValue(results.values(), 1.0, 1.5, u4, Variant::Root(1.1)));
        QVERIFY(verifyValue(results.values(), 1.5, 2.0, u4, Variant::Root(1.2)));
        QVERIFY(verifyValue(results.values(), 2.0, 4.0, u2, Variant::Root(0.21)));
        QVERIFY(verifyValue(results.values(), 2.0, 4.0, u1, Variant::Root(0.3)));
        QVERIFY(verifyValue(results.values(), 2.0, 4.0, u3, Variant::Root(0.4)));
        QVERIFY(verifyValue(results.values(), 4.0, 4.5, u1, Variant::Root(0.5)));
        QVERIFY(verifyValue(results.values(), 4.0, 4.5, u3, Variant::Root(0.6)));
        QVERIFY(verifyValue(results.values(), 4.5, 5.0, u1, Variant::Root(0.7)));
        QVERIFY(verifyValue(results.values(), 4.5, 5.0, u3, Variant::Root(0.8)));
    }

    void interweaveWait()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();


        SequenceName u1("brw", "raw", "T_S11");
        SequenceName u2("brw", "raw_meta", "T_S11");
        SequenceName u3("brw", "raw", "T_S12");
        SequenceName u4("alt", "raw", "T_S11");
        SequenceName u5("alt", "raw", "T_S12");

        filter.filterTargets[u1] = 0;
        filter.filterTargets[u3] = 0;
        filter.filterTargets[u4] = 1;
        filter.inputTargets[u5] = 1;
        filter.filterMetaTargets[u2] = 0;
        filter.setValues[0][0][u1] = 0.1;
        filter.setValues[0][0][u3] = 0.2;
        filter.setValues[0][1][u1] = 0.3;
        filter.setValues[0][1][u3] = 0.4;
        filter.setValues[0][2][u1] = 0.5;
        filter.setValues[0][2][u3] = 0.6;
        filter.setValues[0][3][u1] = 0.7;
        filter.setValues[0][3][u3] = 0.8;

        filter.setValues[0][0][u2] = 0.11;
        filter.setValues[0][1][u2] = 0.21;

        filter.setValues[1][0][u4] = 1.1;
        filter.setValues[1][1][u4] = 1.2;

        filter.expectValues[0][0][u1] = 2.0;
        filter.expectValues[0][1][u1] = 5.0;
        filter.expectValues[0][1][u3] = 7.0;
        filter.expectValues[0][2][u3] = 8.0;
        filter.expectValues[0][3][u3] = 8.0;

        filter.expectValues[0][0][u2] = 1.0;
        filter.expectValues[0][1][u2] = 6.0;

        filter.expectValues[1][0][u4] = 3.0;
        filter.expectValues[1][0][u5] = 3.1;
        filter.expectValues[1][1][u4] = 4.0;
        filter.expectValues[1][1][u5] = 4.1;

        filter.remainingUnhandled << u1 << u3 << u4 << u5;

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u2, Variant::Root(1.0), FP::undefined(), 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u1, Variant::Root(2.0), 1.0, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u5, Variant::Root(3.1), 1.0, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u4, Variant::Root(3.0), 1.0, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u4, Variant::Root(4.0), 1.5, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u5, Variant::Root(4.1), 1.5, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u1, Variant::Root(5.0), 2.0, 4.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u2, Variant::Root(6.0), 2.0, 4.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u3, Variant::Root(7.0), 2.0, 4.5));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u3, Variant::Root(8.0), 4.0, 5.0));
        QTest::qSleep(50);
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
        for (QList<int>::const_iterator line = filter.failures.constBegin(),
                end = filter.failures.constEnd(); line != end; ++line) {
            QByteArray er("Failure at line: ");
            er.append(QByteArray::number(*line));
            QFAIL(er.data());
        }
        QVERIFY(filter.remainingUnhandled.isEmpty());
        QCOMPARE(filter.setCalls[0], 4);
        QCOMPARE(filter.setCalls[1], 2);
        QCOMPARE(filter.setMetaCalls[0], 2);
        QVERIFY(checkAscending(results.values()));
        QVERIFY(verifyValue(results.values(), FP::undefined(), 2.0, u2, Variant::Root(0.11)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u1, Variant::Root(0.1)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u3, Variant::Root(0.2)));
        QVERIFY(verifyValue(results.values(), 1.0, 1.5, u4, Variant::Root(1.1)));
        QVERIFY(verifyValue(results.values(), 1.5, 2.0, u4, Variant::Root(1.2)));
        QVERIFY(verifyValue(results.values(), 2.0, 4.0, u2, Variant::Root(0.21)));
        QVERIFY(verifyValue(results.values(), 2.0, 4.0, u1, Variant::Root(0.3)));
        QVERIFY(verifyValue(results.values(), 2.0, 4.0, u3, Variant::Root(0.4)));
        QVERIFY(verifyValue(results.values(), 4.0, 4.5, u1, Variant::Root(0.5)));
        QVERIFY(verifyValue(results.values(), 4.0, 4.5, u3, Variant::Root(0.6)));
        QVERIFY(verifyValue(results.values(), 4.5, 5.0, u1, Variant::Root(0.7)));
        QVERIFY(verifyValue(results.values(), 4.5, 5.0, u3, Variant::Root(0.8)));
    }

    void backreferenceInput()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();


        SequenceName u1("brw", "raw", "T_S11");
        SequenceName u2("brw", "raw", "T_S12");

        filter.filterTargets[u1] = 0;
        filter.additionalInputs[u1] << u2;

        filter.setValues[0][0][u1] = 0.1;

        filter.expectValues[0][0][u1] = 2.0;
        filter.expectValues[0][0][u2] = 1.0;

        filter.remainingUnhandled << u1 << u2;

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u2, Variant::Root(1.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(2.0), 1.0, 2.0));
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
        for (QList<int>::const_iterator line = filter.failures.constBegin(),
                end = filter.failures.constEnd(); line != end; ++line) {
            QByteArray er("Failure at line: ");
            er.append(QByteArray::number(*line));
            QFAIL(er.data());
        }
        QVERIFY(filter.remainingUnhandled.isEmpty());
        QCOMPARE(filter.setCalls[0], 1);
        QVERIFY(filter.setMetaCalls.isEmpty());
        QVERIFY(checkAscending(results.values()));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u1, Variant::Root(0.1)));
    }

    void requestInputs()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();

        SequenceName u1("brw", "raw", "T_S11");
        SequenceName u2("brw", "raw", "T_S12");
        SequenceName u3("alt", "raw", "T_S11");
        SequenceName u4("alt", "raw", "T_S12");

        filter.filterTargets[u1] = 0;
        filter.additionalInputs[u1] << u2;
        filter.filterTargets[u3] = 1;
        filter.additionalInputs[u3] << u4;

        filter.setValues[0][0][u1] = 0.1;
        filter.setValues[0][1][u1] = 0.2;

        filter.setValues[1][0][u3] = 0.11;
        filter.setValues[1][1][u3] = 0.12;

        filter.expectValues[0][0][u1] = 1.0;
        filter.expectValues[0][0][u2] = 2.0;
        filter.expectValues[0][1][u1] = 6.0;
        filter.expectValues[0][1][u2] = 5.0;

        filter.expectValues[1][0][u3] = 4.0;
        filter.expectValues[1][0][u4] = 3.0;
        filter.expectValues[1][1][u3] = 7.0;
        filter.expectValues[1][1][u4] = 8.0;

        filter.remainingUnhandled << u1 << u2 << u3 << u4;

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u2, Variant::Root(2.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u4, Variant::Root(3.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u3, Variant::Root(4.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u2, Variant::Root(5.0), 2.0, 3.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(6.0), 2.0, 3.0));
        thread->incomingData(SequenceValue(u3, Variant::Root(7.0), 2.0, 3.0));
        thread->incomingData(SequenceValue(u4, Variant::Root(8.0), 2.0, 3.0));
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
        for (QList<int>::const_iterator line = filter.failures.constBegin(),
                end = filter.failures.constEnd(); line != end; ++line) {
            QByteArray er("Failure at line: ");
            er.append(QByteArray::number(*line));
            QFAIL(er.data());
        }
        QVERIFY(filter.remainingUnhandled.isEmpty());
        QCOMPARE(filter.setCalls[0], 2);
        QCOMPARE(filter.setCalls[1], 2);
        QVERIFY(filter.setMetaCalls.isEmpty());
        QVERIFY(checkAscending(results.values()));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u1, Variant::Root(0.1)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u3, Variant::Root(0.11)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u1, Variant::Root(0.2)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u3, Variant::Root(0.12)));
    }

    void requestInputsWait()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();

        SequenceName u1("brw", "raw", "T_S11");
        SequenceName u2("brw", "raw", "T_S12");
        SequenceName u3("alt", "raw", "T_S11");
        SequenceName u4("alt", "raw", "T_S12");

        filter.filterTargets[u1] = 0;
        filter.additionalInputs[u1] << u2;
        filter.inputTargets[u2] = 0;
        filter.filterTargets[u3] = 1;
        filter.additionalInputs[u3] << u4;
        filter.inputTargets[u4] = 1;

        filter.setValues[0][0][u1] = 0.1;
        filter.setValues[0][1][u1] = 0.2;

        filter.setValues[1][0][u3] = 0.11;
        filter.setValues[1][1][u3] = 0.12;

        filter.expectValues[0][0][u1] = 1.0;
        filter.expectValues[0][0][u2] = 2.0;
        filter.expectValues[0][1][u1] = 6.0;
        filter.expectValues[0][1][u2] = 5.0;

        filter.expectValues[1][0][u3] = 4.0;
        filter.expectValues[1][0][u4] = 3.0;
        filter.expectValues[1][1][u3] = 7.0;
        filter.expectValues[1][1][u4] = 8.0;

        filter.remainingUnhandled << u1 << u2 << u3 << u4;

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u2, Variant::Root(2.0), 1.0, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u4, Variant::Root(3.0), 1.0, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u3, Variant::Root(4.0), 1.0, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u2, Variant::Root(5.0), 2.0, 3.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u1, Variant::Root(6.0), 2.0, 3.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u3, Variant::Root(7.0), 2.0, 3.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u4, Variant::Root(8.0), 2.0, 3.0));
        QTest::qSleep(50);
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
        for (QList<int>::const_iterator line = filter.failures.constBegin(),
                end = filter.failures.constEnd(); line != end; ++line) {
            QByteArray er("Failure at line: ");
            er.append(QByteArray::number(*line));
            QFAIL(er.data());
        }
        QVERIFY(filter.remainingUnhandled.isEmpty());
        QCOMPARE(filter.setCalls[0], 2);
        QCOMPARE(filter.setCalls[1], 2);
        QVERIFY(filter.setMetaCalls.isEmpty());
        QVERIFY(checkAscending(results.values()));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u1, Variant::Root(0.1)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u3, Variant::Root(0.11)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u1, Variant::Root(0.2)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u3, Variant::Root(0.12)));
    }

    void inputsUnaltered()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();

        SequenceName u1("brw", "raw", "T_S11");
        SequenceName u2("brw", "raw", "P_S12");
        SequenceName u3("brw", "raw", "BsG_S11");

        filter.filterTargets[u3] = 0;
        filter.additionalInputs[u3] << u1 << u2;

        filter.setValues[0][0][u3] = 0.1;
        filter.setValues[0][1][u3] = 0.2;
        filter.setValues[0][2][u3] = 0.3;

        filter.expectValues[0][0][u1] = 1.0;
        filter.expectValues[0][0][u2] = 2.0;
        filter.expectValues[0][0][u3] = 3.0;
        filter.expectValues[0][1][u1] = 4.0;
        filter.expectValues[0][1][u2] = 5.0;
        filter.expectValues[0][1][u3] = 6.0;
        filter.expectValues[0][2][u1] = 7.0;
        filter.expectValues[0][2][u2] = 8.0;
        filter.expectValues[0][2][u3] = 9.0;

        filter.remainingUnhandled << u1 << u2 << u3;

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u2, Variant::Root(2.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u3, Variant::Root(3.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(4.0), 2.0, 3.0));
        thread->incomingData(SequenceValue(u2, Variant::Root(5.0), 2.0, 3.0));
        thread->incomingData(SequenceValue(u3, Variant::Root(6.0), 2.0, 3.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(7.0), 3.0, 4.0));
        thread->incomingData(SequenceValue(u2, Variant::Root(8.0), 3.0, 4.0));
        thread->incomingData(SequenceValue(u3, Variant::Root(9.0), 3.0, 4.0));
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
        for (QList<int>::const_iterator line = filter.failures.constBegin(),
                end = filter.failures.constEnd(); line != end; ++line) {
            QByteArray er("Failure at line: ");
            er.append(QByteArray::number(*line));
            QFAIL(er.data());
        }
        QVERIFY(filter.remainingUnhandled.isEmpty());
        QCOMPARE(filter.setCalls[0], 3);
        QVERIFY(filter.setMetaCalls.isEmpty());
        QVERIFY(checkAscending(results.values()));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u1, Variant::Root(1.0)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u2, Variant::Root(2.0)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u3, Variant::Root(0.1)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u1, Variant::Root(4.0)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u2, Variant::Root(5.0)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u3, Variant::Root(0.2)));
        QVERIFY(verifyValue(results.values(), 3.0, 4.0, u1, Variant::Root(7.0)));
        QVERIFY(verifyValue(results.values(), 3.0, 4.0, u2, Variant::Root(8.0)));
        QVERIFY(verifyValue(results.values(), 3.0, 4.0, u3, Variant::Root(0.3)));
    }

    void inputsUnalteredWait()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();

        SequenceName u1("brw", "raw", "T_S11");
        SequenceName u2("brw", "raw", "P_S12");
        SequenceName u3("brw", "raw", "BsG_S11");

        filter.filterTargets[u3] = 0;
        filter.additionalInputs[u3] << u1 << u2;

        filter.setValues[0][0][u3] = 0.1;
        filter.setValues[0][1][u3] = 0.2;
        filter.setValues[0][2][u3] = 0.3;

        filter.expectValues[0][0][u1] = 1.0;
        filter.expectValues[0][0][u2] = 2.0;
        filter.expectValues[0][0][u3] = 3.0;
        filter.expectValues[0][1][u1] = 4.0;
        filter.expectValues[0][1][u2] = 5.0;
        filter.expectValues[0][1][u3] = 6.0;
        filter.expectValues[0][2][u1] = 7.0;
        filter.expectValues[0][2][u2] = 8.0;
        filter.expectValues[0][2][u3] = 9.0;

        filter.remainingUnhandled << u1 << u2 << u3;

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u2, Variant::Root(2.0), 1.0, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u3, Variant::Root(3.0), 1.0, 2.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u1, Variant::Root(4.0), 2.0, 3.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u2, Variant::Root(5.0), 2.0, 3.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u3, Variant::Root(6.0), 2.0, 3.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u1, Variant::Root(7.0), 3.0, 4.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u2, Variant::Root(8.0), 3.0, 4.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u3, Variant::Root(9.0), 3.0, 4.0));
        QTest::qSleep(50);
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
        for (QList<int>::const_iterator line = filter.failures.constBegin(),
                end = filter.failures.constEnd(); line != end; ++line) {
            QByteArray er("Failure at line: ");
            er.append(QByteArray::number(*line));
            QFAIL(er.data());
        }
        QVERIFY(filter.remainingUnhandled.isEmpty());
        QCOMPARE(filter.setCalls[0], 3);
        QVERIFY(filter.setMetaCalls.isEmpty());
        QVERIFY(checkAscending(results.values()));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u1, Variant::Root(1.0)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u2, Variant::Root(2.0)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u3, Variant::Root(0.1)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u1, Variant::Root(4.0)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u2, Variant::Root(5.0)));
        QVERIFY(verifyValue(results.values(), 2.0, 3.0, u3, Variant::Root(0.2)));
        QVERIFY(verifyValue(results.values(), 3.0, 4.0, u1, Variant::Root(7.0)));
        QVERIFY(verifyValue(results.values(), 3.0, 4.0, u2, Variant::Root(8.0)));
        QVERIFY(verifyValue(results.values(), 3.0, 4.0, u3, Variant::Root(0.3)));
    }

    void serialize()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();

        SequenceName u1("brw", "raw", "T_S11");
        SequenceName u2("brw", "raw_meta", "T_S11");
        SequenceName u3("brw", "raw", "T_S12");
        SequenceName u4("alt", "raw", "T_S11");
        SequenceName u5("alt", "raw", "T_S12");
        SequenceName u6("alt", "raw", "T_S13");

        filter.filterTargets[u1] = 0;
        filter.filterTargets[u3] = 0;
        filter.filterTargets[u4] = 1;
        filter.inputTargets[u5] = 1;
        filter.injectTargets[u6] = 1;
        filter.filterMetaTargets[u2] = 0;
        filter.setValues[0][0][u1] = 0.1;
        filter.setValues[0][0][u3] = 0.2;
        filter.setValues[0][1][u1] = 0.3;
        filter.setValues[0][1][u3] = 0.4;
        filter.setValues[0][2][u1] = 0.5;
        filter.setValues[0][2][u3] = 0.6;
        filter.setValues[0][3][u1] = 0.7;
        filter.setValues[0][3][u3] = 0.8;

        filter.setValues[0][0][u2] = 0.11;
        filter.setValues[0][1][u2] = 0.21;

        filter.setValues[1][0][u4] = 1.1;
        filter.setValues[1][1][u4] = 1.2;

        filter.expectValues[0][0][u1] = 2.0;
        filter.expectValues[0][1][u1] = 5.0;
        filter.expectValues[0][1][u3] = 7.0;
        filter.expectValues[0][2][u3] = 8.0;
        filter.expectValues[0][3][u3] = 8.0;

        filter.expectValues[0][0][u2] = 1.0;
        filter.expectValues[0][1][u2] = 6.0;

        filter.expectValues[1][0][u4] = 3.0;
        filter.expectValues[1][0][u5] = 3.1;
        filter.expectValues[1][0][u6] = 3.2;
        filter.expectValues[1][1][u4] = 4.0;
        filter.expectValues[1][1][u5] = 4.1;
        filter.expectValues[1][1][u6] = 4.2;

        filter.remainingUnhandled << u1 << u3 << u4 << u5;

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u2, Variant::Root(1.0), FP::undefined(), 2.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(2.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u5, Variant::Root(3.1), 1.0, 2.0));
        thread->incomingData(SequenceValue(u4, Variant::Root(3.0), 1.0, 2.0));
        thread->incomingData(SequenceValue(u6, Variant::Root(3.2), 1.0, 1.9));
        thread->incomingData(SequenceValue(u4, Variant::Root(4.0), 1.5, 2.0));

        thread->setEgress(NULL);
        QByteArray data;
        QDataStream write(&data, QIODevice::WriteOnly);
        thread->serialize(write);
        thread->signalTerminate();
        QVERIFY(thread->wait(30));
        delete thread;

        QDataStream read(&data, QIODevice::ReadOnly);
        thread = new SegmentProcessingHandler(read, &filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(SequenceValue(u5, Variant::Root(4.1), 1.5, 2.0));
        thread->incomingData(SequenceValue(u6, Variant::Root(4.2), 1.5, 2.0));
        thread->incomingData(SequenceValue(u1, Variant::Root(5.0), 2.0, 4.0));
        thread->incomingData(SequenceValue(u2, Variant::Root(6.0), 2.0, 4.0));
        thread->incomingData(SequenceValue(u3, Variant::Root(7.0), 2.0, 4.5));
        thread->incomingData(SequenceValue(u3, Variant::Root(8.0), 4.0, 5.0));
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
        for (QList<int>::const_iterator line = filter.failures.constBegin(),
                end = filter.failures.constEnd(); line != end; ++line) {
            QByteArray er("Failure at line: ");
            er.append(QByteArray::number(*line));
            QFAIL(er.data());
        }
        QVERIFY(filter.remainingUnhandled.isEmpty());
        QCOMPARE(filter.setCalls[0], 4);
        QCOMPARE(filter.setCalls[1], 2);
        QCOMPARE(filter.setMetaCalls[0], 2);
        QVERIFY(checkAscending(results.values()));
        QVERIFY(verifyValue(results.values(), FP::undefined(), 2.0, u2, Variant::Root(0.11)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u1, Variant::Root(0.1)));
        QVERIFY(verifyValue(results.values(), 1.0, 2.0, u3, Variant::Root(0.2)));
        QVERIFY(verifyValue(results.values(), 1.0, 1.5, u4, Variant::Root(1.1)));
        QVERIFY(verifyValue(results.values(), 1.5, 2.0, u4, Variant::Root(1.2)));
        QVERIFY(verifyValue(results.values(), 2.0, 4.0, u2, Variant::Root(0.21)));
        QVERIFY(verifyValue(results.values(), 2.0, 4.0, u1, Variant::Root(0.3)));
        QVERIFY(verifyValue(results.values(), 2.0, 4.0, u3, Variant::Root(0.4)));
        QVERIFY(verifyValue(results.values(), 4.0, 4.5, u1, Variant::Root(0.5)));
        QVERIFY(verifyValue(results.values(), 4.0, 4.5, u3, Variant::Root(0.6)));
        QVERIFY(verifyValue(results.values(), 4.5, 5.0, u1, Variant::Root(0.7)));
        QVERIFY(verifyValue(results.values(), 4.5, 5.0, u3, Variant::Root(0.8)));
    }

    void metadataUnaligned()
    {
        SegmentProcessingStageTestImplementation filter;
        StreamSink::Buffer results;
        filter.reset();

        SequenceName u1("brw", "raw", "T_S11");
        SequenceName u2("brw", "raw", "U_S11");

        filter.additionalInputs[u1] << u2;
        filter.filterTargets[u1] = 1;
        filter.filterMetaTargets[u1] = 1;
        filter.remainingUnhandled << u1;

        SegmentProcessingHandler *thread = new SegmentProcessingHandler(&filter, false);
        thread->start();
        thread->setEgress(&results);

        thread->incomingData(
                SequenceValue(u2.toDefaultStation(), Variant::Root(0.5), 0.5, FP::undefined()));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u2.toMeta(), Variant::Root(1.1), 1.0, FP::undefined()));
        thread->incomingData(SequenceValue(u2, Variant::Root(1.1), 1.1, 6.0));
        thread->incomingData(SequenceValue(u1.toMeta(), Variant::Root(1.0), 2.0, FP::undefined()));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u1, Variant::Root(1.0), 3.0, 6.0));
        QTest::qSleep(50);
        thread->incomingData(SequenceValue(u2, Variant::Root(1.0), 3.0, 4.0));
        QTest::qSleep(50);
        thread->endData();

        QVERIFY(thread->wait(30));
        delete thread;
    }
};

QTEST_MAIN(TestSegmentProcessingStage)

#include "segmentprocessingstage.moc"
