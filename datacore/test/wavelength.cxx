/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <map>
#include <QTest>
#include <QString>
#include <QLocale>

#include "datacore/wavelength.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class BracketResult {
public:
    int lower;
    int upper;
    double target;

    BracketResult(int l, int u, double t) : lower(l), upper(u), target(t)
    { }

    BracketResult() : lower(-1), upper(-1), target(-1)
    { }
};

class BracketConvert {
public:
    BracketResult convert(int lower, int upper, double target) const
    { return BracketResult(lower, upper, target); }

    bool isValid(int v) const
    { return v > 0; }

    BracketResult invalid() const
    { return BracketResult(); }
};

class NearestResult {
public:
    int value;
    double target;

    NearestResult(int v, double t) : value(v), target(t)
    { }

    NearestResult() : value(-1), target(-1)
    { }
};

class NearestConvert {
public:
    int maximum;
    int center;

    NearestConvert() : maximum(-1), center(-1)
    { }

    NearestConvert(int m, int c) : maximum(m), center(c)
    { }

    NearestResult convert(int v, double t) const
    { return NearestResult(v, t); }

    bool isValid(int v) const
    { return v >= 0; }

    bool outsidePossible(int v, double t) const
    {
        Q_UNUSED(t);
        if (v < 0 || maximum < 0 || center < 0)
            return false;
        return qAbs(v - center) > maximum;
    }

    NearestResult invalid() const
    { return NearestResult(); }
};

class TestWavelength : public QObject {
Q_OBJECT

    bool bracketMatch(const BracketResult &input, int l, int u, double t)
    {
        return input.lower == l && input.upper == u && input.target == t;
    }

    bool nearestMatch(const NearestResult &input, int v, double t)
    {
        return input.value == v && input.target == t;
    }

private slots:

    void codes()
    {
        QCOMPARE(Wavelength::code(450), std::string("B"));
        QCOMPARE(Wavelength::code(550), std::string("G"));
        QCOMPARE(Wavelength::code(700), std::string("R"));
        QCOMPARE(Wavelength::code(950), std::string("Q"));
    }

    void bracketing()
    {
        std::multimap<double, int> map;

        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             -1, -1, -1));

        map.emplace(100, 1);
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 1, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(101, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             -1, -1, -1));

        map.emplace(200, 2);

        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 1, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             2, 2, 200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 300));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice,
                                                    false), -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(50, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 50));
        QVERIFY(bracketMatch(Wavelength::bracketing(50, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice,
                                                    false), -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(150, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 150));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 1,
                             2,
                             100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 1,
                             2,
                             200));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             1, 2, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             1, 2, 200));

        map.emplace(300, 3);

        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 1, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             2, 2, 200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             3, 3, 300));
        QVERIFY(bracketMatch(Wavelength::bracketing(50, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 50));
        QVERIFY(bracketMatch(Wavelength::bracketing(50, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice,
                                                    false), -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(400, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             2, 3, 400));
        QVERIFY(bracketMatch(Wavelength::bracketing(400, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice,
                                                    false), -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(150, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 150));
        QVERIFY(bracketMatch(Wavelength::bracketing(250, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             2, 3, 250));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 1,
                             2,
                             100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 2,
                             3,
                             200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 2,
                             3,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 2,
                             3,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 2,
                             3,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             1, 2, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             1, 3, 200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             2, 3, 300));

        map.emplace(400, 4);

        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 1, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             2, 2, 200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             3, 3, 300));
        QVERIFY(bracketMatch(Wavelength::bracketing(50, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 50));
        QVERIFY(bracketMatch(Wavelength::bracketing(50, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice,
                                                    false), -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(400, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             4, 4, 400));
        QVERIFY(bracketMatch(Wavelength::bracketing(500, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             3, 4, 500));
        QVERIFY(bracketMatch(Wavelength::bracketing(500, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice,
                                                    false), -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(150, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 150));
        QVERIFY(bracketMatch(Wavelength::bracketing(250, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             2, 3, 250));
        QVERIFY(bracketMatch(Wavelength::bracketing(350, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             3, 4, 350));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 1,
                             2,
                             100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 2,
                             3,
                             200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 3,
                             4,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(400, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 3,
                             4,
                             400));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 2,
                             3,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 2,
                             3,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(400, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 3,
                             4,
                             400));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             1, 2, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             1, 3, 200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             2, 4, 300));
        QVERIFY(bracketMatch(Wavelength::bracketing(400, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             3, 4, 400));

        map.clear();

        map.emplace(100, -1);
        map.emplace(100, 1);
        map.emplace(100, -2);

        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 1, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(101, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             -1, -1, -1));

        map.emplace(200, -3);
        map.emplace(200, 2);
        map.emplace(200, -4);

        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 1, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             2, 2, 200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 300));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice,
                                                    false), -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(50, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 50));
        QVERIFY(bracketMatch(Wavelength::bracketing(50, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice,
                                                    false), -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(150, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 150));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 1,
                             2,
                             100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 1,
                             2,
                             200));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             1, 2, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             1, 2, 200));

        map.emplace(150, -5);

        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 1, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             2, 2, 200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 300));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice,
                                                    false), -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(50, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 50));
        QVERIFY(bracketMatch(Wavelength::bracketing(50, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice,
                                                    false), -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(150, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 150));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 1,
                             2,
                             100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 1,
                             2,
                             200));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             1, 2, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             1, 2, 200));

        map.emplace(300, -6);
        map.emplace(300, -7);
        map.emplace(300, 3);
        map.emplace(300, -8);
        map.emplace(300, -9);

        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 1, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             2, 2, 200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             3, 3, 300));
        QVERIFY(bracketMatch(Wavelength::bracketing(50, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 50));
        QVERIFY(bracketMatch(Wavelength::bracketing(50, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice,
                                                    false), -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(400, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             2, 3, 400));
        QVERIFY(bracketMatch(Wavelength::bracketing(400, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice,
                                                    false), -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(150, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 150));
        QVERIFY(bracketMatch(Wavelength::bracketing(250, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             2, 3, 250));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 1,
                             2,
                             100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 2,
                             3,
                             200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 2,
                             3,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 2,
                             3,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 2,
                             3,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             1, 2, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             1, 3, 200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             2, 3, 300));

        map.emplace(400, 4);

        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 1, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             2, 2, 200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             3, 3, 300));
        QVERIFY(bracketMatch(Wavelength::bracketing(50, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 50));
        QVERIFY(bracketMatch(Wavelength::bracketing(50, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice,
                                                    false), -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(400, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             4, 4, 400));
        QVERIFY(bracketMatch(Wavelength::bracketing(500, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             3, 4, 500));
        QVERIFY(bracketMatch(Wavelength::bracketing(500, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice,
                                                    false), -1, -1, -1));
        QVERIFY(bracketMatch(Wavelength::bracketing(150, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             1, 2, 150));
        QVERIFY(bracketMatch(Wavelength::bracketing(250, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             2, 3, 250));
        QVERIFY(bracketMatch(Wavelength::bracketing(350, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExactTwice),
                             3, 4, 350));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 1,
                             2,
                             100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 2,
                             3,
                             200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 3,
                             4,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(400, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Above), 3,
                             4,
                             400));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 1,
                             2,
                             200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 2,
                             3,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 2,
                             3,
                             300));
        QVERIFY(bracketMatch(Wavelength::bracketing(400, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::Below), 3,
                             4,
                             400));
        QVERIFY(bracketMatch(Wavelength::bracketing(100, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             1, 2, 100));
        QVERIFY(bracketMatch(Wavelength::bracketing(200, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             1, 3, 200));
        QVERIFY(bracketMatch(Wavelength::bracketing(300, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             2, 4, 300));
        QVERIFY(bracketMatch(Wavelength::bracketing(400, map, BracketConvert(),
                                                    Wavelength::BracketingExactBehavior::ExcludeExact),
                             3, 4, 400));
    }

    void nearest()
    {
        std::multimap<double, int> map;

        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert()), -1,
                             -1));

        map.emplace(100, 0);
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert()), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(150, map, NearestConvert()), 0,
                             150));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert()), 0, 50));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(0, 0)), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(1, 5)), -1,
                             -1));

        map.emplace(200, 10);
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert()), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert()), 0,
                             125));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert()), 0, 50));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert()), 10,
                             200));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert()), 10,
                             225));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert()), 10,
                             175));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(5, 0)), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert(5, 0)), 0,
                             125));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert(5, 0)), 0,
                             50));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(5, 10)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert(5, 10)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert(5, 10)), -1,
                             -1));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert(5, 10)),
                             10, 200));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert(5, 10)),
                             10, 225));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert(5, 10)),
                             10, 175));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert(5, 20)),
                             -1, -1));

        map.emplace(300, 20);
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert()), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert()), 0,
                             125));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert()), 0, 50));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert()), 10,
                             200));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert()), 10,
                             225));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert()), 10,
                             175));
        QVERIFY(nearestMatch(Wavelength::nearest(300, map, NearestConvert()), 20,
                             300));
        QVERIFY(nearestMatch(Wavelength::nearest(325, map, NearestConvert()), 20,
                             325));
        QVERIFY(nearestMatch(Wavelength::nearest(275, map, NearestConvert()), 20,
                             275));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(5, 0)), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert(5, 0)), 0,
                             125));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert(5, 0)), 0,
                             50));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(5, 10)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert(5, 10)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert(5, 10)), -1,
                             -1));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert(5, 10)),
                             10, 200));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert(5, 10)),
                             10, 225));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert(5, 10)),
                             10, 175));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(300, map, NearestConvert(5, 20)),
                             20, 300));
        QVERIFY(nearestMatch(Wavelength::nearest(325, map, NearestConvert(5, 20)),
                             20, 325));
        QVERIFY(nearestMatch(Wavelength::nearest(275, map, NearestConvert(5, 20)),
                             20, 275));
        QVERIFY(nearestMatch(Wavelength::nearest(300, map, NearestConvert(5, 30)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(325, map, NearestConvert(5, 30)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(275, map, NearestConvert(5, 30)),
                             -1, -1));

        map.emplace(400, 30);
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert()), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert()), 0,
                             125));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert()), 0, 50));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert()), 10,
                             200));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert()), 10,
                             225));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert()), 10,
                             175));
        QVERIFY(nearestMatch(Wavelength::nearest(300, map, NearestConvert()), 20,
                             300));
        QVERIFY(nearestMatch(Wavelength::nearest(325, map, NearestConvert()), 20,
                             325));
        QVERIFY(nearestMatch(Wavelength::nearest(275, map, NearestConvert()), 20,
                             275));
        QVERIFY(nearestMatch(Wavelength::nearest(400, map, NearestConvert()), 30,
                             400));
        QVERIFY(nearestMatch(Wavelength::nearest(425, map, NearestConvert()), 30,
                             425));
        QVERIFY(nearestMatch(Wavelength::nearest(375, map, NearestConvert()), 30,
                             375));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(5, 0)), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert(5, 0)), 0,
                             125));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert(5, 0)), 0,
                             50));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(5, 10)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert(5, 10)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert(5, 10)), -1,
                             -1));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert(5, 10)),
                             10, 200));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert(5, 10)),
                             10, 225));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert(5, 10)),
                             10, 175));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(300, map, NearestConvert(5, 20)),
                             20, 300));
        QVERIFY(nearestMatch(Wavelength::nearest(325, map, NearestConvert(5, 20)),
                             20, 325));
        QVERIFY(nearestMatch(Wavelength::nearest(275, map, NearestConvert(5, 20)),
                             20, 275));
        QVERIFY(nearestMatch(Wavelength::nearest(300, map, NearestConvert(5, 30)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(325, map, NearestConvert(5, 30)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(275, map, NearestConvert(5, 30)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(400, map, NearestConvert(5, 30)),
                             30, 400));
        QVERIFY(nearestMatch(Wavelength::nearest(425, map, NearestConvert(5, 30)),
                             30, 425));
        QVERIFY(nearestMatch(Wavelength::nearest(375, map, NearestConvert(5, 30)),
                             30, 375));
        QVERIFY(nearestMatch(Wavelength::nearest(400, map, NearestConvert(5, 40)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(425, map, NearestConvert(5, 40)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(375, map, NearestConvert(5, 40)),
                             -1, -1));

        map.clear();

        map.emplace(100, -1);
        map.emplace(100, 0);
        map.emplace(100, -2);

        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert()), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(150, map, NearestConvert()), 0,
                             150));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert()), 0, 50));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(0, 0)), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(1, 5)), -1,
                             -1));

        map.emplace(200, -3);
        map.emplace(200, 10);
        map.emplace(200, -4);

        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert()), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert()), 0,
                             125));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert()), 0, 50));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert()), 10,
                             200));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert()), 10,
                             225));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert()), 10,
                             175));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(5, 0)), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert(5, 0)), 0,
                             125));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert(5, 0)), 0,
                             50));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(5, 10)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert(5, 10)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert(5, 10)), -1,
                             -1));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert(5, 10)),
                             10, 200));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert(5, 10)),
                             10, 225));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert(5, 10)),
                             10, 175));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert(5, 20)),
                             -1, -1));

        map.emplace(300, -3);
        map.emplace(300, -4);
        map.emplace(300, 20);
        map.emplace(300, -5);
        map.emplace(300, -6);

        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert()), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert()), 0,
                             125));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert()), 0, 50));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert()), 10,
                             200));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert()), 10,
                             225));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert()), 10,
                             175));
        QVERIFY(nearestMatch(Wavelength::nearest(300, map, NearestConvert()), 20,
                             300));
        QVERIFY(nearestMatch(Wavelength::nearest(325, map, NearestConvert()), 20,
                             325));
        QVERIFY(nearestMatch(Wavelength::nearest(275, map, NearestConvert()), 20,
                             275));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(5, 0)), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert(5, 0)), 0,
                             125));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert(5, 0)), 0,
                             50));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(5, 10)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert(5, 10)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert(5, 10)), -1,
                             -1));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert(5, 10)),
                             10, 200));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert(5, 10)),
                             10, 225));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert(5, 10)),
                             10, 175));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(300, map, NearestConvert(5, 20)),
                             20, 300));
        QVERIFY(nearestMatch(Wavelength::nearest(325, map, NearestConvert(5, 20)),
                             20, 325));
        QVERIFY(nearestMatch(Wavelength::nearest(275, map, NearestConvert(5, 20)),
                             20, 275));
        QVERIFY(nearestMatch(Wavelength::nearest(300, map, NearestConvert(5, 30)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(325, map, NearestConvert(5, 30)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(275, map, NearestConvert(5, 30)),
                             -1, -1));

        map.emplace(400, -7);
        map.emplace(400, 30);
        map.emplace(400, -8);

        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert()), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert()), 0,
                             125));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert()), 0, 50));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert()), 10,
                             200));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert()), 10,
                             225));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert()), 10,
                             175));
        QVERIFY(nearestMatch(Wavelength::nearest(300, map, NearestConvert()), 20,
                             300));
        QVERIFY(nearestMatch(Wavelength::nearest(325, map, NearestConvert()), 20,
                             325));
        QVERIFY(nearestMatch(Wavelength::nearest(275, map, NearestConvert()), 20,
                             275));
        QVERIFY(nearestMatch(Wavelength::nearest(400, map, NearestConvert()), 30,
                             400));
        QVERIFY(nearestMatch(Wavelength::nearest(425, map, NearestConvert()), 30,
                             425));
        QVERIFY(nearestMatch(Wavelength::nearest(375, map, NearestConvert()), 30,
                             375));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(5, 0)), 0,
                             100));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert(5, 0)), 0,
                             125));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert(5, 0)), 0,
                             50));
        QVERIFY(nearestMatch(Wavelength::nearest(100, map, NearestConvert(5, 10)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(125, map, NearestConvert(5, 10)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(50, map, NearestConvert(5, 10)), -1,
                             -1));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert(5, 10)),
                             10, 200));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert(5, 10)),
                             10, 225));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert(5, 10)),
                             10, 175));
        QVERIFY(nearestMatch(Wavelength::nearest(200, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(225, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(175, map, NearestConvert(5, 20)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(300, map, NearestConvert(5, 20)),
                             20, 300));
        QVERIFY(nearestMatch(Wavelength::nearest(325, map, NearestConvert(5, 20)),
                             20, 325));
        QVERIFY(nearestMatch(Wavelength::nearest(275, map, NearestConvert(5, 20)),
                             20, 275));
        QVERIFY(nearestMatch(Wavelength::nearest(300, map, NearestConvert(5, 30)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(325, map, NearestConvert(5, 30)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(275, map, NearestConvert(5, 30)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(400, map, NearestConvert(5, 30)),
                             30, 400));
        QVERIFY(nearestMatch(Wavelength::nearest(425, map, NearestConvert(5, 30)),
                             30, 425));
        QVERIFY(nearestMatch(Wavelength::nearest(375, map, NearestConvert(5, 30)),
                             30, 375));
        QVERIFY(nearestMatch(Wavelength::nearest(400, map, NearestConvert(5, 40)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(425, map, NearestConvert(5, 40)),
                             -1, -1));
        QVERIFY(nearestMatch(Wavelength::nearest(375, map, NearestConvert(5, 40)),
                             -1, -1));
    }
};

QTEST_APPLESS_MAIN(TestWavelength)

#include "wavelength.moc"
