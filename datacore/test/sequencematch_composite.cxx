/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtSql>
#include <QString>
#include <datacore/sequencematch.hxx>

#include "datacore/segment.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/archive/selection.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Data;


class TestSequenceMatchComposite : public QObject {
Q_OBJECT
private slots:

    void basic()
    {
        SequenceMatch::OrderedLookup e;
        SequenceSegment s;
        SequenceName u("amf", "clean", "Q_A11");

        s.setValue(u, Variant::Root(2.0));

        QVERIFY(!e.valid());
        QVERIFY(!e.matches(u));
        QVERIFY(!e.registerInput(u));
        QVERIFY(!e.lookup(s).exists());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << e;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> e;
            }
            QVERIFY(!e.matches(u));
            QVERIFY(!e.lookup(s).exists());
            e = SequenceMatch::OrderedLookup(e);
            QVERIFY(!e.matches(u));
            QVERIFY(!e.lookup(s).exists());
        }
        e = SequenceMatch::OrderedLookup();
        QVERIFY(!e.matches(u));
        QVERIFY(!e.registerInput(u));
        QVERIFY(!e.lookup(s).exists());

        e.append(u);
        QVERIFY(e.matches(u));
        QVERIFY(e.reduce() == u);
        QVERIFY(e.registerInput(u));
        QCOMPARE(e.lookup(s).toDouble(), 2.0);
        QVERIFY(e.knownInputs().count(u) != 0);
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << e;
            }
            e = SequenceMatch::OrderedLookup();
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> e;
            }
            QVERIFY(e.matches(u));
            QCOMPARE(e.lookup(s).toDouble(), 2.0);
            e = SequenceMatch::OrderedLookup(e);
            QCOMPARE(e.lookup(s).toDouble(), 2.0);
        }
        e = SequenceMatch::OrderedLookup(u);
        QVERIFY(e.registerInput(u));
        QCOMPARE(e.lookup(s).toDouble(), 2.0);

        e = SequenceMatch::OrderedLookup();
        e.append(".*", "cle[a]n", "Q_A11");
        QVERIFY(e.matches(u));
        QVERIFY(e.registerInput(u));
        QCOMPARE(e.lookup(s).toDouble(), 2.0);
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << e;
            }
            e = SequenceMatch::OrderedLookup();
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> e;
            }
            QVERIFY(e.matches(u));
            QCOMPARE(e.lookup(s).toDouble(), 2.0);
            e = SequenceMatch::OrderedLookup(e);
            QCOMPARE(e.lookup(s).toDouble(), 2.0);
        }
        e = SequenceMatch::OrderedLookup(SequenceMatch::Element(".*", "cle[a]n", "Q_A11"));
        QVERIFY(e.registerInput(u));
        QCOMPARE(e.lookup(s).toDouble(), 2.0);

        e = SequenceMatch::OrderedLookup();
        e.append(".*", "raw", "Q_A11");
        e.append("amf", "clean", "Q_A11");
        QVERIFY(e.matches(u));
        QVERIFY(e.registerInput(u));
        QCOMPARE(e.lookup(s).toDouble(), 2.0);
        s.setValue(SequenceName("sgp", "raw", "Q_A11"), Variant::Root(3.0));
        QVERIFY(e.matches(SequenceName("sgp", "raw", "Q_A11")));
        QVERIFY(!e.matches(SequenceName("sgp", "raw", "Q_A12")));
        QVERIFY(e.registerInput(SequenceName("sgp", "raw", "Q_A11")));
        QCOMPARE(e.lookup(s).toDouble(), 3.0);


        Archive::Selection::List selections = e.toArchiveSelections();
        QCOMPARE(((int) selections.size()), ((int) 2));
        QCOMPARE(selections[0].stations, (Archive::Selection::Match{".*"}));
        QCOMPARE(selections[0].archives, (Archive::Selection::Match{"raw"}));
        QCOMPARE(selections[0].variables, (Archive::Selection::Match{"Q_A11"}));
        QCOMPARE(selections[1].stations, (Archive::Selection::Match{"amf"}));
        QCOMPARE(selections[1].archives, (Archive::Selection::Match{"clean"}));
        QCOMPARE(selections[1].variables, (Archive::Selection::Match{"Q_A11"}));

        e = SequenceMatch::OrderedLookup(std::vector<SequenceMatch::Element>{
                u, SequenceName("sgp", "raw", "Q_A11")
        });
        QVERIFY(e.matches(u));
        QVERIFY(e.registerInput(u));
        QCOMPARE(e.lookup(s).toDouble(), 2.0);
        QVERIFY(e.matches(SequenceName("sgp", "raw", "Q_A11")));
        QCOMPARE(e.lookup(s).toDouble(), 2.0);

        Variant::Write v = Variant::Write::empty();
        v.setString("Q_A11");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.setString("::Q_A11");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.setString("amf::Q_A11");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.setString("sgp::Q_A11");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(!e.matches(u));
        v.setString("amf:clean:Q_A11");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.setString("amf:clean:Q_A11:pm1");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(!e.matches(u));
        v.setString("amf:clean:Q_A11:!pm1");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.setString("amf:clean:Q_A11:=pm1");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(!e.matches(u));
        QVERIFY(e.matches(u.withFlavor("pm1")));

        v.hash("Variable").setString("Q_A11");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.hash("Station").setString("sgp");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(!e.matches(u));
        v.hash("Station").setString("amf");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.hash("Archive").setString("clean");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.hash("LacksFlavors").setString("pm1");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.hash("LacksFlavors").array(0).setString("pm1");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.hash("HasFlavors").setString("pm1");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(!e.matches(u));
        v.hash("HasFlavors").array(0).setString("pm1");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(!e.matches(u));
        v.hash("Flavors").setString("");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.hash("Flavors").array(0).setString("");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.hash("Flavors").setString("pm1");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(!e.matches(u));
        v.hash("Flavors").array(0).setString("pm1");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(!e.matches(u));

        v.array(0).setString("Q_A11");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.array(0).hash("Variable").setString("Q_A11");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.array(0).hash("Variable").setString("Q_A21");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(!e.matches(u));
        v.array(1).hash("Variable").setString("Q_A11");
        v.array(1).hash("Station").setString("amf");
        v.array(1).hash("Archive").setString("clean");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.array(1).setString("amf:clean:Q_A11");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));
        v.array(1).setString("amf:clean:Q_A11:pm1");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(!e.matches(u));
        v.array(1).setString("amf:clean:Q_A11:!pm1");
        e = SequenceMatch::OrderedLookup(v);
        QVERIFY(e.matches(u));

        e = SequenceMatch::OrderedLookup();
        e.append("", "", "", QStringList("stddev") << "pm.+", QStringList("count") << "(c)over");
        QVERIFY(e.matches(SequenceName("brw", "raw", "BsG_S11", {"pm1", "stddev"})));
        QVERIFY(e.matches(SequenceName("brw", "raw", "BsG_S11", {"pm10", "stddev"})));
        QVERIFY(!e.matches(SequenceName("brw", "raw", "BsG_S11", {"pm10"})));
        QVERIFY(!e.matches(SequenceName("brw", "raw", "BsG_S11", {"stddev"})));
        QVERIFY(!e.matches(SequenceName("brw", "raw", "BsG_S11", {"count", "cover"})));
        QVERIFY(!e.matches(SequenceName("brw", "raw", "BsG_S11", {"count"})));
        QVERIFY(!e.matches(SequenceName("brw", "raw", "BsG_S11", {"cover"})));
        QVERIFY(!e.matches(SequenceName("brw", "raw", "BsG_S11", {"pm10", "count"})));
        QVERIFY(!e.matches(SequenceName("brw", "raw", "BsG_S11", {"stddev", "cover"})));
        QVERIFY(!e.matches(SequenceName("brw", "raw", "BsG_S11", {"pm1", "stddev", "count"})));
        QVERIFY(!e.matches(SequenceName("brw", "raw", "BsG_S11", {"pm10", "stddev", "cover"})));
    }

    void registerExpected()
    {
        SequenceMatch::OrderedLookup e;
        e.append("", "", "P_S11", SequenceName::Flavors());
        e.append("[s]gp", "", "P_S12", SequenceName::Flavors());
        e.append("", "raw", "P_S13", SequenceName::Flavors());
        e.append("", "raw", "T_S11", QStringList("pm1"), QStringList());
        e.append("", "raw", "T_S12", QStringList(), QStringList("pm10"));

        e.registerExpectedFlavorless("", "raw", "P_S13");
        QVERIFY(e.knownInputs().empty());
        e.registerExpectedFlavorless("brw", "raw", "");
        QCOMPARE((int)e.knownInputs().size(), 2);
        QVERIFY(e.knownInputs().count(SequenceName("brw", "raw", "P_S13")) != 0);
        QVERIFY(e.knownInputs().count(SequenceName("brw", "raw", "P_S11")) != 0);

        e.registerExpectedFlavorless("sgp", "clean", "");
        QCOMPARE((int)e.knownInputs().size(), 4);
        QVERIFY(e.knownInputs().count(SequenceName("sgp", "clean", "P_S12")) != 0);
        QVERIFY(e.knownInputs().count(SequenceName("sgp", "clean", "P_S11")) != 0);

        e.registerExpectedFlavorless("sgp", "raw", "P_S12");
        QCOMPARE((int)e.knownInputs().size(), 5);
        QVERIFY(e.knownInputs().count(SequenceName("sgp", "raw", "P_S12")) != 0);

        e.registerExpected("sgp", "raw", "T_S11", SequenceName::Flavors{"pm10"});
        QCOMPARE((int)e.knownInputs().size(), 5);
        e.registerExpected("sgp", "raw", "T_S11", SequenceName::Flavors{"pm1"});
        QCOMPARE((int)e.knownInputs().size(), 6);
        QVERIFY(e.knownInputs()
                 .count(SequenceName("sgp", "raw", "T_S11", {"pm1"})) != 0);

        e.registerExpected("sgp", "raw", "T_S12", SequenceName::Flavors{"pm10"});
        QCOMPARE((int)e.knownInputs().size(), 6);
        e.registerExpected("sgp", "raw", "T_S12", SequenceName::Flavors{"stddev"});
        QCOMPARE((int)e.knownInputs().size(), 7);
        QVERIFY(e.knownInputs()
                 .count(SequenceName("sgp", "raw", "T_S12", {"stddev"})) != 0);
    }
};

QTEST_APPLESS_MAIN(TestSequenceMatchComposite)

#include "sequencematch_composite.moc"
