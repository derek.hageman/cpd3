/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtSql>
#include <QString>

#include "datacore/dynamicprimitive.hxx"
#include "datacore/variant/root.hxx"
#include "core/number.hxx"
#include "core/timeutils.hxx"
#include "core/timeparse.hxx"

using namespace CPD3;
using namespace CPD3::Data;

Q_DECLARE_METATYPE(QList<double>);

class TestDynamicPrimitive : public QObject {
Q_OBJECT
private slots:

    void stringDefault()
    {
        DynamicPrimitive<QString> *i = new DynamicPrimitive<QString>::Constant;
        QCOMPARE(i->get(1291739399.0, 1291739399.0), QString());
        QCOMPARE(i->get(1291739399.0), QString());
        QCOMPARE(i->getConst(1291739399.0, 1291739399.0), QString());
        QCOMPARE(i->getConst(1291739399.0), QString());

        DynamicPrimitive<QString> *j = i->clone();
        delete i;
        QCOMPARE(j->get(1291739399.0), QString());
        DynamicPrimitive<QString> *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->get(1291739399.0), QString());
        delete k;
    }

    void stringConstant()
    {
        DynamicPrimitive<QString> *i = new DynamicPrimitive<QString>::Constant(QString("Asdf"));
        QCOMPARE(i->get(1291739399.0, 1291739399.0), QString("Asdf"));
        QCOMPARE(i->get(1291739399.0), QString("Asdf"));
        QCOMPARE(i->getConst(1291739399.0, 1291739399.0), QString("Asdf"));
        QCOMPARE(i->getConst(1291739399.0), QString("Asdf"));

        DynamicPrimitive<QString> *j = i->clone();
        delete i;
        QCOMPARE(j->get(1291739399.0), QString("Asdf"));
        DynamicPrimitive<QString> *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->get(1291739399.0), QString("Asdf"));
        delete k;
    }

    void stringVariable()
    {
        DynamicPrimitive<QString>::Variable *i = new DynamicPrimitive<QString>::Variable;

        i->set(QString("1"));
        i->set(QString("2"), 1000, 2000);
        i->set(QString("3"), 2000, 3000);
        i->set(QString("3"), 3000, 4000);
        i->set(QString("4"), 5000, FP::undefined());
        i->simplify();

        QCOMPARE(i->get(100.0, 200.0), QString("1"));
        QCOMPARE(i->get(100.0), QString("1"));
        QCOMPARE(i->getConst(3000.0, 3100.0), QString("3"));
        QCOMPARE(i->getConst(3150.0), QString("3"));

        DynamicPrimitive<QString> *check = i->compressedType(2100.0, 2300.0);
        QVERIFY(check != NULL);
        QCOMPARE(check->get(5200.0), QString("3"));
        delete check;

        DynamicPrimitive<QString> *j = i->clone();
        delete i;
        QCOMPARE(j->get(2000.0), QString("3"));
        DynamicPrimitive<QString> *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->get(2100.0), QString("3"));
        delete k;
    }

    void stringConfiguration()
    {
        ValueSegment::Transfer config;
        config.emplace_back(1000, 2000, Variant::Root("1"));
        config.emplace_back(2000, 3000, Variant::Root("2"));
        config.emplace_back(3000, 4000, Variant::Root("2"));
        config.emplace_back(5000, 6000, Variant::Root("3"));

        DynamicPrimitive<QString> *i = DynamicStringOption::fromConfiguration(config);

        QCOMPARE(i->get(100.0, 200.0), QString());
        QCOMPARE(i->get(100.0), QString());
        QCOMPARE(i->getConst(3000.0, 3100.0), QString("2"));
        QCOMPARE(i->getConst(3150.0), QString("2"));

        DynamicPrimitive<QString> *j = i->clone();
        delete i;
        QCOMPARE(j->get(2000.0), QString("2"));
        DynamicPrimitive<QString> *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->get(5100.0), QString("3"));
        delete k;
    }

    void boolConstant()
    {
        DynamicPrimitive<bool> *i = new DynamicPrimitive<bool>::Constant(true);
        QCOMPARE(i->get(1291739399.0, 1291739399.0), true);
        QCOMPARE(i->get(1291739399.0), true);
        QCOMPARE(i->getConst(1291739399.0, 1291739399.0), true);
        QCOMPARE(i->getConst(1291739399.0), true);

        DynamicPrimitive<bool> *j = i->clone();
        delete i;
        QCOMPARE(j->get(1291739399.0), true);
        DynamicPrimitive<bool> *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->get(1291739399.0), true);
        delete k;
    }

    void boolVariable()
    {
        DynamicPrimitive<bool>::Variable *i = new DynamicPrimitive<bool>::Variable;

        i->set(false);
        i->set(true, 1000, 2000);
        i->set(false, 2000, 3000);
        i->set(false, 3000, 4000);
        i->set(true, 5000, FP::undefined());
        i->simplify();

        QCOMPARE(i->get(100.0, 200.0), false);
        QCOMPARE(i->get(100.0), false);
        QCOMPARE(i->getConst(3000.0, 3100.0), false);
        QCOMPARE(i->getConst(3150.0), false);

        DynamicPrimitive<bool> *check = i->compressedType(2100.0, 2300.0);
        QVERIFY(check != NULL);
        QCOMPARE(check->get(5200.0), false);
        delete check;

        DynamicPrimitive<bool> *j = i->clone();
        delete i;
        QCOMPARE(j->get(2000.0), false);
        DynamicPrimitive<bool> *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->get(2100.0), false);
        delete k;
    }

    void boolConfiguration()
    {
        ValueSegment::Transfer config;
        config.emplace_back(1000, 2000, Variant::Root(true));
        config.emplace_back(2000, 3000, Variant::Root(false));
        config.emplace_back(3000, 4000, Variant::Root(false));
        config.emplace_back(5000, 6000, Variant::Root(true));

        DynamicPrimitive<bool> *i = DynamicBoolOption::fromConfiguration(config);

        QCOMPARE(i->get(100.0, 200.0), false);
        QCOMPARE(i->get(100.0), false);
        QCOMPARE(i->getConst(3000.0, 3100.0), false);
        QCOMPARE(i->getConst(3150.0), false);

        DynamicPrimitive<bool> *j = i->clone();
        delete i;
        QCOMPARE(j->get(2000.0), false);
        DynamicPrimitive<bool> *k;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << j;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> k;
            }
        }
        delete j;
        QCOMPARE(k->get(5100.0), true);
        delete k;
    }
};

QTEST_APPLESS_MAIN(TestDynamicPrimitive)

#include "dynamicprimitive.moc"
