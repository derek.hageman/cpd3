/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QHash>
#include <QList>

#include "datacore/processingstage.hxx"
#include "datacore/stream.hxx"
#include "core/range.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class GeneralFilterTestImplementation : public ProcessingStageThread {
public:
    bool finished;
    int n;

    GeneralFilterTestImplementation() : finished(false), n(0)
    { }

protected:
    virtual void process(SequenceValue::Transfer &&incoming)
    {
        for (int i = 0, max = incoming.size(); i < max; i++) {
            incoming[i].write() = 1.0 + (++n) * 0.1;
        }
        egress->incomingData(incoming);
    }

    virtual void finish()
    {
        finished = true;
        ProcessingStageThread::finish();
    }
};

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestProcessingStage : public QObject {
Q_OBJECT

private slots:

    void singleData()
    {
        GeneralFilterTestImplementation filter;
        filter.start();
        StreamSink::Buffer results;
        filter.setEgress(&results);

        SequenceName u1("brw", "raw", "T_S11");

        filter.incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        filter.incomingData(SequenceValue(u1, Variant::Root(2.0), 2.0, 3.0));
        filter.incomingData(SequenceValue(u1, Variant::Root(3.0), 3.0, 4.0));
        filter.endData();
        QVERIFY(filter.wait(30));
        QVERIFY(filter.finished);

        QVERIFY(results.ended());
        QCOMPARE((int) results.values().size(), 3);
        QCOMPARE(results.values()[0], SequenceValue(u1, Variant::Root(1.1), 1.0, 2.0));
        QCOMPARE(results.values()[1], SequenceValue(u1, Variant::Root(1.2), 2.0, 3.0));
        QCOMPARE(results.values()[2], SequenceValue(u1, Variant::Root(1.3), 3.0, 4.0));
    }

    void singleDataWait()
    {
        GeneralFilterTestImplementation filter;
        filter.start();
        StreamSink::Buffer results;
        filter.setEgress(&results);

        SequenceName u1("brw", "raw", "T_S11");

        filter.incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        QTest::qSleep(50);
        filter.incomingData(SequenceValue(u1, Variant::Root(2.0), 2.0, 3.0));
        QTest::qSleep(50);
        filter.incomingData(SequenceValue(u1, Variant::Root(3.0), 3.0, 4.0));
        QTest::qSleep(50);
        filter.endData();
        QVERIFY(filter.wait(30));
        QVERIFY(filter.finished);

        QVERIFY(results.ended());
        QCOMPARE((int) results.values().size(), 3);
        QCOMPARE(results.values()[0], SequenceValue(u1, Variant::Root(1.1), 1.0, 2.0));
        QCOMPARE(results.values()[1], SequenceValue(u1, Variant::Root(1.2), 2.0, 3.0));
        QCOMPARE(results.values()[2], SequenceValue(u1, Variant::Root(1.3), 3.0, 4.0));
    }

    void signalTerminate()
    {
        GeneralFilterTestImplementation filter;
        filter.start();
        StreamSink::Buffer results;
        filter.setEgress(&results);

        SequenceName u1("brw", "raw", "T_S11");

        filter.incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        filter.incomingData(SequenceValue(u1, Variant::Root(1.1), 2.0, 3.0));
        filter.signalTerminate();
        QVERIFY(filter.wait());
        QVERIFY(results.ended());
    }

    void signalTerminateWait()
    {
        GeneralFilterTestImplementation filter;
        filter.start();
        StreamSink::Buffer results;
        filter.setEgress(&results);

        SequenceName u1("brw", "raw", "T_S11");

        filter.incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        QTest::qSleep(50);
        filter.incomingData(SequenceValue(u1, Variant::Root(1.1), 2.0, 3.0));
        QTest::qSleep(50);
        filter.signalTerminate();
        QVERIFY(filter.wait(30));
        QVERIFY(results.ended());
    }
};

QTEST_MAIN(TestProcessingStage)

#include "processingstage.moc"
