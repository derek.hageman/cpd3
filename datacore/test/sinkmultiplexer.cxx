/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <QTest>
#include <QByteArray>


#include "datacore/sinkmultiplexer.hxx"
#include "datacore/stream.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"

using namespace CPD3;
using namespace CPD3::Data;

static bool compareLists(const SequenceValue::Transfer &a, const SequenceValue::Transfer &b)
{
    if (a.size() != b.size())
        return false;

    for (std::size_t i = 0; i < a.size();) {
        std::size_t end = i + 1;
        for (; end < a.size() && FP::equal(a.at(end).getStart(), a.at(i).getStart()); ++end) { }

        std::size_t start = i;
        for (; i < end; i++) {
            bool hit = false;
            for (std::size_t j = start; j < end; j++) {
                if (SequenceValue::ValueEqual()(a.at(i), b.at(j))) {
                    hit = true;
                    break;
                }
            }
            if (!hit)
                return false;
        }

    }
    return true;
}

static bool sortResultList(const SequenceValue &a, const SequenceValue &b)
{ return Range::compareStart(a.getStart(), b.getStart()) < 0; }

class TestSinkMultiplexer : public QObject {
Q_OBJECT

    static SequenceValue::Transfer slice(const SequenceValue::Transfer &input,
                                         std::size_t begin,
                                         std::size_t count)
    {
        SequenceValue::Transfer result;
        SequenceValue::Transfer::const_iterator end;

        if (begin + count >= input.size())
            end = input.cend();
        else
            end = input.cbegin() + begin + count;

        std::copy(input.cbegin() + begin, end, Util::back_emplacer(result));
        return result;
    }

private slots:

    void simple()
    {
        SequenceValue::Transfer a;
        SequenceValue::Transfer b;
        SequenceValue::Transfer c;

        a.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.01), FP::undefined(),
                       2.0);
        a.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.02), FP::undefined(),
                       3.0);
        a.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.03), 1.0, 2.0);

        b.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.11), FP::undefined(),
                       2.0);
        b.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.12), 1.0, 3.0);
        b.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.13), 2.0, 3.0);

        c.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.21), 1.0, 2.0);
        c.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.22), 2.0, 3.0);
        c.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.23), 3.0, 4.0);

        SinkMultiplexer::Sink *ia = NULL;
        SinkMultiplexer::Sink *ib = NULL;
        SinkMultiplexer::Sink *ic = NULL;
        StreamSink::Buffer ingress;

        for (std::size_t sA = 0; sA < a.size(); sA++) {
            for (std::size_t eA = sA; eA <= a.size(); eA++) {
                if (sA != 0 && eA == sA)
                    continue;
                SequenceValue::Transfer selectedA = slice(a, sA, (a.size() - sA) - eA);

                for (std::size_t sB = 0; sB < b.size(); sB++) {
                    for (std::size_t eB = sB; eB <= b.size(); eB++) {
                        if (sB != 0 && eB == sB)
                            continue;
                        SequenceValue::Transfer selectedB = slice(b, sB, (b.size() - sB) - eB);

                        for (std::size_t sC = 0; sC < c.size(); sC++) {
                            for (std::size_t eC = sC; eC <= c.size(); eC++) {
                                if (sC != 0 && eC == sC)
                                    continue;
                                SequenceValue::Transfer
                                        selectedC = slice(c, sC, (c.size() - sC) - eC);

                                SequenceValue::Transfer expected;
                                Util::append(selectedA, expected);
                                Util::append(selectedB, expected);
                                Util::append(selectedC, expected);
                                std::sort(expected.begin(), expected.end(), sortResultList);

                                SinkMultiplexer *mux = new SinkMultiplexer;
                                mux->start();
                                ingress.reset();
                                mux->setEgress(&ingress);
                                ia = mux->createSink();
                                ib = mux->createSink();
                                ic = mux->createSink();
                                mux->sinkCreationComplete();
                                ia->incomingData(selectedA);
                                ia->endData();
                                ib->incomingData(selectedB);
                                ib->endData();
                                ic->incomingData(selectedC);
                                ic->endData();
                                QVERIFY(mux->wait(30));
                                delete mux;
                                QVERIFY(ingress.ended());
                                QVERIFY(compareLists(ingress.values(), expected));

                                mux = new SinkMultiplexer;
                                mux->start();
                                ingress.reset();
                                mux->setEgress(&ingress);
                                ia = mux->createSink();
                                ib = mux->createSink();
                                ic = mux->createSink();
                                mux->sinkCreationComplete();
                                ia->incomingData(selectedA);
                                QTest::qSleep(50);
                                ia->endData();
                                QTest::qSleep(50);
                                ib->incomingData(selectedB);
                                QTest::qSleep(50);
                                ib->endData();
                                QTest::qSleep(50);
                                ic->incomingData(selectedC);
                                QTest::qSleep(50);
                                ic->endData();
                                QVERIFY(mux->wait(30));
                                delete mux;
                                QVERIFY(ingress.ended());
                                QVERIFY(compareLists(ingress.values(), expected));

                                mux = new SinkMultiplexer;
                                mux->start();
                                ingress.reset();
                                mux->setEgress(&ingress);
                                ia = mux->createSink();
                                ib = mux->createSink();
                                ic = mux->createSink();
                                ia->incomingData(selectedA);
                                ia->endData();
                                ib->incomingData(selectedB);
                                ib->endData();
                                ic->incomingData(selectedC);
                                ic->endData();
                                mux->sinkCreationComplete();
                                QVERIFY(mux->wait(30));
                                delete mux;
                                QVERIFY(ingress.ended());
                                QVERIFY(compareLists(ingress.values(), expected));

                                mux = new SinkMultiplexer;
                                mux->start();
                                ingress.reset();
                                mux->setEgress(&ingress);
                                ia = mux->createSink();
                                ib = mux->createSink();
                                ic = mux->createSink();
                                mux->sinkCreationComplete();
                                for (const auto &i : selectedB) {
                                    ib->incomingData(i);
                                }
                                for (const auto &i : selectedC) {
                                    ic->incomingData(i);
                                }
                                for (const auto &i : selectedA) {
                                    ia->incomingData(i);
                                }
                                ia->endData();
                                ic->endData();
                                ib->endData();
                                QVERIFY(mux->wait(30));
                                delete mux;
                                QVERIFY(ingress.ended());
                                QVERIFY(compareLists(ingress.values(), expected));


                                mux = new SinkMultiplexer;
                                mux->start();
                                ingress.reset();
                                mux->setEgress(&ingress);
                                ia = mux->createSink();
                                ib = mux->createSink();
                                ic = mux->createSink();
                                mux->sinkCreationComplete();
                                for (const auto &i : selectedC) {
                                    QTest::qSleep(50);
                                    ic->incomingData(i);
                                }
                                for (const auto &i : selectedA) {
                                    QTest::qSleep(50);
                                    ia->incomingData(i);
                                }
                                for (const auto &i : selectedB) {
                                    QTest::qSleep(50);
                                    ib->incomingData(i);
                                }
                                ia->endData();
                                ic->endData();
                                ib->endData();
                                QVERIFY(mux->wait(30));
                                delete mux;
                                QVERIFY(ingress.ended());
                                QVERIFY(compareLists(ingress.values(), expected));

                                std::size_t max = std::max(selectedA.size(),
                                                           std::max(selectedB.size(),
                                                                    selectedC.size()));

                                mux = new SinkMultiplexer;
                                mux->start();
                                ingress.reset();
                                mux->setEgress(&ingress);
                                ia = mux->createSink();
                                ib = mux->createSink();
                                ic = mux->createSink();
                                mux->sinkCreationComplete();
                                for (std::size_t i = 0; i < max; i++) {
                                    if (i < selectedA.size()) {
                                        ia->incomingData(selectedA.at(i));
                                    } else if (i == selectedA.size()) {
                                        ia->endData();
                                    }
                                    if (i < selectedC.size()) {
                                        ic->incomingData(selectedC.at(i));
                                    } else if (i == selectedC.size()) {
                                        ic->endData();
                                    }
                                    if (i < selectedB.size()) {
                                        ib->incomingData(selectedB.at(i));
                                    } else if (i == selectedB.size()) {
                                        ib->endData();
                                    }
                                }
                                if (selectedA.size() == max)
                                    ia->endData();
                                if (selectedB.size() == max)
                                    ib->endData();
                                if (selectedC.size() == max)
                                    ic->endData();
                                QVERIFY(mux->wait(30));
                                delete mux;
                                QVERIFY(ingress.ended());
                                QVERIFY(compareLists(ingress.values(), expected));


                                mux = new SinkMultiplexer;
                                mux->start();
                                ingress.reset();
                                mux->setEgress(&ingress);
                                ia = mux->createSink();
                                ic = mux->createSink();
                                ib = mux->createSink();
                                mux->sinkCreationComplete();
                                for (std::size_t i = 0; i < max; i++) {
                                    if (i < selectedA.size()) {
                                        QTest::qSleep(50);
                                        ia->incomingData(selectedA.at(i));
                                    } else if (i == selectedA.size()) {
                                        QTest::qSleep(50);
                                        ia->endData();
                                    }
                                    if (i < selectedC.size()) {
                                        QTest::qSleep(50);
                                        ic->incomingData(selectedC.at(i));
                                    } else if (i == selectedC.size()) {
                                        QTest::qSleep(50);
                                        ic->endData();
                                    }
                                    if (i < selectedB.size()) {
                                        QTest::qSleep(50);
                                        ib->incomingData(selectedB.at(i));
                                    } else if (i == selectedB.size()) {
                                        QTest::qSleep(50);
                                        ib->endData();
                                    }
                                }
                                if (selectedA.size() == max)
                                    ia->endData();
                                if (selectedB.size() == max)
                                    ib->endData();
                                if (selectedC.size() == max)
                                    ic->endData();
                                QVERIFY(mux->wait(30));
                                delete mux;
                                QVERIFY(ingress.ended());
                                QVERIFY(compareLists(ingress.values(), expected));
                            }
                        }

                        SequenceValue::Transfer expected;
                        Util::append(selectedA, expected);
                        Util::append(selectedB, expected);
                        std::sort(expected.begin(), expected.end(), sortResultList);

                        SinkMultiplexer *mux = new SinkMultiplexer;
                        mux->start();
                        ingress.reset();
                        mux->setEgress(&ingress);
                        ia = mux->createSink();
                        ib = mux->createSink();
                        mux->sinkCreationComplete();
                        ia->incomingData(selectedA);
                        ia->endData();
                        ib->incomingData(selectedB);
                        ib->endData();
                        QVERIFY(mux->wait(30));
                        delete mux;
                        QVERIFY(ingress.ended());
                        QVERIFY(compareLists(ingress.values(), expected));
                    }
                }

                SinkMultiplexer *mux = new SinkMultiplexer;
                mux->start();
                ingress.reset();
                mux->setEgress(&ingress);
                ia = mux->createSink();
                mux->sinkCreationComplete();
                ia->incomingData(selectedA);
                ia->endData();
                QVERIFY(mux->wait(30));
                delete mux;
                QVERIFY(ingress.ended());
                QVERIFY(compareLists(ingress.values(), selectedA));
            }
        }
    }

    void mergeOrderTrim()
    {
        SinkMultiplexer::Sink *ia = NULL;
        SinkMultiplexer::Sink *ib = NULL;
        SinkMultiplexer::Sink *ic = NULL;
        StreamSink::Buffer ingress;

        SequenceValue::Transfer a;
        SequenceValue::Transfer b;
        SequenceValue::Transfer c;

        a.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.01), 100.0, 110.0);
        a.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.02), 110.0, 120.0);
        a.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.03), 120.0, 130.0);

        b.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.11), 50.0, 60.0);
        b.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.12), 60.0, 70.0);
        b.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.13), 70.0, 80.0);

        c.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.21), 65.0, 70.0);
        c.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.22), 70.0, 80.0);
        c.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(0.23), 115.0, 120.0);

        SequenceValue::Transfer expected;
        Util::append(a, expected);
        Util::append(b, expected);
        Util::append(c, expected);
        std::sort(expected.begin(), expected.end(), sortResultList);

        SinkMultiplexer *mux = new SinkMultiplexer;
        mux->start();
        ingress.reset();
        mux->setEgress(&ingress);
        ia = mux->createSink();
        ib = mux->createSink();
        ic = mux->createSink();
        mux->sinkCreationComplete();

        ia->incomingData(a);
        ia->endData();
        ib->incomingData(b);
        ib->endData();
        ic->incomingData(c);
        ic->endData();
        QVERIFY(mux->wait(30));
        delete mux;
        QVERIFY(ingress.ended());
        QVERIFY(compareLists(ingress.values(), expected));
    }

    void bulk()
    {
        SinkMultiplexer::Sink *ia = NULL;
        SinkMultiplexer::Sink *ib = NULL;
        SinkMultiplexer::Sink *ic = NULL;
        StreamSink::Buffer ingress;

        SequenceValue::Transfer a;
        SequenceValue::Transfer b;
        SequenceValue::Transfer c;

        for (int i = 0; i < 4000; i++) {
            a.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(i + 0.1), 100.0 + i,
                           101.0 + i);
            b.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(i + 0.1), 105.0 + i,
                           106.0 + i);
            c.emplace_back(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(i + 0.1), 110.0 + i,
                           111.0 + i);
        }

        SequenceValue::Transfer expected;
        Util::append(a, expected);
        Util::append(b, expected);
        Util::append(c, expected);
        std::sort(expected.begin(), expected.end(), sortResultList);

        SinkMultiplexer *mux = new SinkMultiplexer;
        mux->start();
        ingress.reset();
        mux->setEgress(&ingress);
        ia = mux->createSink();
        ib = mux->createSink();
        ic = mux->createSink();

        for (std::size_t i = 0; i < 400; i++) {
            switch (i % 4) {
            case 0:
                ia->incomingData(slice(a, i * 10, 10));
                ib->incomingData(slice(b, i * 10, 10));
                ic->incomingData(slice(c, i * 10, 10));
                break;
            case 1:
                ib->incomingData(slice(b, i * 10, 10));
                ia->incomingData(slice(a, i * 10, 10));
                ic->incomingData(slice(c, i * 10, 10));
                break;
            case 2:
                ib->incomingData(slice(b, i * 10, 10));
                ic->incomingData(slice(c, i * 10, 10));
                ia->incomingData(slice(a, i * 10, 10));
                break;
            case 3:
                ic->incomingData(slice(c, i * 10, 10));
                ib->incomingData(slice(b, i * 10, 10));
                ia->incomingData(slice(a, i * 10, 10));
                break;
            }
            if (i % 50 == 0)
                QTest::qSleep(50);
        }
        mux->sinkCreationComplete();
        ib->endData();
        ic->endData();
        ia->endData();
        QVERIFY(mux->wait(30));
        delete mux;
        QVERIFY(ingress.ended());
        QVERIFY(compareLists(ingress.values(), expected));


        mux = new SinkMultiplexer;
        mux->start();
        ingress.reset();
        mux->setEgress(&ingress);
        ia = mux->createSink();
        ib = mux->createSink();
        ic = mux->createSink();

        for (int i = 0; i < 2000; i++) {
            ia->incomingData(a.at(i));
            ib->incomingData(b.at(i));
            ic->incomingData(c.at(i));
            if (i % 200 == 0)
                QTest::qSleep(50);
        }

        {
            mux->setEgress(NULL);
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << *mux;
            }
            ia->endData();
            ib->endData();
            ic->endData();
            mux->sinkCreationComplete();
            mux->signalTerminate(false);
            QVERIFY(mux->wait(30));
            delete mux;

            mux = new SinkMultiplexer;
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> *mux;
            }
            mux->start();
            mux->setEgress(&ingress);
            ia = mux->createSink();
            ib = mux->createSink();
            ic = mux->createSink();
        }

        mux->sinkCreationComplete();
        for (int i = 2000; i < 4000; i++) {
            ia->incomingData(a.at(i));
            ib->incomingData(b.at(i));
            ic->incomingData(c.at(i));
            if (i % 200 == 0)
                QTest::qSleep(50);
        }


        ib->endData();
        ic->endData();
        ia->endData();
        QVERIFY(mux->wait(30));
        delete mux;
        QVERIFY(ingress.ended());
        QVERIFY(compareLists(ingress.values(), expected));
    }

};

QTEST_MAIN(TestSinkMultiplexer)

#include "sinkmultiplexer.moc"
