/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <vector>
#include <memory>
#include <algorithm>
#include <functional>
#include <iterator>
#include <QDebugStateSaver>

#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "datacore/stream.hxx"
#include "archive/access.hxx"
#include "core/threadpool.hxx"
#include "core/qtcompat.hxx"
#include "core/util.hxx"


namespace CPD3 {
namespace Data {

/** @file datacore/segmentread.hxx
 * Provides routines to interpret data as non-overlapping segments of values.
 */

template<typename Order, typename T, typename ListType>
static void maintainValueTransparency(ListType &values, T &&incoming)
{
    if (values.empty()) {
        values.emplace_back(std::forward<T>(incoming));
    } else {
        auto targetPosition = std::upper_bound(values.begin(), values.end(), incoming, Order());
        double incomingEnd = incoming.getEnd();
        bool isOpaque = incoming.root().isOverlayOpaque();
        targetPosition = values.emplace(targetPosition, std::forward<T>(incoming));

        /* If the incoming value is opaque and not at the start, remove
         * anything it covers completely (ends before the end of the new
         * one), since they can no longer possibly have an effect.  We
         * only need to go to the first uncovered one, since the other
         * inserts will have done the same thing. */
        if (isOpaque && targetPosition != values.begin()) {
            --targetPosition;
            size_t remaining = targetPosition - values.begin();
            for (;;) {
                if (Range::compareEnd(targetPosition->getEnd(), incomingEnd) > 0)
                    break;
                targetPosition = values.erase(targetPosition);
                if (remaining == 0)
                    break;
                --targetPosition;
                --remaining;
            }
        }
    }
}

/**
 * Maintain a list of values.  This list is maintained in sorted order
 * suitable for use with a reader.
 * 
 * @param values    the list of values being maintained
 * @param incoming  the incoming value to insert
 */
void SequenceSegment::Stream::maintainValues(std::vector<SequenceValue> &values,
                                             const SequenceValue &incoming)
{
    /* Zero length values are technically valid, but can't contribute to
     * valid segments, so discard them */
    if (Range::compareStartEnd(incoming.getStart(), incoming.getEnd()) >= 0)
        return;

    if (FP::defined(incoming.getStart())) {
        for (auto check = values.begin(); check != values.end();) {
            if (!FP::defined(check->getEnd())) {
                ++check;
                continue;
            }
            if (check->getEnd() > incoming.getStart()) {
                ++check;
                continue;
            }
            check = values.erase(check);
        }
    }

    maintainValueTransparency<SequenceIdentity::OrderOverlay>(values, incoming);
}

void SequenceSegment::Stream::maintainValues(std::vector<SequenceValue> &values,
                                             SequenceValue &&incoming)
{
    /* Zero length values are technically valid, but can't contribute to
     * valid segments, so discard them */
    if (Range::compareStartEnd(incoming.getStart(), incoming.getEnd()) >= 0)
        return;

    if (FP::defined(incoming.getStart())) {
        for (auto check = values.begin(); check != values.end();) {
            if (!FP::defined(check->getEnd())) {
                ++check;
                continue;
            }
            if (check->getEnd() > incoming.getStart()) {
                ++check;
                continue;
            }
            check = values.erase(check);
        }
    }

    maintainValueTransparency<SequenceIdentity::OrderOverlay>(values, std::move(incoming));
}

/**
 * Execute the overlay of a set of values.  Usually used in conjunction with
 * maintainValues(std::vector<SequenceValue> &, const SequenceValue &) to generate
 * the final output value.
 *
 * @param values    the input values in overlay order
 * @return          the overlay output
 */
Variant::Root SequenceSegment::Stream::overlayValues(const std::vector<SequenceValue> &values)
{
    class overlay_iterator final {
        std::vector<SequenceValue>::const_iterator backing;
    public:
        using difference_type = std::vector<SequenceValue>::difference_type;
        using value_type = const Variant::Root;
        using pointer = value_type *;
        using reference = value_type &;
        using iterator_category = std::input_iterator_tag;

        overlay_iterator() = default;

        overlay_iterator(std::vector<SequenceValue>::const_iterator backing) : backing(
                std::move(backing))
        { }

        overlay_iterator(const overlay_iterator &) = default;

        overlay_iterator &operator=(const overlay_iterator &) = default;

        overlay_iterator(overlay_iterator &&) = default;

        overlay_iterator &operator=(overlay_iterator &&) = default;

        reference operator*() const
        { return backing->root(); }

        pointer operator->() const
        { return &backing->root(); }

        overlay_iterator &operator++()
        {
            ++backing;
            return *this;
        }

        overlay_iterator operator++(int)
        {
            auto save = *this;
            ++backing;
            return save;
        }

        bool operator==(const overlay_iterator &other) const
        { return backing == other.backing; }

        bool operator!=(const overlay_iterator &other) const
        { return backing != other.backing; }
    };
    return Variant::Root::overlay(overlay_iterator(values.begin()), overlay_iterator(values.end()));
}


SequenceSegment::Stream::Stream() = default;

SequenceSegment::Stream::Stream(const Stream &) = default;

SequenceSegment::Stream &SequenceSegment::Stream::operator=(const Stream &) = default;

SequenceSegment::Stream::Stream(Stream &&) = default;

SequenceSegment::Stream &SequenceSegment::Stream::operator=(Stream &&) = default;

SequenceSegment::Stream::Backend::Backend() = default;

SequenceSegment::Stream::Backend::Backend(const Backend &) = default;

SequenceSegment::Stream::Backend &SequenceSegment::Stream::Backend::operator=(const Backend &) = default;

SequenceSegment::Stream::Backend::Backend(Backend &&) = default;

SequenceSegment::Stream::Backend &SequenceSegment::Stream::Backend::operator=(Backend &&) = default;

void SequenceSegment::Stream::Backend::serialize(QDataStream &stream) const
{
    SegmentStream<SequenceSegment, Backend, SequenceSegment::Transfer>::serialize(stream);
    stream << values << injected;
}

void SequenceSegment::Stream::Backend::deserialize(QDataStream &stream)
{
    SegmentStream<SequenceSegment, Backend, SequenceSegment::Transfer>::deserialize(stream);
    stream >> values >> injected;
}

SequenceSegment SequenceSegment::Stream::Backend::convertActive(double start, double end) const
{
    if (injected.empty())
        return SequenceSegment(start, end, values);
    auto merged = values;
    for (const auto &inject : injected) {
        auto &combined = merged[inject.first];
        for (const auto &add : inject.second) {
            if (Range::compareStartEnd(start, add.getEnd()) >= 0)
                continue;
            combined.emplace_back(add);
        }
        std::sort(combined.begin(), combined.end(), BackendData::OrderOverlay());
    }
    return SequenceSegment(start, end, std::move(merged));
}

bool SequenceSegment::Stream::Backend::purgeBefore(double before)
{
    for (auto unit = injected.begin(); unit != injected.end();) {
        auto &list = unit->second;
        for (auto check = list.begin(); check != list.end();) {
            if (Range::compareStartEnd(before, check->getEnd()) >= 0) {
                check = list.erase(check);
                continue;
            }
            ++check;
        }
        if (list.empty()) {
            unit = injected.erase(unit);
            continue;
        }
        ++unit;
    }

    bool haveData = false;
    for (auto unit = values.begin(); unit != values.end();) {
        auto &list = unit->second;
        for (auto check = list.begin(); check != list.end();) {
            if (Range::compareStartEnd(before, check->getEnd()) >= 0) {
                check = list.erase(check);
                continue;
            }
            ++check;
        }
        if (list.empty()) {
            unit = values.erase(unit);
            continue;
        }
        ++unit;
        haveData = true;
    }
    return haveData;
}

void SequenceSegment::Stream::Backend::addActive(const SequenceValue &value)
{
    maintainValueTransparency<BackendData::OrderOverlay>(values[value.getName()],
                                                         BackendData(value));
}

void SequenceSegment::Stream::Backend::addInjected(const SequenceValue &value)
{
    maintainValueTransparency<BackendData::OrderOverlay>(injected[value.getName()],
                                                         BackendData(value));
}

void SequenceSegment::Stream::Backend::addActive(SequenceValue &&value)
{
    auto &target = values[value.getName()];
    maintainValueTransparency<BackendData::OrderOverlay>(target, BackendData(std::move(value)));
}

void SequenceSegment::Stream::Backend::addInjected(SequenceValue &&value)
{
    auto &target = injected[value.getName()];
    maintainValueTransparency<BackendData::OrderOverlay>(target, BackendData(std::move(value)));
}

void SequenceSegment::Stream::Backend::addActive(ValueMoveData &&value)
{
    maintainValueTransparency<BackendData::OrderOverlay>(values[value.name], std::move(value.add));
}

void SequenceSegment::Stream::Backend::addInjected(ValueMoveData &&value)
{
    maintainValueTransparency<BackendData::OrderOverlay>(injected[value.name],
                                                         std::move(value.add));
}

void SequenceSegment::Stream::Backend::addActive(FullMoveData &&value)
{
    maintainValueTransparency<BackendData::OrderOverlay>(values[std::move(value.name)],
                                                         std::move(value.add));
}

void SequenceSegment::Stream::Backend::addInjected(FullMoveData &&value)
{
    maintainValueTransparency<BackendData::OrderOverlay>(injected[std::move(value.name)],
                                                         std::move(value.add));
}

void SequenceSegment::Stream::Backend::clearActive()
{
    values.clear();
    injected.clear();
}

void SequenceSegment::Stream::Backend::mergeAll(const Backend &other)
{
    if (values.empty() && injected.empty()) {
        values = other.values;
        injected = other.injected;
        return;
    }
    if (other.values.empty() && other.injected.empty())
        return;
    for (const auto &add : other.injected) {
        auto &target = injected[add.first];
        for (const auto &v : add.second) {
            BackendData data(v);
            maintainValueTransparency<BackendData::OrderOverlay>(target, data);
        }
        Q_ASSERT(!target.empty());
    }
    for (const auto &add : other.values) {
        auto &target = values[add.first];
        for (const auto &v : add.second) {
            BackendData data(v);
            maintainValueTransparency<BackendData::OrderOverlay>(target, data);
        }
        Q_ASSERT(!target.empty());
    }
}


QDataStream &operator<<(QDataStream &stream, const SequenceSegment::Stream &reader)
{
    reader.backend.serialize(stream);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, SequenceSegment::Stream &reader)
{
    reader.backend.deserialize(stream);
    return stream;
}

QDebug operator<<(QDebug stream, const SequenceSegment::Stream &reader)
{
    stream << reader.backend;
    return stream;
}


ValueSegment::Stream::Stream() = default;

ValueSegment::Stream::Stream(const ValueSegment::Stream &) = default;

ValueSegment::Stream &ValueSegment::Stream::operator=(const ValueSegment::Stream &) = default;

ValueSegment::Stream::Stream(ValueSegment::Stream &&) = default;

ValueSegment::Stream &ValueSegment::Stream::operator=(ValueSegment::Stream &&) = default;

ValueSegment::Stream::Backend::Backend() = default;

ValueSegment::Stream::Backend::Backend(const Backend &) = default;

ValueSegment::Stream::Backend &ValueSegment::Stream::Backend::operator=(const Backend &) = default;

ValueSegment::Stream::Backend::Backend(Backend &&) = default;

ValueSegment::Stream::Backend &ValueSegment::Stream::Backend::operator=(Backend &&) = default;

void ValueSegment::Stream::Backend::serialize(QDataStream &stream) const
{
    SegmentStream<ValueSegment, Backend, ValueSegment::Transfer>::serialize(stream);
    stream << values << injected;
}

void ValueSegment::Stream::Backend::deserialize(QDataStream &stream)
{
    SegmentStream<ValueSegment, Backend, ValueSegment::Transfer>::deserialize(stream);
    stream >> values >> injected;
}

ValueSegment ValueSegment::Stream::Backend::convertActive(double start, double end) const
{
    if (injected.empty())
        return ValueSegment(start, end, Variant::Root::overlay(values.begin(), values.end()));
    auto merged = values;
    for (const auto &add : injected) {
        if (Range::compareStartEnd(start, add.getEnd()) >= 0)
            continue;
        merged.emplace_back(add);
    }
    std::sort(merged.begin(), merged.end(), BackendData::OrderOverlay());
    return ValueSegment(start, end, Variant::Root::overlay(merged.begin(), merged.end()));
}

bool ValueSegment::Stream::Backend::purgeBefore(double before)
{
    for (auto check = injected.begin(); check != injected.end();) {
        if (Range::compareStartEnd(before, check->getEnd()) >= 0) {
            check = injected.erase(check);
            continue;
        }
        ++check;
    }
    for (auto check = values.begin(); check != values.end();) {
        if (Range::compareStartEnd(before, check->getEnd()) >= 0) {
            check = values.erase(check);
            continue;
        }
        ++check;
    }
    return !values.empty();
}

void ValueSegment::Stream::Backend::addActive(const SequenceValue &value)
{ maintainValueTransparency<BackendData::OrderOverlay>(values, value); }

void ValueSegment::Stream::Backend::addInjected(const SequenceValue &value)
{ maintainValueTransparency<BackendData::OrderOverlay>(injected, value); }

void ValueSegment::Stream::Backend::addActive(SequenceValue &&value)
{ maintainValueTransparency<BackendData::OrderOverlay>(values, std::move(value)); }

void ValueSegment::Stream::Backend::addInjected(SequenceValue &&value)
{ maintainValueTransparency<BackendData::OrderOverlay>(injected, std::move(value)); }

void ValueSegment::Stream::Backend::clearActive()
{
    values.clear();
    injected.clear();
}

void ValueSegment::Stream::Backend::mergeAll(const Backend &other)
{
    if (values.empty() && injected.empty()) {
        values = other.values;
        injected = other.injected;
        return;
    }
    if (other.values.empty() && other.injected.empty())
        return;
    for (const auto &v : other.values) {
        BackendData data(v);
        maintainValueTransparency<BackendData::OrderOverlay>(values, data);
    }
    for (const auto &v : other.injected) {
        BackendData data(v);
        maintainValueTransparency<BackendData::OrderOverlay>(injected, data);
    }
    Q_ASSERT(!values.empty() || !injected.empty());
}


QDataStream &operator<<(QDataStream &stream, const ValueSegment::Stream &reader)
{
    reader.backend.serialize(stream);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, ValueSegment::Stream &reader)
{
    reader.backend.deserialize(stream);
    return stream;
}

QDebug operator<<(QDebug stream, const ValueSegment::Stream &reader)
{
    stream << reader.backend;
    return stream;
}


/**
 * Read a single variable set of non-overlapping segments.  This will overlay
 * all segments into a single stack regardless of their normally independent
 * units.
 * 
 * @param selection the selection to read, this will NOT include the metadata archive
 * @param archive   the reader to access, or NULL to use the default
 */
ValueSegment::Transfer ValueSegment::Stream::read(const Archive::Selection &selection,
                                                  Archive::Access *archive)
{

    std::unique_ptr<Archive::Access> localAccess;
    if (!archive) {
        localAccess.reset(new Archive::Access);
        archive = localAccess.get();
    }

    StreamSink::Iterator incoming;
    archive->readStream(selection.withMetaArchive(false), &incoming)->detach();

    ValueSegment::Transfer result;
    ValueSegment::Stream reader;
    for (;;) {
        auto add = incoming.all();
        if (add.empty())
            break;
        Util::append(reader.add(std::move(add)), result);
    }
    Util::append(reader.finish(), result);

    return result;
}


/**
 * Read a selection of data as non-overlapping segments.
 * 
 * @param selections the selections to read
 * @param archive   the reader to access, or NULL to use the default
 */
SequenceSegment::Transfer SequenceSegment::Stream::read(const Archive::Selection::List &selections,
                                                        Archive::Access *archive)
{
    std::unique_ptr<Archive::Access> localAccess;
    if (!archive) {
        localAccess.reset(new Archive::Access);
        archive = localAccess.get();
    }

    StreamSink::Iterator incoming;
    archive->readStream(selections, &incoming)->detach();

    SequenceSegment::Transfer result;
    SequenceSegment::Stream reader;
    for (;;) {
        auto add = incoming.all();
        if (add.empty())
            break;
        Util::append(reader.add(std::move(add)), result);
    }
    Util::append(reader.finish(), result);

    return result;
}

/**
 * Read a selection of data as non-overlapping segments.
 * 
 * @param selection the selection to read
 * @param archive   the reader to access, or NULL to use the default
 */
SequenceSegment::Transfer SequenceSegment::Stream::read(const Archive::Selection &selection,
                                                        Archive::Access *archive)
{ return read(Archive::Selection::List{selection}, archive); }


ValueSegment::ValueSegment() : start(FP::undefined()), end(FP::undefined()), contents()
{ }

ValueSegment::ValueSegment(const ValueSegment &) = default;

ValueSegment &ValueSegment::operator=(const ValueSegment &) = default;

ValueSegment::ValueSegment(ValueSegment &&) = default;

ValueSegment &ValueSegment::operator=(ValueSegment &&) = default;

ValueSegment::ValueSegment(double setStart, double setEnd, const Variant::Root &setValue) : start(
        setStart), end(setEnd), contents(setValue)
{ }

ValueSegment::ValueSegment(double setStart, double setEnd, Variant::Root &&setValue) : start(
        setStart), end(setEnd), contents(std::move(setValue))
{ }

ValueSegment::ValueSegment(const Variant::Root &value) : start(FP::undefined()),
                                                         end(FP::undefined()),
                                                         contents(value)
{ }

ValueSegment::ValueSegment(Variant::Root &&value) : start(FP::undefined()),
                                                    end(FP::undefined()),
                                                    contents(std::move(value))
{ }

namespace {
class DataSegmentMergeType : public Time::Bounds {
public:
    QString underPath;
    Variant::Root value;

    DataSegmentMergeType(const ValueSegment &from, const QString &targetPath) : Time::Bounds(
            from.getStart(), from.getEnd()),
                                                                                underPath(
                                                                                        targetPath),
                                                                                value(Variant::Root::overlay(
                                                                                        from.root()))
    { }

    DataSegmentMergeType(const DataSegmentMergeType &other, double start, double end)
            : Time::Bounds(start, end)
    {
        if (other.underPath.isEmpty()) {
            value = other.value;
        } else {
            value[other.underPath].set(other.value);
        }
    }

    DataSegmentMergeType(const DataSegmentMergeType &under,
                         const DataSegmentMergeType &over,
                         double start,
                         double end) : Time::Bounds(start, end)
    {
        if (!over.underPath.isEmpty()) {
            auto merged =
                    Variant::Root::overlay(Variant::Root(under.value[under.underPath]), over.value);
            Variant::Root output = under.value;
            output[over.underPath].set(merged);
            value = std::move(output);
        } else {
            value = Variant::Root::overlay(under.value, over.value);
        }
    }
};
}

/**
 * Convert a list of segments to a list of them with the given path applied
 * to each value.
 * 
 * @param input     the input segments
 * @param path      the path to apply
 * @return          the input segments with the path applied to each
 */
ValueSegment::Transfer ValueSegment::withPath(const ValueSegment::Transfer &input,
                                              const QString &path)
{
    ValueSegment::Transfer result;
    result.reserve(input.size());
    for (const auto &add : input) {
        result.emplace_back(add.getStart(), add.getEnd(), Variant::Root(add.value().getPath(path)));
    }
    return result;
}

/**
 * Merge the given list of segments into a final non-overlapping one
 * by overlaying their values.
 * 
 * @param segments      the segments to merge
 * @param targetPaths   the path each segment should have in the final output, if empty or outside the range of the segments it it put at the root
 */
ValueSegment::Transfer ValueSegment::merge(const ValueSegment::Transfer &segments,
                                           const QList<QString> &targetPaths)
{
    std::deque<DataSegmentMergeType> combined;
    for (std::size_t i = 0, max = segments.size(); i < max; i++) {
        QString path;
        if (i < static_cast<std::size_t>(targetPaths.size()))
            path = targetPaths.at(static_cast<int>(i));
        Range::overlayFragmenting(combined, DataSegmentMergeType(segments[i], path));
    }

    ValueSegment::Transfer result;
    result.reserve(combined.size());
    for (auto &add : combined) {
        if (add.underPath.isEmpty()) {
            result.emplace_back(add.getStart(), add.getEnd(), std::move(add.value));
            continue;
        }
        Variant::Root target;
        target[add.underPath].set(add.value);
        result.emplace_back(add.getStart(), add.getEnd(), std::move(target));
    }
    return result;
}


SequenceSegment::OverlayData::OverlayData(const SequenceValue &value) : priority(
        value.getPriority()),
                                                                        start(value.getStart()),
                                                                        end(value.getEnd()),
                                                                        defaultStation(
                                                                                value.getName()
                                                                                     .isDefaultStation()),
                                                                        value(value.root())
{ }

SequenceSegment::OverlayData::OverlayData(SequenceValue &&value) : priority(value.getPriority()),
                                                                   start(value.getStart()),
                                                                   end(value.getEnd()),
                                                                   defaultStation(value.getName()
                                                                                       .isDefaultStation()),
                                                                   value(std::move(value.root()))
{ }

SequenceSegment::OverlayData::OverlayData(const SequenceIdentity &identity,
                                          const Variant::Root &value) : priority(
        identity.getPriority()),
                                                                        start(identity.getStart()),
                                                                        end(identity.getEnd()),
                                                                        defaultStation(
                                                                                identity.getName()
                                                                                        .isDefaultStation()),
                                                                        value(value)
{ }

SequenceSegment::OverlayData::OverlayData(const SequenceIdentity &identity, Variant::Root &&value)
        : priority(identity.getPriority()),
          start(identity.getStart()),
          end(identity.getEnd()),
          defaultStation(identity.getName().isDefaultStation()),
          value(std::move(value))
{ }

SequenceSegment::OverlayData::OverlayData(const SequenceName &name, Variant::Root &&value)
        : priority(0),
          start(FP::undefined()),
          end(FP::undefined()),
          defaultStation(name.isDefaultStation()),
          value(std::move(value))
{ }

QDataStream &operator<<(QDataStream &stream, const SequenceSegment::OverlayData &value)
{
    stream << static_cast<qint32>(value.priority) << value.start << value.end
           << value.defaultStation << value.value;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, SequenceSegment::OverlayData &value)
{
    {
        qint32 i = 0;
        stream >> i;
        value.priority = i;
    }
    stream >> value.start >> value.end >> value.defaultStation >> value.value;
    return stream;
}

/* Must match SequenceIdentity::OrderOverlay */
template<typename T1, typename T2>
static inline bool overlayCompare(bool i1DefaultStation,
                                  bool i2DefaultStation,
                                  const T1 &a,
                                  const T2 &b)
{
    if (i1DefaultStation && !i2DefaultStation)
        return true;
    else if (i2DefaultStation && !i1DefaultStation)
        return false;

    if (a.getPriority() != b.getPriority())
        return a.getPriority() < b.getPriority();

    if (!FP::defined(a.getStart())) {
        if (FP::defined(b.getStart()))
            return true;
    } else if (!FP::defined(b.getStart())) {
        return false;
    } else if (a.getStart() != b.getStart()) {
        return a.getStart() < b.getStart();
    }

#if 0
    bool i1IsMeta = a.getName().isMeta();
    bool i2IsMeta = b.getName().isMeta();
    if (i1IsMeta && !i2IsMeta)
        return true;
    else if (i2IsMeta && !i1IsMeta)
        return false;
#endif

    return false;
}

bool SequenceSegment::OverlayData::OrderOverlay::operator()(const SequenceSegment::OverlayData &a,
                                                            const SequenceSegment::OverlayData &b) const
{ return overlayCompare(a.isDefaultStation(), b.isDefaultStation(), a, b); }

bool SequenceSegment::OverlayData::OrderOverlay::operator()(const SequenceSegment::OverlayData &a,
                                                            const SequenceValue &b) const
{ return overlayCompare(a.isDefaultStation(), b.getName().isDefaultStation(), a, b); }

bool SequenceSegment::OverlayData::OrderOverlay::operator()(const SequenceValue &a,
                                                            const SequenceSegment::OverlayData &b) const
{ return overlayCompare(a.getName().isDefaultStation(), b.isDefaultStation(), a, b); }

bool SequenceSegment::OverlayData::OrderOverlay::operator()(const SequenceValue &a,
                                                            const SequenceValue &b) const
{ return overlayCompare(a.getName().isDefaultStation(), b.getName().isDefaultStation(), a, b); }

SequenceSegment::SequenceSegment() : start(FP::undefined()),
                                     end(FP::undefined()),
                                     values(),
                                     inputs()
{ }

SequenceSegment::SequenceSegment(const SequenceSegment &) = default;

SequenceSegment &SequenceSegment::operator=(const SequenceSegment &) = default;

SequenceSegment::SequenceSegment(SequenceSegment &&) = default;

SequenceSegment &SequenceSegment::operator=(SequenceSegment &&) = default;

SequenceSegment::SequenceSegment(double setStart,
                                 double setEnd,
                                 const SequenceName::Map<std::vector<OverlayData>> &incoming)
        : start(setStart), end(setEnd), values(), inputs()
{
    for (auto &add : incoming) {
        if (add.second.empty())
            continue;

        std::vector<Variant::Root> merge;
        for (const auto &v : add.second) {
            merge.emplace_back(v.root());
        }
        inputs.emplace(add.first, std::move(merge));
    }
}

SequenceSegment::SequenceSegment(double setStart,
                                 double setEnd,
                                 SequenceName::Map<std::vector<OverlayData>> &&incoming) : start(
        setStart), end(setEnd), values(), inputs()
{
    for (auto &add : incoming) {
        if (add.second.empty())
            continue;

        std::vector<Variant::Root> merge;
        for (const auto &v : add.second) {
            merge.emplace_back(std::move(v.root()));
        }
        inputs.emplace(add.first, std::move(merge));
    }
}

SequenceSegment::SequenceSegment(double setStart,
                                 double setEnd,
                                 const SequenceName::Map<std::vector<SequenceValue>> &incoming)
        : start(setStart), end(setEnd), values(), inputs()
{
    for (auto &add : incoming) {
        if (add.second.empty())
            continue;

        std::vector<Variant::Root> merge;
        for (const auto &v : add.second) {
            merge.emplace_back(v.root());
        }
        inputs.emplace(add.first, std::move(merge));
    }
}

SequenceSegment::SequenceSegment(double setStart,
                                 double setEnd,
                                 SequenceName::Map<std::vector<SequenceValue>> &&incoming) : start(
        setStart), end(setEnd), values(), inputs()
{
    for (auto &add : incoming) {
        if (add.second.empty())
            continue;

        std::vector<Variant::Root> merge;
        for (const auto &v : add.second) {
            merge.emplace_back(std::move(v.root()));
        }
        inputs.emplace(add.first, std::move(merge));
    }
}

bool SequenceSegment::exists(const SequenceName &key) const
{
    {
        auto check = values.find(key);
        if (check != values.end() && check->second.read().exists())
            return true;
    }
    auto check = inputs.find(key);
    if (check != inputs.end() && !check->second.empty())
        return true;
    check = inputs.find(key.toDefaultStation());
    if (check != inputs.end() && !check->second.empty())
        return true;
    return false;
}

Variant::Root &SequenceSegment::lookupValue(const SequenceName &key)
{
    auto check = values.find(key);
    if (check != values.end()) {
        return check->second;
    }

    auto end = inputs.end();
    auto defaults = inputs.find(key.toDefaultStation());
    /* Flatten defaults if required */
    if (defaults != end && defaults->second.size() > 1) {
        defaults->second[0] =
                Variant::Root::overlay(defaults->second.begin(), defaults->second.end());
        defaults->second.resize(1);
    }

    auto specific = inputs.find(key);
    if (defaults == end) {
        if (specific == end) {
            return values.emplace(key, Variant::Root()).first->second;
        }

        auto &result = values.emplace(key, Variant::Root::overlay(specific->second.begin(),
                                                                  specific->second.end()))
                             .first
                             ->second;
        /* Purge it now since it's in the value cache */
        inputs.erase(specific);
        return result;
    } else if (specific == end) {
        Q_ASSERT(!defaults->second.empty());
        /* Always merge, so we make sure we've flattened when there was only one
         * default to begin with */
        return values.emplace(key, Variant::Root::overlay(defaults->second.begin(),
                                                          defaults->second.end())).first->second;
    } else {
        auto list = defaults->second;
        Util::append(std::move(specific->second), list);

        auto &result =
                values.emplace(key, Variant::Root::overlay(list.begin(), list.end())).first->second;
        /* Purge it now since it's in the value cache */
        inputs.erase(specific);
        return result;
    }
}

Variant::Write SequenceSegment::getValue(const SequenceName &key)
{ return lookupValue(key).write(); }

Variant::Root SequenceSegment::takeValue(const SequenceName &key)
{ return std::move(lookupValue(key)); }

Variant::Read SequenceSegment::getValue(const SequenceName &key) const
{
    auto check = values.find(key);
    if (check != values.end()) {
        return check->second.read();
    }

    auto end = inputs.end();
    auto defaults = inputs.find(key.toDefaultStation());
    auto specific = inputs.find(key);
    if (defaults == end) {
        if (specific == end)
            return Variant::Read::empty();
        return Variant::Root::overlay(specific->second.begin(), specific->second.end());
    } else if (specific == end) {
        return Variant::Root::overlay(defaults->second.begin(), defaults->second.end());
    } else {
        auto list = defaults->second;
        Util::append(specific->second, list);
        return Variant::Root::overlay(list.begin(), list.end()).read();
    }
}

static bool acceptFlavors(const SequenceName &unit,
                          const QList<QRegExp> &hasFlavors,
                          const QList<QRegExp> &lacksFlavors)
{
    if (hasFlavors.isEmpty() && lacksFlavors.isEmpty())
        return true;
    auto flavors = unit.getFlavors();
    if (!lacksFlavors.isEmpty()) {
        for (const auto &flavor : flavors) {
            QString matchFlavor = QString::fromStdString(flavor);
            for (const auto &check : lacksFlavors) {
                if (check.exactMatch(matchFlavor))
                    return false;
            }
        }
    }

    if (!hasFlavors.isEmpty()) {
        for (const auto &check : hasFlavors) {
            bool hit = false;
            for (const auto &flavor : flavors) {
                if (!check.exactMatch(QString::fromStdString(flavor)))
                    continue;
                hit = true;
            }
            if (!hit)
                return false;
        }
    }

    return true;
}

Variant::Write SequenceSegment::getValue(const QRegExp &station,
                                         const QRegExp &archive,
                                         const QRegExp &variable,
                                         const QList<QRegExp> &hasFlavors,
                                         const QList<QRegExp> &lacksFlavors,
                                         SequenceName *matchedUnit)
{

    for (auto &i : values) {
        if (station.exactMatch(i.first.getStationQString()) &&
                archive.exactMatch(i.first.getArchiveQString()) &&
                variable.exactMatch(i.first.getVariableQString()) &&
                acceptFlavors(i.first, hasFlavors, lacksFlavors)) {
            if (matchedUnit)
                *matchedUnit = i.first;
            return i.second.write();
        }
    }

    auto end = inputs.end();

    auto defaults = inputs.begin();
    for (; defaults != end; ++defaults) {
        if (defaults->first.isDefaultStation() &&
                archive.exactMatch(defaults->first.getArchiveQString()) &&
                variable.exactMatch(defaults->first.getVariableQString()) &&
                acceptFlavors(defaults->first, hasFlavors, lacksFlavors)) {
            break;
        }
    }
    /* Flatten defaults if required */
    if (defaults != end && defaults->second.size() > 1) {
        defaults->second[0] =
                Variant::Root::overlay(defaults->second.begin(), defaults->second.end());
        defaults->second.resize(1);
    }

    auto specific = inputs.begin();
    for (; specific != end; ++specific) {
        if (station.exactMatch(specific->first.getStationQString()) &&
                archive.exactMatch(specific->first.getArchiveQString()) &&
                variable.exactMatch(specific->first.getVariableQString()) &&
                acceptFlavors(specific->first, hasFlavors, lacksFlavors)) {
            break;
        }
    }

    if (defaults == end) {
        if (specific == end) {
            Variant::Write result;
            result.detachFromRoot();
            return result;
        }

        auto result = values.emplace(specific->first,
                                     Variant::Root::overlay(specific->second.begin(),
                                                            specific->second.end()))
                            .first
                            ->second
                            .write();
        /* Purge it now since it's in the value cache */
        inputs.erase(specific);

        if (matchedUnit)
            *matchedUnit = specific->first;

        return result;
    } else if (specific == end) {
        if (matchedUnit) {
            *matchedUnit = defaults->first;
            matchedUnit->setStation({});
        }
        Q_ASSERT(!defaults->second.empty());
        /* Always merge, so we make sure we've flattened when there was only one
         * default to begin with */
        Variant::Root result = defaults->second.front();
        return Variant::Root::overlay(defaults->second.begin(), defaults->second.end()).write();
    } else {
        auto list = defaults->second;
        Util::append(std::move(specific->second), list);

        auto result =
                values.emplace(specific->first, Variant::Root::overlay(list.begin(), list.end()))
                      .first
                      ->second
                      .write();
        /* Purge it now since it's in the value cache */
        inputs.erase(specific);

        if (matchedUnit)
            *matchedUnit = specific->first;

        return result;
    }
}

/**
 * Get all known units.  This does not necessarily indicate all possible
 * value units (due to default station presence).
 * 
 * @param includeDefault    if set then the default station is included
 * @return                  the set of all known units
 */
QSet<SequenceName> SequenceSegment::getUnits(bool includeDefault) const
{
    QSet<SequenceName> result;
    for (const auto &i : values) {
        if (!includeDefault && i.first.isDefaultStation())
            continue;
        result.insert(i.first);
    }
    for (const auto &i : inputs) {
        if (!includeDefault && i.first.isDefaultStation())
            continue;
        result.insert(i.first);
    }
    return result;
}

/**
 * Reduce the given list of multi-segments to the "logical" list of a single
 * one.  This is intended to be used by readers that accept only a single
 * output value.  It will attempt to select the correct station, archive,
 * and variable.
 * 
 * @param input the input list
 * @return      the reduced segment list
 */
ValueSegment::Transfer SequenceSegment::reduce(const SequenceSegment::Transfer &input)
{
    ValueSegment::Transfer result;
    for (const auto &in : input) {
        const SequenceName *sel = nullptr;
        for (const auto &check : in.inputs) {
            if (check.first.isDefaultStation()) {
                sel = &check.first;
                continue;
            }
            sel = nullptr;
            result.emplace_back(in.getStart(), in.getEnd(),
                                Variant::Root(in.getValue(check.first)));
            break;
        }
        /* Only default station values exist, so just use one */
        if (sel) {
            result.emplace_back(in.getStart(), in.getEnd(), Variant::Root(in.getValue(*sel)));
        }
    }
    return result;
}

void SequenceSegment::overlay(const SequenceSegment &over)
{
    for (const auto &o : over.values) {
        values[o.first] = o.second;
    }
    for (const auto &o : over.inputs) {
        Util::append(o.second, inputs[o.first]);
    }
}

void SequenceSegment::overlay(SequenceSegment &&over)
{
    for (auto &o : over.values) {
        values[o.first] = std::move(o.second);
    }
    for (auto &o : over.inputs) {
        Util::append(std::move(o.second), inputs[o.first]);
    }
}

QDataStream &operator<<(QDataStream &stream, const ValueSegment &value)
{
    stream << value.start;
    stream << value.end;
    stream << value.root();
    return stream;
}

QDataStream &operator>>(QDataStream &stream, ValueSegment &value)
{
    stream >> value.start;
    stream >> value.end;
    stream >> value.root();
    return stream;
}

QDebug operator<<(QDebug stream, const ValueSegment &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "ValueSegment(" << Logging::range(v.start, v.end) << "," << v.root() << ")";
    return stream;
}

QDataStream &operator<<(QDataStream &stream, const SequenceSegment &value)
{
    stream << value.start;
    stream << value.end;
    stream << value.values;
    stream << value.inputs;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, SequenceSegment &value)
{
    stream >> value.start;
    stream >> value.end;
    stream >> value.values;
    stream >> value.inputs;
    return stream;
}

QDebug operator<<(QDebug stream, const SequenceSegment &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "SequenceSegment(" << Logging::range(v.start, v.end) << ",[" << v.values << "],["
           << v.inputs << "])";
    return stream;
}

uint qHash(const ValueSegment &key)
{
    return ::qHash(key.start) ^ ::qHash(key.end) ^ ::qHash(Variant::Read::ValueHash()(key.read()));
}


}
}
