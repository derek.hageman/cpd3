/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <vector>
#include <unordered_map>
#include <map>

#include "datacore/segmentprocessingstage.hxx"
#include "core/qtcompat.hxx"

namespace CPD3 {
namespace Data {

/** @file datacore/basicfilter.hxx
 * Provides the interface to simple filters.
 */

SegmentProcessingStage::SequenceHandlerControl::~SequenceHandlerControl() = default;

void SegmentProcessingStage::SequenceHandlerControl::injectUnit(const SequenceName &unit,
                                                                int targetID)
{ inputUnit(unit, targetID); }

void SegmentProcessingStage::SequenceHandlerControl::deferHandling(const SequenceName &)
{ }

SegmentProcessingStage::~SegmentProcessingStage() = default;

SequenceName::Set SegmentProcessingStage::requestedInputs()
{ return {}; }

SequenceName::Set SegmentProcessingStage::predictedOutputs()
{ return {}; }

void SegmentProcessingStage::processMeta(int, SequenceSegment &)
{ }

QSet<double> SegmentProcessingStage::metadataBreaks(int)
{ return {}; }

void SegmentProcessingStage::serialize(QDataStream &)
{ }

void SegmentProcessingStage::clearPropagatedSmoothing(Variant::Write &smoothing)
{
    if (!smoothing.exists())
        return;
    if (Util::equal_insensitive(smoothing.hash("Mode").toString(), "beerslawabsorption", "beerslaw",
                                "beerslawabsorptioninitial",
                                "beerslawinitial", "dewpoint", "td", "rhextrapolate",
                                "relativehumidityextrapolate")) {
        smoothing.remove(false);
    }
}

void SegmentProcessingStage::clearPropagatedSmoothing(Variant::Write &&smoothing)
{ return clearPropagatedSmoothing(smoothing); }


SegmentProcessingStageComponent::~SegmentProcessingStageComponent() = default;

SegmentProcessingStage *SegmentProcessingStageComponent::createBasicFilterPredefined(const ComponentOptions &,
                                                                                     double,
                                                                                     double,
                                                                                     const QList<
                                                                                             SequenceName> &)
{ return createBasicFilterDynamic(); }

SegmentProcessingStage *SegmentProcessingStageComponent::createBasicFilterEditing(double,
                                                                                  double,
                                                                                  const SequenceName::Component &,
                                                                                  const SequenceName::Component &,
                                                                                  const ValueSegment::Transfer &)
{ return createBasicFilterDynamic(); }

SegmentProcessingStage *SegmentProcessingStageComponent::deserializeBasicFilter(QDataStream &)
{
    Q_ASSERT(false);
    return nullptr;
}

void SegmentProcessingStageComponent::extendBasicFilterEditing(double &,
                                                               double &,
                                                               const SequenceName::Component &,
                                                               const SequenceName::Component &,
                                                               const ValueSegment::Transfer &,
                                                               CPD3::Data::Archive::Access *)
{ }

QString SegmentProcessingStageComponent::getBasicSerializationName() const
{ return {}; }

ProcessingStage *SegmentProcessingStageComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{ return new SegmentProcessingHandler(createBasicFilterDynamic(options)); }

ProcessingStage *SegmentProcessingStageComponent::createGeneralFilterPredefined(const ComponentOptions &options,
                                                                                double start,
                                                                                double end,
                                                                                const QList<
                                                                                        SequenceName> &inputs)
{
    return new SegmentProcessingHandler(createBasicFilterPredefined(options, start, end, inputs));
}

ProcessingStage *SegmentProcessingStageComponent::createGeneralFilterEditing(double start,
                                                                             double end,
                                                                             const SequenceName::Component &station,
                                                                             const SequenceName::Component &archive,
                                                                             const ValueSegment::Transfer &config)
{
    return new SegmentProcessingHandler(
            createBasicFilterEditing(start, end, station, archive, config));
}

ProcessingStage *SegmentProcessingStageComponent::deserializeGeneralFilter(QDataStream &stream)
{
    SegmentProcessingStage *f = deserializeBasicFilter(stream);
    return new SegmentProcessingHandler(stream, f);
}

void SegmentProcessingStageComponent::extendGeneralFilterEditing(double &start,
                                                                 double &end,
                                                                 const SequenceName::Component &station,
                                                                 const SequenceName::Component &archive,
                                                                 const ValueSegment::Transfer &config,
                                                                 CPD3::Data::Archive::Access *access)
{ return extendBasicFilterEditing(start, end, station, archive, config, access); }

QString SegmentProcessingStageComponent::getGeneralSerializationName() const
{ return getBasicSerializationName(); }

/**
 * Create a new basic filter handler thread with the given filter.  The default
 * egress is NULL (paused).  This takes ownership of the filter if specified.
 * 
 * @param setFilter     the filter to pass work off to
 * @param takeOwnership if true then ownership of the filter is transferred
 */
SegmentProcessingHandler::SegmentProcessingHandler(SegmentProcessingStage *setFilter,
                                                   bool takeOwnership) : handler(this),
                                                                         filter(setFilter),
                                                                         ownFilter(),
                                                                         sets(),
                                                                         dispatch(),
                                                                         uniqueDispatch(),
                                                                         bypassed()
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        ,
                                                                         lastOutputTime(
                                                                                 FP::undefined())
#endif
{
    Q_ASSERT(filter);
    if (takeOwnership)
        ownFilter.reset(filter);
}

SegmentProcessingHandler::~SegmentProcessingHandler() = default;


SegmentProcessingHandler::Handler::Handler(SegmentProcessingHandler *setParent) : parent(setParent),
                                                                                  updated()
{ }

SegmentProcessingHandler::Set *SegmentProcessingHandler::getSet(int targetID)
{
    if (targetID >= static_cast<int>(sets.size())) {
        for (int i = static_cast<int>(sets.size()); i < targetID; i++) {
            sets.emplace_back(new Set(this, i));
        }
        Set *target = new Set(this, targetID);
        sets.emplace_back(target);
        return target;
    } else {
        return sets[targetID].get();
    }
}

bool SegmentProcessingHandler::isAnOutput(const SequenceName &unit) const
{
    for (const auto &s : sets) {
        if (std::find(s->outputs.begin(), s->outputs.end(), unit) != s->outputs.end() ||
                std::find(s->metaOutputs.begin(), s->metaOutputs.end(), unit) !=
                        s->metaOutputs.end())
            return true;
    }
    return false;
}


std::vector<SegmentProcessingHandler::Dispatch *> &SegmentProcessingHandler::dispatchDefaultFind(
        const SequenceName &unit)
{
    auto d = dispatch.find(unit);
    if (d == dispatch.end()) {
        DefaultDispatch *dDefault = new DefaultDispatch;
        uniqueDispatch.emplace_back(dDefault);

        if (unit.isDefaultStation())
            dDefault->reader.reset(new SequenceSegment::Stream);

        d = dispatch.emplace(unit, std::vector<Dispatch *>{dDefault}).first;
    }
    return d->second;
}

void SegmentProcessingHandler::removeSetReference(std::vector<Dispatch *> &d, const Set *set)
{
    for (std::vector<Dispatch *>::iterator check = d.begin(); check != d.end();) {
        if ((*check)->referencesSet(set)) {
            check = d.erase(check);
            continue;
        }
        ++check;
    }
}

void SegmentProcessingHandler::pruneUnusedDispatch()
{
    std::unordered_set<Dispatch *> used;
    for (const auto &d : dispatch) {
        for (auto t : d.second) {
            used.insert(t);
        }
    }
    for (auto del = uniqueDispatch.begin(); del != uniqueDispatch.end();) {
        if (used.count(del->get()) != 0) {
            ++del;
            continue;
        }

        del = uniqueDispatch.erase(del);
    }
}

void SegmentProcessingHandler::overlayDefault(std::vector<Dispatch *> &def,
                                              SequenceSegment::Stream &target,
                                              double effectiveStart)
{
    static_cast<const DefaultDispatch *>(def.front())->overlay(target, effectiveStart);
}

void SegmentProcessingHandler::Handler::setUnitInputs(const SequenceName &unit,
                                                      SegmentProcessingHandler::Set *target,
                                                      SegmentProcessingHandler::Dispatch *dispatchData,
                                                      SegmentProcessingHandler::Dispatch *dispatchMeta)
{
    Q_ASSERT(!unit.isDefaultStation());
    Q_ASSERT(!unit.isMeta());

    parent->uniqueDispatch.emplace_back(dispatchData);
    parent->uniqueDispatch.emplace_back(dispatchMeta);

    SequenceName unitMeta(unit.toMeta());

    if (FP::defined(effectiveStart))
        target->advanceTime(effectiveStart);

    auto &dDefault = parent->dispatchDefaultFind(unit.toDefaultStation());
    SegmentProcessingHandler::overlayDefault(dDefault, target->reader, effectiveStart);
    SegmentProcessingHandler::removeSetReference(dDefault, target);
    dDefault.push_back(dispatchData);

    auto &dDefaultMeta = parent->dispatchDefaultFind(unitMeta.toDefaultStation());
    SegmentProcessingHandler::overlayDefault(dDefaultMeta, target->metaReader, effectiveStart);
    SegmentProcessingHandler::removeSetReference(dDefaultMeta, target);
    dDefaultMeta.push_back(dispatchMeta);

    auto &dData = parent->dispatchDefaultFind(unit);
    SegmentProcessingHandler::overlayDefault(dData, target->reader, effectiveStart);
    SegmentProcessingHandler::removeSetReference(dData, target);
    dData.push_back(dispatchData);

    auto &dMeta = parent->dispatchDefaultFind(unitMeta);
    SegmentProcessingHandler::overlayDefault(dMeta, target->metaReader, effectiveStart);
    SegmentProcessingHandler::removeSetReference(dMeta, target);
    dMeta.push_back(dispatchMeta);
}

void SegmentProcessingHandler::Handler::filterUnit(const SequenceName &unit, int targetID)
{
    if (unit.isDefaultStation() || unit.isMeta())
        return;
    if (parent->isAnOutput(unit))
        return;
    Q_ASSERT(targetID >= 0);

    SegmentProcessingHandler::Set *target = parent->getSet(targetID);
    Q_ASSERT(std::find(target->outputs.begin(), target->outputs.end(), unit) ==
                     target->outputs.end());
    Q_ASSERT(std::find(target->metaOutputs.begin(), target->metaOutputs.end(), unit) ==
                     target->metaOutputs.end());

    updated.insert(targetID);
    target->outputs.push_back(unit);
    target->inputs.remove(unit);
    target->injected.remove(unit);
    if (!unit.hasFlavor(SequenceName::flavor_end) && !unit.hasFlavor(SequenceName::flavor_cover))
        target->metaOutputs.push_back(unit.toMeta());

    setUnitInputs(unit, target, new SegmentProcessingHandler::SetDataDispatch(target),
                  new SegmentProcessingHandler::SetMetaDispatch(target));
}

void SegmentProcessingHandler::Handler::inputUnit(const SequenceName &unit, int targetID)
{
    if (unit.isDefaultStation() || unit.isMeta())
        return;
    Q_ASSERT(targetID >= 0);

    SegmentProcessingHandler::Set *target = parent->getSet(targetID);
    if (target->inputs.contains(unit))
        return;
    if (std::find(target->outputs.begin(), target->outputs.end(), unit) != target->outputs.end())
        return;
    Q_ASSERT(std::find(target->metaOutputs.begin(), target->metaOutputs.end(), unit) ==
                     target->metaOutputs.end());

    updated.insert(targetID);
    target->inputs.insert(unit);
    target->injected.remove(unit);

    setUnitInputs(unit, target, new SegmentProcessingHandler::SetPureInputDispatch(target),
                  new SegmentProcessingHandler::SetPureInputMetaDispatch(target));
}

void SegmentProcessingHandler::Handler::injectUnit(const SequenceName &unit, int targetID)
{
    if (unit.isDefaultStation() || unit.isMeta())
        return;
    Q_ASSERT(targetID >= 0);

    SegmentProcessingHandler::Set *target = parent->getSet(targetID);
    if (target->inputs.contains(unit))
        return;
    if (target->injected.contains(unit))
        return;
    if (std::find(target->outputs.begin(), target->outputs.end(), unit) != target->outputs.end())
        return;
    Q_ASSERT(std::find(target->metaOutputs.begin(), target->metaOutputs.end(), unit) ==
                     target->metaOutputs.end());

    updated.insert(targetID);
    target->injected.insert(unit);

    setUnitInputs(unit, target, new SegmentProcessingHandler::SetInjectInputDispatch(target),
                  new SegmentProcessingHandler::SetPureInputMetaDispatch(target));
}

bool SegmentProcessingHandler::Handler::isHandled(const SequenceName &unit)
{
    auto d = parent->dispatch.find(unit);
    if (d == parent->dispatch.end())
        return false;
    return d->second.size() > 1;
}

void SegmentProcessingHandler::Handler::deferHandling(const SequenceName &unit)
{
    auto &dData = parent->dispatchDefaultFind(unit);
    if (dData.size() > 1)
        return;
    DefaultDispatch *dDefault = static_cast<DefaultDispatch *>(dData.front());
    if (dDefault->reader)
        return;

    dDefault->reader.reset(new SequenceSegment::Stream);
}

SegmentProcessingHandler::Set::Set(SegmentProcessingHandler *setParent, int setID) : parent(
        setParent),
                                                                                     id(setID),
                                                                                     reader(),
                                                                                     pendingProcessing(),
                                                                                     outputs(),
                                                                                     inputs(),
                                                                                     injected(),
                                                                                     metaReader(),
                                                                                     metaPendingProcessing(),
                                                                                     metaOutputs()
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        ,
                                                                                     lastIncomingTime(
                                                                                             FP::undefined())
#endif
{ }

#ifdef BASICFILTER_STREAM_TIME_DEBUGGING

static void checkSegmentStream(const std::deque<SequenceSegment> &existing,
                               const SequenceSegment::Transfer &incoming)
{
    if (incoming.empty())
        return;
    if (!existing.empty()) {
        Q_ASSERT(Range::compareStart(existing.back().getStart(), incoming.front().getStart()) <= 0);
    }
    for (auto a = incoming.cbegin() + 1, endA = incoming.cend(); a != endA; ++a) {
        Q_ASSERT(Range::compareStart((a - 1)->getStart(), a->getStart()) <= 0);
    }
}

#endif

inline void SegmentProcessingHandler::Set::incomingData(const SequenceValue &value)
{
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
    Q_ASSERT(Range::compareStart(lastIncomingTime, value.getStart()) <= 0);
    lastIncomingTime = value.getStart();
#endif
    auto results = reader.add(value.getIdentity(), value.root());
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
    checkSegmentStream(pendingProcessing, results);
#endif
    if (!results.empty()) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(parent->lastOutputTime, results.front().getStart()) <= 0);
#endif
        Util::append(std::move(results), pendingProcessing);
    }
}

inline void SegmentProcessingHandler::Set::incomingInjected(const SequenceValue &value)
{
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
    Q_ASSERT(Range::compareStart(lastIncomingTime, value.getStart()) <= 0);
    lastIncomingTime = value.getStart();
#endif

    auto results = reader.inject(value.getIdentity(), value.root());
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
    checkSegmentStream(pendingProcessing, results);
#endif
    if (!results.empty()) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(parent->lastOutputTime, results.front().getStart()) <= 0);
#endif
        Util::append(std::move(results), pendingProcessing);
    }
}

inline void SegmentProcessingHandler::Set::incomingMeta(const SequenceValue &value)
{
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
    Q_ASSERT(Range::compareStart(lastIncomingTime, value.getStart()) <= 0);
    lastIncomingTime = value.getStart();
#endif

    auto results = metaReader.add(value.getIdentity(), value.root());
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
    checkSegmentStream(metaPendingProcessing, results);
#endif
    if (!results.empty()) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(parent->lastOutputTime, results.front().getStart()) <= 0);
#endif
        Util::append(std::move(results), metaPendingProcessing);
    }
}

inline void SegmentProcessingHandler::Set::advanceTime(double time)
{
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
    Q_ASSERT(Range::compareStart(lastIncomingTime, time) <= 0);
    lastIncomingTime = time;
#endif

    auto results = reader.advance(time);
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
    checkSegmentStream(pendingProcessing, results);
#endif
    if (!results.empty()) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(parent->lastOutputTime, results.front().getStart()) <= 0);
#endif
        Util::append(std::move(results), pendingProcessing);
    }

    results = metaReader.advance(time);
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
    checkSegmentStream(metaPendingProcessing, results);
#endif
    if (!results.empty()) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(parent->lastOutputTime, results.front().getStart()) <= 0);
#endif
        Util::append(std::move(results), metaPendingProcessing);
    }
}

void SegmentProcessingHandler::Set::finish()
{
    auto results = reader.finish();
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
    checkSegmentStream(pendingProcessing, results);
#endif
    if (!results.empty()) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(parent->lastOutputTime, results.front().getStart()) <= 0);
#endif
        Util::append(std::move(results), pendingProcessing);
    }

    results = metaReader.finish();
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
    checkSegmentStream(metaPendingProcessing, results);
#endif
    if (!results.empty()) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(parent->lastOutputTime, results.front().getStart()) <= 0);
#endif
        Util::append(std::move(results), metaPendingProcessing);
    }
}


bool SegmentProcessingHandler::Dispatch::referencesSet(const Set *set) const
{
    Q_UNUSED(set);
    return false;
}

SegmentProcessingHandler::DefaultDispatch::DefaultDispatch() : latest(), reader()
{ allowBypass = true; }

SegmentProcessingHandler::DefaultDispatch::~DefaultDispatch() = default;

void SegmentProcessingHandler::DefaultDispatch::incoming(const SequenceValue &value)
{
    if (!latest)
        latest.reset(new SequenceValue(value));
    else
        *latest = value;
    if (reader)
        reader->add(value.getIdentity(), value.root());
}

void SegmentProcessingHandler::DefaultDispatch::advanceTime(double time)
{
    if (reader)
        reader->advance(time);
}

void SegmentProcessingHandler::DefaultDispatch::overlay(SequenceSegment::Stream &target,
                                                        double effectiveStart) const
{
    if (reader) {
        target.overlay(*reader);
    }
    if (latest) {
        target.overlay(*latest);
    }
    target.advance(effectiveStart);
}

SegmentProcessingHandler::SetDataDispatch::SetDataDispatch(Set *setTarget) : target(setTarget)
{ allowBypass = false; }

void SegmentProcessingHandler::SetDataDispatch::incoming(const SequenceValue &value)
{ target->incomingData(value); }

bool SegmentProcessingHandler::SetDataDispatch::referencesSet(const Set *set) const
{ return set == target; }

SegmentProcessingHandler::SetMetaDispatch::SetMetaDispatch(Set *setTarget) : target(setTarget)
{ allowBypass = false; }

void SegmentProcessingHandler::SetMetaDispatch::incoming(const SequenceValue &value)
{ target->incomingMeta(value); }

bool SegmentProcessingHandler::SetMetaDispatch::referencesSet(const Set *set) const
{ return set == target; }

SegmentProcessingHandler::SetPureInputDispatch::SetPureInputDispatch(Set *setTarget) : target(
        setTarget)
{ allowBypass = true; }

void SegmentProcessingHandler::SetPureInputDispatch::incoming(const SequenceValue &value)
{ target->incomingData(value); }

bool SegmentProcessingHandler::SetPureInputDispatch::referencesSet(const Set *set) const
{ return set == target; }

SegmentProcessingHandler::SetInjectInputDispatch::SetInjectInputDispatch(Set *setTarget) : target(
        setTarget)
{ allowBypass = true; }

void SegmentProcessingHandler::SetInjectInputDispatch::incoming(const SequenceValue &value)
{ target->incomingInjected(value); }

bool SegmentProcessingHandler::SetInjectInputDispatch::referencesSet(const Set *set) const
{ return set == target; }

SegmentProcessingHandler::SetPureInputMetaDispatch::SetPureInputMetaDispatch(Set *setTarget)
        : target(setTarget)
{ allowBypass = true; }

void SegmentProcessingHandler::SetPureInputMetaDispatch::incoming(const SequenceValue &value)
{ target->incomingMeta(value); }

bool SegmentProcessingHandler::SetPureInputMetaDispatch::referencesSet(const Set *set) const
{ return set == target; }

void SegmentProcessingHandler::flushUndefined()
{
    SequenceValue::Transfer output;

    {
        auto begin = bypassed.begin();
        auto end = bypassed.end();
        auto last = Range::lastUndefinedStart(begin, end);

#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        if (last != begin) {
            Q_ASSERT(!FP::defined(lastOutputTime));
            for (auto check = begin; check != last; ++check) {
                lastOutputTime = check->getStart();
                Q_ASSERT(!FP::defined(lastOutputTime));
            }
        }
#endif
        std::move(begin, last, Util::back_emplacer(output));
        bypassed.erase(begin, last);
    }

    for (const auto &set : sets) {
        auto begin = set->metaPendingProcessing.begin();
        auto end = set->metaPendingProcessing.end();
        auto last = Range::lastUndefinedStart(begin, end);

#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        if (last != begin) {
            Q_ASSERT(!FP::defined(lastOutputTime));
            for (auto check = begin; check != last; ++check) {
                lastOutputTime = check->getStart();
                Q_ASSERT(!FP::defined(lastOutputTime));
            }
        }
#endif

        for (auto p = begin; p != last; ++p) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
            double originalEnd = p->getEnd();
#endif

            filter->processMeta(set->id, *p);

#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
            Q_ASSERT(!FP::defined(p->getStart()));
            Q_ASSERT(FP::equal(originalEnd, p->getEnd()));
#endif

            for (const auto &out : set->metaOutputs) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
                Q_ASSERT(output.empty() ||
                                 Range::compareStart(output.back().getStart(), p->getStart()) <= 0);
#endif
                if (!p->exists(out))
                    continue;

                output.emplace_back(p->getSequenceValue(out));
            }
        }
        set->metaPendingProcessing.erase(begin, last);


        begin = set->pendingProcessing.begin();
        end = set->pendingProcessing.end();
        last = Range::lastUndefinedStart(begin, end);

        for (auto p = begin; p != last; ++p) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
            double originalEnd = p->getEnd();
#endif

            filter->process(set->id, *p);

#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
            Q_ASSERT(!FP::defined(p->getStart()));
            Q_ASSERT(FP::equal(originalEnd, p->getEnd()));
#endif

            for (const auto &out : set->metaOutputs) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
                Q_ASSERT(output.empty() ||
                                 Range::compareStart(output.back().getStart(), p->getStart()) <= 0);
#endif
                if (!p->exists(out))
                    continue;

                output.emplace_back(p->getSequenceValue(out));
            }
        }
        set->pendingProcessing.erase(begin, last);
    }

    if (output.empty())
        return;

    egress->incomingData(std::move(output));
}

void SegmentProcessingHandler::dumpBypassedBefore(double before, SequenceValue::Transfer &output)
{
    Q_ASSERT(FP::defined(before));

    auto begin = bypassed.begin();
    auto end = bypassed.end();
    auto last = Range::heuristicUpperBoundDefined(begin, end, before);
    if (last == begin) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(bypassed.empty() || bypassed.front().getStart() > before);
#endif
        return;
    }

#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
    for (auto check = begin; check != last; ++check) {
        Q_ASSERT(Range::compareStart(lastOutputTime, check->getStart()) <= 0);
        Q_ASSERT(Range::compareStart(check->getStart(), before) <= 0);
        lastOutputTime = check->getStart();
    }
    if (last != end) {
        Q_ASSERT(last->getStart() > before);
    }
#endif

    std::move(begin, last, Util::back_emplacer(output));
    bypassed.erase(begin, last);
}

void SegmentProcessingHandler::FlushState::injectMeta(Set *set)
{
    if (!set->metaPendingProcessing.empty()) {
        startTiming.emplace(set->metaPendingProcessing.front().getStart(),
                            TimingElement(TimingElement::Metadata, set));
    } else if (!finish) {
        double nextTime = set->metaReader.nextTransitionTime();
        if (FP::defined(nextTime)) {
            startTiming.emplace(nextTime, TimingElement(TimingElement::Limiter, set));
        }
    }
}

void SegmentProcessingHandler::FlushState::injectData(Set *set)
{
    if (!set->pendingProcessing.empty()) {
        startTiming.emplace(set->pendingProcessing.front().getStart(),
                            TimingElement(TimingElement::Data, set));
    } else if (!finish) {
        double nextTime = set->reader.nextTransitionTime();
        if (FP::defined(nextTime)) {
            startTiming.emplace(nextTime, TimingElement(TimingElement::Limiter, set));
        }
    }
}

bool SegmentProcessingHandler::processState(FlushState &state, SequenceValue::Transfer &output)
{
    if (state.startTiming.empty())
        return false;

    double effectiveTime = state.startTiming.begin()->first;
    if (!state.finish && effectiveTime > state.before)
        return false;
    dumpBypassedBefore(effectiveTime, output);

    auto working = std::move(state.startTiming.begin()->second);
    if (working.type == FlushState::TimingElement::Limiter) {
        Q_ASSERT(!state.finish);

        auto begin = state.startTiming.begin();
        auto end = state.startTiming.end();
        auto last = state.startTiming.begin();

        do {
            ++last;
            if (last == end)
                return false;
            if (last->first != effectiveTime)
                return false;

            working = std::move(last->second);

#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
            effectiveTime = last->first;
#endif
        } while (working.type == FlushState::TimingElement::Limiter);

        ++last;
        state.startTiming.erase(begin, last);
    } else {
        state.startTiming.erase(state.startTiming.begin());
    }

    switch (working.type) {
    case FlushState::TimingElement::Limiter:
        Q_ASSERT(false);
        break;

    case FlushState::TimingElement::Metadata: {
        Q_ASSERT(!working.set->metaPendingProcessing.empty());

        auto &p = working.set->metaPendingProcessing.front();

#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        double originalStart = p.getStart();
        double originalEnd = p.getEnd();
        Q_ASSERT(FP::equal(originalStart, effectiveTime));
        Q_ASSERT(Range::compareStart(lastOutputTime, originalStart) <= 0);
        lastOutputTime = originalStart;
#endif

        filter->processMeta(working.set->id, p);

#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(FP::equal(originalStart, p.getStart()));
        Q_ASSERT(FP::equal(originalEnd, p.getEnd()));
#endif
        for (const auto &name : working.set->metaOutputs) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
            Q_ASSERT(output.empty() ||
                             Range::compareStart(output.back().getStart(), p.getStart()) <= 0);
#endif
            if (!p.exists(name))
                continue;
            output.emplace_back(SequenceIdentity(name, p.getStart(), p.getEnd()),
                                p.takeValue(name));
        }

        working.set->metaPendingProcessing.pop_front();

        state.injectMeta(working.set);
        return true;
    }

    case FlushState::TimingElement::Data: {
        Q_ASSERT(!working.set->pendingProcessing.empty());

        auto &p = working.set->pendingProcessing.front();

#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        double originalStart = p.getStart();
        double originalEnd = p.getEnd();
        Q_ASSERT(FP::equal(originalStart, effectiveTime));
        Q_ASSERT(Range::compareStart(lastOutputTime, originalStart) <= 0);
        lastOutputTime = originalStart;
#endif

        filter->process(working.set->id, p);

#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(FP::equal(originalStart, p.getStart()));
        Q_ASSERT(FP::equal(originalEnd, p.getEnd()));
#endif
        for (const auto &name : working.set->outputs) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
            Q_ASSERT(output.empty() ||
                             Range::compareStart(output.back().getStart(), p.getStart()) <= 0);
#endif
            if (!p.exists(name))
                continue;
            output.emplace_back(SequenceIdentity(name, p.getStart(), p.getEnd()),
                                p.takeValue(name));
        }

        working.set->pendingProcessing.pop_front();

        state.injectData(working.set);
        return true;
    }
    }

    Q_ASSERT(false);
    return false;
}

void SegmentProcessingHandler::flushData(double before, bool finish)
{
    if (FP::defined(before)) {
        for (const auto &set : sets) {
            set->advanceTime(before);
        }
        for (const auto &d : dispatch) {
            static_cast<DefaultDispatch *>(d.second.front())->advanceTime(before);
        }
    } else if (!finish) {
        return;
    }

    Q_ASSERT(FP::defined(before) || finish);

    /* First, emit all undefined start values, since they always come unconditionally first */
    flushUndefined();

    FlushState state;
    state.finish = finish;
    state.before = before;
    for (const auto &set : sets) {
        state.injectMeta(set.get());
        state.injectData(set.get());
    }

    SequenceValue::Transfer output;
    while (processState(state, output)) {
        if (output.size() > 16384) {
            egress->incomingData(std::move(output));
            output.clear();
        }
    }

    /* No start timing means the full bypass can be output (nothing holding it back) */
    if (state.startTiming.empty() || finish) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        for (const auto &check : bypassed) {
            Q_ASSERT(Range::compareStart(lastOutputTime, check.getStart()) <= 0);
            lastOutputTime = check.getStart();
        }
#endif

        Util::append(std::move(bypassed), output);
        bypassed.clear();
    }

    if (!output.empty()) {
        egress->incomingData(std::move(output));
    }
}

std::vector<
        SegmentProcessingHandler::Dispatch *> &SegmentProcessingHandler::dispatchIncoming(const SequenceValue &inc)
{
    auto d = dispatch.find(inc.getUnit());
    if (d != dispatch.end())
        return d->second;


    const auto &name = inc.getName();
    if (name.isDefaultStation())
        return dispatchDefaultFind(name);

    dispatchDefaultFind(name);
    handler.effectiveStart = inc.getStart();

    if (name.isMeta()) {
        filter->unhandled(name.fromMeta(), &handler);
    } else {
        filter->unhandled(name, &handler);
    }

    if (!handler.updated.isEmpty()) {
        for (auto i : handler.updated) {
            sets[i]->metaReader.setBreaks(filter->metadataBreaks(i));
            std::sort(sets[i]->outputs.begin(), sets[i]->outputs.end(),
                      SequenceName::OrderLogical());
            std::sort(sets[i]->metaOutputs.begin(), sets[i]->metaOutputs.end(),
                      SequenceName::OrderLogical());
        }
        pruneUnusedDispatch();
        handler.updated.clear();
    }

    /* Hash may have been modified by handler, so need to
     * repeat the lookup */
    return dispatch.find(name)->second;
}

void SegmentProcessingHandler::process(SequenceValue::Transfer &&incoming)
{
    if (incoming.empty())
        return;

    double valueStartTime = incoming.front().getStart();
    int flushCounter = 1;
    for (auto &inc : incoming) {
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        Q_ASSERT(Range::compareStart(valueStartTime, inc.getStart()) <= 0);
#endif
        valueStartTime = inc.getStart();

        auto &d = dispatchIncoming(inc);

        bool outputBypassed = true;
        for (auto dis : d) {
            outputBypassed = outputBypassed && dis->allowBypass;
            dis->incoming(inc);
        }

        if (outputBypassed) {
            /* Have to make a copy of it if any dispatch used it, since they need to
             * retain handles and the bypass may be directly moved to the output */
            if (d.empty()) {
                bypassed.emplace_back(std::move(inc));
            } else {
                bypassed.emplace_back(inc);
            }
        }

        if (flushCounter % 16384 == 0) {
            flushData(valueStartTime);
        }
        ++flushCounter;
    }

    flushData(valueStartTime);
}

void SegmentProcessingHandler::finish()
{
    for (const auto &set : sets) {
        set->finish();
    }
    flushData(FP::undefined(), true);

    egress->endData();
}

template<typename T>
static QDataStream &operator<<(QDataStream &stream, const std::vector<T> &list)
{
    stream << (quint32) list.size();
    for (typename std::vector<T>::const_iterator v = list.begin(), endV = list.end();
            v != endV;
            ++v) {
        stream << *v;
    }
    return stream;
}

template<typename T>
static QDataStream &operator>>(QDataStream &stream, std::vector<T> &list)
{
    quint32 n;
    stream >> n;
    list.clear();
    list.reserve(n);
    for (quint32 i = 0; i < n; i++) {
        T v;
        stream >> v;
        list.push_back(v);
    }
    return stream;
}

SegmentProcessingHandler::Set::Set(SegmentProcessingHandler *setParent,
                                   int setID, QDataStream &stream) : parent(setParent),
                                                                     id(setID),
                                                                     reader(),
                                                                     pendingProcessing(),
                                                                     outputs(),
                                                                     inputs(),
                                                                     injected(),
                                                                     metaReader(),
                                                                     metaPendingProcessing(),
                                                                     metaOutputs()
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        ,
                                                                     lastIncomingTime(
                                                                             FP::undefined())
#endif
{
    stream >> reader >> pendingProcessing >> outputs >> inputs >> injected;
    stream >> metaReader >> metaPendingProcessing >> metaOutputs;
}

void SegmentProcessingHandler::Set::serialize(QDataStream &stream) const
{
    stream << reader << pendingProcessing << outputs << inputs << injected;
    stream << metaReader << metaPendingProcessing << metaOutputs;
}

enum BasicFilterHandlerDispatchType {
    DISPATCH_DEFAULT = 0,
    DISPATCH_DATA,
    DISPATCH_METADATA,
    DISPATCH_PUREINPUT,
    DISPATCH_INJECTINPUT,
    DISPATCH_PUREINPUTMETA,
};


/**
 * Create a handler from de-serializing the given stream.  This takes
 * ownership of the filter (which should have been de-serialized before
 * calling this) if specified.
 * 
 * @param stream    the stream to read from
 * @param setFilter the filter
 * @param takeOwnership if true then ownership of the filter is transferred
 */
SegmentProcessingHandler::SegmentProcessingHandler(QDataStream &stream,
                                                   SegmentProcessingStage *setFilter,
                                                   bool takeOwnership) : AsyncProcessingStage(
        stream),
                                                                         handler(this),
                                                                         filter(setFilter),
                                                                         ownFilter(),
                                                                         sets(),
                                                                         dispatch(),
                                                                         uniqueDispatch(),
                                                                         bypassed()
#ifdef BASICFILTER_STREAM_TIME_DEBUGGING
        ,
                                                                         lastOutputTime(
                                                                                 FP::undefined())
#endif
{
    Q_ASSERT(filter);
    if (takeOwnership)
        ownFilter.reset(filter);

    stream >> bypassed;

    quint32 n = 0;
    stream >> n;
    for (quint32 i = 0; i < n; ++i) {
        sets.emplace_back(new Set(this, (int) i, stream));
    }

    stream >> n;
    for (quint32 i = 0; i < n; ++i) {
        Dispatch *dis = nullptr;

        BasicFilterHandlerDispatchType type;
        {
            quint8 ti;
            stream >> ti;
            type = static_cast<BasicFilterHandlerDispatchType>(ti);
        }

        switch (type) {
        case DISPATCH_DEFAULT: {
            DefaultDispatch *dt = new DefaultDispatch;
            bool hasLatest = false;
            stream >> hasLatest;
            if (hasLatest) {
                dt->latest.reset(new SequenceValue);
                stream >> *(dt->latest);
            }
            bool hasReader = false;
            stream >> hasReader;
            if (hasReader) {
                dt->reader.reset(new SequenceSegment::Stream);
                stream >> *(dt->reader);
            }
            dis = dt;
            break;
        }

        case DISPATCH_DATA: {
            quint32 idxSet;
            stream >> idxSet;
            dis = new SetDataDispatch(sets[idxSet].get());
            break;
        }

        case DISPATCH_METADATA: {
            quint32 idxSet;
            stream >> idxSet;
            dis = new SetMetaDispatch(sets[idxSet].get());
            break;
        }

        case DISPATCH_PUREINPUT: {
            quint32 idxSet;
            stream >> idxSet;
            dis = new SetPureInputDispatch(sets[idxSet].get());
            break;
        }

        case DISPATCH_INJECTINPUT: {
            quint32 idxSet;
            stream >> idxSet;
            dis = new SetInjectInputDispatch(sets[idxSet].get());
            break;
        }

        case DISPATCH_PUREINPUTMETA: {
            quint32 idxSet;
            stream >> idxSet;
            dis = new SetPureInputMetaDispatch(sets[idxSet].get());
            break;
        }

        default:
            Q_ASSERT(false);
            break;
        }
        uniqueDispatch.emplace_back(dis);
    }

    stream >> n;
    for (quint32 i = 0; i < n; ++i) {
        SequenceName unit;
        stream >> unit;

        std::vector<Dispatch *> dl;
        Deserialize::container(stream, dl, [this, &stream]() {
            quint32 index = 0;
            stream >> index;
            Q_ASSERT(index < uniqueDispatch.size());
            return uniqueDispatch[index].get();
        });

        dispatch.emplace(std::move(unit), std::move(dl));
    }
}

void SegmentProcessingHandler::DefaultDispatch::serialize(QDataStream &stream) const
{
    stream << (quint8) DISPATCH_DEFAULT;
    if (latest) {
        stream << true;
        stream << *latest;
    } else {
        stream << false;
    }
    if (reader) {
        stream << true;
        stream << *reader;
    } else {
        stream << false;
    }
}

void SegmentProcessingHandler::SetDataDispatch::serialize(QDataStream &stream) const
{
    stream << (quint8) DISPATCH_DATA;
    stream << (quint32) target->id;
}

void SegmentProcessingHandler::SetMetaDispatch::serialize(QDataStream &stream) const
{
    stream << (quint8) DISPATCH_METADATA;
    stream << (quint32) target->id;
}

void SegmentProcessingHandler::SetPureInputDispatch::serialize(QDataStream &stream) const
{
    stream << (quint8) DISPATCH_PUREINPUT;
    stream << (quint32) target->id;
}

void SegmentProcessingHandler::SetInjectInputDispatch::serialize(QDataStream &stream) const
{
    stream << (quint8) DISPATCH_INJECTINPUT;
    stream << (quint32) target->id;
}

void SegmentProcessingHandler::SetPureInputMetaDispatch::serialize(QDataStream &stream) const
{
    stream << (quint8) DISPATCH_PUREINPUTMETA;
    stream << (quint32) target->id;
}

/**
 * Serializes the basic filter into the given stream.  The actual filter is
 * written first, and so on the deserialize end must be loaded from the 
 * component first.
 * <br>
 * This requires that the output be paused (NULL egress).  It's also implied
 * that the thread will be terminated immediately after the call without
 * generating any more data (since that wouldn't make sense).
 * 
 * @param stream        the target stream
 */
void SegmentProcessingHandler::serialize(QDataStream &stream)
{
    filter->serialize(stream);

    AsyncProcessingStage::serialize(stream);

    stream << bypassed;

    stream << static_cast<quint32>(sets.size());
    for (quint32 i = 0, max = static_cast<quint32>(sets.size()); i < max; i++) {
        sets[i]->serialize(stream);
    }

    stream << static_cast<quint32>(uniqueDispatch.size());
    std::unordered_map<Dispatch *, quint32> dispatchIndex;
    for (quint32 i = 0, max = static_cast<quint32>(uniqueDispatch.size()); i < max; i++) {
        uniqueDispatch[i]->serialize(stream);
        dispatchIndex.emplace(uniqueDispatch[i].get(), i);
    }

    stream << static_cast<quint32>(dispatch.size());
    for (const auto &i : dispatch) {
        stream << i.first;
        Serialize::container(stream, i.second, [&stream, &dispatchIndex](Dispatch *item) {
            stream << dispatchIndex[item];
        });
    }
}

SequenceName::Set SegmentProcessingHandler::requestedInputs()
{ return filter->requestedInputs(); }

SequenceName::Set SegmentProcessingHandler::predictedOutputs()
{ return filter->predictedOutputs(); }

}
}
