/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATACOREDATAIOXML_H
#define CPD3DATACOREDATAIOXML_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QXmlSimpleReader>
#include <QXmlDefaultHandler>
#include <QXmlStreamWriter>

#include "datacore/datacore.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"

namespace CPD3 {
namespace Data {

/** @file datacore/dataioxml.h
 * The header for XML related data io.  Requires Qt's config to have XML 
 * to compile correctly.
 */

/**
 * A utility class that provides parsing for a single Value from an XML reader.
 * This is intended to be wrapped by another class implementing 
 * QXmlDefaultHandler and calling the functions (which are direct analogs of
 * several of the functions required) as needed to parse Values.
 */
class CPD3DATACORE_EXPORT XMLDataInputParser {
    QString lastError;

    QString auxData;
    QString elementData;
    enum ParseDataMode {
        IGNORE, DOUBLE, INTEGER, BOOLEAN, STRING, BINARY, FLAGNAME, STRINGLOCALE, OVERLAY
    };
    ParseDataMode elementDataMode;

    enum ParseElementMode {
        VALUE,
        FLAG,
        FLAGDATA,
        LSTRING,
        STRINGDATA,
        ARRAY,
        MATRIX,
        HASH,
        KEYFRAME,
        METASIMPLE,
        METAHASH,
        METAFLAGS
    };

    std::vector<Variant::Write> stack;
    QList<ParseElementMode> mode;
public:
    XMLDataInputParser();

    XMLDataInputParser(const XMLDataInputParser &other);

    XMLDataInputParser &operator=(const XMLDataInputParser &other);

    explicit XMLDataInputParser(Variant::Write output);

    /**
     * The status of the parsing operation.
     */
    enum ParseStatus {
        /** Parsing should continue. */
                CONTINUE = 0, /** Parsing the Value is finished. */
                FINISHED, /** An error occurred while parsing .*/
                ERROR
    };


    QString errorString() const;

    ParseStatus characters(const QString &ch);

    ParseStatus startElement(const QString &namespaceURI,
                             const QString &localName,
                             const QString &qName,
                             const QXmlAttributes &atts);

    ParseStatus
            endElement(const QString &namespaceURI, const QString &localName, const QString &qName);
};

/**
 * A utility class that provides parsing for value paths from an XML reader.
 * This is intended to be wrapped by another class implementing 
 * QXmlDefaultHandler and calling the functions (which are direct analogs of
 * several of the functions required) as needed to parse Values.  The XML 
 * stream should be positioned on the first element of the path.
 */
class CPD3DATACORE_EXPORT XMLDataPathParser {
    QString lastError;

    QString componentType;
    QString elementData;
    int inElementData;

    std::vector<Variant::PathElement::Encoded> path;
    Variant::Path *target;
public:
    XMLDataPathParser();

    XMLDataPathParser(const XMLDataPathParser &other);

    XMLDataPathParser &operator=(const XMLDataPathParser &other);

    XMLDataPathParser(Variant::Path *output);

    /**
     * The status of the parsing operation.
     */
    enum ParseStatus {
        /** Parsing should continue. */
                CONTINUE = 0, /** Parsing the Value is finished. */
                FINISHED, /** An error occurred while parsing .*/
                ERROR
    };


    QString errorString() const;

    ParseStatus characters(const QString &ch);

    ParseStatus startElement(const QString &namespaceURI,
                             const QString &localName,
                             const QString &qName,
                             const QXmlAttributes &atts);

    ParseStatus
            endElement(const QString &namespaceURI, const QString &localName, const QString &qName);
};

/**
 * Provides a simple interface for writing Values in XML.
 */
class CPD3DATACORE_EXPORT XMLDataOutput {
    XMLDataOutput()
    { }

public:
    static void write(QXmlStreamWriter *xml,
                      const Variant::Read &value,
                      int indent = 0,
                      int indentSpaces = 0);

    static void writePath(QXmlStreamWriter *xml, const Variant::Path &path,
                          int indent = 0,
                          int indentSpaces = 0);
};

}
}

#endif
