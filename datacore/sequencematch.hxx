/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATACORE_SEQUENCEMATCH_HXX
#define CPD3DATACORE_SEQUENCEMATCH_HXX

#include "core/first.hxx"

#include <vector>
#include <memory>

#include <QString>
#include <QDataStream>
#include <QDebug>

#include "datacore.hxx"
#include "stream.hxx"
#include "variant/handle.hxx"
#include "valueoptionparse.hxx"
#include "core/number.hxx"
#include "archive/selection.hxx"

namespace CPD3 {
namespace Data {

class SequenceSegment;

namespace SequenceMatch {

class Element;

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Element &e);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, Element &e);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const Element &e);

class Composite;

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Composite &c);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, Composite &c);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const Composite &c);

class OrderedLookup;

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const OrderedLookup &o);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, OrderedLookup &o);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const OrderedLookup &o);

class Basic;

CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const std::unique_ptr<Basic> &b);

CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, std::unique_ptr<Basic> &b);

CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const std::unique_ptr<Basic> &b);


/**
 * A single element of matching, this provides a basic interface
 * for testing if a sequence name matches.
 */
class CPD3DATACORE_EXPORT Element final {
    class MatchString {
    public:
        virtual ~MatchString();

        virtual bool matches(const SequenceName::Component &test) const = 0;

        virtual bool integrate(Archive::Selection::Match &list) const = 0;

        virtual bool neverMatches() const = 0;

        virtual bool canFill() const = 0;

        virtual SequenceName::Component fillValue() const = 0;

        virtual void serialize(QDataStream &stream) const = 0;

        virtual QString describe() const = 0;

        virtual MatchString *clone() const = 0;

        static MatchString *deserialize(QDataStream &stream);
    };

    class MatchStringAlways;

    class MatchStringNever;

    class MatchStringStatic;

    class MatchStringRegularExpression;


    class MatchSet {
    public:
        virtual ~MatchSet();

        virtual bool matches(const SequenceName::Flavors &test) const = 0;

        struct Integration {
            Archive::Selection::Match *has;
            Archive::Selection::Match *lacks;
            Archive::Selection::Match *exact;

            Integration();

            explicit Integration(Archive::Selection &target);

            void clear();

            void matchAny();
        };

        virtual bool integrate(Integration &target) const = 0;

        virtual void serialize(QDataStream &stream) const = 0;

        virtual bool neverMatches() const = 0;

        virtual bool canFill() const = 0;

        virtual SequenceName::Flavors fillValue() const = 0;

        virtual QString describe() const = 0;

        virtual MatchSet *clone() const = 0;

        static MatchSet *deserialize(QDataStream &stream);
    };

    class MatchSetAlways;

    class MatchSetNever;

    class MatchSetEmpty;

    class MatchSetStaticBase;

    class MatchSetRegularExpressionBase;

    class MatchSetStaticExact;

    class MatchSetRegularExpressionExact;

    class MatchSetStaticHas;

    class MatchSetRegularExpressionHas;

    class MatchSetStaticLacks;

    class MatchSetRegularExpressionLacks;


    class MatchComponent {
    public:
        virtual ~MatchComponent();

        virtual bool matches(const SequenceName &name) const = 0;

        virtual bool integrate(Archive::Selection &selection) const = 0;

        virtual bool neverMatches() const = 0;

        struct Contents {
            bool station;
            bool archive;
            bool variable;
            bool flavors;

            inline Contents() : station(false), archive(false), variable(false), flavors(false)
            { }

            inline Contents(bool station, bool archive, bool variable, bool flavors) : station(
                    station), archive(archive), variable(variable), flavors(flavors)
            { }

            Contents(const SequenceName &name, bool flavors);

            bool complete() const;
        };

        virtual bool fill(SequenceName &name, Contents &filled) const = 0;

        virtual bool appliesTo(const Contents &check) const = 0;

        virtual void serialize(QDataStream &stream) const = 0;

        virtual QString describe() const = 0;

        virtual MatchComponent *clone() const = 0;

        static MatchComponent *deserialize(QDataStream &stream);
    };

    class MatchComponentNever;

    template<typename BaseType, typename MatchType = MatchString>
    class MatchComponentSingle : public MatchComponent {
    protected:
        std::unique_ptr<MatchType> matcher;

        MatchComponentSingle() = default;

        MatchComponentSingle(MatchType *matcher) : matcher(matcher)
        { }

        MatchComponentSingle(std::unique_ptr<MatchType> &&matcher) : matcher(std::move(matcher))
        { }

        MatchComponentSingle(const MatchComponentSingle<BaseType, MatchType> &other) : matcher(
                other.matcher->clone())
        { }

        MatchComponentSingle(QDataStream &stream) : matcher(MatchType::deserialize(stream))
        { }

    public:
        virtual ~MatchComponentSingle() = default;

        virtual bool matches(const SequenceName &name) const
        { return matcher->matches(static_cast<const BaseType *>(this)->component(name)); }

        virtual bool integrate(Archive::Selection &selection) const
        {
            decltype(static_cast<const BaseType *>(this)->integrationTarget(selection))
                    target = static_cast<const BaseType *>(this)->integrationTarget(selection);
            target.clear();
            return matcher->integrate(target);
        }

        virtual bool fill(SequenceName &name, Contents &filled) const
        {
            if (appliesTo(filled))
                return matches(name);
            if (!matcher->canFill())
                return false;
            static_cast<const BaseType *>(this)->applyFill(name, matcher->fillValue(), filled);
            return true;
        }

        virtual bool neverMatches() const
        { return matcher->neverMatches(); }

        virtual void serialize(QDataStream &stream) const
        { matcher->serialize(stream); }
    };

    class MatchStationSingle;

    class MatchDefaultStationSingle;

    class MatchExcludeDefaultStation;

    class MatchArchiveSingle;

    class MatchMetaArchiveSingle;

    class MatchExcludeMetaArchive;

    class MatchVariableSingle;

    class MatchFlavorsSingle;


    template<typename BaseType, typename MatchType = MatchString>
    class MatchComponentComposite : public MatchComponent {
    protected:
        std::vector<std::unique_ptr<MatchType>> matchers;

        MatchComponentComposite() = default;

        MatchComponentComposite(std::vector<std::unique_ptr<MatchType>> &&matchers) : matchers(
                std::move(matchers))
        { }

        MatchComponentComposite(const MatchComponentComposite<BaseType, MatchType> &other)
        {
            for (const auto &m : other.matchers) {
                matchers.emplace_back(m->clone());
            }
        }

        MatchComponentComposite(QDataStream &stream) : matchers()
        {
            quint32 n = 0;
            stream >> n;
            for (quint32 i = 0; i < n; ++i) {
                matchers.emplace_back(MatchType::deserialize(stream));
            }
        }

    public:
        virtual ~MatchComponentComposite() = default;

        virtual bool matches(const SequenceName &name) const
        {
            auto check = static_cast<const BaseType *>(this)->component(name);
            for (const auto &m : matchers) {
                if (m->matches(check))
                    return true;
            }
            return false;
        }

        virtual bool integrate(Archive::Selection &selection) const
        {
            decltype(static_cast<const BaseType *>(this)->integrationTarget(selection))
                    target = static_cast<const BaseType *>(this)->integrationTarget(selection);
            target.clear();
            for (const auto &m : matchers) {
                if (!m->integrate(target))
                    return false;
            }
            return true;
        }

        virtual bool fill(SequenceName &name, Contents &filled) const
        {
            if (appliesTo(filled))
                return matches(name);
            for (const auto &m : matchers) {
                if (!m->canFill())
                    continue;
                SequenceName tempName = name;
                Contents tempFilled = filled;
                static_cast<const BaseType *>(this)->applyFill(tempName, m->fillValue(),
                                                               tempFilled);

                /* For successful filling, require that they all agree on the result */
                bool agreed = true;
                auto check = static_cast<const BaseType *>(this)->component(tempName);
                for (const auto &m2 : matchers) {
                    if (!m2->matches(check)) {
                        agreed = false;
                        break;
                    }
                }
                if (!agreed)
                    continue;
                name = std::move(tempName);
                filled = std::move(tempFilled);
                return true;
            }
            return false;
        }

        virtual bool neverMatches() const
        {
            for (const auto &m : matchers) {
                if (m->neverMatches())
                    return true;
            }
            return false;
        }

        virtual void serialize(QDataStream &stream) const
        {
            stream << (quint32) matchers.size();
            for (const auto &m : matchers) {
                m->serialize(stream);
            }
        }

        virtual QString describe() const
        {
            QStringList desc;
            for (const auto &m : matchers) {
                desc.append(m->describe());
            }
            return QString("{%1}").arg(desc.join(','));
        }
    };

    class MatchStationComposite;

    class MatchDefaultStationComposite;

    class MatchArchiveComposite;

    class MatchMetaArchiveComposite;

    class MatchVariableComposite;

    class MatchFlavorsComposite;


    std::vector<std::unique_ptr<MatchComponent>> matchers;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Element &select);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, Element &select);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const Element &select);

    friend class Composite;

    friend class Basic;

public:
    /**
     * The type used for regular expression patterns.
     */
    typedef QString Pattern;

    /**
     * The type used for a list regular expression patterns.
     */
    typedef QStringList PatternList;

    Element();

    Element(const Element &other);

    Element &operator=(const Element &other);

    Element(Element &&other);

    Element &operator=(Element &&other);

    /**
     * Special pre-configured match types.
     */
    enum class SpecialMatch {
        /** Match no sequence names. */
                None,

        /** Match all sequence names. */
                All,

        /** Match only "data" sequence names, excluding the default
         * flavors. */
                Data
    };

    /**
     * Create an element that matches a special type.
     *
     * @param specialMatch  the special match type
     */
    Element(SpecialMatch specialMatch);

    /**
     * Create the matching element.
     *
     * @param station   the regular expression matching the station
     * @param archive   the regular expression matching the archive
     * @param variable  the regular expression matching the variable
     */
    Element(const Pattern &station, const Pattern &archive, const Pattern &variable);

    /**
     * Create the matching element.
     *
     * @param station       the regular expression matching the station
     * @param archive       the regular expression matching the archive
     * @param variable      the regular expression matching the variable
     * @param hasFlavors    the regular expression of required flavors
     * @param lacksFlavors  the regular expression of excluded flavors
     */
    Element(const Pattern &station,
            const Pattern &archive,
            const Pattern &variable,
            PatternList hasFlavors,
            PatternList lacksFlavors);

    Element(const SequenceName::Component &station,
            const SequenceName::Component &archive,
            const SequenceName::Component &variable,
            std::vector<SequenceName::Component> hasFlavors,
            std::vector<SequenceName::Component> lacksFlavors);

    /**
     * Create the matching element.
     *
     * @param station       the regular expression matching the station
     * @param archive       the regular expression matching the archive
     * @param variable      the regular expression matching the variable
     * @param exactFlavors  the regular expressions of exact flavors required
     */
    Element(const Pattern &station,
            const Pattern &archive,
            const Pattern &variable,
            const SequenceName::Flavors &exactFlavors);

    Element(const SequenceName::Component &station,
            const SequenceName::Component &archive,
            const SequenceName::Component &variable,
            const SequenceName::Flavors &exactFlavors);

    /**
     * Create the matching element.
     *
     * @param stations      the regular expression of accepted stations
     * @param archives      the regular expression of accepted archives
     * @param variables     the regular expression of accepted variables
     */
    Element(PatternList stations, PatternList archives, PatternList variables);

    /**
     * Create the matching element.
     *
     * @param stations      the regular expression of accepted stations
     * @param archives      the regular expression of accepted archives
     * @param variables     the regular expression of accepted variables
     * @param hasFlavors    the regular expression of required flavors
     * @param lacksFlavors  the regular expression of excluded flavors
     */
    Element(PatternList stations,
            PatternList archives,
            PatternList variables,
            PatternList hasFlavors,
            PatternList lacksFlavors);

    /**
     * Create the matching element.
     *
     * @param stations      the regular expression of accepted stations
     * @param archives      the regular expression of accepted archives
     * @param variables     the regular expression of accepted variables
     * @param exactFlavors  the regular expressions of exact flavors required
     */
    Element(PatternList stations, PatternList archives, PatternList variables,
            const SequenceName::Flavors &exactFlavors);

    /**
     * The options used when creating an element matcher from a configuration.
     */
    struct CPD3DATACORE_EXPORT ConfigurationOptions {
        /**
         * Accept metadata archives in addition to any explicitly specified.
         */
        bool acceptMetadata;

        /**
         * Accept the default station in addition to any explicitly specified.
         */
        bool acceptDefaultStation;

        ConfigurationOptions();
    };

    /**
     * Create the matching element.
     *
     * @param config                the configuration
     */
    explicit Element(const Variant::Read &config,
                     const ConfigurationOptions &options = ConfigurationOptions());

    /**
     * Create the matching element.
     *
     * @param config                the configuration
     * @param defaultStations       the regular expression of accepted stations set by default
     * @param defaultArchives       the regular expression of accepted archives set by default
     * @param defaultVariables      the regular expression of accepted variables set by default
     * @param defaultHasFlavors     the regular expression of required flavors set by default
     * @param defaultLacksFlavors   the regular expression of excluded flavors set by default
     * @param options               the options applied to the configuration
     */
    Element(const Variant::Read &config,
            PatternList defaultStations,
            PatternList defaultArchives,
            PatternList defaultVariables,
            PatternList defaultHasFlavors,
            PatternList defaultLacksFlavors,
            const ConfigurationOptions &options = ConfigurationOptions());

    /**
     * Create the matching element.
     *
     * @param config                the configuration
     * @param defaultStations       the regular expression of accepted stations set by default
     * @param defaultArchives       the regular expression of accepted archives set by default
     * @param defaultVariables      the regular expression of accepted variables set by default
     * @param options               the options applied to the configuration
     */
    Element(const Variant::Read &config,
            PatternList defaultStations,
            PatternList defaultArchives,
            PatternList defaultVariables,
            const ConfigurationOptions &options = ConfigurationOptions());

    /**
     * Create the matching element.
     *
     * @param config                the configuration
     * @param defaultStations       the regular expression of accepted stations set by default
     * @param defaultArchives       the regular expression of accepted archives set by default
     * @param defaultVariables      the regular expression of accepted variables set by default
     * @param exactFlavors          the regular expressions of exact flavors required set by default
     * @param options               the options applied to the configuration
     */
    Element(const Variant::Read &config,
            PatternList defaultStations,
            PatternList defaultArchives,
            PatternList defaultVariables,
            const SequenceName::Flavors &defaultExactFlavors,
            const ConfigurationOptions &options = ConfigurationOptions());

    /**
     * Create the element matching an exact sequence name.
     *
     * @param name  the sequence name
     */
    Element(const SequenceName &name);

    /**
     * Create an element that matches a single variable.
     *
     * @param variable  the variable to match
     * @return          a matching element
     */
    static Element variable(const Pattern &variable);

    /**
     * Test if the element matches a sequence name.
     *
     * @param name  the sequence name
     * @return      true if the sequence name matches
     */
    bool matches(const SequenceName &name) const;

    /**
     * Convert the element matcher to an approximate archive selection.
     * The resulting selection may match more than what the element exactly
     * specifies.
     *
     * @param defaultStations   the stations to set by default
     * @param defaultArchives   the archives to set by default
     * @param defaultVariables  the variable to set by default
     * @return                  an approximate archive selection
     */
    Archive::Selection toArchiveSelection(const Archive::Selection::Match &defaultStations = {},
                                          const Archive::Selection::Match &defaultArchives = {},
                                          const Archive::Selection::Match &defaultVariables = {}) const;

    /**
     * Test if the element can never match anything.
     *
     * @return  true if the element will never match anything
     */
    bool neverMatches() const;

    /**
     * Reduce the element to a single sequence name.
     *
     * @return          the reduced name
     */
    SequenceName reduce() const;

    /**
     * Attempt to fill the missing components of the name with any
     * reducable ones and return false if it does not match, or does
     * not provide a completely filled name.
     *
     * @param original          the original, possibly incomplete name
     * @param definedFlavors    true of the original has flavors set
     * @return                  true if the name was filled successfully
     */
    bool fill(SequenceName &original, bool definedFlavors = true) const;

    /**
     * Override the element to exactly match the given target, with
     * whatever components of it are valid (non-empty)
     *
     * @param target            the target name
     * @param includeFlavors    include the flavors as defined in the target
     */
    void setMatch(const SequenceName &target, bool includeFlavors = true);

    /**
     * Clear all matching components of the element
     *
     * @param station       clear any station matching
     * @param archive       clear any archive matching
     * @param variable      clear any variable matching
     * @param flavors       clear any flavors matching
     */
    void clearMatch(bool station, bool archive, bool variable, bool flavors = false);


    /**
     * Get a string description of the element.
     *
     * @return      a string description
     */
    QString describe() const;

    /**
     * Test if any of the elements in the container match the specified name.
     *
     * @tparam Container    an iterable container
     * @param container     the container
     * @param name          the name to match
     * @return              true if any of the elements in the container match
     */
    template<typename Container>
    static bool matchesAny(const Container &container, const SequenceName &name)
    {
        for (const auto &e : container) {
            if (e.matches(name))
                return true;
        }
        return false;
    }

private:
    Element(const Variant::Read &config,
            const ConfigurationOptions &options,
            PatternList defaultStations,
            PatternList defaultArchives,
            PatternList defaultVariables,
            PatternList defaultHasFlavors,
            PatternList defaultLacksFlavors,
            bool useExactFlavors);
};


/**
 * A basic sequence matcher that only provides an interface to determine
 * if a sequence name matches any component element.
 */
class CPD3DATACORE_EXPORT Composite {
    std::vector<Element> elements;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const Composite &c);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, Composite &c);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const Composite &c);

public:
    Composite();

    virtual ~Composite();

    Composite(const Composite &other);

    Composite &operator=(const Composite &other);

    Composite(Composite &&other);

    Composite &operator=(Composite &&other);

    /**
     * Create a composite matcher of a single element.
     *
     * @param element       the matching element
     */
    explicit Composite(const Element &element);

    /**
     * Create a composite matcher of a single element.
     *
     * @param element       the matching element
     */
    explicit Composite(Element &&element);

    /**
     * Create a composite matcher from a list of elements
     *
     * @param elements      the matching elements
     */
    explicit Composite(const std::vector<Element> &elements);

    /**
     * Create a composite matcher from a list of elements
     *
     * @param elements      the matching elements
     */
    explicit Composite(std::vector<Element> &&elements);

    /**
     * The options for configuring a composite matcher
     */
    struct CPD3DATACORE_EXPORT ConfigurationOptions : public Element::ConfigurationOptions {

    };

    /**
     * Create a composite matcher from a configuration.
     *
     * @param config        the configuration
     * @param options       the options applied to the configuration
     */
    explicit Composite(const Variant::Read &config,
                       const ConfigurationOptions &options = ConfigurationOptions());

    /**
     * Create a composite matcher from a configuration.
     *
     * @param config                the configuration
     * @param defaultStations       the regular expression of accepted stations set by default
     * @param defaultArchives       the regular expression of accepted archives set by default
     * @param defaultVariables      the regular expression of accepted variables set by default
     * @param options               the options applied to the configuration
     */
    Composite(const Variant::Read &config,
              const Element::PatternList &defaultStations,
              const Element::PatternList &defaultArchives,
              const Element::PatternList &defaultVariables,
              const ConfigurationOptions &options = ConfigurationOptions());

    /**
     * Create a composite matcher from a configuration.
     *
     * @param config                the configuration
     * @param defaultStations       the regular expression of accepted stations set by default
     * @param defaultArchives       the regular expression of accepted archives set by default
     * @param defaultVariables      the regular expression of accepted variables set by default
     * @param defaultHasFlavors     the regular expression of required flavors set by default
     * @param defaultLacksFlavors   the regular expression of excluded flavors set by default
     * @param options               the options applied to the configuration
     */
    Composite(const Variant::Read &config,
              const Element::PatternList &defaultStations,
              const Element::PatternList &defaultArchives,
              const Element::PatternList &defaultVariables,
              const Element::PatternList &defaultHasFlavors,
              const Element::PatternList &defaultLacksFlavors,
              const ConfigurationOptions &options = ConfigurationOptions());

    /**
     * Create a composite matcher from a configuration.
     *
     * @param config                the configuration
     * @param defaultStations       the regular expression of accepted stations set by default
     * @param defaultArchives       the regular expression of accepted archives set by default
     * @param defaultVariables      the regular expression of accepted variables set by default
     * @param exactFlavors          the regular expressions of exact flavors required set by default
     * @param options               the options applied to the configuration
     */
    Composite(const Variant::Read &config,
              const Element::PatternList &defaultStations,
              const Element::PatternList &defaultArchives,
              const Element::PatternList &defaultVariables,
              const SequenceName::Flavors &defaultExactFlavors,
              const ConfigurationOptions &options = ConfigurationOptions());

    /**
     * Test if the composite matches a sequence name.
     *
     * @param name  the sequence name
     * @return      true if the sequence name matches
     */
    bool matches(const SequenceName &name) const;

    /**
     * Get a string description of the element composite.
     *
     * @return      a string description
     */
    virtual QString describe() const;


    /**
     * Add a matcher to the composite.
     *
     * @param element   the matching element
     */
    void append(const Element &element);

    /**
     * Add a matcher to the composite.
     *
     * @param element   the matching element
     */
    void append(Element &&element);

    /**
     * Add an exact matcher to the composite.
     *
     * @param name      then name to match
     */
    void append(const SequenceName &name);

    /**
     * Add a matcher to the composite.
     *
     * @param station       the station as a regular expression
     * @param archive       the archive as a regular expression
     * @param variable      the variable as a regular expression
     */
    void append(const Element::Pattern &station,
                const Element::Pattern &archive,
                const Element::Pattern &variable);

    /**
     * Add a matcher to the composite.
     *
     * @param station       the station as a regular expression
     * @param archive       the archive as a regular expression
     * @param variable      the variable as a regular expression
     * @param exactFlavors  the exact flavors to match as regular expressions
     */
    void append(const Element::Pattern &station,
                const Element::Pattern &archive,
                const Element::Pattern &variable,
                const SequenceName::Flavors &exactFlavors);

    /**
     * Add a matcher to the composite.
     *
     * @param station       the station as a regular expression
     * @param archive       the archive as a regular expression
     * @param variable      the variable as a regular expression
     * @param hasFlavors    the required flavors as regular expressions
     * @param lacksFlavors  the excluded flavors as regular expressions
     */
    void append(const Element::Pattern &station,
                const Element::Pattern &archive,
                const Element::Pattern &variable,
                Element::PatternList hasFlavors,
                Element::PatternList lacksFlavors);

    /**
     * Add the entire contents of another matcher to this one.
     *
     * @param other     the other matcher
     */
    void append(const Composite &other);

    /**
     * Add the entire contents of another matcher to this one.
     *
     * @param other     the other matcher
     */
    void append(Composite &&other);


    /**
     * Remove all archive matching from the composite.
     */
    void clearArchiveSpecification();

    /**
     * Require all matches in the composite to also match the specified flavors.
     *
     * @param flavors       the flavors to require
     */
    void requireFlavors(const SequenceName::Flavors &flavors);

    /**
     * Require all matches in the composite to exclude the specified flavors.
     *
     * @param flavors       the flavors to exclude
     */
    void excludeFlavors(const SequenceName::Flavors &flavors);

    /**
     * Test if the selection is valid (non-empty).
     *
     * @return          true if there are matching elements
     */
    bool valid() const;

    /**
     * Reduce the composite to a single sequence name.
     *
     * @return          the reduced name
     */
    SequenceName reduce() const;

    /**
     * Reduce the composite to any constant sequence names that can be derived.
     *
     * @return          any constant sequence names
     */
    SequenceName::Set reduceMultiple() const;

    /**
     * Convert the composite to an approximate archive selection list.
     * The resulting selections may match more than what the element exactly
     * specifies.
     *
     * @param defaultStations   the stations to set by default
     * @param defaultArchives   the archives to set by default
     * @param defaultVariables  the variable to set by default
     * @return                  approximate archive selections
     */
    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const;


    /**
     * Add an exact match of a sequence name to a configuration for a composite.
     *
     * @param config            the configuration
     * @param name              the name to add
     */
    static void addToConfiguration(Variant::Write &config, const SequenceName &name);

    /**
     * Add a match of regular expression of a configuration for a composite.
     *
     * @param config            the configuration
     * @param station           the station
     * @param archive           the archive
     * @param variable          the variable
     */
    static void addToConfiguration(Variant::Write &config,
                                   const Element::Pattern &station,
                                   const Element::Pattern &archive,
                                   const Element::Pattern &variable);

    /** @see addToConfiguration(Value &, const SequenceName &) */
    static void addToConfiguration(Variant::Write &&config, const SequenceName &name);

    /** @see addToConfiguration(Value &, const QString &, const QString &, const QString &) */
    static void addToConfiguration(Variant::Write &&config,
                                   const Element::Pattern &station,
                                   const Element::Pattern &archive,
                                   const Element::Pattern &variable);

protected:
    /**
     * Find the first matching element in the composite.
     *
     * @param name      the name to match
     * @return          the matching index or static_cast<std::size_t>(-1) if none
     */
    std::size_t findMatching(const SequenceName &name) const;

    /**
     * Run a functor on all elements.
     *
     * @tparam Functor  the functor type, called with the element and the index
     * @param f         the functor to call
     */
    template<typename Functor>
    void forAllElements(Functor f) const
    {
        std::size_t index = 0;
        for (const auto &e : elements) {
            f(e, index);
            ++index;
        }
    }
};

/**
 * A basic composite matcher, that also provides in order lookup.
 */
class CPD3DATACORE_EXPORT OrderedLookup : public Composite {
    struct Known {
        std::size_t index;
        SequenceName name;

        inline Known() = default;

        inline Known(const Known &other) = default;

        inline Known &operator=(const Known &other) = default;

        inline Known(Known &&other) = default;

        inline Known &operator=(Known &&other) = default;

        inline Known(std::size_t index, const SequenceName &name) : index(index), name(name)
        { }

        inline Known(std::size_t index, SequenceName &&name) : index(index), name(std::move(name))
        { }

        static bool compare(const Known &a, const Known &b);
    };

    std::vector<Known> known;

    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream, const OrderedLookup &o);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream, OrderedLookup &o);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const OrderedLookup &o);

public:
    OrderedLookup();

    virtual ~OrderedLookup();

    OrderedLookup(const OrderedLookup &other);

    OrderedLookup &operator=(const OrderedLookup &other);

    OrderedLookup(OrderedLookup &&other);

    OrderedLookup &operator=(OrderedLookup &&other);

    /**
     * Create a composite matcher of a single element.
     *
     * @param element       the matching element
     */
    explicit OrderedLookup(const Element &element);

    /**
     * Create a composite matcher of a single element.
     *
     * @param element       the matching element
     */
    explicit OrderedLookup(Element &&element);

    /**
     * Create a composite matcher from a list of elements.
     *
     * @param elements      the matching elements
     */
    explicit OrderedLookup(const std::vector<Element> &elements);

    /**
     * Create a composite matcher from a list of elements.
     *
     * @param elements      the matching elements
     */
    explicit OrderedLookup(std::vector<Element> &&elements);

    /**
     * Create a composite matcher from a configuration.
     *
     * @param config        the configuration
     * @param options       the options applied to the configuration
     */
    explicit OrderedLookup(const Variant::Read &config,
                           const Composite::ConfigurationOptions &options = Composite::ConfigurationOptions());

    /**
     * Create a composite matcher from a configuration.
     *
     * @param config                the configuration
     * @param defaultStations       the regular expression of accepted stations set by default
     * @param defaultArchives       the regular expression of accepted archives set by default
     * @param defaultVariables      the regular expression of accepted variables set by default
     * @param options               the options applied to the configuration
     */
    OrderedLookup(const Variant::Read &config,
                  const Element::PatternList &defaultStations,
                  const Element::PatternList &defaultArchives,
                  const Element::PatternList &defaultVariables,
                  const Composite::ConfigurationOptions &options = Composite::ConfigurationOptions());

    /**
     * Create a composite matcher from a configuration.
     *
     * @param config                the configuration
     * @param defaultStations       the regular expression of accepted stations set by default
     * @param defaultArchives       the regular expression of accepted archives set by default
     * @param defaultVariables      the regular expression of accepted variables set by default
     * @param defaultHasFlavors     the regular expression of required flavors set by default
     * @param defaultLacksFlavors   the regular expression of excluded flavors set by default
     * @param options               the options applied to the configuration
     */
    OrderedLookup(const Variant::Read &config,
                  const Element::PatternList &defaultStations,
                  const Element::PatternList &defaultArchives,
                  const Element::PatternList &defaultVariables,
                  const Element::PatternList &defaultHasFlavors,
                  const Element::PatternList &defaultLacksFlavors,
                  const Composite::ConfigurationOptions &options = Composite::ConfigurationOptions());

    /**
     * Create a composite matcher from a configuration.
     *
     * @param config                the configuration
     * @param defaultStations       the regular expression of accepted stations set by default
     * @param defaultArchives       the regular expression of accepted archives set by default
     * @param defaultVariables      the regular expression of accepted variables set by default
     * @param exactFlavors          the regular expressions of exact flavors required set by default
     * @param options               the options applied to the configuration
     */
    OrderedLookup(const Variant::Read &config,
                  const Element::PatternList &defaultStations,
                  const Element::PatternList &defaultArchives,
                  const Element::PatternList &defaultVariables,
                  const SequenceName::Flavors &defaultExactFlavors,
                  const Composite::ConfigurationOptions &options = Composite::ConfigurationOptions());

    QString describe() const override;

    /**
     * Register an input to the lookup.
     *
     * @param input the input to add
     * @return      true if the lookup matches
     */
    bool registerInput(const SequenceName &input);

    /**
     * Register an input to the lookup.
     *
     * @param input the input to add
     * @return      true if the lookup matches
     */
    bool registerInput(SequenceName &&input);

    /**
     * Get the set of all known inputs.
     *
     * @return      the set of known inputs
     */
    SequenceName::Set knownInputs() const;

    /**
     * Lookup a value from a segment.
     *
     * @param segment       the segment
     * @return              the value in the segment
     */
    Variant::Write lookup(SequenceSegment &segment) const;

    /**
     * Lookup a value from a segment, without modifing the segment to insert one
     * if required.
     *
     * @param segment       the segment
     * @return              the value in the segment or a dummy if there is no match
     */
    Variant::Read lookup(const SequenceSegment &segment) const;

    void registerExpectedFlavorless(const SequenceName::Component &station = {},
                                    const SequenceName::Component &archive = {},
                                    const SequenceName::Component &variable = {});

    void registerExpected(const SequenceName::Component &station,
                          const SequenceName::Component &archive,
                          const SequenceName::Component &variable,
                          const SequenceName::Flavors &flavors);

    void registerExpected(const SequenceName::Component &station = {},
                          const SequenceName::Component &archive = {},
                          const SequenceName::Component &variable = {},
                          const std::unordered_set<
                                  SequenceName::Flavors> &flavors = SequenceName::defaultDataFlavors());

};

/**
 * A basic sequence matcher, that only provides true or false tests
 * for if a sequence name matches.
 */
class CPD3DATACORE_EXPORT Basic {
    friend CPD3DATACORE_EXPORT QDataStream &operator<<(QDataStream &stream,
                                                       const std::unique_ptr<Basic> &b);

    friend CPD3DATACORE_EXPORT QDataStream &operator>>(QDataStream &stream,
                                                       std::unique_ptr<Basic> &b);

    friend CPD3DATACORE_EXPORT QDebug operator<<(QDebug stream, const std::unique_ptr<Basic> &b);

    static Basic *deserialize(QDataStream &stream);

public:
    virtual ~Basic();

    /**
     * Advance the matcher to the given start time.  Any data before the
     * start will have undefined results.
     *
     * @param time  the new start time
     */
    virtual void advance(double time) = 0;

    /**
     * Test if the given name matches and advance to the start time.
     *
     * @param name      the sequence name
     * @param start     the start time
     * @param end       the end time
     * @param advance   advance the matcher to the start time
     * @return          true if the name and times match
     */
    virtual bool matches(const SequenceName &name, double start, double end) = 0;

    /** @see matches(const SequenceName &, double, double) */
    inline bool matches(const SequenceValue &value)
    { return matches(value.getUnit(), value.getStart(), value.getEnd()); }

    /**
     * Test if the given name matches.
     *
     * @param name      the sequence name
     * @param start     the start time
     * @param end       the end time
     * @return          true if the name and times match
     */
    virtual bool matches(const SequenceName &name, double start, double end) const = 0;

    /** @see matches(const SequenceName &, double, double) const */
    inline bool matches(const SequenceValue &value) const
    { return matches(value.getUnit(), value.getStart(), value.getEnd()); }

    /**
     * Test if the given name matches and advance to the start time.
     *
     * @param name      the sequence name
     * @param point     the point in time
     * @return          true if the name matches at the given time
     */
    virtual bool matches(const SequenceName &name, double point) = 0;

    /**
     * Test if the given name matches.
     *
     * @param name      the sequence name
     * @param point     the point in time
     * @return          true if the name matches at the given time
     */
    virtual bool matches(const SequenceName &name, double point) const = 0;

    /**
     * Test if the name has any possible matches at any times.
     *
     * @param name      the sequence name
     * @return          true if the name has a possible match
     */
    virtual bool matches(const SequenceName &name) const = 0;

    /**
     * The options used when compiling basic matchers.
     */
    struct CPD3DATACORE_EXPORT CompileOptions {
        /**
         * Ignore the station and do not generate matchers for it.
         */
        bool ignoreStation;

        /**
         * Ignore the archive and do not generate matchers for it.
         */
        bool ignoreArchive;

        /**
         * Ignore the variable and do not generate matchers for it.
         */
        bool ignoreVariable;

        /**
         * Ignore the flavors and do not generate matchers for it.
         */
        bool ignoreFlavors;

        CompileOptions();
    };

    /**
     * Compile a matcher from the given archive selections.
     *
     * @param selections    the selections
     * @param start         the expected data start
     * @param end           the expected data end
     * @param options       the options to compile with
     * @return              a new matcher
     */
    static std::unique_ptr<Basic> compile(const Archive::Selection::List &selections,
                                          double start = FP::undefined(),
                                          double end = FP::undefined(),
                                          const CompileOptions &options = CompileOptions());

    /** @see compile(const Archive::Selection::List &, double, double, const CompileOptions &) */
    static inline std::unique_ptr<Basic> compile(const Archive::Selection &selection,
                                                 double start = FP::undefined(),
                                                 double end = FP::undefined(),
                                                 const CompileOptions &options = CompileOptions())
    { return compile(Archive::Selection::List{selection}, start, end, options); }

    /**
     * Compile a list of match elements from an archive selection list.
     *
     * @param selections    the selections
     * @param options       the options to compile with
     * @return              a list of match elements
     */
    static std::vector<Element> compileToElements(const Archive::Selection::List &selections,
                                                  const CompileOptions &options = CompileOptions());

    /**
     * Create a copy of the matcher.
     *
     * @return  a new copy of the matcher
     */
    virtual std::unique_ptr<Basic> clone() const = 0;

    /**
     * Fetch a description of the matcher.
     *
     * @return  a description
     */
    virtual QString describe() const = 0;

protected:

    virtual void serialize(QDataStream &stream) const = 0;

private:
    static Element compileElement(const Archive::Selection &selection,
                                  const CompileOptions &options);
};

}
}
}

#endif //CPD3DATACORE_SEQUENCEMATCH_HXX
