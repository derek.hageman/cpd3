/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cstring>
#include <QLoggingCategory>
#include <QBuffer>
#include <QtEndian>
#include <QXmlDeclHandler>
#include <QXmlParseException>
#include <QXmlAttributes>
#include <QJsonValue>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonParseError>
#include <QJsonDocument>
#include <QRegularExpression>

#include "externalsource.hxx"
#include "dataioxml.hxx"
#include "core/timeparse.hxx"
#include "core/csv.hxx"


Q_LOGGING_CATEGORY(log_datacore_externalsource, "cpd3.datacore.externalsource", QtWarningMsg)


namespace CPD3 {
namespace Data {

ExternalSource::~ExternalSource() = default;

bool ExternalSource::wait(double timeout)
{ return finished.wait(std::bind(&ExternalSource::isFinished, this), timeout); }

SequenceName::Set ExternalSource::predictedOutputs()
{ return SequenceName::Set(); }


ExternalConverter::~ExternalConverter() = default;

void ExternalConverter::incomingData(const Util::ByteArray &data)
{ return incomingData(Util::ByteView(data)); }

void ExternalConverter::incomingData(Util::ByteArray &&data)
{ return incomingData(data); }

void ExternalConverter::incomingData(const QByteArray &data)
{ return incomingData(Util::ByteView(data)); }

void ExternalConverter::incomingData(QByteArray &&data)
{ return incomingData(data); }

void ExternalConverter::incomingData(const std::string &str)
{ return incomingData(Util::ByteView(str.data(), str.size())); }

void ExternalConverter::incomingData(const QString &str)
{ return incomingData(str.toUtf8()); }

void ExternalConverter::incomingData(const char *str)
{
    if (!str)
        return;
    return incomingData(Util::ByteView(str, std::strlen(str)));
}

static constexpr std::size_t bufferProcessIterations = 2048;
static constexpr std::size_t bufferProcessSize = 16384;

namespace Internal {

class StandardDataInputXMLHandler : public QXmlDefaultHandler {
public:
    StandardDataInput &dataInput;
    int callCounter;

    QString lastError;

    QString errorString() const override
    {
        if (lastError.length() > 0)
            return lastError;
        return parser.errorString();
    }

    bool fatalError(const QXmlParseException &exception) override
    {
        qCWarning(log_datacore_externalsource) << "Error parsing input XML at line"
                                               << exception.lineNumber() << ", column"
                                               << exception.columnNumber() << ":"
                                               << exception.message();
        return false;
    }

    SequenceValue output;
    double lastStart;
    XMLDataInputParser parser;
    int depth;

    bool startElement(const QString &namespaceURI,
                      const QString &localName,
                      const QString &qName,
                      const QXmlAttributes &atts) override
    {
        if (++callCounter % bufferProcessIterations == 0) {
            if (dataInput.isTerminated()) {
                lastError = QObject::tr("XML parsing terminated");
                return false;
            }
        }

        depth++;

        if (depth == 1) {
            if (localName != "cpd3data" || atts.value("version") != "1.0") {
                lastError = QObject::tr("Unrecognized input XML version");
                return false;
            }
            return true;
        }
        if (depth == 2) {
            if (localName != "value") {
                lastError = QObject::tr("Unrecognized tag in primary value list");
                return false;
            }

            auto station = atts.value("station").toStdString();
            if (station.length() == 0) {
                lastError = QObject::tr("No station specified");
                return false;
            }
            auto archive = atts.value("archive").toStdString();
            if (archive.length() == 0) {
                lastError = QObject::tr("No archive specified");
                return false;
            }
            auto variable = atts.value("variable").toStdString();
            if (variable.length() == 0) {
                lastError = QObject::tr("No variable specified");
                return false;
            }
            QString flavors(atts.value("flavors"));

            double start = FP::undefined();
            QString parse(atts.value("start"));
            if (parse.length() > 0) {
                try {
                    start = TimeParse::parseTime(parse);
                } catch (const TimeParsingException &tpe) {
                    lastError = QObject::tr("Invalid start time: %1").arg(tpe.getDescription());
                    return false;
                }
            }
            if (Range::compareStart(lastStart, start) > 0) {
                lastError = QObject::tr("Value start times not in ascending order");
                return false;
            }
            lastStart = start;

            double end = FP::undefined();
            parse = atts.value("end");
            if (parse.length() > 0) {
                try {
                    end = TimeParse::parseTime(parse);
                } catch (const TimeParsingException &tpe) {
                    lastError = QObject::tr("Invalid end time: %1").arg(tpe.getDescription());
                    return false;
                }
            }

            int priority = 0;
            parse = atts.value("priority");
            if (parse.length() > 0) {
                bool ok;
                priority = parse.toInt(&ok);
                if (!ok) {
                    lastError = QObject::tr("Invalid priority");
                    return false;
                }
            }

            Variant::Root value;
            parser = XMLDataInputParser(value.write());

            output = SequenceValue({{std::move(station), std::move(archive), std::move(variable),
                                     Util::set_from_qstring(
                                             flavors.split(' ', QString::SkipEmptyParts))}, start,
                                    end, priority}, std::move(value));
        }
        return parser.startElement(namespaceURI, localName, qName, atts) !=
                XMLDataInputParser::ERROR;
    }

    bool endElement(const QString &namespaceURI,
                    const QString &localName,
                    const QString &qName) override
    {
        depth--;

        if (depth > 0) {
            if (parser.endElement(namespaceURI, localName, qName) == XMLDataInputParser::ERROR)
                return false;
        }

        if (depth == 1) {
            if (dataInput.egress) {
                dataInput.outputData(std::move(output));
            }
        }

        return true;
    }

    bool characters(const QString &ch) override
    {
        return parser.characters(ch) != XMLDataInputParser::ERROR;
    }

    explicit StandardDataInputXMLHandler(StandardDataInput &dataInput) : dataInput(dataInput),
                                                                         callCounter(0),
                                                                         lastStart(FP::undefined()),
                                                                         depth(0)
    { }
};


struct XMLContainer {
    QXmlSimpleReader xml;
    QXmlInputSource xmlData;
    StandardDataInputXMLHandler handler;
    bool first;

    XMLContainer(StandardDataInput &dataInput) : handler(dataInput), first(true)
    {
        xml.setContentHandler(&handler);
        xml.setErrorHandler(&handler);
    }

    bool process(const QByteArray &data)
    {
        if (data.isEmpty())
            return true;

        xmlData.setData(data);
        if (first) {
            first = false;
            return xml.parse(&xmlData, true);
        } else {
            return xml.parseContinue();
        }
    }

    void completed()
    {
        if (!first)
            xml.parseContinue();
    }
};

}

ExternalSourceProcessor::ExternalSourceProcessor() : state(State::Initialize),
                                                     egress(nullptr),
                                                     dataEnded(false)
{ }

ExternalSourceProcessor::~ExternalSourceProcessor()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        switch (state) {
        case State::Initialize:
            state = State::InitializeTerminate;
            break;
        case State::Running:
            state = State::Terminating;
            break;
        case State::Terminating:
        case State::InitializeTerminate:
        case State::Completed:
            break;
        }
    }
    externalChanged.notify_all();
    if (thread.joinable())
        thread.join();
}

void ExternalSourceProcessor::run()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!lock) {
            if (egressLock)
                egressLock.unlock();
            lock.lock();
        }

        switch (state) {
        case State::Initialize:
            state = State::Running;
            lock.unlock();

            internalChanged.notify_all();
            if (!begin()) {
                lock.lock();
                state = State::Completed;
                lock.unlock();
                internalChanged.notify_all();
                finished();
                return;
            }
            break;

        case State::InitializeTerminate:
            state = State::Running;
            lock.unlock();

            internalChanged.notify_all();
            if (!begin()) {
                lock.lock();
                state = State::Completed;
                lock.unlock();
                internalChanged.notify_all();
                finished();
                return;
            }
            /* Fall through */
        case State::Terminating: {
            if (lock)
                lock.unlock();

            finalize();

            {
                egressLock = std::unique_lock<std::mutex>(egressMutex);

                finish();

                if (!egressLock)
                    egressLock.lock();
                if (egress) {
                    egress->endData();
                    egress = nullptr;
                }

                egressLock.unlock();
            }

            lock.lock();
            state = State::Completed;
            lock.unlock();
            internalChanged.notify_all();
            finished();
            return;
        }

        case State::Running: {
            egressLock = std::unique_lock<std::mutex>(egressMutex);

            bool wasStalled = isStalled();

            bool shouldProcess = prepare();
            if (!shouldProcess) {
                if (egressLock)
                    egressLock.unlock();

                {
                    bool nowStalled = isStalled();
                    if (nowStalled != wasStalled) {
                        lock.unlock();
                        stallRequested(nowStalled);
                        continue;
                    }
                }
                externalChanged.wait(lock);
                continue;
            }

            if (!egress) {
                if (egressLock)
                    egressLock.unlock();

                {
                    bool nowStalled = isStalled();
                    if (nowStalled != wasStalled) {
                        lock.unlock();
                        stallRequested(nowStalled);
                        continue;
                    }
                }
                externalChanged.wait(lock);
                continue;
            }

            lock.unlock();
            if (!process()) {
                if (egressLock)
                    egressLock.unlock();
                lock.lock();
                state = State::Terminating;
            } else {
                if (egressLock)
                    egressLock.unlock();
                lock.lock();
            }

            {
                bool nowStalled = isStalled();
                if (nowStalled != wasStalled) {
                    lock.unlock();
                    stallRequested(nowStalled);
                    continue;
                }
            }
            break;
        }

        case State::Completed:
            internalChanged.notify_all();
            return;
        }
    }
}

void ExternalSourceProcessor::externalNotify()
{ externalChanged.notify_all(); }

bool ExternalSourceProcessor::isTerminated(bool lockMutex)
{
    if (lockMutex) {
        std::lock_guard<std::mutex> lock(mutex);
        return state != State::Running;
    }
#ifndef NDEBUG
    Q_ASSERT(!mutex.try_lock());
#endif
    return state != State::Running;
}

void ExternalSourceProcessor::releaseEgressLock()
{
    if (!egressLock)
        return;
    egressLock.unlock();
}

std::unique_lock<std::mutex> ExternalSourceProcessor::processingRelock()
{
    Q_ASSERT(egressLock);
    egressLock.unlock();

    std::unique_lock<std::mutex> lock(mutex);
    return std::move(lock);
}

bool ExternalSourceProcessor::begin()
{ return true; }

void ExternalSourceProcessor::finalize()
{ }

void ExternalSourceProcessor::finish()
{ }


void ExternalSourceProcessor::incomingData(const Util::ByteView &data)
{
    if (data.empty())
        return;
    std::lock_guard<std::mutex> lock(mutex);
    buffer += data;
    externalNotify();
}

void ExternalSourceProcessor::incomingData(Util::ByteArray &&data)
{
    if (data.empty())
        return;
    std::lock_guard<std::mutex> lock(mutex);
    buffer += std::move(data);
    externalNotify();
}

void ExternalSourceProcessor::endData()
{
    std::lock_guard<std::mutex> lock(mutex);
    dataEnded = true;
    externalNotify();
}

bool ExternalSourceProcessor::isStalled() const
{ return buffer.size() > 16 * 1024 * 1024; }

bool ExternalSourceProcessor::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return state == State::Completed;
}

void ExternalSourceProcessor::start()
{
    Q_ASSERT(state == State::Initialize || state == State::InitializeTerminate);
    Q_ASSERT(!thread.joinable());
    thread = std::thread(std::bind(&ExternalSourceProcessor::run, this));
}

bool ExternalSourceProcessor::wait(double timeout)
{
    return Threading::waitForTimeout(timeout, mutex, internalChanged,
                                     [this] { return state == State::Completed; });
}

void ExternalSourceProcessor::setEgress(StreamSink *egress)
{
    std::unique_lock<std::mutex> lock(mutex);
    std::unique_lock<std::mutex> elock(egressMutex);
    if (state == State::Completed || state == State::Terminating) {
        lock.unlock();
        if (egress && egress != this->egress) {
            elock.unlock();
            wait();
            egress->endData();
            return;
        }
        this->egress = egress;
        elock.unlock();
        wait();
        return;
    }
    this->egress = egress;
    externalNotify();
}

void ExternalSourceProcessor::signalTerminate()
{
    std::lock_guard<std::mutex> lock(mutex);
    switch (state) {
    case State::Initialize:
        state = State::InitializeTerminate;
        break;
    case State::Running:
        state = State::Terminating;
        break;
    case State::Terminating:
    case State::InitializeTerminate:
    case State::Completed:
        return;
    }
    externalNotify();
}


StandardDataInput::StandardDataInput() : isHeader(true),
                                         processingEnded(false),
                                         bufferConsumed(false),
                                         type(InputType::Unidentified),
                                         nextIndex(0)
{ }

StandardDataInput::~StandardDataInput()
{
    if (child) {
        child->wait();
        child.reset();
    }
}

void StandardDataInput::setEgress(StreamSink *egress)
{
    /* Re-implemented to handle the child set correctly */
    std::unique_lock<std::mutex> lock(mutex);
    std::unique_lock<std::mutex> elock(egressMutex);

    if (child) {
        lock.unlock();
        elock.unlock();
        child->setEgress(egress);
        return;
    }

    if (state == State::Completed || state == State::Terminating) {
        lock.unlock();
        if (egress && egress != this->egress) {
            elock.unlock();
            wait();
            egress->endData();
            return;
        }
        this->egress = egress;
        elock.unlock();
        wait();
        return;
    }
    this->egress = egress;
    externalNotify();
}

void StandardDataInput::signalTerminate()
{
    ExternalSourceProcessor::signalTerminate();
    std::unique_lock<std::mutex> lock(mutex);
    if (child) {
        lock.unlock();
        child->signalTerminate();
    }
}

void StandardDataInput::incomingData(const Util::ByteView &data)
{
    if (data.empty())
        return;
    std::unique_lock<std::mutex> lock(mutex);
    if (child) {
        lock.unlock();
        child->incomingData(data);
        return;
    }
    buffer += data;
    externalNotify();
}

void StandardDataInput::incomingData(const Util::ByteArray &data)
{
    if (data.empty())
        return;
    std::unique_lock<std::mutex> lock(mutex);
    if (child) {
        lock.unlock();
        child->incomingData(data);
        return;
    }
    buffer += data;
    externalNotify();
}

void StandardDataInput::incomingData(Util::ByteArray &&data)
{
    if (data.empty())
        return;
    std::unique_lock<std::mutex> lock(mutex);
    if (child) {
        lock.unlock();
        child->incomingData(std::move(data));
        return;
    }
    buffer += std::move(data);
    externalNotify();
}

void StandardDataInput::incomingData(const QByteArray &data)
{
    if (data.isEmpty())
        return;
    std::unique_lock<std::mutex> lock(mutex);
    if (child) {
        lock.unlock();
        child->incomingData(data);
        return;
    }
    buffer += data;
    externalNotify();
}

void StandardDataInput::incomingData(QByteArray &&data)
{
    if (data.isEmpty())
        return;
    std::unique_lock<std::mutex> lock(mutex);
    if (child) {
        lock.unlock();
        child->incomingData(std::move(data));
        return;
    }
    buffer += std::move(data);
    externalNotify();
}

void StandardDataInput::endData()
{
    std::unique_lock<std::mutex> lock(mutex);
    dataEnded = true;
    externalNotify();
    if (child) {
        lock.unlock();
        child->endData();
    }
}


XMLDataInputParser::XMLDataInputParser() : elementDataMode(IGNORE)
{ }

XMLDataInputParser::XMLDataInputParser(const XMLDataInputParser &other) = default;

XMLDataInputParser &XMLDataInputParser::operator=(const XMLDataInputParser &other) = default;

XMLDataInputParser::XMLDataInputParser(Variant::Write output) : elementDataMode(IGNORE)
{
    stack.emplace_back(std::move(output));
}

/**
 * Get the last error string.
 *
 * @return the error string
 */
QString XMLDataInputParser::errorString() const
{ return lastError; }

/**
 * Handle XML character data.  This is a direct analog of the
 * QXmlDefaultHandler of the same name.
 *
 * @return the status of the parsing
 */
XMLDataInputParser::ParseStatus XMLDataInputParser::characters(const QString &ch)
{
    if (elementDataMode == IGNORE) return CONTINUE;
    elementData.append(ch);
    return CONTINUE;
}

/**
 * Handle start of an element.  This is a direct analog of the
 * QXmlDefaultHandler of the same name.
 *
 * @return the status of the parsing
 */
XMLDataInputParser::ParseStatus XMLDataInputParser::startElement(const QString &,
                                                                 const QString &localName,
                                                                 const QString &,
                                                                 const QXmlAttributes &atts)
{
    Q_ASSERT(!stack.empty());

    elementDataMode = IGNORE;

    if (mode.length() > 0) {
        ParseElementMode effectiveMode = mode.last();
        switch (effectiveMode) {
        case VALUE:
            lastError = QObject::tr("Nested values not allowed");
            return ERROR;
        case FLAGDATA:
            lastError = QObject::tr("Nested flags not allowed");
            return ERROR;
        case STRINGDATA:
            lastError = QObject::tr("Nested localized strings not allowed");
            return ERROR;
        case FLAG:
            if (localName != "flag") {
                lastError = QObject::tr("Unexpected tag in flag sequence");
                return ERROR;
            }
            elementData.clear();
            elementDataMode = FLAGNAME;
            mode.append(FLAGDATA);
            return CONTINUE;
        case LSTRING:
            if (localName != "locale") {
                lastError = QObject::tr("Unexpected tag in localized string sequence");
                return ERROR;
            }
            auxData = atts.value("name");
            elementData.clear();
            elementDataMode = STRINGLOCALE;
            mode.append(STRINGDATA);
            return CONTINUE;
        case ARRAY:
            stack.emplace_back(stack.back().toArray().after_back());
            break;
        case MATRIX: {
            Variant::PathElement::MatrixIndex indices;
            for (const auto &add : atts.value("index").split(',')) {
                bool ok;
                int idx = add.toInt(&ok);
                if (!ok || idx < 0) {
                    lastError = QObject::tr("Error parsing matrix index");
                    return ERROR;
                }
                indices.push_back(static_cast<std::size_t>(idx));
            }
            stack.emplace_back(stack.back().matrix(indices));
            break;
        }
        case HASH:
            stack.emplace_back(stack.back().hash(atts.value("key")));
            break;
        case KEYFRAME: {
            bool ok;
            double v = atts.value("key").toDouble(&ok);
            if (!ok) {
                lastError = QObject::tr("Error parsing keyframe value");
                return ERROR;
            }
            stack.emplace_back(stack.back().keyframe(v));
            break;
        }
        case METAHASH:
        case METAFLAGS: {
            if (localName == "value") {
                if (effectiveMode == METAHASH)
                    stack.emplace_back(stack.back().metadataHashChild(atts.value("key")));
                else
                    stack.emplace_back(stack.back().metadataSingleFlag(atts.value("key")));
                break;
            }
        }
        case METASIMPLE:
            stack.emplace_back(stack.back().metadata(atts.value("key")));
            break;
        default:
            break;
        }
    }

    QString type(atts.value("type").toLower());
    if (type.length() == 0 || type == "invalid" || type == "empty") {
        stack.back().setEmpty();
        mode.append(VALUE);
        return CONTINUE;
    }

    if (type == "real" || type == "double") {
        elementData.clear();
        elementDataMode = DOUBLE;
        mode.append(VALUE);
        return CONTINUE;
    }
    if (type == "integer") {
        elementData.clear();
        elementDataMode = INTEGER;
        mode.append(VALUE);
        return CONTINUE;
    }
    if (type == "boolean") {
        elementData.clear();
        elementDataMode = BOOLEAN;
        mode.append(VALUE);
        return CONTINUE;
    }
    if (type == "string") {
        elementData.clear();
        elementDataMode = STRING;
        mode.append(VALUE);
        return CONTINUE;
    }
    if (type == "bytes" || type == "binary") {
        elementData.clear();
        elementDataMode = BINARY;
        mode.append(VALUE);
        return CONTINUE;
    }
    if (type == "flags") {
        mode.append(FLAG);
        return CONTINUE;
    }
    if (type == "overlay") {
        elementData.clear();
        elementDataMode = OVERLAY;
        mode.append(VALUE);
        return CONTINUE;
    }
    if (type == "localizedstring") {
        mode.append(LSTRING);
        return CONTINUE;
    }
    if (type == "array") {
        stack.back().setType(Variant::Type::Array);
        mode.append(ARRAY);
        return CONTINUE;
    }
    if (type == "matrix") {
        stack.back().setType(Variant::Type::Matrix);
        mode.append(MATRIX);
        return CONTINUE;
    }
    if (type == "hash") {
        stack.back().setType(Variant::Type::Hash);
        mode.append(HASH);
        return CONTINUE;
    }
    if (type == "keyframe") {
        stack.back().setType(Variant::Type::Keyframe);
        mode.append(KEYFRAME);
        return CONTINUE;
    }
    if (type == "metareal" || type == "metadouble") {
        stack.back().setType(Variant::Type::MetadataReal);
        mode.append(METASIMPLE);
        return CONTINUE;
    }
    if (type == "metainteger") {
        stack.back().setType(Variant::Type::MetadataInteger);
        mode.append(METASIMPLE);
        return CONTINUE;
    }
    if (type == "metaboolean") {
        stack.back().setType(Variant::Type::MetadataBoolean);
        mode.append(METASIMPLE);
        return CONTINUE;
    }
    if (type == "metastring") {
        stack.back().setType(Variant::Type::MetadataString);
        mode.append(METASIMPLE);
        return CONTINUE;
    }
    if (type == "metabinary" || type == "metadatabytes") {
        stack.back().setType(Variant::Type::MetadataBytes);
        mode.append(METASIMPLE);
        return CONTINUE;
    }
    if (type == "metaarray") {
        stack.back().setType(Variant::Type::MetadataArray);
        mode.append(METASIMPLE);
        return CONTINUE;
    }
    if (type == "metamatrix") {
        stack.back().setType(Variant::Type::MetadataMatrix);
        mode.append(METASIMPLE);
        return CONTINUE;
    }
    if (type == "metakeyframe") {
        stack.back().setType(Variant::Type::MetadataKeyframe);
        mode.append(METASIMPLE);
        return CONTINUE;
    }
    if (type == "metahash") {
        stack.back().setType(Variant::Type::MetadataHash);
        mode.append(METAHASH);
        return CONTINUE;
    }
    if (type == "metaflags") {
        stack.back().setType(Variant::Type::MetadataFlags);
        mode.append(METAFLAGS);
        return CONTINUE;
    }

    lastError = QObject::tr("Unrecognized value type \"%1\"").arg(type);
    return ERROR;
}

/**
 * Handle end of an element.  This is a direct analog of the
 * QXmlDefaultHandler of the same name.
 *
 * @return the status of the parsing
 */
XMLDataInputParser::ParseStatus XMLDataInputParser::endElement(const QString &,
                                                               const QString &,
                                                               const QString &)
{
    Q_ASSERT(mode.length() > 0);
    Q_ASSERT(!stack.empty());

    ParseElementMode finished = mode.takeLast();

    if (finished == FLAGDATA) {
        if (elementData.length() > 0) {
            stack.back().applyFlag(elementData.toStdString());
        }
        return CONTINUE;
    } else if (finished == STRINGDATA) {
        stack.back().setDisplayString(auxData, elementData);
        return CONTINUE;
    }

    switch (elementDataMode) {
    case FLAGNAME:
    case STRINGLOCALE:
    case IGNORE:
        break;
    case DOUBLE:
        if (elementData.length() > 0) {
            bool ok;
            double v = elementData.toDouble(&ok);
            if (!ok) {
                lastError = QObject::tr("Error parsing number");
                return ERROR;
            }
            stack.back().setReal(v);
        } else {
            stack.back().setDouble(FP::undefined());
        }
        break;
    case INTEGER:
        if (elementData.length() > 0) {
            bool ok;
            qint64 v;
            if (elementData.startsWith("0x", Qt::CaseInsensitive))
                v = elementData.toLongLong(&ok, 16);
            else
                v = elementData.toLongLong(&ok);
            if (!ok) {
                lastError = QObject::tr("Error parsing number");
                return ERROR;
            }
            stack.back().setInteger(v);
        } else {
            stack.back().setInteger(INTEGER::undefined());
        }
        break;
    case BOOLEAN:
        if (elementData.length() > 0) {
            if (elementData.compare(QLatin1String("true"), Qt::CaseInsensitive) == 0) {
                stack.back().setBool(true);
                break;
            }
            if (elementData.compare(QLatin1String("false"), Qt::CaseInsensitive) == 0) {
                stack.back().setBool(false);
                break;
            }
            bool ok;
            qint64 vi = elementData.toLongLong(&ok);
            if (vi != 0 && ok) {
                stack.back().setBool(true);
                break;
            }
            double vd = elementData.toDouble(&ok);
            if (vd != 0.0 && ok) {
                stack.back().setBool(true);
                break;
            }
            lastError = QObject::tr("Error parsing boolean");
            return ERROR;
        }
        stack.back().setBool(false);
        break;
    case STRING:
        stack.back().setString(elementData);
        break;
    case BINARY:
        stack.back().setBinary(QByteArray::fromBase64(elementData.toLatin1()));
        break;
    case OVERLAY:
        if (elementData.length() > 0) {
            stack.back().setOverlay(elementData);
        }
        break;
    }

    stack.pop_back();
    elementDataMode = IGNORE;

    if (!stack.empty()) {
        return CONTINUE;
    } else {
        return FINISHED;
    }
}


XMLDataPathParser::XMLDataPathParser() : inElementData(0), target(nullptr)
{ }

XMLDataPathParser::XMLDataPathParser(const XMLDataPathParser &other) = default;

XMLDataPathParser &XMLDataPathParser::operator=(const XMLDataPathParser &other) = default;

XMLDataPathParser::XMLDataPathParser(Variant::Path *output) : inElementData(0), target(output)
{ }


/**
 * Get the last error string.
 *
 * @return the error string
 */
QString XMLDataPathParser::errorString() const
{ return lastError; }

/**
 * Handle XML character data.  This is a direct analog of the
 * QXmlDefaultHandler of the same name.
 *
 * @return the status of the parsing
 */
XMLDataPathParser::ParseStatus XMLDataPathParser::characters(const QString &ch)
{
    if (inElementData < 1) return CONTINUE;
    elementData.append(ch);
    return CONTINUE;
}

/**
 * Handle start of an element.  This is a direct analog of the
 * QXmlDefaultHandler of the same name.
 *
 * @return the status of the parsing
 */
XMLDataPathParser::ParseStatus XMLDataPathParser::startElement(const QString &,
                                                               const QString &localName,
                                                               const QString &,
                                                               const QXmlAttributes &)
{
    inElementData++;
    if (inElementData > 1) {
        lastError = QObject::tr("Nested path elements are not allowed");
        return ERROR;
    } else if (inElementData == 1) {
        componentType = localName;
        elementData.clear();
    } else {
        lastError = QObject::tr("Invalid XML structure");
        return ERROR;
    }
    return CONTINUE;
}

/**
 * Handle end of an element.  This is a direct analog of the
 * QXmlDefaultHandler of the same name.
 *
 * @return the status of the parsing
 */
XMLDataPathParser::ParseStatus XMLDataPathParser::endElement(const QString &,
                                                             const QString &,
                                                             const QString &)
{
    inElementData--;
    if (inElementData < 0) {
        if (target) {
            target->clear();
            for (const auto &e : path) {
                target->emplace_back(Variant::PathElement::decode(e));
            }
        }
        return FINISHED;
    } else if (inElementData == 0) {
        path.emplace_back(componentType.toStdString(), elementData.toStdString());
    } else {
        lastError = QObject::tr("Invalid XML structure");
        return ERROR;
    }
    return CONTINUE;
}

bool StandardDataInput::processXML()
{
    if (!xmlContainer->process(processingBuffer.toQByteArrayRef()))
        return false;
    processingBuffer.clear();
    if (processingEnded) {
        xmlContainer->completed();
        return false;
    }
    return true;
}

static bool parseJSONValue(Variant::Write &target,
                           const QJsonValue &data,
                           const QJsonObject &metadata);

static inline bool parseJSONValue(Variant::Write &&target,
                                  const QJsonValue &data,
                                  const QJsonObject &metadata)
{ return parseJSONValue(target, data, metadata); }

template<typename MapType>
static bool parseJSONMap(MapType &target, const QJsonObject &data, const QJsonObject &metadata)
{
    std::unordered_set<QString> keys;
    Util::merge(data.keys(), keys);
    Util::merge(metadata.keys(), keys);
    for (const auto &key : keys) {
        if (!parseJSONValue(target[key], data.value(key), metadata.value(key).toObject()))
            return false;
    }
    return true;
}

template<typename MapType>
static bool parseJSONMap(MapType &&target, const QJsonObject &data, const QJsonObject &metadata)
{ return parseJSONMap<MapType>(target, data, metadata); }

static bool parseJSONValue(Variant::Write &target,
                           const QJsonValue &data,
                           const QJsonObject &metadata)
{
    QString type(metadata.value("type").toString().toLower());

    if (type == "real" || type == "double") {
        target.setDouble(data.toDouble(FP::undefined()));
        return true;
    } else if (type == "invalid" || type == "empty") {
        target.setEmpty();
        return true;
    } else if (type == "integer") {
        double v = data.toDouble(FP::undefined());
        if (!FP::defined(v))
            target.setInteger(INTEGER::undefined());
        else
            target.setInteger((qint64) std::floor(v + 0.5));
        return true;
    } else if (type == "boolean") {
        target.setBool(data.toBool(false));
        return true;
    } else if (type == "string") {
        target.setString(data.toString(QString()));
        return true;
    } else if (type == "localizedstring") {
        QJsonObject strings(data.toObject());
        target.setType(Variant::Type::String);
        for (QJsonObject::const_iterator v = strings.constBegin(), endV = strings.constEnd();
                v != endV;
                ++v) {
            target.setDisplayString(v.key(), v.value().toString());
        }
        return true;
    } else if (type == "binary") {
        target.setBinary(QByteArray::fromBase64(data.toString().toLatin1()));
        return true;
    } else if (type == "flags") {
        QJsonArray flags(data.toArray());
        target.setType(Variant::Type::Flags);
        for (const auto &v : flags) {
            auto f = v.toString();
            if (f.isEmpty())
                continue;
            target.applyFlag(f.toStdString());
        }
        return true;
    } else if (type == "array") {
        target.setType(Variant::Type::Array);
        QJsonArray children(data.toArray());
        QJsonArray mchildren(metadata.value("values").toArray());
        for (int i = 0, max = std::max(children.size(), mchildren.size()); i < max; i++) {
            if (!parseJSONValue(target.array(static_cast<std::size_t>(i)), children.at(i),
                                mchildren.at(i).toObject()))
                return false;
        }
        return true;
    } else if (type == "matrix") {
        target.setType(Variant::Type::Matrix);
        auto matrix = target.toMatrix();
        {
            Variant::PathElement::MatrixIndex shape;
            for (const auto &v : metadata.value("dimensions").toArray()) {
                int i = v.toInt(-1);
                if (i <= 0)
                    continue;
                shape.push_back(static_cast<std::size_t>(i));
            }
            if (!shape.empty())
                matrix.reshape(std::move(shape));
        }

        QJsonArray children(data.toArray());
        QJsonArray mchildren(metadata.value("values").toArray());
        for (int i = 0, max = std::max(children.size(), mchildren.size()); i < max; i++) {
            if (!parseJSONValue(matrix[i], children.at(i), mchildren.at(i).toObject()))
                return false;
        }
        return true;
    } else if (type == "hash") {
        target.setType(Variant::Type::Hash);
        return parseJSONMap(target.toHash(), data.toObject(), metadata.value("values").toObject());
    } else if (type == "keyframe") {
        target.setType(Variant::Type::Keyframe);
        QJsonArray children(data.toArray());
        QJsonArray mchildren(metadata.value("values").toArray());
        for (int i = 0, max = std::max(children.size(), mchildren.size()); i < max; i++) {
            QJsonObject mc(mchildren.at(i).toObject());
            double index = mc.value("index").toDouble(FP::undefined());
            if (!FP::defined(index))
                continue;

            if (!parseJSONValue(target.keyframe(index), children.at(i), mc))
                return false;
        }
        return true;
    } else if (type == "metareal" || type == "metadouble") {
        target.setType(Variant::Type::MetadataReal);
        return parseJSONMap(target.toMetadataReal(), data.toObject(),
                            metadata.value("metadata").toObject());
    } else if (type == "metainteger") {
        target.setType(Variant::Type::MetadataInteger);
        return parseJSONMap(target.toMetadataInteger(), data.toObject(),
                            metadata.value("metadata").toObject());
    } else if (type == "metaboolean") {
        target.setType(Variant::Type::MetadataBoolean);
        return parseJSONMap(target.toMetadataBoolean(), data.toObject(),
                            metadata.value("metadata").toObject());
    } else if (type == "metastring") {
        target.setType(Variant::Type::MetadataString);
        return parseJSONMap(target.toMetadataString(), data.toObject(),
                            metadata.value("metadata").toObject());
    } else if (type == "metabinary" || type == "metabytes") {
        target.setType(Variant::Type::MetadataBytes);
        return parseJSONMap(target.toMetadataBytes(), data.toObject(),
                            metadata.value("metadata").toObject());
    } else if (type == "metaflags") {
        target.setType(Variant::Type::MetadataFlags);
        if (!parseJSONMap(target.toMetadataFlags(), data.toObject().value("metadata").toObject(),
                          metadata.value("metadata").toObject()))
            return false;
        return parseJSONMap(target.toMetadataSingleFlag(),
                            data.toObject().value("values").toObject(),
                            metadata.value("values").toObject());
    } else if (type == "metaarray") {
        target.setType(Variant::Type::MetadataArray);
        return parseJSONMap(target.toMetadataArray(), data.toObject(),
                            metadata.value("metadata").toObject());
    } else if (type == "metamatrix") {
        target.setType(Variant::Type::MetadataMatrix);
        return parseJSONMap(target.toMetadataMatrix(), data.toObject(),
                            metadata.value("metadata").toObject());
    } else if (type == "metahash") {
        target.setType(Variant::Type::MetadataHash);
        if (!parseJSONMap(target.toMetadataHash(), data.toObject().value("metadata").toObject(),
                          metadata.value("metadata").toObject()))
            return false;
        return parseJSONMap(target.toMetadataHashChild(),
                            data.toObject().value("values").toObject(),
                            metadata.value("values").toObject());
    } else if (type == "metakeyframe") {
        target.setType(Variant::Type::MetadataKeyframe);
        return parseJSONMap(target.toMetadataKeyframe(), data.toObject(),
                            metadata.value("metadata").toObject());
    } else if (type == "overlay") {
        QString ovr(data.toString());
        if (!ovr.isEmpty())
            target.setOverlay(ovr);
        return true;
    }

    if (data.isDouble()) {
        target.setDouble(data.toDouble(FP::undefined()));
        return true;
    } else if (data.isString()) {
        target.setString(data.toString());
        return true;
    } else if (data.isArray()) {
        target.setType(Variant::Type::Array);
        QJsonArray children(data.toArray());
        QJsonArray mchildren(metadata.value("values").toArray());
        for (int i = 0, max = std::max(children.size(), mchildren.size()); i < max; i++) {
            if (!parseJSONValue(target.array(static_cast<std::size_t>(i)), children.at(i),
                                mchildren.at(i).toObject()))
                return false;
        }
        return true;
    } else if (data.isObject()) {
        target.setType(Variant::Type::Hash);
        return parseJSONMap(target.toHash(), data.toObject(), metadata.value("values").toObject());
    }

    return true;
}

static double parseJSONTime(const QJsonValue &source, bool *ok = nullptr)
{
    if (ok)
        *ok = true;
    if (source.isString()) {
        QString parse(source.toString());
        if (parse.length() > 0) {
            try {
                return TimeParse::parseTime(parse);
            } catch (const TimeParsingException &tpe) {
                qCWarning(log_datacore_externalsource) << "Error parsing JSON time" << parse << ":"
                                                       << tpe.getDescription();
                if (ok)
                    *ok = false;
                return FP::undefined();
            }
        }
    } else if (source.isDouble()) {
        return source.toDouble(FP::undefined());
    }
    return FP::undefined();
}

static bool readJSONData(const QJsonObject &object, SequenceValue &target)
{
    {
        bool ok = false;
        target.setStart(parseJSONTime(object.value("start"), &ok));
        if (!ok)
            return false;
        target.setEnd(parseJSONTime(object.value("end"), &ok));
        if (!ok)
            return false;
    }

    {
        auto station = object.value("station").toString().toStdString();
        if (station.empty()) {
            qCWarning(log_datacore_externalsource) << "Empty station in JSON parsing";
            return false;
        }

        auto archive = object.value("archive").toString().toStdString();
        if (archive.empty()) {
            qCWarning(log_datacore_externalsource) << "Empty variable in JSON parsing";
            return false;
        }

        auto variable = object.value("variable").toString().toStdString();
        if (variable.empty()) {
            qCWarning(log_datacore_externalsource) << "Empty variable in JSON parsing";
            return false;
        }

        target.setName(SequenceName(std::move(station), std::move(archive), std::move(variable),
                                    Util::set_from_qstring(object.value("flavors")
                                                                 .toString()
                                                                 .split(' ',
                                                                        QString::SkipEmptyParts))));
    }

    target.setPriority(object.value("priority").toInt(0));

    Variant::Root v;
    if (!parseJSONValue(v.write(), object.value("value"), object.value("structure").toObject()))
        return false;
    target.setRoot(std::move(v));
    return true;
}

bool StandardDataInput::processJSON()
{
    QJsonParseError err;
    QJsonDocument doc(QJsonDocument::fromJson(processingBuffer.toQByteArrayRef(), &err));
    if (err.error != QJsonParseError::NoError) {
        qCWarning(log_datacore_externalsource) << "JSON parse error:" << err.errorString();
        return false;
    }

    if (doc.isArray()) {
        QJsonArray array(doc.array());
        double lastStart = FP::undefined();
        for (QJsonArray::const_iterator value = array.constBegin(), endV = array.constEnd();
                value != endV;
                ++value) {
            SequenceValue v;
            if (!readJSONData((*value).toObject(), v))
                return false;
            if (Range::compareStart(lastStart, v.getStart()) > 0) {
                qCWarning(log_datacore_externalsource) << "Start times out of order, prior:"
                                                       << Logging::time(lastStart) << "is after"
                                                       << Logging::time(v.getStart());
                continue;
            }
            lastStart = v.getStart();
            outputData(std::move(v));
        }
    } else {
        SequenceValue v;

        QJsonObject obj(doc.object());
        if (!obj.contains("station") && !obj.contains("archive") && !obj.contains("variable")) {
            v.setUnit(SequenceName("nil", "raw", "none"));
            Variant::Root add;
            if (!parseJSONValue(add.write(), obj, QJsonObject()))
                return false;
            v.setRoot(std::move(add));
        } else {
            if (!readJSONData(doc.object(), v))
                return false;
        }

        outputData(std::move(v));
    }

    return true;
}

std::size_t StandardDataInput::processBasicSerialization(QDataStream &stream,
                                                         SequenceValue::Transfer &result)
{
    quint8 type = 0xFFU;
    stream >> type;

    auto maximumIndex = indexLookup.size();
    if (type & 0x80U) {
        type &= 0x7FU;
        if (type == 0U) {
            SequenceName unit;
            stream >> unit;
            if (nextIndex >= maximumIndex) {
                indexLookup.emplace_back(std::move(unit));
            } else {
                indexLookup[nextIndex] = std::move(unit);
            }
            ++nextIndex;
            Q_ASSERT(nextIndex >= 0 && nextIndex <= 0xFFFFU);
            return 1;
        } else if (type == 1U) {
            processingEnded = true;
            return 1;
        }

        qCWarning(log_datacore_externalsource) << "Unrecognized serialization packet type: "
                                               << type;
        return 0;
    }
    Q_ASSERT(type <= 0x7FU);

    double start = FP::undefined();
    double end = FP::undefined();
    qint32 priority = 0;

    stream >> start >> end >> priority;
    for (std::uint_fast8_t n = 0; n < static_cast<std::uint_fast8_t>(type); n++) {
        quint16 unitIndex = 0;
        stream >> unitIndex;
        Variant::Root value = Variant::Root::deserialize(stream);

        if (static_cast<std::size_t>(unitIndex) >= maximumIndex) {
            qCWarning(log_datacore_externalsource) << "Unit index" << unitIndex << "out of range";
            continue;
        }

        result.emplace_back(
                SequenceIdentity(indexLookup[unitIndex], start, end, static_cast<int>(priority)),
                std::move(value));
    }
    return static_cast<std::size_t>(type);
}

bool StandardDataInput::processRaw()
{
    Util::ByteArray::Device dev(processingBuffer);
    dev.open(QIODevice::ReadOnly);
    QDataStream stream(&dev);
    stream.setByteOrder(QDataStream::LittleEndian);
    stream.setVersion(QDataStream::Qt_4_5);

    SequenceValue::Transfer result;
    std::size_t counter = 0;
    std::size_t offset = 0;
    for (;;) {
        if (counter >= bufferProcessIterations || result.size() > bufferProcessSize) {
            processingBuffer.pop_front(offset);
            if (!result.empty()) {
                outputData(std::move(result));
            }
            return true;
        }

        if (isHeader) {
            if (processingBuffer.size() < 3)
                break;
            Q_ASSERT(processingBuffer.front() == 0xC4U);
            Q_ASSERT(processingBuffer[1] == 0xD3U);
            if (processingBuffer[2] != 1U && processingBuffer[2] != 2) {
                qCWarning(log_datacore_externalsource) << "Unrecognized data format version"
                                                       << processingBuffer[2];
            }
            isHeader = false;
            offset += 3;
            continue;
        }
        auto effectiveLength = processingBuffer.size() - offset;
        if (effectiveLength <= 2)
            break;

        quint32 length = qFromLittleEndian<quint16>(processingBuffer.data<const uchar *>(offset));
        if (length == 0xFFFFU) {
            length = qFromLittleEndian<quint32>(processingBuffer.data<const uchar *>(offset + 2));
            if (effectiveLength - 6 < static_cast<std::size_t>(length))
                break;
            offset += 6;
        } else {
            if (effectiveLength - 2 < static_cast<std::size_t>(length))
                break;
            offset += 2;
        }
        if (length <= 2 && (length != 1 && processingBuffer[offset] != 0x81U)) {
            qCWarning(log_datacore_externalsource) << "Incoming length (" << length
                                                   << ") is too short to be valid";
            continue;
        }
        dev.seek(offset);
        offset += length;

        counter += processBasicSerialization(stream, result);
    }
    if (offset > 0) {
        processingBuffer.pop_front(offset);
    }

    if (!result.empty()) {
        outputData(std::move(result));
    }

    return !processingEnded;
}

bool StandardDataInput::processDirect()
{
    Util::ByteArray::LineIterator it(processingBuffer);

    std::size_t counter = 0;
    SequenceValue::Transfer result;
    while (auto line = it.next(processingEnded)) {
        if (isHeader) {
            if (!line.string_start("CPD3DATA")) {
                qCWarning(log_datacore_externalsource) << "Unrecognized data format" << line;
                isHeader = false;
                continue;
            }
            auto idxD = line.indexOf('-');
            if (idxD == line.npos) {
                qCWarning(log_datacore_externalsource) << "Unrecognized data format" << line;
            } else {
                auto idxSpace = line.indexOf(' ');
                if (idxSpace != line.npos && idxSpace > idxD) {
                    line = line.mid(idxD + 1, idxSpace - idxD - 1);
                } else {
                    line = line.mid(idxD + 1);
                }
                if (!line.string_equal("1.0") && !line.string_equal("2.0")) {
                    qCWarning(log_datacore_externalsource) << "Unrecognized data format version"
                                                           << line;
                }
            }

            isHeader = false;
            continue;
        }

        QByteArray decoded(QByteArray::fromBase64(line.toQByteArrayRef()));
        QDataStream stream(&decoded, QIODevice::ReadOnly);
        stream.setByteOrder(QDataStream::LittleEndian);
        stream.setVersion(QDataStream::Qt_4_5);

        counter += processBasicSerialization(stream, result);
        if (counter >= bufferProcessIterations || result.size() > bufferProcessSize) {
            processingBuffer.pop_front(it.getCurrentOffset());
            if (!result.empty()) {
                outputData(std::move(result));
            }
            return true;
        }
    }

    processingBuffer.pop_front(it.getCurrentOffset());

    if (!result.empty()) {
        outputData(std::move(result));
    }

    return !processingEnded;
}

/* This exists so we don't send the end twice */
StandardDataInput::DiscardIngress::DiscardIngress() = default;

StandardDataInput::DiscardIngress::~DiscardIngress() = default;

void StandardDataInput::DiscardIngress::incomingData(const SequenceValue::Transfer &)
{ Q_ASSERT(false); }

void StandardDataInput::DiscardIngress::incomingData(SequenceValue::Transfer &&)
{ Q_ASSERT(false); }

void StandardDataInput::DiscardIngress::incomingData(const SequenceValue &)
{ Q_ASSERT(false); }

void StandardDataInput::DiscardIngress::incomingData(SequenceValue &&)
{ Q_ASSERT(false); }

void StandardDataInput::DiscardIngress::endData()
{ }

bool StandardDataInput::createExternal(const QString &name)
{
    ExternalConverterComponent
            *component = qobject_cast<ExternalConverterComponent *>(ComponentLoader::create(name));
    if (!component || !component->requiresInputDevice())
        return false;
    child.reset(component->createDataIngress());
    if (!child)
        return false;

    child->accelerationKey.connect([this](const std::string &key) { this->accelerationKey(key); });
    child->finished.connect([this] { this->finished(); });

    /* It has no reference to us, so this is safe with the mutex held */
    child->setEgress(egress);
    egress = &childDiscard;

    child->start();
    child->incomingData(std::move(buffer));
    if (dataEnded)
        child->endData();
    buffer.clear();
    return true;
}

bool StandardDataInput::processExternal()
{
    Q_ASSERT(child != nullptr);
    return !child->isFinished();
}

static bool isJSONData(const Util::ByteArray &buffer)
{
    static thread_local QRegularExpression check(R"(^\s*[\[\{]\s*[\[\{"])");
    return check.match(buffer.toQString(), 0, QRegularExpression::NormalMatch,
                       QRegularExpression::AnchoredMatchOption).hasMatch();
}

static bool isCPD2Data(const Util::ByteArray &buffer)
{
    static thread_local QRegularExpression
            check(R"(^![a-z][a-z0-9_]{2})", QRegularExpression::CaseInsensitiveOption);
    return check.match(buffer.toQString(), 0, QRegularExpression::NormalMatch,
                       QRegularExpression::AnchoredMatchOption).hasMatch();
}

static bool isEBASData(const Util::ByteArray &buffer)
{
    static thread_local QRegularExpression check(R"(^\s*\d+\s+1001\s*[\n\r])");
    return check.match(buffer.toQString(), 0, QRegularExpression::NormalMatch,
                       QRegularExpression::AnchoredMatchOption).hasMatch();
}

static bool isBase64(const Util::ByteArray &buffer)
{
    static thread_local QRegularExpression check(R"(^[A-Za-z0-9+/]+=*[\n\r]?)");
    return check.match(buffer.toQString(), 0, QRegularExpression::NormalMatch,
                       QRegularExpression::AnchoredMatchOption).hasMatch();
}

bool StandardDataInput::identifyStream()
{
    auto length = buffer.size();
    if (length > 4 && buffer.string_start("<?xml")) {
        type = InputType::XML;
        qCDebug(log_datacore_externalsource) << "Input identified as XML CPD3 data";
        return true;
    } else if (length > 8 && buffer.string_start("CPD3DATA")) {
        auto data = buffer.data<char *>();
        auto ptrN = reinterpret_cast<char *>(std::memchr(data, '\n', length));
        if (ptrN)
            length = ptrN - data;
        auto ptrR = reinterpret_cast<char *>(std::memchr(data, '\r', length));

        if (ptrN || ptrR) {
            QString parse;
            if (!ptrR || ptrN < ptrR) {
                parse = QString::fromUtf8(data, static_cast<int>(ptrN - data));
            } else {
                parse = QString::fromUtf8(data, static_cast<int>(ptrR - data));
            }

            int idxSpace = parse.indexOf(' ');
            if (idxSpace != -1) {
                processAccelerationKey = parse.mid(idxSpace + 1).toStdString();
            }

            type = InputType::Direct;
            qCDebug(log_datacore_externalsource) << "Input identified as base-64 CPD3 data";
            return true;
        } else if (dataEnded) {
            type = InputType::Unidentified;
            return true;
        }
    } else if (length > 2 && buffer.front() == 0xC4U && buffer[1] == 0xD3U) {
        type = InputType::Raw;
        qCDebug(log_datacore_externalsource) << "Input identified as binary CPD3 data";
        return true;
    } else if ((length > 2 || dataEnded) && isJSONData(buffer)) {
        type = InputType::JSON;
        qCDebug(log_datacore_externalsource) << "Input identified as JSON data";
        return true;
    } else if (length > 4 && isCPD2Data(buffer)) {
        type = InputType::CPD2;
        qCDebug(log_datacore_externalsource) << "Input identified as CPD2 data";
        return true;
    } else if (length > 6 && isEBASData(buffer)) {
        type = InputType::EBAS;
        qCDebug(log_datacore_externalsource) << "Input identified as EBAS data";
        return true;
    } else if (length > 9 || dataEnded) {
        if (isBase64(buffer)) {
            buffer -= "CPD3DATA-2.0\n";
            type = InputType::Direct;
            qCDebug(log_datacore_externalsource) << "Input defaulting to base-64 CPD3 data";
            return true;
        } else {
            type = InputType::Unidentified;
            return true;
        }
    }

    return false;
}

bool StandardDataInput::prepare()
{
    if (type == InputType::Unidentified) {
        if (!identifyStream()) {
            if (!dataEnded)
                return false;
        }

        switch (type) {
        case InputType::XML:
            xmlContainer.reset(new Internal::XMLContainer(*this));
            break;

        case InputType::CPD2:
            if (!createExternal("import_cpd2")) {
                type = InputType::Unidentified;
                return true;
            }
            break;

        case InputType::EBAS:
            if (!createExternal("import_ebas")) {
                type = InputType::Unidentified;
                return true;
            }
            break;

        default:
            break;
        }
    }

    switch (type) {
    case InputType::CPD2:
    case InputType::EBAS:
        return child->isFinished();
    default:
        break;
    }

    if (!processAccelerationKey.empty())
        return true;

    /* Don't copy out of buffers while we can't output, so the limiting
     * works while paused */
    if (!egress && !dataEnded)
        return false;

    if (!buffer.empty()) {
        /*
         * If we've done a consume, do a process before we take anything out of the
         * processing buffer.  Since the processing does a lot of incremental emits
         * without needing more data, we want to leave it here if we can so stalling
         * works correctly.
         */
        if (!dataEnded) {
            if (bufferConsumed && processingBuffer.size() > 2 * 1024 * 1024) {
                return true;
            }
        }

        processingBuffer += std::move(buffer);
        buffer.clear();
        processingEnded = dataEnded;
        return true;
    } else if (dataEnded) {
        processingEnded = dataEnded;
        return true;
    }

    return false;
}

bool StandardDataInput::process()
{
    if (!processAccelerationKey.empty()) {
        accelerationKey(processAccelerationKey);
        processAccelerationKey.clear();
    }
    switch (type) {
    case InputType::Unidentified:
        return false;
    case InputType::Direct: {
        Q_ASSERT(egress != nullptr);
        auto oldSize = processingBuffer.size();
        if (!processDirect())
            return false;
        bufferConsumed = (oldSize != processingBuffer.size());
        return true;
    }
    case InputType::XML:
        Q_ASSERT(egress != nullptr);
        return processXML();
    case InputType::Raw: {
        Q_ASSERT(egress != nullptr);
        auto oldSize = processingBuffer.size();
        if (!processRaw())
            return false;
        bufferConsumed = (oldSize != processingBuffer.size());
        return true;
    }
    case InputType::JSON:
        Q_ASSERT(egress != nullptr);
        if (!processingEnded)
            return true;
        processJSON();
        return false;
    case InputType::CPD2:
    case InputType::EBAS:
        return processExternal();
    }
    Q_ASSERT(false);
    return false;
}


ExternalConverterComponent::~ExternalConverterComponent() = default;

bool ExternalConverterComponent::requiresInputDevice()
{ return true; }

ComponentOptions ExternalConverterComponent::getOptions()
{ return ComponentOptions(); }

QList<ComponentExample> ExternalConverterComponent::getExamples()
{ return QList<ComponentExample>(); }


ExternalSourceComponent::~ExternalSourceComponent() = default;

ComponentOptions ExternalSourceComponent::getOptions()
{ return ComponentOptions(); }

QList<ComponentExample> ExternalSourceComponent::getExamples()
{ return QList<ComponentExample>(); }

int ExternalSourceComponent::ingressAllowStations()
{ return 0; }

int ExternalSourceComponent::ingressRequireStations()
{ return 0; }

bool ExternalSourceComponent::ingressRequiresTime()
{ return false; }

bool ExternalSourceComponent::ingressAcceptsUndefinedBounds()
{ return true; }

Time::LogicalTimeUnit ExternalSourceComponent::ingressDefaultTimeUnit()
{ return Time::Day; }

ExternalConverter *ExternalSourceComponent::createExternalIngress(const ComponentOptions &options,
                                                                  const std::vector<
                                                                          SequenceName::Component> &stations)
{ return createExternalIngress(options, FP::undefined(), FP::undefined(), stations); }

}
}