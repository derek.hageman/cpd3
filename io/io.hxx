/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3IO_HXX
#define CPD3IO_HXX

#include <QtCore/QtGlobal>

#if defined(cpd3io_EXPORTS)
#   define CPD3IO_EXPORT Q_DECL_EXPORT
#else
#   define CPD3IO_EXPORT Q_DECL_IMPORT
#endif

#endif
