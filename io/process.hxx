/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_PROCESS_HXX
#define CPD3IO_PROCESS_HXX

#include "core/first.hxx"

#include <vector>
#include <string>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <QString>
#include <QStringList>

#include "io.hxx"
#include "access.hxx"
#include "core/number.hxx"
#include "core/threading.hxx"

namespace CPD3 {
namespace IO {
namespace Process {

/**
 * The stream type for a process channel.
 */
class CPD3IO_EXPORT Stream : public Generic::Stream {
public:
    Stream();

    virtual ~Stream();

    /**
     * Explicitly close the stream, informing the process that is cannot accept more data.
     */
    virtual void close() = 0;
};

/**
 * An instance of a process.  The default stream method creates a connection to
 * the combined input, output, and error streams, as available.
 */
class CPD3IO_EXPORT Instance : public Generic::Backing {
public:
    virtual ~Instance();

    /**
     * Connect the process's output to another process's input.  This pipes
     * the output from the process into the input of another one.  Neither process
     * can be started.
     *
     * @param target    the target to pipe output to
     */
    virtual void pipeTo(const std::shared_ptr<Instance> &target) = 0;

    /**
     * Start the process.
     *
     * @return  true on success
     */
    virtual bool start() = 0;

    /**
     * Start the process detached.  I/O channels may not be available depending
     * on the backend used.
     *
     * @return  true on success
     */
    virtual bool detach() = 0;

    /**
     * Get a stream that connects to the process's standard input.  The stream never reads
     * anything but will only end when the process does or it closes its input (so it
     * cannot accept more data).
     * <br>
     * The process must be have the input in capture mode.
     *
     * @return  a write only stream connected to the process's standard input
     */
    virtual std::unique_ptr<Stream> inputStream() = 0;

    /**
     * Get a stream that connects to the process's standard output.  The stream allows
     * reading of anything the process writes.
     * <br>
     * The process must be have the output in capture mode.
     *
     * @param allowWrite    writes to the stream write to standard input
     * @return              a stream connected to the process's standard output
     */
    virtual std::unique_ptr<Stream> outputStream(bool allowWrite = false) = 0;

    /**
     * Get a stream that connects to the process's standard error.  The stream allows
     * reading of anything the process writes.
     * <br>
     * The process must be have the error in capture mode.
     *
     * @param allowWrite    writes to the stream write to standard error
     * @return              a stream connected to the process's standard output
     */
    virtual std::unique_ptr<Stream> errorStream(bool allowWrite = false) = 0;

    /**
     * Get a stream that connects to the process's standard output and standard error.
     * <br>
     * At least one of the channels must be in capture mode.
     *
     * @param allowWrite    writes to the stream write to standard input
     * @return              a stream connected to the process's output channels
     */
    virtual std::unique_ptr<Stream> combinedOutputStream(bool allowWrite = false) = 0;

    /**
     * Terminate the process.
     */
    virtual void terminate() = 0;

    /**
     * Kill the process.
     */
    virtual void kill() = 0;

    /**
     * Test if the process is currently running.
     */
    virtual bool isRunning() const = 0;

    /**
     * Wait for the process to complete.
     *
     * @param timeout   the maximum time to wait in seconds
     * @return          true if the process has completed
     */
    virtual bool wait(double timeout = FP::undefined());

    /**
     * Emitted when the process exits, with the exit code and a flag if the exit was
     * a normal exit.
     */
    Threading::Signal<int, bool> exited;

    /**
     * A simple monitor for checking if a process exits normally.
     */
    class CPD3IO_EXPORT ExitMonitor final {
        Instance &process;
        Threading::Receiver receiver;
        int exitCode;
        bool exitNormal;
    public:
        /**
         * Create the monitor.
         * <br>
         * The process must be started after this call.
         *
         * @param process   the process to monitor
         */
        explicit ExitMonitor(Instance &process);

        ~ExitMonitor();

        /**
         * Wait for the process to exit.
         *
         * @return  true if the process has completed
         */
        bool wait();

        /**
         * Test if the process exited normally.
         *
         * @return  true if the process did a normal (non-crash) exit
         */
        bool isNormal();

        /**
         * Get the exit code of the process.
         *
         * @return  the process exit code
         */
        int getExitCode();

        /**
         * Test if the process has completed normally with an exit code of zero.
         *
         * @return  true if the process succeeded
         */
        bool success();

        /**
         * Connect a terminate request signal to call the process terminate.
         *
         * @param source    the source signal
         */
        void connectTerminate(Threading::Signal<> &source);
    };

protected:
    Instance();
};

/**
 * A builder structure for a process.
 */
struct CPD3IO_EXPORT Spawn {
    /**
     * The command to run.
     */
    std::string command;

    /**
     * The arguments to the process.
     */
    std::vector<std::string> arguments;

    /**
     * The handling of the process environment.
     */
    enum class Environment {
        /**
         * Inherit the environment from the current process.
         */
                Inherit,

        /**
         * Start with an empty environment.
         */
                Empty,
    } environment;

    /**
     * The environment variables to set.
     */
    std::unordered_map<std::string, std::string> environmentSet;

    /**
     * The environment variables to clear (unset).
     */
    std::unordered_set<std::string> environmentClear;

    /**
     * The handling mode for a process I/O stream.
     */
    enum class StreamMode {
        /**
         * Discard all contents of the stream.
         */
                Discard,

        /**
         * Forward the stream to the same stream of the parent.
         */
                Forward,

        /**
         * Capture the stream for access by the parent.
         */
                Capture,

        /**
         * Redirect the stream to a file.
         */
                File,

        /**
         * Redirect the stream to a file, appending output.
         */
                FileAppend
    };

    /**
     * Standard input handling mode.
     * <br>
     * Defaults to capture, allowing writes to the child process.
     */
    StreamMode inputStream;

    /**
     * The file used for input, if in file access mode.
     */
    std::string inputFile;

    /**
     * Standard output handling mode.
     * <br>
     * Defaults to capture, allowing reads from the child process.
     */
    StreamMode outputStream;

    /**
     * The file used for output, if in file access mode.
     */
    std::string outputFile;

    /**
     * Standard error handling mode.
     * <br>
     * Defaults to forward, so the child displays errors to the parent's standard error.
     */
    StreamMode errorStream;

    /**
     * The file used for error output, if in file access mode.
     */
    std::string errorFile;


    /**
     * Create the process handle.
     * <br>
     * This does not start the process.
     *
     * @return  a handle to the process instance
     */
    std::shared_ptr<Instance> create() const;

    /** @see create() */
    inline std::shared_ptr<Instance> operator()() const
    { return create(); }

    /**
     * Create the process and detach it.
     *
     * @return  true on success
     */
    bool detach() const;

    /**
     * Run the process, wait for it to exit and return true if the
     * entire operation was successful (normal exit and zero exit code).
     *
     * @return  true on success
     */
    bool run() const;


    Spawn();

    ~Spawn();

    Spawn(const Spawn &) = default;

    Spawn &operator=(const Spawn &) = default;

    Spawn(Spawn &&) = default;

    Spawn &operator=(Spawn &&) = default;


    /**
     * Parse a given string into a process and arguments.  Quoting and escaping are
     * applied.
     *
     * @param parse the string to parse
     */
    Spawn(const std::string &parse);

    Spawn(const QString &parse);

    Spawn(const char *parse);

    /**
     * Create a process spawn builder.
     *
     * @param command       the process command
     * @param arguments     the arguments to the command
     */
    Spawn(std::string command, std::vector<std::string> arguments);

    Spawn(const char *command, std::vector<std::string> arguments);

    Spawn(const QString &command, const QStringList &arguments);


    /**
     * Set all streams of the builder to discard.
     *
     * @return  the current builder
     */
    Spawn &discard();

    /**
     * Set all streams of the builder to forward.
     *
     * @return  the current builder
     */
    Spawn &forward();

    /**
     * Set all streams of the builder to capture.
     *
     * @return  the current builder
     */
    Spawn &capture();

    /**
     * Add an argument to the process.
     *
     * @param argument  the argument
     * @return          the current builder
     */
    Spawn &operator+=(std::string argument);

    Spawn &operator+=(const char *argument);

    Spawn &operator+=(const QString &argument);


    /**
     * Create a builder for a shell command string.  The interpreter varies based on the
     * host operating system (e.g. /bin/sh on Unix, CMD.exe on Windows).
     *
     * @param command   the shell command
     * @return          a new process builder
     */
    static Spawn shell(const std::string &command);

    static Spawn shell(const QString &command);

    static Spawn shell(const char *command);


    /**
     * A container for a pipeline of processes.
     */
    struct CPD3IO_EXPORT Pipeline {
        std::vector<std::shared_ptr<Instance>> processes;

        ~Pipeline();

        /**
         * Start all processes in the pipeline.
         *
         * @return   true on success
         */
        bool start();

        /**
         * Start the pipeline in detached mode.
         *
         * @return  true on success
         */
        bool detach();

        /**
         * Terminate all processes in the pipeline.
         */
        void terminate();

        /**
         * Kill all processes in the pipeline.
         */
        void kill();

        /**
         * Wait for all processes in the pipeline to complete.
         *
         * @param timeout   the maximum time to wait in seconds
         * @return          true if the pipeline has completed
         */
        bool wait(double timeout = FP::undefined());

        /**
         * Get the input handle to the pipeline.
         *
         * @return          the pipeline input
         */
        inline const std::shared_ptr<Instance> &input() const
        { return processes.front(); }

        /**
         * get the output handle to the pipeline.
         *
         * @return          the pipeline output
         */
        inline const std::shared_ptr<Instance> &output() const
        { return processes.back(); }
    };

    /**
     * Create a pipeline from process spawn builders.
     *
     * @param spawn     the spawn builders
     * @return          an un-started process pipeline
     */
    static Pipeline pipeline(const std::vector<Spawn> &spawn);
};

#ifdef Q_OS_UNIX

/**
 * Attempt to close all open file descriptors of the current process.
 * <br>
 * This generally only makes sense after a fork and before entering something that will
 * do an _exit() to avoid cleanup.
 */
CPD3IO_EXPORT void closeAllOpenFDs();

/**
 * Attempt to daemonize the current process.  This starts a new process group, changes
 * directory to / and optionally redirects stdio to /dev/null.  Failure results in
 * an _exit().
 * <br>
 * This generally only makes sense after a fork() very near the start of a process.
 *
 * @param keepStdio keep standard I/O open
 */
CPD3IO_EXPORT void executeDaemonize(bool keepStdio = false);

/**
 * Set the process name.
 * <br>
 * Note that QCoreApplication does not allow the pointers it is passed to be modified, so it
 * must be constructed with copies if this function is used.
 *
 * @param name  the name
 * @param argc  the original argc
 * @param argv  the original argv
 */
CPD3IO_EXPORT void setProcessName(const std::string &name, int &argc, char **&argv);

#endif

/**
 * A class that provides a lock to change the current working directory of the entire process.
 * On destruction, the old working directory is restored.  Only a single instance of the
 * change is permitted process wide, so other attempts will block.
 * <br>
 * Note that this still changes the working directory for any other threads, so anything
 * else running that's counting on it would also be affected.
 */
class CPD3IO_EXPORT WorkingDirectoryLock final {
public:
    WorkingDirectoryLock() = delete;

    WorkingDirectoryLock(const WorkingDirectoryLock &) = delete;

    WorkingDirectoryLock(WorkingDirectoryLock &&) = delete;

    WorkingDirectoryLock &operator=(const WorkingDirectoryLock &) = delete;

    WorkingDirectoryLock &operator=(WorkingDirectoryLock &&) = delete;


    /**
     * Acquire the lock, blocking if there is an existing lock.
     *
     * @param directory the directory to change to
     */
    explicit WorkingDirectoryLock(const QString &directory);

    explicit WorkingDirectoryLock(const std::string &directory);

    explicit WorkingDirectoryLock(const char *directory);

    ~WorkingDirectoryLock();
};

}
}
}

#endif //CPD3IO_PROCESS_HXX
