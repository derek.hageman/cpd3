/*
 * Copyright (c) 2020 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_DRIVERS_URL_HXX
#define CPD3IO_DRIVERS_URL_HXX

#include "core/first.hxx"

#include <functional>
#include <memory>
#include <QUrl>
#include <QSslConfiguration>
#include <QNetworkRequest>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>

#include "io/io.hxx"
#include "io/access.hxx"

namespace CPD3 {
namespace IO {

class QIOWrapper;

namespace URL {

class Stream;

class Backing;

/**
 * A context to use with URL access.
 */
class CPD3IO_EXPORT Context final {
    class Thread;

    std::unique_ptr<Thread> thread;

    friend class Stream;

    friend class Backing;

public:
    Context();

    ~Context();
};

/**
 * A stream attached to an in progress URL request.
 */
class CPD3IO_EXPORT Stream final : public Generic::Stream {
    std::shared_ptr<Context> context;
    std::unique_ptr<QIOWrapper> wrapper;
    QNetworkReply *reply;
    QTimer *timeout;

    Stream(Backing &backing,
           const std::function<
                   QNetworkReply *(QNetworkRequest &request, QNetworkAccessManager &manager)> &rep);

    friend class Backing;

public:
    Stream() = delete;

    virtual ~Stream();

    /**
     * Start the timeout on the request, aborting it if it does not complete.
     *
     * @param seconds   the maximum time to wait
     */
    void startTimeout(double seconds);

    /**
     * Test if the URL response contained an error.  Only valid after ended() is emitted.
     *
     * @return  true if there was an error
     */
    bool responseError() const;

    /**
     * Abort the request.
     */
    void abort();

    void write(const Util::ByteView &) override;

    void write(const Util::ByteArray &) override;

    void write(Util::ByteArray &&) override;

    void write(const QByteArray &) override;

    void write(QByteArray &&) override;

    void start() override;

    bool isEnded() const override;

    void readStall(bool enable) override;

    /**
     * Emitted while the request is in progress, with the fraction completed.  This is not
     * guaranteed to be emitted or seen by the caller, because the download started immediately
     * on stream creation.
     */
    Threading::Signal<double> receiveProgress;
};

/**
 * A backing for URL access.  The request is not executed until a stream is
 * is requested.  Each stream creation repeats the request.
 */
class CPD3IO_EXPORT Backing : public Generic::Backing {
    std::shared_ptr<Context> context;
    QNetworkRequest request;

    friend class Stream;

public:
    Backing() = delete;

    virtual ~Backing();

    /**
     * Set the TLS/SSL configuration used with the request.
     *
     * @param tls   the TLS configuration
     */
    void tls(const QSslConfiguration &tls);

    /**
     * Set a fixed type header in the request.
     *
     * @param name  the header type
     * @param value the header value
     */
    void header(QNetworkRequest::KnownHeaders name, const QVariant &value);

    /**
     * Set a raw header in the request.
     *
     * @param name  the header name
     * @param value the header value
     */
    void header(const Util::ByteView &name, const Util::ByteView &value);

    /**
     * Get the request URL.
     *
     * @return  the request URL
     */
    QUrl url() const;

protected:
    /**
     * Create the backing handler.
     *
     * @param url       the URL for the request
     * @param context   the context to execute the request with
     */
    Backing(const QUrl &url, std::shared_ptr<Context> context);

    /**
     * Perform a read only stream request.
     *
     * @param reply     the function to create the reply, executed on the manager thread
     * @return          a read stream
     */
    std::unique_ptr<Generic::Stream> create_stream(const std::function<
            QNetworkReply *(QNetworkRequest &request, QNetworkAccessManager &manager)> &reply);
};

}


namespace Access {

CPD3IO_EXPORT Handle urlGET(const std::string &url, std::shared_ptr<URL::Context> context = {});

CPD3IO_EXPORT Handle urlGET(const QUrl &url, std::shared_ptr<URL::Context> context = {});

CPD3IO_EXPORT Handle urlPUT(const std::string &url,
                            Handle data,
                            std::shared_ptr<URL::Context> context = {});

CPD3IO_EXPORT Handle urlPUT(const QUrl &url,
                            Handle data,
                            std::shared_ptr<URL::Context> context = {});

CPD3IO_EXPORT Handle urlPUT(const std::string &url,
                            std::unique_ptr<IO::Generic::Stream> &&data,
                            std::shared_ptr<URL::Context> context = {});

CPD3IO_EXPORT Handle urlPUT(const QUrl &url,
                            std::unique_ptr<IO::Generic::Stream> &&data,
                            std::shared_ptr<URL::Context> context = {});

CPD3IO_EXPORT Handle urlPOST(const std::string &url,
                             Handle data,
                             std::shared_ptr<URL::Context> context = {});

CPD3IO_EXPORT Handle urlPOST(const QUrl &url,
                             Handle data,
                             std::shared_ptr<URL::Context> context = {});

CPD3IO_EXPORT Handle urlPOST(const std::string &url,
                             std::unique_ptr<IO::Generic::Stream> &&data,
                             std::shared_ptr<URL::Context> context = {});

CPD3IO_EXPORT Handle urlPOST(const QUrl &url,
                             std::unique_ptr<IO::Generic::Stream> &&data,
                             std::shared_ptr<URL::Context> context = {});

}

}
}

#endif //CPD3IO_DRIVERS_URL_HXX
