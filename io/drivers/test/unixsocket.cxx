/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QTemporaryFile>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>


#include "io/drivers/unixsocket.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::IO;

class TestUnixSocket : public QObject {
Q_OBJECT

    static QByteArray readData(int fd, int expected)
    {
        ElapsedTimer et;
        et.start();

        QByteArray target;
        while (et.elapsed() < 10000) {
            int original = target.size();
            if (original >= expected)
                return target;
            target.resize(expected);

            auto n = ::read(fd, target.data() + original, expected - original);
            if (n == 0) {
                target.resize(original);
                return target;
            } else if (n < 0) {
                int en = errno;
                if (en != EAGAIN && en != EINTR && en != EWOULDBLOCK) {
                    target.resize(original);
                    return target;
                }
                target.resize(original);
                QTest::qSleep(1);
                continue;
            }

            target.resize(original + (int) n);
        }
        return target;
    }

    static bool writeData(int fd, const QByteArray &data)
    {
        ElapsedTimer et;
        et.start();

        int written = 0;
        while (written < data.size()) {
            if (et.elapsed() > 10000)
                return false;

            auto n = ::write(fd, data.data() + written, data.size() - written);
            if (n < 0) {
                int en = errno;
                if (en != EAGAIN && en != EINTR && en != EWOULDBLOCK) {
                    return false;
                }
                QTest::qSleep(1);
                continue;
            }
            written += (int) n;
        }
        return true;
    }

    static bool waitForClosed(int fd)
    {
        ElapsedTimer et;
        et.start();

        while (et.elapsed() < 10000) {
            char dummy = 0;
            auto n = ::read(fd, &dummy, 1);
            if (n == 0) {
                return true;
            } else if (n < 0) {
                int en = errno;
                if (en != EAGAIN && en != EINTR && en != EWOULDBLOCK) {
                    return true;
                }
                QTest::qSleep(1);
                continue;
            }
        }

        return false;
    }

    std::string socketPath;

private slots:

    void initTestCase()
    {
        CPD3::Logging::suppressForTesting();

        {
            QTemporaryFile file;
            file.setAutoRemove(false);
            QVERIFY(file.open());
            socketPath = file.fileName().toStdString();
        }
        ::unlink(socketPath.c_str());
    }

    void cleanupTestCase()
    {
        ::unlink(socketPath.c_str());
    }

    void init()
    {
        ::unlink(socketPath.c_str());
    }

    void client()
    {
        int server = ::socket(AF_UNIX, SOCK_STREAM, 0);
        QVERIFY(server != -1);
        {
            struct ::sockaddr_un addr;
            ::memset(&addr, 0, sizeof(addr));
            addr.sun_family = AF_UNIX;
            ::strncpy(addr.sun_path, socketPath.c_str(), sizeof(addr.sun_path));
            addr.sun_path[sizeof(addr.sun_path) - 1] = 0;
            QVERIFY(::bind(server, (const struct ::sockaddr *) &addr, sizeof(addr)) != -1);
        }
        ::listen(server, 8);

        std::unique_ptr<Socket::Unix::Connection> stream;
        std::thread thd([&] {
            stream = Socket::Unix::connect(socketPath);
        });

        int connection = ::accept(server, nullptr, 0);
        QVERIFY(connection != -1);

        thd.join();
        QVERIFY(stream.get() != nullptr);
        QCOMPARE(stream->socketPath(), socketPath);

        std::mutex mutex;
        std::condition_variable cv;
        Util::ByteArray dataRead;
        bool didEnd = false;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            didEnd = true;
            cv.notify_all();
        });
        stream->start();

        QByteArray send = "Some data";
        stream->write(send);
        QCOMPARE(readData(connection, send.size()), send);

        QVERIFY(writeData(connection, send.mid(0, 2)));
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return dataRead.size() >= 2; });
        }
        stream->readStall(true);
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), send.mid(0, 2));
        }

        QVERIFY(writeData(connection, send.mid(2)));
        stream->readStall(false);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= send.size(); });
        }
        stream.reset();

        QCOMPARE(dataRead.toQByteArrayRef(), send);
        QVERIFY(waitForClosed(connection));
        ::close(connection);

        thd = std::thread([&] {
            connection = ::accept(server, nullptr, 0);
        });
        stream = Socket::Unix::connect(socketPath);

        QVERIFY(stream.get() != nullptr);
        thd.join();
        QVERIFY(connection != -1);

        ::close(server);
        server = -1;

        dataRead.clear();
        didEnd = false;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            didEnd = true;
            cv.notify_all();
        });
        stream->start();

        QVERIFY(writeData(connection, send));
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= send.size(); });
        }
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), send);
            dataRead.clear();
        }

        stream->write(Util::ByteArray(send));
        QCOMPARE(readData(connection, send.size()), send);

        QVERIFY(writeData(connection, send));

        ::close(connection);
        connection = -1;

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return didEnd; });
        }

        QVERIFY(didEnd);
        QCOMPARE(dataRead.toQByteArrayRef(), send);
        QVERIFY(stream->isEnded());

        stream.reset();
    }

    void serverBasic()
    {
        std::mutex mutex;
        std::condition_variable cv;
        std::unique_ptr<Socket::Unix::Connection> stream;

        Socket::Unix::Server server([&](std::unique_ptr<Socket::Connection> &&conn) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                Q_ASSERT(!stream);
                Q_ASSERT(dynamic_cast<Socket::Unix::Connection *>(conn.get()));
                stream = std::unique_ptr<Socket::Unix::Connection>(
                        dynamic_cast<Socket::Unix::Connection *>(conn.release()));
                cv.notify_all();
            }
        }, socketPath);

        QVERIFY(server.startListening());
        QVERIFY(server.isListening());

        int client = ::socket(AF_UNIX, SOCK_STREAM, 0);
        QVERIFY(client != -1);
        {
            struct ::sockaddr_un addr;
            ::memset(&addr, 0, sizeof(addr));
            addr.sun_family = AF_UNIX;
            ::strncpy(addr.sun_path, socketPath.c_str(), sizeof(addr.sun_path));
            addr.sun_path[sizeof(addr.sun_path) - 1] = 0;
            QVERIFY(::connect(client, (const struct ::sockaddr *) &addr, sizeof(addr)) != -1);
        }

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return stream.get() != nullptr; });
        }
        QVERIFY(stream.get() != nullptr);
        QCOMPARE(stream->socketPath(), socketPath);

        Util::ByteArray dataRead;
        bool didEnd = false;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            didEnd = true;
            cv.notify_all();
        });
        stream->start();

        QByteArray send = "Some data";
        stream->write(send);
        QCOMPARE(readData(client, send.size()), send);

        QVERIFY(writeData(client, send.mid(0, 2)));
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return dataRead.size() >= 2; });
        }
        stream->readStall(true);
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), send.mid(0, 2));
        }

        QVERIFY(writeData(client, send.mid(2)));
        stream->readStall(false);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= send.size(); });
        }
        stream.reset();

        QCOMPARE(dataRead.toQByteArrayRef(), send);
        QVERIFY(waitForClosed(client));
        ::close(client);
    }

    void serverDestroy()
    {
        std::mutex mutex;
        std::condition_variable cv;
        std::unique_ptr<Socket::Unix::Connection> stream;

        std::unique_ptr<Socket::Unix::Server>
                server(new Socket::Unix::Server([&](std::unique_ptr<Socket::Connection> &&conn) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                Q_ASSERT(!stream);
                Q_ASSERT(dynamic_cast<Socket::Unix::Connection *>(conn.get()));
                stream = std::unique_ptr<Socket::Unix::Connection>(
                        dynamic_cast<Socket::Unix::Connection *>(conn.release()));
                cv.notify_all();
            }
        }, socketPath));

        ::unlink(socketPath.c_str());
        QVERIFY(server->startListening(false));
        QVERIFY(server->isListening());

        int client = ::socket(AF_UNIX, SOCK_STREAM, 0);
        QVERIFY(client != -1);
        {
            struct ::sockaddr_un addr;
            ::memset(&addr, 0, sizeof(addr));
            addr.sun_family = AF_UNIX;
            ::strncpy(addr.sun_path, socketPath.c_str(), sizeof(addr.sun_path));
            addr.sun_path[sizeof(addr.sun_path) - 1] = 0;
            QVERIFY(::connect(client, (const struct ::sockaddr *) &addr, sizeof(addr)) != -1);
        }

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return stream.get() != nullptr; });
        }
        QVERIFY(stream.get() != nullptr);
        QCOMPARE(stream->socketPath(), socketPath);

        server.reset();

        Util::ByteArray dataRead;
        bool didEnd = false;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            didEnd = true;
            cv.notify_all();
        });
        stream->start();

        QByteArray send = "Some data";
        QVERIFY(writeData(client, send));
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= send.size(); });
        }
        QCOMPARE(dataRead.toQByteArrayRef(), send);

        stream->write(send);
        stream.reset();
        QCOMPARE(readData(client, send.size()), send);

        QVERIFY(waitForClosed(client));
        ::close(client);
    }

    void both()
    {
        std::mutex mutex;
        std::condition_variable cv;
        std::unique_ptr<Socket::Unix::Connection> serverStream;

        Socket::Unix::Server server([&](std::unique_ptr<Socket::Connection> &&conn) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                Q_ASSERT(!serverStream);
                Q_ASSERT(dynamic_cast<Socket::Unix::Connection *>(conn.get()));
                serverStream = std::unique_ptr<Socket::Unix::Connection>(
                        dynamic_cast<Socket::Unix::Connection *>(conn.release()));
                cv.notify_all();
            }
        }, socketPath);

        QVERIFY(server.startListening());
        QVERIFY(server.isListening());

        auto clientStream = Socket::Unix::connect(socketPath);
        QVERIFY(clientStream.get() != nullptr);

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return serverStream.get() != nullptr; });
        }
        QVERIFY(serverStream.get() != nullptr);


        Util::ByteArray clientData;
        bool clientEnd = false;
        clientStream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            clientData += d;
            cv.notify_all();
        });
        clientStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            clientEnd = true;
            cv.notify_all();
        });
        clientStream->start();

        Util::ByteArray serverData;
        bool serverEnd = false;
        serverStream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            serverData += d;
            cv.notify_all();
        });
        serverStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            serverEnd = true;
            cv.notify_all();
        });
        serverStream->start();

        Util::ByteArray sendData("Socket data");
        serverStream->write(sendData);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return clientData.size() >= sendData.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), sendData.toQByteArrayRef());
            QVERIFY(!clientEnd);
            clientData.clear();
        }

        clientStream->write(sendData);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return serverData.size() >= sendData.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), sendData.toQByteArrayRef());
            QVERIFY(!serverEnd);
            serverData.clear();
        }

        serverStream->write(sendData);
        serverStream.reset();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return clientEnd; });
            QVERIFY(clientEnd);
            QCOMPARE(clientData.toQByteArrayRef(), sendData.toQByteArrayRef());
            clientData.clear();
        }
        clientStream.reset();

        clientEnd = false;
        serverEnd = false;

        clientStream = Socket::Unix::connect(socketPath);
        QVERIFY(clientStream.get() != nullptr);

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return serverStream.get() != nullptr; });
        }
        QVERIFY(serverStream.get() != nullptr);

        clientStream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            clientData += d;
            cv.notify_all();
        });
        clientStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            clientEnd = true;
            cv.notify_all();
        });
        clientStream->start();

        serverStream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            serverData += d;
            cv.notify_all();
        });
        serverStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            serverEnd = true;
            cv.notify_all();
        });
        serverStream->start();

        clientStream->write(sendData);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return serverData.size() >= sendData.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), sendData.toQByteArrayRef());
            QVERIFY(!serverEnd);
            serverData.clear();
        }

        serverStream->write(sendData);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return clientData.size() >= sendData.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), sendData.toQByteArrayRef());
            QVERIFY(!clientEnd);
            clientData.clear();
        }

        clientStream->write(sendData);
        clientStream.reset();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return serverEnd; });
            QVERIFY(serverEnd);
            QCOMPARE(serverData.toQByteArrayRef(), sendData.toQByteArrayRef());
            serverData.clear();
        }
    }
};

QTEST_APPLESS_MAIN(TestUnixSocket)

#include "unixsocket.moc"
