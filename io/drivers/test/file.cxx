/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <QTest>
#include <QEventLoop>
#include <QTimer>
#include <QTemporaryFile>

#include "io/drivers/file.hxx"
#include "core/waitutils.hxx"

using namespace CPD3;
using namespace CPD3::IO;


class TestIODriverFile : public QObject {
Q_OBJECT

    static bool startAndWait(Generic::Stream &stream)
    {
        std::mutex mutex;
        std::condition_variable cv;
        bool isEnded = false;

        stream.ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            isEnded = true;
            cv.notify_all();
        });

        stream.start();

        std::unique_lock<std::mutex> lock(mutex);
        cv.wait_for(lock, std::chrono::seconds(30), [&]() { return isEnded; });
        return isEnded;
    }

private slots:

    void buffered()
    {
        Util::ByteArray data("AbCDEF1234567890");
        QTemporaryFile file;
        QVERIFY(file.open());
        file.write(data.toQByteArrayRef());
        file.close();

        File::Mode mode;
        mode.create = false;
        mode.bufferedMode(true);
        auto backing = Access::file(file.fileName().toStdString(), mode);
        QVERIFY(backing.get() != nullptr);

        QCOMPARE(QString::fromStdString(backing->filename()), file.fileName());
        QCOMPARE((int) backing->size(), (int) data.size());

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(stream->isEnded());

            Util::ByteArray contents("Asdf");
            stream->write(contents);
            stream->write(Util::ByteView(contents));
            stream->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                stream->write(qba);
            }
            stream->write(std::move(contents));
            stream->write("Asdfg");
            stream->write(std::string("QWERTY"));
            stream->write(QString("ZXCV"));

            QCOMPARE(dataRead.toQByteArrayRef(), QByteArray("AbCDEF1234567890"));
        }

        Util::ByteArray finalContents("AbCDEF1234567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCV");
        {
            QFile check(file.fileName());
            QVERIFY(check.open(QIODevice::ReadOnly));
            QCOMPARE(check.readAll(), finalContents.toQByteArrayRef());
        }

        {
            auto block = backing->block();
            QVERIFY(block.get() != nullptr);

            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), finalContents.mid(0, 10).toQByteArrayRef());
            }
            {
                QByteArray target;
                block->read(target, 10);
                QCOMPARE(target, finalContents.mid(10, 10).toQByteArrayRef());
            }
            QVERIFY(!block->readFinished());
            block->seek(static_cast<std::size_t>(-1));

            Util::ByteArray contents("Asdf");
            block->write(contents);
            block->write(Util::ByteView(contents));
            block->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                block->write(qba);
            }
            block->write(std::move(contents));
            block->write("Asdfg");
            block->write(std::string("QWERTY"));
            block->write(QString("ZXCV"));

            block->seek(block->tell() - 2);
            block->write("123");
            block->move(-3);
            QVERIFY(!block->readFinished());
            {
                QByteArray target;
                block->read(target, 3);
                QCOMPARE(target, QByteArray("123"));
            }

            block->seek(0);
            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), QByteArray("AbCDEF1234"));
            }
            QVERIFY(!block->readFinished());
            QCOMPARE((int) block->tell(), 10);
            {
                Util::ByteArray target;
                block->read(target);
                QCOMPARE(target.toQByteArrayRef(), QByteArray(
                        "567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCVAsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123"));
            }
            QVERIFY(block->readFinished());
        }

        finalContents += "AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123";
        {
            QFile check(file.fileName());
            QVERIFY(check.open(QIODevice::ReadOnly));
            QCOMPARE(check.readAll(), finalContents.toQByteArrayRef());
        }

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QCOMPARE(dataRead.toQByteArrayRef(), finalContents.toQByteArrayRef());
            QVERIFY(stream->isEnded());
        }

        backing->resize(0);
        QCOMPARE((int) backing->size(), 0);

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QVERIFY(dataRead.empty());
            QVERIFY(stream->isEnded());
        }

    }

    void unbuffered()
    {
        Util::ByteArray data("AbCDEF1234567890");
        QTemporaryFile file;
        QVERIFY(file.open());
        file.write(data.toQByteArrayRef());
        file.close();

        File::Mode mode;
        mode.create = false;
        mode.bufferedMode(false);
        auto backing = Access::file(file.fileName().toStdString(), mode);
        QVERIFY(backing.get() != nullptr);

        QCOMPARE(QString::fromStdString(backing->filename()), file.fileName());
        QCOMPARE((int) backing->size(), (int) data.size());

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(stream->isEnded());

            Util::ByteArray contents("Asdf");
            stream->write(contents);
            stream->write(Util::ByteView(contents));
            stream->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                stream->write(qba);
            }
            stream->write(std::move(contents));
            stream->write("Asdfg");
            stream->write(std::string("QWERTY"));
            stream->write(QString("ZXCV"));

            QCOMPARE(dataRead.toQByteArrayRef(), QByteArray("AbCDEF1234567890"));
        }

        Util::ByteArray finalContents("AbCDEF1234567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCV");
        {
            QFile check(file.fileName());
            QVERIFY(check.open(QIODevice::ReadOnly));
            QCOMPARE(check.readAll(), finalContents.toQByteArrayRef());
        }

        {
            auto block = backing->block();
            QVERIFY(block.get() != nullptr);

            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), finalContents.mid(0, 10).toQByteArrayRef());
            }
            {
                QByteArray target;
                block->read(target, 10);
                QCOMPARE(target, finalContents.mid(10, 10).toQByteArrayRef());
            }
            QVERIFY(!block->readFinished());
            block->seek(static_cast<std::size_t>(-1));

            Util::ByteArray contents("Asdf");
            block->write(contents);
            block->write(Util::ByteView(contents));
            block->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                block->write(qba);
            }
            block->write(std::move(contents));
            block->write("Asdfg");
            block->write(std::string("QWERTY"));
            block->write(QString("ZXCV"));

            block->seek(block->tell() - 2);
            block->write("123");
            block->move(-3);
            QVERIFY(!block->readFinished());
            {
                QByteArray target;
                block->read(target, 3);
                QCOMPARE(target, QByteArray("123"));
            }

            block->seek(0);
            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), QByteArray("AbCDEF1234"));
            }
            QVERIFY(!block->readFinished());
            QCOMPARE((int) block->tell(), 10);
            {
                Util::ByteArray target;
                block->read(target);
                QCOMPARE(target.toQByteArrayRef(), QByteArray(
                        "567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCVAsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123"));
            }
            QVERIFY(block->readFinished());
        }

        finalContents += "AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123";
        {
            QFile check(file.fileName());
            QVERIFY(check.open(QIODevice::ReadOnly));
            QCOMPARE(check.readAll(), finalContents.toQByteArrayRef());
        }

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QCOMPARE(dataRead.toQByteArrayRef(), finalContents.toQByteArrayRef());
            QVERIFY(stream->isEnded());
        }

        backing->resize(0);
        QCOMPARE((int) backing->size(), 0);

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QVERIFY(dataRead.empty());
            QVERIFY(stream->isEnded());
        }

    }

    void temporaryFile()
    {
        Util::ByteArray data("AbCDEF1234567890");
        auto backing = Access::temporaryFile(true);
        QVERIFY(backing.get() != nullptr);

        QVERIFY(!backing->filename().empty());
        QCOMPARE((int) backing->size(), 0);

        {
            QFile file(QString::fromStdString(backing->filename()));
            QVERIFY(file.exists());
            QVERIFY(file.open(QIODevice::WriteOnly));
            file.write(data.toQByteArrayRef());
        }

        QCOMPARE((int) backing->size(), (int) data.size());

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(stream->isEnded());

            Util::ByteArray contents("Asdf");
            stream->write(contents);
            stream->write(Util::ByteView(contents));
            stream->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                stream->write(qba);
            }
            stream->write(std::move(contents));
            stream->write("Asdfg");
            stream->write(std::string("QWERTY"));
            stream->write(QString("ZXCV"));

            QCOMPARE(dataRead.toQByteArrayRef(), QByteArray("AbCDEF1234567890"));
        }

        Util::ByteArray finalContents("AbCDEF1234567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCV");
        {
            QFile check(QString::fromStdString(backing->filename()));
            QVERIFY(check.open(QIODevice::ReadOnly));
            QCOMPARE(check.readAll(), finalContents.toQByteArrayRef());
        }

        {
            auto block = backing->block();
            QVERIFY(block.get() != nullptr);

            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), finalContents.mid(0, 10).toQByteArrayRef());
            }
            {
                QByteArray target;
                block->read(target, 10);
                QCOMPARE(target, finalContents.mid(10, 10).toQByteArrayRef());
            }
            QVERIFY(!block->readFinished());
            block->seek(static_cast<std::size_t>(-1));

            Util::ByteArray contents("Asdf");
            block->write(contents);
            block->write(Util::ByteView(contents));
            block->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                block->write(qba);
            }
            block->write(std::move(contents));
            block->write("Asdfg");
            block->write(std::string("QWERTY"));
            block->write(QString("ZXCV"));

            block->seek(block->tell() - 2);
            block->write("123");
            block->move(-3);
            QVERIFY(!block->readFinished());
            {
                QByteArray target;
                block->read(target, 3);
                QCOMPARE(target, QByteArray("123"));
            }

            block->seek(0);
            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), QByteArray("AbCDEF1234"));
            }
            QVERIFY(!block->readFinished());
            QCOMPARE((int) block->tell(), 10);
            {
                Util::ByteArray target;
                block->read(target);
                QCOMPARE(target.toQByteArrayRef(), QByteArray(
                        "567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCVAsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123"));
            }
            QVERIFY(block->readFinished());
        }

        finalContents += "AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123";
        {
            QFile check(QString::fromStdString(backing->filename()));
            QVERIFY(check.open(QIODevice::ReadOnly));
            QCOMPARE(check.readAll(), finalContents.toQByteArrayRef());
        }

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QCOMPARE(dataRead.toQByteArrayRef(), finalContents.toQByteArrayRef());
            QVERIFY(stream->isEnded());
        }

        backing->resize(0);
        QCOMPARE((int) backing->size(), 0);

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QVERIFY(dataRead.empty());
            QVERIFY(stream->isEnded());
        }

        QString name = QString::fromStdString(backing->filename());
        QVERIFY(QFile::exists(name));
        backing.reset();
        QVERIFY(!QFile::exists(name));
    }
};

QTEST_MAIN(TestIODriverFile)

#include "file.moc"
