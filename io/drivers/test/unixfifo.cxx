/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <mutex>
#include <condition_variable>
#include <QTest>
#include <QEventLoop>
#include <QTimer>
#include <QTemporaryFile>
#include <QBuffer>

#include "io/drivers/unixfifo.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::IO;


class TestIODriverUnixFIFO : public QObject {
Q_OBJECT

    std::string fifoPath;

private slots:

    void initTestCase()
    {
        CPD3::Logging::suppressForTesting();

        {
            QTemporaryFile file;
            file.setAutoRemove(false);
            QVERIFY(file.open());
            fifoPath = file.fileName().toStdString();
        }
        ::unlink(fifoPath.c_str());
    }

    void cleanupTestCase()
    {
        ::unlink(fifoPath.c_str());
    }

    void basic()
    {
        QByteArray data("AbCDEF1234567890");

        auto inputHandle = Access::unixFIFORead(fifoPath, true);
        auto outputHandle = Access::unixFIFOWrite(fifoPath, false);

        auto inputStream = inputHandle->stream();
        QVERIFY(inputStream.get() != nullptr);
        auto outputStream = outputHandle->stream();
        QVERIFY(outputStream.get() != nullptr);

        std::mutex mutex;
        std::condition_variable cv;
        Util::ByteArray dataRead;
        bool inputEnd = false;
        inputStream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        inputStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            inputEnd = true;
            cv.notify_all();
        });
        inputStream->start();

        bool outputEnd = false;
        outputStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            outputEnd = true;
            cv.notify_all();
        });
        outputStream->start();

        outputStream->write(data);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= data.size(); });
            QCOMPARE(dataRead.toQByteArrayRef(), data);
            dataRead.clear();
        }

        inputStream.reset();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return outputEnd; });
        }
        QVERIFY(outputEnd);
        outputStream.reset();

        outputStream = outputHandle->stream();
        QVERIFY(outputStream.get() == nullptr);

        ::unlink(fifoPath.c_str());

        outputStream = outputHandle->stream();
        QVERIFY(outputStream.get() == nullptr);

        ::mkfifo(fifoPath.c_str(), 0600);

        outputStream = outputHandle->stream();
        QVERIFY(outputStream.get() == nullptr);

        inputStream = inputHandle->stream();
        QVERIFY(inputStream.get() != nullptr);
        dataRead.clear();
        inputEnd = false;
        inputStream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        inputStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            inputEnd = true;
            cv.notify_all();
        });
        inputStream->start();

        outputStream = outputHandle->stream();
        QVERIFY(outputStream.get() != nullptr);
        outputEnd = false;
        outputStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            outputEnd = true;
            cv.notify_all();
        });
        outputStream->start();

        outputStream->write(data);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= data.size(); });
            QCOMPARE(dataRead.toQByteArrayRef(), data);
            dataRead.clear();
        }

        outputStream->write(data);
        outputStream.reset();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return inputEnd; });
        }
        QVERIFY(inputEnd);
        QCOMPARE(dataRead.toQByteArrayRef(), data);
    }


};

QTEST_APPLESS_MAIN(TestIODriverUnixFIFO)

#include "unixfifo.moc"
