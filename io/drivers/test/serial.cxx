/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <QTest>
#include <QEventLoop>
#include <QTimer>

#include "io/drivers/serial.hxx"
#include "core/waitutils.hxx"

using namespace CPD3;
using namespace CPD3::IO;

//#define NULL_MODEM_PORT_A   "/dev/ttyUSB0"
//#define NULL_MODEM_PORT_B   "/dev/ttyUSB2"


class TestIODriverSerial : public QObject {
Q_OBJECT


private slots:

    void enumerate()
    {
        auto ports = Serial::enumeratePorts();
#ifdef NULL_MODEM_PORT_A
        QVERIFY(ports.count(NULL_MODEM_PORT_A) != 0);
#endif
#ifdef NULL_MODEM_PORT_B
        QVERIFY(ports.count(NULL_MODEM_PORT_B) != 0);
#endif
    }

#if defined(NULL_MODEM_PORT_A)

    void portSetup()
    {
        auto handle = Access::serial(NULL_MODEM_PORT_A);
        QVERIFY(handle.get() != nullptr);
        QCOMPARE(handle->portName(), std::string(NULL_MODEM_PORT_A));

        auto stream = handle->stream();
        QVERIFY(stream.get() != nullptr);

        auto serial = dynamic_cast<Serial::Stream *>(stream.get());
        QVERIFY(serial != nullptr);
        QCOMPARE(serial->portName(), std::string(NULL_MODEM_PORT_A));

        QVERIFY(!serial->describePort().empty());
        serial->describeError();

        serial->pulseDTR();
        serial->setExplicitRTSOnTransmit();
        serial->pulseRTS();

        QVERIFY(serial->setBaud(9600));
        QVERIFY(serial->setParity(Serial::Stream::Parity::None));
        QVERIFY(serial->setDataBits(8));
        QVERIFY(serial->setStopBits(1));
        QVERIFY(serial->setRS232StandardFlowControl());
        QVERIFY(serial->setNoHardwareFlowControl());
        QVERIFY(serial->setSoftwareFlowControl(Serial::Stream::SoftwareFlowControl::Disable));
        QVERIFY(serial->flushPort());
    }

#endif

#if defined(NULL_MODEM_PORT_A) && defined(NULL_MODEM_PORT_B)

    void nullModemLoopback()
    {
        auto hA = Access::serial(NULL_MODEM_PORT_A);
        QVERIFY(hA.get() != nullptr);
        auto sA = hA->stream();
        QVERIFY(sA.get() != nullptr);
        auto serialA = dynamic_cast<Serial::Stream *>(sA.get());
        QVERIFY(serialA != nullptr);

        auto hB = Access::serial(NULL_MODEM_PORT_B);
        QVERIFY(hB.get() != nullptr);
        auto sB = hB->stream();
        QVERIFY(sB.get() != nullptr);
        auto serialB = dynamic_cast<Serial::Stream *>(sB.get());
        QVERIFY(serialB != nullptr);


        QVERIFY(serialA->setBaud(9600));
        QVERIFY(serialA->setParity(Serial::Stream::Parity::None));
        QVERIFY(serialA->setDataBits(8));
        QVERIFY(serialA->setStopBits(1));
        QVERIFY(serialA->setNoHardwareFlowControl());
        QVERIFY(serialA->setSoftwareFlowControl(Serial::Stream::SoftwareFlowControl::Disable));
        QVERIFY(serialA->flushPort(false));

        QVERIFY(serialB->setBaud(9600));
        QVERIFY(serialB->setParity(Serial::Stream::Parity::None));
        QVERIFY(serialB->setDataBits(8));
        QVERIFY(serialB->setStopBits(1));
        QVERIFY(serialB->setNoHardwareFlowControl());
        QVERIFY(serialB->setSoftwareFlowControl(Serial::Stream::SoftwareFlowControl::Disable));
        QVERIFY(serialB->flushPort(false));

        std::mutex mutex;
        std::condition_variable cv;

        Util::ByteArray dataReadA;
        bool endA = false;
        serialA->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataReadA += d;
            cv.notify_all();
        });
        serialA->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            endA = true;
            cv.notify_all();
        });
        serialA->start();

        Util::ByteArray dataReadB;
        bool endB = false;
        serialB->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataReadB += d;
            cv.notify_all();
        });
        serialB->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            endB = true;
            cv.notify_all();
        });
        serialB->start();

        QTest::qSleep(500);

        QByteArray data("AbCDEF1234567890");

        serialA->write(data);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataReadB.size() >= data.size(); });
            QCOMPARE(dataReadB.toQByteArrayRef(), data);
            QVERIFY(dataReadA.empty());
            dataReadB.clear();
            QVERIFY(!endA);
            QVERIFY(!endB);
        }

        serialB->write(data);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataReadA.size() >= data.size(); });
            QCOMPARE(dataReadA.toQByteArrayRef(), data);
            QVERIFY(dataReadB.empty());
            dataReadA.clear();
            QVERIFY(!endA);
            QVERIFY(!endB);
        }

        QVERIFY(serialA->setBaud(19200));
        QVERIFY(serialB->setBaud(19200));

        bool okA = false;
        std::thread flushA([&] {
            okA = serialA->flushPort();
        });
        bool okB = false;
        std::thread flushB([&] {
            okB = serialB->flushPort();
        });
        flushA.join();
        flushB.join();
        QVERIFY(okA);
        QVERIFY(okB);

        {
            std::lock_guard<std::mutex> lock(mutex);
            dataReadA.clear();
            dataReadB.clear();
        }

        serialA->write(data);
        serialB->write(data);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() {
                return (int) dataReadA.size() >= data.size() &&
                        (int) dataReadB.size() >= data.size();
            });
            QCOMPARE(dataReadA.toQByteArrayRef(), data);
            QCOMPARE(dataReadB.toQByteArrayRef(), data);
            dataReadA.clear();
            dataReadB.clear();
            QVERIFY(!endA);
            QVERIFY(!endB);
        }
    }

#endif
};

QTEST_MAIN(TestIODriverSerial)

#include "serial.moc"
