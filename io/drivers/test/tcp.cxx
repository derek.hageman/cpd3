/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QEventLoop>
#include <QTimer>
#include <QTcpServer>
#include <QTcpSocket>
#include <QHostAddress>

#include "io/drivers/tcp.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::IO;


static const char *cert1Data = "-----BEGIN CERTIFICATE-----\n"
                               "MIIFazCCA1OgAwIBAgIUcQWCjvR+2UjdwEiq/hfK5nRTy2gwDQYJKoZIhvcNAQEL\n"
                               "BQAwRTELMAkGA1UEBhMCVVMxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM\n"
                               "GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMjA4MjQyMjQ0NDNaFw0zMjA4\n"
                               "MjEyMjQ0NDNaMEUxCzAJBgNVBAYTAlVTMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw\n"
                               "HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggIiMA0GCSqGSIb3DQEB\n"
                               "AQUAA4ICDwAwggIKAoICAQCT0tQuQnLGh7D4IfHWvd2551Hz151BWvLa28zH7eYb\n"
                               "+OrjenNA+UF4Oeg2jOTdveHYqE3Kx1cOYcAaFoybXzTy3OCSsPqXyYIzc1MsSSCx\n"
                               "6fCg/+m2HadtmEjMQdjdfwOphpqj616ZguRCkUu9UeP8Y+VStFp9F1E2mw/G7JTu\n"
                               "ssFrN71g/G+HtMcAyiEcEfzX19Tnz4+hpY8D6O24vntegrn3GtCZXXmIL/h0UZ35\n"
                               "hTXnSNOi7gSRz6mKoKTPNvFxPA2fw3gksFYKYNLeVaM7ylfe+Hb4SajSC6jJp1Ku\n"
                               "EMrcPvE77j2QSQEFgX3it9peJ2GJ+YpOCE2Kgkc7JqyFfmlb/P4q3M74dYooxlvd\n"
                               "tkQZbAGCaI3QX+cND1F3S3YdJi87ksl7J33DnIxJwDCayKLwonb3K92zNYWi3YVK\n"
                               "70mF4kRCEXmZa5nr2mu5NcMG78jMBLmTgkkK+D2d7sW3g92yvQO/zatucTqo81Lv\n"
                               "gsq6pJ46UTHS6FJriKHprARvl3ncFtcUtVlWmCTci3MXsGH9Qm0OlnQDtStHwIpA\n"
                               "BJW/M6fARXenJr8ezVVj+nrVNMj1SGXu+Lx7MyluCbwwTDhQBEKsDC+WJln1our4\n"
                               "eng7IX7m/fEg3/JlpfcujcJYsW8dx/PzDjA212TUt8bZkoZI8y5+Oug3u8Pp763u\n"
                               "WQIDAQABo1MwUTAdBgNVHQ4EFgQURNp3BrFtSLhpTeXbIoq+YebXumYwHwYDVR0j\n"
                               "BBgwFoAURNp3BrFtSLhpTeXbIoq+YebXumYwDwYDVR0TAQH/BAUwAwEB/zANBgkq\n"
                               "hkiG9w0BAQsFAAOCAgEAhuwOAltN+WL8X+sJpI+Y3IDBHucaeBOD1+be37TjSMWo\n"
                               "DMqFcCdZSKiRK0Ww03Wecjxx83A+HU6H/rERmu4NxNiEXFtLYU8l16KFdoqp0egT\n"
                               "FWKifL/vikMYM/nMhpeR+9fylA+/AqfoNZkeAH13S7qliGVYRkz2Ci/yqMMr+Ph9\n"
                               "aorambR4SKh3O75rOrOumPStC9adWQfUNcN6tZGORZranvdZjYRo+197c8Tk9y/r\n"
                               "mZ/YbxJ46V2rIRtbt3Wq8iDUSTZqMRAcegEex7Vz9zv9A6GyqSkeki2CR+P39wgN\n"
                               "zXKHviHNnnhtNEwdDnsJKTP6cA8t7Qorxjx8eZic1gxDNF7FSZ4FYiJZL+9rWZrC\n"
                               "E7BkaOf/IS6B3bXCuIPO+no5/BSCuuTBxwpuVqr5iEjK6/Wmrtzn9g7sxo8LikfU\n"
                               "m/8OhSe4UcffMWALlw1iy5WO78JicpSa+n7wtcNSkIsCout4DjXn9AXY4YyumQAT\n"
                               "QerRcM0WH3TvPv1C87yEWQPAPzCHtDKl+ITfXF5NbezzwevryABIKta16GVabeBi\n"
                               "MbixwC7IixqB306CVhFMkba5vNMlCqrWGPBcnMdwIcgjsn4dV4TnNU7wiHd3qxpS\n"
                               "Z5GdEPBJs+SiWzFHMAz4dsfQrune67PePBbXdQfddEPNg0hOPpGzNYYMZVjE+/0=\n"
                               "-----END CERTIFICATE-----";
static const char *key1Data = "-----BEGIN PRIVATE KEY-----\n"
                              "MIIJQQIBADANBgkqhkiG9w0BAQEFAASCCSswggknAgEAAoICAQCT0tQuQnLGh7D4\n"
                              "IfHWvd2551Hz151BWvLa28zH7eYb+OrjenNA+UF4Oeg2jOTdveHYqE3Kx1cOYcAa\n"
                              "FoybXzTy3OCSsPqXyYIzc1MsSSCx6fCg/+m2HadtmEjMQdjdfwOphpqj616ZguRC\n"
                              "kUu9UeP8Y+VStFp9F1E2mw/G7JTussFrN71g/G+HtMcAyiEcEfzX19Tnz4+hpY8D\n"
                              "6O24vntegrn3GtCZXXmIL/h0UZ35hTXnSNOi7gSRz6mKoKTPNvFxPA2fw3gksFYK\n"
                              "YNLeVaM7ylfe+Hb4SajSC6jJp1KuEMrcPvE77j2QSQEFgX3it9peJ2GJ+YpOCE2K\n"
                              "gkc7JqyFfmlb/P4q3M74dYooxlvdtkQZbAGCaI3QX+cND1F3S3YdJi87ksl7J33D\n"
                              "nIxJwDCayKLwonb3K92zNYWi3YVK70mF4kRCEXmZa5nr2mu5NcMG78jMBLmTgkkK\n"
                              "+D2d7sW3g92yvQO/zatucTqo81Lvgsq6pJ46UTHS6FJriKHprARvl3ncFtcUtVlW\n"
                              "mCTci3MXsGH9Qm0OlnQDtStHwIpABJW/M6fARXenJr8ezVVj+nrVNMj1SGXu+Lx7\n"
                              "MyluCbwwTDhQBEKsDC+WJln1our4eng7IX7m/fEg3/JlpfcujcJYsW8dx/PzDjA2\n"
                              "12TUt8bZkoZI8y5+Oug3u8Pp763uWQIDAQABAoICAAQPAfEetS7G/ziC2xRbrGys\n"
                              "IZuFsB1BIWGZrxTOrvaV5iIWhLdUGKRzx5DG28pMSjkD/vXKNRVRoP/+XkAuVCJ2\n"
                              "ZsqURh8YguUFfbM9s0J7QzZybFDqHcSULayBAtKrB/dVuSV2wwdJORnsShVxvAYc\n"
                              "GonjpofgdeP/TCa4rGqk2Qtn8YDKEYha8etPQWs3QbdN51wxflaweF2hvbQos7Ov\n"
                              "uGokz/UkBEAVXBeafZFCI22dGeVzbkfwGQ4ztK61ShoDLXcVW3U7a7NXfvaoiIWt\n"
                              "262FKiQNRq+0rv7RY6NgledH6aDs/L77SCZF7MZGqL3UzDho8++KShG7W7gPquUp\n"
                              "OOy1eLRg7ppyxBf2JWP0SEbxTN8yhIQwtdwgKKk3Q3N+/fXwBUVhi/Rv9dmkLUP2\n"
                              "r7IVZNAAmgTMNGgA6PNqUPNAKlruJTQ8SFflSNe5qulkB3OmQ6uHvJu7x/z9HvvE\n"
                              "kJ/3oZiw1a6VzMpVe4MZgwGDD2GUZDpPwvxNZi/5bHM9ltWqFH+6mYzAmMNvKASg\n"
                              "Gl36BXAPTELF/xd7Dfd47zN2CvDKBQ51BCdnUCiQ76qE6f9LrCfOh5rNT/ALNKT4\n"
                              "3btBFfoN2WHskzAXD7qAoQYDi5M9xAliNKpJnWu7AXJkpTJOv3CVYNMIvJ0ml6jc\n"
                              "WI9Yst7PIXhUN79aupfxAoIBAQDG0sMnZNp+uAAoqVylWJl7NApHSB1qhWxTZ+pL\n"
                              "tvmbPLI19Qu8RWZyoH2RgOSAqCUhBaMFiyF7FxZsE3DSXw9pqptk81HK6CAjnoVS\n"
                              "dKP3/GDSjtkk4kTsK5bVavlmoKuZUAmh2HJmYMrrUSA9Uyu5NsLl9Y0Rpk8H5kyf\n"
                              "Gm1auYufcw9FuK4tcvTZ1MZEAIKQ7SoV4i1uytEeDYT5h3vpLVMOvH4RM5Urw5S/\n"
                              "3ZTrdgJACbNrL+p98HTyRGxeeMqJEt+3JLeDmFOMll95Cng6+smzSmOxOQku5apx\n"
                              "7ytR3D6s1nTsigLAAxVmXSXPEtbDKaEHrl8zw7yDdHHsPC7RAoIBAQC+VX/vD14K\n"
                              "r4CLQh1HUpqwgrdDDfCTlUyPPLNTwUcew2t64IxIFxRJr5/dOkuL6Rz5GH1ivzY3\n"
                              "XJ3bkxDwLXW59dWcy+anivbI0MhXqmWtOe8XE+a3COZr1zYV84W7dJMMsi8pI2Vi\n"
                              "0/7hv52xWAPOZnSLDBrP2oSkkpibPujWU8bi8R70joUa7v4UPh7u+q23+PvhKyMQ\n"
                              "deqVAFl1Mb9WVkenBcfWoze5sx3cF0bkKXutxN1uh9gr2Uwjch/4Yh7kSMs4lEmZ\n"
                              "51aGU/gC0EprIM3O7+o88NZxgC+GF6YgrpOlpx/pKUPBe5aO1ShBT+BO8AER1GFl\n"
                              "v72v/Ez/V/kJAoIBAEwUnmTWrN1Mn5Lvq+oBi0mf0kcQi2EViSwpWXh7newPP8px\n"
                              "6Hm0vM0kKKii/81TilGmjIk0gi1N2mCk18lIYud2R1xL14KjbJj0seOpioz8YDhy\n"
                              "PRlmFCWjUGZ+Ns1UshVKkUUDRFN0unFta77LsrF/CPliCwcz8o39TFidjjbnRUxQ\n"
                              "hQmS7+OoV5V7XBrtbwjyF+aj0+rPZVHwrm+lrn69v0imTD9c07oZbzQ0ICYx1A6Z\n"
                              "J7TecwaaGsYR9L35ztbBCCZWwHp0sZPcftAcd4FqMgCPeLJ6Ns9hRuWuNY9vjfQp\n"
                              "ZDiXXxIGnAu9nRguB0xLA7miuf9e6SYMSwOwy+ECggEAWZVod55mBuV/vQvLOAyb\n"
                              "HkUH+JmRCAWXWTuas+sejE4yQk15+VxTgjMVLU0IzbtUlbF/IoEZBYmkCvr3V/Qt\n"
                              "mu8oMXqO/4Cakv4hrZFX9eZ0sAn/51pbCZrrq/1IjmhZ5fnf1J8CUzewmZRUpmnk\n"
                              "sLrsU53I6NfS4prVFQzRDj+0NpCCn1yNLZYbJG/wo059gT/BXcOt50t4s9TMRiq6\n"
                              "AeruIqDH5DBCRDcX8MVL6ovT2H/2MNXjWxAVlAFdJs0X+R6B+Aljcvq9cNAIxVpA\n"
                              "DJgOBj5Jo5E/fYB13ck3ud4xRCCbFmUDrQd8X5HYNpVf4Ad6mWe6x+ctYq1/mBdR\n"
                              "8QKCAQA6krd3nFC3qDt1SMV1ZkPO+KX7U0HyOlXR4SOm6lrwxvmRO9A8WP04slSQ\n"
                              "N+nLSAAVBAmVzW5WNEGeKI9DiygMZkK88/zNblP+ehNypWhcZ0EvFuUS5yaT/KhJ\n"
                              "7DQEp6jTIPHqm7N/4SF7lCV6gR9WlFJoKEvjT4o6bYVNLIpy2tAB6gphwRpEdnBj\n"
                              "VLLEQ/H9kUgPtNmCnMy/zPCucDgSiqyZOBcwBTvqj90np4cnU4NFDAOzqst6sLok\n"
                              "R+md2F0bM8BjYYczybdYvVq2/GQIi3I4ikTuDk6DANppQY89J/m0yaT+Fpmm8L9a\n"
                              "aPLk7jPofbX2GWYZdOSJ2GoNuTbj\n"
                              "-----END PRIVATE KEY-----";
static const char *cert2Data = "-----BEGIN CERTIFICATE-----\n"
                               "MIIFazCCA1OgAwIBAgIUfBlD9z2l9v0/Ko1Y1Et9UMUXfgowDQYJKoZIhvcNAQEL\n"
                               "BQAwRTELMAkGA1UEBhMCVVMxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM\n"
                               "GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMjA4MjQyMjQ1NDFaFw0zMjA4\n"
                               "MjEyMjQ1NDFaMEUxCzAJBgNVBAYTAlVTMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw\n"
                               "HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggIiMA0GCSqGSIb3DQEB\n"
                               "AQUAA4ICDwAwggIKAoICAQCiokRHZYO0kpbdPqJeK0LcDrG7pfQemeeShGAZj4Gm\n"
                               "5Qqfi0VavMY5Qz3rkXNhfi2d2TJ7vAn+luZ+XK1EZ9HYnauFrJIcgRBQkFLaulHL\n"
                               "R0E9TRVqknGYYUtkppX1IJ2J5ug07QXNz0LEnsGY6CL3Goefv+TMFl2O1+PJxJ8R\n"
                               "ooR13gmHVtzRASgK4qjUkvQ2wmk3EuJmFzYfpyM5kPkrO4zJuqf0zsZKA12CBcfg\n"
                               "EKTMmpO7Aekt6j9Sh2EqTPhuS37tJXDvlHot1bw2AaiICugMx8iXtuI/46Imwh5r\n"
                               "9sXeGU1HWiSkQm0Ci4xMEyGQtwEWk3Ca4hPS3SXiDv8tEYdnUYOCINAOqWsFgXUq\n"
                               "l5lgYBHwb560ZxmqFquFDFUr0uorfDMIJVymvzMuZTptxSmOqXdnkPfaj2cmoEOl\n"
                               "8P2OPUSy0GL2KFdXbenRM5cL3i218FG+sbRKAvE8SOdPlbLiFO00C/29xgXbS+lA\n"
                               "LJpNrqWI+wbE2BtW8pIbyFtix28RItUBJH+m1RyN6HlZ4eBAGJNYsC82Ht11XigP\n"
                               "6Bi3XIIYQoy5vic7ak/Elni2Kv+/RMF08T2EUDYgoEhnr5sfDB9EQh7EU+sK+rRr\n"
                               "32GujuAPSRzGsPOfB/6CPRbYvhPHF8MN0lcuFOQ6oJ0YpA6a1IJ7zjuyIOdliBoe\n"
                               "RwIDAQABo1MwUTAdBgNVHQ4EFgQUgKr7P2bigfa5EN8jnFFwVm9MP+IwHwYDVR0j\n"
                               "BBgwFoAUgKr7P2bigfa5EN8jnFFwVm9MP+IwDwYDVR0TAQH/BAUwAwEB/zANBgkq\n"
                               "hkiG9w0BAQsFAAOCAgEAYwEfXElpKlmKSJhoHV2BiA41yJ8Qqe+W49pvUCJLouko\n"
                               "ThPxZ9hLfKtGASQnriaWWR+wee3morVTL5gxg25YZcIKAb9ivzPaEBq6NVOfnXmu\n"
                               "3fZeastWpUAf3IfFj+S0u7r39aBMLBN0AR9UA+JTRQ/rSiZfZbaKFwBrEv56kbAn\n"
                               "8PstFifkIR3RcTOsLY7lgxvAv2EwZIllSRX3ulC+ur51I4S2DDjqoOVtsdJuDuuk\n"
                               "ytLyBFAwFR+EPaSNpcHvSCoHcNG+3jRpssgK662hnatJgn41y7dOWaHVodPeOiQ0\n"
                               "Fvfv1WXlUdr5JAS7lmiuyTA7ft6lMXasS1dmkcvT89yu4awlW9u8QVOnCmHug5ml\n"
                               "UklCeHaWnJEydnxvat0gRNB07nvJj1KGoa+ufVQgu3NhO/WTnUR4T8mGwT7Ljx1y\n"
                               "2nElBItfIaarexDG7JvKAN043Ex8bMvAVIbXHEAJOZvp+SEVJbmtuTx+SWoM1iCi\n"
                               "LbZhBoSYgwg/C67CyikrdaHcXIfH4wLU+aOTlvKYDZ1PpRMHAI9QYz8n4uy/x8+0\n"
                               "lG8s2vrKogXMTE5Dg/hCtpNeqwhgsseh0WF6QfuWOEVJ10qFZgCPptybh7yBoaw9\n"
                               "PiOmBGUjXsxCkknO4Q7zJqjkplDkfZfb1I9Mt0P0N1U0zJyQeML+hb4fAVhDfxI=\n"
                               "-----END CERTIFICATE-----";
static const char *key2Data = "-----BEGIN PRIVATE KEY-----\n"
                              "MIIJQgIBADANBgkqhkiG9w0BAQEFAASCCSwwggkoAgEAAoICAQCiokRHZYO0kpbd\n"
                              "PqJeK0LcDrG7pfQemeeShGAZj4Gm5Qqfi0VavMY5Qz3rkXNhfi2d2TJ7vAn+luZ+\n"
                              "XK1EZ9HYnauFrJIcgRBQkFLaulHLR0E9TRVqknGYYUtkppX1IJ2J5ug07QXNz0LE\n"
                              "nsGY6CL3Goefv+TMFl2O1+PJxJ8RooR13gmHVtzRASgK4qjUkvQ2wmk3EuJmFzYf\n"
                              "pyM5kPkrO4zJuqf0zsZKA12CBcfgEKTMmpO7Aekt6j9Sh2EqTPhuS37tJXDvlHot\n"
                              "1bw2AaiICugMx8iXtuI/46Imwh5r9sXeGU1HWiSkQm0Ci4xMEyGQtwEWk3Ca4hPS\n"
                              "3SXiDv8tEYdnUYOCINAOqWsFgXUql5lgYBHwb560ZxmqFquFDFUr0uorfDMIJVym\n"
                              "vzMuZTptxSmOqXdnkPfaj2cmoEOl8P2OPUSy0GL2KFdXbenRM5cL3i218FG+sbRK\n"
                              "AvE8SOdPlbLiFO00C/29xgXbS+lALJpNrqWI+wbE2BtW8pIbyFtix28RItUBJH+m\n"
                              "1RyN6HlZ4eBAGJNYsC82Ht11XigP6Bi3XIIYQoy5vic7ak/Elni2Kv+/RMF08T2E\n"
                              "UDYgoEhnr5sfDB9EQh7EU+sK+rRr32GujuAPSRzGsPOfB/6CPRbYvhPHF8MN0lcu\n"
                              "FOQ6oJ0YpA6a1IJ7zjuyIOdliBoeRwIDAQABAoICADPXSrGBg+6a7Zkfvo0K+DiC\n"
                              "PJhmqX7Zq03ygVmUe40SJIU/1T87vmoBa6r8Bc31dSAEXInBomPzgQyViSutdmA4\n"
                              "vjSRkk+gum0b3DVZv/nuwDaErEd439nlZa1zRojJOT58itdYGIoGv69CNc8CbCbd\n"
                              "X48GEa4WkQMYAUXPNa9e4R9bRClOgHvlBPkXUB7Wqx8LcJN8IwvM6VVEpz9R3YMw\n"
                              "68tgAurPwLhWA0gPhKuBUq7ftSiezs/yg/XQLqJXv7cUvKRmU24jI6EZqPGELM8U\n"
                              "vuxYALKPuuYHryfwp9bdZixKX25Xpydu/yWZCwMy8/eXxjKGlBXi69PC57D7+83F\n"
                              "MzAC49icsDogN4zwbABzVhVcvtgqDyyndB3fWdRn4gXU6/3+zDnSCAaEDoxGjPIn\n"
                              "2TitYZSZWwWMsa8yy/HvMrRVEWX1PCrNv2KTwtcL12s9yauSg4StH6my6Di5x8tT\n"
                              "hGl0GfE5nGUSRF9Pld9FZydHIRJ9TsPQ6JzMlZr2lvL46rkk9Iza2plWXAp5Pw8a\n"
                              "gD+ail1GZ45D68Nl3f8tl6CjMAqw+9Oba81gSBO/fmoAz0ba2c7Xa7HgN6863yC1\n"
                              "cE2YKAtDK7fE33YEAhyWQ3TGOpXbmMmaYfoRH4T7D/8qYiB6LDxiz20v9wVuT3mb\n"
                              "4rGzSK09KcxqeAfZd5zRAoIBAQDWWWCVii6aWuGmBvssZwFOO6J+HmMXtpRJtmsA\n"
                              "u0V5h8mz1lVNd1O0OXAqpUVTDO0auVVPFc/+JLlFye5x5hXlNsnPU6BJeBkDp3Vt\n"
                              "RRKHNR0YTB2xxLAS0IKyUKYoTUf1yJ5B5FlMb+wNgrkdc0FQrzYf+k9YGsDejz0B\n"
                              "KQZ3L28p3o19RMamaRKEWDQ0cNWYDnbMKBjbzRvDFcFE2yiFoa+8sCu3ZCIZBJ0i\n"
                              "ARWy0KjEdgpeoCAEJvHy7XN+ufdA6tRhL5u13CaZaD9ky0Q30y0m0Dylwf08aBCL\n"
                              "DuS/UNsMKVOXx4VaiWglX5itMg53/lEqJ+FWyGyPb0TuJm13AoIBAQDCPFru4V5z\n"
                              "F3Z5B9YnSTRUa1EwPGSqijrVecc53ze9hAnJUtM86jolcbVAs/fsDsv3HZFmQ8op\n"
                              "DkVTNNmrY5OlpAHk4Tu37WGzLbgQ9hRXatJ+VDresKdbgUL/W3U33DhxORdwWGMz\n"
                              "NyXemwwGrlSmB9g92mQJu6bxjuxlr/GQA2Bq5ww5xYNI4VmizJylN6L8HcY/8DFz\n"
                              "ssJZDyxZcnzJ8wwfuQyR8O6P8tKVRYKpkZijW+wadFJ3YNqnbaFHySFqVQIoucIu\n"
                              "cOOueWwJ4nTrfP5X/FyewFe1RAEBAacBfWSupX3Lyqfhql7N2+EAbWMu+GcmVTrE\n"
                              "nZjudMJ2msmxAoIBAQCwCWEwDvctH1xYXT1k/wdsd7+AsnYRYIoya5U/WY26PJGj\n"
                              "l3AhGdHtnoqHC4p+pYwIuZLdS32xK/nY3fltI1hyEef11Kk8DV0Fyj6/Sc3oqu9F\n"
                              "KCXu/CjamtHrnh6H1Mg3i9alKONJylVj1ysui5xlpi/eXVVSd05UufCRBl9f/N3f\n"
                              "1f3/lj9LJdnmRQZC1zQkGCTqkDeyNKYLNs+uPArwjJRmc4zDpYH3Z/mglqF6ZLFl\n"
                              "/8eOZRVGM7HbH7YNqq7saq+XTWElHzO4I4yEvAc/jbh0OurD+yh9tr13ZvjoQoBT\n"
                              "YZFcgqj7bQPSRct+2jBVzdBcvbnr6th1iB1lNLwLAoIBAFuxVlQSvB2k49663lPp\n"
                              "cPSxPMCCohsH6kJAnNYrFAs5O814dhP5lr16clA7JTygt5TOtocKVXMQM5XWCzOn\n"
                              "bFnzlJlR4nkvbMHDQXNwV6X03a2ZYbkit76wxMn7iNh384UvqGr2rs3MqOnvU7wL\n"
                              "rBY8+c4pPLSDi4nZKKvQZT7Q+UE+FT1oilNrXn9GDGTKYPyXqbEhWJb/ulOEoDER\n"
                              "pOtI+142Y0K59ESsItEo4UffXakwicLrSsrkRBRp4osa3dVuj/hdyNdXn4QL+f6o\n"
                              "kv1gIkmy7p5auoztr3OkLgy7/z2bDkmYP6x+WwbfV9Z1zS6tQTSNY5LRuxJGuE55\n"
                              "P+ECggEAJVnMoTv7bxlqAXGKAJbufzmNr8ahI9/AEkBpMJc2ZxN0GwHl5nv/DZWN\n"
                              "6s6n2fwy6pygWlwOWGn+LL0gsJWUc5UNsQjukZyg7n/eAfl2evc2yiYR4HJub5aZ\n"
                              "JIFcnuh+Bzwy7zJrtAR/XrgyTIN0l+5UcfFhknMBZSFOv3KETFdiLfX4Zz0/I/83\n"
                              "rIIF0IHlisbDrp8gBNlCJGI2/Z+TJjxJc1KlSX75xaRvttx6uFvDg9moTfqJF8hS\n"
                              "xQWv6cansKSsUAtOYGOG4G+T94Q8XXlsbCZ2Gv8uTiEE9/NAQ2DkVGSKvJNK2q5t\n"
                              "q/WkfvYkbGOrhauIOsr+yxmWYkf86g==\n"
                              "-----END PRIVATE KEY-----";

class TestTCP : public QObject {
Q_OBJECT

    static QTcpSocket *acceptConnection(QTcpServer &server)
    {
        ElapsedTimer et;
        et.start();
        while (et.elapsed() < 10000) {
            QEventLoop el;
            QTimer::singleShot(500, &el, &QEventLoop::quit);
            QObject::connect(&server, &QTcpServer::newConnection, &el, &QEventLoop::quit);
            if (server.hasPendingConnections())
                return server.nextPendingConnection();
            el.exec();
        }
        return nullptr;
    }

    static QByteArray readData(QIODevice *device, int expected)
    {
        QByteArray result;
        ElapsedTimer et;
        et.start();
        while (et.elapsed() < 10000 && result.size() < expected) {
            QEventLoop el;
            QTimer::singleShot(500, &el, &QEventLoop::quit);
            QObject::connect(device, &QIODevice::readyRead, &el, &QEventLoop::quit);
            if (device->bytesAvailable() > 0) {
                result += device->read(expected - result.size());
                continue;
            }
            el.exec();
        }
        return result;
    }

    static void flushWrite(QTcpSocket *socket)
    {
        socket->flush();
        ElapsedTimer et;
        et.start();
        while (et.elapsed() < 10000) {
            QEventLoop el;
            QTimer::singleShot(500, &el, &QEventLoop::quit);
            QObject::connect(socket, &QAbstractSocket::stateChanged, &el, &QEventLoop::quit);
            QObject::connect(socket, &QIODevice::bytesWritten, &el, &QEventLoop::quit);
            if (socket->bytesToWrite() == 0)
                return;
            el.exec();
        }
    }

    static bool waitForClosed(QTcpSocket *socket)
    {
        ElapsedTimer et;
        et.start();
        while (et.elapsed() < 10000) {
            QEventLoop el;
            QTimer::singleShot(500, &el, &QEventLoop::quit);
            QObject::connect(socket, &QAbstractSocket::stateChanged, &el, &QEventLoop::quit);
            if (socket->state() == QAbstractSocket::UnconnectedState)
                return true;
            socket->read(1);
            el.exec();
        }
        return false;
    }

    QHostAddress localhost;

private slots:

    void initTestCase()
    {
        CPD3::Logging::suppressForTesting();

#ifndef QT_NO_IPV4
        localhost = QHostAddress(QHostAddress::LocalHost);
#else
        localhost = QHostAddress(QHostAddress::LocalHostIPv6);
#endif
    }

    void client()
    {
        QTcpServer server;
        QVERIFY(server.listen(localhost));
        auto port = server.serverPort();
        QVERIFY(port > 0);

        auto stream = Socket::TCP::loopback(port);
        QVERIFY(stream.get() != nullptr);

        auto socket = acceptConnection(server);
        QVERIFY(socket != nullptr);
        QVERIFY(!server.hasPendingConnections());
        socket->open(QIODevice::ReadWrite | QIODevice::Unbuffered);

        QCOMPARE((int) stream->peerPort(), (int) port);

        std::mutex mutex;
        std::condition_variable cv;
        Util::ByteArray dataRead;
        bool didEnd = false;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            didEnd = true;
            cv.notify_all();
        });
        stream->start();

        QByteArray send = "Some data";
        stream->write(send);
        QCOMPARE(readData(socket, send.size()), send);

        QCOMPARE((int) socket->write(send.mid(0, 2)), 2);
        flushWrite(socket);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return dataRead.size() >= 2; });
        }
        stream->readStall(true);
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), send.mid(0, 2));
        }

        QCOMPARE((int) socket->write(send.mid(2)), send.size() - 2);
        stream->readStall(false);
        flushWrite(socket);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= send.size(); });
        }
        stream.reset();

        QCOMPARE(dataRead.toQByteArrayRef(), send);
        QVERIFY(waitForClosed(socket));

        socket->deleteLater();

        {
            std::thread thd([&] {
                stream = Socket::TCP::connect("localhost", port, true);
            });
            socket = acceptConnection(server);
            QVERIFY(socket != nullptr);
            thd.join();
        }
        QVERIFY(!server.hasPendingConnections());
        QVERIFY(stream.get() != nullptr);
        socket->open(QIODevice::ReadWrite | QIODevice::Unbuffered);

        dataRead.clear();
        didEnd = false;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            didEnd = true;
            cv.notify_all();
        });
        stream->start();

        QCOMPARE((int) socket->write(send), send.size());
        flushWrite(socket);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= send.size(); });
        }
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), send);
            dataRead.clear();
        }

        stream->write(Util::ByteArray(send));
        QCOMPARE(readData(socket, send.size()), send);

        QCOMPARE((int) socket->write(send), send.size());

        socket->disconnectFromHost();
        QVERIFY(waitForClosed(socket));

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return didEnd; });
        }

        QVERIFY(didEnd);
        QCOMPARE(dataRead.toQByteArrayRef(), send);
        QVERIFY(stream->isEnded());

        stream.reset();


        {
            std::thread thd([&] {
                stream = Socket::TCP::connect(localhost, port, localhost, true);
            });
            socket = acceptConnection(server);
            QVERIFY(socket != nullptr);
            thd.join();
        }
        QVERIFY(!server.hasPendingConnections());
        QVERIFY(stream.get() != nullptr);
        socket->open(QIODevice::ReadWrite | QIODevice::Unbuffered);

        dataRead.clear();
        didEnd = false;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            didEnd = true;
            cv.notify_all();
        });
        stream->start();

        QCOMPARE((int) socket->write(send), send.size());
        flushWrite(socket);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= send.size(); });
        }
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), send);
            dataRead.clear();
        }

        stream->write(Util::ByteArray(send));
        QCOMPARE(readData(socket, send.size()), send);

        QCOMPARE((int) socket->write(send), send.size());

        socket->disconnectFromHost();
        QVERIFY(waitForClosed(socket));

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return didEnd; });
        }

        QVERIFY(didEnd);
        QCOMPARE(dataRead.toQByteArrayRef(), send);
        QVERIFY(stream->isEnded());

        stream.reset();
    }

    void serverBasic()
    {
        std::mutex mutex;
        std::condition_variable cv;
        std::unique_ptr<Socket::TCP::Connection> stream;

        Socket::TCP::Server server([&](std::unique_ptr<Socket::Connection> &&conn) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                Q_ASSERT(!stream);
                Q_ASSERT(dynamic_cast<Socket::TCP::Connection *>(conn.get()));
                stream = std::unique_ptr<Socket::TCP::Connection>(
                        dynamic_cast<Socket::TCP::Connection *>(conn.release()));
                cv.notify_all();
            }
        }, Socket::TCP::Server::AddressFamily::Loopback);

        QVERIFY(server.startListening());
        QVERIFY(server.isListening());
        QVERIFY(server.localPort() != 0);
        QVERIFY(!server.localAddress().empty());

        std::unique_ptr<QTcpSocket> socket(new QTcpSocket);
        socket->connectToHost(localhost, server.localPort(),
                              QIODevice::ReadWrite | QIODevice::Unbuffered);
        QVERIFY(socket->waitForConnected(10000));
        QCOMPARE(socket->state(), QAbstractSocket::ConnectedState);

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return stream.get() != nullptr; });
        }
        QVERIFY(stream.get() != nullptr);
        QCOMPARE(stream->peerPort(), socket->localPort());

        Util::ByteArray dataRead;
        bool didEnd = false;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            didEnd = true;
            cv.notify_all();
        });
        stream->start();

        QByteArray send = "Some data";
        stream->write(send);
        QCOMPARE(readData(socket.get(), send.size()), send);

        QCOMPARE((int) socket->write(send.mid(0, 2)), 2);
        flushWrite(socket.get());
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return dataRead.size() >= 2; });
        }
        stream->readStall(true);
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), send.mid(0, 2));
        }

        QCOMPARE((int) socket->write(send.mid(2)), send.size() - 2);
        stream->readStall(false);
        flushWrite(socket.get());
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= send.size(); });
        }
        stream.reset();

        QCOMPARE(dataRead.toQByteArrayRef(), send);
        QVERIFY(waitForClosed(socket.get()));
        socket.reset();
    }

    void serverDestroy()
    {
        std::mutex mutex;
        std::condition_variable cv;
        std::unique_ptr<Socket::TCP::Connection> stream;

        std::unique_ptr<Socket::TCP::Server>
                server(new Socket::TCP::Server([&](std::unique_ptr<Socket::Connection> &&conn) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                Q_ASSERT(!stream);
                Q_ASSERT(dynamic_cast<Socket::TCP::Connection *>(conn.get()));
                stream = std::unique_ptr<Socket::TCP::Connection>(
                        dynamic_cast<Socket::TCP::Connection *>(conn.release()));
                cv.notify_all();
            }
        }, "localhost"));

        QVERIFY(server->startListening());
        QVERIFY(server->isListening());
        QVERIFY(server->localPort() != 0);

        std::unique_ptr<QTcpSocket> socket(new QTcpSocket);
        socket->connectToHost("localhost", server->localPort(),
                              QIODevice::ReadWrite | QIODevice::Unbuffered);
        QVERIFY(socket->waitForConnected(10000));
        QCOMPARE(socket->state(), QAbstractSocket::ConnectedState);

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return stream.get() != nullptr; });
        }
        QVERIFY(stream.get() != nullptr);
        QCOMPARE(stream->peerPort(), socket->localPort());

        server.reset();

        Util::ByteArray dataRead;
        bool didEnd = false;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            didEnd = true;
            cv.notify_all();
        });
        stream->start();

        QByteArray send = "Some data";
        QCOMPARE((int) socket->write(send), send.size());
        flushWrite(socket.get());
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= send.size(); });
        }
        QCOMPARE(dataRead.toQByteArrayRef(), send);

        stream->write(send);
        stream.reset();
        QCOMPARE(readData(socket.get(), send.size()), send);

        QVERIFY(waitForClosed(socket.get()));
    }

    void ssl()
    {
        std::mutex mutex;
        std::condition_variable cv;
        std::unique_ptr<Socket::TCP::Connection> serverStream;

        Socket::TCP::Configuration serverConfig;
        serverConfig.tls = [](QSslSocket &socket) {
            socket.setPeerVerifyMode(QSslSocket::QueryPeer);
            socket.ignoreSslErrors(QList<QSslError>() << QSslError::SelfSignedCertificate
                                                      << QSslError::SelfSignedCertificateInChain
                                                      << QSslError::HostNameMismatch);
            socket.setPrivateKey(QSslKey(key1Data, QSsl::Rsa));
            socket.setLocalCertificate(QSslCertificate(cert1Data));
            return true;
        };
        Socket::TCP::Configuration clientConfig;
        clientConfig.tls = [](QSslSocket &socket) {
            socket.setPeerVerifyMode(QSslSocket::QueryPeer);
            socket.ignoreSslErrors(QList<QSslError>() << QSslError::SelfSignedCertificate
                                                      << QSslError::SelfSignedCertificateInChain
                                                      << QSslError::HostNameMismatch);
            socket.setPrivateKey(QSslKey(key2Data, QSsl::Rsa));
            socket.setLocalCertificate(QSslCertificate(cert2Data));
            return true;
        };

        Socket::TCP::Server server([&](std::unique_ptr<Socket::Connection> &&conn) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                Q_ASSERT(!serverStream);
                Q_ASSERT(dynamic_cast<Socket::TCP::Connection *>(conn.get()));
                serverStream = std::unique_ptr<Socket::TCP::Connection>(
                        dynamic_cast<Socket::TCP::Connection *>(conn.release()));
                cv.notify_all();
            }
        }, Socket::TCP::Server::AddressFamily::Loopback, serverConfig);

        QVERIFY(server.startListening());
        QVERIFY(server.isListening());
        QVERIFY(server.localPort() != 0);
        clientConfig.port = server.localPort();

        auto clientStream = Socket::TCP::loopback(clientConfig, true);
        QVERIFY(clientStream.get() != nullptr);

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return serverStream.get() != nullptr; });
        }
        QVERIFY(serverStream.get() != nullptr);

        QCOMPARE(serverStream->peerCertificate().digest(QCryptographicHash::Sha512),
                 QSslCertificate(cert2Data).digest(QCryptographicHash::Sha512));
        QCOMPARE(clientStream->peerCertificate().digest(QCryptographicHash::Sha512),
                 QSslCertificate(cert1Data).digest(QCryptographicHash::Sha512));


        Util::ByteArray clientData;
        bool clientEnd = false;
        clientStream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            clientData += d;
            cv.notify_all();
        });
        clientStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            clientEnd = true;
            cv.notify_all();
        });
        clientStream->start();

        Util::ByteArray serverData;
        bool serverEnd = false;
        serverStream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            serverData += d;
            cv.notify_all();
        });
        serverStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            serverEnd = true;
            cv.notify_all();
        });
        serverStream->start();

        Util::ByteArray sendData("Socket data");
        serverStream->write(sendData);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return clientData.size() >= sendData.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), sendData.toQByteArrayRef());
            QVERIFY(!clientEnd);
            clientData.clear();
        }

        clientStream->write(sendData);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return serverData.size() >= sendData.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), sendData.toQByteArrayRef());
            QVERIFY(!serverEnd);
            serverData.clear();
        }

        serverStream->write(sendData);
        serverStream.reset();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return clientEnd; });
            QVERIFY(clientEnd);
            QCOMPARE(clientData.toQByteArrayRef(), sendData.toQByteArrayRef());
            clientData.clear();
        }
        clientStream.reset();

        clientEnd = false;
        serverEnd = false;

        clientStream = Socket::TCP::loopback(clientConfig, true);
        QVERIFY(clientStream.get() != nullptr);

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return serverStream.get() != nullptr; });
        }
        QVERIFY(serverStream.get() != nullptr);

        QCOMPARE(serverStream->peerCertificate().digest(QCryptographicHash::Sha512),
                 QSslCertificate(cert2Data).digest(QCryptographicHash::Sha512));
        QCOMPARE(clientStream->peerCertificate().digest(QCryptographicHash::Sha512),
                 QSslCertificate(cert1Data).digest(QCryptographicHash::Sha512));

        clientStream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            clientData += d;
            cv.notify_all();
        });
        clientStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            clientEnd = true;
            cv.notify_all();
        });
        clientStream->start();

        serverStream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            serverData += d;
            cv.notify_all();
        });
        serverStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            serverEnd = true;
            cv.notify_all();
        });
        serverStream->start();

        clientStream->write(sendData);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return serverData.size() >= sendData.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), sendData.toQByteArrayRef());
            QVERIFY(!serverEnd);
            serverData.clear();
        }

        serverStream->write(sendData);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return clientData.size() >= sendData.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), sendData.toQByteArrayRef());
            QVERIFY(!clientEnd);
            clientData.clear();
        }

        clientStream->write(sendData);
        clientStream.reset();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return serverEnd; });
            QVERIFY(serverEnd);
            QCOMPARE(serverData.toQByteArrayRef(), sendData.toQByteArrayRef());
            serverData.clear();
        }
    }
};

QTEST_MAIN(TestTCP)

#include "tcp.moc"
