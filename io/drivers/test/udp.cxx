/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QEventLoop>
#include <QTimer>
#include <QUdpSocket>
#include <QHostAddress>

#include "io/drivers/udp.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::IO;


class TestUDP : public QObject {
Q_OBJECT

    QByteArray readPacket(QUdpSocket &socket)
    {
        ElapsedTimer et;
        et.start();
        while (et.elapsed() < 10000) {
            QEventLoop el;
            QTimer::singleShot(500, &el, &QEventLoop::quit);
            QObject::connect(&socket, &QIODevice::readyRead, &el, &QEventLoop::quit);
            if (socket.hasPendingDatagrams()) {
                QByteArray result;
                result.resize(65536);
                auto n = socket.readDatagram(result.data(), result.size());
                if (n > 0) {
                    result.resize(n);
                    return result;
                }
            }
            el.exec();
        }
        return {};
    }

    QHostAddress localhost;

private slots:

    void initTestCase()
    {
        CPD3::Logging::suppressForTesting();

#ifndef QT_NO_IPV4
        localhost = QHostAddress(QHostAddress::LocalHost);
#else
        localhost = QHostAddress(QHostAddress::LocalHostIPv6);
#endif
    }

    void basic()
    {
        QUdpSocket socket;
        QVERIFY(socket.bind(localhost));
        auto socketPort = socket.localPort();
        QVERIFY(socketPort > 0);

        auto stream = UDP::Configuration().loopback(socketPort)
                                          .listen(UDP::Configuration::AddressFamily::Loopback)();
        QVERIFY(stream.get() != nullptr);

        auto streamPort = stream->localPort();
        QVERIFY((int) streamPort > 0);

        std::mutex mutex;
        std::condition_variable cv;
        Util::ByteArray dataRead;
        std::vector<UDP::Packet> packetRead;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->received.connect([&](const UDP::Packet &p) {
            std::lock_guard<std::mutex> lock(mutex);
            packetRead.emplace_back(p);
            cv.notify_all();
        });

        QByteArray send = "Some data";
        stream->write(send);
        QCOMPARE(readPacket(socket), send);

        QCOMPARE(socket.writeDatagram(send, localhost, streamPort), (int) send.size());
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() {
                return (int) dataRead.size() >= send.size() && !packetRead.empty();
            });
        }
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), send);
            dataRead.clear();
            QCOMPARE((int) packetRead.size(), 1);
            QCOMPARE(packetRead.front().data.toQByteArrayRef(), send);
            QCOMPARE((int) packetRead.front().port, (int) socketPort);
            QVERIFY(packetRead.front().address.isLoopback());
            packetRead.clear();
        }

        QCOMPARE(socket.writeDatagram(send, localhost, streamPort), (int) send.size());
        QCOMPARE(socket.writeDatagram(send, localhost, streamPort), (int) send.size());

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() {
                return (int) dataRead.size() >= send.size() * 2 && packetRead.size() >= 2;
            });
        }
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), (send + send));
            dataRead.clear();
            QCOMPARE((int) packetRead.size(), 2);
            QCOMPARE(packetRead.front().data.toQByteArrayRef(), send);
            QCOMPARE((int) packetRead.front().port, (int) socketPort);
            QVERIFY(packetRead.front().address.isLoopback());
            QCOMPARE(packetRead.back().data.toQByteArrayRef(), send);
            QCOMPARE((int) packetRead.back().port, (int) socketPort);
            QVERIFY(packetRead.back().address.isLoopback());
            packetRead.clear();
        }

        stream->sendTo(send, localhost, socketPort);
        stream->sendTo(send, localhost, socketPort);
        QCOMPARE(readPacket(socket), send);
        QCOMPARE(readPacket(socket), send);
    }

    void resolve()
    {
        QUdpSocket socket;
        QVERIFY(socket.bind(localhost));
        auto socketPort = socket.localPort();
        QVERIFY(socketPort > 0);

        std::uint16_t streamPort = 0;
        {
            QUdpSocket temp;
            temp.bind(localhost);
            streamPort = temp.localPort();
        }
        QVERIFY(streamPort > 0);

        UDP::Configuration config;
        config.connect("localhost", socketPort);
        config.listen("localhost", streamPort);
        config.requireAllRemote = true;
        config.requireAllLocal = true;
        config.waitForResolution = true;
        auto stream = config();
        QVERIFY(stream.get() != nullptr);
        QCOMPARE(stream->localPort(), streamPort);

        std::mutex mutex;
        std::condition_variable cv;
        Util::ByteArray dataRead;
        std::vector<UDP::Packet> packetRead;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->received.connect([&](const UDP::Packet &p) {
            std::lock_guard<std::mutex> lock(mutex);
            packetRead.emplace_back(p);
            cv.notify_all();
        });

        QByteArray send = "Some data";
        stream->write(send);
        QCOMPARE(readPacket(socket), send);

        QCOMPARE(socket.writeDatagram(send, localhost, streamPort), (int) send.size());
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() {
                return (int) dataRead.size() >= send.size() && !packetRead.empty();
            });
        }
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), send);
            dataRead.clear();
            QCOMPARE((int) packetRead.size(), 1);
            QCOMPARE(packetRead.front().data.toQByteArrayRef(), send);
            QCOMPARE((int) packetRead.front().port, (int) socketPort);
            QVERIFY(packetRead.front().address.isLoopback());
            packetRead.clear();
        }

        QCOMPARE(socket.writeDatagram(send, localhost, streamPort), (int) send.size());
        QCOMPARE(socket.writeDatagram(send, localhost, streamPort), (int) send.size());

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() {
                return (int) dataRead.size() >= send.size() * 2 && packetRead.size() >= 2;
            });
        }
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), (send + send));
            dataRead.clear();
            QCOMPARE((int) packetRead.size(), 2);
            QCOMPARE(packetRead.front().data.toQByteArrayRef(), send);
            QCOMPARE((int) packetRead.front().port, (int) socketPort);
            QVERIFY(packetRead.front().address.isLoopback());
            QCOMPARE(packetRead.back().data.toQByteArrayRef(), send);
            QCOMPARE((int) packetRead.back().port, (int) socketPort);
            QVERIFY(packetRead.back().address.isLoopback());
            packetRead.clear();
        }

        stream->sendTo(send, "localhost", socketPort);
        stream->sendTo(send, "localhost", socketPort);
        QCOMPARE(readPacket(socket), send);
        QCOMPARE(readPacket(socket), send);

        stream.reset();
        stream = config();
        QVERIFY(stream.get() != nullptr);
        QCOMPARE(stream->localPort(), streamPort);

        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->received.connect([&](const UDP::Packet &p) {
            std::lock_guard<std::mutex> lock(mutex);
            packetRead.emplace_back(p);
            cv.notify_all();
        });

        stream->write(send);
        QCOMPARE(readPacket(socket), send);

        QCOMPARE(socket.writeDatagram(send, localhost, streamPort), (int) send.size());
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() {
                return (int) dataRead.size() >= send.size() && !packetRead.empty();
            });
        }
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), send);
            dataRead.clear();
            QCOMPARE((int) packetRead.size(), 1);
            QCOMPARE(packetRead.front().data.toQByteArrayRef(), send);
            QCOMPARE((int) packetRead.front().port, (int) socketPort);
            QVERIFY(packetRead.front().address.isLoopback());
            packetRead.clear();
        }

#if !defined(QT_NO_IPV4) && !defined(QT_NO_IPV6)
        socket.close();
        QVERIFY(socket.bind(QHostAddress(QHostAddress::LocalHostIPv6), socketPort));

        stream->write(send);
        QCOMPARE(readPacket(socket), send);
#endif
    }
};

QTEST_MAIN(TestUDP)

#include "udp.moc"
