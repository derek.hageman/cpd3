/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QEventLoop>
#include <QTimer>
#include <QLocalSocket>
#include <QLocalServer>

#include "io/drivers/localsocket.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::IO;

class TestLocalSocket : public QObject {
Q_OBJECT

    static QLocalSocket *acceptConnection(QLocalServer &server)
    {
        ElapsedTimer et;
        et.start();
        while (et.elapsed() < 10000) {
            QEventLoop el;
            QTimer::singleShot(500, &el, &QEventLoop::quit);
            QObject::connect(&server, &QLocalServer::newConnection, &el, &QEventLoop::quit);
            if (server.hasPendingConnections())
                return server.nextPendingConnection();
            el.exec();
        }
        return nullptr;
    }

    static QByteArray readData(QIODevice *device, int expected)
    {
        QByteArray result;
        ElapsedTimer et;
        et.start();
        while (et.elapsed() < 10000 && result.size() < expected) {
            QEventLoop el;
            QTimer::singleShot(500, &el, &QEventLoop::quit);
            QObject::connect(device, &QIODevice::readyRead, &el, &QEventLoop::quit);
            if (device->bytesAvailable() > 0) {
                result += device->read(expected - result.size());
                continue;
            }
            el.exec();
        }
        return result;
    }

    static void flushWrite(QLocalSocket *socket)
    {
        socket->flush();
        ElapsedTimer et;
        et.start();
        while (et.elapsed() < 10000) {
            QEventLoop el;
            QTimer::singleShot(500, &el, &QEventLoop::quit);
            QObject::connect(socket, &QLocalSocket::stateChanged, &el, &QEventLoop::quit);
            QObject::connect(socket, &QIODevice::bytesWritten, &el, &QEventLoop::quit);
            if (socket->bytesToWrite() == 0)
                return;
            el.exec();
        }
    }

    static bool waitForClosed(QLocalSocket *socket)
    {
        ElapsedTimer et;
        et.start();
        while (et.elapsed() < 10000) {
            QEventLoop el;
            QTimer::singleShot(500, &el, &QEventLoop::quit);
            QObject::connect(socket, &QLocalSocket::stateChanged, &el, &QEventLoop::quit);
            if (socket->state() == QLocalSocket::UnconnectedState)
                return true;
            socket->read(1);
            el.exec();
        }
        return false;
    }

    QString serverName;

private slots:

    void initTestCase()
    {
        CPD3::Logging::suppressForTesting();

        serverName = "CPD3IOTestLocalSocket-" + Random::string();
    }

    void cleanupTestCase()
    {
        QLocalServer::removeServer(serverName);
    }

    void client()
    {
        QLocalServer server;
        QVERIFY(server.listen(serverName));

        auto stream = Socket::Local::connect(serverName);
        QVERIFY(stream.get() != nullptr);

        auto socket = acceptConnection(server);
        QVERIFY(socket != nullptr);
        QVERIFY(!server.hasPendingConnections());
        socket->open(QIODevice::ReadWrite | QIODevice::Unbuffered);
        QCOMPARE(QString::fromStdString(stream->serverName()), serverName);

        std::mutex mutex;
        std::condition_variable cv;
        Util::ByteArray dataRead;
        bool didEnd = false;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            didEnd = true;
            cv.notify_all();
        });
        stream->start();

        QByteArray send = "Some data";
        stream->write(send);
        QCOMPARE(readData(socket, send.size()), send);

        QCOMPARE((int) socket->write(send.mid(0, 2)), 2);
        flushWrite(socket);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return dataRead.size() >= 2; });
        }
        stream->readStall(true);
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), send.mid(0, 2));
        }

        QCOMPARE((int) socket->write(send.mid(2)), send.size() - 2);
        stream->readStall(false);
        flushWrite(socket);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= send.size(); });
        }
        stream.reset();

        QCOMPARE(dataRead.toQByteArrayRef(), send);
        QVERIFY(waitForClosed(socket));

        socket->deleteLater();

        std::thread thd([&] {
            stream = Socket::Local::connect(serverName.toStdString(), {}, true);
        });
        socket = acceptConnection(server);
        QVERIFY(socket != nullptr);
        thd.join();
        QVERIFY(!server.hasPendingConnections());
        QVERIFY(stream.get() != nullptr);
        socket->open(QIODevice::ReadWrite | QIODevice::Unbuffered);

        dataRead.clear();
        didEnd = false;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            didEnd = true;
            cv.notify_all();
        });
        stream->start();

        QCOMPARE((int) socket->write(send), send.size());
        flushWrite(socket);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= send.size(); });
        }
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), send);
            dataRead.clear();
        }

        stream->write(Util::ByteArray(send));
        QCOMPARE(readData(socket, send.size()), send);

        QCOMPARE((int) socket->write(send), send.size());

        socket->disconnectFromServer();
        QVERIFY(waitForClosed(socket));

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return didEnd; });
        }

        QVERIFY(didEnd);
        QCOMPARE(dataRead.toQByteArrayRef(), send);
        QVERIFY(stream->isEnded());

        stream.reset();
    }

    void serverBasic()
    {
        std::mutex mutex;
        std::condition_variable cv;
        std::unique_ptr<Socket::Local::Connection> stream;

        Socket::Local::Server server([&](std::unique_ptr<Socket::Connection> &&conn) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                Q_ASSERT(!stream);
                Q_ASSERT(dynamic_cast<Socket::Local::Connection *>(conn.get()));
                stream = std::unique_ptr<Socket::Local::Connection>(
                        dynamic_cast<Socket::Local::Connection *>(conn.release()));
                cv.notify_all();
            }
        }, serverName);

        QVERIFY(server.startListening());
        QVERIFY(server.isListening());

        std::unique_ptr<QLocalSocket> socket(new QLocalSocket);
        socket->connectToServer(serverName, QIODevice::ReadWrite | QIODevice::Unbuffered);
        QVERIFY(socket->waitForConnected(10000));
        QCOMPARE(socket->state(), QLocalSocket::ConnectedState);

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return stream.get() != nullptr; });
        }
        QVERIFY(stream.get() != nullptr);
        QCOMPARE(QString::fromStdString(stream->serverName()), serverName);

        Util::ByteArray dataRead;
        bool didEnd = false;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            didEnd = true;
            cv.notify_all();
        });
        stream->start();

        QByteArray send = "Some data";
        stream->write(send);
        QCOMPARE(readData(socket.get(), send.size()), send);

        QCOMPARE((int) socket->write(send.mid(0, 2)), 2);
        flushWrite(socket.get());
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return dataRead.size() >= 2; });
        }
        stream->readStall(true);
        {
            std::lock_guard<std::mutex> lock(mutex);
            QCOMPARE(dataRead.toQByteArrayRef(), send.mid(0, 2));
        }

        QCOMPARE((int) socket->write(send.mid(2)), send.size() - 2);
        stream->readStall(false);
        flushWrite(socket.get());
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= send.size(); });
        }
        stream.reset();

        QCOMPARE(dataRead.toQByteArrayRef(), send);
        QVERIFY(waitForClosed(socket.get()));
        socket.reset();
    }

    void serverDestroy()
    {
        std::mutex mutex;
        std::condition_variable cv;
        std::unique_ptr<Socket::Local::Connection> stream;

        std::unique_ptr<Socket::Local::Server>
                server(new Socket::Local::Server([&](std::unique_ptr<Socket::Connection> &&conn) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                Q_ASSERT(!stream);
                Q_ASSERT(dynamic_cast<Socket::Local::Connection *>(conn.get()));
                stream = std::unique_ptr<Socket::Local::Connection>(
                        dynamic_cast<Socket::Local::Connection *>(conn.release()));
                cv.notify_all();
            }
        }, serverName.toStdString()));

        QVERIFY(server->startListening());
        QVERIFY(server->isListening());

        std::unique_ptr<QLocalSocket> socket(new QLocalSocket);
        socket->connectToServer(serverName, QIODevice::ReadWrite | QIODevice::Unbuffered);
        QVERIFY(socket->waitForConnected(10000));
        QCOMPARE(socket->state(), QLocalSocket::ConnectedState);

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return stream.get() != nullptr; });
        }
        QVERIFY(stream.get() != nullptr);
        QCOMPARE(QString::fromStdString(stream->serverName()), serverName);

        server.reset();

        Util::ByteArray dataRead;
        bool didEnd = false;
        stream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            dataRead += d;
            cv.notify_all();
        });
        stream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            didEnd = true;
            cv.notify_all();
        });
        stream->start();

        QByteArray send = "Some data";
        QCOMPARE((int) socket->write(send), send.size());
        flushWrite(socket.get());
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return (int) dataRead.size() >= send.size(); });
        }
        QCOMPARE(dataRead.toQByteArrayRef(), send);

        stream->write(send);
        stream.reset();
        QCOMPARE(readData(socket.get(), send.size()), send);

        QVERIFY(waitForClosed(socket.get()));
    }

    void both()
    {
        std::mutex mutex;
        std::condition_variable cv;
        std::unique_ptr<Socket::Local::Connection> serverStream;

        Socket::Local::Server server([&](std::unique_ptr<Socket::Connection> &&conn) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                Q_ASSERT(!serverStream);
                Q_ASSERT(dynamic_cast<Socket::Local::Connection *>(conn.get()));
                serverStream = std::unique_ptr<Socket::Local::Connection>(
                        dynamic_cast<Socket::Local::Connection *>(conn.release()));
                cv.notify_all();
            }
        }, serverName);

        QVERIFY(server.startListening());
        QVERIFY(server.isListening());

        auto clientStream = Socket::Local::connect(serverName);
        QVERIFY(clientStream.get() != nullptr);

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return serverStream.get() != nullptr; });
        }
        QVERIFY(serverStream.get() != nullptr);


        Util::ByteArray clientData;
        bool clientEnd = false;
        clientStream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            clientData += d;
            cv.notify_all();
        });
        clientStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            clientEnd = true;
            cv.notify_all();
        });
        clientStream->start();

        Util::ByteArray serverData;
        bool serverEnd = false;
        serverStream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            serverData += d;
            cv.notify_all();
        });
        serverStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            serverEnd = true;
            cv.notify_all();
        });
        serverStream->start();

        Util::ByteArray sendData("Socket data");
        serverStream->write(sendData);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return clientData.size() >= sendData.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), sendData.toQByteArrayRef());
            QVERIFY(!clientEnd);
            clientData.clear();
        }

        clientStream->write(sendData);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return serverData.size() >= sendData.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), sendData.toQByteArrayRef());
            QVERIFY(!serverEnd);
            serverData.clear();
        }

        serverStream->write(sendData);
        serverStream.reset();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return clientEnd; });
            QVERIFY(clientEnd);
            QCOMPARE(clientData.toQByteArrayRef(), sendData.toQByteArrayRef());
            clientData.clear();
        }
        clientStream.reset();

        clientEnd = false;
        serverEnd = false;

        clientStream = Socket::Local::connect(serverName, {}, true);
        QVERIFY(clientStream.get() != nullptr);

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return serverStream.get() != nullptr; });
        }
        QVERIFY(serverStream.get() != nullptr);

        clientStream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            clientData += d;
            cv.notify_all();
        });
        clientStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            clientEnd = true;
            cv.notify_all();
        });
        clientStream->start();

        serverStream->read.connect([&](const Util::ByteArray &d) {
            std::lock_guard<std::mutex> lock(mutex);
            serverData += d;
            cv.notify_all();
        });
        serverStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            serverEnd = true;
            cv.notify_all();
        });
        serverStream->start();

        clientStream->write(sendData);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return serverData.size() >= sendData.size(); });
            QCOMPARE(serverData.toQByteArrayRef(), sendData.toQByteArrayRef());
            QVERIFY(!serverEnd);
            serverData.clear();
        }

        serverStream->write(sendData);
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10),
                        [&]() { return clientData.size() >= sendData.size(); });
            QCOMPARE(clientData.toQByteArrayRef(), sendData.toQByteArrayRef());
            QVERIFY(!clientEnd);
            clientData.clear();
        }

        clientStream->write(sendData);
        clientStream.reset();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return serverEnd; });
            QVERIFY(serverEnd);
            QCOMPARE(serverData.toQByteArrayRef(), sendData.toQByteArrayRef());
            serverData.clear();
        }
    }

    void asyncConnectionFailure()
    {
        std::mutex mutex;
        std::condition_variable cv;

        auto clientStream = Socket::Local::connect(serverName, {}, false);

        bool clientEnd = false;
        clientStream->ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            clientEnd = true;
            cv.notify_all();
        });
        clientStream->start();

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(10), [&]() { return clientEnd; });
            QVERIFY(clientEnd);
        }

        QVERIFY(clientStream->isEnded());
    }
};

QTEST_MAIN(TestLocalSocket)

#include "localsocket.moc"
