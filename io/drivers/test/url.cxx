/*
 * Copyright (c) 2020 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QTemporaryFile>

#include "io/drivers/url.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::IO;

class TestURL : public QObject {
Q_OBJECT

    static bool startAndWait(Generic::Stream &stream)
    {
        std::mutex mutex;
        std::condition_variable cv;
        bool isEnded = false;

        stream.ended.connect([&]() {
            std::lock_guard<std::mutex> lock(mutex);
            isEnded = true;
            cv.notify_all();
        });

        stream.start();

        std::unique_lock<std::mutex> lock(mutex);
        cv.wait_for(lock, std::chrono::seconds(30), [&]() { return isEnded; });
        return isEnded;
    }

private slots:

    void initTestCase()
    {
        //CPD3::Logging::suppressForTesting();
    }

    void get()
    {
        Util::ByteArray data("AbCDEF1234567890");
        QTemporaryFile file;
        QVERIFY(file.open());
        file.write(data.toQByteArrayRef());
        file.close();

        auto context = std::make_shared<URL::Context>();
        auto backing = Access::urlGET("file://" + file.fileName().toStdString(), context);
        QVERIFY(backing.get() != nullptr);

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(stream->isEnded());
        }
        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(stream->isEnded());
        }
    }

#if 1

    void getHTTP()
    {
        auto backing = Access::urlGET("http://noaa.gov");
        QVERIFY(backing.get() != nullptr);

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QVERIFY(dataRead.toQByteArrayRef().contains("<body"));
            QVERIFY(stream->isEnded());
        }
    }

    void putStream()
    {
        Util::ByteArray data("AbCDEF1234567890");
        QTemporaryFile file;
        QVERIFY(file.open());
        file.write(data.toQByteArrayRef());
        file.close();

        auto context = std::make_shared<URL::Context>();
        auto backing =
                Access::urlPUT("ftp://anonymous@awftp.cmdl.noaa.gov/incoming/aer/nil/cpd3test.urlstream",
                               std::unique_ptr<IO::Generic::Stream>(
                                       new IO::Generic::Stream::ByteView(data)), context);
        QVERIFY(backing.get() != nullptr);

        auto stream = backing->stream();
        QVERIFY(stream.get() != nullptr);

        Util::ByteArray dataRead;
        stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
        QVERIFY(startAndWait(*stream));
        QVERIFY(dataRead.empty());
        QVERIFY(stream->isEnded());

        stream.reset();
    }

    void putBacking()
    {
        Util::ByteArray data("AbCDEF12345678901");
        QTemporaryFile file;
        QVERIFY(file.open());
        file.write(data.toQByteArrayRef());
        file.close();

        auto context = std::make_shared<URL::Context>();
        auto backing =
                Access::urlPUT("ftp://anonymous@awftp.cmdl.noaa.gov/incoming/aer/nil/cpd3test.urlbacking",
                               IO::Access::buffer(data), context);
        QVERIFY(backing.get() != nullptr);

        auto stream = backing->stream();
        QVERIFY(stream.get() != nullptr);
        stream.reset();
    }

#endif
};

QTEST_MAIN(TestURL)

#include "url.moc"
