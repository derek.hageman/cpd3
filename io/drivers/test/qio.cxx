/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <QTest>
#include <QEventLoop>
#include <QTimer>
#include <QTemporaryFile>
#include <QBuffer>

#include "io/drivers/qio.hxx"
#include "core/waitutils.hxx"

using namespace CPD3;
using namespace CPD3::IO;


class TestIODriverQIO : public QObject {
Q_OBJECT

    static bool startAndWait(Generic::Stream &stream)
    {
        bool isEnded = false;
        QEventLoop el;
        stream.ended.connect(&el, [&]() {
            el.quit();
            isEnded = true;
        }, true);
        stream.start();
        el.exec();
        return isEnded;
    }

    static void waitForCondition(const std::function<bool()> &cond)
    {
        ElapsedTimer et;
        et.start();
        while (et.elapsed() < 10000) {
            QEventLoop el;
            QTimer::singleShot(100, &el, &QEventLoop::quit);
            if (cond())
                break;
            el.exec();
        }
    }

private slots:

    void shared()
    {
        QByteArray data("AbCDEF1234567890");
        auto qio = std::make_shared<QBuffer>(&data);
        QVERIFY(qio->open(QIODevice::ReadWrite));

        auto backing = Access::qio(qio);
        QVERIFY(backing.get() != nullptr);

        {
            auto block = backing->block();
            QVERIFY(block.get() != nullptr);

            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), data.mid(0, 10));
            }
            {
                QByteArray target;
                block->read(target, 2);
                QCOMPARE(target, data.mid(10, 2));
            }
            QVERIFY(!block->readFinished());
            block->seek(static_cast<std::size_t>(-1));

            Util::ByteArray contents("Asdf");
            block->write(contents);
            block->write(Util::ByteView(contents));
            block->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                block->write(qba);
            }
            block->write(std::move(contents));
            block->write("Asdfg");
            block->write(std::string("QWERTY"));
            block->write(QString("ZXCV"));

            block->seek(block->tell() - 2);
            block->write("123");
            block->move(-3);
            QVERIFY(!block->readFinished());
            {
                QByteArray target;
                block->read(target, 3);
                QCOMPARE(target, QByteArray("123"));
            }

            block->seek(0);
            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), QByteArray("AbCDEF1234"));
            }
            QVERIFY(!block->readFinished());
            QCOMPARE((int) block->tell(), 10);
            {
                Util::ByteArray target;
                block->read(target);
                QCOMPARE(target.toQByteArrayRef(), QByteArray(
                        "567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123"));
            }
            QVERIFY(block->readFinished());
        }

        qio->seek(0);
        QCOMPARE(data, QByteArray("AbCDEF1234567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123"));

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QCOMPARE(dataRead.toQByteArrayRef(), data);
            QVERIFY(stream->isEnded());

            Util::ByteArray contents("Asdf");
            stream->write(contents);
            stream->write(Util::ByteView(contents));
            stream->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                stream->write(qba);
            }
            stream->write(std::move(contents));
            stream->write("Asdfg");
            stream->write(std::string("QWERTY"));
            stream->write(QString("ZXCV"));

            QCOMPARE(dataRead.toQByteArrayRef(), data);
        }

        QByteArray finalContents
                ("AbCDEF1234567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCV");
        waitForCondition([&]() { return finalContents == data; });
        QCOMPARE(finalContents, data);

        qio->seek(0);
        backing = Access::qio(qio);
        QVERIFY(backing.get() != nullptr);

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QCOMPARE(dataRead.toQByteArrayRef(), data);
            QVERIFY(stream->isEnded());
        }

        data.clear();
        qio->seek(0);
        backing = Access::qio(qio);
        QVERIFY(backing.get() != nullptr);

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QVERIFY(dataRead.empty());
            QVERIFY(stream->isEnded());
        }

    }

    void ownershipTransferred()
    {
        QByteArray data("AbCDEF1234567890");
        auto qio = new QBuffer(&data);
        QVERIFY(qio->open(QIODevice::ReadWrite));

        auto backing = Access::qio(std::unique_ptr<QIODevice>(qio));
        QVERIFY(backing.get() != nullptr);

        {
            auto block = backing->block();
            QVERIFY(block.get() != nullptr);

            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), data.mid(0, 10));
            }
            {
                QByteArray target;
                block->read(target, 2);
                QCOMPARE(target, data.mid(10, 2));
            }
            QVERIFY(!block->readFinished());
            block->seek(static_cast<std::size_t>(-1));

            Util::ByteArray contents("Asdf");
            block->write(contents);
            block->write(Util::ByteView(contents));
            block->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                block->write(qba);
            }
            block->write(std::move(contents));
            block->write("Asdfg");
            block->write(std::string("QWERTY"));
            block->write(QString("ZXCV"));

            block->seek(block->tell() - 2);
            block->write("123");
            block->move(-3);
            QVERIFY(!block->readFinished());
            {
                QByteArray target;
                block->read(target, 3);
                QCOMPARE(target, QByteArray("123"));
            }

            block->seek(0);
            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), QByteArray("AbCDEF1234"));
            }
            QVERIFY(!block->readFinished());
            QCOMPARE((int) block->tell(), 10);
            {
                Util::ByteArray target;
                block->read(target);
                QCOMPARE(target.toQByteArrayRef(), QByteArray(
                        "567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123"));
            }
            QVERIFY(block->readFinished());
        }

        qio->seek(0);
        QCOMPARE(data, QByteArray("AbCDEF1234567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123"));

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QCOMPARE(dataRead.toQByteArrayRef(), data);
            QVERIFY(stream->isEnded());

            Util::ByteArray contents("Asdf");
            stream->write(contents);
            stream->write(Util::ByteView(contents));
            stream->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                stream->write(qba);
            }
            stream->write(std::move(contents));
            stream->write("Asdfg");
            stream->write(std::string("QWERTY"));
            stream->write(QString("ZXCV"));

            QCOMPARE(dataRead.toQByteArrayRef(), data);
        }


        QByteArray finalContents
                ("AbCDEF1234567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCV");
        waitForCondition([&]() { return finalContents == data; });
        QCOMPARE(finalContents, data);

        backing.reset();
        qio = nullptr;

        QCOMPARE(finalContents, data);
    }

    void ownershipRetained()
    {
        QByteArray data("AbCDEF1234567890");
        std::unique_ptr<QBuffer> qio(new QBuffer(&data));
        QVERIFY(qio->open(QIODevice::ReadWrite));

        auto backing = Access::qio(qio.get());
        QVERIFY(backing.get() != nullptr);

        {
            auto block = backing->block();
            QVERIFY(block.get() != nullptr);

            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), data.mid(0, 10));
            }
            {
                QByteArray target;
                block->read(target, 2);
                QCOMPARE(target, data.mid(10, 2));
            }
            QVERIFY(!block->readFinished());
            block->seek(static_cast<std::size_t>(-1));

            Util::ByteArray contents("Asdf");
            block->write(contents);
            block->write(Util::ByteView(contents));
            block->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                block->write(qba);
            }
            block->write(std::move(contents));
            block->write("Asdfg");
            block->write(std::string("QWERTY"));
            block->write(QString("ZXCV"));

            block->seek(block->tell() - 2);
            block->write("123");
            block->move(-3);
            QVERIFY(!block->readFinished());
            {
                QByteArray target;
                block->read(target, 3);
                QCOMPARE(target, QByteArray("123"));
            }

            block->seek(0);
            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), QByteArray("AbCDEF1234"));
            }
            QVERIFY(!block->readFinished());
            QCOMPARE((int) block->tell(), 10);
            {
                Util::ByteArray target;
                block->read(target);
                QCOMPARE(target.toQByteArrayRef(), QByteArray(
                        "567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123"));
            }
            QVERIFY(block->readFinished());
        }

        qio->seek(0);
        QCOMPARE(data, QByteArray("AbCDEF1234567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123"));

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QCOMPARE(dataRead.toQByteArrayRef(), data);
            QVERIFY(stream->isEnded());

            Util::ByteArray contents("Asdf");
            stream->write(contents);
            stream->write(Util::ByteView(contents));
            stream->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                stream->write(qba);
            }
            stream->write(std::move(contents));
            stream->write("Asdfg");
            stream->write(std::string("QWERTY"));
            stream->write(QString("ZXCV"));

            QCOMPARE(dataRead.toQByteArrayRef(), data);
        }

        QByteArray finalContents
                ("AbCDEF1234567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCV");
        waitForCondition([&]() { return finalContents == data; });
        QCOMPARE(finalContents, data);

        qio->seek(0);
        backing = Access::qio(qio.get());
        QVERIFY(backing.get() != nullptr);

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QCOMPARE(dataRead.toQByteArrayRef(), data);
            QVERIFY(stream->isEnded());
        }

        data.clear();
        qio->seek(0);
        backing = Access::qio(qio.get());
        QVERIFY(backing.get() != nullptr);

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QVERIFY(dataRead.empty());
            QVERIFY(stream->isEnded());
        }
    }

    void dedicatedThread()
    {
        Util::ByteArray data("AbCDEF1234567890");

        auto backing = Access::qio([&] {
            auto result = std::unique_ptr<QBuffer>(new QBuffer);
            result->open(QIODevice::ReadWrite);
            result->write(data.toQByteArrayRef());
            result->seek(0);
            return std::move(result);
        });
        QVERIFY(backing.get() != nullptr);

        {
            auto block = backing->block();
            QVERIFY(block.get() != nullptr);

            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), data.mid(0, 10).toQByteArrayRef());
            }
            {
                QByteArray target;
                block->read(target, 2);
                QCOMPARE(target, data.mid(10, 2).toQByteArrayRef());
            }
            QVERIFY(!block->readFinished());
            block->seek(static_cast<std::size_t>(-1));

            Util::ByteArray contents("Asdf");
            block->write(contents);
            block->write(Util::ByteView(contents));
            block->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                block->write(qba);
            }
            block->write(std::move(contents));
            block->write("Asdfg");
            block->write(std::string("QWERTY"));
            block->write(QString("ZXCV"));

            block->seek(block->tell() - 2);
            block->write("123");
            block->move(-3);
            QVERIFY(!block->readFinished());
            {
                QByteArray target;
                block->read(target, 3);
                QCOMPARE(target, QByteArray("123"));
            }

            block->seek(0);
            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), QByteArray("AbCDEF1234"));
            }
            QVERIFY(!block->readFinished());
            QCOMPARE((int) block->tell(), 10);
            {
                Util::ByteArray target;
                block->read(target);
                QCOMPARE(target.toQByteArrayRef(), QByteArray(
                        "567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123"));
            }
            QVERIFY(block->readFinished());

            block->seek(0);
        }

        data += "AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123";

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            QVERIFY(startAndWait(*stream));
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(stream->isEnded());

            Util::ByteArray contents("Asdf");
            stream->write(contents);
            stream->write(Util::ByteView(contents));
            stream->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                stream->write(qba);
            }
            stream->write(std::move(contents));
            stream->write("Asdfg");
            stream->write(std::string("QWERTY"));
            stream->write(QString("ZXCV"));

            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
        }
    }

};

QTEST_MAIN(TestIODriverQIO)

#include "qio.moc"
