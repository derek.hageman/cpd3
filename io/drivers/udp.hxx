/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_UDP_HXX
#define CPD3IO_UDP_HXX

#include "core/first.hxx"

#include <cstdint>
#include <QHostAddress>

#include "io/io.hxx"
#include "io/access.hxx"

namespace CPD3 {
namespace IO {
namespace UDP {

/**
 * A UDP packet from a peer.
 */
struct CPD3IO_EXPORT Packet final {
    Util::ByteArray data;
    QHostAddress address;
    std::uint16_t port;

    Packet();

    Packet(const Packet &) = default;

    Packet &operator=(const Packet &) = default;

    Packet(Packet &&) = default;

    Packet &operator=(Packet &&) = default;
};

/**
 * A UDP stream connection.
 */
class CPD3IO_EXPORT Stream : public Generic::Stream {
public:
    Stream();

    virtual ~Stream();

    /**
     * Send a packet to a specific host.
     *
     * @param data      the packet data
     * @param address   the target address
     * @param port      the target port
     */
    virtual void sendTo(const Util::ByteView &data,
                        const QHostAddress &address,
                        std::uint16_t port) = 0;

    virtual void sendTo(Util::ByteArray &&data, const QHostAddress &address, std::uint16_t port);

    void sendTo(const QByteArray &data, const QHostAddress &address, std::uint16_t port);

    void sendTo(const std::string &str, const QHostAddress &address, std::uint16_t port);

    void sendTo(const QString &str, const QHostAddress &address, std::uint16_t port);

    void sendTo(const char *str, const QHostAddress &address, std::uint16_t port);


    /**
     * Send a packet to a host, resolving the address if required.
     *
     * @param data      the packet data
     * @param host      the target host name or address
     * @param port      the target port
     */
    virtual void sendTo(const Util::ByteView &data, std::string host, std::uint16_t port) = 0;

    virtual void sendTo(Util::ByteArray &&data, std::string host, std::uint16_t port);

    void sendTo(const QByteArray &data, std::string host, std::uint16_t port);

    void sendTo(const std::string &str, std::string host, std::uint16_t port);

    void sendTo(const QString &str, std::string host, std::uint16_t port);

    void sendTo(const char *str, std::string host, std::uint16_t port);


    /**
     * Get the local port the stream is bound to.
     *
     * @return  the local port
     */
    virtual std::uint16_t localPort() const = 0;


    /**
     * Emitted for each packet received.
     */
    Threading::Signal<const Packet &> received;
};

/**
 * The configuration for a UDP stream.
 */
struct CPD3IO_EXPORT Configuration final {
    /**
     * Use any available port.
     */
    static constexpr std::uint16_t port_any = 0;

    Configuration();

    ~Configuration();

    Configuration(const Configuration &) = default;

    Configuration &operator=(const Configuration &) = default;

    Configuration(Configuration &&) = default;

    Configuration &operator=(Configuration &&) = default;


    /**
     * Create a UDP stream using the configuration.
     *
     * @return              the UDP stream
     */
    std::unique_ptr<Stream> create() const;

    /** @see create() */
    std::unique_ptr<Stream> operator()() const;


    /**
     * A listen address family.
     */
    enum class AddressFamily {
        /** Listen for connections on any/all network interfaces. */
                Any,

        /** Listen for connections on any loopback interface. */
                Loopback,

        /** Listen for connections on the IPv4 loopback interface only. */
                LoopbackIPv4,

        /** Listen for connections on the IPv6 loopback interface only. */
                LoopbackIPv6
    };

    /**
     * The local addresses to bind to.
     */
    std::vector<std::pair<QHostAddress, std::uint16_t>> localAddresses;
    /**
     * The local host names or address to bind to.
     */
    std::vector<std::pair<std::string, std::uint16_t>> localHosts;

    /**
     * Require all local addresses to bind during creation.
     */
    bool requireAllLocal;

    /**
     * The remote addresses to send to.
     */
    std::vector<std::pair<QHostAddress, std::uint16_t>> remoteAddresses;

    /**
     * The remote host names or addresses to send to.
     */
    std::vector<std::pair<std::string, std::uint16_t>> remoteHosts;

    /**
     * Require all remote hosts to resolve during creation.
     */
    bool requireAllRemote;

    /**
     * Wait for all addresses to resolve during creation.
     */
    bool waitForResolution;


    /**
     * Set the target port for all addresses.
     *
     * @param port  the port
     * @return      the updated configuration
     */
    Configuration &targetPort(std::uint16_t port);

    /**
     * Set the target port for all addresses.
     *
     * @param port  the port
     * @return      the updated configuration
     */
    Configuration &listenPort(std::uint16_t port);


    /**
     * Set the listen addresses to a given family.
     *
     * @param address   the address family to listen on
     * @return          the updated configuration
     */
    Configuration &listen(AddressFamily address, std::uint16_t port = port_any);


    /**
     * Add an address to listen on.
     *
     * @param address   the address to listen on
     * @param port      the port to listen on
     * @return          the updated configuration
     */
    Configuration &listen(const QHostAddress &address, std::uint16_t port = port_any);

    /**
     * Add an address or host name to listen on.
     *
     * @param address   the address to listen on
     * @param port      the port to listen on
     * @return          the updated configuration
     */
    Configuration &listen(std::string address, std::uint16_t port = port_any);


    /**
     * Connect to the loopback interface.
     *
     * @param port      the port to connect to
     * @return          the updated configuration
     */
    Configuration &loopback(std::uint16_t port);


    /**
     * Connect to a remote address, leaving the port unspecified (either set later or
     * using the previously set one).
     *
     * @param address   the address to connect to
     * @return          the updated configuration
     */
    Configuration &connect(const QHostAddress &address);

    /**
     * Connect to a remote address.
     *
     * @param address   the address to connect to
     * @param port      the port to connect to
     * @return          the updated configuration
     */
    Configuration &connect(const QHostAddress &address, std::uint16_t port);

    /**
     * Connect to a remote address or host, leaving the port unspecified (either set later or
     * using the previously set one).
     *
     * @param address   the address to connect to
     * @return          the updated configuration
     */
    Configuration &connect(std::string address);

    /**
     * Connect to a remote address or host.
     *
     * @param address   the address to connect to
     * @param port      the port to connect to
     * @return          the updated configuration
     */
    Configuration &connect(std::string address, std::uint16_t port);

};


}
}
}

#endif //CPD3IO_UDP_HXX
