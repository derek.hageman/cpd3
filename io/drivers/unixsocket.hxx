/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_UNIXSOCKET_HXX
#define CPD3IO_UNIXSOCKET_HXX

#include "core/first.hxx"

#include <thread>
#include <mutex>

#include "io/io.hxx"
#include "io/socket.hxx"
#include "io/notifywake.hxx"

namespace CPD3 {
namespace IO {
namespace Socket {
namespace Unix {

/**
 * The configuration for a Unix socket.
 */
struct CPD3IO_EXPORT Configuration final {
};

/**
 * A Unix domain socket connection.
 */
class CPD3IO_EXPORT Connection : public Socket::Connection {
public:
    Connection();

    virtual ~Connection();

    /**
     * Get path the socket resides at.
     *
     * @return the socket path
     */
    virtual std::string socketPath() const = 0;
};

/**
 * Connect to a Unix socket.
 *
 * @param path          the socket path
 * @param config        the configuration
 * @return              the connection or null on a failure
 */
CPD3IO_EXPORT std::unique_ptr<Connection> connect(std::string path,
                                                  const Configuration &config = {});


/**
 * A Unix socket server.  The server will listen on a Unix domain socket and will call a provided
 * function for each incoming connection, when they are ready to use.
 * <br>
 * Destroying the server stops it listening but does not close any existing connections
 * that have been fully established.
 */
class CPD3IO_EXPORT Server : public Socket::Server {
    std::string path;

#ifdef Q_OS_UNIX
    IncomingConnection connection;
    Configuration config;

    int socket;

    NotifyWake wake;

    std::thread thread;
    std::mutex mutex;
    bool terminated;

    void run();

    void shutdown();

#endif

public:
    Server() = delete;

    virtual ~Server();

    Server(const Server &) = delete;

    Server &operator=(const Server &) = delete;

    /**
     * Create a server listening on the specified Unix socket path.
     *
     * @param connection    the function to call for incoming connections
     * @param path          the socket path
     * @param config        the configuration
     */
    Server(IncomingConnection connection, std::string path, Configuration config = {});

    /**
     * Start listening and accepting connections.
     *
     * @param removeExisting    if set, then remove any existing socket instead of failing
     * @return                  true on success
     */
    bool startListening(bool removeExisting = true);

    /**
     * Stop listening and refuse further connections.
     */
    void stopListening();

    /**
     * Test if the server is currently listening for connections.
     *
     * @return  true if new connections are accepted
     */
    bool isListening() const;

    std::string describeServer() const override;
};

}
}
}
}

#endif //CPD3IO_UNIXSOCKET_HXX
