/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <QAbstractSocket>
#include <QLocalSocket>
#include <QFileDevice>
#include <QNetworkReply>
#include <QEventLoop>

#ifdef HAVE_QSERIALPORT
#include <QSerialPort>
#endif

#include "qiowrapper.hxx"

namespace CPD3 {
namespace IO {

static constexpr std::size_t readBlockSize = 65536;
static constexpr std::size_t stallThreshold = 4194304;


QIOWrapper::QIOWrapper(QIODevice &device) : device(device),
                                            readStarted(false),
                                            readStalled(false),
                                            readEnded(false),
                                            writeStarted(false),
                                            sentWriteStall(false)
{ }

QIOWrapper::~QIOWrapper() = default;

void QIOWrapper::write(const Util::ByteView &data)
{
    Q_ASSERT(device.thread() == QThread::currentThread());
    writeBuffer += data;
}

void QIOWrapper::write(Util::ByteArray &&data)
{
    Q_ASSERT(device.thread() == QThread::currentThread());
    writeBuffer += std::move(data);
}

void QIOWrapper::start()
{
    Q_ASSERT(device.thread() == QThread::currentThread());
    if (readStarted)
        return;
    readStarted = true;

    /* Reading impossible (e.g. socket failure), so end it right now */
    if (!(device.openMode() & QIODevice::ReadOnly)) {
        if (readEnded)
            return;
        readEnded = true;
        ended();
        return;
    }

    if (!receiver)
        receiver.reset(new QObject);

    connections.emplace_back(QObject::connect(&device, &QIODevice::readyRead, receiver.get(),
                                              std::bind(&QIOWrapper::serviceRead, this),
                                              Qt::QueuedConnection));

    connections.emplace_back(
            QObject::connect(&device, &QIODevice::readChannelFinished, receiver.get(),
                             std::bind(&QIOWrapper::serviceRead, this), Qt::QueuedConnection));

    if (auto socket = dynamic_cast<const QAbstractSocket *>(&device)) {
        qRegisterMetaType<QAbstractSocket::SocketState>("QAbstractSocket::SocketState");
        qRegisterMetaType<QAbstractSocket::SocketError>("QAbstractSocket::SocketError");

        connections.emplace_back(
                QObject::connect(socket, &QAbstractSocket::stateChanged, receiver.get(),
                                 std::bind(&QIOWrapper::serviceRead, this), Qt::QueuedConnection));
        connections.emplace_back(QObject::connect(socket, static_cast<void (QAbstractSocket::*)(
                                                          QAbstractSocket::SocketError)>(&QAbstractSocket::error), receiver.get(),
                                                  std::bind(&QIOWrapper::serviceRead, this),
                                                  Qt::QueuedConnection));

        readPoll.reset(new QTimer);
        readPoll->setSingleShot(false);
        readPoll->setInterval(500);
        connections.emplace_back(QObject::connect(readPoll.get(), &QTimer::timeout, receiver.get(),
                                                  std::bind(&QIOWrapper::serviceRead, this),
                                                  Qt::DirectConnection));
        readPoll->start();
    } else if (auto socket = dynamic_cast<const QLocalSocket *>(&device)) {
        qRegisterMetaType<QLocalSocket::LocalSocketState>("QLocalSocket::LocalSocketState");
        qRegisterMetaType<QLocalSocket::LocalSocketError>("QLocalSocket::LocalSocketError");

        connections.emplace_back(
                QObject::connect(socket, &QLocalSocket::stateChanged, receiver.get(),
                                 std::bind(&QIOWrapper::serviceRead, this), Qt::QueuedConnection));
        connections.emplace_back(QObject::connect(socket,
                                                  static_cast<void (QLocalSocket::*)(QLocalSocket::LocalSocketError)>(&QLocalSocket::error),
                                                  receiver.get(),
                                                  std::bind(&QIOWrapper::serviceRead, this),
                                                  Qt::QueuedConnection));

        readPoll.reset(new QTimer);
        readPoll->setSingleShot(false);
        readPoll->setInterval(500);
        connections.emplace_back(QObject::connect(readPoll.get(), &QTimer::timeout, receiver.get(),
                                                  std::bind(&QIOWrapper::serviceRead, this),
                                                  Qt::DirectConnection));
        readPoll->start();
    } else if (auto reply = dynamic_cast<const QNetworkReply *>(&device)) {
        connections.emplace_back(QObject::connect(reply, &QNetworkReply::finished, receiver.get(),
                                                  std::bind(&QIOWrapper::serviceRead, this),
                                                  Qt::QueuedConnection));
    }
#ifdef HAVE_QSERIALPORT
    else if (auto port = dynamic_cast<const QSerialPort *>(&device)) {
        qRegisterMetaType<QSerialPort::SerialPortError>("QSerialPort::SerialPortError");

        connections.emplace_back(
                QObject::connect(port, &QSerialPort::errorOccurred, receiver.get(),
                                 std::bind(&QIOWrapper::serviceRead, this), Qt::QueuedConnection));

        readPoll.reset(new QTimer);
        readPoll->setSingleShot(false);
        readPoll->setInterval(500);
        connections.emplace_back(QObject::connect(readPoll.get(), &QTimer::timeout, receiver.get(),
                                                  std::bind(&QIOWrapper::serviceRead, this),
                                                  Qt::DirectConnection));
        readPoll->start();
    }
#endif

    Threading::runQueuedFunctor(receiver.get(), std::bind(&QIOWrapper::serviceRead, this));
}

bool QIOWrapper::isEnded() const
{
    Q_ASSERT(device.thread() == QThread::currentThread());

    if (!(device.openMode() & QIODevice::ReadOnly))
        return true;
    return deviceAtEnd(device);
}

void QIOWrapper::readStall(bool enable)
{
    Q_ASSERT(device.thread() == QThread::currentThread());
    if (enable == readStalled)
        return;
    readStalled = enable;

    if (readStarted && !readEnded && !readStalled && receiver) {
        Threading::runQueuedFunctor(receiver.get(), std::bind(&QIOWrapper::serviceRead, this));
    }
}

void QIOWrapper::destroy(bool flushWrite,int flushTimeout)
{
    for (auto &c : connections) {
        QObject::disconnect(c);
    }
    connections.clear();
    if (receiver)
        receiver->disconnect();
    if (readPoll)
        readPoll->disconnect();
    if (device.thread() == QThread::currentThread()) {
        receiver.reset();
        readPoll.reset();
    } else {
        if (receiver)
            receiver.release()->deleteLater();
        if (readPoll)
            readPoll.release()->deleteLater();
    }

    if (flushWrite) {
        Q_ASSERT(device.thread() == QThread::currentThread());

        if (flushTimeout >= 0) {
            QTimer::singleShot(flushTimeout, &device, &QIODevice::close);
        }

        while (!writeBuffer.empty()) {
            if (auto socket = dynamic_cast<const QAbstractSocket *>(&device)) {
                switch (socket->state()) {
                case QAbstractSocket::ConnectedState:
                    break;
                case QAbstractSocket::UnconnectedState:
                    return;
                default: {
                    QEventLoop loop;
                    QTimer::singleShot(500, &loop, &QEventLoop::quit);
                    QObject::connect(socket, &QAbstractSocket::stateChanged, &loop,
                                     &QEventLoop::quit, Qt::DirectConnection);
                    loop.exec();
                    continue;
                }
                }
            } else if (auto socket = dynamic_cast<const QLocalSocket *>(&device)) {
                switch (socket->state()) {
                case QLocalSocket::ConnectedState:
                    break;
                case QLocalSocket::UnconnectedState:
                    return;
                default: {
                    QEventLoop loop;
                    QTimer::singleShot(500, &loop, &QEventLoop::quit);
                    QObject::connect(socket, &QLocalSocket::stateChanged, &loop, &QEventLoop::quit,
                                     Qt::DirectConnection);
                    loop.exec();
                    continue;
                }
                }
            } else if (auto reply = dynamic_cast<const QNetworkReply *>(&device)) {
                if (!reply->isRunning())
                    return;
            }
            auto n = device.write(writeBuffer.data<const char *>(), writeBuffer.size());
            if (n < 0)
                return;
            Q_ASSERT(static_cast<std::size_t>(n) <= writeBuffer.size());
            writeBuffer.pop_front(n);
        }

        for (;;) {
            QEventLoop loop;

            QObject::connect(&device, &QIODevice::bytesWritten, &loop, &QEventLoop::quit,
                             Qt::DirectConnection);
            QTimer::singleShot(500, &loop, &QEventLoop::quit);

            if (auto socket = dynamic_cast<QAbstractSocket *>(&device)) {
                QObject::connect(socket, &QAbstractSocket::stateChanged, &loop, &QEventLoop::quit,
                                 Qt::DirectConnection);
                QObject::connect(socket,
                                 static_cast<void (QAbstractSocket::*)(QAbstractSocket::SocketError)>(&QAbstractSocket::error),
                                 &loop, &QEventLoop::quit, Qt::DirectConnection);

                switch (socket->state()) {
                case QAbstractSocket::UnconnectedState:
                    device.close();
                    return;
                case QAbstractSocket::ConnectedState:
                    switch (socket->error()) {
                    case QAbstractSocket::RemoteHostClosedError:
                    case QAbstractSocket::ConnectionRefusedError:
                        device.close();
                        return;
                    default:
                        break;
                    }

                    if (device.bytesToWrite() == 0) {
                        socket->disconnectFromHost();
                    } else {
                        /* Poll for remote close detection */
                        socket->read(1);
                    }
                    break;
                default:
                    break;
                }
            } else if (auto socket = dynamic_cast<QLocalSocket *>(&device)) {
                QObject::connect(socket, &QLocalSocket::stateChanged, &loop, &QEventLoop::quit,
                                 Qt::DirectConnection);
                QObject::connect(socket,
                                 static_cast<void (QLocalSocket::*)(QLocalSocket::LocalSocketError)>(&QLocalSocket::error),
                                 &loop, &QEventLoop::quit, Qt::DirectConnection);

                switch (socket->state()) {
                case QLocalSocket::UnconnectedState:
                    device.close();
                    return;
                case QLocalSocket::ConnectedState:
                    switch (socket->error()) {
                    case QLocalSocket::ServerNotFoundError:
                    case QLocalSocket::ConnectionRefusedError:
                        device.close();
                        return;
                    default:
                        break;
                    }

                    if (device.bytesToWrite() == 0) {
                        socket->disconnectFromServer();
                    } else {
                        /* Poll for remote close detection */
                        socket->read(1);
                    }
                    break;
                default:
                    break;
                }
            } else if (auto reply = dynamic_cast<QNetworkReply *>(&device)) {
                QObject::connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit,
                                 Qt::DirectConnection);

                if (!reply->isRunning()) {
                    device.close();
                    return;
                }
                if (device.bytesToWrite() == 0) {
                    device.close();
                    return;
                }
            } else if (auto file = dynamic_cast<QFileDevice *>(&device)) {
                file->flush();
                if (device.bytesToWrite() == 0) {
                    device.close();
                    return;
                }
            } else {
                if (device.bytesToWrite() == 0) {
                    device.close();
                    return;
                }
            }

            loop.exec();
        }
    }
}

bool QIOWrapper::drainWriteBuffer()
{
    Q_ASSERT(device.thread() == QThread::currentThread());
    while (!writeBuffer.empty()) {
        auto n = device.write(writeBuffer.data<const char *>(), writeBuffer.size());
        if (n < 0) {
            return false;
        } else if (n == 0) {
            QThread::yieldCurrentThread();
            continue;
        }
        Q_ASSERT(static_cast<std::size_t>(n) <= writeBuffer.size());
        writeBuffer.pop_front(n);
    }
    if (sentWriteStall) {
        sentWriteStall = false;
        writeStall(false);
    }
    return true;
}

void QIOWrapper::discardWriteBuffer()
{
    Q_ASSERT(device.thread() == QThread::currentThread());
    writeBuffer.clear();
    if (sentWriteStall) {
        sentWriteStall = false;
        writeStall(false);
    }
}

void QIOWrapper::processWriteBuffer(bool queued)
{
    Q_ASSERT(device.thread() == QThread::currentThread());
    if (!writeStarted) {
        writeStarted = true;
        if (!receiver)
            receiver.reset(new QObject);
        connections.emplace_back(QObject::connect(&device, &QIODevice::bytesWritten, receiver.get(),
                                                  std::bind(&QIOWrapper::serviceWrite, this),
                                                  Qt::QueuedConnection));
        if (auto socket = dynamic_cast<const QAbstractSocket *>(&device)) {
            qRegisterMetaType<QAbstractSocket::SocketState>("QAbstractSocket::SocketState");

            connections.emplace_back(
                    QObject::connect(socket, &QAbstractSocket::stateChanged, receiver.get(),
                                     std::bind(&QIOWrapper::serviceWrite, this),
                                     Qt::QueuedConnection));
        } else if (auto socket = dynamic_cast<const QLocalSocket *>(&device)) {
            qRegisterMetaType<QLocalSocket::LocalSocketState>("QLocalSocket::LocalSocketState");

            connections.emplace_back(
                    QObject::connect(socket, &QLocalSocket::stateChanged, receiver.get(),
                                     std::bind(&QIOWrapper::serviceWrite, this),
                                     Qt::QueuedConnection));
        } else if (auto reply = dynamic_cast<const QNetworkReply *>(&device)) {
            connections.emplace_back(
                    QObject::connect(reply, &QNetworkReply::finished, receiver.get(),
                                     std::bind(&QIOWrapper::serviceWrite, this),
                                     Qt::QueuedConnection));
        }
    }
    if (!device.bytesToWrite()) {
        if (queued) {
            Q_ASSERT(receiver);
            Threading::runQueuedFunctor(receiver.get(), std::bind(&QIOWrapper::serviceWrite, this));
        } else {
            serviceWrite();
        }
    }
}

void QIOWrapper::serviceWrite()
{
    if (writeBuffer.empty())
        return;
    if (auto socket = dynamic_cast<const QAbstractSocket *>(&device)) {
        switch (socket->state()) {
        case QAbstractSocket::ConnectedState:
            break;
        case QAbstractSocket::UnconnectedState:
            /* Disconnected and not connecting, so just discard it */
            writeBuffer.clear();
            return;
        default:
            /* May become writable, so just wait */
            return;
        }
    } else if (auto socket = dynamic_cast<const QLocalSocket *>(&device)) {
        switch (socket->state()) {
        case QLocalSocket::ConnectedState:
            break;
        case QLocalSocket::UnconnectedState:
            /* Disconnected and not connecting, so just discard it */
            writeBuffer.clear();
            return;
        default:
            /* May become writable, so just wait */
            return;
        }
    } else if (auto reply = dynamic_cast<const QNetworkReply *>(&device)) {
        if (!reply->isRunning()) {
            /* No more writing possible */
            writeBuffer.clear();
            return;
        }
    }

    auto n = device.write(writeBuffer.data<const char *>(), writeBuffer.size());
    if (n < 0) {
        writeBuffer.clear();
    } else if (n > 0) {
        writeBuffer.pop_front(n);
    }

    if (writeBuffer.size() > stallThreshold) {
        if (!sentWriteStall) {
            sentWriteStall = true;
            writeStall(true);
        }
    } else {
        if (sentWriteStall) {
            sentWriteStall = false;
            writeStall(false);
        }
    }
}

void QIOWrapper::serviceRead()
{
    if (!readStarted || readEnded || readStalled)
        return;
    if (!(device.openMode() & QIODevice::ReadOnly))
        return;

    readBuffer.resize(readBlockSize);
    auto n = device.read(readBuffer.data<char *>(), readBuffer.size());
    if (n < 0) {
        if (auto socket = dynamic_cast<const QAbstractSocket *>(&device)) {
            switch (socket->state()) {
            case QAbstractSocket::HostLookupState:
            case QAbstractSocket::ConnectingState:
                /* Attempting to service while still connecting is an error, but
                 * we don't want to end until/unless the connection actually fails */
                return;
            default:
                break;
            }
        }

        readPoll.reset();
        readEnded = true;
        ended();
        return;
    } else if (n > 0) {
        Q_ASSERT(static_cast<std::size_t>(n) <= readBuffer.size());
        readBuffer.resize(n);
        read(readBuffer);

        Q_ASSERT(receiver);
        Threading::runQueuedFunctor(receiver.get(), std::bind(&QIOWrapper::serviceRead, this));
        return;
    }

    /*
     * Note that we can get here (0 read) on sockets without the socket being closed
     * (unbuferred mode and no data available returns 0).  So we have to have a check
     * for the "true" end regardless.
     */

    if (!deviceAtEnd(device))
        return;

    readPoll.reset();
    readEnded = true;
    ended();
}

bool QIOWrapper::deviceAtEnd(const QIODevice &device)
{
    if (auto socket = dynamic_cast<const QAbstractSocket *>(&device)) {
        switch (socket->state()) {
        case QAbstractSocket::UnconnectedState:
        case QAbstractSocket::ClosingState:
            return true;
        default:
            switch (socket->error()) {
            case QAbstractSocket::RemoteHostClosedError:
            case QAbstractSocket::ConnectionRefusedError:
                return true;
            default:
                break;
            }
            return false;
        }
    } else if (auto socket = dynamic_cast<const QLocalSocket *>(&device)) {
        switch (socket->state()) {
        case QLocalSocket::UnconnectedState:
        case QLocalSocket::ClosingState:
            return true;
        default:
            switch (socket->error()) {
            case QLocalSocket::ServerNotFoundError:
            case QLocalSocket::ConnectionRefusedError:
                return true;
            default:
                break;
            }
            return false;
        }
    } else if (auto reply = dynamic_cast<const QNetworkReply *>(&device)) {
        return !reply->isRunning();
    }
#ifdef HAVE_QSERIALPORT
    else if (auto port = dynamic_cast<const QSerialPort *>(&device)) {
        if (!port->isOpen())
            return true;

        switch (port->error()) {
        case QSerialPort::DeviceNotFoundError:
        case QSerialPort::PermissionError:
        case QSerialPort::WriteError:
        case QSerialPort::ReadError:
        case QSerialPort::ResourceError:
            return true;
        default:
            return false;
        }
    }
#endif
    return device.atEnd();
}

}
}