/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <QThread>
#include <QLoggingCategory>
#include <QGlobalStatic>
#include <QNetworkAccessManager>

#include "url.hxx"
#include "qiowrapper.hxx"
#include "core/threading.hxx"

Q_LOGGING_CATEGORY(log_io_driver_url, "cpd3.io.driver.url", QtWarningMsg)

namespace CPD3 {
namespace IO {

namespace URL {

Backing::~Backing() = default;

Backing::Backing(const QUrl &url, std::shared_ptr<Context> context) : context(std::move(context)),
                                                                      request(url)
{
    request.setAttribute(QNetworkRequest::HttpPipeliningAllowedAttribute, true);
    request.setAttribute(QNetworkRequest::SpdyAllowedAttribute, true);
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 1)
    request.setAttribute(QNetworkRequest::HTTP2AllowedAttribute, true);
#endif
#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
    request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
#endif
    request.setAttribute(QNetworkRequest::CacheSaveControlAttribute, false);
}

class Context::Thread final : public QThread {
    struct Startup {
        std::mutex mutex;
        std::condition_variable notify;
        bool ready;

        Startup() : ready(false)
        { }
    };

    Startup *startup;
public:
    std::unique_ptr<QNetworkAccessManager> manager;

    Thread()
    {
        setObjectName("URLContextQThread");

        Startup st;
        this->startup = &st;

        start();

        {
            std::unique_lock<std::mutex> lock(st.mutex);
            st.notify.wait(lock, [&st] { return st.ready; });
            Q_ASSERT(st.ready);
            Q_ASSERT(manager.get() != nullptr);
        }
    }

    virtual ~Thread() = default;

protected:
    void run() override
    {
        manager.reset(new QNetworkAccessManager);
        {
            Q_ASSERT(startup);
            std::lock_guard<std::mutex> lock(startup->mutex);
            startup->ready = true;
        }
        startup->notify.notify_all();
        startup = nullptr;

        QThread::exec();

        manager.reset();
    }
};

Context::Context() : thread(new Thread())
{ }

Context::~Context()
{
    if (thread->manager && thread->manager->thread() == QThread::currentThread()) {
        thread->quit();
        thread.release()->deleteLater();
    } else {
        thread->quit();
        thread->wait();
        thread.reset();
    }
}

void Backing::tls(const QSslConfiguration &tls)
{ request.setSslConfiguration(tls); }

void Backing::header(QNetworkRequest::KnownHeaders name, const QVariant &value)
{ request.setHeader(name, value); }

void Backing::header(const Util::ByteView &name, const Util::ByteView &value)
{ request.setRawHeader(name.toQByteArray(), value.toQByteArray()); }

QUrl Backing::url() const
{ return request.url(); }


Stream::Stream(Backing &backing,
               const std::function<QNetworkReply *(QNetworkRequest &request,
                                                   QNetworkAccessManager &manager)> &rep) : context(
        backing.context), reply(nullptr)
{
    Threading::runBlockingFunctor(context->thread->manager.get(), [&]() {
        reply = rep(backing.request, *context->thread->manager);
        if (!reply)
            return;
        wrapper.reset(new QIOWrapper(*reply));
        auto progressSignal = receiveProgress;
        QObject::connect(reply, &QNetworkReply::downloadProgress,
                         [progressSignal](qint64 bytesReceived, qint64 bytesTotal) {
                             if (bytesTotal <= 0)
                                 return;
                             progressSignal(static_cast<double>(bytesReceived) /
                                                    static_cast<double>(bytesTotal));
                         });
    });
    if (!wrapper)
        return;
    read = wrapper->read;
    ended = wrapper->ended;
    writeStall = wrapper->writeStall;
}

Stream::~Stream()
{
    if (!reply || !wrapper)
        return;
    if (reply->thread() == QThread::currentThread()) {
        wrapper->destroy(true, timeout ? -1 : 30000);
        wrapper.reset();
        reply->deleteLater();
        reply = nullptr;
    } else {
        Threading::runBlockingFunctor(reply, [this]() {
            wrapper->destroy(true, timeout ? -1 : 30000);
            wrapper.reset();
            reply->deleteLater();
            reply = nullptr;
        });
    }
}

void Stream::write(const Util::ByteView &)
{ }

void Stream::write(const Util::ByteArray &)
{ }

void Stream::write(Util::ByteArray &&)
{ }

void Stream::write(const QByteArray &)
{ }

void Stream::write(QByteArray &&)
{ }

void Stream::start()
{
    if (!reply)
        return;
    if (reply->thread() == QThread::currentThread()) {
        Q_ASSERT(wrapper);
        wrapper->start();
        return;
    }
    Threading::runBlockingFunctor(reply, [this]() {
        Q_ASSERT(wrapper);
        wrapper->start();
    });
}

bool Stream::isEnded() const
{
    if (!reply)
        return true;
    if (reply->thread() == QThread::currentThread()) {
        Q_ASSERT(wrapper);
        return wrapper->isEnded();
    }
    bool result = false;
    Threading::runBlockingFunctor(reply, [this, &result]() {
        Q_ASSERT(wrapper);
        result = wrapper->isEnded();
    });
    return result;
}

void Stream::readStall(bool enable)
{
    if (!reply)
        return;
    if (reply->thread() == QThread::currentThread()) {
        Q_ASSERT(wrapper);
        return wrapper->readStall(enable);
    }
    Threading::runBlockingFunctor(reply, [this, enable]() {
        wrapper->readStall(enable);
    });
}

void Stream::startTimeout(double seconds)
{
    if (!reply)
        return;
    if (reply->thread() == QThread::currentThread()) {
        if (!reply)
            return;
        if (timeout)
            timeout->deleteLater();
        timeout = new QTimer(reply);
        if (!FP::defined(seconds) || seconds < 0.0)
            return;
        int milliseconds = static_cast<int>(std::ceil(seconds * 1000));
        timeout->setSingleShot(true);
        QObject::connect(timeout, &QTimer::timeout, reply, &QNetworkReply::abort,
                         Qt::QueuedConnection);
        timeout->start(milliseconds);
        return;
    }
    Threading::runBlockingFunctor(reply, [this, seconds]() {
        if (!reply)
            return;
        if (timeout)
            timeout->deleteLater();
        timeout = new QTimer(reply);
        if (!FP::defined(seconds) || seconds < 0.0)
            return;
        int milliseconds = static_cast<int>(std::ceil(seconds * 1000));
        timeout->setSingleShot(true);
        QObject::connect(timeout, &QTimer::timeout, reply, &QNetworkReply::abort,
                         Qt::QueuedConnection);
        timeout->start(milliseconds);
    });
}

bool Stream::responseError() const
{
    if (!reply)
        return true;
    if (reply->thread() == QThread::currentThread())
        return reply->error() != QNetworkReply::NoError;

    bool haveError = true;
    Threading::runBlockingFunctor(reply, [this, &haveError]() {
        if (!reply)
            return;
        haveError = (reply->error() != QNetworkReply::NoError);
    });
    return haveError;
}

void Stream::abort()
{
    if (!reply)
        return;
    if (reply->thread() == QThread::currentThread())
        return reply->abort();

    Threading::runQueuedFunctor(reply, [this]() {
        if (!reply)
            return;
        reply->abort();
    });
}

std::unique_ptr<Generic::Stream> Backing::create_stream(const std::function<
        QNetworkReply *(QNetworkRequest &request, QNetworkAccessManager &manager)> &reply)
{ return std::unique_ptr<Generic::Stream>(new Stream(*this, reply)); }

}


namespace Access {

namespace {

struct DefaultContext {
    std::shared_ptr<URL::Context> context;

    DefaultContext() : context(std::make_shared<URL::Context>())
    { }

    static std::shared_ptr<URL::Context> get();
};

Q_GLOBAL_STATIC(DefaultContext, defaultContext)

std::shared_ptr<URL::Context> DefaultContext::get()
{
    auto ctx = defaultContext();
    if (!ctx)
        return std::make_shared<URL::Context>();
    return ctx->context;
}

}

Handle urlGET(const QUrl &url, std::shared_ptr<URL::Context> context)
{
    if (!context)
        context = DefaultContext::get();

    class Backing final : public URL::Backing {
    public:
        Backing(const QUrl &url, std::shared_ptr<URL::Context> &&context) : URL::Backing(url,
                                                                                         std::move(
                                                                                                 context))
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        {
            return create_stream([](QNetworkRequest &request, QNetworkAccessManager &manager) {
                return manager.get(request);
            });
        }

        std::unique_ptr<Generic::Block> block() override
        { return {}; }
    };
    return std::make_shared<Backing>(url, std::move(context));
}

Handle urlGET(const std::string &url, std::shared_ptr<URL::Context> context)
{ return urlGET(QUrl(QString::fromStdString(url)), std::move(context)); }

Handle urlPUT(const QUrl &url, Handle data, std::shared_ptr<URL::Context> context)
{
    if (!context)
        context = DefaultContext::get();

    class Backing final : public URL::Backing {
        Handle data;
    public:
        Backing(const QUrl &url, Handle &&data, std::shared_ptr<URL::Context> &&context)
                : URL::Backing(url, std::move(context)), data(std::move(data))
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        {
            return create_stream([this](QNetworkRequest &request,
                                        QNetworkAccessManager &manager) -> QNetworkReply * {
                auto dev = data->qioStream();
                if (!dev || !dev->open(QIODevice::ReadOnly))
                    return nullptr;
                auto reply = manager.put(request, dev.get());
                QObject::connect(reply, &QNetworkReply::finished, dev.get(), &QObject::deleteLater);
                dev.release()->setParent(reply);
                return reply;
            });
        }

        std::unique_ptr<Generic::Block> block() override
        { return {}; }
    };
    return std::make_shared<Backing>(url, std::move(data), std::move(context));
}

Handle urlPUT(const QUrl &url,
              std::unique_ptr<IO::Generic::Stream> &&data,
              std::shared_ptr<URL::Context> context)
{
    if (!context)
        context = DefaultContext::get();

    class Backing final : public URL::Backing {
        std::unique_ptr<IO::Generic::Stream> data;
    public:
        Backing(const QUrl &url,
                std::unique_ptr<IO::Generic::Stream> &&data,
                std::shared_ptr<URL::Context> &&context) : URL::Backing(url, std::move(context)),
                                                           data(std::move(data))
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        {
            return create_stream([this](QNetworkRequest &request,
                                        QNetworkAccessManager &manager) -> QNetworkReply * {
                if (!data)
                    return nullptr;
                auto dev = Generic::Backing::qioStream(std::move(data));
                if (!dev || !dev->open(QIODevice::ReadOnly))
                    return nullptr;
                auto reply = manager.put(request, dev.get());
                QObject::connect(reply, &QNetworkReply::finished, dev.get(), &QObject::deleteLater);
                dev.release()->setParent(reply);
                return reply;
            });
        }

        std::unique_ptr<Generic::Block> block() override
        { return {}; }
    };
    return std::make_shared<Backing>(url, std::move(data), std::move(context));
}

Handle urlPUT(const std::string &url, Handle data, std::shared_ptr<URL::Context> context)
{ return urlPUT(QUrl(QString::fromStdString(url)), std::move(data), std::move(context)); }

Handle urlPUT(const std::string &url,
              std::unique_ptr<IO::Generic::Stream> &&data,
              std::shared_ptr<URL::Context> context)
{ return urlPUT(QUrl(QString::fromStdString(url)), std::move(data), std::move(context)); }

Handle urlPOST(const QUrl &url, Handle data, std::shared_ptr<URL::Context> context)
{
    if (!context)
        context = DefaultContext::get();

    class Backing final : public URL::Backing {
        Handle data;
    public:
        Backing(const QUrl &url, Handle &&data, std::shared_ptr<URL::Context> &&context)
                : URL::Backing(url, std::move(context)), data(std::move(data))
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        {
            return create_stream([this](QNetworkRequest &request,
                                        QNetworkAccessManager &manager) -> QNetworkReply * {
                auto dev = data->qioStream();
                if (!dev || !dev->open(QIODevice::ReadOnly))
                    return nullptr;
                auto reply = manager.post(request, dev.get());
                QObject::connect(reply, &QNetworkReply::finished, dev.get(), &QObject::deleteLater);
                dev.release()->setParent(reply);
                return reply;
            });
        }

        std::unique_ptr<Generic::Block> block() override
        { return {}; }
    };
    return std::make_shared<Backing>(url, std::move(data), std::move(context));
}

Handle urlPOST(const QUrl &url,
               std::unique_ptr<IO::Generic::Stream> &&data,
               std::shared_ptr<URL::Context> context)
{
    if (!context)
        context = DefaultContext::get();

    class Backing final : public URL::Backing {
        std::unique_ptr<IO::Generic::Stream> data;
    public:
        Backing(const QUrl &url,
                std::unique_ptr<IO::Generic::Stream> &&data,
                std::shared_ptr<URL::Context> &&context) : URL::Backing(url, std::move(context)),
                                                           data(std::move(data))
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        {
            return create_stream([this](QNetworkRequest &request,
                                        QNetworkAccessManager &manager) -> QNetworkReply * {
                if (!data)
                    return nullptr;
                auto dev = Generic::Backing::qioStream(std::move(data));
                if (!dev || !dev->open(QIODevice::ReadOnly))
                    return nullptr;
                auto reply = manager.post(request, dev.get());
                QObject::connect(reply, &QNetworkReply::finished, dev.get(), &QObject::deleteLater);
                dev.release()->setParent(reply);
                return reply;
            });
        }

        std::unique_ptr<Generic::Block> block() override
        { return {}; }
    };
    return std::make_shared<Backing>(url, std::move(data), std::move(context));
}

Handle urlPOST(const std::string &url, Handle data, std::shared_ptr<URL::Context> context)
{ return urlPOST(QUrl(QString::fromStdString(url)), std::move(data), std::move(context)); }

Handle urlPOST(const std::string &url,
               std::unique_ptr<IO::Generic::Stream> &&data,
               std::shared_ptr<URL::Context> context)
{ return urlPOST(QUrl(QString::fromStdString(url)), std::move(data), std::move(context)); }

}

}
}