/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include "serial.hxx"


namespace CPD3 {
namespace IO {

namespace Serial {

Backing::Backing(std::string name) : name(std::move(name))
{ }

Backing::~Backing() = default;

std::unique_ptr<Generic::Block> Backing::block()
{ return {}; }

Stream::Stream(std::string name) : name(std::move(name))
{ }

Stream::~Stream() = default;

}


namespace Access {

std::shared_ptr<Serial::Backing> serial(const QString &name)
{ return serial(name.toStdString()); }

std::shared_ptr<Serial::Backing> serial(const char *name)
{ return serial(std::string(name)); }

}

}
}