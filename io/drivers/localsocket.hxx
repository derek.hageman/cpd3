/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_LOCALSOCKET_HXX
#define CPD3IO_LOCALSOCKET_HXX

#include "core/first.hxx"

#include <QLocalSocket>

#include "io/io.hxx"
#include "io/socket.hxx"

namespace CPD3 {
namespace IO {
namespace Socket {
namespace Local {

/**
 * The configuration for a local socket.
 */
struct CPD3IO_EXPORT Configuration final {
};

/**
 * A Qt local socket connection.
 */
class CPD3IO_EXPORT Connection : public Socket::Connection::QtConnection {
protected:
    /**
     * The socket the connection is for.
     */
    QLocalSocket &socket;
public:
    Connection() = delete;

    virtual ~Connection();

    /**
     * Get the server name the socket is connected to.
     *
     * @return the server name
     */
    virtual std::string serverName() const = 0;

protected:
    /**
     * Create the local socket connection.
     *
     * @param socket    the socket
     * @param config    the configuration
     */
    explicit Connection(QLocalSocket &socket, const Configuration &config);
};

/**
 * Connect to a local socket.  Asynchronous connections may finish connection later, but will
 * accept data immediately and queue it for when they are established.
 *
 * @param server        the (Qt) server name
 * @param config        the configuration
 * @param synchronous   if set, then do not return until the connection is established and ready to use
 * @return              the connection or null on a failure
 */
CPD3IO_EXPORT std::unique_ptr<Connection> connect(const QString &server,
                                                  const Configuration &config = {},
                                                  bool synchronous = true);

CPD3IO_EXPORT std::unique_ptr<Connection> connect(const std::string &server,
                                                  const Configuration &config = {},
                                                  bool synchronous = true);

CPD3IO_EXPORT std::unique_ptr<Connection> connect(const char *server,
                                                  const Configuration &config = {},
                                                  bool synchronous = true);


/**
 * A local socket server.  The server will listen on a Qt socket interface and will call a provided
 * function for each incoming connection, when they are ready to use.
 * <br>
 * Destroying the server stops it listening but does not close any existing connections
 * that have been fully established.
 */
class CPD3IO_EXPORT Server : public Socket::Server {
    class Worker;

    std::shared_ptr<Worker> worker;

public:
    Server() = delete;

    virtual ~Server();

    Server(const Server &) = delete;

    Server &operator=(const Server &) = delete;

    /**
     * Create a server listening on the specified Qt socket name.
     *
     * @param connection    the function to call for incoming connections
     * @param name          the (Qt) server name
     * @param config        the configuration
     */
    Server(IncomingConnection connection, std::string name, Configuration config = {});

    Server(IncomingConnection connection, const QString &name, Configuration config = {});

    Server(IncomingConnection connection, const char *name, Configuration config = {});

    /**
     * Start listening and accepting connections.
     *
     * @param removeExisting    if set, then remove any existing socket instead of failing
     * @return                  true on success
     */
    bool startListening(bool removeExisting = true);

    /**
     * Stop listening and refuse further connections.
     */
    void stopListening();

    /**
     * Test if the server is currently listening for connections.
     *
     * @return  true if new connections are accepted
     */
    bool isListening() const;

    std::string describeServer() const override;
};

}
}
}
}

#endif //CPD3IO_LOCALSOCKET_HXX
