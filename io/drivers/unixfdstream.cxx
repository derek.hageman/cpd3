/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <cstring>
#include <unistd.h>
#include <poll.h>
#include <errno.h>
#include <string.h>
#include <QLoggingCategory>

#include "unixfdstream.hxx"


Q_LOGGING_CATEGORY(log_io_driver_unixfdstream, "cpd3.io.driver.unixfdstream", QtWarningMsg)


namespace CPD3 {
namespace IO {

UnixFDStream::UnixFDStream(int fd, bool enableRead, bool enableWrite, bool ownFD) : fd(fd),
                                                                                    enableRead(
                                                                                            enableRead),
                                                                                    enableWrite(
                                                                                            enableWrite),
                                                                                    ownFD(ownFD),
                                                                                    terminated(
                                                                                            false),
                                                                                    readStarted(
                                                                                            false),
                                                                                    readEnded(
                                                                                            false),
                                                                                    readShouldStall(
                                                                                            false),
                                                                                    lockState(
                                                                                            LockState::Unlocked)
{
    Q_ASSERT(fd != -1);

    NotifyWake::setNonblocking(fd);

    thread = std::thread(&UnixFDStream::run, this);
}

UnixFDStream::~UnixFDStream()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    wake();
    external.notify_all();

    if (thread.joinable()) {
        thread.join();
    }

    if (fd != -1 && ownFD) {
        NotifyWake::interruptLoop(::close, fd);
    }
}

void UnixFDStream::write(const Util::ByteView &data)
{
    if (data.empty())
        return;
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (!enableWrite || fd == -1)
            return;
        writeBuffer += data;
    }
    wake();
}

void UnixFDStream::write(Util::ByteArray &&data)
{
    if (data.empty())
        return;
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (!enableWrite || fd == -1)
            return;
        writeBuffer += std::move(data);
    }
    wake();
}

void UnixFDStream::start()
{
    bool immediateEnd = false;
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (readEnded) {
            immediateEnd = true;
        } else {
            readStarted = true;
        }
    }
    if (immediateEnd) {
        ended();
        return;
    }
    wake();
}

bool UnixFDStream::isEnded() const
{
    std::lock_guard<std::mutex> lock(const_cast<UnixFDStream *>(this)->mutex);
    return readEnded;
}

void UnixFDStream::readStall(bool enable)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        readShouldStall = enable;
    }
    if (!enable) {
        wake();
    }
}

void UnixFDStream::run()
{
    static constexpr std::size_t stallThreshold = 4194304;
    static constexpr std::size_t readBlockSize = 65536;

    struct ::pollfd pfds[2];
    ::memset(pfds, 0, sizeof(pfds));
    wake.install(pfds[0]);

    pfds[1].fd = fd;

    /* Wait to check these until reading starts, if it's enabled */
    if (!enableRead) {
        pfds[1].events |= POLLERR | POLLHUP | POLLNVAL;
    }


    Util::ByteArray dataToWrite;
    Util::ByteArray readBuffer;
    bool wasStalled = false;
    for (;;) {
        bool isLocked = false;
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (enableWrite) {
                dataToWrite += std::move(writeBuffer);
            }
            writeBuffer.clear();

            if (terminated && dataToWrite.empty()) {
                lockState = LockState::Unlocked;
                return;
            }

            if (enableRead) {
                if (readStarted && !readEnded) {
                    if (!readShouldStall) {
                        pfds[1].events |= POLLIN | POLLERR | POLLHUP | POLLNVAL;
#ifdef POLLRDHUP
                        pfds[1].events |= POLLRDHUP;
#endif
                    } else {
                        pfds[1].events &= ~(POLLIN | POLLERR | POLLHUP | POLLNVAL);
#ifdef POLLRDHUP
                        pfds[1].events &= ~POLLRDHUP;
#endif
                    }
                }
            }

            switch (lockState) {
            case LockState::Unlocked:
                break;
            case LockState::LockRequested:
                isLocked = true;
                lockState = LockState::Locked;
                external.notify_all();
                break;
            case LockState::Locked:
                isLocked = true;
                break;
            case LockState::LockedFlushWrite:
                isLocked = true;
                lockState = LockState::Locked;
                external.notify_all();
                dataToWrite.clear();
                writeBuffer.clear();
                break;
            }
        }

        if (!wasStalled) {
            if (dataToWrite.size() > stallThreshold) {
                wasStalled = true;
                writeStall(true);
            }
        } else {
            if (dataToWrite.size() < stallThreshold) {
                wasStalled = false;
                writeStall(false);
            }
        }

        if (!dataToWrite.empty()) {
            pfds[1].events |= POLLOUT;
        } else {
            pfds[1].events &= ~POLLOUT;
        }

        int timeout = -1;
        if (!isLocked && fd != -1 && (pfds[1].events != 0)) {
            bool reading = (pfds[1].events & POLLIN) != 0;
            bool writing = (pfds[1].events & POLLOUT) != 0;
            timeout = waitEntry(reading, writing);
            if (!reading) {
                pfds[1].events &= ~POLLIN;
            }
            if (!writing) {
                pfds[1].events &= ~POLLOUT;
            }
        }

        int rc = ::poll(pfds, (!isLocked && pfds[1].events != 0) ? 2 : 1, timeout);
        if (rc == 0) {
            continue;
        } else if (rc < 0) {
            int en = errno;
            if (en == EAGAIN || en == EINTR)
                continue;
            qCDebug(log_io_driver_unixfdstream) << "Poll failed:" << std::strerror(en);
            return unrecoverableFailure();
        }


        if (pfds[0].revents & POLLIN) {
            if (!wake.clear()) {
                return unrecoverableFailure();
            }
        }

        if (pfds[1].events == 0 || isLocked)
            continue;

        if ((pfds[1].revents & POLLIN) != 0) {
            readBuffer.resize(readBlockSize);
            auto n = ::read(fd, readBuffer.data(), readBuffer.size());
            if (n < 0) {
                int en = errno;
                if (en != EAGAIN && en != EINTR && en != EWOULDBLOCK) {
                    qCDebug(log_io_driver_unixfdstream) << "Read failed:" << std::strerror(en);
                    return unrecoverableFailure();
                }
            } else if (n == 0) {
                pfds[1].events &= ~POLLIN;
#ifdef POLLRDHUP
                pfds[1].events &= ~POLLRDHUP;
#endif
                generateReadEnd();
            } else {
                readBuffer.resize(n);
                dataRead(readBuffer);
            }
        }
#ifdef POLLRDHUP
        else if ((pfds[1].revents & POLLRDHUP) != 0) {
            qCDebug(log_io_driver_unixfdstream) << "Other end of stream closed";
            if (!enableWrite) {
                return unrecoverableFailure();
            } else {
                /* This could be a socket with the remote write end closed, so just end
                 * reading rather than aborting entirely */
                pfds[1].events &= ~(POLLIN | POLLRDHUP);
                generateReadEnd();
            }
        }
#endif
        else if ((pfds[1].revents & (POLLERR | POLLHUP | POLLNVAL)) != 0) {
            qCDebug(log_io_driver_unixfdstream) << "Other end of stream closed";
            return unrecoverableFailure();
        }

        if ((pfds[1].revents & POLLOUT) != 0) {
            auto n = ::write(fd, dataToWrite.data(), dataToWrite.size());
            if (n < 0) {
                int en = errno;
                if (en == EPIPE) {
                    /* Other end closed, just discard */
                    dataToWrite.clear();

                    /* No read end to detect on, so fail/end here */
                    if (!enableRead) {
                        qCDebug(log_io_driver_unixfdstream) << "Other end of stream closed";
                        return unrecoverableFailure();
                    }
                } else if (en != EAGAIN && en != EINTR && en != EWOULDBLOCK) {
                    qCDebug(log_io_driver_unixfdstream) << "Write failed:" << std::strerror(en);
                    return unrecoverableFailure();
                }
            } else if (n > 0) {
                dataToWrite.pop_front(n);
                dataWritten(n);
            }
        }
    }
}

void UnixFDStream::unrecoverableFailure()
{
    int sfd = fd;
    bool doEnd;
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (fd == -1)
            return;
        fd = -1;
        doEnd = readStarted && !readEnded;
        readEnded = true;
        terminated = true;
        lockState = LockState::Unlocked;
    }
    external.notify_all();
    if (ownFD) {
        NotifyWake::interruptLoop(::close, sfd);
    }
    if (doEnd) {
        ended();
    }
}

void UnixFDStream::generateReadEnd()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (readEnded)
            return;
        readEnded = true;
    }
    ended();
}

int UnixFDStream::waitEntry(bool &, bool &)
{ return -1; }

void UnixFDStream::dataRead(const Util::ByteArray &contents)
{ read(contents); }

void UnixFDStream::dataWritten(std::size_t)
{ }

UnixFDStream::Lock::Lock(UnixFDStream &strm) : stream(strm)
{
    {
        std::unique_lock<std::mutex> lock(stream.mutex);
        stream.external.wait(lock, [this] {
            return stream.terminated || stream.lockState == LockState::Unlocked;
        });
        if (stream.terminated)
            return;
        Q_ASSERT(stream.lockState == LockState::Unlocked);
        stream.lockState = LockState::LockRequested;
    }
    stream.wake();
    {
        std::unique_lock<std::mutex> lock(stream.mutex);
        stream.external.wait(lock, [this] {
            return stream.terminated || stream.lockState == LockState::Locked;
        });
        if (stream.terminated)
            return;
        Q_ASSERT(stream.lockState == LockState::Locked);
    }
}

UnixFDStream::Lock::~Lock()
{
    {
        std::lock_guard<std::mutex> lock(stream.mutex);
        if (stream.lockState != LockState::Locked)
            return;
        stream.lockState = LockState::Unlocked;
    }
    stream.external.notify_all();
    stream.wake();
}

int UnixFDStream::Lock::fd() const
{ return stream.fd; }

void UnixFDStream::Lock::flush(std::size_t maximumReadFlush)
{
    {
        std::unique_lock<std::mutex> lock(stream.mutex);
        if (stream.terminated)
            return;
        Q_ASSERT(stream.lockState == LockState::Locked);
        stream.lockState = LockState::LockedFlushWrite;
    }
    stream.wake();

    if (maximumReadFlush) {
        Util::ByteArray buffer;
        buffer.resize(4096);

        struct ::pollfd pfds[1];
        ::memset(pfds, 0, sizeof(pfds));
        pfds[0].fd = fd();
        pfds[0].events = POLLIN;

        while (maximumReadFlush != 0) {
            int rc = ::poll(pfds, 1, 0);
            if (rc == 0) {
                break;
            } else if (rc < 0) {
                int en = errno;
                if (en == EAGAIN || en == EINTR)
                    continue;
                qCDebug(log_io_driver_unixfdstream) << "Flush poll failed:" << std::strerror(en);
                break;
            }

            if ((pfds[0].revents & POLLIN) == 0)
                continue;

            auto maxrd = buffer.size();
            if (maximumReadFlush != static_cast<std::size_t>(-1))
                maxrd = std::min(maxrd, maximumReadFlush);

            auto n = ::read(pfds[0].fd, buffer.data(), maxrd);
            if (n < 0) {
                int en = errno;
                if (en != EAGAIN && en != EINTR && en != EWOULDBLOCK) {
                    qCDebug(log_io_driver_unixfdstream) << "Flush read failed:"
                                                        << std::strerror(en);
                    break;
                }
            } else if (n == 0) {
                qCDebug(log_io_driver_unixfdstream) << "Read ended while flushing";
                break;
            }

            if (maximumReadFlush != static_cast<std::size_t>(-1)) {
                Q_ASSERT(static_cast<std::size_t>(n) <= maximumReadFlush);
                maximumReadFlush -= n;
            }
        }
    }

    {
        std::unique_lock<std::mutex> lock(stream.mutex);
        stream.external.wait(lock, [this] {
            return stream.terminated || stream.lockState == LockState::Locked;
        });
        if (stream.terminated)
            return;
        Q_ASSERT(stream.lockState == LockState::Locked);
    }
}

}
}