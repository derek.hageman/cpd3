/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_DRIVERS_STDIO_HXX
#define CPD3IO_DRIVERS_STDIO_HXX

#include "core/first.hxx"

#include "io/io.hxx"
#include "io/access.hxx"

namespace CPD3 {
namespace IO {

namespace STDIO {

/**
 * Standard I/O handling backing.
 */
class CPD3IO_EXPORT Backing : public Generic::Backing {
public:
    virtual ~Backing();

    /**
     * Get a unique string that identifies the pipeline input, which will be matched
     * on the matching output end.
     *
     * @return the pipeline identity or empty if not available
     */
    virtual std::string pipelineInputIdentity() = 0;

    /**
     * Get a unique string that identifies the pipeline output, which will be matched
     * on the matching input end.
     *
     * @return the pipeline identity or empty if not available
     */
    virtual std::string pipelineOutputIdentity() = 0;

    /**
     * Get the output terminal width.
     *
     * @return  the terminal width in characters
     */
    virtual int terminalWidth();

    /**
     * Get the output terminal height.
     *
     * @return  the terminal height in lines
     */
    virtual int terminalHeight();
};

/**
 * The stream access for standard I/O.
 */
class CPD3IO_EXPORT Stream : public Generic::Stream {
public:
    virtual ~Stream();

    /**
     * Get a unique string that identifies the pipeline input, which will be matched
     * on the matching output end.
     *
     * @return the pipeline identity or empty if not available
     */
    virtual std::string pipelineInputIdentity() const = 0;

    /**
     * Get a unique string that identifies the pipeline output, which will be matched
     * on the matching input end.
     *
     * @return the pipeline identity or empty if not available
     */
    virtual std::string pipelineOutputIdentity() const = 0;

    /**
     * Get the output terminal width.
     *
     * @return  the terminal width in characters
     */
    virtual int terminalWidth();

    /**
     * Get the output terminal height.
     *
     * @return  the terminal height in lines
     */
    virtual int terminalHeight();
};

}

namespace Access {

/**
 * Create a stream only handle for standard input input and output.
 * <br>
 * Note that this does allow shared multiple streams, it does not synchronize state
 * between them.  So only one stream (the first) is guaranteed to receive all signals
 * and only a single stream may control state (stalls, etc).
 *
 * @param own   take ownership, closing stdio when the last reference is dropped
 * @param error write to stderr instead of stdout
 * @return      the access handle
 */
CPD3IO_EXPORT Handle stdio(bool own = true, bool error = false);

/**
 * Create a handle for standard input only.
 * <br>
 * Note that this does allow shared multiple streams, it does not synchronize state
 * between them.  So only one stream (the first) is guaranteed to receive all signals
 * and only a single stream may control state (stalls, etc).
 *
 * @param own   take ownership, closing standard input when the last reference is dropped
 * @return      the access handle
 */
CPD3IO_EXPORT Handle stdio_in(bool own = true);

/**
 * Create a handle for standard output only.
 * <br>
 * Note that this does allow shared multiple streams, it does not synchronize state
 * between them.  So only one stream (the first) is guaranteed to receive all signals
 * and only a single stream may control state (stalls, etc).
 *
 * @param own   take ownership, closing standard output when the last reference is dropped
 * @param error write to stderr instead of stdout
 * @return      the access handle
 */
CPD3IO_EXPORT Handle stdio_out(bool own = true, bool error = false);

}

}
}

#endif //CPD3IO_DRIVERS_STDIO_HXX
