/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <vector>
#include <mutex>
#include <condition_variable>
#include <QThread>
#include <QAbstractSocket>
#include <QLocalSocket>
#include <QTimer>
#include <QMetaObject>

#include "qio.hxx"
#include "qiowrapper.hxx"
#include "core/threading.hxx"

namespace CPD3 {
namespace IO {

namespace {

class QIOBacking : public Generic::Backing, public std::enable_shared_from_this<QIOBacking> {
protected:
    QIODevice &device;
    QIOWrapper wrapper;

    explicit QIOBacking(QIODevice &device) : device(device), wrapper(device)
    { }

public:

    QIOBacking() = delete;

    virtual ~QIOBacking() = default;

    std::unique_ptr<Generic::Stream> stream() override
    {
        class Stream final : public Generic::Stream {
            std::shared_ptr<QIOBacking> context;
            QIODevice &device;

            void transferredWrite(Util::ByteArray &&data)
            {
                struct Local {
                    std::shared_ptr<QIOBacking> context;
                    Util::ByteArray data;

                    Local(const std::shared_ptr<QIOBacking> &context, Util::ByteArray &&data)
                            : context(context), data(std::move(data))
                    { }
                };

                auto local = std::make_shared<Local>(context, std::move(data));
                Threading::runQueuedFunctor(&device, [local]() {
                    local->context->wrapper.write(std::move(local->data));
                    local->context->wrapper.processWriteBuffer();
                });
            }

        public:
            Stream(std::shared_ptr<QIOBacking> &&context, QIODevice &device) : context(
                    std::move(context)), device(device)
            {
                read = this->context->wrapper.read;
                ended = this->context->wrapper.ended;
                writeStall = this->context->wrapper.writeStall;
            }

            virtual ~Stream()
            {
                if (device.thread() == QThread::currentThread()) {
                    context->wrapper.drainWriteBuffer();
                } else {
                    Threading::runBlockingFunctor(&device, [this]() {
                        context->wrapper.drainWriteBuffer();
                    });
                }
            }

            void write(const Util::ByteView &data) override
            {
                if (device.thread() == QThread::currentThread()) {
                    context->wrapper.write(data);
                    context->wrapper.processWriteBuffer(true);
                    return;
                }
                transferredWrite(Util::ByteArray(data));
            }

            void write(Util::ByteArray &&data) override
            {
                if (device.thread() == QThread::currentThread()) {
                    context->wrapper.write(std::move(data));
                    context->wrapper.processWriteBuffer(true);
                    return;
                }
                transferredWrite(std::move(data));
            }

            void start() override
            {

                if (device.thread() == QThread::currentThread()) {
                    context->wrapper.start();
                    return;
                }
                auto ctx = context;
                Threading::runQueuedFunctor(&device, [ctx]() {
                    ctx->wrapper.start();
                });
            }

            bool isEnded() const override
            {
                if (device.thread() == QThread::currentThread())
                    return context->wrapper.isEnded();
                bool result = false;
                Threading::runBlockingFunctor(&device, [this, &result]() {
                    result = context->wrapper.isEnded();
                });
                return result;
            }

            void readStall(bool enable) override
            {
                if (device.thread() == QThread::currentThread())
                    return context->wrapper.readStall(enable);
                Threading::runBlockingFunctor(&device, [this, enable]() {
                    context->wrapper.readStall(enable);
                });
            }
        };
        return std::unique_ptr<Generic::Stream>(new Stream(shared_from_this(), device));
    }

    std::unique_ptr<Generic::Block> block() override
    {
        static constexpr std::size_t readBlockSize = 65536;

        class Block final : public Generic::Block {
            std::shared_ptr<QIOBacking> context;
            QIODevice &device;
            bool haveHadError;

            void doRead(Util::ByteArray *target, std::size_t maximum)
            {
                if (maximum != static_cast<std::size_t>(-1)) {
                    while (maximum) {
                        device.waitForReadyRead(-1);

                        auto original = target->size();
                        auto n = device.read(target->tail<char *>(maximum), maximum);
                        if (n < 0) {
                            haveHadError = true;
                            target->resize(original);
                            return;
                        } else if (n == 0) {
                            target->resize(original);
                            return;
                        }
                        Q_ASSERT(static_cast<std::size_t>(n) <= maximum);
                        target->resize(original + static_cast<std::size_t>(n));
                        maximum -= static_cast<std::size_t>(n);
                    }
                    return;
                }

                while (!device.atEnd()) {
                    device.waitForReadyRead(-1);

                    auto original = target->size();
                    auto n = device.read(target->tail<char *>(readBlockSize), readBlockSize);
                    if (n < 0) {
                        haveHadError = true;
                        target->resize(original);
                        return;
                    } else if (n == 0) {
                        target->resize(original);
                        return;
                    }
                    Q_ASSERT(static_cast<std::size_t>(n) <= readBlockSize);
                    target->resize(original + static_cast<std::size_t>(n));
                }
            }

            void doReadQ(QByteArray *target, int maximum)
            {
                if (maximum > 0) {
                    while (maximum) {
                        auto original = target->size();
                        target->resize(original + maximum);
                        auto n = device.read(target->data() + original, maximum);
                        if (n < 0) {
                            haveHadError = true;
                            target->resize(original);
                            return;
                        } else if (n == 0) {
                            target->resize(original);
                            return;
                        }
                        Q_ASSERT(n <= maximum);
                        target->resize(static_cast<int>(original + n));
                        maximum -= n;
                    }
                    return;
                }

                while (!device.atEnd()) {
                    auto original = target->size();
                    target->resize(static_cast<int>(original + readBlockSize));
                    auto n = device.read(target->data() + original, readBlockSize);
                    if (n < 0) {
                        haveHadError = true;
                        target->resize(original);
                        return;
                    } else if (n == 0) {
                        target->resize(original);
                        return;
                    }
                    Q_ASSERT(static_cast<std::size_t>(n) <= readBlockSize);
                    target->resize(static_cast<int>(original + n));
                }
            }

        public:
            Block(std::shared_ptr<QIOBacking> &&context, QIODevice &device) : context(context),
                                                                              device(device),
                                                                              haveHadError(false)
            { }

            virtual ~Block() = default;

            void write(const Util::ByteView &data) override
            {
                if (device.thread() == QThread::currentThread()) {
                    context->wrapper.write(data);
                    if (!context->wrapper.drainWriteBuffer()) {
                        haveHadError = true;
                    }
                    return;
                }
                Threading::runBlockingFunctor(&device, [this, &data]() {
                    context->wrapper.write(data);
                    if (!context->wrapper.drainWriteBuffer()) {
                        haveHadError = true;
                    }
                });
            }

            void write(Util::ByteArray &&data) override
            {
                if (device.thread() == QThread::currentThread()) {
                    context->wrapper.write(std::move(data));
                    if (!context->wrapper.drainWriteBuffer()) {
                        haveHadError = true;
                    }
                    return;
                }
                Threading::runBlockingFunctor(&device, [this, &data]() {
                    context->wrapper.write(std::move(data));
                    if (!context->wrapper.drainWriteBuffer()) {
                        haveHadError = true;
                    }
                });
            }

            bool inError() const override
            {
                if (device.thread() == QThread::currentThread())
                    return haveHadError;
                bool result = true;
                Threading::runBlockingFunctor(&device, [this, &result]() {
                    result = haveHadError;
                });
                return result;
            }

            std::string describeError() const override
            {
                if (device.thread() == QThread::currentThread())
                    return device.errorString().toStdString();
                QString str;
                Threading::runBlockingFunctor(&device, [this, &str]() {
                    str = device.errorString();
                });
                return str.toStdString();
            }


            bool readFinished() const override
            {
                if (device.thread() == QThread::currentThread())
                    return context->wrapper.isEnded();
                bool result = false;
                Threading::runBlockingFunctor(&device, [this, &result]() {
                    result = context->wrapper.isEnded();
                });
                return result;
            }

            void read(Util::ByteArray &target, std::size_t maximum) override
            {
                if (!maximum)
                    return;
                if (device.thread() == QThread::currentThread())
                    return doRead(&target, maximum);
                Threading::runBlockingFunctor(&device,
                                              std::bind(&Block::doRead, this, &target, maximum));
            }

            void read(QByteArray &target, int maximum = -1) override
            {
                if (!maximum)
                    return;
                if (device.thread() == QThread::currentThread())
                    return doReadQ(&target, maximum);
                Threading::runBlockingFunctor(&device,
                                              std::bind(&Block::doReadQ, this, &target, maximum));
            }


            void seek(std::size_t absolute) override
            {
                if (device.thread() == QThread::currentThread()) {
                    if (absolute == static_cast<std::size_t>(-1))
                        absolute = device.size();
                    device.seek(absolute);
                    return;
                }
                Threading::runBlockingFunctor(&device, [this, absolute]() {
                    auto pos = absolute;
                    if (pos == static_cast<std::size_t>(-1))
                        pos = device.size();
                    device.seek(pos);
                });
            }

            void move(std::ptrdiff_t delta) override
            {
                if (device.thread() == QThread::currentThread()) {
                    auto pos = device.pos() + delta;
                    if (pos < 0)
                        pos = 0;
                    device.seek(pos);
                    return;
                }
                Threading::runBlockingFunctor(&device, [this, delta]() {
                    auto pos = device.pos() + delta;
                    if (pos < 0)
                        pos = 0;
                    device.seek(pos);
                });
            }

            std::size_t tell() const override
            {
                if (device.thread() == QThread::currentThread())
                    return static_cast<std::size_t>(device.pos());
                std::size_t result = 0;
                Threading::runBlockingFunctor(&device, [this, &result]() {
                    result = static_cast<std::size_t>(device.pos());
                });
                return result;
            }
        };
        return std::unique_ptr<Generic::Block>(new Block(shared_from_this(), device));
    }
};

}

namespace Access {

Handle qio(std::shared_ptr<QIODevice> device)
{
    Q_ASSERT(device.get() != nullptr);

    class Backing final : public QIOBacking {
        std::shared_ptr<QIODevice> device;
    public:
        explicit Backing(std::shared_ptr<QIODevice> &&device) : QIOBacking(*device),
                                                                device(std::move(device))
        { }

        virtual ~Backing()
        { wrapper.destroy(false); }
    };
    return std::make_shared<Backing>(std::move(device));
}

Handle qio(std::unique_ptr<QIODevice> &&device)
{
    Q_ASSERT(device.get() != nullptr);

    class Backing final : public QIOBacking {
        std::unique_ptr<QIODevice> device;
    public:
        explicit Backing(std::unique_ptr<QIODevice> &&device) : QIOBacking(*device),
                                                                device(std::move(device))
        { }

        virtual ~Backing()
        {
            wrapper.destroy(false);
            device.release()->deleteLater();
        }
    };
    return std::make_shared<Backing>(std::move(device));
}

Handle qio(QIODevice *device)
{
    Q_ASSERT(device != nullptr);

    class Backing final : public QIOBacking {
    public:
        explicit Backing(QIODevice *device) : QIOBacking(*device)
        { }

        virtual ~Backing()
        { wrapper.destroy(false); }
    };
    return std::make_shared<Backing>(device);
}

Handle qio(const std::function<std::unique_ptr<QIODevice>()> &create)
{
    struct Result {
        const std::function<std::unique_ptr<QIODevice>()> &create;

        std::mutex mutex;
        std::condition_variable notify;
        bool ready;

        Handle handle;
        std::unique_ptr<QThread> thread;

        explicit Result(const std::function<std::unique_ptr<QIODevice>()> &create) : create(create),
                                                                                     ready(false)
        { }
    };

    Result result(create);

    class Backing final : public QIOBacking {
        std::unique_ptr<QThread> thread;
    public:
        Backing(QIODevice *device, std::unique_ptr<QThread> &&thread) : QIOBacking(*device),
                                                                        thread(std::move(thread))
        { }

        virtual ~Backing()
        {
            if (device.thread() != QThread::currentThread()) {
                Threading::runBlockingFunctor(&device, [this]() { wrapper.destroy(); });
                thread->quit();
                thread->wait();
            } else {
                /*
                 * This can happen if the last reference to the shared pointer is via a
                 * queued call.
                 */

                wrapper.destroy();
                thread->quit();

                /*
                 * Note the context is the thread, which will be owned by another thread, so
                 * we rely on that parent thread's event loop to do the delete.
                 */
                Q_ASSERT(thread->thread() != QThread::currentThread());
                auto thr = std::make_shared<std::unique_ptr<QThread>>(std::move(thread));
                Threading::runQueuedFunctor(thr->get(), [thr]() {
                    (*thr)->wait();
                    thr->reset();
                });
            }
        }
    };

    class Thread final : public QThread {
        Result *result;
        std::unique_ptr<QIODevice> device;
    public:
        explicit Thread(Result *result) : result(result)
        { setObjectName("QIODedicatedThread"); }

        virtual ~Thread() = default;

    protected:
        void run() override
        {
            device = result->create();

            if (!device) {
                {
                    std::lock_guard<std::mutex> lock(result->mutex);
                    result->ready = true;
                    result->notify.notify_all();
                }
                result = nullptr;
                return;
            }

            result->handle = std::make_shared<Backing>(device.get(), std::move(result->thread));

            {
                std::lock_guard<std::mutex> lock(result->mutex);
                result->ready = true;
                result->notify.notify_all();
            }
            result = nullptr;

            QThread::exec();

            device.reset();
        }
    };

    result.thread = std::unique_ptr<QThread>(new Thread(&result));
    result.thread->start();

    {
        std::unique_lock<std::mutex> lock(result.mutex);
        result.notify.wait(lock, [&result]() { return result.ready; });
    }
    Q_ASSERT(result.ready);

    if (result.thread) {
        result.thread->wait();
    }

    return std::move(result.handle);
}

}

}
}