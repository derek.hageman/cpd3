/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <memory>
#include "stdio.hxx"

#ifdef Q_OS_UNIX

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#ifdef Q_OS_LINUX

#include <sys/ioctl.h>

#endif

#include "unixfdstream.hxx"

#endif


namespace CPD3 {
namespace IO {

namespace STDIO {

Backing::~Backing() = default;

int Backing::terminalWidth()
{ return -1; }

int Backing::terminalHeight()
{ return -1; }

Stream::~Stream() = default;

int Stream::terminalWidth()
{ return -1; }

int Stream::terminalHeight()
{ return -1; }

}

namespace Access {

#ifdef Q_OS_UNIX

/* This is set if we can uniquely identify pipes by their stat'ed inode and
 * device.
 *
 * This is a work around for pipes initiated by a script that output to that
 * script's standard output.  The interpreter will create an intermediate pipe
 * that isolates the script's standard output (which won't close until the
 * script finishes), that means our normal method of detecting the end never
 * goes through (since the script process keeps waiting so it never ends).
 *
 * The conditions this is true may not always work, but it should be okay
 * on Linux and BSD (so OSX). */
static std::string pipelineIdentity(UnixFDStream &stream)
{
    UnixFDStream::Lock lock(stream);
    if (lock.fd() < 0)
        return {};

    struct ::stat sb;
    if (::fstat(lock.fd(), &sb) != 0)
        return {};

    std::string token;
    token += std::to_string(sb.st_dev);
    token += "-";
    token += std::to_string(sb.st_ino);
    return token;
}

static int getTerminalWidth(UnixFDStream &stream)
{
    UnixFDStream::Lock lock(stream);
    if (lock.fd() < 0)
        return -1;
#ifdef Q_OS_LINUX
    {
        struct ::winsize w;
        if (::ioctl(lock.fd(), TIOCGWINSZ, &w) == 0) {
            return w.ws_col;
        }
    }
#endif
    if (::isatty(lock.fd())) {
        QByteArray rowEnv(qgetenv("COLUMNS"));
        if (rowEnv.length() > 0) {
            bool ok = false;
            int n = rowEnv.toInt(&ok);
            if (ok && n > 0)
                return n;
        }
    }
    return -1;
}

static int getTerminalHeight(UnixFDStream &stream)
{
    UnixFDStream::Lock lock(stream);
    if (lock.fd() < 0)
        return -1;
#ifdef Q_OS_LINUX
    {
        struct ::winsize w;
        if (::ioctl(lock.fd(), TIOCGWINSZ, &w) == 0) {
            return w.ws_row;
        }
    }
#endif
    if (::isatty(lock.fd())) {
        QByteArray rowEnv(qgetenv("ROWS"));
        if (rowEnv.length() > 0) {
            bool ok = false;
            int n = rowEnv.toInt(&ok);
            if (ok && n > 0)
                return n;
        }
    }
    return -1;
}

Handle stdio(bool own, bool error)
{
    class Backing : public STDIO::Backing, public std::enable_shared_from_this<Backing> {
        UnixFDStream in;
        UnixFDStream out;

        class Stream : public STDIO::Stream {
            std::shared_ptr<Backing> context;
        public:
            explicit Stream(std::shared_ptr<Backing> &&context) : context(std::move(context))
            {
                read = this->context->in.read;
                ended = this->context->in.ended;
                writeStall = this->context->out.writeStall;
            }

            virtual ~Stream() = default;

            void write(const Util::ByteView &data) override
            { return context->out.write(data); }

            void write(Util::ByteArray &&data) override
            { return context->out.write(std::move(data)); }

            void start() override
            { return context->in.start(); }

            bool isEnded() const override
            { return context->in.isEnded(); }

            void readStall(bool enable) override
            { return context->in.readStall(enable); }

            std::string pipelineInputIdentity() const override
            { return context->pipelineInputIdentity(); }

            std::string pipelineOutputIdentity() const override
            { return context->pipelineOutputIdentity(); }

            int terminalWidth() override
            { return context->terminalWidth(); }

            int terminalHeight() override
            { return context->terminalHeight(); }
        };

        friend class Stream;

    public:
        Backing(bool own, bool error) : in(0, true, false, own),
                                        out(error ? 2 : 1, false, true, own)
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        { return std::unique_ptr<Generic::Stream>(new Stream(shared_from_this())); }

        std::unique_ptr<Generic::Block> block() override
        { return {}; }

        std::string pipelineInputIdentity() override
        { return pipelineIdentity(in); }

        std::string pipelineOutputIdentity() override
        { return pipelineIdentity(out); }

        int terminalWidth() override
        { return getTerminalWidth(out); }

        int terminalHeight() override
        { return getTerminalHeight(out); }
    };
    return std::make_shared<Backing>(own, error);
}

Handle stdio_in(bool own)
{
    class Backing : public STDIO::Backing, public std::enable_shared_from_this<Backing> {
        UnixFDStream fd;

        class Stream : public STDIO::Stream {
            std::shared_ptr<Backing> context;
        public:
            explicit Stream(std::shared_ptr<Backing> &&context) : context(std::move(context))
            {
                read = this->context->fd.read;
                ended = this->context->fd.ended;
                writeStall = this->context->fd.writeStall;
            }

            virtual ~Stream() = default;

            void write(const Util::ByteView &) override
            { }

            void write(const Util::ByteArray &) override
            { }

            void write(Util::ByteArray &&) override
            { }

            void write(const QByteArray &) override
            { }

            void write(QByteArray &&) override
            { }

            void start() override
            { return context->fd.start(); }

            bool isEnded() const override
            { return context->fd.isEnded(); }

            void readStall(bool enable) override
            { return context->fd.readStall(enable); }

            std::string pipelineInputIdentity() const override
            { return context->pipelineInputIdentity(); }

            std::string pipelineOutputIdentity() const override
            { return context->pipelineOutputIdentity(); }
        };

        friend class Stream;

    public:
        explicit Backing(bool own) : fd(0, true, false, own)
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        { return std::unique_ptr<Generic::Stream>(new Stream(shared_from_this())); }

        std::unique_ptr<Generic::Block> block() override
        { return {}; }

        std::string pipelineInputIdentity() override
        { return pipelineIdentity(fd); }

        std::string pipelineOutputIdentity() override
        { return {}; }
    };
    return std::make_shared<Backing>(own);
}

Handle stdio_out(bool own, bool error)
{
    class Backing : public STDIO::Backing, public std::enable_shared_from_this<Backing> {
        UnixFDStream fd;

        class Stream : public STDIO::Stream {
            std::shared_ptr<Backing> context;
        public:
            explicit Stream(std::shared_ptr<Backing> &&context) : context(std::move(context))
            {
                read = this->context->fd.read;
                ended = this->context->fd.ended;
                writeStall = this->context->fd.writeStall;
            }

            virtual ~Stream() = default;

            void write(const Util::ByteView &data) override
            { return context->fd.write(data); }

            void write(Util::ByteArray &&data) override
            { return context->fd.write(std::move(data)); }

            void start() override
            { }

            bool isEnded() const override
            { return false; }

            std::string pipelineInputIdentity() const override
            { return context->pipelineInputIdentity(); }

            std::string pipelineOutputIdentity() const override
            { return context->pipelineOutputIdentity(); }

            int terminalWidth() override
            { return context->terminalWidth(); }

            int terminalHeight() override
            { return context->terminalHeight(); }
        };

        friend class Stream;

    public:
        Backing(bool own, bool error) : fd(error ? 2 : 1, false, true, own)
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        { return std::unique_ptr<Generic::Stream>(new Stream(shared_from_this())); }

        std::unique_ptr<Generic::Block> block() override
        { return {}; }

        std::string pipelineInputIdentity() override
        { return {}; }

        std::string pipelineOutputIdentity() override
        { return pipelineIdentity(fd); }

        int terminalWidth() override
        { return getTerminalWidth(fd); }

        int terminalHeight() override
        { return getTerminalHeight(fd); }
    };
    return std::make_shared<Backing>(own, error);
}


#else

Handle stdio(bool, bool)
{ return Access::noop(); }

Handle stdio_in(bool own)
{ return Access::noop(); }

Handle stdio_out(bool, bool)
{ return Access::noop(); }

#endif

}
}
}