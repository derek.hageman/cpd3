/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <QThread>
#include <QLoggingCategory>
#include <QHostInfo>
#include <QTcpServer>
#include <QTimer>

#include "tcp.hxx"
#include "core/qtcompat.hxx"

Q_LOGGING_CATEGORY(log_io_driver_tcp, "cpd3.io.driver.tcp", QtWarningMsg)

namespace CPD3 {
namespace IO {
namespace Socket {
namespace TCP {

Configuration::Configuration() : port(port_any)
{ }

Configuration::~Configuration() = default;

Configuration::Configuration(std::uint16_t port, std::function<bool(QSslSocket &)> tls) : port(
        port), tls(std::move(tls))
{ }

Configuration::Configuration(std::function<bool(QSslSocket &socket)> tls) : port(port_any),
                                                                            tls(std::move(tls))
{ }


Connection::Connection(QTcpSocket &socket, const Configuration &) : QtConnection(socket),
                                                                    socket(socket)
{ }

Connection::~Connection() = default;

QSslCertificate Connection::peerCertificate() const
{ return QSslCertificate(); }

std::uint16_t Connection::peerPort() const
{ return socket.peerPort(); }

static std::unique_ptr<Connection> connect(const std::function<
        void(QTcpSocket &socket)> &startConnection,
                                           std::string peerName,
                                           const Configuration &config,
                                           bool synchronous)
{
    struct Result {
        const std::function<void(QTcpSocket &socket)> &startConnection;
        std::string peerName;
        const Configuration &config;
        bool synchronous;

        std::mutex mutex;
        std::condition_variable notify;
        bool ready;

        std::unique_ptr<Connection> connection;
        std::unique_ptr<QThread> thread;

        Result(const std::function<void(QTcpSocket &socket)> &startConnection,
               std::string peerName,
               const Configuration &config,
               bool synchronous) : startConnection(startConnection),
                                   peerName(std::move(peerName)),
                                   config(config),
                                   synchronous(synchronous),
                                   ready(false)
        { }
    };

    Result result(startConnection, std::move(peerName), config, synchronous);

    class OutgoingConnection : public Connection {
        std::string peerName;
        std::uint16_t targetPort;
        std::unique_ptr<QThread> thread;
        std::unique_ptr<QTimer> startupTimeout;

        enum class ReadyState {
            Connecting, Ready, Failed, Shutdown
        } readyState;
        std::function<void(bool isOk)> readyFunc;

    protected:
        std::vector<QMetaObject::Connection> startupConnections;

    public:
        QHostAddress socketAddress;

        OutgoingConnection(QTcpSocket &socket, Result &result) : Connection(socket, result.config),
                                                                 peerName(std::move(
                                                                         result.peerName)),
                                                                 targetPort(result.config.port),
                                                                 thread(std::move(result.thread)),
                                                                 startupTimeout(new QTimer),
                                                                 readyState(ReadyState::Connecting)
        {
            startupConnections.emplace_back(
                    QObject::connect(&socket, &QAbstractSocket::stateChanged,
                                     std::bind(&OutgoingConnection::checkFailed, this)));

            startupTimeout->setSingleShot(true);
            startupConnections.emplace_back(
                    QObject::connect(startupTimeout.get(), &QTimer::timeout, &socket,
                                     &QIODevice::close, Qt::DirectConnection));
            startupTimeout->start(30000);
        }

        virtual ~OutgoingConnection() = default;

        void shutdown()
        {
            if (!thread) {
                readyState = ReadyState::Shutdown;
                for (auto &c : startupConnections) {
                    QObject::disconnect(c);
                }
                startupConnections.clear();
                return;
            }
            if (socket.thread() != QThread::currentThread()) {
                Threading::runBlockingFunctor(&socket, [this]() {
                    qCDebug(log_io_driver_tcp) << "Connection to" << describePeer()
                                               << "shutting down";

                    readyState = ReadyState::Shutdown;
                    for (auto &c : startupConnections) {
                        QObject::disconnect(c);
                    }
                    startupConnections.clear();
                    startupTimeout.reset();
                    destroy();
                });
                thread->quit();
                thread->wait();
            } else {
                /*
                 * This can happen if the last reference to the shared pointer is via a
                 * queued call.
                 */

                qCDebug(log_io_driver_tcp) << "Connection to" << describePeer() << "shutting down";
                readyState = ReadyState::Shutdown;
                for (auto &c : startupConnections) {
                    QObject::disconnect(c);
                }
                startupConnections.clear();
                startupTimeout.reset();
                destroy();
                thread->quit();

                /*
                 * Note the context is the thread, which will be owned by another thread, so
                 * we rely on that parent thread's event loop to do the delete.
                 */
                Q_ASSERT(thread->thread() != QThread::currentThread());
                auto thr = std::make_shared<std::unique_ptr<QThread>>(std::move(thread));
                Threading::runQueuedFunctor(thr->get(), [thr]() {
                    (*thr)->wait();
                    thr->reset();
                });
            }
        }

        void checkFailed()
        {
            switch (socket.state()) {
            case QAbstractSocket::UnconnectedState:
            case QAbstractSocket::ClosingState:
                if (readyState != ReadyState::Connecting)
                    break;
                readyState = ReadyState::Failed;
                qCDebug(log_io_driver_tcp) << "Connection to" << describePeer()
                                           << "closed during startup:" << socket.errorString();

                for (auto &c : startupConnections) {
                    QObject::disconnect(c);
                }
                startupConnections.clear();
                startupTimeout.reset();

                if (readyFunc) {
                    readyFunc(false);
                    readyFunc = std::function<void(bool isOk)>();
                }
                break;
            default:
                break;
            }
        }

        void runOnConnection(std::function<void(bool isOk)> func)
        {
            switch (readyState) {
            case ReadyState::Connecting:
                break;
            case ReadyState::Ready:
                func(true);
                return;
            case ReadyState::Failed:
            case ReadyState::Shutdown:
                func(false);
                return;
            }

            readyFunc = std::move(func);
        }

        std::unique_ptr<QThread> releaseThread()
        { return std::move(thread); }

        std::string describePeer() const override
        { return peerName; }

        QHostAddress peerAddress() const override
        { return socketAddress; }

        std::uint16_t peerPort() const override
        { return targetPort; }


        void connectionReadyToUse()
        {
            if (readyState != ReadyState::Connecting)
                return;
            readyState = ReadyState::Ready;

            for (auto &c : startupConnections) {
                QObject::disconnect(c);
            }
            startupConnections.clear();
            startupTimeout.reset();

            if (readyFunc) {
                readyFunc(true);
                readyFunc = std::function<void(bool isOk)>();
            }
        }
    };

    class OutgoingConnectionTCP final : public OutgoingConnection {
        QHostAddress boundAddress;
    public:
        void socketStateChanged()
        {
            if (socket.state() != QAbstractSocket::ConnectedState)
                return;
            qCDebug(log_io_driver_tcp) << "TCP connection to" << describePeer() << "ready";
            connectionReadyToUse();
            boundAddress = socket.localAddress();
        }

        OutgoingConnectionTCP(QTcpSocket &socket, Result &result) : OutgoingConnection(socket,
                                                                                       result)
        {
            startupConnections.emplace_back(
                    QObject::connect(&socket, &QAbstractSocket::stateChanged,
                                     std::bind(&OutgoingConnectionTCP::socketStateChanged, this)));
        }

        virtual ~OutgoingConnectionTCP()
        { shutdown(); }

        QHostAddress localAddress() const override
        { return boundAddress; }
    };

    class OutgoingConnectionTLS final : public OutgoingConnection {
        bool encryptionStarted;
        QSslCertificate certificate;
        QHostAddress boundAddress;
    public:
        void socketStateChanged()
        {
            if (encryptionStarted)
                return;
            if (socket.state() != QAbstractSocket::ConnectedState)
                return;
            encryptionStarted = true;
            auto ssl = static_cast<QSslSocket *>(&socket);

            qCDebug(log_io_driver_tcp) << "Starting TLS client encryption to" << describePeer();
            ssl->startClientEncryption();
            boundAddress = socket.localAddress();
            socketEncrypted();
        }

        void socketEncrypted()
        {
            auto ssl = static_cast<QSslSocket *>(&socket);
            if (!ssl->isEncrypted())
                return;
            certificate = ssl->peerCertificate();

            qCDebug(log_io_driver_tcp) << "TLS connection to" << describePeer() << "ready";
            connectionReadyToUse();
        }

        OutgoingConnectionTLS(QSslSocket &socket, Result &result) : OutgoingConnection(socket,
                                                                                       result),
                                                                    encryptionStarted(false)
        {
            startupConnections.emplace_back(
                    QObject::connect(&socket, &QAbstractSocket::stateChanged,
                                     std::bind(&OutgoingConnectionTLS::socketStateChanged, this)));
            startupConnections.emplace_back(QObject::connect(&socket, &QSslSocket::encrypted,
                                                             std::bind(
                                                                     &OutgoingConnectionTLS::socketEncrypted,
                                                                     this)));
        }

        virtual ~OutgoingConnectionTLS()
        { shutdown(); }

        QSslCertificate peerCertificate() const override
        { return certificate; }

        QHostAddress localAddress() const override
        { return boundAddress; }
    };

    class Thread final : public QThread {
        Result *result;
        std::unique_ptr<QTcpSocket> socket;

        void resultReady()
        {
            {
                std::lock_guard<std::mutex> lock(result->mutex);
                result->ready = true;
                result->notify.notify_all();
            }
            result = nullptr;
        }

    public:
        explicit Thread(Result *result) : result(result)
        { setObjectName("TCPClientQThread"); }

        virtual ~Thread() = default;

    protected:
        void run() override
        {
            std::unique_ptr<OutgoingConnection> connection;
            if (result->config.tls) {
                auto ssl = new QSslSocket;
                socket.reset(ssl);
                if (!result->config.tls(*ssl))
                    return resultReady();
                connection.reset(new OutgoingConnectionTLS(*ssl, *result));
            } else {
                socket.reset(new QTcpSocket);
                connection.reset(new OutgoingConnectionTCP(*socket, *result));
            }

            result->startConnection(*socket);
            connection->socketAddress = socket->peerAddress();
            connection->checkFailed();

            if (result->synchronous) {
                qCDebug(log_io_driver_tcp) << "Synchronous connection to"
                                           << connection->describePeer() << "started";
                connection->runOnConnection([this, &connection](bool isOk) {
                    if (!isOk) {
                        qCDebug(log_io_driver_tcp) << "Synchronous connection failed:"
                                                   << socket->errorString();

                        result->thread = connection->releaseThread();
                        QThread::quit();
                        return;
                    }

                    qCDebug(log_io_driver_tcp) << "Synchronous connection to"
                                               << connection->describePeer() << "completed";
                    result->connection = std::move(connection);
                    resultReady();
                });
            } else {
                qCDebug(log_io_driver_tcp) << "Asynchronous connection to"
                                           << connection->describePeer() << "started";
                result->connection = std::move(connection);
                resultReady();
            }

            QThread::exec();

            connection.reset();
            socket.reset();

            if (result)
                return resultReady();
        }
    };

    result.thread = std::unique_ptr<QThread>(new Thread(&result));
    result.thread->start();

    {
        std::unique_lock<std::mutex> lock(result.mutex);
        result.notify.wait(lock, [&result]() { return result.ready; });
    }
    Q_ASSERT(result.ready);

    if (result.thread) {
        result.thread->wait();
    }

    return std::move(result.connection);
}

std::unique_ptr<Connection> connect(const QHostAddress &address,
                                    const Configuration &config,
                                    bool synchronous)
{
    auto peerName = address.toString().toStdString();
    if (address.protocol() == QAbstractSocket::IPv6Protocol)
        peerName = "[" + peerName + "]";
    return connect([&address, &config](QTcpSocket &socket) {
        qCDebug(log_io_driver_tcp) << "Connecting to" << address << "port" << config.port;
        return socket.connectToHost(address, config.port,
                                    QIODevice::ReadWrite | QIODevice::Unbuffered);
    }, peerName, config, synchronous);
}

std::unique_ptr<Connection> connect(const std::string &address,
                                    const Configuration &config,
                                    bool synchronous)
{
    auto connectAddr = QString::fromStdString(address);
    return connect([&connectAddr, &config](QTcpSocket &socket) {
        qCDebug(log_io_driver_tcp) << "Connecting to" << connectAddr << "port" << config.port;
        return socket.connectToHost(connectAddr, config.port,
                                    QIODevice::ReadWrite | QIODevice::Unbuffered);
    }, address, config, synchronous);
}

std::unique_ptr<Connection> connect(const QHostAddress &address,
                                    const Configuration &config,
                                    const QHostAddress &local,
                                    bool synchronous)
{
    auto peerName = address.toString().toStdString();
    if (address.protocol() == QAbstractSocket::IPv6Protocol)
        peerName = "[" + peerName + "]";
    return connect([&address, &config, &local](QTcpSocket &socket) {
        qCDebug(log_io_driver_tcp) << "Connecting to" << address << "port" << config.port << "from"
                                   << local;
        if (!socket.bind(local))
            return;
        return socket.connectToHost(address, config.port,
                                    QIODevice::ReadWrite | QIODevice::Unbuffered);
    }, peerName, config, synchronous);
}

std::unique_ptr<Connection> connect(const std::string &address,
                                    const Configuration &config,
                                    const std::string &local,
                                    bool synchronous)
{
    auto connectAddr = QString::fromStdString(address);
    auto bindAddr = QHostAddress(QString::fromStdString(local));
    return connect([&connectAddr, &config, &bindAddr](QTcpSocket &socket) {
        qCDebug(log_io_driver_tcp) << "Connecting to" << connectAddr << "port" << config.port
                                   << "from" << bindAddr;
        if (!socket.bind(bindAddr))
            return;
        return socket.connectToHost(connectAddr, config.port,
                                    QIODevice::ReadWrite | QIODevice::Unbuffered);
    }, address, config, synchronous);
}

std::unique_ptr<Connection> loopback(const Configuration &config, bool synchronous)
{
#ifndef QT_NO_IPV4
    return connect([&config](QTcpSocket &socket) {
        qCDebug(log_io_driver_tcp) << "Connecting to loopback interface port" << config.port;
        return socket.connectToHost(QHostAddress(QHostAddress::LocalHost), config.port,
                                    QIODevice::ReadWrite | QIODevice::Unbuffered);
    }, "localhost", config, synchronous);
#else
    return connect([&config](QTcpSocket &socket) {
        qCDebug(log_io_driver_tcp) << "Connecting to loopback interface port" << config.port;
        return socket.connectToHost(QHostAddress(QHostAddress::LocalHostIPv6), config.port,
                                    QIODevice::ReadWrite | QIODevice::Unbuffered);
    }, "localhost", config, synchronous);
#endif
}


class Server::Worker final : public std::enable_shared_from_this<Server::Worker> {
    IncomingConnection incoming;
    Configuration config;
    std::vector<std::string> lookup;
    std::vector<QHostAddress> address;

    std::unique_ptr<QThread> thread;
    QObject *receiver;

    std::mutex mutex;
    std::condition_variable notify;
    std::size_t remainingResolvers;
    std::size_t failedResolvers;

    bool serversStarted;
    bool serversClosed;

    std::unordered_map<QTcpSocket *, std::unique_ptr<Connection>> handshakeConnections;

    class TCPServer final : public QTcpServer {
    public:
        TCPServer()
        { }

        virtual ~TCPServer() = default;

    protected:
        void incomingConnection(qintptr socketDescriptor) override
        {
            auto socket = new QTcpSocket(this);
            socket->setSocketDescriptor(socketDescriptor, QAbstractSocket::ConnectedState,
                                        QIODevice::ReadWrite | QIODevice::Unbuffered);
            return addPendingConnection(socket);
        }
    };

    class TLSServer final : public QTcpServer {
        Worker &parent;
    public:
        explicit TLSServer(Worker &parent) : parent(parent)
        { }

        virtual ~TLSServer() = default;

    protected:
        void incomingConnection(qintptr socketDescriptor) override
        {
            Q_ASSERT(parent.config.tls);

            auto socket = new QSslSocket(this);
            socket->setSocketDescriptor(socketDescriptor, QAbstractSocket::ConnectedState,
                                        QIODevice::ReadWrite | QIODevice::Unbuffered);
            if (!parent.config.tls(*socket)) {
                socket->close();
                delete socket;
                return;
            }

            /* TLS started when the connection is accepted */

            return addPendingConnection(socket);
        }
    };

    std::vector<std::unique_ptr<QTcpServer>> servers;

    void startup()
    {
        struct Result {
            std::mutex mutex;
            std::condition_variable notify;
            bool ready;

            QObject *receiver;

            Result() : ready(false), receiver(nullptr)
            { }
        };

        Result result;

        class Thread final : public QThread {
            Result *result;
            std::unique_ptr<QObject> receiver;

            void resultReady()
            {
                {
                    std::lock_guard<std::mutex> lock(result->mutex);
                    result->ready = true;
                    result->notify.notify_all();
                }
                result = nullptr;
            }

        public:
            explicit Thread(Result *result) : result(result)
            { setObjectName("TCPServerQThread"); }

            virtual ~Thread() = default;

        protected:
            void run() override
            {
                receiver.reset(new QObject);

                result->receiver = receiver.get();
                resultReady();

                QThread::exec();

                receiver.reset();
            }
        };

        thread = std::unique_ptr<QThread>(new Thread(&result));
        thread->start();

        {
            std::unique_lock<std::mutex> lock(result.mutex);
            result.notify.wait(lock, [&result]() { return result.ready; });
        }
        Q_ASSERT(result.ready);

        Q_ASSERT(result.receiver);
        receiver = result.receiver;

        startResolving();
    }

    void shutdown()
    {
        Q_ASSERT(receiver->thread() == QThread::currentThread());

        for (const auto &srv : servers) {
            srv->close();
        }
        servers.clear();

        /*
         * This can only be called when there are no more connections (handshake or active),
         * since they keep a pointer to us.  So we don't have to worry about destroying them or the
         * servers destroying their sockets, since they're parented to the servers.
         */
        Q_ASSERT(handshakeConnections.empty());
    }

    std::string getDescription() const
    {
        Q_ASSERT(receiver);
        Q_ASSERT(receiver->thread() == QThread::currentThread());

        if (address.size() == 1) {
            auto name = address.front().toString().toStdString();
            if (address.front().protocol() == QAbstractSocket::IPv6Protocol)
                name = "[" + name + "]";
            return name + ":" + std::to_string(localPort());
        }
        if (!address.empty()) {
            std::string name;
            for (const auto &addr : address) {
                if (!name.empty())
                    name += ",";
                name += addr.toString().toStdString();
            }
            return "[" + name + "]:" + std::to_string(localPort());
        }
        if (lookup.size() == 1) {
            return lookup.front() + ":" + std::to_string(localPort());
        }
        if (!lookup.empty()) {
            std::string name;
            for (const auto &host : lookup) {
                if (!name.empty())
                    name += ",";
                name += host;
            }
            return "[" + name + "]:" + std::to_string(localPort());
        }
        return "[]";
    }

    void startResolving()
    {
        for (const auto &add : lookup) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                remainingResolvers++;
            }
            Socket::queuedResolve(add, receiver, [this, add](const QHostInfo &host) {
                for (const auto &addr : host.addresses()) {
                    address.emplace_back(addr);
                }
                if (host.error() != QHostInfo::NoError) {
                    qCDebug(log_io_driver_tcp) << "Name resolution for" << add << "failed:"
                                               << host.errorString();
                    std::lock_guard<std::mutex> lock(mutex);
                    failedResolvers++;
                }
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    remainingResolvers--;
                }
                notify.notify_all();
            });
        }
    }

    void attachSocket(QTcpSocket *socket)
    {
        class IncomingConnection : public Connection {
            QTcpSocket *ownedSocket;
            std::string peerName;
            std::uint16_t targetPort;
            QHostAddress boundAddress;
            std::shared_ptr<Worker> worker;
            std::unique_ptr<QTimer> startupTimeout;

            enum class ReadyState {
                Connecting, Ready, Failed, Shutdown
            } readyState;

            static std::string toPeerName(QTcpSocket &socket)
            {
                auto addr = socket.peerAddress();
                auto peerName = addr.toString().toStdString();
                if (addr.protocol() == QAbstractSocket::IPv6Protocol)
                    peerName = "[" + peerName + "]";
                peerName += ":" + std::to_string(socket.peerPort());
                return peerName;
            }

        protected:
            std::vector<QMetaObject::Connection> startupConnections;

        public:
            QHostAddress socketAddress;

            IncomingConnection(QTcpSocket *socket, std::shared_ptr<Worker> worker) : Connection(
                    *socket, worker->config),
                                                                                     ownedSocket(
                                                                                             socket),
                                                                                     peerName(
                                                                                             toPeerName(
                                                                                                     *socket)),
                                                                                     targetPort(
                                                                                             socket->peerPort()),
                                                                                     boundAddress(
                                                                                             socket->localAddress()),
                                                                                     worker(std::move(
                                                                                             worker)),
                                                                                     startupTimeout(
                                                                                             new QTimer),
                                                                                     readyState(
                                                                                             ReadyState::Connecting)
            {
                startupConnections.emplace_back(
                        QObject::connect(socket, &QAbstractSocket::stateChanged,
                                         std::bind(&IncomingConnection::checkFailed, this)));

                startupTimeout->setSingleShot(true);
                startupConnections.emplace_back(
                        QObject::connect(startupTimeout.get(), &QTimer::timeout, socket,
                                         &QIODevice::close, Qt::DirectConnection));
                startupTimeout->start(30000);
            }

            virtual ~IncomingConnection() = default;

            void shutdown()
            {
                if (socket.thread() != QThread::currentThread()) {
                    /* Only possible after startup has completed (i.e. connection removed
                     * from handshake) */
                    Q_ASSERT(startupConnections.empty());

                    Threading::runBlockingFunctor(&socket, [this]() {
                        startupTimeout.reset();

                        qCDebug(log_io_driver_tcp) << "Connection to" << describePeer()
                                                   << "shutting down";
                        readyState = ReadyState::Shutdown;
                        destroy();

                        /* Queue the socket for deletion by the worker thread, since we now have the
                         * only reference to it */
                        ownedSocket->deleteLater();
                    });
                } else {
                    /*
                     * This can happen if the last reference to the shared pointer is via a
                     * queued call or if the socket fails during handshake.
                     */

                    qCDebug(log_io_driver_tcp) << "Connection to" << describePeer()
                                               << "shutting down";
                    readyState = ReadyState::Shutdown;
                    for (auto &c : startupConnections) {
                        QObject::disconnect(c);
                    }
                    startupTimeout.reset();
                    destroy();

                    /* Queue the socket for deletion by the worker thread, since we now have the
                     * only reference to it */
                    ownedSocket->deleteLater();
                }
            }

            bool checkFailed()
            {
                switch (socket.state()) {
                case QAbstractSocket::UnconnectedState:
                case QAbstractSocket::ClosingState: {
                    if (readyState != ReadyState::Connecting)
                        break;
                    readyState = ReadyState::Failed;
                    qCDebug(log_io_driver_tcp) << "Connection to" << describePeer()
                                               << "closed during startup:" << socket.errorString();

                    for (auto &c : startupConnections) {
                        QObject::disconnect(c);
                    }
                    startupConnections.clear();
                    startupTimeout.reset();

                    auto &container = worker->handshakeConnections;
                    container.erase(ownedSocket);
                    /* Self deleted */
                    return true;
                }
                default:
                    break;
                }
                return false;
            }

            std::string describePeer() const override
            { return peerName; }

            QHostAddress peerAddress() const override
            { return socketAddress; }

            std::uint16_t peerPort() const override
            { return targetPort; }

            QHostAddress localAddress() const override
            { return boundAddress; }


            void connectionReadyToUse()
            {
                if (readyState != ReadyState::Connecting)
                    return;
                readyState = ReadyState::Ready;

                for (auto &c : startupConnections) {
                    QObject::disconnect(c);
                }
                startupConnections.clear();
                startupTimeout.reset();

                auto &container = worker->handshakeConnections;
                auto handshakeRef = container.find(ownedSocket);
                Q_ASSERT(handshakeRef != container.end());
                auto ptr = std::move(handshakeRef->second);
                container.erase(handshakeRef);

                const auto &call = worker->incoming;
                call(std::move(ptr));
                /* Self potentially deleted */
            }
        };

        class IncomingConnectionTCP final : public IncomingConnection {
        public:
            void socketStateChanged()
            {
                if (socket.state() != QAbstractSocket::ConnectedState)
                    return;
                qCDebug(log_io_driver_tcp) << "TCP connection to" << describePeer() << "ready";
                return connectionReadyToUse();
            }

            IncomingConnectionTCP(QTcpSocket *socket, std::shared_ptr<Worker> worker)
                    : IncomingConnection(socket, std::move(worker))
            {
                startupConnections.emplace_back(
                        QObject::connect(socket, &QAbstractSocket::stateChanged,
                                         std::bind(&IncomingConnectionTCP::socketStateChanged,
                                                   this)));
            }

            virtual ~IncomingConnectionTCP()
            { shutdown(); }
        };

        class IncomingConnectionTLS final : public IncomingConnection {
            bool encryptionStarted;
            QSslCertificate certificate;
        public:
            void socketStateChanged()
            {
                if (encryptionStarted)
                    return;
                if (socket.state() != QAbstractSocket::ConnectedState)
                    return;
                encryptionStarted = true;
                auto ssl = static_cast<QSslSocket *>(&socket);

                qCDebug(log_io_driver_tcp) << "Starting TLS server encryption to" << describePeer();
                ssl->startServerEncryption();
                socketEncrypted();
            }

            void socketEncrypted()
            {
                auto ssl = static_cast<QSslSocket *>(&socket);
                if (!ssl->isEncrypted())
                    return;
                certificate = ssl->peerCertificate();

                qCDebug(log_io_driver_tcp) << "TLS connection to" << describePeer() << "ready";
                return connectionReadyToUse();
            }

            IncomingConnectionTLS(QSslSocket *socket, std::shared_ptr<Worker> worker)
                    : IncomingConnection(socket, std::move(worker)), encryptionStarted(false)
            {
                startupConnections.emplace_back(
                        QObject::connect(socket, &QAbstractSocket::stateChanged,
                                         std::bind(&IncomingConnectionTLS::socketStateChanged,
                                                   this)));
                startupConnections.emplace_back(QObject::connect(socket, &QSslSocket::encrypted,
                                                                 std::bind(
                                                                         &IncomingConnectionTLS::socketEncrypted,
                                                                         this)));
            }

            virtual ~IncomingConnectionTLS()
            { shutdown(); }

            QSslCertificate peerCertificate() const override
            { return certificate; }
        };

        std::unique_ptr<IncomingConnection> connection;
        if (auto sslSocket = dynamic_cast<QSslSocket *>(socket)) {
            auto ssl = new IncomingConnectionTLS(sslSocket, shared_from_this());
            handshakeConnections.emplace(socket, std::unique_ptr<Connection>(ssl));
            if (ssl->checkFailed())
                return;
            ssl->socketStateChanged();
        } else {
            auto tcp = new IncomingConnectionTCP(socket, shared_from_this());
            handshakeConnections.emplace(socket, std::unique_ptr<Connection>(tcp));
            if (tcp->checkFailed())
                return;
            tcp->socketStateChanged();
        }
    }

    void acceptConnections(QTcpServer *server)
    {
        for (;;) {
            auto conn = server->nextPendingConnection();
            if (!conn)
                break;
            attachSocket(conn);
        }
    }

    static void shutdownServer(QTcpServer &server)
    {
        server.close();
        for (;;) {
            auto conn = server.nextPendingConnection();
            if (!conn)
                break;
            conn->close();
            conn->deleteLater();
        }
    }

public:
    Worker(IncomingConnection &&incoming, std::vector<std::string> &&address, Configuration config)
            : incoming(std::move(incoming)),
              config(std::move(config)),
              lookup(std::move(address)),
              receiver(nullptr),
              remainingResolvers(0),
              failedResolvers(0),
              serversStarted(false),
              serversClosed(false)
    { startup(); }

    Worker(IncomingConnection &&incoming, std::vector<QHostAddress> &&address, Configuration config)
            : incoming(std::move(incoming)),
              config(std::move(config)),
              address(std::move(address)),
              receiver(nullptr),
              remainingResolvers(0),
              failedResolvers(0),
              serversStarted(false),
              serversClosed(false)
    { startup(); }

    ~Worker()
    {
        if (!thread)
            return;
        Q_ASSERT(receiver);
        if (receiver->thread() != QThread::currentThread()) {
            Threading::runBlockingFunctor(receiver, [this]() {
                qCDebug(log_io_driver_tcp) << "TCP server" << getDescription() << "shutting down";
                shutdown();
            });
            thread->quit();
            thread->wait();
        } else {
            /*
             * This can happen if the last reference to the shared pointer is via a
             * queued call.
             */

            qCDebug(log_io_driver_tcp) << "TCP server" << getDescription() << "shutting down";
            shutdown();
            thread->quit();

            /*
             * Note the context is the thread, which will be owned by another thread, so
             * we rely on that parent thread's event loop to do the delete.
             */
            Q_ASSERT(thread->thread() != QThread::currentThread());
            auto thr = std::make_shared<std::unique_ptr<QThread>>(std::move(thread));
            Threading::runQueuedFunctor(thr->get(), [thr]() {
                (*thr)->wait();
                thr->reset();
            });
        }
    }

    std::uint16_t localPort() const
    {
        std::lock_guard<std::mutex> lock(const_cast<Worker *>(this)->mutex);
        return config.port;
    }

    std::vector<QHostAddress> localAddress() const
    {
        if (receiver->thread() == QThread::currentThread())
            return address;
        std::vector<QHostAddress> addr;
        Threading::runBlockingFunctor(receiver, [this, &addr]() {
            addr = address;
        });
        return addr;
    }

    bool startListening(bool requireAll)
    {
        Q_ASSERT(receiver->thread() != QThread::currentThread());

        /* First wait for all names to be resolved */
        {
            std::unique_lock<std::mutex> lock(mutex);
            if (serversStarted) {
                lock.unlock();
                qCWarning(log_io_driver_tcp) << "TCP server already listening";
                return false;
            }
            serversStarted = true;

            notify.wait(lock, [this]() { return remainingResolvers == 0; });
            if (requireAll && failedResolvers)
                return false;
        }

        bool ok = false;
        Threading::runBlockingFunctor(receiver, [this, requireAll, &ok]() {
            if (address.empty()) {
                qCDebug(log_io_driver_tcp) << "No addresses available to listen on";
                return;
            }

            for (const auto &addr : address) {
                std::unique_ptr<QTcpServer> server;
                if (config.tls) {
                    server.reset(new TLSServer(*this));
                } else {
                    server.reset(new TCPServer);
                }
                if (!server->listen(addr, config.port)) {
                    qCDebug(log_io_driver_tcp) << "Failed to listen on" << addr << ":"
                                               << server->errorString();
                    continue;
                }
                servers.emplace_back(std::move(server));
            }

            if (servers.empty())
                return;

            {
                std::lock_guard<std::mutex> lock(mutex);
                if (config.port == config.port_any) {
                    config.port = servers.front()->serverPort();
                }
            }

            if (requireAll) {
                if (servers.size() != address.size()) {
                    for (const auto &srv : servers) {
                        shutdownServer(*srv);
                    }
                    servers.clear();
                    return;
                }
            }

            qCDebug(log_io_driver_tcp) << "TCP server" << getDescription()
                                       << "accepting connections";

            for (const auto &srv : servers) {
                QObject::connect(srv.get(), &QTcpServer::newConnection,
                                 std::bind(&Worker::acceptConnections, this, srv.get()));
                acceptConnections(srv.get());
            }

            ok = true;
        });

        if (!ok) {
            std::lock_guard<std::mutex> lock(mutex);
            serversClosed = true;
        }

        return ok;
    }

    void stopListening()
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (!serversStarted)
                return;
            if (serversClosed)
                return;
            serversClosed = true;
        }

        if (receiver->thread() == QThread::currentThread()) {
            for (const auto &srv : servers) {
                shutdownServer(*srv);
            }
            handshakeConnections.clear();
            qCDebug(log_io_driver_tcp) << "TCP server" << getDescription()
                                       << "no longer accepting connections";
            return;
        }

        Threading::runBlockingFunctor(receiver, [this]() {
            for (const auto &srv : servers) {
                shutdownServer(*srv);
            }
            handshakeConnections.clear();
            qCDebug(log_io_driver_tcp) << "TCP server" << getDescription()
                                       << "no longer accepting connections";
        });
    }

    bool isListening()
    {
        std::lock_guard<std::mutex> lock(mutex);
        return serversStarted && !serversClosed;
    }

    std::string describeServer() const
    {
        if (receiver->thread() == QThread::currentThread())
            return getDescription();
        std::string desc;
        Threading::runBlockingFunctor(receiver, [this, &desc]() {
            desc = getDescription();
        });
        return desc;
    }
};

Server::~Server()
{ stopListening(); }

Server::Server(IncomingConnection connection, Configuration config) : Server(std::move(connection),
                                                                             AddressFamily::Any,
                                                                             std::move(config))
{ }

Server::Server(IncomingConnection connection, std::string address, Configuration config) : Server(
        std::move(connection), std::vector<std::string>{std::move(address)}, std::move(config))
{ }

Server::Server(IncomingConnection connection, const QHostAddress &address, Configuration config)
        : Server(std::move(connection), std::vector<QHostAddress>{address}, std::move(config))
{ }

static std::vector<QHostAddress> toHostAddress(Server::AddressFamily address)
{
    switch (address) {
    case Server::AddressFamily::Any:
        return {QHostAddress(QHostAddress::Any)};
    case Server::AddressFamily::Loopback: {
        std::vector<QHostAddress> result;
#ifndef QT_NO_IPV4
        result.emplace_back(QHostAddress::LocalHost);
#endif
#ifndef QT_NO_IPV6
        result.emplace_back(QHostAddress::LocalHostIPv6);
#endif
        return result;
    }
    case Server::AddressFamily::LoopbackIPv4:
        return {QHostAddress(QHostAddress::LocalHost)};
    case Server::AddressFamily::LoopbackIPv6:
        return {QHostAddress(QHostAddress::LocalHostIPv6)};
    default:
        break;
    }
    return {};
}

Server::Server(IncomingConnection connection, AddressFamily address, Configuration config) : Server(
        std::move(connection), toHostAddress(address), std::move(config))
{ }

Server::Server(IncomingConnection connection,
               const QList<QHostAddress> &address,
               Configuration config) : Server(std::move(connection),
                                              std::vector<QHostAddress>(address.begin(),
                                                                        address.end()),
                                              std::move(config))
{ }

Server::Server(IncomingConnection connection,
               std::vector<std::string> address,
               Configuration config) : Socket::Server(connection),
                                       worker(std::make_shared<Worker>(std::move(connection),
                                                                       std::move(address),
                                                                       std::move(config)))
{ }

Server::Server(IncomingConnection connection,
               std::vector<QHostAddress> address,
               Configuration config) : Socket::Server(connection),
                                       worker(std::make_shared<Worker>(std::move(connection),
                                                                       std::move(address),
                                                                       std::move(config)))
{ }

std::uint16_t Server::localPort() const
{ return worker->localPort(); }

std::vector<QHostAddress> Server::localAddress() const
{ return worker->localAddress(); }

bool Server::startListening(bool requireAll)
{ return worker->startListening(requireAll); }

void Server::stopListening()
{ return worker->stopListening(); }

bool Server::isListening() const
{ return worker->isListening(); }

std::string Server::describeServer() const
{ return worker->describeServer(); }

}
}
}
}