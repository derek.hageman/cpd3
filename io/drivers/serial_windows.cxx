/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <Windows.h>
#include <algorithm>
#include <thread>
#include <mutex>
#include <cstring>
#include <chrono>
#include <QLoggingCategory>

#include "serial.hxx"
#include "io/notifywake.hxx"
#include "core/qtcompat.hxx"


Q_LOGGING_CATEGORY(log_io_driver_serial, "cpd3.io.driver.serial", QtWarningMsg)

namespace CPD3 {
namespace IO {


namespace {

/* http://www.codeproject.com/Articles/2682/Serial-Communication-in-Windows */

class WindowsSerialStream : public Serial::Stream {
    NotifyWake wake;
    HANDLE port;

    std::thread thread;
    std::mutex mutex;
    std::condition_variable external;
    bool terminated;

    Util::ByteArray writeBuffer;
    bool readStarted;
    bool readEnded;
    bool readShouldStall;

    enum class LockState {
        Unlocked, LockRequested, Locked, LockedFlushStart, LockFlushReading,
    } lockState;
    std::size_t lockFlushMaximumRead;

    std::string lastError;

    using Clock = std::chrono::steady_clock;

    /* Never accessed while locked, so safe without a mutex */
    bool rtsControlEnabled;
    bool rtsReceiveDiscard;
    bool rtsInvert;
    bool inBlankTime;
    bool wasWriting;
    bool inReadDiscard;
    std::size_t writtenSinceStart;
    std::chrono::duration<double> blankAfterWrite;
    std::chrono::duration<double> blankAfterPerCharacter;
    Clock::time_point blankEndTime;

    void unrecoverableFailure()
    {
        HANDLE chandle = port;
        bool doEnd;
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (port == INVALID_HANDLE_VALUE)
                return;
            port = INVALID_HANDLE_VALUE;
            doEnd = readStarted && !readEnded;
            readEnded = true;
            terminated = true;
            lockState = LockState::Unlocked;
        }
        ::CloseHandle(chandle);
        if (doEnd) {
            ended();
        }
    }

    DWORD waitEntry(bool &writing, bool &reading, OVERLAPPED &ov)
    {
        static constexpr DWORD defaultWaitTime = 250;

        inReadDiscard = false;
        if (!rtsControlEnabled)
            return defaultWaitTime;
        if (port == INVALID_HANDLE_VALUE)
            return defaultWaitTime;

        if (inBlankTime) {
            auto now = Clock::now();
            if (now < blankEndTime) {
                writing = false;
                inReadDiscard = rtsReceiveDiscard;
                if (inReadDiscard)
                    reading = true;

                auto remaining =
                        std::chrono::duration_cast<std::chrono::milliseconds>(blankEndTime - now);
                if (remaining.count() < 1)
                    return 1;
                return static_cast<DWORD>(remaining.count());
            }

            setRTS(rtsInvert);
            inBlankTime = false;
        }

        if (!writing && wasWriting) {
            wasWriting = false;

            auto drainStart = Clock::now();
            DWORD events = EV_TXEMPTY;
            if (!::WaitCommEvent(port, &events, &ov) && ::GetLastError() != ERROR_IO_PENDING) {
                qCDebug(log_io_driver_serial) << "Failed to drain write buffer:"
                                              << NotifyWake::getWindowsError();
            }

            if (blankAfterWrite.count() > 0 || blankAfterPerCharacter.count() > 0) {
                inBlankTime = true;
                inReadDiscard = rtsReceiveDiscard;
                if (inReadDiscard)
                    reading = true;

                auto now = Clock::now();
                blankEndTime = now + std::chrono::duration_cast<Clock::duration>(blankAfterWrite);

                if (blankAfterPerCharacter.count() > 0) {
                    auto charRemaining = std::chrono::duration_cast<std::chrono::duration<double>>(
                            now - drainStart);
                    charRemaining += blankAfterPerCharacter * writtenSinceStart;
                    if (charRemaining.count() > 0) {
                        blankEndTime += std::chrono::duration_cast<Clock::duration>(charRemaining);
                    }
                }

                auto remaining =
                        std::chrono::duration_cast<std::chrono::milliseconds>(blankEndTime - now);
                if (remaining.count() < 1)
                    return 1;
                return static_cast<int>(remaining.count());
            } else {
                setRTS(rtsInvert);
            }
        } else if (writing && !wasWriting) {
            wasWriting = true;
            writtenSinceStart = 0;
            setRTS(!rtsInvert);
        }

        if (writing) {
            inReadDiscard = rtsReceiveDiscard;
            if (inReadDiscard)
                reading = true;
        }

        return defaultWaitTime;
    }

    void run()
    {
        static constexpr std::size_t stallThreshold = 4194304;
        static constexpr std::size_t readBlockSize = 65536;

        OVERLAPPED ov;
        std::memset(&ov, 0, sizeof(ov));
        ov.hEvent = ::CreateEvent(0, TRUE, 0, 0);
        if (ov.hEvent == NULL) {
            qCWarning(log_io_driver_serial) << "Failed to create overlapped IO event:"
                                            << NotifyWake::getWindowsError();
            return unrecoverableFailure();
        }

        HANDLE evHandles[2];
        wake.install(evHandles[0]);
        evHandles[1] = ov.hEvent;


        Util::ByteArray dataToWrite;
        Util::ByteArray readBuffer;
        bool wasStalled = false;
        for (;;) {
            bool isReading = false;
            bool isLocked = false;
            bool isLockReading = false;
            {
                std::lock_guard<std::mutex> lock(mutex);
                dataToWrite += std::move(writeBuffer);
                writeBuffer.clear();

                if (terminated && dataToWrite.empty()) {
                    lockState = LockState::Unlocked;
                    return;
                }

                if (readStarted && !readEnded) {
                    isReading = !readShouldStall && port != INVALID_HANDLE_VALUE;
                }

                switch (lockState) {
                case LockState::Unlocked:
                    break;
                case LockState::LockRequested:
                    isLocked = true;
                    lockState = LockState::Locked;
                    external.notify_all();
                    break;
                case LockState::Locked:
                    isLocked = true;
                    break;
                case LockState::LockedFlushStart:
                case LockState::LockFlushReading:
                    isLocked = true;
                    isLockReading = readStarted && !readEnded && lockFlushMaximumRead != 0;
                    if (isLockReading) {
                        lockState = LockState::LockFlushReading;
                    } else {
                        lockState = LockState::Locked;
                        external.notify_all();
                    }
                    dataToWrite.clear();
                    writeBuffer.clear();
                    break;
                }
            }

            if (!wasStalled) {
                if (dataToWrite.size() > stallThreshold) {
                    wasStalled = true;
                    writeStall(true);
                }
            } else {
                if (dataToWrite.size() < stallThreshold) {
                    wasStalled = false;
                    writeStall(false);
                }
            }

            if (isLockReading) {
                readBuffer.resize(4096);
                while (lockFlushMaximumRead != 0) {
                    auto maxrd = readBuffer.size();
                    if (lockFlushMaximumRead != static_cast<std::size_t>(-1))
                        maxrd = std::min<std::size_t>(maxrd, lockFlushMaximumRead);

                    DWORD nrd = 0;
                    if (!::ReadFile(port, readBuffer.data(), maxrd, &nrd, &ov) &&
                            ::GetLastError() != ERROR_IO_PENDING) {
                        qCDebug(log_io_driver_serial) << "Port flush read failed on" << portName()
                                                      << ":" << NotifyWake::getWindowsError();
                        return unrecoverableFailure();
                    }
                    if (nrd <= 0) {
                        break;
                    }

                    if (lockFlushMaximumRead != static_cast<std::size_t>(-1)) {
                        Q_ASSERT(static_cast<std::size_t>(nrd) <= lockFlushMaximumRead);
                        lockFlushMaximumRead -= nrd;
                    }
                }

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    if (lockState == LockState::LockFlushReading) {
                        lockState = LockState::Locked;
                        dataToWrite.clear();
                        writeBuffer.clear();
                    }
                }
                external.notify_all();
                continue;
            }

            bool isWriting = !dataToWrite.empty() && port != INVALID_HANDLE_VALUE;
            DWORD waitTimeout = waitEntry(isWriting, isReading, ov);

            bool didWrite = false;
            if (!isLocked && isWriting) {
                Q_ASSERT(port != INVALID_HANDLE_VALUE);

                if (!::WriteFile(port, dataToWrite.data(), dataToWrite.size(), NULL, &ov) &&
                        ::GetLastError() != ERROR_IO_PENDING) {
                    qCDebug(log_io_driver_serial) << "Port write failed on" << portName() << ":"
                                                  << NotifyWake::getWindowsError();
                    return unrecoverableFailure();
                }
                writtenSinceStart += dataToWrite.size();
                dataToWrite.clear();
                didWrite = true;
            }

            if (isLocked || !isReading || port == INVALID_HANDLE_VALUE) {
                if (didWrite)
                    continue;
                DWORD ev = ::WaitForMultipleObjects(1, evHandles, FALSE, waitTimeout);
                if (ev == WAIT_TIMEOUT)
                    continue;
                if (ev == WAIT_FAILED) {
                    qCDebug(log_io_driver_serial) << "Event wait failed on" << portName() << ":"
                                                  << NotifyWake::getWindowsError();
                    return unrecoverableFailure();
                }
                continue;
            }

            Q_ASSERT(isReading);

            bool didRead = false;
            {
                Q_ASSERT(port != INVALID_HANDLE_VALUE);

                readBuffer.resize(readBlockSize);
                DWORD nrd = 0;
                if (!::ReadFile(port, readBuffer.data(), readBuffer.size(), &nrd, &ov) &&
                        ::GetLastError() != ERROR_IO_PENDING) {
                    qCDebug(log_io_driver_serial) << "Port read failed on" << portName() << ":"
                                                  << NotifyWake::getWindowsError();
                    return unrecoverableFailure();
                }
                if (nrd > 0) {
                    if (!inReadDiscard) {
                        readBuffer.resize(nrd);
                        read(readBuffer);
                    }
                    didRead = true;
                }
            }

            if (didWrite || didRead)
                continue;

            DWORD ev = 0;
            if (!::WaitCommEvent(port, &ev, &ov)) {
                if (::GetLastError() != ERROR_IO_PENDING) {
                    qCDebug(log_io_driver_serial) << "Error waiting for serial port event on"
                                                  << portName() << ":"
                                                  << NotifyWake::getWindowsError();
                    return unrecoverableFailure();
                }
            } else {
                continue;
            }

            ev = ::WaitForMultipleObjects(2, evHandles, FALSE, waitTimeout);
            if (ev == WAIT_TIMEOUT)
                continue;
            if (ev == WAIT_FAILED) {
                qCDebug(log_io_driver_serial) << "Event wait failed on" << portName() << ":"
                                              << NotifyWake::getWindowsError();
                return unrecoverableFailure();
            }
        }
    }


    class Lock final {
        WindowsSerialStream &stream;
    public:
        explicit Lock(WindowsSerialStream &strm) : stream(strm)
        {
            {
                std::unique_lock<std::mutex> lock(stream.mutex);
                stream.external.wait(lock, [this] {
                    return stream.terminated || stream.lockState == LockState::Unlocked;
                });
                if (stream.terminated)
                    return;
                Q_ASSERT(stream.lockState == LockState::Unlocked);
                stream.lockState = LockState::LockRequested;
            }
            stream.wake();
            {
                std::unique_lock<std::mutex> lock(stream.mutex);
                stream.external.wait(lock, [this] {
                    return stream.terminated || stream.lockState == LockState::Locked;
                });
                if (stream.terminated)
                    return;
                Q_ASSERT(stream.lockState == LockState::Locked);
            }
        }

        ~Lock()
        {
            {
                std::lock_guard<std::mutex> lock(stream.mutex);
                if (stream.lockState != LockState::Locked)
                    return;
                stream.lockState = LockState::Unlocked;
            }
            stream.external.notify_all();
            stream.wake();
        }

        void flush(std::size_t maximumReadFlush = static_cast<std::size_t>(-1))
        {
            {
                std::unique_lock<std::mutex> lock(stream.mutex);
                if (stream.terminated)
                    return;
                Q_ASSERT(stream.lockState == LockState::Locked);
                stream.lockState = LockState::LockedFlushStart;
                stream.lockFlushMaximumRead = maximumReadFlush;
            }
            stream.wake();
            {
                std::unique_lock<std::mutex> lock(stream.mutex);
                stream.external.wait(lock, [this] {
                    return stream.terminated || stream.lockState == LockState::Locked;
                });
                if (stream.terminated)
                    return;
                Q_ASSERT(stream.lockState == LockState::Locked);
            }
        }

        bool beginDCB(DCB &dcb)
        {
            if (stream.port == INVALID_HANDLE_VALUE) {
                stream.lastError = "not open";
                return false;
            }

            std::memset(&dcb, 0, sizeof(DCB));
            dcb.DCBlength = sizeof(DCB);

            if (!::GetCommState(stream.port, &dcb)) {
                stream.lastError = "GetCommState: " + NotifyWake::getWindowsError();
                return false;
            }
            return true;
        }

        bool endDCB(DCB &dcb)
        {
            if (stream.port == INVALID_HANDLE_VALUE) {
                stream.lastError = "not open";
                return false;
            }

            if (!::SetCommState(stream.port, &dcb)) {
                stream.lastError = "SetCommState: " + NotifyWake::getWindowsError();
                return false;
            }
            return true;
        }
    };

    friend class Lock;

    bool setRTS(bool raise)
    {
        if (!::EscapeCommFunction(port, raise ? SETRTS : CLRRTS)) {
            qCDebug(log_io_driver_serial) << "Error setting RTS:" << NotifyWake::getWindowsError();
            return false;
        }
        return true;
    }

    void disableRTSControl()
    {
        if (!rtsControlEnabled)
            return;
        rtsControlEnabled = false;
        setRTS(true);
    }

public:
    WindowsSerialStream(std::string name, HANDLE port) : Serial::Stream(std::move(name)),
                                                         port(port),
                                                         terminated(false),
                                                         readStarted(false),
                                                         readEnded(false),
                                                         readShouldStall(false),
                                                         lockState(LockState::Unlocked),
                                                         lockFlushMaximumRead(0),
                                                         rtsControlEnabled(false),
                                                         rtsReceiveDiscard(false),
                                                         rtsInvert(false),
                                                         inBlankTime(false),
                                                         wasWriting(false),
                                                         inReadDiscard(false),
                                                         writtenSinceStart(0),
                                                         blankAfterWrite(0),
                                                         blankAfterPerCharacter(0)
    {
        thread = std::thread(std::bind(&WindowsSerialStream::run, this));
    }

    virtual ~WindowsSerialStream()
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            terminated = true;
        }
        wake();

        if (thread.joinable())
            thread.join();

        if (port != INVALID_HANDLE_VALUE)
            ::CloseHandle(port);
    }

    void write(const Util::ByteView &data) override
    {
        if (data.empty())
            return;
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (terminated || port == INVALID_HANDLE_VALUE)
                return;
            writeBuffer += data;
        }
        wake();
    }

    void write(Util::ByteArray &&data) override
    {
        if (data.empty())
            return;
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (terminated || port == INVALID_HANDLE_VALUE)
                return;
            writeBuffer += std::move(data);
        }
        wake();
    }

    void start() override
    {
        bool immediateEnd = false;
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (readEnded) {
                immediateEnd = true;
            } else {
                readStarted = true;
            }
        }
        if (immediateEnd) {
            ended();
            return;
        }
        wake();
    }

    bool isEnded() const override
    {
        std::lock_guard<std::mutex> lock(const_cast<WindowsSerialStream *>(this)->mutex);
        return readEnded;
    }

    void readStall(bool enable) override
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            readShouldStall = enable;
        }
        if (!enable) {
            wake();
        }
    }


    std::string describePort() const override
    {
        auto desc = portName();
        if (Util::starts_with(desc, R"(\\.\)") || Util::starts_with(desc, R"(//./)"))
            desc.erase(0, 4);

        DCB dcb;
        std::memset(&dcb, 0, sizeof(DCB));
        dcb.DCBlength = sizeof(DCB);

        {
            std::lock_guard<std::mutex> lock(const_cast<WindowsSerialStream *>(this)->mutex);
            if (port == INVALID_HANDLE_VALUE)
                return desc;
            if (!::GetCommState(port, &dcb))
                return desc;
        }

        desc += ":";
        if (dcb.BaudRate > 0) {
            desc += std::to_string(static_cast<int>(dcb.BaudRate));
        }
        switch (dcb.Parity) {
        default:
        case NOPARITY:
            desc += "N";
            break;
        case EVENPARITY:
            desc += "E";
            break;
        case ODDPARITY:
            desc += "O";
            break;
        case MARKPARITY:
            desc += "M";
            break;
        case SPACEPARITY:
            desc += "S";
            break;
        }
        if (dcb.ByteSize > 0) {
            desc += std::to_string(static_cast<int>(dcb.ByteSize));
        }
        switch (dcb.StopBits) {
        case ONESTOPBIT:
        default:
            desc += "1";
            break;
        case TWOSTOPBITS:
            desc += "2";
            break;
        case ONE5STOPBITS:
            desc += "H";
            break;
        }
        return desc;
    }

    std::string describeError() const override
    { return lastError; }

    bool setBaud(int rate) override
    {
        Lock lock(*this);
        DCB dcb;
        if (!lock.beginDCB(dcb))
            return false;

        dcb.BaudRate = static_cast<DWORD>(rate);

        return lock.endDCB(dcb);
    }

    bool setDataBits(int bits) override
    {
        Lock lock(*this);
        DCB dcb;
        if (!lock.beginDCB(dcb))
            return false;

        dcb.ByteSize = static_cast<BYTE>(bits);

        return lock.endDCB(dcb);
    }

    bool setStopBits(int bits) override
    {
        Lock lock(*this);
        DCB dcb;
        if (!lock.beginDCB(dcb))
            return false;

        switch (bits) {
        case 1:
            dcb.StopBits = ONESTOPBIT;
            break;
        case 2:
            dcb.StopBits = TWOSTOPBITS;
            break;
        case 3:
            dcb.StopBits = ONE5STOPBITS;
            break;
        default:
            lastError = "Invalid number of stop bits (" + std::to_string(bits) + ")";
            return false;
        }

        return lock.endDCB(dcb);
    }

    bool setParity(Parity parity) override
    {
        Lock lock(*this);
        DCB dcb;
        if (!lock.beginDCB(dcb))
            return false;

        switch (parity) {
        case Parity::None:
            dcb.Parity = NOPARITY;
            break;
        case Parity::Even:
            dcb.Parity = EVENPARITY;
            break;
        case Parity::Odd:
            dcb.Parity = ODDPARITY;
            break;
        case Parity::Mark:
            dcb.Parity = MARKPARITY;
            break;
        case Parity::Space:
            dcb.Parity = SPACEPARITY;
            break;
        }

        return lock.endDCB(dcb);
    }

    bool setNoHardwareFlowControl() override
    {
        Lock lock(*this);
        DCB dcb;
        if (!lock.beginDCB(dcb))
            return false;

        dcb.fRtsControl = RTS_CONTROL_DISABLE;
        dcb.fOutxCtsFlow = FALSE;

        disableRTSControl();
        return lock.endDCB(dcb);
    }

    bool setRS232StandardFlowControl() override
    {
        Lock lock(*this);
        DCB dcb;
        if (!lock.beginDCB(dcb))
            return false;

        dcb.fRtsControl = RTS_CONTROL_ENABLE;
        dcb.fOutxCtsFlow = TRUE;

        disableRTSControl();
        return lock.endDCB(dcb);
    }

    bool setExplicitRTSOnTransmit(double blankAfter = 0.0,
                                  double blankPerCharacter = 0.0,
                                  bool discardReceive = false,
                                  bool invert = false) override
    {
        Lock lock(*this);
        DCB dcb;
        if (!lock.beginDCB(dcb))
            return false;

        dcb.fRtsControl = RTS_CONTROL_DISABLE;
        dcb.fOutxCtsFlow = FALSE;

        if (!lock.endDCB(dcb))
            return false;

        if (!setRTS(invert))
            return false;

        rtsControlEnabled = true;
        rtsReceiveDiscard = discardReceive;
        rtsInvert = invert;
        wasWriting = false;
        inBlankTime = false;
        blankEndTime = Clock::now();

        if (blankAfter > 0.0) {
            blankAfterWrite = std::chrono::duration<double>(blankAfter);
        } else {
            blankAfterWrite = std::chrono::duration<double>(0);
        }

        if (blankPerCharacter > 0.0) {
            blankAfterPerCharacter = std::chrono::duration<double>(blankPerCharacter);
        } else {
            blankAfterPerCharacter = std::chrono::duration<double>(0);
        }

        return true;
    }


    bool setSoftwareFlowControl(SoftwareFlowControl control, bool) override
    {
        Lock lock(*this);
        DCB dcb;
        if (!lock.beginDCB(dcb))
            return false;

        switch (control) {
        case SoftwareFlowControl::Disable:
            dcb.fInX = FALSE;
            dcb.fOutX = FALSE;
            break;
        case SoftwareFlowControl::Both:
            dcb.fOutX = TRUE;
            dcb.fInX = TRUE;
            break;
        case SoftwareFlowControl::XON:
            dcb.fOutX = TRUE;
            dcb.fInX = FALSE;
            break;
        case SoftwareFlowControl::XOFF:
            dcb.fInX = TRUE;
            dcb.fOutX = FALSE;
            break;
        }

        return lock.endDCB(dcb);
    }


    bool flushPort(bool blank = true) override
    {
        Lock lock(*this);
        DCB dcb;
        bool ok = lock.beginDCB(dcb);
        if (port == INVALID_HANDLE_VALUE) {
            lastError = "not open";
            return false;
        }

        if (!::PurgeComm(port, PURGE_RXCLEAR | PURGE_TXCLEAR)) {
            lastError = "PurgeComm: " + NotifyWake::getWindowsError();
            ok = false;
        }

        std::chrono::duration<double> sleepTime(0.1);
        if (blank) {
            auto start = std::chrono::steady_clock::now();
            if (!::SetCommBreak(port)) {
                lastError = "SetCommBreak: " + NotifyWake::getWindowsError();
                ok = false;
            }
            auto end = std::chrono::steady_clock::now();
            std::chrono::duration<double> elapsed = end - start;
            auto n = elapsed.count();
            if (n < 0.5) {
                sleepTime += std::chrono::duration<double>(0.5 - n);
            }
        }

        if (dcb.BaudRate > 0) {
            static constexpr std::uint_fast64_t symbolsToSleep = 10 * 2; /* ~2 frames */
            auto hzToSleep = static_cast<std::uint_fast64_t>(dcb.BaudRate) * symbolsToSleep;
            sleepTime += std::chrono::duration<double>(1.0 / hzToSleep);
        }

        std::this_thread::sleep_for(sleepTime);

        std::size_t nFlush = 4096;
        if (dcb.BaudRate > 0) {
            auto flushBytes = static_cast<std::size_t>(dcb.BaudRate * sleepTime.count());
            flushBytes /= 10;
            flushBytes += 1;
            if (flushBytes > nFlush) {
                nFlush = flushBytes;
            }
        }

        lock.flush(nFlush);
        return ok;
    }

    bool pulseRTS(double width = 0.05, bool invert = false) override
    {
        Lock lock(*this);
        if (port == INVALID_HANDLE_VALUE) {
            lastError = "not open";
            return false;
        }

        if (width > 0.0) {
            if (!::EscapeCommFunction(port, invert ? SETRTS : CLRRTS)) {
                lastError = "EscapeCommFunction: " + NotifyWake::getWindowsError();
                return false;
            }

            std::this_thread::sleep_for(std::chrono::duration<double>(width));
        }

        if (!::EscapeCommFunction(port, invert ? CLRRTS : SETRTS)) {
            lastError = "EscapeCommFunction: " + NotifyWake::getWindowsError();
            return false;
        }

        return true;
    }

    bool pulseDTR(double width = 0.05, bool invert = false) override
    {
        Lock lock(*this);
        if (port == INVALID_HANDLE_VALUE) {
            lastError = "not open";
            return false;
        }

        if (width > 0.0) {
            if (!::EscapeCommFunction(port, invert ? SETDTR : CLRDTR)) {
                lastError = "EscapeCommFunction: " + NotifyWake::getWindowsError();
                return false;
            }

            std::this_thread::sleep_for(std::chrono::duration<double>(width));
        }

        if (!::EscapeCommFunction(port, invert ? CLRDTR : SETDTR)) {
            lastError = "EscapeCommFunction: " + NotifyWake::getWindowsError();
            return false;
        }

        return true;
    }
};

}

namespace Access {

std::shared_ptr<Serial::Backing> serial(std::string name)
{
    class Backing : public Serial::Backing {
    public:
        Backing(std::string &&name) : Serial::Backing(std::move(name))
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        {
            std::wstring rawPort(QString::fromStdString(portName()).toStdWString());
            HANDLE h =
                    ::CreateFileW(rawPort.data(), GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING,
                                  FILE_FLAG_OVERLAPPED, 0);
            if (h == INVALID_HANDLE_VALUE) {
                qCDebug(log_io_driver_serial) << "Failed to open serial port" << portName() << ":"
                                              << NotifyWake::getWindowsError();
                return {};
            }

            COMMTIMEOUTS timeouts;
            memset(&timeouts, 0, sizeof(timeouts));
            timeouts.ReadIntervalTimeout = MAXDWORD;
            timeouts.ReadTotalTimeoutMultiplier = 0;
            timeouts.ReadTotalTimeoutConstant = 0;
            timeouts.WriteTotalTimeoutMultiplier = 0;
            timeouts.WriteTotalTimeoutConstant = 10;
            if (!::SetCommTimeouts(h, &timeouts)) {
                qCDebug(log_io_driver_serial) << "Failed to set serial port" << portName()
                                              << "timeouts:" << NotifyWake::getWindowsError();
                ::CloseHandle(h);
                return {};
            }

            if (!::SetCommMask(h, EV_RXCHAR)) {
                qCDebug(log_io_driver_serial) << "Serial port" << portName()
                                              << "event mask set failed:"
                                              << NotifyWake::getWindowsError();
                ::CloseHandle(h);
                return {};
            }

            return std::unique_ptr<Generic::Stream>(new WindowsSerialStream(portName(), h));
        }
    };
    return std::make_shared<Backing>(std::move(name));
}

}

namespace Serial {

bool Backing::isValid(const std::string &name)
{ return Util::starts_with_insensitive(name, "COM"); }

static std::unordered_set<std::string> enumerateFromRegistry()
{
    std::unordered_set<std::string> result;
    HKEY serialPortList;

    if (::RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"HARDWARE\\DEVICEMAP\\SERIALCOMM", 0, KEY_READ,
                       &serialPortList) != ERROR_SUCCESS)
        return result;

    DWORD nKeys = 0;
    DWORD longestName = 0;
    DWORD longestValue = 0;

    if (::RegQueryInfoKey(serialPortList, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr,
                          &nKeys, &longestName, &longestValue, nullptr, nullptr) != ERROR_SUCCESS) {
        ::RegCloseKey(serialPortList);
        return result;
    }

    longestName++;
    longestValue++;

    std::vector<wchar_t> name(longestName + 1);
    std::vector<wchar_t> value(longestValue + 1);
    DWORD lName;
    DWORD lValue;

    for (DWORD i = 0; i < nKeys; i++) {
        lName = longestName;
        lValue = longestValue * sizeof(wchar_t);
        if (::RegEnumValue(serialPortList, i, name.data(), &lName, nullptr, nullptr,
                           reinterpret_cast<LPBYTE>(value.data()), &lValue) != ERROR_SUCCESS)
            continue;
        if (lValue < 1)
            continue;

        result.insert(QString::fromWCharArray(value.data(), lValue).toStdString());
    }

    ::RegCloseKey(serialPortList);

    return result;
}

std::unordered_set<std::string> enumeratePorts()
{
    auto result = enumerateFromRegistry();

    for (int i = 0; i < 255; i++) {
        wchar_t bfr[MAX_PATH];
        QString p = QString("COM%1").arg(i);
        if (result.count(p.toStdString()))
            continue;
        if (!::QueryDosDevice(p.toStdWString().data(), bfr, MAX_PATH))
            continue;

        result.insert(p.toStdString());
    }

    return result;
}

}

}
}