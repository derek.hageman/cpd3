/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_DRIVERS_SERIAL_HXX
#define CPD3IO_DRIVERS_SERIAL_HXX

#include "core/first.hxx"

#include <unordered_set>

#include "io/io.hxx"
#include "io/access.hxx"

namespace CPD3 {
namespace IO {

namespace Serial {

class Backing;

}

namespace Access {

/**
 * Create an access handle for a serial port.
 *
 * @param name  the OS specific serial port name
 * @return      the serial port name
 */
CPD3IO_EXPORT std::shared_ptr<Serial::Backing> serial(std::string name);

CPD3IO_EXPORT std::shared_ptr<Serial::Backing> serial(const QString &name);

CPD3IO_EXPORT std::shared_ptr<Serial::Backing> serial(const char *name);

}

namespace Serial {

/**
 * Get a list of available serial ports.
 *
 * @return  the list of serial ports
 */
CPD3IO_EXPORT std::unordered_set<std::string> enumeratePorts();

/**
 * Serial port handling handling backing.
 */
class CPD3IO_EXPORT Backing : public Generic::Backing {
    std::string name;
protected:
    explicit Backing(std::string name);

public:
    Backing() = delete;

    virtual ~Backing();

    std::unique_ptr<Generic::Block> block() override;

    /**
     * Get the name of the serial port.
     *
     * @return  the backed serial port name
     */
    inline const std::string &portName() const
    { return name; }

    /**
     * Test if a name is a valid serial port.
     *
     * @param name  the serial port name
     * @return      true of the serial port is valid
     */
    static bool isValid(const std::string &name);
};

/**
 * A serial port access stream.
 */
class CPD3IO_EXPORT Stream : public Generic::Stream {
    std::string name;
protected:
    explicit Stream(std::string name);

public:
    Stream() = delete;

    virtual ~Stream();

    /**
     * Get the name of the serial port.
     *
     * @return  the backed serial port name
     */
    inline const std::string &portName() const
    { return name; }

    /**
     * Get a description of the port and settings
     *
     * @return  a description string
     */
    virtual std::string describePort() const = 0;

    /**
     * Get a human readable description of the latest error.
     *
     * @return  a description of the error
     */
    virtual std::string describeError() const = 0;

    /**
     * Set the baud rate of the serial port.
     *
     * @param rate  the number of symbols per second
     * @return      true on success
     */
    virtual bool setBaud(int rate) = 0;

    /**
     * Set the number of data bits.
     *
     * @param bits  the number of data bits
     * @return      true on success
     */
    virtual bool setDataBits(int bits) = 0;

    /**
     * Set the number of stop bits.
     *
     * @param bits  the number of stop bits
     * @return      true on success
     */
    virtual bool setStopBits(int bits) = 0;

    /** Serial port parity types. */
    enum class Parity {
        None, Even, Odd, Mark, Space
    };

    /**
     * Set the serial port parity.
     *
     * @param parity    the port parity
     * @return          true on success
     */
    virtual bool setParity(Parity parity) = 0;

    /**
     * Disable hardware flow control.
     *
     * @return          true on success
     */
    virtual bool setNoHardwareFlowControl() = 0;

    /**
     * Set RS-232 standard RTS/CTS hardware flow control.
     *
     * @return          true on success
     */
    virtual bool setRS232StandardFlowControl() = 0;

    /**
     * Enable explicit control of the RTS line for outgoing flow control.  This can be
     * used to control a RS-485 transceiver.
     *
     * @param blankAfter        the amount of time after transmission completed to hold the RTS line
     * @param blankPerCharacter the amount of time per character after transmission to hold the RTS line, used for hardware that does not accurately report transmission complete
     * @param discardReceive    discard anything received during transmission (e.g. on a half duplex line)
     * @param invert            invert RTS control (lower it while transmitting)
     * @return                  true on success
     */
    virtual bool setExplicitRTSOnTransmit(double blankAfter = 0.0,
                                          double blankPerCharacter = 0.0,
                                          bool discardReceive = false,
                                          bool invert = false) = 0;


    /** Serial port software flow control types. */
    enum class SoftwareFlowControl {
        Disable, Both, XON, XOFF
    };

    /**
     * Set the serial port software flow control.
     *
     * @param control       the software flow control type
     * @param enableResume  enable XANY for flow control resume
     * @return              true on success
     */
    virtual bool setSoftwareFlowControl(SoftwareFlowControl control, bool enableResume = false) = 0;


    /**
     * Flush the port (hardware and software buffers).
     *
     * @param blank send a hardware break
     * @return      true on success
     */
    virtual bool flushPort(bool blank = true) = 0;

    /**
     * Send a pulse on RTS.  This will leave the line in the "low" state.
     *
     * @param width     the pulse width or zero to only set the control line
     * @param invert    invert the state and pulse (i.e. pulse to high and leave it low)
     * @return          true on success
     */
    virtual bool pulseRTS(double width = 0.05, bool invert = false) = 0;

    /**
     * Send a pulse on DTR.  This will leave the line in the "low" state.
     *
     * @param width     the pulse width or zero to only set the control line
     * @param invert    invert the state and pulse (i.e. pulse to high and leave it low)
     * @return          true on success
     */
    virtual bool pulseDTR(double width = 0.05, bool invert = false) = 0;
};

}

}
}

#endif //CPD3IO_DRIVERS_SERIAL_HXX
