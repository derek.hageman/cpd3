/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_DRIVERS_QIOWRAPPER_HXX
#define CPD3IO_DRIVERS_QIOWRAPPER_HXX

#include "core/first.hxx"

#include <QIODevice>
#include <QMetaObject>
#include <QTimer>

#include "io/io.hxx"
#include "core/threading.hxx"
#include "core/util.hxx"

namespace CPD3 {
namespace IO {

/**
 * A class handing wrapping of QIODevice operations.  This must be created on a QThread
 * that owns the device and will have an event loop running for it.  This does NOT implement
 * thread synchronization, so all access to it must be checked for the QThread parent and
 * run through queued functors, if needed.
 * <br>
 * This API mirrors Generic::Stream so the wrapper can be used with that easily.  It also
 * provides some functions used to implement Generic::Block.
 */
class QIOWrapper final {
    QIODevice &device;

    std::unique_ptr<QObject> receiver;
    std::unique_ptr<QTimer> readPoll;
    std::vector<QMetaObject::Connection> connections;

    Util::ByteArray readBuffer;
    Util::ByteArray writeBuffer;
    bool readStarted;
    bool readStalled;
    bool readEnded;
    bool writeStarted;
    bool sentWriteStall;

    void serviceWrite();

    void serviceRead();

public:
    QIOWrapper() = delete;

    explicit QIOWrapper(QIODevice &device);

    ~QIOWrapper();

    /**
     * Write data.
     *
     * @param data  the data to write
     */
    void write(const Util::ByteView &data);

    void write(Util::ByteArray &&data);

    /**
     * Start streaming reads.  Writes are available immediately, however.
     */
    void start();

    /**
     * Test if the read has ended.
     * @return  true if reading has completed
     */
    bool isEnded() const;

    /**
     * Emitted when data have been read from the stream.
     */
    Threading::Signal<const Util::ByteArray &> read;

    /**
     * Emitted when the stream has ended (only after the start).
     */
    Threading::Signal<> ended;

    /**
     * Emitted when the write should stall, if possible.  The stream will
     * still accept data but this signal is used to indicate excessive buffering.
     */
    Threading::Signal<bool> writeStall;

    /**
     * Request a read stall (stop emitting read data).  This is optional and may not be
     * respected or may take time to take effect.  So the read signal has to continue to be
     * handled.
     *
     * @param enable    enable the stall
     */
    void readStall(bool enable);


    /**
     * Destroy the wrapper.  Used by the destructor or other finalization.
     * <br>
     * Flushing writes requires that this is called by the owning thread.
     *
     * @param flushWrite    finish writing all queued data and do a graceful disconnect and close
     * @param flushTimeout  the maxium time to wait during write flushing, in milliseconds
     */
    void destroy(bool flushWrite = true, int flushTimeout = 30000);

    /**
     * Empty the write buffer, waiting until all data have been sent to the device.
     *
     * @return  false if an error was encountered
     */
    bool drainWriteBuffer();

    /**
     * Discard the contents of the write buffer.
     */
    void discardWriteBuffer();

    /**
     * Start processing the write buffer in the background.
     *
     * @param queued    queue the initial write, if any, instead of doing it immediately
     */
    void processWriteBuffer(bool queued = false);

    /**
     * Test if a device is at the end.  This augments the normal QIODevice check by
     * also checking socket state.
     *
     * @param device    the device to check
     * @return          true if the device is ended
     */
    static bool deviceAtEnd(const QIODevice &device);
};

}
}

#endif //CPD3IO_DRIVERS_QIOWRAPPER_HXX
