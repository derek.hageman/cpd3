/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include "unixfifo.hxx"

#ifdef Q_OS_UNIX

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include <QLoggingCategory>

#include "unixfdstream.hxx"
#include "core/qtcompat.hxx"

Q_LOGGING_CATEGORY(log_io_driver_unixfifo, "cpd3.io.driver.unixfifo", QtWarningMsg)


namespace CPD3 {
namespace IO {

namespace Access {


static bool isFIFO(int fd)
{
    struct ::stat sb;
    if (::fstat(fd, &sb) == -1)
        return false;
    return S_ISFIFO(sb.st_mode);
}

static int openFIFO(const std::string &filename, bool create, int mode)
{
    NotifyWake::blockSigpipe();

    mode |= O_NONBLOCK | O_NOCTTY;
#ifdef O_CLOEXEC
    mode |= O_CLOEXEC;
#endif

    int fd = ::open(filename.c_str(), mode);
    if (fd == -1) {
        if (create) {
            ::unlink(filename.c_str());
            if (::mkfifo(filename.c_str(), 0666) == -1) {
                qCDebug(log_io_driver_unixfifo) << "Unable to create FIFO" << filename << ":"
                                                << std::strerror(errno);
                return -1;
            }

            fd = ::open(filename.c_str(), mode);
        }
        if (fd < 0) {
            qCDebug(log_io_driver_unixfifo) << "Failed to open FIFO" << filename << ":"
                                            << std::strerror(errno);
            return -1;
        }
    }
    if (!isFIFO(fd)) {
        NotifyWake::interruptLoop(::close, fd);
        fd = -1;

        if (create) {
            ::unlink(filename.c_str());
            if (::mkfifo(filename.c_str(), 0666) == -1) {
                if (errno != EEXIST) {
                    qCDebug(log_io_driver_unixfifo) << "Unable to create FIFO" << filename << ":"
                                                    << std::strerror(errno);
                    return -1;
                }
            }

            fd = ::open(filename.c_str(), mode);
            if (fd < 0) {
                qCDebug(log_io_driver_unixfifo) << "Failed to open FIFO" << filename << ":"
                                                << std::strerror(errno);
                return -1;
            }
            if (!isFIFO(fd)) {
                NotifyWake::interruptLoop(::close, fd);
                fd = -1;
            }
        }
        if (fd < 0) {
            qCDebug(log_io_driver_unixfifo) << "Unable to open" << filename << "as a FIFO";
            return -1;
        }
    }

#ifndef O_CLOEXEC
    NotifyWake::setCloseOnExec(fd);
#endif

    return fd;
}

Handle unixFIFORead(std::string filename, bool create)
{
    class Backing : public Generic::Backing {
        std::string filename;
        bool create;
    public:
        virtual ~Backing() = default;

        Backing(std::string &&filename, bool create) : filename(std::move(filename)), create(create)
        { }

        std::unique_ptr<Generic::Block> block() override
        { return {}; }

        std::unique_ptr<Generic::Stream> stream() override
        {
            if (filename.empty())
                return {};

            class Stream : public Generic::Stream {
                UnixFDStream stream;
            public:
                explicit Stream(int fd) : stream(fd, true, false)
                {
                    read = stream.read;
                    ended = stream.ended;
                    writeStall = stream.writeStall;
                }

                virtual ~Stream() = default;

                void start() override
                { return stream.start(); }

                void write(const Util::ByteView &data) override
                { return stream.write(data); }

                void write(Util::ByteArray &&data) override
                { return stream.write(std::move(data)); }

                bool isEnded() const override
                { return stream.isEnded(); }

                void readStall(bool enable) override
                { return stream.readStall(enable); }
            };

            int fd = openFIFO(filename, create, O_RDONLY);
            if (fd < 0)
                return {};
            return std::unique_ptr<Generic::Stream>(new Stream(fd));
        }
    };
    return std::make_shared<Backing>(std::move(filename), create);
}

Handle unixFIFORead(const char *filename, bool create)
{ return unixFIFORead(std::string(filename), create); }

Handle unixFIFORead(const QString &filename, bool create)
{ return unixFIFORead(filename.toStdString(), create); }


Handle unixFIFOWrite(std::string filename, bool create)
{
    class Backing : public Generic::Backing {
        std::string filename;
        bool create;
    public:
        virtual ~Backing() = default;

        Backing(std::string &&filename, bool create) : filename(std::move(filename)), create(create)
        { }

        std::unique_ptr<Generic::Block> block() override
        { return {}; }

        std::unique_ptr<Generic::Stream> stream() override
        {
            if (filename.empty())
                return {};

            class Stream : public Generic::Stream {
                UnixFDStream stream;
            public:
                explicit Stream(int fd) : stream(fd, false, true)
                {
                    read = stream.read;
                    ended = stream.ended;
                    writeStall = stream.writeStall;
                }

                virtual ~Stream() = default;

                void start() override
                { return stream.start(); }

                void write(const Util::ByteView &data) override
                { return stream.write(data); }

                void write(Util::ByteArray &&data) override
                { return stream.write(std::move(data)); }

                bool isEnded() const override
                { return stream.isEnded(); }

                void readStall(bool enable) override
                { return stream.readStall(enable); }
            };

            int fd = openFIFO(filename, create, O_WRONLY);
            if (fd < 0)
                return {};
            return std::unique_ptr<Generic::Stream>(new Stream(fd));
        }
    };
    return std::make_shared<Backing>(std::move(filename), create);
}

Handle unixFIFOWrite(const char *filename, bool create)
{ return unixFIFOWrite(std::string(filename), create); }

Handle unixFIFOWrite(const QString &filename, bool create)
{ return unixFIFOWrite(filename.toStdString(), create); }

}

}
}

#else

namespace CPD3 {
namespace IO {

namespace Access {

CPD3IO_EXPORT Handle unixFIFORead(std::string, bool)
{ return Access::invalid(); }

Handle unixFIFORead(const char *, bool create)
{ return Access::invalid(); }

Handle unixFIFORead(const QString &, bool create)
{ return Access::invalid(); }

Handle unixFIFOWrite(std::string, bool create)
{ return Access::invalid(); }

Handle unixFIFOWrite(const char *, bool create)
{ return Access::invalid(); }

Handle unixFIFOWrite(const QString &, bool create)
{ return Access::invalid(); }

}

}
}

#endif