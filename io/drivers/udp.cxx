/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <cstring>
#include <QHostInfo>
#include <QUdpSocket>
#include <QLoggingCategory>

#include "udp.hxx"
#include "io/socket.hxx"
#include "core/qtcompat.hxx"

Q_LOGGING_CATEGORY(log_io_driver_udp, "cpd3.io.driver.udp", QtWarningMsg)

namespace CPD3 {
namespace IO {
namespace UDP {

Packet::Packet() : port(0)
{ }


Configuration::Configuration() : requireAllLocal(false),
                                 requireAllRemote(false),
                                 waitForResolution(false)
{ }

Configuration::~Configuration() = default;

std::unique_ptr<Stream> Configuration::operator()() const
{ return create(); }

Configuration &Configuration::targetPort(std::uint16_t port)
{
    for (auto &set : remoteAddresses) {
        set.second = port;
    }
    for (auto &set : remoteHosts) {
        set.second = port;
    }
    return *this;
}

Configuration &Configuration::listenPort(std::uint16_t port)
{
    for (auto &set : localAddresses) {
        set.second = port;
    }
    for (auto &set : localHosts) {
        set.second = port;
    }
    return *this;
}

Configuration &Configuration::listen(Configuration::AddressFamily address, std::uint16_t port)
{
    localAddresses.clear();
    localHosts.clear();

    switch (address) {
    case AddressFamily::Any:
        localAddresses.emplace_back(QHostAddress(QHostAddress::Any), port);
        break;
    case AddressFamily::Loopback:
#ifndef QT_NO_IPV4
        localAddresses.emplace_back(QHostAddress(QHostAddress::LocalHost), port);
#endif
#ifndef QT_NO_IPV6
        localAddresses.emplace_back(QHostAddress(QHostAddress::LocalHostIPv6), port);
#endif
        break;
    case AddressFamily::LoopbackIPv4:
        localAddresses.emplace_back(QHostAddress(QHostAddress::LocalHost), port);
    case AddressFamily::LoopbackIPv6:
        localAddresses.emplace_back(QHostAddress(QHostAddress::LocalHostIPv6), port);
    default:
        break;
    }
    return *this;
}

Configuration &Configuration::listen(const QHostAddress &address, std::uint16_t port)
{
    localAddresses.emplace_back(address, port);
    return *this;
}

Configuration &Configuration::listen(std::string address, std::uint16_t port)
{
    localHosts.emplace_back(std::move(address), port);
    return *this;
}

Configuration &Configuration::loopback(std::uint16_t port)
{
    remoteAddresses.clear();
    remoteHosts.clear();
#ifndef QT_NO_IPV4
    remoteAddresses.emplace_back(QHostAddress(QHostAddress::LocalHost), port);
#else
    remoteAddresses.emplace_back(QHostAddress(QHostAddress::LocalHostIPv6), port);
#endif
    return *this;
}

Configuration &Configuration::connect(const QHostAddress &address)
{
    std::uint16_t port = port_any;
    if (!remoteAddresses.empty()) {
        port = remoteAddresses.back().second;
    } else if (!remoteHosts.empty()) {
        port = remoteHosts.back().second;
    }
    return connect(address, port);
}

Configuration &Configuration::connect(const QHostAddress &address, std::uint16_t port)
{
    remoteAddresses.emplace_back(address, port);
    return *this;
}

Configuration &Configuration::connect(std::string address)
{
    std::uint16_t port = port_any;
    if (!remoteHosts.empty()) {
        port = remoteHosts.back().second;
    } else if (!remoteAddresses.empty()) {
        port = remoteAddresses.back().second;
    }
    return connect(std::move(address), port);
}

Configuration &Configuration::connect(std::string address, std::uint16_t port)
{
    remoteHosts.emplace_back(std::move(address), port);
    return *this;
}


Stream::Stream() = default;

Stream::~Stream() = default;

void Stream::sendTo(Util::ByteArray &&data, const QHostAddress &address, std::uint16_t port)
{ return sendTo(Util::ByteView(data), address, port); }

void Stream::sendTo(const QByteArray &data, const QHostAddress &address, std::uint16_t port)
{ return sendTo(Util::ByteView(data), address, port); }

void Stream::sendTo(const std::string &str, const QHostAddress &address, std::uint16_t port)
{ return sendTo(Util::ByteView(str.data(), str.size()), address, port); }

void Stream::sendTo(const QString &str, const QHostAddress &address, std::uint16_t port)
{ return sendTo(str.toUtf8(), address, port); }

void Stream::sendTo(const char *str, const QHostAddress &address, std::uint16_t port)
{
    if (!str)
        return;
    return sendTo(Util::ByteView(str, std::strlen(str)), address, port);
}

void Stream::sendTo(Util::ByteArray &&data, std::string host, std::uint16_t port)
{ return sendTo(Util::ByteView(data), std::move(host), port); }

void Stream::sendTo(const QByteArray &data, std::string host, std::uint16_t port)
{ return sendTo(Util::ByteView(data), std::move(host), port); }

void Stream::sendTo(const std::string &str, std::string host, std::uint16_t port)
{ return sendTo(Util::ByteView(str.data(), str.size()), std::move(host), port); }

void Stream::sendTo(const QString &str, std::string host, std::uint16_t port)
{ return sendTo(str.toUtf8(), std::move(host), port); }

void Stream::sendTo(const char *str, std::string host, std::uint16_t port)
{
    if (!str)
        return;
    return sendTo(Util::ByteView(str, std::strlen(str)), std::move(host), port);
}


namespace {

class UDPHandler : public Stream {
    std::mutex mutex;
    std::condition_variable cv;

    class Thread : public QThread {
        UDPHandler &handler;
    public:
        std::unique_ptr<QObject> receiver;
        std::vector<std::unique_ptr<QUdpSocket>> sockets;

        explicit Thread(UDPHandler &handler) : handler(handler)
        { setObjectName("UDPHandlerQThread"); }

        virtual ~Thread() = default;

    protected:
        void run() override
        {
            {
                std::lock_guard<std::mutex> lock(handler.mutex);
                receiver.reset(new QObject);
            }
            handler.cv.notify_all();

            QThread::exec();

            sockets.clear();
            receiver.reset();
        }
    };

    std::unique_ptr<Thread> thread;

    std::vector<std::pair<QHostAddress, std::uint16_t>> remoteTargets;


    void readFromSocket(QUdpSocket *socket)
    {
        Packet packet;
        for (;;) {
            packet.data.resize(65536);

            auto n = socket->readDatagram(packet.data.data<char *>(), packet.data.size(),
                                          &packet.address, &packet.port);
            if (n <= 0)
                break;

            packet.data.resize(static_cast<std::size_t>(n));

            read(packet.data);
            received(packet);
        }
    }

    bool bindLocal(const QHostAddress &address, std::uint16_t port)
    {
        Q_ASSERT(thread->receiver->thread() == QThread::currentThread());

        std::unique_ptr<QUdpSocket> socket(new QUdpSocket);
        if (!socket->bind(address, static_cast<quint16>(port))) {
            qCDebug(log_io_driver_udp) << "Bind failed for" << address.toString() << "port" << port
                                       << ":" << socket->errorString();
            return false;
        }

        qCDebug(log_io_driver_udp) << "Bound UDP socket to" << address.toString() << "port"
                                   << socket->localPort();

        QObject::connect(socket.get(), &QIODevice::readyRead, socket.get(),
                         std::bind(&UDPHandler::readFromSocket, this, socket.get()),
                         Qt::QueuedConnection);

        auto ptr = socket.get();
        thread->sockets.emplace_back(std::move(socket));
        readFromSocket(ptr);
        return true;
    }

    bool bindRemote(const QHostAddress &address, std::uint16_t port)
    {
        Q_ASSERT(thread->receiver->thread() == QThread::currentThread());

        if (port == 0)
            return false;
        if (address.isNull())
            return false;

        qCDebug(log_io_driver_udp) << "UDP default target added for" << address.toString() << "port"
                                   << port;

        remoteTargets.emplace_back(address, port);
        return true;
    }

    std::uint16_t effectiveLocalPort() const
    {
        Q_ASSERT(thread->receiver->thread() == QThread::currentThread());
        if (thread->sockets.empty())
            return 0;
        return thread->sockets.front()->localPort();
    }

    static bool isBetterForSending(const QHostAddress &target,
                                   const QHostAddress &existing,
                                   const QHostAddress &check)
    {
#if QT_VERSION >= QT_VERSION_CHECK(5, 8, 0)
        if (existing.isEqual(target, QHostAddress::StrictConversion))
            return false;
        if (check.isEqual(target, QHostAddress::StrictConversion))
            return true;
#else
        if (existing == target)
            return false;
        if (check == target)
            return true;
#endif
        switch (target.protocol()) {
        case QAbstractSocket::IPv4Protocol:
            if (check.protocol() == QAbstractSocket::IPv6Protocol)
                return false;
            break;
        case QAbstractSocket::IPv6Protocol:
            if (check.protocol() == QAbstractSocket::IPv4Protocol)
                return false;
            break;
        default:
            break;
        }

#if QT_VERSION >= QT_VERSION_CHECK(5, 11, 0)
        if (target.isLinkLocal()) {
            if (!check.isLinkLocal())
                return false;
            if (check.scopeId() == target.scopeId()) {
                if (!existing.isLinkLocal() || existing.scopeId() != target.scopeId())
                    return true;
            }
            return false;
        } else {
            if (check.isLinkLocal())
                return false;
        }
#endif
        if (!target.scopeId().isEmpty()) {
            if (check.scopeId() == target.scopeId()) {
                if (existing.scopeId() != target.scopeId())
                    return true;
            }
        }

        if (check == QHostAddress(QHostAddress::Any)) {
            return existing != QHostAddress(QHostAddress::Any);
        }
        if (target.protocol() != QAbstractSocket::IPv6Protocol &&
                check == QHostAddress(QHostAddress::AnyIPv4)) {
            return existing != QHostAddress(QHostAddress::AnyIPv4);
        }
        if (target.protocol() != QAbstractSocket::IPv4Protocol &&
                check == QHostAddress(QHostAddress::AnyIPv6)) {
            return existing != QHostAddress(QHostAddress::AnyIPv6);
        }

        /* Now just heuristically prefer some basic subnet local addresses */
        switch (target.protocol()) {
        case QAbstractSocket::IPv4Protocol:
            if (target.isInSubnet(check, 8)) {
                return !target.isInSubnet(existing, 8);
            }
            break;
        case QAbstractSocket::IPv6Protocol:
            if (target.isInSubnet(check, 64)) {
                return !target.isInSubnet(existing, 64);
            }
            break;
        default:
            break;
        }


        return false;
    }

    void executeWrite(Util::ByteArray &&data, const QHostAddress &address, std::uint16_t port)
    {
        Q_ASSERT(thread->receiver->thread() == QThread::currentThread());
        QUdpSocket *bestSocket = nullptr;
        for (const auto &socket : thread->sockets) {
            if (!bestSocket) {
                bestSocket = socket.get();
                continue;
            }
            if (isBetterForSending(address, bestSocket->localAddress(), socket->localAddress())) {
                bestSocket = socket.get();
            }
        }
        if (!bestSocket)
            return;
        bestSocket->writeDatagram(data.data<const char *>(), data.size(), address, port);
    }

    void executeWrite(Util::ByteArray &&data)
    {
        Q_ASSERT(thread->receiver->thread() == QThread::currentThread());
        if (remoteTargets.empty())
            return;
        auto addr = remoteTargets.begin();
        for (auto end = remoteTargets.end() - 1; addr != end; ++addr) {
            executeWrite(Util::ByteArray(data), addr->first, addr->second);
        }
        return executeWrite(std::move(data), addr->first, addr->second);
    }

    void executeWrite(Util::ByteArray &&data, const QHostInfo &host, std::uint16_t port)
    {
        auto addresses = host.addresses();
        if (addresses.empty())
            return;
        auto addr = addresses.begin();
        for (auto end = addresses.end() - 1; addr != end; ++addr) {
            executeWrite(Util::ByteArray(data), *addr, port);
        }
        return executeWrite(std::move(data), *addr, port);
    }

    void transferredWrite(Util::ByteArray &&data)
    {
        struct Local {
            UDPHandler &handler;
            Util::ByteArray data;

            Local(UDPHandler &handler, Util::ByteArray &&data) : handler(handler),
                                                                 data(std::move(data))
            { }
        };

        auto local = std::make_shared<Local>(*this, std::move(data));
        Threading::runQueuedFunctor(thread->receiver.get(), [local]() {
            local->handler.executeWrite(std::move(local->data));
        });
    }

    void transferredWrite(Util::ByteArray &&data, const QHostAddress &address, std::uint16_t port)
    {
        struct Local {
            UDPHandler &handler;
            Util::ByteArray data;
            QHostAddress address;
            std::uint16_t port;

            Local(UDPHandler &handler,
                  Util::ByteArray &&data,
                  const QHostAddress &address,
                  std::uint16_t port) : handler(handler),
                                        data(std::move(data)),
                                        address(address),
                                        port(port)
            { }
        };

        auto local = std::make_shared<Local>(*this, std::move(data), address, port);
        Threading::runQueuedFunctor(thread->receiver.get(), [local]() {
            local->handler.executeWrite(std::move(local->data), local->address, local->port);
        });
    }

    void transferredWrite(Util::ByteArray &&data, std::string &&host, std::uint16_t port)
    {
        struct Local {
            UDPHandler &handler;
            Util::ByteArray data;
            std::string host;
            std::uint16_t port;

            Local(UDPHandler &handler,
                  Util::ByteArray &&data,
                  std::string &&host,
                  std::uint16_t port) : handler(handler),
                                        data(std::move(data)),
                                        host(std::move(host)),
                                        port(port)
            { }
        };

        auto local = std::make_shared<Local>(*this, std::move(data), std::move(host), port);
        Socket::queuedResolve(local->host, thread->receiver.get(), [local](const QHostInfo &host) {
            if (host.error() != QHostInfo::NoError) {
                qCDebug(log_io_driver_udp) << "Name resolution for" << local->host << "failed:"
                                           << host.errorString();
            }
            local->handler.executeWrite(std::move(local->data), host, local->port);
        });
    }

public:
    UDPHandler() : thread(new Thread(*this))
    {
        thread->start();
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [this]() { return thread->receiver.get() != nullptr; });
        }
    }

    virtual ~UDPHandler()
    {
        thread->quit();
        thread->wait();
    }

    bool initialize(const Configuration &config)
    {
        struct Context {
            /* Reference is only used by the blocking call, so it's safe to use rather
             * than making a copy */
            const Configuration &config;

            std::size_t remainingLocalResolvers;
            std::size_t failedLocalResolvers;
            std::size_t failedLocalBinds;
            std::size_t completedLocalBinds;

            std::size_t remainingRemoteResolvers;
            std::size_t failedRemoteResolvers;
            std::size_t failedRemoteBinds;

            explicit Context(const Configuration &config) : config(config),
                                                            remainingLocalResolvers(0),
                                                            failedLocalResolvers(0),
                                                            failedLocalBinds(0),
                                                            completedLocalBinds(0),
                                                            remainingRemoteResolvers(0),
                                                            failedRemoteResolvers(0),
                                                            failedRemoteBinds(0)
            { }
        };
        auto context = std::make_shared<Context>(config);

        for (const auto &resolve : config.localHosts) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                context->remainingLocalResolvers++;
            }
            Socket::queuedResolve(resolve.first, thread->receiver.get(),
                                  [this, resolve, context](const QHostInfo &host) {
                                      for (const auto &addr : host.addresses()) {
                                          if (!bindLocal(addr, resolve.second)) {
                                              std::lock_guard<std::mutex> lock(mutex);
                                              context->failedLocalBinds++;
                                          } else {
                                              std::lock_guard<std::mutex> lock(mutex);
                                              context->completedLocalBinds++;
                                          }
                                      }

                                      if (host.error() != QHostInfo::NoError) {
                                          qCDebug(log_io_driver_udp) << "Name resolution for"
                                                                     << resolve.first << "failed:"
                                                                     << host.errorString();
                                          std::lock_guard<std::mutex> lock(mutex);
                                          context->failedLocalResolvers++;
                                      }

                                      {
                                          std::lock_guard<std::mutex> lock(mutex);
                                          context->remainingLocalResolvers--;
                                      }
                                      cv.notify_all();
                                  });
        }

        for (const auto &resolve : config.remoteHosts) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                context->remainingRemoteResolvers++;
            }
            Socket::queuedResolve(resolve.first, thread->receiver.get(),
                                  [this, resolve, context](const QHostInfo &host) {
                                      for (const auto &addr : host.addresses()) {
                                          if (!bindRemote(addr, resolve.second)) {
                                              std::lock_guard<std::mutex> lock(mutex);
                                              context->failedRemoteBinds++;
                                          }
                                      }

                                      if (host.error() != QHostInfo::NoError) {
                                          qCDebug(log_io_driver_udp) << "Name resolution for"
                                                                     << resolve.first << "failed:"
                                                                     << host.errorString();
                                          std::lock_guard<std::mutex> lock(mutex);
                                          context->failedRemoteResolvers++;
                                      }

                                      {
                                          std::lock_guard<std::mutex> lock(mutex);
                                          context->remainingRemoteResolvers--;
                                      }
                                      cv.notify_all();
                                  });
        }

        Threading::runBlockingFunctor(thread->receiver.get(), [this, context]() {
            for (const auto &add : context->config.localAddresses) {
                if (!bindLocal(add.first, add.second)) {
                    std::lock_guard<std::mutex> lock(mutex);
                    context->failedLocalBinds++;
                } else {
                    std::lock_guard<std::mutex> lock(mutex);
                    context->completedLocalBinds++;
                }
            }
            for (const auto &add : context->config.remoteAddresses) {
                if (!bindRemote(add.first, add.second)) {
                    std::lock_guard<std::mutex> lock(mutex);
                    context->failedRemoteBinds++;
                }
            }
        });

        std::unique_lock<std::mutex> lock(mutex);

        if (config.waitForResolution) {
            cv.wait(lock, [context]() {
                return context->remainingLocalResolvers == 0 &&
                        context->remainingRemoteResolvers == 0;
            });
        }

        if (config.requireAllLocal) {
            cv.wait(lock, [context]() {
                return context->remainingLocalResolvers == 0;
            });
            if (context->failedLocalResolvers || context->failedLocalBinds)
                return false;
        }

        if (config.requireAllRemote) {
            cv.wait(lock, [context]() {
                return context->remainingRemoteResolvers == 0;
            });
            if (context->failedRemoteResolvers || context->failedRemoteBinds)
                return false;
        }

        /* Need at least one socket before we can do anything */
        cv.wait(lock, [context]() {
            return context->completedLocalBinds != 0 || context->remainingLocalResolvers == 0;
        });
        return context->completedLocalBinds != 0;
    }

    void start() override
    { }

    void readStall(bool) override
    { }

    bool isEnded() const override
    { return false; }

    std::uint16_t localPort() const override
    {
        if (thread->receiver->thread() == QThread::currentThread())
            return effectiveLocalPort();
        std::uint16_t result = 0;
        Threading::runBlockingFunctor(thread->receiver.get(), [this, &result]() {
            result = effectiveLocalPort();
        });
        return result;
    }

    void write(const Util::ByteView &data) override
    { transferredWrite(Util::ByteArray(data)); }

    void write(Util::ByteArray &&data) override
    { transferredWrite(std::move(data)); }

    void sendTo(const Util::ByteView &data,
                const QHostAddress &address,
                std::uint16_t port) override
    { transferredWrite(Util::ByteArray(data), address, port); }

    void sendTo(Util::ByteArray &&data, const QHostAddress &address, std::uint16_t port) override
    { transferredWrite(std::move(data), address, port); }

    void sendTo(const Util::ByteView &data, std::string host, std::uint16_t port) override
    { transferredWrite(Util::ByteArray(data), std::move(host), port); }

    void sendTo(Util::ByteArray &&data, std::string host, std::uint16_t port) override
    { transferredWrite(std::move(data), std::move(host), port); }
};

}


std::unique_ptr<Stream> Configuration::create() const
{
    std::unique_ptr<UDPHandler> stream(new UDPHandler);
    if (!stream->initialize(*this))
        return {};
    return std::move(stream);
}

}
}
}