/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <algorithm>
#include <cstring>
#include <thread>
#include <chrono>
#include <unordered_set>
#include <unordered_map>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <QLoggingCategory>
#include <QRegularExpression>
#include <QDir>
#include <QFileInfo>

#include "serial.hxx"
#include "unixfdstream.hxx"
#include "file.hxx"
#include "core/qtcompat.hxx"

#if defined(Q_OS_LINUX)

/*
 * From asm/termbits.h: we can't include both termios.h and the linux definitions because
 * they aren't gaurded against each other.
 */
extern "C" {
struct termios2 {
    tcflag_t c_iflag;        /* input mode flags */
    tcflag_t c_oflag;        /* output mode flags */
    tcflag_t c_cflag;        /* control mode flags */
    tcflag_t c_lflag;        /* local mode flags */
    cc_t c_line;            /* line discipline */
    cc_t c_cc[NCCS];        /* control characters */
    speed_t c_ispeed;        /* input speed */
    speed_t c_ospeed;        /* output speed */
};
#define    BOTHER 0010000
}

#elif defined(Q_OS_DARWIN) && defined(HAVE_IOKIT)

#include <CoreFoundation/CoreFoundation.h>
#include <IOKit/IOKitLib.h>
#include <IOKit/serial/IOSerialKeys.h>

#endif

Q_LOGGING_CATEGORY(log_io_driver_serial, "cpd3.io.driver.serial", QtWarningMsg)


namespace CPD3 {
namespace IO {

namespace {

namespace {
using TermiosBaud = std::pair<int, ::speed_t>;
static const TermiosBaud bauds[] = {
#ifdef B50
        {50, B50},
#endif
#ifdef B75
        {75, B75},
#endif
#ifdef B134
        {134, B134},
#endif
#ifdef B150
        {150, B150},
#endif
        {150, B150},
#ifdef B200
        {200, B200},
#endif
#ifdef B300
        {300, B300},
#endif
#ifdef B600
        {600, B600},
#endif
#ifdef B1200
        {1200, B1200},
#endif
#ifdef B1800
        {1800, B1800},
#endif
#ifdef B2400
        {2400, B2400},
#endif
#ifdef B4800
        {4800, B4800},
#endif
#ifdef B9600
        {9600, B9600},
#endif
#ifdef B19200
        {19200, B19200},
#endif
#ifdef B38400
        {38400, B38400},
#endif
#ifdef B57600
        {57600, B57600},
#endif
#ifdef B115200
        {115200, B115200},
#endif
#ifdef B230400
        {230400, B230400},
#endif
};

static bool compareBaud(const TermiosBaud &a, const TermiosBaud &b)
{ return a.first < b.first; }

static const TermiosBaud &convertBaud(int b)
{
    auto start = &bauds[0];
    auto end = start + sizeof(bauds) / sizeof(bauds[0]);
    Q_ASSERT(start != end);
    auto lower = std::lower_bound(start, end, TermiosBaud(b, 0), compareBaud);
    if (lower == end)
        return *(end - 1);
    if (lower + 1 == end)
        return *lower;
    if (std::abs((lower + 1)->first - b) < std::abs(lower->first - b))
        return *(lower + 1);
    return *lower;
}
}

class UnixSerialStream : public Serial::Stream {
    class StreamHandler : public UnixFDStream {
        using Clock = std::chrono::steady_clock;
        int fd;
        /* Never accessed while locked, so safe without a mutex */
        bool rtsControlEnabled;
        bool rtsReceiveDiscard;
        bool rtsInvert;
        bool inBlankTime;
        bool wasWriting;
        bool inReadDiscard;
        std::size_t writtenSinceStart;
        std::chrono::duration<double> blankAfterWrite;
        std::chrono::duration<double> blankAfterPerCharacter;
        Clock::time_point blankEndTime;

        bool setRTS(bool raise)
        {
#if defined(TIOCMGET) && defined(TIOCMSET) && defined(TIOCM_RTS)
            int bits = 0;

            if (::ioctl(fd, TIOCMGET, &bits) == -1) {
                qCDebug(log_io_driver_serial) << "Error in RTS TIOCMGET:" << std::strerror(errno);
                return false;
            }

            if (!raise) {
                bits &= ~TIOCM_RTS;
            } else {
                bits |= TIOCM_RTS;
            }

            if (::ioctl(fd, TIOCMSET, &bits) == -1) {
                qCDebug(log_io_driver_serial) << "Error in RTS TIOCMSET:" << std::strerror(errno);
                return false;
            }

            return true;
#else
            Q_UNUSED(raise);
            qCDebug(log_io_driver_serial) << "RTS control not supported";
            return false;
#endif
        }

    public:

        explicit StreamHandler(int fd) : UnixFDStream(fd, true, true),
                                         fd(fd),
                                         rtsControlEnabled(false),
                                         rtsReceiveDiscard(false),
                                         rtsInvert(false),
                                         inBlankTime(false),
                                         wasWriting(false),
                                         inReadDiscard(false),
                                         writtenSinceStart(0),
                                         blankAfterWrite(0),
                                         blankAfterPerCharacter(0)
        { }

        virtual ~StreamHandler() = default;

        void disableRTSControl()
        {
            if (!rtsControlEnabled)
                return;
            rtsControlEnabled = false;
            setRTS(true);
        }

        bool enableRTSOnTransmit(double blankAfter = 0.0,
                                 double blankPerCharacter = 0.0,
                                 bool discardReceive = false,
                                 bool invert = false)
        {
            if (!setRTS(invert))
                return false;

            rtsControlEnabled = true;
            rtsReceiveDiscard = discardReceive;
            rtsInvert = invert;
            wasWriting = false;
            inBlankTime = false;
            blankEndTime = Clock::now();

            if (blankAfter > 0.0) {
                blankAfterWrite = std::chrono::duration<double>(blankAfter);
            } else {
                blankAfterWrite = std::chrono::duration<double>(0);
            }

            if (blankPerCharacter > 0.0) {
                blankAfterPerCharacter = std::chrono::duration<double>(blankPerCharacter);
            } else {
                blankAfterPerCharacter = std::chrono::duration<double>(0);
            }

            return true;
        }

    protected:
        int waitEntry(bool &reading, bool &writing) override
        {
            inReadDiscard = false;
            if (!rtsControlEnabled)
                return UnixFDStream::waitEntry(reading, writing);

            if (inBlankTime) {
                auto now = Clock::now();
                if (now < blankEndTime) {
                    writing = false;
                    inReadDiscard = rtsReceiveDiscard;
                    if (inReadDiscard)
                        reading = true;

                    auto remaining = std::chrono::duration_cast<std::chrono::milliseconds>(
                            blankEndTime - now);
                    if (remaining.count() < 1)
                        return 1;
                    return static_cast<int>(remaining.count());
                }

                setRTS(rtsInvert);
                inBlankTime = false;
            }

            if (!writing && wasWriting) {
                wasWriting = false;

                auto drainStart = Clock::now();
                if (::tcdrain(fd) == -1 && errno != EINTR) {
                    qCDebug(log_io_driver_serial) << "Failed to drain write buffer:"
                                                  << std::strerror(errno);
                }

                if (blankAfterWrite.count() > 0 || blankAfterPerCharacter.count() > 0) {
                    inBlankTime = true;
                    inReadDiscard = rtsReceiveDiscard;
                    if (inReadDiscard)
                        reading = true;

                    auto now = Clock::now();
                    blankEndTime =
                            now + std::chrono::duration_cast<Clock::duration>(blankAfterWrite);

                    if (blankAfterPerCharacter.count() > 0) {
                        auto charRemaining =
                                std::chrono::duration_cast<std::chrono::duration<double>>(
                                        now - drainStart);
                        charRemaining += blankAfterPerCharacter * writtenSinceStart;
                        if (charRemaining.count() > 0) {
                            blankEndTime +=
                                    std::chrono::duration_cast<Clock::duration>(charRemaining);
                        }
                    }

                    auto remaining = std::chrono::duration_cast<std::chrono::milliseconds>(
                            blankEndTime - now);
                    if (remaining.count() < 1)
                        return 1;
                    return static_cast<int>(remaining.count());
                } else {
                    setRTS(rtsInvert);
                }
            } else if (writing && !wasWriting) {
                wasWriting = true;
                writtenSinceStart = 0;
                setRTS(!rtsInvert);
            }

            if (writing) {
                inReadDiscard = rtsReceiveDiscard;
                if (inReadDiscard)
                    reading = true;
            }

            return UnixFDStream::waitEntry(reading, writing);
        }

        void dataRead(const Util::ByteArray &data) override
        {
            if (inReadDiscard)
                return;
            return UnixFDStream::dataRead(data);
        }

        void dataWritten(std::size_t count) override
        {
            writtenSinceStart += count;
            return UnixFDStream::dataWritten(count);
        }
    };

    StreamHandler stream;

    int effectiveBaud;
    int effectiveDataBits;
    int effectiveStopBits;
    Parity effectiveParity;
    std::string lastError;

    bool beginTermios(UnixFDStream::Lock &lock, struct ::termios &tio)
    {
        if (lock.fd() < 0) {
            lastError = "not open";
            return false;
        }
        if (::tcgetattr(lock.fd(), &tio) != 0) {
            lastError = "tcgetattr: ";
            lastError += std::strerror(errno);
            return false;
        }
        return true;
    }

    bool endTermios(UnixFDStream::Lock &lock, struct ::termios &tio)
    {
        if (lock.fd() < 0) {
            lastError = "not open";
            return false;
        }
        if (::tcsetattr(lock.fd(), TCSANOW, &tio) != 0) {
            lastError = "tcgetattr: ";
            lastError += std::strerror(errno);
            return false;
        }
        return true;
    }

#ifdef Q_OS_LINUX

    void clearCustomBaud(UnixFDStream::Lock &lock)
    {
        if (lock.fd() < 0)
            return;

        struct ::termios2 tio;

        if (::ioctl(lock.fd(), TCGETS2, &tio) == -1)
            return;

        if ((tio.c_cflag & BOTHER) == 0)
            return;

        tio.c_cflag &= ~BOTHER;
        tio.c_cflag |= CBAUD;
        ::ioctl(lock.fd(), TCSETS2, &tio);
    }

    bool setCustomBaud(UnixFDStream::Lock &lock, int rate)
    {
        if (lock.fd() < 0)
            return false;

        struct ::termios2 tio;
        if (::ioctl(lock.fd(), TCGETS2, &tio) == -1)
            return false;

        tio.c_cflag &= ~CBAUD;
        tio.c_cflag |= BOTHER;
        tio.c_ispeed = rate;
        tio.c_ospeed = rate;

        return ::ioctl(lock.fd(), TCSETS2, &tio) != -1;
    }

#else
    void clearCustomBaud(UnixFDStream::Lock &)
    { }

    bool setCustomBaud(UnixFDStream::Lock &, int)
    { return false; }
#endif

public:
    UnixSerialStream(std::string name, int fd) : Serial::Stream(std::move(name)),
                                                 stream(fd),
                                                 effectiveBaud(0),
                                                 effectiveDataBits(0),
                                                 effectiveStopBits(0),
                                                 effectiveParity(Parity::None)
    {
        read = stream.read;
        ended = stream.ended;
        writeStall = stream.writeStall;
    }

    virtual ~UnixSerialStream() = default;

    void start() override
    { return stream.start(); }

    void write(const Util::ByteView &data) override
    { return stream.write(data); }

    void write(Util::ByteArray &&data) override
    { return stream.write(std::move(data)); }

    bool isEnded() const override
    { return stream.isEnded(); }

    void readStall(bool enable) override
    { return stream.readStall(enable); }


    std::string describePort() const override
    {
        auto desc = portName();
        if (Util::starts_with(desc, "/dev/"))
            desc.erase(0, 5);

        desc += ":";
        if (effectiveBaud > 0) {
            desc += std::to_string(effectiveBaud);
        }
        switch (effectiveParity) {
        case Parity::None:
            desc += "N";
            break;
        case Parity::Even:
            desc += "E";
            break;
        case Parity::Odd:
            desc += "O";
            break;
        case Parity::Mark:
            desc += "M";
            break;
        case Parity::Space:
            desc += "S";
            break;
        }
        if (effectiveDataBits > 0) {
            desc += std::to_string(effectiveDataBits);
        }
        if (effectiveStopBits > 0) {
            desc += std::to_string(effectiveStopBits);
        }
        return desc;
    }

    std::string describeError() const override
    { return lastError; }

    bool setBaud(int rate) override
    {
        UnixFDStream::Lock lock(stream);

        auto baud = convertBaud(rate);

        if (baud.first != rate) {
            if (setCustomBaud(lock, rate)) {
                effectiveBaud = rate;
                return true;
            }
            clearCustomBaud(lock);
        } else {
            clearCustomBaud(lock);
        }

        struct ::termios tio;
        if (!beginTermios(lock, tio))
            return false;

        if (::cfsetispeed(&tio, baud.second) == -1) {
            lastError = "cfsetispeed: ";
            lastError += std::strerror(errno);
            return false;
        }
        if (::cfsetospeed(&tio, baud.second) == -1) {
            lastError = "cfsetospeed: ";
            lastError += std::strerror(errno);
            return false;
        }

        if (!endTermios(lock, tio))
            return false;
        effectiveBaud = baud.first;
        return true;
    }

    bool setDataBits(int bits) override
    {
        UnixFDStream::Lock lock(stream);
        struct ::termios tio;
        if (!beginTermios(lock, tio))
            return false;

        tio.c_cflag &= ~(CSIZE);
        switch (bits) {
#ifdef CS5
        case 5:
            tio.c_cflag |= CS5;
            break;
#endif
#ifdef CS6
        case 6:
            tio.c_cflag |= CS6;
            break;
#endif
#ifdef CS7
        case 7:
            tio.c_cflag |= CS7;
            break;
#endif
#ifdef CS8
        case 8:
            tio.c_cflag |= CS8;
            break;
#endif
        default:
            lastError = "Unsupported number of data bits (" + std::to_string(bits) + ")";
            return false;
        }

        if (!endTermios(lock, tio))
            return false;
        effectiveDataBits = bits;
        return true;
    }

    bool setStopBits(int bits) override
    {
        UnixFDStream::Lock lock(stream);
        struct ::termios tio;
        if (!beginTermios(lock, tio))
            return false;

        tio.c_cflag &= ~(CSTOPB);
        switch (bits) {
        case 1:
            break;
        case 2:
            tio.c_cflag |= CSTOPB;
            break;
        default:
            lastError = "Unsupported number of stop bits (" + std::to_string(bits) + ")";
            return false;
        }

        if (!endTermios(lock, tio))
            return false;
        effectiveStopBits = bits;
        return true;
    }

    bool setParity(Parity parity) override
    {
        UnixFDStream::Lock lock(stream);
        struct ::termios tio;
        if (!beginTermios(lock, tio))
            return false;

        tio.c_cflag &= ~(PARENB | PARODD);
#ifdef CMSPAR
        tio.c_cflag &= ~CMSPAR;
#endif
        tio.c_iflag |= IGNPAR;

        switch (parity) {
        case Parity::None:
            break;
        case Parity::Even:
            tio.c_cflag |= PARENB;
            tio.c_iflag &= ~IGNPAR;
            break;
        case Parity::Odd:
            tio.c_cflag |= PARENB | PARODD;
            tio.c_iflag &= ~IGNPAR;
            break;
        case Parity::Mark:
#ifdef CMSPAR
            tio.c_cflag |= PARENB | CMSPAR | PARODD;
            tio.c_iflag &= ~IGNPAR;
            break;
#else
            lastError = "Unsupported parity (mark)";
            return false;
#endif
        case Parity::Space:
#ifdef CMSPAR
            tio.c_cflag |= PARENB | CMSPAR;
            tio.c_iflag &= ~IGNPAR;
            break;
#else
            lastError = "Unsupported parity (space)";
            return false;
#endif
        }

        if (!endTermios(lock, tio))
            return false;
        effectiveParity = parity;
        return true;
    }

    bool setNoHardwareFlowControl() override
    {
        UnixFDStream::Lock lock(stream);
        struct ::termios tio;
        if (!beginTermios(lock, tio))
            return false;

#ifdef CRTSCTS
        tio.c_iflag &= ~(CRTSCTS);
#endif
#ifdef CLOCAL
        tio.c_cflag |= CLOCAL;
#endif

        stream.disableRTSControl();
        return endTermios(lock, tio);
    }

    bool setRS232StandardFlowControl() override
    {
        UnixFDStream::Lock lock(stream);
        struct ::termios tio;
        if (!beginTermios(lock, tio))
            return false;

#ifdef CRTSCTS
        tio.c_iflag |= CRTSCTS;
#endif
#ifdef CLOCAL
        tio.c_cflag &= ~CLOCAL;
#endif

        stream.disableRTSControl();
        return endTermios(lock, tio);
    }


#if defined(TIOCMGET) && defined(TIOCM_RTS)

    bool setExplicitRTSOnTransmit(double blankAfter = 0.0,
                                  double blankPerCharacter = 0.0,
                                  bool discardReceive = false,
                                  bool invert = false) override
    {
        UnixFDStream::Lock lock(stream);
        struct ::termios tio;
        if (!beginTermios(lock, tio))
            return false;

#ifdef CRTSCTS
        tio.c_iflag &= ~(CRTSCTS);
#endif
#ifdef CLOCAL
        tio.c_cflag |= CLOCAL;
#endif

        if (!endTermios(lock, tio))
            return false;

        return stream.enableRTSOnTransmit(blankAfter, blankPerCharacter, discardReceive, invert);
    }

#else
    bool setExplicitRTSOnTransmit(double, double, bool, bool) override
    { return false; }
#endif


    bool setSoftwareFlowControl(SoftwareFlowControl control, bool enableResume = false) override
    {
        UnixFDStream::Lock lock(stream);
        struct ::termios tio;
        if (!beginTermios(lock, tio))
            return false;

        tio.c_iflag &= ~(IXON | IXOFF | IXANY);
        switch (control) {
        case SoftwareFlowControl::Disable:
            break;
        case SoftwareFlowControl::Both:
            tio.c_iflag |= (IXON | IXOFF);
            break;
        case SoftwareFlowControl::XON:
            tio.c_iflag |= IXON;
            break;
        case SoftwareFlowControl::XOFF:
            tio.c_iflag |= IXOFF;
            break;
        }
        if (enableResume) {
            tio.c_iflag |= IXANY;
        }

        return endTermios(lock, tio);
    }

    bool flushPort(bool blank = true) override
    {
        UnixFDStream::Lock lock(stream);
        if (lock.fd() < 0) {
            lastError = "not open";
            return false;
        }
        bool ok = true;

        if (::tcflush(lock.fd(), TCIOFLUSH) == -1) {
            lastError = "tcflush: ";
            lastError += std::strerror(errno);
            ok = false;
        }

        std::chrono::duration<double> sleepTime(0.1);
        if (blank) {
            auto start = std::chrono::steady_clock::now();
            if (::tcsendbreak(lock.fd(), 0) == -1) {
                lastError = "tcsendbreak: ";
                lastError += std::strerror(errno);
                ok = false;
            }
            auto end = std::chrono::steady_clock::now();
            std::chrono::duration<double> elapsed = end - start;
            auto n = elapsed.count();
            if (n < 0.5) {
                sleepTime += std::chrono::duration<double>(0.5 - n);
            }
        }

        if (effectiveBaud > 0) {
            static constexpr std::uint_fast64_t symbolsToSleep = 10 * 2; /* ~2 frames */
            auto hzToSleep = static_cast<std::uint_fast64_t>(effectiveBaud) * symbolsToSleep;
            sleepTime += std::chrono::duration<double>(1.0 / hzToSleep);
        }

        std::this_thread::sleep_for(sleepTime);

        std::size_t nFlush = 4096;
        if (effectiveBaud > 0) {
            auto flushBytes = static_cast<std::size_t>(effectiveBaud * sleepTime.count());
            flushBytes /= 10;
            flushBytes += 1;
            if (flushBytes > nFlush) {
                nFlush = flushBytes;
            }
        }

        lock.flush(nFlush);
        return ok;
    }


#if defined(TIOCMGET) && defined(TIOCMSET) && defined(TIOCM_RTS)

    bool pulseRTS(double width = 0.05, bool invert = false) override
    {
        UnixFDStream::Lock lock(stream);
        if (lock.fd() < 0) {
            lastError = "not open";
            return false;
        }

        int bits = 0;

        if (::ioctl(lock.fd(), TIOCMGET, &bits) == -1) {
            lastError = "ioctl(TIOCMGET): ";
            lastError += std::strerror(errno);
            return false;
        }

        if (width > 0.0) {
            if (invert) {
                bits |= TIOCM_RTS;
            } else {
                bits &= ~TIOCM_RTS;
            }
            if (::ioctl(lock.fd(), TIOCMSET, &bits) == -1) {
                lastError = "ioctl(TIOCMSET): ";
                lastError += std::strerror(errno);
                return false;
            }

            std::this_thread::sleep_for(std::chrono::duration<double>(width));
        }


        if (invert) {
            bits &= ~TIOCM_RTS;
        } else {
            bits |= TIOCM_RTS;
        }
        if (::ioctl(lock.fd(), TIOCMSET, &bits) == -1) {
            lastError = "ioctl(TIOCMSET): ";
            lastError += std::strerror(errno);
            return false;
        }
        return true;
    }

#else
    bool pulseRTS(double, bool) override
    { return false; }
#endif

#if defined(TIOCMGET) && defined(TIOCMSET) && defined(TIOCM_DTR)

    bool pulseDTR(double width = 0.05, bool invert = false) override
    {
        UnixFDStream::Lock lock(stream);
        if (lock.fd() < 0) {
            lastError = "not open";
            return false;
        }

        int bits = 0;

        if (::ioctl(lock.fd(), TIOCMGET, &bits) == -1) {
            lastError = "ioctl(TIOCMGET): ";
            lastError += std::strerror(errno);
            return false;
        }

        if (width > 0.0) {
            if (invert) {
                bits |= TIOCM_DTR;
            } else {
                bits &= ~TIOCM_DTR;
            }
            if (::ioctl(lock.fd(), TIOCMSET, &bits) == -1) {
                lastError = "ioctl(TIOCMSET): ";
                lastError += std::strerror(errno);
                return false;
            }

            std::this_thread::sleep_for(std::chrono::duration<double>(width));
        }


        if (invert) {
            bits &= ~TIOCM_DTR;
        } else {
            bits |= TIOCM_DTR;
        }
        if (::ioctl(lock.fd(), TIOCMSET, &bits) == -1) {
            lastError = "ioctl(TIOCMSET): ";
            lastError += std::strerror(errno);
            return false;
        }
        return true;
    }

#else
    bool pulseDTR(double, bool) override
    { return false; }
#endif
};

}

namespace Access {

std::shared_ptr<Serial::Backing> serial(std::string name)
{
    class Backing : public Serial::Backing {
    public:
        Backing(std::string &&name) : Serial::Backing(std::move(name))
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        {
            int flags = O_RDWR | O_NOCTTY | O_NONBLOCK;
#ifdef O_CLOEXEC
            flags |= O_CLOEXEC;
#endif
            int fd = ::open(portName().c_str(), flags);
            if (fd < 0) {
                qCDebug(log_io_driver_serial) << "Failed to open" << portName() << ":"
                                              << std::strerror(errno);
                return {};
            }
#ifndef O_CLOEXEC
            NotifyWake::setCloseOnExec(fd);
#endif

            struct ::termios tio;
            if (::tcgetattr(fd, &tio) != 0) {
                qCDebug(log_io_driver_serial) << "Failed to get termios on" << portName() << ":"
                                              << std::strerror(errno);
                NotifyWake::interruptLoop(::close, fd);
                return {};
            }

            /* Boilerplate for raw (non-escaped) with no echo or other buffering. */
            tio.c_iflag &= ~(BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);
            tio.c_iflag |= IGNBRK;
            tio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG | IEXTEN);
            tio.c_oflag &= ~OPOST;
            tio.c_cflag |= CREAD;
            tio.c_cc[VMIN] = 0;
            tio.c_cc[VTIME] = 0;

            if (::tcsetattr(fd, TCSANOW, &tio) != 0) {
                qCDebug(log_io_driver_serial) << "Failed to set termios on" << portName() << ":"
                                              << std::strerror(errno);
                NotifyWake::interruptLoop(::close, fd);
                return {};
            }

            return std::unique_ptr<Generic::Stream>(new UnixSerialStream(portName(), fd));
        }
    };
    return std::make_shared<Backing>(std::move(name));
}

}

namespace Serial {

bool Backing::isValid(const std::string &name)
{
    struct ::stat s;
    if (::stat(name.c_str(), &s) == -1)
        return false;
    if (!S_ISCHR(s.st_mode))
        return false;

    int flags = O_RDWR | O_NOCTTY | O_NONBLOCK;
#ifdef O_CLOEXEC
    flags |= O_CLOEXEC;
#endif
    int fd = ::open(name.c_str(), flags);
    if (fd < 0)
        return false;
#ifndef O_CLOEXEC
    NotifyWake::setCloseOnExec(fd);
#endif
    if (::fstat(fd, &s) == -1) {
        NotifyWake::interruptLoop(::close, fd);
        return false;
    }
    if (!S_ISCHR(s.st_mode)) {
        NotifyWake::interruptLoop(::close, fd);
        return false;
    }

    struct ::termios tio;
    if (::tcgetattr(fd, &tio) == -1) {
        NotifyWake::interruptLoop(::close, fd);
        return false;
    }

    NotifyWake::interruptLoop(::close, fd);
    return true;
}

#if defined(Q_OS_LINUX)

static std::unordered_map<std::string, std::unordered_set<std::string>> getProcDrivers()
{
    std::unordered_map<std::string, std::unordered_set<std::string>> result;

    auto drivers = Access::file("/proc/tty/drivers", File::Mode::readOnly())->block();
    if (!drivers)
        return result;

    QRegularExpression sep(R"(\s+)");
    while (auto line = drivers->readLine()) {
        QStringList fields(line.toQString(false).split(sep, QString::KeepEmptyParts));
        if (fields.size() < 5)
            continue;
        if (!fields.at(4).startsWith("serial", Qt::CaseInsensitive))
            continue;

        const auto &devicePrefix = fields.at(1);
        if (devicePrefix.isEmpty())
            continue;

        int lastSlashIdx = devicePrefix.lastIndexOf('/');
        if (lastSlashIdx == -1) {
            result["/"].insert(devicePrefix.toStdString() + "*");
            continue;
        }

        if (lastSlashIdx == 0 && devicePrefix.length() == 1)
            continue;

        auto rootName = devicePrefix.mid(lastSlashIdx + 1).toStdString();
        if (rootName == "tty")
            continue;
        rootName += "*";
        result[devicePrefix.left(lastSlashIdx).toStdString()].insert(std::move(rootName));
    }

    return result;
}

#elif defined(Q_OS_DARWIN) && defined(HAVE_IOKIT)

static QString getCalloutName(::io_registry_entry_t reg)
{
    const CFStringRef prop = reinterpret_cast<CFStringRef>(::IORegistryEntrySearchCFProperty(reg, kIOServicePlane, CFSTR(kIOCalloutDeviceKey),
                                                  kCFAllocatorDefault, 0));
    if (!prop)
        return {};
    if (::CFGetTypeID(prop) != ::CFStringGetTypeID()) {
        ::CFRelease(prop);
        return {};
    }
    auto result = QString::fromCFString(prop);
    ::CFRelease(prop);
    return result;
}

static std::unordered_map<std::string, std::unordered_set<std::string>> getIOKit()
{
    std::unordered_map<std::string, std::unordered_set<std::string>> result;

    auto dict = ::IOServiceMatching(kIOSerialBSDServiceValue);
    if (!dict)
        return result;

    ::CFDictionaryAddValue(dict, CFSTR(kIOSerialBSDTypeKey), CFSTR(kIOSerialBSDAllTypes));

    ::io_iterator_t it = 0;
    if (::IOServiceGetMatchingServices(kIOMasterPortDefault, dict, &it) != KERN_SUCCESS)
        return result;

    for (;;) {
        auto service = ::IOIteratorNext(it);
        if (!service)
            break;

        for (;;) {
            auto callout = getCalloutName(service);
            if (callout.isEmpty()) {
                ::io_registry_entry_t parent = 0;
                ::IORegistryEntryGetParentEntry(service, kIOServicePlane, &parent);
                ::IOObjectRelease(service);
                service = parent;
                if (!service)
                    break;
            }

            if (!callout.isEmpty()) {
                int lastSlashIdx = callout.lastIndexOf('/');
                if (lastSlashIdx == -1) {
                    result["/"].insert(callout.toStdString());
                } else if (lastSlashIdx != 0 || callout.length() > 1) {
                    result[callout.left(lastSlashIdx).toStdString()].insert(
                            callout.mid(lastSlashIdx + 1).toStdString());
                }

                ::IOObjectRelease(service);
                break;
            }
        }
    }

    ::IOObjectRelease(it);

    return result;
}

#endif

std::unordered_set<std::string> enumeratePorts()
{
    std::unordered_map<std::string, std::unordered_set<std::string>> prefixes;

#if defined(Q_OS_LINUX)
    prefixes = getProcDrivers();
#elif defined(Q_OS_DARWIN)
    prefixes = getIOKit();
#endif

    if (prefixes.empty()) {
#if defined(Q_OS_LINUX)
        prefixes["/dev"].insert("ttyS*");
        prefixes["/dev"].insert("ttyUSB*");
        prefixes["/dev"].insert("ttyACM*");
#elif defined(Q_OS_DARWIN)
        prefixes["/dev"].insert("cu.*");
#elif defined(Q_OS_FREEBSD)
        prefixes["/dev"].insert("ttyU*");
#else
        prefixes["/dev"].insert("ttyS*");
        prefixes["/dev"].insert("ttyUSB*");
#endif
    }

    std::unordered_set<std::string> result;
    for (const auto &prefix : prefixes) {
        for (const auto &info : QDir(QString::fromStdString(prefix.first)).entryInfoList(
                Util::to_qstringlist(prefix.second),
                QDir::Files | QDir::System | QDir::NoDotAndDotDot | QDir::Readable)) {
            auto target = info.canonicalFilePath();
            if (target.isEmpty())
                continue;
            result.insert(target.toStdString());
        }
    }

    for (auto check = result.begin(); check != result.end();) {
        if (!Backing::isValid(*check)) {
            check = result.erase(check);
            continue;
        }

        ++check;
    }

    return result;
}

}

}
}