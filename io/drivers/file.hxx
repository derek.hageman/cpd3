/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_DRIVERS_FILE_HXX
#define CPD3IO_DRIVERS_FILE_HXX

#include "core/first.hxx"

#include <string>

#include "io/io.hxx"
#include "io/access.hxx"

class QFileInfo;

namespace CPD3 {
namespace IO {

namespace File {

class Backing;

/**
 * File access mode settings.
 */
struct CPD3IO_EXPORT Mode {
    /**
     * Enable reading.
     */
    bool read;

    /**
     * Enable writing.
     */
    bool write;

    /**
     * Create the file if it does not exist.
     */
    bool create;

    /**
     * Text mode (newline translation).
     */
    bool text;

    /**
     * Append mode (seek to the end).
     */
    bool append;

    enum class Access {
        /**
         * Use buffered access, if available.  Do not use if there are multiple accessors to
         * the same file (stream, block, or in another process).
         */
                Buffered,

        /**
         * Use unbuffered access, if available.  This allows "immediate" reflections of changes
         * to the file to be seen.
         */
                Unbuffered,

        /**
         * Use non-blocking access to open the file, if available.  This will usually mean
         * buffered access as well (so that calls to the stream do not block).  Generally
         * meaningless (i.e. buffered) on block access.
         */
                NonBlocking,
    };

    Access access;

    Mode();

    /**
     * Write only access.
     *
     * @return  a write only mode
     */
    static Mode writeOnly();

    /**
     * Read only access.
     *
     * @return  a read only mode
     */
    static Mode readOnly();

    /**
     * Read and write access.
     *
     * @return  a read-write mode
     */
    static Mode readWrite();

    /**
     * Enable text mode.
     *
     * @param enable    true to enable text mode
     * @return          the modified mode
     */
    Mode &textMode(bool enable = true);

    /**
     * Enable append mode.
     *
     * @param enable    true to enable append mode
     * @return          the modified mode
     */
    Mode &appendMode(bool enable = true);

    /**
     * Enable buffered mode.
     *
     * @param enable    true to enable buffered mode
     * @return          the modified mode
     */
    Mode &bufferedMode(bool enable = true);
};

}

namespace Access {

/**
 * Create an access handle for a file.
 *
 * @param filename  the file name
 * @param mode      the access mode
 * @return          an access handle
 */
CPD3IO_EXPORT std::shared_ptr<File::Backing> file(std::string filename,
                                                  const File::Mode &mode = {});

CPD3IO_EXPORT std::shared_ptr<File::Backing> file(const char *filename,
                                                  const File::Mode &mode = {});

CPD3IO_EXPORT std::shared_ptr<File::Backing> file(const QString &filename,
                                                  const File::Mode &mode = {});

CPD3IO_EXPORT std::shared_ptr<File::Backing> file(const QFileInfo &filename,
                                                  const File::Mode &mode = {});

/**
 * Create a temporary file in read-write mode.  The file is removed when the
 * last reference to it goes out of scope.
 *
 * @param buffered  enable buffered access
 * @param text      enable text mode access
 * @return          a temporary file access handle
 */
CPD3IO_EXPORT std::shared_ptr<File::Backing> temporaryFile(bool buffered = false,
                                                           bool text = false);

/**
 * Create a temporary file name which the caller is responsible for removing.
 *
 * @return          a new temporary file name
 */
CPD3IO_EXPORT std::string detachedTemporaryFile();

}

namespace File {

class Stream;

class Block;

/**
 * File handling backing.
 */
class CPD3IO_EXPORT Backing : public Generic::Backing {
    std::string targetFile;
    Mode mode;

protected:
    class Context {
    public:
        virtual ~Context();
    };

    Backing(std::string &&filename, const File::Mode &mode, std::shared_ptr<Context> context = {});

    std::shared_ptr<Context> context;

    friend class Stream;

    friend class Block;

    virtual bool nameAccessible() const;

public:
    virtual ~Backing();

    std::unique_ptr<Generic::Stream> stream() override;

    std::unique_ptr<Generic::Block> block() override;

    /**
     * Get the filename associated with the backing.
     *
     * @return  the backed file name
     */
    inline const std::string &filename() const
    { return targetFile; }

    /**
     * Resize the backed file.
     *
     * @param size  the new size
     */
    void resize(std::size_t size);

    /**
     * Get the file size in bytes.
     *
     * @return  the file size
     */
    std::size_t size() const;

    /**
     * Remove the backed file.
     */
    virtual void remove();
};

/**
 * A file in block access mode.
 */
class CPD3IO_EXPORT Block : public Generic::Block {
protected:
    using Context = std::shared_ptr<Backing::Context>;

private:
    Context context;

public:
    virtual ~Block();

protected:
    explicit Block(const Context &context);
};

/**
 * A file in stream access mode.
 */
class CPD3IO_EXPORT Stream : public Generic::Stream {
protected:
    using Context = std::shared_ptr<Backing::Context>;

private:
    Context context;

public:
    virtual ~Stream();

protected:
    explicit Stream(const Context &context);
};

#ifdef Q_OS_UNIX

/**
 * Convert a Unix file descriptor into a stream handler.
 * <br>
 * This takes ownership of the file and will close it on deletion.
 *
 * @param fd        the Unix file
 * @param read      enable reading
 * @param write     enable writing
 * @return          the file as a stream accessor
 */
CPD3IO_EXPORT std::unique_ptr<Stream> fdStream(int fd, bool read = true, bool write = true);

/**
 * Convert a Unix file descriptor into a block handler.
 * <br>
 * This takes ownership of the file and will close it on deletion.
 *
 * @param fd        the Unix file
 * @return          the file as a block accessor
 */
CPD3IO_EXPORT std::unique_ptr<Block> fdBlock(int fd);

#endif

}

}
}

#endif //CPD3IO_DRIVERS_FILE_HXX
