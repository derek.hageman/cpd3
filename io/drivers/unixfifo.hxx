/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_DRIVERS_UNIXFIFO_HXX
#define CPD3IO_DRIVERS_UNIXFIFO_HXX

#include "core/first.hxx"

#include <string>
#include <QString>

#include "io/io.hxx"
#include "io/access.hxx"

namespace CPD3 {
namespace IO {

namespace Access {

/**
 * Create an access handle for a Unix FIFO read end.
 *
 * @param filename  the file name
 * @param create    create the FIFO if it does not exist or is not a FIFO
 * @return          an access handle
 */
CPD3IO_EXPORT Handle unixFIFORead(std::string filename, bool create = false);

CPD3IO_EXPORT Handle unixFIFORead(const char *filename, bool create = false);

CPD3IO_EXPORT Handle unixFIFORead(const QString &filename, bool create = false);

/**
 * Create an access handle for a Unix FIFO write end.
 *
 * @param filename  the file name
 * @param create    create the FIFO if it does not exist or is not a FIFO
 * @return          an access handle
 */
CPD3IO_EXPORT Handle unixFIFOWrite(std::string filename, bool create = false);

CPD3IO_EXPORT Handle unixFIFOWrite(const char *filename, bool create = false);

CPD3IO_EXPORT Handle unixFIFOWrite(const QString &filename, bool create = false);

}

}
}

#endif //CPD3IO_DRIVERS_UNIXFIFO_HXX
