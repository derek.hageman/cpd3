/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <cstdio>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <cerrno>
#include <cstring>
#include <QTemporaryFile>
#include <QFile>
#include <QLoggingCategory>
#include <QFileInfo>

#ifdef Q_OS_UNIX

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "unixfdstream.hxx"
#include "io/notifywake.hxx"

#endif

#ifdef Q_OS_WIN32

#include <Windows.h>

#endif

#include "file.hxx"
#include "core/qtcompat.hxx"

#ifdef __GLIBC__

#include <gnu/libc-version.h>

# if __GLIBC__ >= 2 && defined(__GLIBC_MINOR__) && __GLIBC_MINOR__ >= 7
# define HAVE_E_FLAG
# endif

#endif

#ifdef Q_OS_WIN32

#pragma warning(disable:4996)

#endif

Q_LOGGING_CATEGORY(log_io_driver_file, "cpd3.io.driver.file", QtWarningMsg)

namespace CPD3 {
namespace IO {

static void removeFile(const std::string &filename)
{ QFile::remove(QString::fromStdString(filename)); }

static constexpr std::size_t readBlockSize = 65536;

namespace Access {

std::shared_ptr<File::Backing> file(std::string filename, const File::Mode &mode)
{
    class StandardBacking : public File::Backing {
    public:
        virtual ~StandardBacking() = default;

        StandardBacking(std::string &&filename, const File::Mode &mode) : Backing(
                std::move(filename), mode)
        { }
    };
    return std::make_shared<StandardBacking>(std::move(filename), mode);
}

std::shared_ptr<File::Backing> file(const char *filename, const File::Mode &mode)
{ return file(std::string(filename), mode); }

std::shared_ptr<File::Backing> file(const QString &filename, const File::Mode &mode)
{ return file(filename.toStdString(), mode); }

std::shared_ptr<File::Backing> file(const QFileInfo &filename, const File::Mode &mode)
{ return file(filename.absoluteFilePath(), mode); }

std::string detachedTemporaryFile()
{
    QTemporaryFile file;
    if (!file.open()) {
        /* This doesn't always pass the last error along well, and there have been failures,
         * so add some OS specific attempts at better debug */
#if defined(Q_OS_UNIX)
        qCDebug(log_io_driver_file) << "Failed to create temporary file:"
                                       << file.errorString() << "OS error:" << errno
                                       << "template:" << file.fileTemplate();
#elif defined(Q_OS_WIN32)
        qCDebug(log_io_driver_file) << "Failed to create temporary file:"
                                       << file.errorString() << "OS error:"
                                       << ::GetLastError() << "template:"
                                       << file.fileTemplate();
#else
        qCDebug(log_io_driver_file) << "Failed to create temporary file:"
                                       << file.errorString() << "template:"
                                       << file.fileTemplate();
#endif
        return {};
    }
    file.setAutoRemove(false);
    return file.fileName().toStdString();
}

std::shared_ptr<File::Backing> temporaryFile(bool buffered, bool text)
{
    class TemporaryBacking : public File::Backing {
    public:
        class Context : public File::Backing::Context {
            std::string filename;
        public:
            explicit Context(std::string filename) : filename(std::move(filename))
            { }

            virtual ~Context()
            {
                if (!filename.empty()) {
                    removeFile(filename);
                }
            }

            void invalidate()
            { filename.clear(); }
        };

        TemporaryBacking(std::string &&filename,
                         const File::Mode &mode,
                         std::shared_ptr<Context> context) : Backing(std::move(filename), mode,
                                                                     std::move(context))
        { }

        virtual ~TemporaryBacking() = default;

        bool nameAccessible() const override
        {
            if (!context)
                return false;
            return File::Backing::nameAccessible();
        }

        void remove() override
        {
            if (!context)
                return;
            File::Backing::remove();
            static_cast<Context *>(context.get())->invalidate();
            context.reset();
        }
    };
    auto name = detachedTemporaryFile();
    if (name.empty())
        return {};
    auto context = std::make_shared<TemporaryBacking::Context>(name);
    File::Mode m;
    m.create = false;
    m.access = buffered ? File::Mode::Access::Buffered : File::Mode::Access::Unbuffered;
    m.text = text;
    m.append = false;
    return std::make_shared<TemporaryBacking>(std::move(name), m, std::move(context));
}

}

namespace File {

Mode::Mode() : read(true),
               write(true),
               create(true),
               text(false),
               append(false),
               access(Access::Unbuffered)
{ }

Mode Mode::writeOnly()
{
    Mode m;
    m.read = false;
    return m;
}

Mode Mode::readOnly()
{
    Mode m;
    m.write = false;
    m.create = false;
    return m;
}

Mode Mode::readWrite()
{ return {}; }

Mode &Mode::textMode(bool enable)
{
    text = enable;
    return *this;
}

Mode &Mode::appendMode(bool enable)
{
    append = enable;
    return *this;
}

Mode &Mode::bufferedMode(bool enable)
{
    access = Access::Buffered;
    return *this;
}

Backing::Context::~Context() = default;

Backing::Backing(std::string &&filename, const File::Mode &mode, std::shared_ptr<Context> context)
        : targetFile(std::move(filename)), mode(mode), context(std::move(context))
{ }

File::Backing::~Backing() = default;

static std::FILE *openStdioFile(const std::string &filename, const Mode &mode)
{
    std::string mstr;
    if (mode.write) {
        if (mode.append) {
            mstr = "a";
            if (mode.read) {
                mstr += "+";
            }
        } else if (!mode.read) {
            mstr = "w";
        } else {
            mstr = "r+";
        }
    } else if (mode.read) {
        mstr = "r";
    } else {
        return nullptr;
    }

    Q_ASSERT(!mstr.empty());

    if (!mode.text)
        mstr += "b";

#ifdef HAVE_E_FLAG
    mstr += "e";
#endif

    std::FILE *f = std::fopen(filename.c_str(), mstr.c_str());
    if (!f)
        return nullptr;

#if defined(Q_OS_UNIX) && !defined(HAVE_E_FLAG)
    int fd = ::fileno(f);
    if (fd >= 0) {
        NotifyWake::setCloseOnExec(fd);
    }
#endif

    return f;
}

#ifdef Q_OS_UNIX

static int openFDFile(const std::string &filename, const Mode &mode)
{
    int flags = O_NOCTTY;
#ifdef O_CLOEXEC
    flags |= O_CLOEXEC;
#endif

    if (mode.read) {
        if (mode.write) {
            flags |= O_RDWR;
            if (mode.append) {
                flags |= O_APPEND;
            }
        } else {
            flags |= O_RDONLY;
        }
    } else if (mode.write) {
        flags |= O_WRONLY;
        if (!mode.append) {
            flags |= O_TRUNC;
        } else {
            flags |= O_APPEND;
        }
    } else {
        return -1;
    }

    if (mode.create) {
        flags |= O_CREAT;
    }

    if (mode.access == Mode::Access::NonBlocking) {
        flags |= O_NONBLOCK;
    }

    int fd = ::open(filename.c_str(), flags, 0666);

#ifndef O_CLOEXEC
    if (fd >= 0) {
        NotifyWake::setCloseOnExec(fd);
    }
#endif

    return fd;
}

#endif

namespace {

class StdioStream : public Stream {
    std::FILE *file;
    bool buffered;

    std::thread reader;
    std::thread writer;

    std::mutex mutex;
    std::condition_variable notify;
    bool terminated;

    bool readShouldStall;
    enum class ReadState {
        Unstarted,
        Running,
        Ended,
        Unavailable,
    } readState;

    Util::ByteArray writeBuffer;

    void runWrite()
    {
        static constexpr std::size_t stallThreshold = 4194304;

        bool wasStalled = false;
        std::unique_lock<std::mutex> lock(mutex);
        for (;;) {
            if (!lock)
                lock.lock();

            if (!writeBuffer.empty()) {
                if (!wasStalled && writeBuffer.size() > stallThreshold) {
                    wasStalled = true;
                    lock.unlock();
                    writeStall(true);
                    continue;
                }
                auto n = std::fwrite(writeBuffer.data(), 1, writeBuffer.size(), file);
                if (n < writeBuffer.size()) {
                    qCDebug(log_io_driver_file) << "Error writing to stream:" << std::ferror(file);
                }
                writeBuffer.clear();
                if (!buffered) {
                    lock.unlock();
                    notify.notify_all();
                }
                continue;
            } else {
                if (wasStalled) {
                    wasStalled = false;
                    lock.unlock();
                    writeStall(false);
                    continue;
                }
            }

            if (terminated)
                return;

            notify.wait(lock);
        }
    }

    void runRead()
    {
        Util::ByteArray buffer;
        std::unique_lock<std::mutex> lock(mutex);
        for (;;) {
            if (!lock)
                lock.lock();
            if (terminated)
                return;
            if (std::feof(file)) {
                readState = ReadState::Ended;
                lock.unlock();
                ended();
                return;
            }
            if (readShouldStall) {
                notify.wait(lock);
                continue;
            }

            buffer.resize(readBlockSize);
            auto n = std::fread(buffer.data(), 1, buffer.size(), file);
            if (n < 0) {
                qCDebug(log_io_driver_file) << "Error reading from stream:" << std::ferror(file);
                readState = ReadState::Ended;
                lock.unlock();
                ended();
                return;
            }
            if (n == 0)
                continue;

            buffer.resize(n);
            lock.unlock();
            read(buffer);
        }
    }

public:
    StdioStream(std::FILE *file, const Mode &mode, const Context &context) : Stream(context),
                                                                             file(file),
                                                                             buffered(mode.access !=
                                                                                              Mode::Access::Unbuffered),
                                                                             terminated(false),
                                                                             readShouldStall(false),
                                                                             readState(ReadState::Unstarted)
    {
        if (!buffered) {
            std::setbuf(file, nullptr);
        }
        if (mode.write) {
            writer = std::thread(&StdioStream::runWrite, this);
        }
        if (!mode.read) {
            readState = ReadState::Unavailable;
        }
    }

    virtual ~StdioStream()
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            terminated = true;
        }
        notify.notify_all();

        if (reader.joinable()) {
            reader.join();
        }
        if (writer.joinable()) {
            writer.join();
        }

        std::fclose(file);
    }

    void start() override
    {
        {
            std::unique_lock<std::mutex> lock(mutex);
            switch (readState) {
            case ReadState::Unstarted:
                break;
            case ReadState::Running:
            case ReadState::Ended:
                return;
            case ReadState::Unavailable:
                readState = ReadState::Ended;
                lock.unlock();
                ended();
                return;
            }
        }
        reader = std::thread(&StdioStream::runRead, this);
    }

    void write(const Util::ByteView &data) override
    {
        if (data.empty())
            return;
        {
            std::lock_guard<std::mutex> lock(mutex);
            writeBuffer += data;
        }
        notify.notify_all();
        if (!buffered) {
            Q_ASSERT(writer.joinable());
            std::unique_lock<std::mutex> lock(mutex);
            notify.wait(lock, [this]() { return writeBuffer.empty(); });
        }
    }

    void write(Util::ByteArray &&data) override
    {
        if (data.empty())
            return;
        {
            std::lock_guard<std::mutex> lock(mutex);
            writeBuffer += std::move(data);
        }
        notify.notify_all();
        if (!buffered) {
            Q_ASSERT(writer.joinable());
            std::unique_lock<std::mutex> lock(mutex);
            notify.wait(lock, [this]() { return writeBuffer.empty(); });
        }
    }

    bool isEnded() const override
    {
        std::lock_guard<std::mutex> lock(const_cast<StdioStream *>(this)->mutex);
        return readState == ReadState::Ended;
    }

    void readStall(bool enable) override
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            readShouldStall = enable;
        }
        if (!enable) {
            notify.notify_all();
        }
    }
};

#ifdef Q_OS_UNIX

class FDStream : public Stream {
    UnixFDStream stream;
public:
    FDStream(int fd, const Mode &mode, const Context &context) : Stream(context),
                                                                 stream(fd, mode.read, mode.write)
    {
        read = stream.read;
        ended = stream.ended;
        writeStall = stream.writeStall;
    }

    virtual ~FDStream() = default;

    void start() override
    { return stream.start(); }

    void write(const Util::ByteView &data) override
    { return stream.write(data); }

    void write(Util::ByteArray &&data) override
    { return stream.write(std::move(data)); }

    bool isEnded() const override
    { return stream.isEnded(); }

    void readStall(bool enable) override
    { return stream.readStall(enable); }
};

#endif

}

std::unique_ptr<Generic::Stream> Backing::stream()
{
    if (!nameAccessible())
        return {};

#ifdef Q_OS_UNIX

    if (mode.access != Mode::Access::Unbuffered || !mode.write) {
        auto fd = openFDFile(filename(), mode);
        if (fd < 0) {
            qCDebug(log_io_driver_file) << "Failed to open file:" << std::strerror(errno);
            return {};
        }
        return std::unique_ptr<Generic::Stream>(new FDStream(fd, mode, context));
    }

#endif

    if (!mode.create) {
        if (!QFile::exists(QString::fromStdString(filename())))
            return {};
    }

    std::FILE *file = openStdioFile(filename(), mode);
    if (!file) {
        qCDebug(log_io_driver_file) << "Failed to open file:" << std::strerror(errno);
        return {};
    }
    return std::unique_ptr<Generic::Stream>(new StdioStream(file, mode, context));
}

namespace {

class StdioBlock : public Block {
    std::FILE *file;
public:
    StdioBlock(std::FILE *file, const Mode &mode, const Context &context) : Block(context),
                                                                            file(file)
    {
        if (mode.access == Mode::Access::Unbuffered) {
            std::setbuf(file, nullptr);
        }
    }

    virtual ~StdioBlock()
    { std::fclose(file); }

    bool inError() const override
    { return std::ferror(file) != 0; }

    std::string describeError() const override
    {
        auto err = std::ferror(file);
        if (!err)
            return {};
        return std::to_string(err);
    }

    void write(const Util::ByteView &data) override
    { std::fwrite(data.data(), 1, data.size(), file); }

    bool readFinished() const override
    { return std::feof(file); }

    void read(Util::ByteArray &target, std::size_t maximum = static_cast<std::size_t>(-1)) override
    {
        if (!maximum)
            return;
        if (maximum != static_cast<std::size_t>(-1)) {
            auto original = target.size();
            auto n = std::fread(target.tail(maximum), 1, maximum, file);
            target.resize(original + n);
        } else {
            while (!readFinished()) {
                auto original = target.size();
                auto n = std::fread(target.tail(readBlockSize), 1, readBlockSize, file);
                target.resize(original + n);
            }
        }
    }

    void read(QByteArray &target, int maximum = -1) override
    {
        if (!maximum)
            return;
        if (maximum > 0) {
            auto original = target.size();
            target.resize(original + maximum);
            auto n = std::fread(target.data() + original, 1, maximum, file);
            target.resize(static_cast<int>(original + n));
        } else {
            while (!readFinished()) {
                auto original = target.size();
                target.resize(static_cast<int>(original + readBlockSize));
                auto n = std::fread(target.data() + original, 1, readBlockSize, file);
                target.resize(static_cast<int>(original + n));
            }
        }
    }

    void seek(std::size_t absolute) override
    {
        if (absolute == static_cast<std::size_t>(-1)) {
            std::fseek(file, 0, SEEK_END);
            return;
        }
        std::fseek(file, absolute, SEEK_SET);
    }

    void move(std::ptrdiff_t delta) override
    { std::fseek(file, delta, SEEK_CUR); }

    std::size_t tell() const override
    { return std::ftell(file); }
};


#ifdef Q_OS_UNIX

class FDBlock : public Block {
    int fd;
    bool haveReadEOF;

    bool haveHadError;
    int lastErrno;
public:
    FDBlock(int fd, const Context &context) : Block(context),
                                              fd(fd),
                                              haveReadEOF(false),
                                              haveHadError(false),
                                              lastErrno(0)
    { }

    virtual ~FDBlock()
    {
        NotifyWake::interruptLoop(::close, fd);
    }

    bool inError() const override
    { return haveHadError; }

    std::string describeError() const override
    { return std::strerror(lastErrno); }

    void write(const Util::ByteView &data) override
    {
        auto remaining = data;
        while (!remaining.empty()) {
            auto nwr = ::write(fd, remaining.data(), remaining.size());
            if (nwr < 0) {
                haveHadError = true;
                lastErrno = errno;
                return;
            }
            remaining = remaining.mid(nwr);
        }
    }

    bool readFinished() const override
    { return haveReadEOF; }

    void read(Util::ByteArray &target, std::size_t maximum = static_cast<std::size_t>(-1)) override
    {
        if (!maximum)
            return;
        if (haveReadEOF)
            return;
        if (maximum != static_cast<std::size_t>(-1)) {
            auto original = target.size();
            auto n = ::read(fd, target.tail(maximum), maximum);
            if (n < 0) {
                haveHadError = true;
                lastErrno = errno;
                target.resize(original);
                return;
            } else if (n == 0) {
                target.resize(original);
                haveReadEOF = true;
                return;
            }
            haveReadEOF = false;
            target.resize(original + n);
        } else {
            for (;;) {
                auto original = target.size();
                auto n = ::read(fd, target.tail(readBlockSize), readBlockSize);
                if (n < 0) {
                    haveHadError = true;
                    lastErrno = errno;
                    target.resize(original);
                    return;
                } else if (n == 0) {
                    target.resize(original);
                    haveReadEOF = true;
                    return;
                }
                target.resize(original + n);
            }
        }
    }

    void read(QByteArray &target, int maximum = -1) override
    {
        if (!maximum)
            return;
        if (haveReadEOF)
            return;
        if (maximum > 0) {
            auto original = target.size();
            target.resize(original + maximum);
            auto n = ::read(fd, target.data() + original, maximum);
            if (n < 0) {
                haveHadError = true;
                lastErrno = errno;
                target.resize(original);
                return;
            } else if (n == 0) {
                target.resize(original);
                haveReadEOF = true;
                return;
            }
            haveReadEOF = false;
            target.resize(static_cast<int>(original + n));
        } else {
            for (;;) {
                auto original = target.size();
                target.resize(static_cast<int>(original + readBlockSize));
                auto n = ::read(fd, target.data() + original, readBlockSize);
                if (n < 0) {
                    haveHadError = true;
                    lastErrno = errno;
                    target.resize(original);
                    return;
                } else if (n == 0) {
                    target.resize(original);
                    haveReadEOF = true;
                    return;
                }
                target.resize(static_cast<int>(original + n));
            }
        }
    }

    void seek(std::size_t absolute) override
    {
        haveReadEOF = false;
        if (absolute == static_cast<std::size_t>(-1)) {
            ::lseek(fd, 0, SEEK_END);
            return;
        }
        ::lseek(fd, absolute, SEEK_SET);
    }

    void move(std::ptrdiff_t delta) override
    {
        haveReadEOF = false;
        ::lseek(fd, delta, SEEK_CUR);
    }

    std::size_t tell() const override
    { return ::lseek(fd, 0, SEEK_CUR); }
};

#endif

}

std::unique_ptr<Generic::Block> Backing::block()
{
    if (!nameAccessible())
        return {};

#ifdef Q_OS_UNIX

    if (mode.access == Mode::Access::Unbuffered) {
        auto fd = openFDFile(filename(), mode);
        if (fd < 0) {
            qCDebug(log_io_driver_file) << "Failed to open file:" << std::strerror(errno);
            return {};
        }
        return std::unique_ptr<Generic::Block>(new FDBlock(fd, context));
    }

#endif

    if (!mode.create) {
        if (!QFile::exists(QString::fromStdString(filename())))
            return {};
    }

    std::FILE *file = openStdioFile(filename(), mode);
    if (!file) {
        qCDebug(log_io_driver_file) << "Failed to open file:" << std::strerror(errno);
        return {};
    }
    return std::unique_ptr<Generic::Block>(new StdioBlock(file, mode, context));
}

void Backing::resize(std::size_t size)
{
    if (!mode.write)
        return;
    if (!nameAccessible())
        return;
    QFile::resize(QString::fromStdString(filename()), static_cast<qint64>(size));
}

void Backing::remove()
{
    if (!mode.write)
        return;
    if (!nameAccessible())
        return;
    removeFile(filename());
}

std::size_t Backing::size() const
{
    if (!nameAccessible())
        return 0;
    return QFileInfo(QString::fromStdString(filename())).size();
}

bool Backing::nameAccessible() const
{
    if (!mode.create || !mode.write) {
        if (!QFile::exists(QString::fromStdString(filename())))
            return false;
    }
    return true;
}


Block::Block(const Context &context) : context(context)
{ }

Block::~Block() = default;

Stream::Stream(const Context &context) : context(context)
{ }

Stream::~Stream() = default;

#ifdef Q_OS_UNIX

std::unique_ptr<Stream> fdStream(int fd, bool read, bool write)
{
    Mode mode;
    mode.read = read;
    mode.write = write;
    return std::unique_ptr<Stream>(new FDStream(fd, mode, {}));
}

std::unique_ptr<Block> fdBlock(int fd)
{
    return std::unique_ptr<Block>(new FDBlock(fd, {}));
}

#endif

}

}
}