/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_TCP_HXX
#define CPD3IO_TCP_HXX

#include "core/first.hxx"

#include <cstdint>
#include <vector>
#include <QHostAddress>
#include <QSslKey>
#include <QSslCertificate>
#include <QTcpSocket>
#include <QSslSocket>
#include <QList>

#include "io/io.hxx"
#include "io/socket.hxx"

namespace CPD3 {
namespace IO {
namespace Socket {
namespace TCP {

/**
 * The configuration for a TCP socket.
 */
struct CPD3IO_EXPORT Configuration final {
    /**
     * Use any available port, only meaningful for server connections.
     */
    static constexpr std::uint16_t port_any = 0;

    /**
     * The port the connection or server should use.
     */
    std::uint16_t port;

    /**
     * A function called on a socket to configure it for TLS/SSL.
     */
    std::function<bool(QSslSocket &socket)> tls;

    Configuration();

    ~Configuration();

    /**
     * Create a configuration for a port.
     *
     * @param port  the port to use
     * @param tls   the optional TLS setup function
     */
    Configuration(std::uint16_t port, std::function<bool(QSslSocket &socket)> tls = {});

    /**
     * Create a configuration for TLS.
     *
     * @param tls   the TLS setup function
     */
    Configuration(std::function<bool(QSslSocket &socket)> tls);

    Configuration(const Configuration &) = default;

    Configuration &operator=(const Configuration &) = default;

    Configuration(Configuration &&) = default;

    Configuration &operator=(Configuration &&) = default;
};

/**
 * A TCP connection.
 */
class CPD3IO_EXPORT Connection : public Socket::Connection::QtConnection {
protected:
    /**
     * The socket the connection is for.
     */
    QTcpSocket &socket;
public:
    Connection() = delete;

    virtual ~Connection();

    /**
     * Get the address of the peer.  For asynchronous connections, this may nobe be
     * immediately available.
     *
     * @return the peer address
     */
    virtual QHostAddress peerAddress() const = 0;

    /**
     * Get the port the connection is directed to on the remote peer.
     *
     * @return  the remote port
     */
    virtual std::uint16_t peerPort() const = 0;

    /**
     * Get the TLS certificate (if any) that the peer has presented.
     *
     * @return  the peer certificate
     */
    virtual QSslCertificate peerCertificate() const;

    /**
     * Get the local address of the socket.
     *
     * @return the local address
     */
    virtual QHostAddress localAddress() const = 0;

protected:
    /**
     * Create the TCP connection.
     *
     * @param socket    the socket
     * @param config    the configuration
     */
    explicit Connection(QTcpSocket &socket, const Configuration &config);
};

/**
 * Connect to a remote host.  Asynchronous connections may finish connection later, but will
 * accept data immediately and queue it for when they are established.
 *
 * @param address       the address or host name
 * @param config        the configuration including a port
 * @param synchronous   if set, then do not return until the connection is established and ready to use
 * @return              the connection or null on a failure
 */
CPD3IO_EXPORT std::unique_ptr<Connection> connect(const std::string &address,
                                                  const Configuration &config,
                                                  bool synchronous = false);

CPD3IO_EXPORT std::unique_ptr<Connection> connect(const QHostAddress &address,
                                                  const Configuration &config,
                                                  bool synchronous = false);

/**
 * Connect to a remote host.  Asynchronous connections may finish connection later, but will
 * accept data immediately and queue it for when they are established.
 *
 * @param address       the address or host name
 * @param config        the configuration including a port
 * @param local         the local address to bind
 * @param synchronous   if set, then do not return until the connection is established and ready to use
 * @return              the connection or null on a failure
 */
CPD3IO_EXPORT std::unique_ptr<Connection> connect(const std::string &address,
                                                  const Configuration &config,
                                                  const std::string &local,
                                                  bool synchronous = false);

CPD3IO_EXPORT std::unique_ptr<Connection> connect(const QHostAddress &address,
                                                  const Configuration &config,
                                                  const QHostAddress &local,
                                                  bool synchronous = false);

/**
 * Connect to the loopback interface.  Asynchronous connections may finish connection later,
 * but will accept data immediately and queue it for when they are established.
 *
 * @param config        the configuration including a port
 * @param synchronous   if set, then do not return until the connection is established and ready to use
 * @return              the connection or null on a failure
 */
CPD3IO_EXPORT std::unique_ptr<Connection> loopback(const Configuration &config,
                                                   bool synchronous = false);


/**
 * A TCP server.  The server can listen on one or more interfaces and will call a provided
 * function for each incoming connection, when they are ready to use.
 * <br>
 * Destroying the server stops it listening but does not close any existing connections
 * that have been fully established.
 */
class CPD3IO_EXPORT Server : public Socket::Server {
    class Worker;

    std::shared_ptr<Worker> worker;

public:
    Server() = delete;

    virtual ~Server();

    Server(const Server &) = delete;

    Server &operator=(const Server &) = delete;

    /**
     * Create a server listening on all available addresses.
     *
     * @param connection    the function to call for incoming connections
     * @param config        the configuration
     */
    explicit Server(IncomingConnection connection, Configuration config = {});

    /**
     * Create a server listening on the specified address(es).
     *
     * @param connection    the function to call for incoming connections
     * @param address       the address or host name to listen on
     * @param config        the configuration
     */
    Server(IncomingConnection connection, std::string address, Configuration config = {});

    /**
     * Create a server listening on the specified address.
     *
     * @param connection    the function to call for incoming connections
     * @param address       the address to listen on
     * @param config        the configuration
     */
    Server(IncomingConnection connection, const QHostAddress &address, Configuration config = {});

    /**
     * Create a server listening on the specified addresses.
     *
     * @param connection    the function to call for incoming connections
     * @param address       the addresses and/or host names to listen on
     * @param config        the configuration
     */
    Server(IncomingConnection connection,
           std::vector<std::string> address,
           Configuration config = {});

    /**
     * Create a server listening on the specified addresses.
     *
     * @param connection    the function to call for incoming connections
     * @param address       the addresses to listen on
     * @param config        the configuration
     */
    Server(IncomingConnection connection,
           std::vector<QHostAddress> address,
           Configuration config = {});

    /**
     * Create a server listening on the specified addresses.
     *
     * @param connection    the function to call for incoming connections
     * @param address       the addresses to listen on
     * @param config        the configuration
     */
    Server(IncomingConnection connection,
           const QList<QHostAddress> &address,
           Configuration config = {});

    /**
     * A listen address family.
     */
    enum class AddressFamily {
        /** Listen for connections on any/all network interfaces. */
                Any,

        /** Listen for connections on any loopback interface. */
                Loopback,

        /** Listen for connections on the IPv4 loopback interface only. */
                LoopbackIPv4,

        /** Listen for connections on the IPv6 loopback interface only. */
                LoopbackIPv6
    };

    /**
     * Create a server listening on the specified address(es).
     *
     * @param connection    the function to call for incoming connections
     * @param address       the address family to listen on
     * @param config        the configuration
     */
    explicit Server(IncomingConnection connection,
                    AddressFamily address, Configuration config = {});


    /**
     * Get the port the server is listening on.
     *
     * @return  the local port
     */
    std::uint16_t localPort() const;

    /**
     * Get the address(es) the server is listening on.
     *
     * @return  the local addresses
     */
    std::vector<QHostAddress> localAddress() const;

    /**
     * Start listening and accepting connections.
     *
     * @param requireAll    if set then the call will fail if any of the addresses/hosts fail to bind
     * @return              true on success
     */
    bool startListening(bool requireAll = false);

    /**
     * Stop listening and refuse further connections.
     */
    void stopListening();

    /**
     * Test if the server is currently listening for connections.
     *
     * @return  true if new connections are accepted
     */
    bool isListening() const;

    std::string describeServer() const override;
};

}
}
}
}

#endif //CPD3IO_TCP_HXX
