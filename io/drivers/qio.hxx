/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_DRIVERS_QIO_HXX
#define CPD3IO_DRIVERS_QIO_HXX

#include "core/first.hxx"

#include "io/io.hxx"
#include "io/access.hxx"

namespace CPD3 {
namespace IO {

namespace Access {

/**
 * Create a handle for a device.
 * <br>
 * The thread owning the device must run an event loop.
 *
 * @param device    the device
 * @return          an access handle
 */
CPD3IO_EXPORT Handle qio(std::shared_ptr<QIODevice> device);

CPD3IO_EXPORT Handle qio(std::unique_ptr<QIODevice> &&device);

/**
 * Create a handle for a device.  This does NOT transfer ownership, so the caller
 * must delete the device but only after all access to it has first been removed.
 * <br>
 * The thread owning the device must run an event loop.
 *
 * @param device    the device
 * @return          a handle
 */
CPD3IO_EXPORT Handle qio(QIODevice *device);

/**
 * Create a device on a dedicated access thread.  The provided function will be
 * called on a dedicated QThread that will handle events for the device.
 *
 * @param create    the function to create the device
 * @return          the access handle
 */
CPD3IO_EXPORT Handle qio(const std::function<std::unique_ptr<QIODevice>()> &create);

}

}
}

#endif //CPD3IO_DRIVERS_QIO_HXX
