/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_DRIVERS_UNIXFDSTREAM_HXX
#define CPD3IO_DRIVERS_UNIXFDSTREAM_HXX

#include "core/first.hxx"

#include <mutex>
#include <thread>
#include <condition_variable>
#include <functional>

#include "io/io.hxx"
#include "io/notifywake.hxx"
#include "core/util.hxx"
#include "core/threading.hxx"

namespace CPD3 {
namespace IO {

/**
 * An implementation for Unix file streams.  This takes a file descriptor and
 * handles the threading/poll for reads and/or writes on the file.  This API mirrors
 * Generic::Stream and is intended to be used as part of an implementation of that.
 */
class CPD3IO_EXPORT UnixFDStream {
    int fd;
    bool enableRead;
    bool enableWrite;
    bool ownFD;

    NotifyWake wake;

    std::thread thread;
    std::mutex mutex;
    std::condition_variable external;
    bool terminated;
    bool readStarted;
    bool readEnded;
    bool readShouldStall;
    Util::ByteArray writeBuffer;

    enum class LockState {
        Unlocked, LockRequested, Locked, LockedFlushWrite,
    } lockState;

    void run();

    void unrecoverableFailure();

    void generateReadEnd();

public:
    UnixFDStream() = delete;

    /**
     * Create the stream.
     *
     * @param fd            the file
     * @param enableRead    enable reading from the file
     * @param enableWrite   enable writing to the file
     * @param ownFD         take ownership of the file (close it on completion)
     */
    UnixFDStream(int fd, bool enableRead, bool enableWrite, bool ownFD = true);

    virtual ~UnixFDStream();

    /**
     * Write data.
     *
     * @param data  the data to write
     */
    void write(const Util::ByteView &data);

    void write(Util::ByteArray &&data);

    /**
     * Start reading.  Writes are available immediately, however.
     */
    void start();

    /**
     * Test if the read has ended.
     * @return  true if reading has completed
     */
    bool isEnded() const;

    /**
     * Request a read stall (stop emitting read data).  This is optional and may not be
     * respected or may take time to take effect.  So the read signal has to continue to be
     * handled.
     *
     * @param enable    enable the stall
     */
    void readStall(bool enable);

    /**
     * Acquire a lock on the stream.  This prevents the stream from reading or
     * writing to the file descriptor while the lock exists.
     */
    class CPD3IO_EXPORT Lock final {
        UnixFDStream &stream;
    public:
        /**
         * Acquire the lock.
         *
         * @param stream    the fd stream
         */
        explicit Lock(UnixFDStream &stream);

        ~Lock();

        /**
         * Get the FD associated with the stream.
         *
         * @return  the stream FD or -1 if it has already closed
         */
        int fd() const;

        /**
         * Flush the stream, clearing all data in the local buffers.
         * <br>
         * This can also read from the kernel until a poll() call returns that the fd is no
         * longer ready to read.
         *
         * @param maximumReadFlush  maximum amount of data to read and discard from the stream or -1 for unlimited
         */
        void flush(std::size_t maximumReadFlush = static_cast<std::size_t>(-1));
    };

    friend class Lock;

    /**
     * Emitted when data have been read from the stream.
     */
    Threading::Signal<const Util::ByteArray &> read;

    /**
     * Emitted when the stream has ended (only after the start).
     */
    Threading::Signal<> ended;

    /**
     * Emitted when the write should stall, if possible.  The stream will
     * still accept data but this signal is used to indicate excessive buffering.
     */
    Threading::Signal<bool> writeStall;

protected:
    /**
     * Called before entering the wait/poll with an active file descriptor.
     * <br>
     * The default implementation always return -1.
     *
     * @param reading   true if reading is enabled
     * @param writing   true if there is data to write
     * @return          the maximum number of milliseconds to wait or -1 to wait forever
     */
    virtual int waitEntry(bool &reading, bool &writing);

    /**
     * Called when data are read from the file descriptor.
     * <br>
     * The default implementation just calls read() with the data.
     *
     * @param data      the data that has been read
     */
    virtual void dataRead(const Util::ByteArray &data);

    /**
     * Called after data have been written.
     * <br>
     * The default implementation does nothing.
     *
     * @param count     the number of bytes written
     */
    virtual void dataWritten(std::size_t count);
};

}
}

#endif //CPD3IO_DRIVERS_UNIXFDSTREAM_HXX
