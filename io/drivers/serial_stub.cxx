/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include "serial.hxx"


namespace CPD3 {
namespace IO {

namespace Access {

std::shared_ptr<Serial::Backing> serial(std::string name)
{
    class Backing : public Serial::Backing {
    public:
        explicit Backing(std::string &&name) : Serial::Backing(std::move(name))
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        { return {}; }
    };
    return std::make_shared<Backing>(std::move(name));
}

}

namespace Serial {

bool Backing::isValid(const std::string &)
{ return true; }

std::unordered_set<std::string> enumeratePorts()
{ return {}; }

}

}
}