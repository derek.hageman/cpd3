/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include "unixsocket.hxx"

#ifdef Q_OS_UNIX

#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <QLoggingCategory>

#include "core/qtcompat.hxx"
#include "unixfdstream.hxx"

Q_LOGGING_CATEGORY(log_io_driver_unixsocket, "cpd3.io.driver.unixsocket", QtWarningMsg)

namespace CPD3 {
namespace IO {
namespace Socket {
namespace Unix {

Connection::Connection() = default;

Connection::~Connection() = default;

namespace {
class UnixSocketConnection : public Connection {
    std::string path;
    UnixFDStream stream;
public:
    UnixSocketConnection(int fd, std::string path, const Configuration &) : path(std::move(path)),
                                                                            stream(fd, true, true)
    {
        read = stream.read;
        ended = stream.ended;
        writeStall = stream.writeStall;
    }

    virtual ~UnixSocketConnection() = default;

    void start() override
    { return stream.start(); }

    void write(const Util::ByteView &data) override
    { return stream.write(data); }

    void write(Util::ByteArray &&data) override
    { return stream.write(std::move(data)); }

    bool isEnded() const override
    { return stream.isEnded(); }

    void readStall(bool enable) override
    { return stream.readStall(enable); }

    std::string socketPath() const override
    { return path; }

    std::string describePeer() const override
    { return path; }
};
}


std::unique_ptr<Connection> connect(std::string path, const Configuration &config)
{
    NotifyWake::blockSigpipe();

    int fd = ::socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd == -1) {
        qCDebug(log_io_driver_unixsocket) << "Unable to create unix socket:"
                                          << std::strerror(errno);
        return {};
    }
    {
        struct ::sockaddr_un addr;
        ::memset(&addr, 0, sizeof(addr));
        addr.sun_family = AF_UNIX;
        ::strncpy(addr.sun_path, path.c_str(), sizeof(addr.sun_path));
        addr.sun_path[sizeof(addr.sun_path) - 1] = 0;
        if (::connect(fd, (const struct ::sockaddr *) &addr, sizeof(addr)) == -1) {
            qCDebug(log_io_driver_unixsocket) << "Unable to connect unix socket" << path << ":"
                                              << std::strerror(errno);
            NotifyWake::interruptLoop(::close, fd);
            return {};
        }
    }

    if (!NotifyWake::setNonblocking(fd)) {
        NotifyWake::interruptLoop(::close, fd);
        return {};
    }
    NotifyWake::setCloseOnExec(fd);

    return std::unique_ptr<UnixSocketConnection>(
            new UnixSocketConnection(fd, std::move(path), config));
}

Server::Server(IncomingConnection connection, std::string path, Configuration config)
        : Socket::Server(connection),
          path(std::move(path)),
          connection(std::move(connection)),
          config(std::move(config)),
          socket(-1),
          terminated(false)
{
    thread = std::thread(std::bind(&Server::run, this));
}

Server::~Server()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    wake();
    if (thread.joinable())
        thread.join();
    if (socket != -1)
        NotifyWake::interruptLoop(::close, socket);
}

static int bindSocket(const std::string &path, bool tryUnlink = false)
{
    NotifyWake::blockSigpipe();

    int fd = ::socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd == -1) {
        qCDebug(log_io_driver_unixsocket) << "Unable to create unix socket:"
                                          << std::strerror(errno);
        return -1;
    }

    if (!NotifyWake::setNonblocking(fd)) {
        NotifyWake::interruptLoop(::close, fd);
        return -1;
    }
    NotifyWake::setCloseOnExec(fd);

    struct ::sockaddr_un addr;
    ::memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    ::strncpy(addr.sun_path, path.c_str(), sizeof(addr.sun_path));
    addr.sun_path[sizeof(addr.sun_path) - 1] = 0;

    if (tryUnlink) {
        ::unlink(addr.sun_path);
    }

    if (::bind(fd, (const struct ::sockaddr *) &addr, sizeof(addr)) == -1) {
        if (tryUnlink && errno == EADDRINUSE) {
            NotifyWake::interruptLoop(::close, fd);
            ::unlink(addr.sun_path);
            return bindSocket(path);
        }
        qCDebug(log_io_driver_unixsocket) << "Unable to bind unix socket" << path << ":"
                                          << std::strerror(errno);
        NotifyWake::interruptLoop(::close, fd);
        return -1;
    }

    qCDebug(log_io_driver_unixsocket) << "Bound Unix socket" << path;

    return fd;
}

bool Server::startListening(bool removeExisting)
{
    if (path.empty())
        return false;

    {
        std::lock_guard<std::mutex> lock(mutex);
        if (socket != -1) {
            qCDebug(log_io_driver_unixsocket) << "Unix server" << describeServer()
                                              << "already listening";
            return false;
        }

        int fd = bindSocket(path, removeExisting);
        if (fd == -1)
            return false;

        if (::listen(fd, 32) != 0) {
            qCDebug(log_io_driver_unixsocket) << "Failed to listen on Unix socket"
                                              << describeServer() << ":" << std::strerror(errno);
            NotifyWake::interruptLoop(::close, fd);
            return false;
        }

        socket = fd;
    }
    wake();
    return true;
}

void Server::stopListening()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    wake();
    if (thread.joinable())
        thread.join();
    if (socket != -1) {
        NotifyWake::interruptLoop(::close, socket);
        socket = -1;
    }
}

bool Server::isListening() const
{
    std::lock_guard<std::mutex> lock(const_cast<Server *>(this)->mutex);
    return socket != -1;
}

std::string Server::describeServer() const
{ return path; }

void Server::shutdown()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (socket == -1)
        return;
    if (!path.empty()) {
        ::unlink(path.c_str());
    }
    NotifyWake::interruptLoop(::close, socket);
    socket = -1;
}

void Server::run()
{
    struct ::pollfd pfds[2];
    ::memset(pfds, 0, sizeof(pfds));
    wake.install(pfds[0]);

    for (;;) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (terminated) {
                if (socket != -1) {
                    ::unlink(path.c_str());
                    NotifyWake::interruptLoop(::close, socket);
                    socket = -1;
                }
                return;
            }

            if (socket != -1) {
                pfds[1].fd = socket;
                pfds[1].events = POLLIN;
            } else {
                pfds[1].fd = -1;
                pfds[1].events = 0;
            }
        }

        int rc = ::poll(pfds, pfds[1].events != 0 ? 2 : 1, -1);
        if (rc == 0) {
            continue;
        } else if (rc < 0) {
            int en = errno;
            if (en == EAGAIN || en == EINTR)
                continue;
            qCDebug(log_io_driver_unixsocket) << "Poll failed:" << std::strerror(en);
            return shutdown();
        }


        if (pfds[0].revents & POLLIN) {
            if (!wake.clear()) {
                return shutdown();
            }
        }

        if (pfds[1].events == 0)
            continue;
        if ((pfds[1].revents & POLLIN) == 0)
            continue;

        int fd = ::accept(pfds[1].fd, nullptr, 0);
        if (fd == -1) {
            int en = errno;
            if (en != EAGAIN && en != EINTR && en != EWOULDBLOCK && en != ECONNABORTED) {
                qCDebug(log_io_driver_unixsocket) << "Accept failed:" << std::strerror(en);
                return shutdown();
            }
            continue;
        }

        qCDebug(log_io_driver_unixsocket) << "Unix socket connection accepted on"
                                          << describeServer();

        NotifyWake::setCloseOnExec(fd);

        class UnixServerConnection final : public UnixSocketConnection {
        public:
            UnixServerConnection(int fd, const std::string &path, const Configuration &config)
                    : UnixSocketConnection(fd, path, config)
            { }

            virtual ~UnixServerConnection() = default;

            std::string describePeer() const override
            { return "[" + socketPath() + "]"; }
        };

        connection(
                std::unique_ptr<UnixServerConnection>(new UnixServerConnection(fd, path, config)));
    }
}

}
}
}
}

#else

namespace CPD3 {
namespace IO {
namespace Socket {
namespace Unix {

Connection::Connection() = default;

Connection::~Connection() = default;


std::unique_ptr<Connection> connect(std::string, const Configuration &)
{ return {}; }


Server::~Server() = default;

Server::Server(IncomingConnection connection, std::string path, Configuration) :
        Socket::Server(connection), path(std::move(path))
{ }

bool Server::startListening(bool)
{ return false; }

void Server::stopListening()
{ }

bool Server::isListening() const
{ return false; }

std::string Server::describeServer() const
{ return path; }

}
}
}
}

#endif
