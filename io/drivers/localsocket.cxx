/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <vector>
#include <QThread>
#include <QLoggingCategory>
#include <QLocalServer>
#include <QTimer>

#include "localsocket.hxx"
#include "core/qtcompat.hxx"


Q_LOGGING_CATEGORY(log_io_driver_localsocket, "cpd3.io.driver.localsocket", QtWarningMsg)

namespace CPD3 {
namespace IO {
namespace Socket {
namespace Local {

Connection::Connection(QLocalSocket &socket, const Configuration &) : QtConnection(socket),
                                                                      socket(socket)
{ }

Connection::~Connection() = default;

std::unique_ptr<Connection> connect(const QString &server,
                                    const Configuration &config,
                                    bool synchronous)
{
    struct Result {
        const QString &serverName;
        const Configuration &config;
        bool synchronous;

        std::mutex mutex;
        std::condition_variable notify;
        bool ready;

        std::unique_ptr<Connection> connection;
        std::unique_ptr<QThread> thread;

        Result(const QString &serverName, const Configuration &config, bool synchronous)
                : serverName(serverName), config(config), synchronous(synchronous), ready(false)
        { }
    };

    Result result(server, config, synchronous);

    class OutgoingConnection final : public Connection {
        std::string name;
        std::unique_ptr<QThread> thread;
        std::unique_ptr<QTimer> startupTimeout;

        enum class ReadyState {
            Connecting, Ready, Failed, Shutdown
        } readyState;
        std::function<void(bool isOk)> readyFunc;

        void connectionReadyToUse()
        {
            if (readyState != ReadyState::Connecting)
                return;
            qCDebug(log_io_driver_localsocket) << "Connection to" << describePeer() << "ready";
            readyState = ReadyState::Ready;

            for (auto &c : startupConnections) {
                QObject::disconnect(c);
            }
            startupConnections.clear();
            startupTimeout.reset();

            if (readyFunc) {
                readyFunc(true);
                readyFunc = std::function<void(bool isOk)>();
            }
        }

    protected:
        std::vector<QMetaObject::Connection> startupConnections;

    public:

        OutgoingConnection(QLocalSocket &socket, Result &result) : Connection(socket,
                                                                              result.config),
                                                                   name(result.serverName
                                                                              .toStdString()),
                                                                   thread(std::move(result.thread)),
                                                                   startupTimeout(new QTimer),
                                                                   readyState(
                                                                           ReadyState::Connecting)
        {
            startupConnections.emplace_back(QObject::connect(&socket, &QLocalSocket::stateChanged,
                                                             std::bind(
                                                                     &OutgoingConnection::stateUpdated,
                                                                     this)));

            startupTimeout->setSingleShot(true);
            startupConnections.emplace_back(
                    QObject::connect(startupTimeout.get(), &QTimer::timeout, &socket,
                                     &QIODevice::close, Qt::DirectConnection));
            startupTimeout->start(30000);
        }

        virtual ~OutgoingConnection()
        {
            if (!thread) {
                readyState = ReadyState::Shutdown;
                for (auto &c : startupConnections) {
                    QObject::disconnect(c);
                }
                return;
            }
            if (socket.thread() != QThread::currentThread()) {
                Threading::runBlockingFunctor(&socket, [this]() {
                    qCDebug(log_io_driver_localsocket) << "Connection to" << describePeer()
                                                       << "shutting down";
                    readyState = ReadyState::Shutdown;
                    for (auto &c : startupConnections) {
                        QObject::disconnect(c);
                    }
                    startupTimeout.reset();
                    destroy();
                });
                thread->quit();
                thread->wait();
            } else {
                /*
                 * This can happen if the last reference to the shared pointer is via a
                 * queued call.
                 */

                qCDebug(log_io_driver_localsocket) << "Connection to" << describePeer()
                                                   << "shutting down";
                readyState = ReadyState::Shutdown;
                for (auto &c : startupConnections) {
                    QObject::disconnect(c);
                }
                startupTimeout.reset();
                destroy();
                thread->quit();

                /*
                 * Note the context is the thread, which will be owned by another thread, so
                 * we rely on that parent thread's event loop to do the delete.
                 */
                Q_ASSERT(thread->thread() != QThread::currentThread());
                auto thr = std::make_shared<std::unique_ptr<QThread>>(std::move(thread));
                Threading::runQueuedFunctor(thr->get(), [thr]() {
                    (*thr)->wait();
                    thr->reset();
                });
            }
        }

        void stateUpdated()
        {
            switch (socket.state()) {
            case QLocalSocket::UnconnectedState:
            case QLocalSocket::ClosingState:
                if (readyState != ReadyState::Connecting)
                    break;
                readyState = ReadyState::Failed;
                qCDebug(log_io_driver_localsocket) << "Connection to" << describePeer()
                                                   << "closed during startup:"
                                                   << socket.errorString();

                for (auto &c : startupConnections) {
                    QObject::disconnect(c);
                }
                startupConnections.clear();
                startupTimeout.reset();

                if (readyFunc) {
                    readyFunc(false);
                    readyFunc = std::function<void(bool isOk)>();
                }
                break;
            case QLocalSocket::ConnectedState:
                return connectionReadyToUse();
            default:
                break;
            }
        }

        void runOnConnection(std::function<void(bool isOk)> func)
        {
            switch (readyState) {
            case ReadyState::Connecting:
                break;
            case ReadyState::Ready:
                func(true);
                return;
            case ReadyState::Failed:
            case ReadyState::Shutdown:
                func(false);
                return;
            }

            readyFunc = std::move(func);
        }

        std::unique_ptr<QThread> releaseThread()
        { return std::move(thread); }

        std::string describePeer() const override
        { return name; }

        std::string serverName() const override
        { return name; }
    };

    class Thread final : public QThread {
        Result *result;
        std::unique_ptr<QLocalSocket> socket;

        void resultReady()
        {
            {
                std::lock_guard<std::mutex> lock(result->mutex);
                result->ready = true;
                result->notify.notify_all();
            }
            result = nullptr;
        }

    public:
        explicit Thread(Result *result) : result(result)
        { setObjectName("LocalSocketClientQThread"); }

        virtual ~Thread() = default;

    protected:
        void run() override
        {
            socket.reset(new QLocalSocket);
            std::unique_ptr<OutgoingConnection>
                    connection(new OutgoingConnection(*socket, *result));

            qCDebug(log_io_driver_localsocket) << "Connection to local server" << result->serverName
                                               << "started";
            socket->connectToServer(result->serverName,
                                    QIODevice::ReadWrite | QIODevice::Unbuffered);
            connection->stateUpdated();

            if (result->synchronous) {
                qCDebug(log_io_driver_localsocket) << "Synchronous connection to"
                                                   << connection->describePeer() << "started";
                connection->runOnConnection([this, &connection](bool isOk) {
                    if (!isOk) {
                        qCDebug(log_io_driver_localsocket) << "Synchronous connection failed:"
                                                           << socket->errorString();

                        result->thread = connection->releaseThread();
                        QThread::quit();
                        return;
                    }

                    qCDebug(log_io_driver_localsocket) << "Synchronous connection to"
                                                       << connection->describePeer() << "completed";
                    result->connection = std::move(connection);
                    resultReady();
                });
            } else {
                qCDebug(log_io_driver_localsocket) << "Asynchronous connection to"
                                                   << connection->describePeer() << "started";
                result->connection = std::move(connection);
                resultReady();
            }

            QThread::exec();

            connection.reset();
            socket.reset();

            if (result)
                return resultReady();
        }
    };

    result.thread = std::unique_ptr<QThread>(new Thread(&result));
    result.thread->start();

    {
        std::unique_lock<std::mutex> lock(result.mutex);
        result.notify.wait(lock, [&result]() { return result.ready; });
    }
    Q_ASSERT(result.ready);

    if (result.thread) {
        result.thread->wait();
    }

    return std::move(result.connection);
}

std::unique_ptr<Connection> connect(const std::string &server,
                                    const Configuration &config,
                                    bool synchronous)
{ return connect(QString::fromStdString(server), config, synchronous); }

std::unique_ptr<Connection> connect(const char *server,
                                    const Configuration &config,
                                    bool synchronous)
{ return connect(QString(server), config, synchronous); }


class Server::Worker final : public std::enable_shared_from_this<Server::Worker> {
    IncomingConnection incoming;
    Configuration config;
    std::string name;

    std::unique_ptr<QThread> thread;
    QObject *receiver;

    std::mutex mutex;
    std::condition_variable notify;

    bool serverStarted;
    bool serverClosed;

    std::unordered_map<QLocalSocket *, std::unique_ptr<Connection>> handshakeConnections;

    class LocalServer final : public QLocalServer {
    public:
        std::vector<std::unique_ptr<QLocalSocket>> pendingConnections;

        LocalServer() : QLocalServer(nullptr)
        { }

        virtual ~LocalServer() = default;

    protected:
        void incomingConnection(quintptr socketDescriptor) override
        {
            std::unique_ptr<QLocalSocket> socket(new QLocalSocket);
            socket->setSocketDescriptor(socketDescriptor, QLocalSocket::ConnectedState,
                                        QIODevice::ReadWrite | QIODevice::Unbuffered);
            pendingConnections.emplace_back(std::move(socket));
            emit newConnection();
        }
    };

    std::unique_ptr<LocalServer> server;

    void startup()
    {
        struct Result {
            std::mutex mutex;
            std::condition_variable notify;
            bool ready;

            QObject *receiver;

            Result() : ready(false), receiver(nullptr)
            { }
        };

        Result result;

        class Thread final : public QThread {
            Result *result;
            std::unique_ptr<QObject> receiver;

            void resultReady()
            {
                {
                    std::lock_guard<std::mutex> lock(result->mutex);
                    result->ready = true;
                    result->notify.notify_all();
                }
                result = nullptr;
            }

        public:
            explicit Thread(Result *result) : result(result)
            { setObjectName("LocalSocketServerQThread"); }

            virtual ~Thread() = default;

        protected:
            void run() override
            {
                receiver.reset(new QObject);

                result->receiver = receiver.get();
                resultReady();

                QThread::exec();

                receiver.reset();
            }
        };

        thread = std::unique_ptr<QThread>(new Thread(&result));
        thread->start();

        {
            std::unique_lock<std::mutex> lock(result.mutex);
            result.notify.wait(lock, [&result]() { return result.ready; });
        }
        Q_ASSERT(result.ready);

        Q_ASSERT(result.receiver);
        receiver = result.receiver;
    }

    void shutdown()
    {
        Q_ASSERT(receiver->thread() == QThread::currentThread());

        if (server)
            server->close();
        server.reset();

        /*
         * This can only be called when there are no more connections (handshake or active),
         * since they keep a pointer to us.  So we don't have to worry about destroying them or the
         * servers destroying their sockets, since they're parented to the servers.
         */
        Q_ASSERT(handshakeConnections.empty());
    }

    void attachSocket(std::unique_ptr<QLocalSocket> &&socket)
    {
        class IncomingConnection final : public Connection {
            std::unique_ptr<QLocalSocket> ownedSocket;
            std::string name;
            std::shared_ptr<Worker> worker;
            std::unique_ptr<QTimer> startupTimeout;

            enum class ReadyState {
                Connecting, Ready, Failed, Shutdown
            } readyState;

            void connectionReadyToUse()
            {
                if (readyState != ReadyState::Connecting)
                    return;
                qCDebug(log_io_driver_localsocket) << "Local socket connection on" << describePeer()
                                                   << "ready";
                readyState = ReadyState::Ready;

                for (auto &c : startupConnections) {
                    QObject::disconnect(c);
                }
                startupConnections.clear();
                startupTimeout.reset();

                auto &container = worker->handshakeConnections;
                auto handshakeRef = container.find(ownedSocket.get());
                Q_ASSERT(handshakeRef != container.end());
                auto ptr = std::move(handshakeRef->second);
                container.erase(handshakeRef);

                const auto &call = worker->incoming;
                call(std::move(ptr));
                /* Self potentially deleted */
            }

        protected:
            std::vector<QMetaObject::Connection> startupConnections;

        public:
            IncomingConnection(std::unique_ptr<QLocalSocket> &&socket,
                               std::shared_ptr<Worker> worker) : Connection(*socket,
                                                                            worker->config),
                                                                 ownedSocket(std::move(socket)),
                                                                 name(worker->describeServer()),
                                                                 worker(std::move(worker)),
                                                                 startupTimeout(new QTimer),
                                                                 readyState(ReadyState::Connecting)
            {
                startupConnections.emplace_back(
                        QObject::connect(&(this->socket), &QLocalSocket::stateChanged,
                                         std::bind(&IncomingConnection::stateUpdated, this)));

                startupTimeout->setSingleShot(true);
                startupConnections.emplace_back(
                        QObject::connect(startupTimeout.get(), &QTimer::timeout, &(this->socket),
                                         &QIODevice::close, Qt::DirectConnection));
                startupTimeout->start(30000);
            }

            virtual ~IncomingConnection()
            {
                if (socket.thread() != QThread::currentThread()) {
                    /* Only possible after startup has completed (i.e. connection removed
                     * from handshake) */
                    Q_ASSERT(startupConnections.empty());

                    Threading::runBlockingFunctor(&socket, [this]() {
                        qCDebug(log_io_driver_localsocket) << "Connection on" << describePeer()
                                                           << "shutting down";
                        readyState = ReadyState::Shutdown;
                        destroy();

                        /* Queue the socket for deletion by the worker thread, since we now have the
                         * only reference to it */
                        ownedSocket.release()->deleteLater();
                    });
                } else {
                    /*
                     * This can happen if the last reference to the shared pointer is via a
                     * queued call or if the socket fails during handshake.
                     */

                    for (auto &c : startupConnections) {
                        QObject::disconnect(c);
                    }
                    startupTimeout.reset();

                    qCDebug(log_io_driver_localsocket) << "Connection on" << describePeer()
                                                       << "shutting down";
                    readyState = ReadyState::Shutdown;
                    destroy();

                    /* Queue the socket for deletion by the worker thread, since we now have the
                     * only reference to it */
                    ownedSocket.release()->deleteLater();
                }
            }

            void stateUpdated()
            {
                switch (socket.state()) {
                case QLocalSocket::UnconnectedState:
                case QLocalSocket::ClosingState: {
                    if (readyState != ReadyState::Connecting)
                        break;
                    readyState = ReadyState::Failed;
                    qCDebug(log_io_driver_localsocket) << "Connection on" << describePeer()
                                                       << "closed during startup:"
                                                       << socket.errorString();

                    for (auto &c : startupConnections) {
                        QObject::disconnect(c);
                    }
                    startupConnections.clear();
                    startupTimeout.reset();

                    auto &container = worker->handshakeConnections;
                    container.erase(ownedSocket.get());
                    /* Self deleted */
                    return;
                }
                case QLocalSocket::ConnectedState:
                    return connectionReadyToUse();
                default:
                    break;
                }
            }

            std::string describePeer() const override
            { return "[" + name + "]"; }

            std::string serverName() const override
            { return name; }
        };

        auto ptr = socket.get();
        IncomingConnection
                *connection = new IncomingConnection(std::move(socket), shared_from_this());
        handshakeConnections.emplace(ptr, std::unique_ptr<Connection>(connection));
        connection->stateUpdated();
    }

    void acceptConnections()
    {
        if (!server)
            return;
        for (auto &conn : server->pendingConnections) {
            attachSocket(std::move(conn));
        }
        server->pendingConnections.clear();
    }

    void shutdownServer()
    {
        if (!server)
            return;
        server->close();
        for (const auto &conn : server->pendingConnections) {
            conn->close();
        }
        server->pendingConnections.clear();
    }

public:
    Worker(IncomingConnection &&incoming, std::string &&name, Configuration config) : incoming(
            std::move(incoming)),
                                                                                      config(std::move(
                                                                                              config)),
                                                                                      name(std::move(
                                                                                              name)),
                                                                                      receiver(
                                                                                              nullptr),
                                                                                      serverStarted(
                                                                                              false),
                                                                                      serverClosed(
                                                                                              false)
    { startup(); }

    ~Worker()
    {
        if (!thread)
            return;
        Q_ASSERT(receiver);
        if (receiver->thread() != QThread::currentThread()) {
            Threading::runBlockingFunctor(receiver, [this]() {
                qCDebug(log_io_driver_localsocket) << "Local server" << describeServer()
                                                   << "shutting down";
                shutdown();
            });
            thread->quit();
            thread->wait();
        } else {
            /*
             * This can happen if the last reference to the shared pointer is via a
             * queued call.
             */

            qCDebug(log_io_driver_localsocket) << "Local server" << describeServer()
                                               << "shutting down";
            shutdown();
            thread->quit();

            /*
             * Note the context is the thread, which will be owned by another thread, so
             * we rely on that parent thread's event loop to do the delete.
             */
            Q_ASSERT(thread->thread() != QThread::currentThread());
            auto thr = std::make_shared<std::unique_ptr<QThread>>(std::move(thread));
            Threading::runQueuedFunctor(thr->get(), [thr]() {
                (*thr)->wait();
                thr->reset();
            });
        }
    }

    bool startListening(bool removeExisting)
    {
        Q_ASSERT(receiver->thread() != QThread::currentThread());

        {
            std::unique_lock<std::mutex> lock(mutex);
            if (serverStarted) {
                lock.unlock();
                qCWarning(log_io_driver_localsocket) << "Local server already listening";
                return false;
            }
            serverStarted = true;
        }

        bool ok = false;
        Threading::runBlockingFunctor(receiver, [this, removeExisting, &ok]() {
            if (name.empty()) {
                qCDebug(log_io_driver_localsocket) << "No server name available to listen on";
                return;
            }

            if (server) {
                qCDebug(log_io_driver_localsocket) << "Server" << describeServer()
                                                   << "already listening";
                return;
            }

            QString socketName = QString::fromStdString(name);
            if (removeExisting) {
                QLocalServer::removeServer(socketName);
            }

            server.reset(new LocalServer);
            if (!server->listen(socketName)) {
                qCDebug(log_io_driver_localsocket) << "Failed to listen on" << name << ":"
                                                   << server->errorString();
                return;
            }

            qCDebug(log_io_driver_localsocket) << "Local server" << describeServer()
                                               << "accepting connections";

            QObject::connect(server.get(), &QLocalServer::newConnection,
                             std::bind(&Worker::acceptConnections, this));
            acceptConnections();

            ok = true;
        });

        if (!ok) {
            std::lock_guard<std::mutex> lock(mutex);
            serverClosed = true;
        }

        return ok;
    }

    void stopListening()
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (!serverStarted)
                return;
            if (serverClosed)
                return;
            serverClosed = true;
        }

        if (receiver->thread() == QThread::currentThread()) {
            shutdownServer();
            handshakeConnections.clear();
            qCDebug(log_io_driver_localsocket) << "Local server" << describeServer()
                                               << "no longer accepting connections";
            return;
        }

        Threading::runBlockingFunctor(receiver, [this]() {
            shutdownServer();
            handshakeConnections.clear();
            qCDebug(log_io_driver_localsocket) << "Local server" << describeServer()
                                               << "no longer accepting connections";
        });
    }

    bool isListening()
    {
        std::lock_guard<std::mutex> lock(mutex);
        return serverStarted && !serverClosed;
    }

    std::string describeServer() const
    { return name; }
};

Server::~Server()
{ stopListening(); }

Server::Server(Server::IncomingConnection connection, std::string name, Configuration config)
        : Socket::Server(connection),
          worker(std::make_shared<Worker>(std::move(connection), std::move(name),
                                          std::move(config)))
{ }

Server::Server(Server::IncomingConnection connection, const QString &name, Configuration config)
        : Server(std::move(connection), name.toStdString(), std::move(config))
{ }

Server::Server(Server::IncomingConnection connection, const char *name, Configuration config)
        : Server(std::move(connection), std::string(name), std::move(config))
{ }

bool Server::startListening(bool requireAll)
{ return worker->startListening(requireAll); }

void Server::stopListening()
{ return worker->stopListening(); }

bool Server::isListening() const
{ return worker->isListening(); }

std::string Server::describeServer() const
{ return worker->describeServer(); }

}
}
}
}