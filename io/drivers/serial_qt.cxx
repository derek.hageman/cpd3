/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <chrono>
#include <thread>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QLoggingCategory>

#include "serial.hxx"
#include "qiowrapper.hxx"
#include "core/threading.hxx"
#include "core/qtcompat.hxx"

Q_LOGGING_CATEGORY(log_io_driver_serial, "cpd3.io.driver.serial", QtWarningMsg)


namespace CPD3 {
namespace IO {

namespace {

class QtSerialStream : public Serial::Stream {
    std::unique_ptr<QThread> thread;
    QSerialPort &port;
    QIOWrapper wrapper;
    bool hardwareFlowControlRequested;
    bool softwareFlowControlRequested;

    void transferredWrite(Util::ByteArray &&data)
    {
        struct Local {
            QIOWrapper &wrapper;
            Util::ByteArray data;

            Local(QIOWrapper &wrapper, Util::ByteArray &&data) : wrapper(wrapper),
                                                                 data(std::move(data))
            { }
        };

        auto local = std::make_shared<Local>(wrapper, std::move(data));
        Threading::runQueuedFunctor(&port, [local]() {
            local->wrapper.write(std::move(local->data));
            local->wrapper.processWriteBuffer();
        });
    }

    std::string getDescription() const
    {
        Q_ASSERT(port.thread() == QThread::currentThread());
        std::string desc = port.portName().toStdString();
        desc += ":";

        desc += std::to_string(port.baudRate());

        switch (port.parity()) {
        case QSerialPort::NoParity:
        default:
            desc += "N";
            break;
        case QSerialPort::EvenParity:
            desc += "E";
            break;
        case QSerialPort::OddParity:
            desc += "O";
            break;
        case QSerialPort::MarkParity:
            desc += "M";
            break;
        case QSerialPort::SpaceParity:
            desc += "S";
            break;
        }

        switch (port.dataBits()) {
        case QSerialPort::Data5:
            desc += "5";
            break;
        case QSerialPort::Data6:
            desc += "6";
            break;
        case QSerialPort::Data7:
            desc += "7";
            break;
        case QSerialPort::Data8:
        default:
            desc += "8";
            break;
        }

        switch (port.stopBits()) {
        case QSerialPort::OneStop:
        default:
            desc += "1";
            break;
        case QSerialPort::TwoStop:
            desc += "2";
            break;
        case QSerialPort::OneAndHalfStop:
            desc += "H";
            break;
        }

        return desc;
    }

    bool doFlush(bool blank)
    {
        Q_ASSERT(port.thread() == QThread::currentThread());

        wrapper.discardWriteBuffer();
        port.flush();
        if (!port.clear())
            return false;

        std::chrono::duration<double> sleepTime(0.1);
        if (blank) {
            auto start = std::chrono::steady_clock::now();
            if (!port.sendBreak())
                return false;
            auto end = std::chrono::steady_clock::now();
            std::chrono::duration<double> elapsed = end - start;
            auto n = elapsed.count();
            if (n < 0.5) {
                sleepTime += std::chrono::duration<double>(0.5 - n);
            }
        }

        auto effectiveBaud = port.baudRate();
        if (effectiveBaud > 0) {
            static constexpr std::uint_fast64_t symbolsToSleep = 10 * 2; /* ~2 frames */
            auto hzToSleep = static_cast<double>(effectiveBaud) * symbolsToSleep;
            sleepTime += std::chrono::duration<double>(1.0 / hzToSleep);
        }

        std::this_thread::sleep_for(sleepTime);

        std::size_t nFlush = 4096;
        if (effectiveBaud > 0) {
            auto flushBytes = static_cast<std::size_t>(effectiveBaud * sleepTime.count());
            flushBytes /= 10;
            flushBytes += 1;
            if (flushBytes > nFlush) {
                nFlush = flushBytes;
            }
        }

        QCoreApplication::processEvents();
        Util::ByteArray buffer;
        while (nFlush > 0 && port.waitForReadyRead(0)) {
            buffer.resize(4096);
            auto n = port.read(buffer.data<char *>(), std::min<qint64>(buffer.size(), nFlush));
            if (n <= 0)
                break;
            Q_ASSERT(static_cast<std::size_t>(n) <= nFlush);
            nFlush -= static_cast<std::size_t>(n);
        }

        return true;
    }

public:
    QtSerialStream(std::string name, QSerialPort &port, std::unique_ptr<QThread> &&thread)
            : Serial::Stream(std::move(name)),
              thread(std::move(thread)),
              port(port),
              wrapper(port),
              hardwareFlowControlRequested(false),
              softwareFlowControlRequested(false)
    {
        read = wrapper.read;
        ended = wrapper.ended;
        writeStall = wrapper.writeStall;
    }

    virtual ~QtSerialStream()
    {
        if (port.thread() != QThread::currentThread()) {
            Threading::runBlockingFunctor(&port, [this]() { wrapper.destroy(); });
            thread->quit();
            thread->wait();
        } else {
            /*
             * This can happen if the last reference to the shared pointer is via a
             * queued call.
             */

            wrapper.destroy();
            thread->quit();

            /*
             * Note the context is the thread, which will be owned by another thread, so
             * we rely on that parent thread's event loop to do the delete.
             */
            Q_ASSERT(thread->thread() != QThread::currentThread());
            auto thr = std::make_shared<std::unique_ptr<QThread>>(std::move(thread));
            Threading::runQueuedFunctor(thr->get(), [thr]() {
                (*thr)->wait();
                thr->reset();
            });
        }
    }

    void write(const Util::ByteView &data) override
    { transferredWrite(Util::ByteArray(data)); }

    void write(Util::ByteArray &&data) override
    { transferredWrite(std::move(data)); }

    void start() override
    { Threading::runQueuedFunctor(&port, std::bind(&QIOWrapper::start, &wrapper)); }

    bool isEnded() const override
    {
        if (port.thread() == QThread::currentThread())
            return wrapper.isEnded();
        bool result = false;
        Threading::runBlockingFunctor(&port, [this, &result]() {
            result = wrapper.isEnded();
        });
        return result;
    }

    void readStall(bool enable) override
    {
        if (port.thread() == QThread::currentThread())
            return wrapper.readStall(enable);
        Threading::runBlockingFunctor(&port, [this, enable]() {
            wrapper.readStall(enable);
        });
    }


    std::string describePort() const override
    {
        if (port.thread() == QThread::currentThread())
            return getDescription();
        std::string desc;
        Threading::runBlockingFunctor(&port, [this, &desc]() {
            desc = getDescription();
        });
        return desc;
    }

    std::string describeError() const override
    {
        if (port.thread() == QThread::currentThread())
            return port.errorString().toStdString();
        QString error;
        Threading::runBlockingFunctor(&port, [this, &error]() {
            error = port.errorString();
        });
        return error.toStdString();
    }

    bool setBaud(int rate) override
    {
        if (port.thread() == QThread::currentThread())
            return port.setBaudRate(rate);
        bool ok = false;
        Threading::runBlockingFunctor(&port, [this, rate, &ok]() {
            ok = port.setBaudRate(rate);
        });
        return ok;
    }

    bool setDataBits(int bits) override
    {
        QSerialPort::DataBits qBits = QSerialPort::Data8;
        switch (bits) {
        case 5:
            qBits = QSerialPort::Data5;
            break;
        case 6:
            qBits = QSerialPort::Data6;
            break;
        case 7:
            qBits = QSerialPort::Data7;
            break;
        case 8:
            qBits = QSerialPort::Data8;
            break;
        default:
            qCDebug(log_io_driver_serial) << "Invalid number of data bits (" << bits << ")";
            return false;
        }
        if (port.thread() == QThread::currentThread())
            return port.setDataBits(qBits);
        bool ok = false;
        Threading::runBlockingFunctor(&port, [this, qBits, &ok]() {
            ok = port.setDataBits(qBits);
        });
        return ok;
    }

    bool setStopBits(int bits) override
    {
        QSerialPort::StopBits qBits = QSerialPort::OneStop;
        switch (bits) {
        case 1:
            qBits = QSerialPort::OneStop;
            break;
        case 2:
            qBits = QSerialPort::TwoStop;
            break;
        case 3:
            qBits = QSerialPort::OneAndHalfStop;
            break;
        default:
            qCDebug(log_io_driver_serial) << "Invalid number of stop bits (" << bits << ")";
            return false;
        }
        if (port.thread() == QThread::currentThread())
            return port.setStopBits(qBits);
        bool ok = false;
        Threading::runBlockingFunctor(&port, [this, qBits, &ok]() {
            ok = port.setStopBits(qBits);
        });
        return ok;
    }

    bool setParity(Parity parity) override
    {
        QSerialPort::Parity qParity = QSerialPort::NoParity;
        switch (parity) {
        case Parity::None:
            qParity = QSerialPort::NoParity;
            break;
        case Parity::Even:
            qParity = QSerialPort::EvenParity;
            break;
        case Parity::Odd:
            qParity = QSerialPort::OddParity;
            break;
        case Parity::Mark:
            qParity = QSerialPort::MarkParity;
            break;
        case Parity::Space:
            qParity = QSerialPort::SpaceParity;
            break;
        }
        if (port.thread() == QThread::currentThread())
            return port.setParity(qParity);
        bool ok = false;
        Threading::runBlockingFunctor(&port, [this, qParity, &ok]() {
            ok = port.setParity(qParity);
        });
        return ok;
    }

    bool setNoHardwareFlowControl() override
    {
        if (port.thread() == QThread::currentThread()) {
            hardwareFlowControlRequested = false;
            if (softwareFlowControlRequested) {
                return port.setFlowControl(QSerialPort::SoftwareControl);
            } else {
                return port.setFlowControl(QSerialPort::NoFlowControl);
            }
        }
        bool ok = false;
        Threading::runBlockingFunctor(&port, [this, &ok]() {
            hardwareFlowControlRequested = false;
            if (softwareFlowControlRequested) {
                ok = port.setFlowControl(QSerialPort::SoftwareControl);
            } else {
                ok = port.setFlowControl(QSerialPort::NoFlowControl);
            }
        });
        return ok;
    }

    bool setRS232StandardFlowControl() override
    {
        if (port.thread() == QThread::currentThread()) {
            hardwareFlowControlRequested = true;
            return port.setFlowControl(QSerialPort::HardwareControl);
        }
        bool ok = false;
        Threading::runBlockingFunctor(&port, [this, &ok]() {
            hardwareFlowControlRequested = true;
            ok = port.setFlowControl(QSerialPort::HardwareControl);
        });
        return ok;
    }


    bool setExplicitRTSOnTransmit(double, double, bool, bool) override
    { return false; }


    bool setSoftwareFlowControl(SoftwareFlowControl control, bool) override
    {
        bool enable = false;
        switch (control) {
        case SoftwareFlowControl::Disable:
            enable = false;
            break;
        case SoftwareFlowControl::Both:
            enable = true;
            break;
        case SoftwareFlowControl::XON:
            qCDebug(log_io_driver_serial) << "XON only not supported";
            return false;
        case SoftwareFlowControl::XOFF:
            qCDebug(log_io_driver_serial) << "XOFF only not supported";
            return false;
        }

        if (port.thread() == QThread::currentThread()) {
            softwareFlowControlRequested = enable;
            if (softwareFlowControlRequested) {
                return port.setFlowControl(QSerialPort::SoftwareControl);
            } else if (hardwareFlowControlRequested) {
                return port.setFlowControl(QSerialPort::HardwareControl);
            } else {
                return port.setFlowControl(QSerialPort::NoFlowControl);
            }
        }
        bool ok = false;
        Threading::runBlockingFunctor(&port, [this, enable, &ok]() {
            softwareFlowControlRequested = enable;
            if (softwareFlowControlRequested) {
                ok = port.setFlowControl(QSerialPort::SoftwareControl);
            } else if (hardwareFlowControlRequested) {
                ok = port.setFlowControl(QSerialPort::HardwareControl);
            } else {
                ok = port.setFlowControl(QSerialPort::NoFlowControl);
            }
        });
        return ok;
    }


    bool flushPort(bool blank = true) override
    {
        if (port.thread() == QThread::currentThread())
            return doFlush(blank);
        bool ok = false;
        Threading::runBlockingFunctor(&port, [this, blank, &ok]() {
            ok = doFlush(blank);
        });
        return ok;
    }

    bool pulseRTS(double, bool) override
    { return false; }

    bool pulseDTR(double, bool) override
    { return false; }
};

}

namespace Access {

std::shared_ptr<Serial::Backing> serial(std::string name)
{
    class Backing : public Serial::Backing {
    public:
        Backing(std::string &&name) : Serial::Backing(std::move(name))
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        {
            struct Result {
                const std::string &portName;

                std::mutex mutex;
                std::condition_variable notify;
                bool ready;

                std::unique_ptr<QtSerialStream> stream;
                std::unique_ptr<QThread> thread;

                explicit Result(const std::string &portName) : portName(portName)
                { }
            };

            Result result(portName());

            class Thread final : public QThread {
                Result *result;
                std::unique_ptr<QSerialPort> port;

                void resultReady()
                {
                    {
                        std::lock_guard<std::mutex> lock(result->mutex);
                        result->ready = true;
                        result->notify.notify_all();
                    }
                    result = nullptr;
                }

            public:
                explicit Thread(Result *result) : result(result)
                { setObjectName("SerialPortQThread"); }

                virtual ~Thread() = default;

            protected:
                void run() override
                {
                    port.reset(new QSerialPort(QString::fromStdString(result->portName)));

                    if (!port->open(QIODevice::ReadWrite)) {
                        qCDebug(log_io_driver_serial) << "Error opening port" << result->portName
                                                      << ":" << port->errorString();
                        port.reset();
                        return resultReady();
                    }

                    result->stream
                          .reset(new QtSerialStream(result->portName, *port,
                                                    std::move(result->thread)));
                    resultReady();

                    QThread::exec();

                    port->close();
                    port.reset();
                }
            };

            result.thread = std::unique_ptr<QThread>(new Thread(&result));
            result.thread->start();

            {
                std::unique_lock<std::mutex> lock(result.mutex);
                result.notify.wait(lock, [&result]() { return result.ready; });
            }
            Q_ASSERT(result.ready);

            if (result.thread) {
                result.thread->wait();
            }

            return std::move(result.stream);
        }
    };
    return std::make_shared<Backing>(std::move(name));
}

}

namespace Serial {

bool Backing::isValid(const std::string &name)
{
    QSerialPort port(QString::fromStdString(name));
    if (!port.open(QIODevice::ReadWrite))
        return false;
    port.close();
    return true;
}

std::unordered_set<std::string> enumeratePorts()
{
    std::unordered_set<std::string> result;
    for (const auto &port : QSerialPortInfo::availablePorts()) {
#ifdef Q_OS_UNIX
        result.insert(port.systemLocation().toStdString());
#else
        result.insert(port.portName().toStdString());
#endif
    }
    return result;
}

}

}
}