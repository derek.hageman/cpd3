/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "io/access.hxx"

using namespace CPD3;
using namespace CPD3::IO;


class TestAccessSimple : public QObject {
Q_OBJECT

private slots:

    void noopEnding()
    {
        auto backing = Access::noop(false);
        QVERIFY(backing.get() != nullptr);

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            bool didEnd = false;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            stream->ended.connect([&didEnd]() { didEnd = true; });
            stream->start();
            QVERIFY(didEnd);
            QVERIFY(dataRead.empty());
            QVERIFY(stream->isEnded());
            stream->readStall(true);

            Util::ByteArray contents("Asdf");
            stream->write(contents);
            stream->write(Util::ByteView(contents));
            stream->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                stream->write(qba);
            }
            stream->write(std::move(contents));
            stream->write("Asdfg");
            stream->write(std::string("QWERTY"));
            stream->write(QString("ZXCV"));
        }
        {
            auto block = backing->block();
            QVERIFY(block.get() != nullptr);

            {
                Util::ByteArray target;
                block->read(target, 10);
                QVERIFY(target.empty());
            }
            {
                QByteArray target;
                block->read(target, 10);
                QVERIFY(target.isEmpty());
            }
            QVERIFY(block->readFinished());

            Util::ByteArray contents("Asdf");
            block->write(contents);
            block->write(Util::ByteView(contents));
            block->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                block->write(qba);
            }
            block->write(std::move(contents));
            block->write("Asdfg");
            block->write(std::string("QWERTY"));
            block->write(QString("ZXCV"));

            QVERIFY(!block->inError());
            QVERIFY(block->describeError().empty());
        }
    }

    void noopEndless()
    {
        auto backing = Access::noop(true);
        QVERIFY(backing.get() != nullptr);

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            bool didEnd = false;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            stream->ended.connect([&didEnd]() { didEnd = true; });
            stream->start();
            QVERIFY(!didEnd);
            QVERIFY(dataRead.empty());
            QVERIFY(!stream->isEnded());
            stream->readStall(true);

            Util::ByteArray contents("Asdf");
            stream->write(contents);
            stream->write(Util::ByteView(contents));
            stream->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                stream->write(qba);
            }
            stream->write(std::move(contents));
            stream->write("Asdfg");
            stream->write(std::string("QWERTY"));
            stream->write(QString("ZXCV"));
        }
        {
            auto block = backing->block();
            QVERIFY(block.get() != nullptr);

            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE((int) target.size(), 10);
            }
            {
                QByteArray target;
                block->read(target, 11);
                QCOMPARE((int) target.size(), 11);
            }
            QVERIFY(!block->readFinished());

            Util::ByteArray contents("Asdf");
            block->write(contents);
            block->write(Util::ByteView(contents));
            block->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                block->write(qba);
            }
            block->write(std::move(contents));
            block->write("Asdfg");
            block->write(std::string("QWERTY"));
            block->write(QString("ZXCV"));

            QVERIFY(!block->inError());
            QVERIFY(block->describeError().empty());
        }
    }

    void loopback()
    {
        auto backing = Access::loopback();
        QVERIFY(backing.get() != nullptr);

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            bool didEnd = false;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            stream->ended.connect([&didEnd]() { didEnd = true; });
            stream->start();
            QVERIFY(!didEnd);
            QVERIFY(dataRead.empty());
            QVERIFY(!stream->isEnded());

            Util::ByteArray contents("Asdf");
            stream->write(contents);
            stream->write(Util::ByteView(contents));
            stream->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                stream->write(qba);
            }
            stream->write(std::move(contents));
            stream->write("Asdfg");
            stream->write(std::string("QWERTY"));
            stream->write(QString("ZXCV"));

            QCOMPARE(dataRead.toQByteArrayRef(), QByteArray("AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCV"));
        }
        {
            auto block = backing->block();
            QVERIFY(block.get() != nullptr);

            {
                Util::ByteArray target;
                block->read(target, 10);
                QVERIFY(target.empty());
            }
            {
                QByteArray target;
                block->read(target, 10);
                QVERIFY(target.isEmpty());
            }
            QVERIFY(block->readFinished());

            Util::ByteArray contents("Asdf");
            block->write(contents);
            block->write(Util::ByteView(contents));
            block->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                block->write(qba);
            }
            block->write(std::move(contents));
            block->write("Asdfg");
            block->write(std::string("QWERTY"));
            block->write(QString("ZXCV"));

            block->seek(block->tell() - 2);
            block->write("123");
            block->move(-3);
            QVERIFY(!block->readFinished());
            {
                QByteArray target;
                block->read(target, 3);
                QCOMPARE(target, QByteArray("123"));
            }
            QVERIFY(block->readFinished());

            block->seek(0);
            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), QByteArray("AsdfAsdfAs"));
            }
            QVERIFY(!block->readFinished());
            QCOMPARE((int) block->tell(), 10);
            {
                Util::ByteArray target;
                block->read(target, 50);
                QCOMPARE(target.toQByteArrayRef(), QByteArray("dfAsdfAsdfAsdfgQWERTYZX123"));
            }
            QVERIFY(block->readFinished());
        }
    }

    void pipeline()
    {
        auto pipe = Access::pipe();

        Util::ByteArray dataFirst;
        bool endFirst = false;
        pipe.first->read.connect([&dataFirst](const Util::ByteArray &d) { dataFirst += d; });
        pipe.first->ended.connect([&endFirst]() { endFirst = true; });

        Util::ByteArray dataSecond;
        bool endSecond = false;
        pipe.second->read.connect([&dataSecond](const Util::ByteArray &d) { dataSecond += d; });
        pipe.second->ended.connect([&endSecond]() { endSecond = true; });

        Util::ByteArray contents("Asdf");
        pipe.first->write(contents);
        pipe.first->write(Util::ByteView(contents));
        pipe.first->write(contents.toQByteArray());
        {
            auto qba = contents.toQByteArray();
            pipe.first->write(qba);
        }
        pipe.first->write(std::move(contents));
        pipe.first->write("Asdfg");
        pipe.first->write(std::string("QWERTY"));
        pipe.first->write(QString("ZXCV"));

        QVERIFY(dataFirst.empty());
        QVERIFY(!endFirst);
        QVERIFY(dataSecond.empty());
        QVERIFY(!endSecond);

        pipe.second->start();
        QCOMPARE(dataSecond.toQByteArrayRef(), QByteArray("AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCV"));
        QVERIFY(!endSecond);

        pipe.first->write("12345");
        QCOMPARE(dataSecond.toQByteArrayRef(),
                 QByteArray("AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCV12345"));
        QVERIFY(!endSecond);

        pipe.first->start();
        pipe.second->write("QWERTY");
        QCOMPARE(dataFirst.toQByteArrayRef(), QByteArray("QWERTY"));
        QVERIFY(!endFirst);

        QCOMPARE(dataSecond.toQByteArrayRef(),
                 QByteArray("AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCV12345"));
        QVERIFY(!endSecond);
        pipe.first.reset();
        QVERIFY(endSecond);
        QCOMPARE(dataSecond.toQByteArrayRef(),
                 QByteArray("AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCV12345"));

        QVERIFY(!endFirst);
        QCOMPARE(dataFirst.toQByteArrayRef(), QByteArray("QWERTY"));
        pipe.second.reset();
    }

    void bufferStatic()
    {
        Util::ByteArray data("AbCDEF1234567890");
        auto backing = Access::buffer(data);
        QVERIFY(backing.get() != nullptr);

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            bool didEnd = false;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            stream->ended.connect([&didEnd]() { didEnd = true; });
            stream->start();
            QVERIFY(didEnd);
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(stream->isEnded());

            Util::ByteArray contents("Asdf");
            stream->write(contents);
            stream->write(Util::ByteView(contents));
            stream->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                stream->write(qba);
            }
            stream->write(std::move(contents));
            stream->write("Asdfg");
            stream->write(std::string("QWERTY"));
            stream->write(QString("ZXCV"));

            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
        }
        {
            auto block = backing->block();
            QVERIFY(block.get() != nullptr);
            QVERIFY(!block->readFinished());

            Util::ByteArray dataRead;
            block->read(dataRead);
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(block->readFinished());

            Util::ByteArray contents("Asdf");
            block->write(contents);
            block->write(Util::ByteView(contents));
            block->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                block->write(qba);
            }
            block->write(std::move(contents));
            block->write("Asdfg");
            block->write(std::string("QWERTY"));
            block->write(QString("ZXCV"));

            QCOMPARE((int) block->tell(), (int) data.size());
            block->seek(data.size() - 2);
            dataRead.clear();
            block->read(dataRead);
            QCOMPARE(Util::ByteView(dataRead), data.mid(data.size() - 2));
            QVERIFY(block->readFinished());

            block->move(-2);
            block->read(dataRead, 1);
            QCOMPARE(dataRead.toQByteArrayRef(), QByteArray("909"));
            QVERIFY(!block->readFinished());

            block->move(-100);
            dataRead.clear();
            block->read(dataRead);
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(block->readFinished());
        }
    }

    void bufferShared()
    {
        auto data = std::make_shared<Util::ByteArray>("AbCDEF1234567890");
        auto backing = Access::buffer(data);
        QVERIFY(backing.get() != nullptr);

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            bool didEnd = false;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            stream->ended.connect([&didEnd]() { didEnd = true; });
            stream->start();
            QVERIFY(didEnd);
            QCOMPARE(dataRead.toQByteArrayRef(), data->toQByteArrayRef());
            QVERIFY(stream->isEnded());

            Util::ByteArray contents("Asdf");
            stream->write(contents);
            stream->write(Util::ByteView(contents));
            stream->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                stream->write(qba);
            }
            stream->write(std::move(contents));
            stream->write("Asdfg");
            stream->write(std::string("QWERTY"));
            stream->write(QString("ZXCV"));

            QCOMPARE(dataRead.toQByteArrayRef(), QByteArray("AbCDEF1234567890"));
        }

        Util::ByteArray finalContents("AbCDEF1234567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCV");
        QCOMPARE(data->toQByteArrayRef(), finalContents.toQByteArrayRef());

        {
            auto block = backing->block();
            QVERIFY(block.get() != nullptr);

            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), data->mid(0, 10).toQByteArrayRef());
            }
            {
                QByteArray target;
                block->read(target, 10);
                QCOMPARE(target, data->mid(10, 10).toQByteArrayRef());
            }
            QVERIFY(!block->readFinished());
            block->seek(static_cast<std::size_t>(-1));
            QVERIFY(block->readFinished());

            Util::ByteArray contents("Asdf");
            block->write(contents);
            block->write(Util::ByteView(contents));
            block->write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                block->write(qba);
            }
            block->write(std::move(contents));
            block->write("Asdfg");
            block->write(std::string("QWERTY"));
            block->write(QString("ZXCV"));

            block->seek(block->tell() - 2);
            block->write("123");
            block->move(-3);
            QVERIFY(!block->readFinished());
            {
                QByteArray target;
                block->read(target, 3);
                QCOMPARE(target, QByteArray("123"));
            }
            QVERIFY(block->readFinished());

            block->seek(0);
            {
                Util::ByteArray target;
                block->read(target, 10);
                QCOMPARE(target.toQByteArrayRef(), QByteArray("AbCDEF1234"));
            }
            QVERIFY(!block->readFinished());
            QCOMPARE((int) block->tell(), 10);
            {
                Util::ByteArray target;
                block->read(target);
                QCOMPARE(target.toQByteArrayRef(), QByteArray(
                        "567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCVAsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123"));
            }
            QVERIFY(block->readFinished());
        }

        finalContents += "AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZX123";
        QCOMPARE(data->toQByteArrayRef(), finalContents.toQByteArrayRef());

        {
            auto stream = backing->stream();
            QVERIFY(stream.get() != nullptr);

            Util::ByteArray dataRead;
            bool didEnd = false;
            stream->read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            stream->ended.connect([&didEnd]() { didEnd = true; });
            stream->start();
            QVERIFY(didEnd);
            QCOMPARE(dataRead.toQByteArrayRef(), finalContents.toQByteArrayRef());
            QVERIFY(stream->isEnded());
        }
    }

    void lineByLine()
    {
        Util::ByteArray data
                ("qwerty1234\n\n112233445566778899\nAAAAAAA\nBBBBB\nC\r\nDDDDDD\n\r\r11111111111111111111111111111111111111111111\n");

        auto backing = Access::buffer(data);
        QVERIFY(backing.get() != nullptr);
        auto block = backing->block();
        QVERIFY(block.get() != nullptr);
        QVERIFY(!block->readFinished());

        auto line = block->readLine();
        QCOMPARE(line.toQByteArrayRef(), QByteArray("qwerty1234"));

        line = block->readLine();
        QCOMPARE(line.toQByteArrayRef(), QByteArray("112233445566778899"));

        line = block->readLine();
        QCOMPARE(line.toQByteArrayRef(), QByteArray("AAAAAAA"));

        line = block->readLine();
        QCOMPARE(line.toQByteArrayRef(), QByteArray("BBBBB"));

        line = block->readLine();
        QCOMPARE(line.toQByteArrayRef(), QByteArray("C"));

        line = block->readLine();
        QCOMPARE(line.toQByteArrayRef(), QByteArray("DDDDDD"));

        line = block->readLine();
        QCOMPARE(line.toQByteArrayRef(),
                 QByteArray("11111111111111111111111111111111111111111111"));

        line = block->readLine();
        QVERIFY(line.empty());

        QVERIFY(block->readFinished());
    }

    void readAll()
    {
        Util::ByteArray data("1234567890qwerty");

        auto backing = Access::buffer(data);
        QVERIFY(backing.get() != nullptr);
        auto stream = backing->stream();
        QVERIFY(stream.get() != nullptr);

        QCOMPARE(stream->readAll().toQByteArrayRef(), data.toQByteArrayRef());
    }

    void iteratorIncremental()
    {
        auto backing = Access::loopback();
        QVERIFY(backing.get() != nullptr);

        auto input = backing->stream();
        QVERIFY(input.get() != nullptr);
        auto output = backing->stream();
        QVERIFY(output.get() != nullptr);

        Generic::Stream::Iterator it(*output);
        input->write("1234");
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("1234"));
        QVERIFY(it.next(0).empty());
        QVERIFY(!it.waitForEnd(0));
        input->write("qwerty");
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("qwerty"));
        QVERIFY(it.next(0.001).empty());
        QVERIFY(!it.waitForEnd(0.001));
    }

    void iteratorBulk()
    {
        Util::ByteArray data("AbCDEF1234567890");
        auto backing = Access::buffer(data);
        QVERIFY(backing.get() != nullptr);

        auto output = backing->stream();
        QVERIFY(output.get() != nullptr);

        Generic::Stream::Iterator it(*output);
        QVERIFY(it.waitForEnd());
        QVERIFY(it.waitForEnd(0));
        QVERIFY(it.waitForEnd(0.001));
        QCOMPARE(it.all().toQByteArrayRef(), data.toQByteArrayRef());
        QVERIFY(it.all().empty());
        QVERIFY(it.all(0).empty());
        QVERIFY(it.all(0.001).empty());
    }

    void blockStream()
    {
        Util::ByteArray data("AbCDEF1234567890");
        auto backing = Access::buffer(data);
        QVERIFY(backing.get() != nullptr);

        auto block = backing->block();
        Q_ASSERT(block.get() != nullptr);

        {
            std::mutex mutex;
            std::condition_variable cv;
            Generic::Block::StreamReader stream(*block, 4);

            Util::ByteArray dataRead;
            bool didEnd = false;
            stream.read.connect([&](const Util::ByteArray &d) {
                std::lock_guard<std::mutex> lock(mutex);
                dataRead += d;
            });
            stream.ended.connect([&]() {
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    didEnd = true;
                }
                cv.notify_all();
            });
            stream.start();
            stream.readStall(true);
            std::this_thread::yield();
            stream.readStall(false);

            {
                std::unique_lock<std::mutex> lock(mutex);
                cv.wait_for(lock, std::chrono::seconds(10), [&] { return didEnd; });
                QVERIFY(didEnd);
            }
            QVERIFY(stream.isEnded());

            QCOMPARE(dataRead, data);
        }
    }

    void byteWrappers()
    {
        {
            Util::ByteArray data("AbCDEF1234567890");
            Generic::Stream::ByteArray stream(data);

            Util::ByteArray dataRead;
            bool didEnd = false;
            stream.read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            stream.ended.connect([&didEnd]() { didEnd = true; });
            stream.start();
            QVERIFY(didEnd);
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(stream.isEnded());

            Util::ByteArray contents("Asdf");
            stream.write(contents);
            stream.write(Util::ByteView(contents));
            stream.write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                stream.write(qba);
            }
            stream.write(std::move(contents));
            stream.write("Asdfg");
            stream.write(std::string("QWERTY"));
            stream.write(QString("ZXCV"));

            QCOMPARE(dataRead.toQByteArrayRef(), QByteArray("AbCDEF1234567890"));
            QCOMPARE(data.toQByteArrayRef(),
                     QByteArray("AbCDEF1234567890AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCV"));
        }
        {
            Util::ByteArray data("AbCDEF1234567890");
            Generic::Stream::ByteView stream(data);

            Util::ByteArray dataRead;
            bool didEnd = false;
            stream.read.connect([&dataRead](const Util::ByteArray &d) { dataRead += d; });
            stream.ended.connect([&didEnd]() { didEnd = true; });
            stream.start();
            QVERIFY(didEnd);
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(stream.isEnded());

            Util::ByteArray contents("Asdf");
            stream.write(contents);
            stream.write(Util::ByteView(contents));
            stream.write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                stream.write(qba);
            }
            stream.write(std::move(contents));
            stream.write("Asdfg");
            stream.write(std::string("QWERTY"));
            stream.write(QString("ZXCV"));

            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QCOMPARE(data.toQByteArrayRef(), QByteArray("AbCDEF1234567890"));
        }

        {
            Util::ByteArray data("AbCDEF1234567890");
            Generic::Block::ByteArray block(data);
            QVERIFY(!block.readFinished());

            Util::ByteArray dataRead;
            block.read(dataRead);
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(block.readFinished());

            Util::ByteArray contents("Asdf");
            block.write(contents);
            block.write(Util::ByteView(contents));
            block.write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                block.write(qba);
            }
            block.write(std::move(contents));
            block.write("Asdfg");
            block.write(std::string("QWERTY"));
            block.write(QString("ZXCV"));

            QCOMPARE((int) block.tell(), (int) data.size());

            data += "AsdfAsdfAsdfAsdfAsdfAsdfgQWERTYZXCV";

            block.seek(data.size() - 2);
            dataRead.clear();
            block.read(dataRead);
            QCOMPARE(Util::ByteView(dataRead), data.mid(data.size() - 2));
            QVERIFY(block.readFinished());

            block.move(-2);
            block.read(dataRead, 1);
            QCOMPARE(dataRead.toQByteArrayRef(), QByteArray("CVC"));
            QVERIFY(!block.readFinished());

            block.move(-100);
            dataRead.clear();
            block.read(dataRead);
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(block.readFinished());
        }
        {
            Util::ByteArray data("AbCDEF1234567890");
            Generic::Block::ByteView block(data);
            QVERIFY(!block.readFinished());

            Util::ByteArray dataRead;
            block.read(dataRead);
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(block.readFinished());

            Util::ByteArray contents("Asdf");
            block.write(contents);
            block.write(Util::ByteView(contents));
            block.write(contents.toQByteArray());
            {
                auto qba = contents.toQByteArray();
                block.write(qba);
            }
            block.write(std::move(contents));
            block.write("Asdfg");
            block.write(std::string("QWERTY"));
            block.write(QString("ZXCV"));

            QCOMPARE((int) block.tell(), (int) data.size());
            block.seek(data.size() - 2);
            dataRead.clear();
            block.read(dataRead);
            QCOMPARE(Util::ByteView(dataRead), data.mid(data.size() - 2));
            QVERIFY(block.readFinished());

            block.move(-2);
            block.read(dataRead, 1);
            QCOMPARE(dataRead.toQByteArrayRef(), QByteArray("909"));
            QVERIFY(!block.readFinished());

            block.move(-100);
            dataRead.clear();
            block.read(dataRead);
            QCOMPARE(dataRead.toQByteArrayRef(), data.toQByteArrayRef());
            QVERIFY(block.readFinished());
        }
    }
};

QTEST_APPLESS_MAIN(TestAccessSimple)

#include "access_simple.moc"
