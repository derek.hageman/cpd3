/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QEventLoop>
#include <QTimer>

#include "io/access.hxx"
#include "core/waitutils.hxx"

using namespace CPD3;
using namespace CPD3::IO;


class TestAccessQIO : public QObject {
Q_OBJECT

    static QByteArray readData(QIODevice *device, int expected)
    {
        QByteArray result;
        ElapsedTimer et;
        et.start();
        while (et.elapsed() < 10000 && result.size() < expected) {
            QEventLoop el;
            QTimer::singleShot(500, &el, &QEventLoop::quit);
            QObject::connect(device, &QIODevice::readyRead, &el, &QEventLoop::quit);
            if (device->bytesAvailable() > 0) {
                result += device->read(expected - result.size());
                continue;
            }
            el.exec();
        }
        return result;
    }

private slots:

    void loopbackStream()
    {
        auto backing = Access::loopback();
        QVERIFY(backing.get() != nullptr);
        auto qio = backing->qioStream();
        QVERIFY(qio.get() != nullptr);

        QVERIFY(qio->open(QIODevice::ReadWrite));
        QByteArray dataWritten = "Test data";
        QCOMPARE((int) qio->write(dataWritten), dataWritten.size());

        QCOMPARE(readData(qio.get(), dataWritten.size()), dataWritten);

        QCOMPARE((int) qio->write(dataWritten.mid(0, 5)), 5);
        qio->write(dataWritten.mid(5));

        QCOMPARE(readData(qio.get(), 6), dataWritten.mid(0, 6));
        QCOMPARE(readData(qio.get(), dataWritten.size() - 6), dataWritten.mid(6));

        qio->close();
        qio->close();
        QVERIFY(qio->atEnd());
    }

    void loopbackBlock()
    {
        auto backing = Access::loopback();
        QVERIFY(backing.get() != nullptr);
        auto qio = backing->qioBlock();
        QVERIFY(qio.get() != nullptr);

        QVERIFY(qio->open(QIODevice::ReadWrite));
        QByteArray dataWritten = "Test data";
        QCOMPARE((int) qio->write(dataWritten), dataWritten.size());

        QCOMPARE((int) qio->pos(), dataWritten.size());
        QVERIFY(qio->atEnd());
        qio->seek(0);
        QVERIFY(!qio->atEnd());
        QCOMPARE(qio->read(50), dataWritten);
        QVERIFY(qio->atEnd());

        qio->close();
        qio->close();
        QVERIFY(qio->atEnd());
    }
};

QTEST_MAIN(TestAccessQIO)

#include "access_qio.moc"
