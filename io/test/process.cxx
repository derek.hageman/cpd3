/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <QTest>
#include <QEventLoop>
#include <QTimer>
#include <QTemporaryFile>

#include "io/process.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::IO;


class TestProcess : public QObject {
Q_OBJECT

    struct MonitorContext {
        std::mutex mutex;
        std::condition_variable cv;
        bool exited;
        int exitCode;
        bool exitNormal;

        explicit MonitorContext(Process::Instance *ptr) : exited(false),
                                                          exitCode(-1),
                                                          exitNormal(false)
        {
            ptr->exited.connect([this](int ec, bool en) {
                std::lock_guard<std::mutex> lock(mutex);
                exited = true;
                exitCode = ec;
                exitNormal = en;
                cv.notify_all();
            });
        }

        explicit MonitorContext(const std::shared_ptr<Process::Instance> &ptr) : MonitorContext(
                ptr.get())
        { }

        bool wait()
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(30), [&]() { return exited; });
            return exited;
        }
    };

    struct StreamMonitor {
        MonitorContext &context;
        Util::ByteArray data;
        bool ended;

        StreamMonitor(MonitorContext &ctx, Process::Stream *ptr) : context(ctx), ended(false)
        {
            Q_ASSERT(ptr != nullptr);

            ptr->read.connect([this](const Util::ByteArray &add) {
                std::lock_guard<std::mutex> lock(context.mutex);
                data += add;
                context.cv.notify_all();
            });
            ptr->ended.connect([this] {
                std::lock_guard<std::mutex> lock(context.mutex);
                Q_ASSERT(!ended);
                ended = true;
                context.cv.notify_all();
            });

            ptr->start();
        }

        StreamMonitor(MonitorContext &ctx, const std::unique_ptr<Process::Stream> &ptr)
                : StreamMonitor(ctx, ptr.get())
        { }

        StreamMonitor(MonitorContext &ctx, const std::unique_ptr<IO::Generic::Stream> &ptr)
                : StreamMonitor(ctx, dynamic_cast<Process::Stream *>(ptr.get()))
        { }

        bool wait()
        {
            std::unique_lock<std::mutex> lock(context.mutex);
            context.cv.wait_for(lock, std::chrono::seconds(30), [&]() { return ended; });
            return ended;
        }
    };

private slots:

    void initTestCase()
    {
        CPD3::Logging::suppressForTesting();
    }

    void capture()
    {
        auto cmd = Process::Spawn::shell("echo TESTKEY").capture().create();
        QVERIFY(cmd.get() != nullptr);

        MonitorContext ctx(cmd);

        auto streamInput = cmd->inputStream();
        QVERIFY(streamInput.get() != nullptr);
        StreamMonitor monitorInput(ctx, streamInput);

        auto streamOutput = cmd->outputStream();
        QVERIFY(streamOutput.get() != nullptr);
        StreamMonitor monitorOutput(ctx, streamOutput);

        auto streamError = cmd->errorStream();
        QVERIFY(streamError.get() != nullptr);
        StreamMonitor monitorError(ctx, streamError);

        auto streamCombined = cmd->combinedOutputStream();
        QVERIFY(streamCombined.get() != nullptr);
        StreamMonitor monitorCombined(ctx, streamCombined);

        auto streamBasic = cmd->stream();
        QVERIFY(streamBasic.get() != nullptr);
        StreamMonitor monitorBasic(ctx, streamBasic);

        QVERIFY(cmd->start());
        QVERIFY(cmd->wait(30.0));
        QVERIFY(ctx.wait());
        QVERIFY(ctx.exitNormal);
        QCOMPARE(ctx.exitCode, 0);
        QVERIFY(!cmd->isRunning());

        QVERIFY(monitorInput.wait());
        QVERIFY(monitorInput.data.empty());

        QVERIFY(monitorOutput.wait());
        QVERIFY(monitorOutput.data.toQByteArrayRef().contains("TESTKEY"));

        QVERIFY(monitorError.wait());
        QVERIFY(monitorError.data.empty());

        QVERIFY(monitorCombined.wait());
        QVERIFY(monitorCombined.data.toQByteArrayRef().contains("TESTKEY"));

        QVERIFY(monitorBasic.wait());
        QVERIFY(monitorBasic.data.toQByteArrayRef().contains("TESTKEY"));
    }

    void discard()
    {
        auto cmd = Process::Spawn::shell("echo TESTKEY").discard().create();
        QVERIFY(cmd.get() != nullptr);

        MonitorContext ctx(cmd);

        auto streamInput = cmd->inputStream();
        QVERIFY(streamInput.get() != nullptr);
        StreamMonitor monitorInput(ctx, streamInput);

        auto streamOutput = cmd->outputStream();
        QVERIFY(streamOutput.get() != nullptr);
        StreamMonitor monitorOutput(ctx, streamOutput);

        auto streamError = cmd->errorStream();
        QVERIFY(streamError.get() != nullptr);
        StreamMonitor monitorError(ctx, streamError);

        auto streamCombined = cmd->combinedOutputStream();
        QVERIFY(streamCombined.get() != nullptr);
        StreamMonitor monitorCombined(ctx, streamCombined);

        auto streamBasic = cmd->stream();
        QVERIFY(streamBasic.get() != nullptr);
        StreamMonitor monitorBasic(ctx, streamBasic);

        QVERIFY(cmd->start());
        QVERIFY(cmd->wait(30.0));
        QVERIFY(ctx.wait());
        QVERIFY(ctx.exitNormal);
        QCOMPARE(ctx.exitCode, 0);
        QVERIFY(!cmd->isRunning());

        QVERIFY(monitorInput.wait());
        QVERIFY(monitorInput.data.empty());

        QVERIFY(monitorOutput.wait());
        QVERIFY(monitorOutput.data.empty());

        QVERIFY(monitorError.wait());
        QVERIFY(monitorError.data.empty());

        QVERIFY(monitorCombined.wait());
        QVERIFY(monitorCombined.data.empty());

        QVERIFY(monitorBasic.wait());
        QVERIFY(monitorBasic.data.empty());
    }

#if 0

    void forward()
    {
        auto cmd = Process::Spawn::shell("echo Test success").forward().create();
        QVERIFY(cmd.get() != nullptr);

        MonitorContext ctx(cmd);

        QVERIFY(cmd->start());
        QVERIFY(cmd->wait(30.0));
        QVERIFY(ctx.wait());
        QVERIFY(ctx.exitNormal);
        QCOMPARE(ctx.exitCode, 0);
        QVERIFY(!cmd->isRunning());
    }

#endif

#ifndef Q_OS_WIN32

    void terminate()
    {
        auto cmd = Process::Spawn::shell("sleep 30").discard().create();
        QVERIFY(cmd.get() != nullptr);
        QVERIFY(cmd->start());
        QVERIFY(!cmd->wait(0.25));
        QVERIFY(cmd->isRunning());
        cmd->terminate();
        QVERIFY(cmd->wait(30.0));
    }

#endif

    void kill()
    {
        auto cmd = Process::Spawn::shell("sleep 30").discard().create();
        QVERIFY(cmd.get() != nullptr);
        QVERIFY(cmd->start());
        QVERIFY(!cmd->wait(0.25));
        QVERIFY(cmd->isRunning());
        cmd->kill();
        QVERIFY(cmd->wait(30.0));
    }

    void destroy()
    {
        auto cmd = Process::Spawn::shell("sleep 30").discard().create();
        QVERIFY(cmd.get() != nullptr);
        QVERIFY(cmd->start());
        QVERIFY(!cmd->wait(0.25));
        QVERIFY(cmd->isRunning());
        cmd.reset();
    }

    void detach()
    {
        auto cmd = Process::Spawn::shell("sleep 1").discard().create();
        QVERIFY(cmd.get() != nullptr);
        QVERIFY(cmd->detach());
    }

#ifdef Q_OS_UNIX

    void inputWrite()
    {
        auto cmd = Process::Spawn("cat", {"-"}).create();
        QVERIFY(cmd.get() != nullptr);

        MonitorContext ctx(cmd);

        auto streamInput = cmd->inputStream();
        QVERIFY(streamInput.get() != nullptr);
        StreamMonitor monitorInput(ctx, streamInput);

        auto streamOutput = cmd->outputStream();
        QVERIFY(streamOutput.get() != nullptr);
        StreamMonitor monitorOutput(ctx, streamOutput);

        QVERIFY(cmd->start());
        QVERIFY(cmd->isRunning());

        streamInput->write("Forwarded Data\n");
        streamInput->close();

        QVERIFY(cmd->wait(30.0));
        QVERIFY(ctx.wait());
        QVERIFY(ctx.exitNormal);
        QCOMPARE(ctx.exitCode, 0);
        QVERIFY(!cmd->isRunning());

        QVERIFY(monitorInput.wait());
        QVERIFY(monitorInput.data.empty());

        QVERIFY(monitorOutput.wait());
        QCOMPARE(monitorOutput.data.toQByteArrayRef(), QByteArray("Forwarded Data\n"));
    }

    void loopback()
    {
        auto cmd = Process::Spawn("cat", {"-"}).capture().create();
        QVERIFY(cmd.get() != nullptr);

        MonitorContext ctx(cmd);

        QVERIFY(cmd->start());

        auto streamCombined = cmd->combinedOutputStream(true);
        QVERIFY(streamCombined.get() != nullptr);
        StreamMonitor monitorCombined(ctx, streamCombined);

        auto streamBasic = cmd->stream();
        QVERIFY(streamBasic.get() != nullptr);
        StreamMonitor monitorBasic(ctx, streamBasic);

        QByteArray data = "Send data\n";

        streamBasic->write(data);
        {
            std::unique_lock<std::mutex> lock(ctx.mutex);
            ctx.cv.wait(lock, [&] { return (int) monitorBasic.data.size() >= data.size(); });
            QCOMPARE(monitorBasic.data.toQByteArrayRef(), data);
            monitorBasic.data.clear();
        }
        {
            std::unique_lock<std::mutex> lock(ctx.mutex);
            ctx.cv.wait(lock, [&] { return (int) monitorCombined.data.size() >= data.size(); });
            QCOMPARE(monitorCombined.data.toQByteArrayRef(), data);
            monitorCombined.data.clear();
        }

        streamCombined->write(data);
        {
            std::unique_lock<std::mutex> lock(ctx.mutex);
            ctx.cv.wait(lock, [&] { return (int) monitorBasic.data.size() >= data.size(); });
            QCOMPARE(monitorBasic.data.toQByteArrayRef(), data);
            monitorBasic.data.clear();
        }
        {
            std::unique_lock<std::mutex> lock(ctx.mutex);
            ctx.cv.wait(lock, [&] { return (int) monitorCombined.data.size() >= data.size(); });
            QCOMPARE(monitorCombined.data.toQByteArrayRef(), data);
            monitorCombined.data.clear();
        }

        dynamic_cast<Process::Stream *>(streamBasic.get())->close();

        QVERIFY(cmd->wait(30.0));
        QVERIFY(ctx.wait());
        QVERIFY(ctx.exitNormal);
        QCOMPARE(ctx.exitCode, 0);
        QVERIFY(!cmd->isRunning());

        QVERIFY(monitorCombined.wait());
        QVERIFY(monitorCombined.data.empty());

        QVERIFY(monitorBasic.wait());
        QVERIFY(monitorBasic.data.empty());
    }

    void pipeline()
    {
        auto pipe = Process::Spawn::pipeline({{"cat", {"-"}},
                                              {"sed", {"-e", "s/a/A/g"}},
                                              {"cat", {"-"}},});

        MonitorContext ctx(pipe.output());

        auto streamInput = pipe.input()->inputStream();
        QVERIFY(streamInput.get() != nullptr);
        StreamMonitor monitorInput(ctx, streamInput);

        auto streamOutput = pipe.output()->outputStream();
        QVERIFY(streamOutput.get() != nullptr);
        StreamMonitor monitorOutput(ctx, streamOutput);

        QVERIFY(pipe.start());

        streamInput->write("Forwarded Data\n");
        streamInput->close();

        QVERIFY(pipe.wait(30.0));
        QVERIFY(ctx.wait());
        QVERIFY(ctx.exitNormal);
        QCOMPARE(ctx.exitCode, 0);

        QVERIFY(monitorInput.wait());
        QVERIFY(monitorInput.data.empty());

        QVERIFY(monitorOutput.wait());
        QCOMPARE(monitorOutput.data.toQByteArrayRef(), QByteArray("ForwArded DAtA\n"));
    }

    void fileEnds()
    {
        QTemporaryFile inputFile;
        QVERIFY(inputFile.open());
        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());

        QByteArray data("Test data");
        QCOMPARE(inputFile.write(data), data.size());
        inputFile.close();

        Process::Spawn spawn("cat", {"-"});
        spawn.inputStream = Process::Spawn::StreamMode::File;
        spawn.inputFile = inputFile.fileName().toStdString();
        spawn.outputStream = Process::Spawn::StreamMode::File;
        spawn.outputFile = outputFile.fileName().toStdString();

        auto cmd = spawn.create();
        QVERIFY(cmd.get() != nullptr);

        MonitorContext ctx(cmd);

        QVERIFY(cmd->start());
        QVERIFY(cmd->wait(30.0));
        QVERIFY(ctx.wait());
        QVERIFY(ctx.exitNormal);
        QCOMPARE(ctx.exitCode, 0);
        QVERIFY(!cmd->isRunning());

        QCOMPARE(outputFile.readAll(), data);
    }

#endif
};

QTEST_MAIN(TestProcess)

#include "process.moc"
