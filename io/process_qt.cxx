/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <unordered_set>
#include <QProcess>
#include <QThread>
#include <QIODevice>
#include <QMetaObject>
#include <QTimer>
#include <QLoggingCategory>
#include <QProcessEnvironment>

#include "process.hxx"
#include "core/threading.hxx"
#include "core/qtcompat.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_io_process)

namespace CPD3 {
namespace IO {
namespace Process {
namespace {

/*
 * Need a single shared thread so we can pipe processes to each other.
 */
class GlobalState final {
public:
    struct Context {
        QProcess *process;
        QObject *receiver;

        Context() : process(nullptr), receiver(nullptr)
        { }
    };

private:
    std::mutex mutex;
    std::condition_variable notify;

    class Thread final : public QThread {
        GlobalState &state;
        std::unordered_map<QProcess *, std::unique_ptr<QProcess>> processes;
        std::unordered_map<QObject *, std::unique_ptr<QObject>> receivers;
    public:
        std::unique_ptr<QObject> sharedContext;

        explicit Thread(GlobalState &state) : state(state)
        { setObjectName("QProcessSharedThread"); }

        virtual ~Thread() = default;

        Context attach()
        {
            Q_ASSERT(sharedContext->thread() == QThread::currentThread());

            std::unique_ptr<QObject> receiver(new QObject);
            std::unique_ptr<QProcess> process(new QProcess);

            Context result;
            result.process = process.get();
            result.receiver = receiver.get();

            processes.emplace(result.process, std::move(process));
            receivers.emplace(result.receiver, std::move(receiver));

            return result;
        }

        void releaseReceiver(Context &context)
        {
            Q_ASSERT(sharedContext->thread() == QThread::currentThread());
            Q_ASSERT(context.receiver != nullptr);

            receivers.erase(context.receiver);
            context.receiver = nullptr;
        }

        void releaseProcess(Context &context)
        {
            Q_ASSERT(sharedContext->thread() == QThread::currentThread());
            Q_ASSERT(context.process != nullptr);

            processes.erase(context.process);
            context.process = nullptr;
        }

    protected:
        void run() override
        {
            {
                std::lock_guard<std::mutex> lock(state.mutex);
                sharedContext.reset(new QObject);
            }
            state.notify.notify_all();

            qCDebug(log_io_process) << "Started shared process handling thread";
            QThread::exec();

            processes.clear();
            receivers.clear();
            sharedContext.reset();
        }
    };

    std::unique_ptr<Thread> thread;
public:
    GlobalState() = default;

    ~GlobalState()
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (!thread)
                return;
        }

        if (thread->sharedContext->thread() == QThread::currentThread()) {
            /*
             * Note the context is the thread, which will be owned by another thread, so
             * we rely on that parent thread's event loop to do the delete.
             */
            Q_ASSERT(thread->thread() != QThread::currentThread());
            auto thr = std::make_shared<std::unique_ptr<QThread>>(std::move(thread));
            Threading::runQueuedFunctor(thr->get(), [thr]() {
                (*thr)->wait();
                thr->reset();
            });
        } else {
            thread->quit();
            thread->wait();
        }
    }

    Context attach()
    {
        {
            std::unique_lock<std::mutex> lock(mutex);
            if (!thread) {
                thread.reset(new Thread(*this));
                thread->start();
            }
            notify.wait(lock, [this] { return thread->sharedContext.get() != nullptr; });
        }

        Q_ASSERT(thread->sharedContext.get() != nullptr);
        if (thread->sharedContext->thread() == QThread::currentThread()) {
            return thread->attach();
        } else {
            Context result;
            Threading::runBlockingFunctor(thread->sharedContext.get(), [this, &result] {
                result = thread->attach();
            });
            return result;
        }
    }

    void detachReceiver(Context &context)
    {
        if (context.receiver == nullptr)
            return;
        Q_ASSERT(thread.get() != nullptr);
        Q_ASSERT(thread->sharedContext.get() != nullptr);
        Q_ASSERT(thread->sharedContext->thread() == QThread::currentThread());

        return thread->releaseReceiver(context);
    }

    void detachProcess(Context &context)
    {
        if (context.receiver == nullptr)
            return;
        Q_ASSERT(thread.get() != nullptr);
        Q_ASSERT(thread->sharedContext.get() != nullptr);
        Q_ASSERT(thread->sharedContext->thread() == QThread::currentThread());

        return thread->releaseProcess(context);
    }

    void runDestroy(const std::function<void()> &destroy)
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (!thread)
                return;
        }
        if (thread->sharedContext->thread() == QThread::currentThread()) {
            destroy();
        } else {
            Threading::runBlockingFunctor(thread->sharedContext.get(), destroy);
        }
    }
};

Q_GLOBAL_STATIC(GlobalState, shared)

static constexpr std::size_t readBlockSize = 65536;
static constexpr std::size_t stallThreshold = 4194304;

class QtProcessInstance final
        : public Instance, public std::enable_shared_from_this<QtProcessInstance> {
    Spawn spawn;
    bool pipeAttached;

    GlobalState::Context context;

    Util::ByteArray readBuffer;
    Util::ByteArray writeBuffer;
    enum State {
        Initialize, Running, Detached, Completed,
    } state;
    bool inputChannelClosed;
    bool outputChannelClosed;
    bool errorChannelClosed;

    bool sentWriteStall;
    bool readStallAny;
    bool readStallOutput;
    bool readStallError;

    struct LogHandler {
        std::string name;
        std::unique_ptr<QLoggingCategory> storage;

        const QLoggingCategory &operator()() const
        {
            if (storage)
                return *storage;
            return log_io_process();
        }
    };

    LogHandler log;

    Threading::Signal<bool> writeStall;
    Threading::Signal<const Util::ByteArray &> readAny;
    Threading::Signal<const Util::ByteArray &> readOutput;
    Threading::Signal<const Util::ByteArray &> readError;
    Threading::Signal<> endOfWrite;
    Threading::Signal<> endOfAnyRead;
    Threading::Signal<> endOfOutput;
    Threading::Signal<> endOfError;

    std::unique_ptr<QTimer> readPoll;
    std::vector<QMetaObject::Connection> connections;

    void inputEnded()
    {
        if (inputChannelClosed)
            return;
        qCDebug(log) << "Process input read ending";
        inputChannelClosed = true;
        if (sentWriteStall) {
            sentWriteStall = false;
            writeStall(false);
        }
        endOfWrite();
    }

    void outputEnded()
    {
        if (outputChannelClosed)
            return;
        qCDebug(log) << "Process output write ending";
        outputChannelClosed = true;
        endOfOutput();
        if (errorChannelClosed)
            endOfAnyRead();
    }

    void errorEnded()
    {
        if (errorChannelClosed)
            return;
        qCDebug(log) << "Process error write ending";
        errorChannelClosed = true;
        endOfError();
        if (outputChannelClosed)
            endOfAnyRead();
    }

    void outputDataAvailable(const Util::ByteArray &data)
    {
        readOutput(data);
        readAny(data);
    }

    void errorDataAvailable(const Util::ByteArray &data)
    {
        readError(data);
        readAny(data);
    }

    void destroy()
    {
        shared()->detachReceiver(context);
        readPoll.reset();
        for (auto &c : connections) {
            QObject::disconnect(c);
        }


        if (state == State::Running) {
            context.process->terminate();
            if (!context.process->waitForFinished(5000)) {
                context.process->kill();
                context.process->waitForFinished(-1);
            }

            state = State::Completed;
            exited(-1, false);
        }

        shared()->detachProcess(context);

        inputEnded();
        outputEnded();
        errorEnded();
    }

    bool setupCommand() const
    {
        if (spawn.command.empty()) {
            qCDebug(log_io_process) << "No command specified for process";
            return false;
        }
        context.process->setProgram(QString::fromStdString(spawn.command));
        return true;
    }

    bool setupArguments() const
    {
        context.process->setArguments(Util::to_qstringlist(spawn.arguments));
        return true;
    }

    bool setupEnvironment() const
    {
        QProcessEnvironment env;

        switch (spawn.environment) {
        case Spawn::Environment::Inherit:
            env = QProcessEnvironment::systemEnvironment();
            break;
        case Spawn::Environment::Empty:
            break;
        }

        for (const auto &add : spawn.environmentSet) {
            env.insert(QString::fromStdString(add.first), QString::fromStdString(add.second));
        }
        for (const auto &rem : spawn.environmentClear) {
            env.remove(QString::fromStdString(rem));
        }

        context.process->setProcessEnvironment(env);
        return true;
    }

    bool startProcess()
    {
        Q_ASSERT(context.process->thread() == QThread::currentThread());
        Q_ASSERT(state == State::Initialize);

        QIODevice::OpenMode mode = QIODevice::Unbuffered;

        if (spawn.outputStream == Spawn::StreamMode::Forward) {
            if (spawn.errorStream == Spawn::StreamMode::Forward) {
                context.process->setProcessChannelMode(QProcess::ForwardedChannels);
            } else {
                context.process->setProcessChannelMode(QProcess::ForwardedOutputChannel);
            }
        } else {
            if (spawn.errorStream == Spawn::StreamMode::Forward) {
                context.process->setProcessChannelMode(QProcess::ForwardedErrorChannel);
            } else {
                context.process->setProcessChannelMode(QProcess::SeparateChannels);
            }
        }

        readPoll.reset(new QTimer);
        readPoll->setSingleShot(false);
        readPoll->setInterval(500);

        switch (spawn.inputStream) {
        case Spawn::StreamMode::Discard:
            context.process->setStandardInputFile(QProcess::nullDevice());
            mode |= QIODevice::WriteOnly;
            inputEnded();
            break;
        case Spawn::StreamMode::Forward:
            inputEnded();
            context.process->setInputChannelMode(QProcess::ForwardedInputChannel);
            break;
        case Spawn::StreamMode::Capture:
            mode |= QIODevice::WriteOnly;
            connections.emplace_back(
                    QObject::connect(context.process, &QIODevice::bytesWritten, context.receiver,
                                     std::bind(&QtProcessInstance::serviceInputWrite, this),
                                     Qt::QueuedConnection));
            context.process->setInputChannelMode(QProcess::ManagedInputChannel);
            break;
        case Spawn::StreamMode::File:
        case Spawn::StreamMode::FileAppend:
            mode |= QIODevice::WriteOnly;
            context.process->setStandardInputFile(QString::fromStdString(spawn.inputFile));
            inputEnded();
            break;
        }

        if (pipeAttached) {
            if (spawn.errorStream == Spawn::StreamMode::Forward) {
                context.process->setProcessChannelMode(QProcess::ForwardedErrorChannel);
            } else {
                context.process->setProcessChannelMode(QProcess::SeparateChannels);
            }
            outputEnded();
        } else {
            switch (spawn.outputStream) {
            case Spawn::StreamMode::Discard:
                context.process->setStandardOutputFile(QProcess::nullDevice());
                mode |= QIODevice::ReadOnly;
                outputEnded();
                break;
            case Spawn::StreamMode::Forward:
                outputEnded();
                break;
            case Spawn::StreamMode::Capture:
                mode |= QIODevice::ReadOnly;
                connections.emplace_back(
                        QObject::connect(context.process, &QProcess::readyReadStandardOutput,
                                         context.receiver,
                                         std::bind(&QtProcessInstance::serviceOutputRead, this),
                                         Qt::QueuedConnection));
                connections.emplace_back(
                        QObject::connect(context.process, &QIODevice::readChannelFinished,
                                         context.receiver,
                                         std::bind(&QtProcessInstance::serviceOutputRead, this),
                                         Qt::QueuedConnection));
                connections.emplace_back(
                        QObject::connect(readPoll.get(), &QTimer::timeout, context.receiver,
                                         std::bind(&QtProcessInstance::serviceOutputRead, this),
                                         Qt::DirectConnection));
                break;
            case Spawn::StreamMode::File:
                mode |= QIODevice::ReadOnly;
                context.process->setStandardOutputFile(QString::fromStdString(spawn.outputFile));
                outputEnded();
                break;
            case Spawn::StreamMode::FileAppend:
                mode |= QIODevice::ReadOnly;
                context.process
                       ->setStandardOutputFile(QString::fromStdString(spawn.outputFile),
                                               QIODevice::Append);
                outputEnded();
                break;
            }
        }

        switch (spawn.errorStream) {
        case Spawn::StreamMode::Discard:
            context.process->setStandardErrorFile(QProcess::nullDevice());
            mode |= QIODevice::ReadOnly;
            errorEnded();
            break;
        case Spawn::StreamMode::Forward:
            errorEnded();
            break;
        case Spawn::StreamMode::Capture:
            mode |= QIODevice::ReadOnly;
            connections.emplace_back(
                    QObject::connect(context.process, &QProcess::readyReadStandardError,
                                     context.receiver,
                                     std::bind(&QtProcessInstance::serviceOutputRead, this),
                                     Qt::QueuedConnection));
            connections.emplace_back(
                    QObject::connect(context.process, &QIODevice::readChannelFinished,
                                     context.receiver,
                                     std::bind(&QtProcessInstance::serviceErrorRead, this),
                                     Qt::QueuedConnection));
            connections.emplace_back(
                    QObject::connect(readPoll.get(), &QTimer::timeout, context.receiver,
                                     std::bind(&QtProcessInstance::serviceErrorRead, this),
                                     Qt::DirectConnection));
            break;
        case Spawn::StreamMode::File:
            mode |= QIODevice::ReadOnly;
            context.process->setStandardErrorFile(QString::fromStdString(spawn.errorFile));
            errorEnded();
            break;
        case Spawn::StreamMode::FileAppend:
            mode |= QIODevice::ReadOnly;
            context.process
                   ->setStandardErrorFile(QString::fromStdString(spawn.errorFile),
                                          QIODevice::Append);
            errorEnded();
            break;
        }

        if (!setupCommand())
            return false;
        if (!setupArguments())
            return false;
        if (!setupEnvironment())
            return false;

        connections.emplace_back(QObject::connect(context.process, static_cast<void (QProcess::*)(
                                                          int,
                                                          QProcess::ExitStatus)>(&QProcess::finished), context.receiver,
                                                  std::bind(&QtProcessInstance::checkProcessState,
                                                            this), Qt::QueuedConnection));
        connections.emplace_back(
                QObject::connect(readPoll.get(), &QTimer::timeout, context.receiver,
                                 std::bind(&QtProcessInstance::checkProcessState, this),
                                 Qt::DirectConnection));

        context.process->start(mode);
        if (!context.process->waitForStarted()) {
            qCDebug(log_io_process) << "Failed to start process for" << spawn.command << ":"
                                    << context.process->errorString();
            return false;
        }
        auto pid = context.process->processId();
        state = State::Running;

        log.name = "cpd3.io.process." + std::to_string(static_cast<std::uint_fast64_t>(pid));
        log.storage.reset(new QLoggingCategory(log.name.c_str(), QtWarningMsg));
        qCDebug(log) << "Started process for" << spawn.command << "with pid" << pid;

        readPoll->start();
        processInputWrite();
        serviceOutputRead();
        serviceErrorRead();
        checkProcessState();
        return true;
    }

    bool startDetached()
    {
        Q_ASSERT(context.process->thread() == QThread::currentThread());
        Q_ASSERT(state == State::Initialize);

        if (pipeAttached) {
            qCDebug(log_io_process) << "Detached processes do not support pipes";
            return false;
        }

#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
        switch (spawn.inputStream) {
        case Spawn::StreamMode::Discard:
            context.process->setStandardInputFile(QProcess::nullDevice());
            break;
        case Spawn::StreamMode::Forward:
            break;
        case Spawn::StreamMode::Capture:
            qCDebug(log_io_process) << "Detached processes do not support input capture";
            return false;
        case Spawn::StreamMode::File:
        case Spawn::StreamMode::FileAppend:
            context.process->setStandardInputFile(QString::fromStdString(spawn.inputFile));
            break;
        }

        switch (spawn.outputStream) {
        case Spawn::StreamMode::Discard:
            context.process->setStandardOutputFile(QProcess::nullDevice());
            break;
        case Spawn::StreamMode::Forward:
            break;
        case Spawn::StreamMode::Capture:
            qCDebug(log_io_process) << "Detached processes do not support output capture";
            return false;
        case Spawn::StreamMode::File:
            context.process->setStandardOutputFile(QString::fromStdString(spawn.outputFile));
            break;
        case Spawn::StreamMode::FileAppend:
            context.process->setStandardOutputFile(QString::fromStdString(spawn.outputFile),
                                           QIODevice::Append);
            break;
        }

        switch (spawn.errorStream) {
        case Spawn::StreamMode::Discard:
            context.process->setStandardErrorFile(QProcess::nullDevice());
            break;
        case Spawn::StreamMode::Forward:
            break;
        case Spawn::StreamMode::Capture:
            qCDebug(log_io_process) << "Detached processes do not support error capture";
            return false;
        case Spawn::StreamMode::File:
            context.process->setStandardErrorFile(QString::fromStdString(spawn.errorFile));
            break;
        case Spawn::StreamMode::FileAppend:
            context.process->setStandardErrorFile(QString::fromStdString(spawn.errorFile),
                                          QIODevice::Append);
            break;
        }

        if (!setupCommand())
            return false;
        if (!setupArguments())
            return false;
        if (!setupEnvironment())
            return false;

        qint64 pid = 0;
        if (!context.process->startDetached(&pid)) {
            qCDebug(log_io_process) << "Error starting detached process for" << spawn.command;
            return false;
        }

        state = State::Detached;

        log.name = "cpd3.io.process." + std::to_string(static_cast<std::uint_fast64_t>(pid));
        log.storage.reset(new QLoggingCategory(log.name.c_str(), QtWarningMsg));
        qCDebug(log) << "Started detached process for" << spawn.command << "with pid" << pid;
        return true;
#else
        switch (spawn.inputStream) {
        case Spawn::StreamMode::Discard:
            qCDebug(log_io_process)
                << "Input stream discard converted to forward for detached processes";
            break;
        case Spawn::StreamMode::Forward:
            break;
        case Spawn::StreamMode::Capture:
        case Spawn::StreamMode::File:
        case Spawn::StreamMode::FileAppend:
            qCDebug(log_io_process) << "Detached processes only support input forwarding";
            return false;
        }
        switch (spawn.outputStream) {
        case Spawn::StreamMode::Discard:
            qCDebug(log_io_process)
                << "Output stream discard converted to forward for detached processes";
            break;
        case Spawn::StreamMode::Forward:
            break;
        case Spawn::StreamMode::Capture:
        case Spawn::StreamMode::File:
        case Spawn::StreamMode::FileAppend:
            qCDebug(log_io_process) << "Detached processes only support output forwarding";
            return false;
        }
        switch (spawn.errorStream) {
        case Spawn::StreamMode::Discard:
            qCDebug(log_io_process)
                << "Error stream discard converted to forward for detached processes";
            break;
        case Spawn::StreamMode::Forward:
            break;
        case Spawn::StreamMode::Capture:
        case Spawn::StreamMode::File:
        case Spawn::StreamMode::FileAppend:
            qCDebug(log_io_process) << "Detached processes only support error forwarding";
            return false;
        }

        if (spawn.environment != Spawn::Environment::Inherit) {
            qCDebug(log_io_process) << "Detached processes must inherit their parent environment";
            return false;
        }
        if (!spawn.environmentSet.empty() || !spawn.environmentClear.empty()) {
            qCDebug(log_io_process) << "Detached processes cannot manipulate their environment";
            return false;
        }

        qint64 pid = 0;
        if (!QProcess::startDetached(QString::fromStdString(spawn.command),
                                     Util::to_qstringlist(spawn.arguments), {}, &pid)) {
            qCDebug(log_io_process) << "Error starting detached process for" << spawn.command;
            return false;
        }

        state = State::Detached;

        qCDebug(log_io_process) << "Started detached process for" << spawn.command << "with pid"
                                << pid;
        return true;
#endif
    }

    void serviceInputWrite()
    {
        if (inputChannelClosed) {
            writeBuffer.clear();

            if (sentWriteStall) {
                sentWriteStall = false;
                writeStall(false);
            }
            return;
        }

        if (writeBuffer.empty())
            return;
        auto n = context.process->write(writeBuffer.data<const char *>(), writeBuffer.size());
        if (n < 0) {
            writeBuffer.clear();
        } else if (n > 0) {
            writeBuffer.pop_front(n);
        }

        if (writeBuffer.size() > stallThreshold) {
            if (!sentWriteStall) {
                sentWriteStall = true;
                writeStall(true);
            }
        } else {
            if (sentWriteStall) {
                sentWriteStall = false;
                writeStall(false);
            }
        }
    }

    void processInputWrite()
    {
        Q_ASSERT(context.process->thread() == QThread::currentThread());

        if (inputChannelClosed) {
            writeBuffer.clear();
            return;
        }

        if (!context.process->bytesToWrite()) {
            Threading::runQueuedFunctor(context.receiver,
                                        std::bind(&QtProcessInstance::serviceInputWrite, this));
        }
    }

    void serviceOutputRead()
    {
        if (outputChannelClosed)
            return;
        if (readStallAny || readStallOutput)
            return;

        context.process->setReadChannel(QProcess::StandardOutput);

        readBuffer.resize(readBlockSize);
        auto n = context.process->read(readBuffer.data<char *>(), readBuffer.size());
        if (n < 0) {
            outputEnded();
            return;
        } else if (n > 0) {
            Q_ASSERT(static_cast<std::size_t>(n) <= readBuffer.size());
            readBuffer.resize(n);
            outputDataAvailable(readBuffer);

            Threading::runQueuedFunctor(context.receiver,
                                        std::bind(&QtProcessInstance::serviceOutputRead, this));
            return;
        }

        if (state == State::Running)
            return;
        outputEnded();
    }

    void serviceErrorRead()
    {
        if (errorChannelClosed)
            return;
        if (readStallAny || readStallError)
            return;

        context.process->setReadChannel(QProcess::StandardError);

        readBuffer.resize(readBlockSize);
        auto n = context.process->read(readBuffer.data<char *>(), readBuffer.size());
        if (n < 0) {
            errorEnded();
            return;
        } else if (n > 0) {
            Q_ASSERT(static_cast<std::size_t>(n) <= readBuffer.size());
            readBuffer.resize(n);
            errorDataAvailable(readBuffer);

            Threading::runQueuedFunctor(context.receiver,
                                        std::bind(&QtProcessInstance::serviceErrorRead, this));
            return;
        }

        if (state == State::Running)
            return;
        errorEnded();
    }

    void checkProcessState()
    {
        if (state != State::Running)
            return;

        switch (context.process->state()) {
        case QProcess::NotRunning:
            break;
        case QProcess::Starting:
        case QProcess::Running:
            return;
        }

        context.process->waitForFinished(-1);

        qCDebug(log) << "Process completed";

        state = State::Completed;
        exited(context.process->exitCode(), context.process->exitStatus() == QProcess::NormalExit);

        serviceOutputRead();
        serviceErrorRead();
        inputEnded();
    }

    void setReadStallOutput(bool enable)
    {
        Q_ASSERT(context.process->thread() == QThread::currentThread());
        if (enable == readStallOutput)
            return;
        readStallOutput = enable;

        if (!readStallOutput && !outputChannelClosed) {
            Threading::runQueuedFunctor(context.receiver,
                                        std::bind(&QtProcessInstance::serviceOutputRead, this));
        }
    }

    void setReadStallError(bool enable)
    {
        Q_ASSERT(context.process->thread() == QThread::currentThread());
        if (enable == readStallError)
            return;
        readStallError = enable;

        if (!readStallError && !errorChannelClosed) {
            Threading::runQueuedFunctor(context.receiver,
                                        std::bind(&QtProcessInstance::serviceErrorRead, this));
        }
    }

    void setReadStallAny(bool enable)
    {
        Q_ASSERT(context.process->thread() == QThread::currentThread());
        if (enable == readStallAny)
            return;
        readStallAny = enable;

        if (!readStallAny && !outputChannelClosed) {
            Threading::runQueuedFunctor(context.receiver,
                                        std::bind(&QtProcessInstance::serviceOutputRead, this));
        }
        if (!readStallAny && !errorChannelClosed) {
            Threading::runQueuedFunctor(context.receiver,
                                        std::bind(&QtProcessInstance::serviceErrorRead, this));
        }
    }

    void closeInput()
    {
        Q_ASSERT(context.process->thread() == QThread::currentThread());
        if (inputChannelClosed)
            return;
        if (state != State::Running)
            return;

        while (!writeBuffer.empty()) {
            auto n = context.process->write(writeBuffer.data<const char *>(), writeBuffer.size());
            if (n < 0) {
                break;
            }
            Q_ASSERT(static_cast<std::size_t>(n) <= writeBuffer.size());
            writeBuffer.pop_front(n);
        }

        context.process->closeWriteChannel();
        inputEnded();
    }

    void closeOutput()
    {
        if (outputChannelClosed)
            return;
        if (state != State::Running)
            return;
        context.process->closeReadChannel(QProcess::StandardOutput);
        outputEnded();
    }

    void closeError()
    {
        if (errorChannelClosed)
            return;
        if (state != State::Running)
            return;
        context.process->closeReadChannel(QProcess::StandardError);
        errorEnded();
    }

    void transferredWrite(Util::ByteArray &&data)
    {
        struct Local {
            QtProcessInstance &process;
            Util::ByteArray data;

            Local(QtProcessInstance &process, Util::ByteArray &&data) : process(process),
                                                                        data(std::move(data))
            { }
        };

        auto local = std::make_shared<Local>(*this, std::move(data));
        Threading::runQueuedFunctor(context.receiver, [local]() {
            local->process.writeBuffer += std::move(local->data);
            local->process.processInputWrite();
        });
    }

    void streamWrite(const Util::ByteView &data)
    {
        if (context.receiver->thread() == QThread::currentThread()) {
            writeBuffer += data;
            processInputWrite();
            return;
        }
        transferredWrite(Util::ByteArray(data));
    }

    void streamWrite(Util::ByteArray &&data)
    {
        if (context.receiver->thread() == QThread::currentThread()) {
            writeBuffer += std::move(data);
            processInputWrite();
            return;
        }
        transferredWrite(std::move(data));
    }

public:
    explicit QtProcessInstance(const Spawn &spawn) : spawn(spawn),
                                                     pipeAttached(false),
                                                     context(shared()->attach()),
                                                     state(State::Initialize),
                                                     inputChannelClosed(false),
                                                     outputChannelClosed(false),
                                                     errorChannelClosed(false),
                                                     sentWriteStall(false),
                                                     readStallAny(false),
                                                     readStallOutput(false),
                                                     readStallError(false)
    { }

    virtual ~QtProcessInstance()
    {
        shared()->runDestroy(std::bind(&QtProcessInstance::destroy, this));
    }

    bool detach() override
    {
        if (context.receiver->thread() == QThread::currentThread())
            return startDetached();

        bool ok = false;
        Threading::runBlockingFunctor(context.receiver, [this, &ok] {
            ok = startDetached();
        });
        return ok;
    }

    bool start() override
    {
        if (context.receiver->thread() == QThread::currentThread())
            return startProcess();

        bool ok = false;
        Threading::runBlockingFunctor(context.receiver, [this, &ok] {
            ok = startProcess();
        });
        return ok;
    }

    void pipeTo(const std::shared_ptr<Instance> &target) override
    {
        auto pipeTarget = dynamic_cast<QtProcessInstance *>(target.get());
        if (!pipeTarget) {
            if (context.receiver->thread() == QThread::currentThread()) {
                pipeAttached = false;
            } else {
                Threading::runBlockingFunctor(context.receiver, [this] {
                    pipeAttached = false;
                });
            }
            return;
        }

        if (context.receiver->thread() == QThread::currentThread()) {
            context.process->setStandardOutputProcess(pipeTarget->context.process);
        } else {
            Threading::runBlockingFunctor(context.receiver, [this, pipeTarget] {
                context.process->setStandardOutputProcess(pipeTarget->context.process);
            });
        }
    }

    void terminate() override
    {
        if (context.receiver->thread() == QThread::currentThread()) {
            context.process->terminate();
            return;
        }
        Threading::runBlockingFunctor(context.receiver, [this] {
            context.process->terminate();
        });
    }

    void kill() override
    {
        if (context.receiver->thread() == QThread::currentThread()) {
            context.process->kill();
            return;
        }
        Threading::runBlockingFunctor(context.receiver, [this] {
            context.process->kill();
        });
    }

    bool isRunning() const override
    {
        if (context.receiver->thread() == QThread::currentThread())
            return state == State::Running;
        bool result = false;
        Threading::runBlockingFunctor(context.receiver, [this, &result] {
            result = (state == State::Running);
        });
        return result;
    }

    std::unique_ptr<Stream> inputStream() override
    {
        class Result final : public Stream {
            std::shared_ptr<QtProcessInstance> process;
            Threading::Receiver receiver;
        public:
            explicit Result(std::shared_ptr<QtProcessInstance> &&proc) : process(std::move(proc))
            { }

            virtual ~Result()
            {
                receiver.disconnect();
            }

            void start() override
            {
                if (process->context.receiver->thread() == QThread::currentThread()) {
                    if (process->inputChannelClosed) {
                        ended();
                        return;
                    }
                    process->endOfWrite.connect(receiver, [this] {
                        ended();
                    });
                    return;
                }
                Threading::runBlockingFunctor(process->context.receiver, [this] {
                    if (process->inputChannelClosed) {
                        ended();
                        return;
                    }
                    process->endOfWrite.connect(receiver, [this] {
                        ended();
                    });
                });
            }

            bool isEnded() const override
            {
                if (process->context.receiver->thread() == QThread::currentThread())
                    return process->inputChannelClosed;
                bool result = true;
                Threading::runBlockingFunctor(process->context.receiver, [this, &result] {
                    result = process->inputChannelClosed;
                });
                return result;
            }

            void write(const Util::ByteView &data) override
            { return process->streamWrite(data); }

            void write(Util::ByteArray &&data) override
            { return process->streamWrite(std::move(data)); }

            void close() override
            {
                if (process->context.receiver->thread() == QThread::currentThread())
                    return process->closeInput();
                Threading::runBlockingFunctor(process->context.receiver, [this] {
                    process->closeInput();
                });
            }
        };
        return std::unique_ptr<Stream>(new Result(shared_from_this()));
    }

    std::unique_ptr<Stream> outputStream(bool allowWrite = false) override
    {
        class Result final : public Stream {
            std::shared_ptr<QtProcessInstance> process;
            Threading::Receiver receiver;
            bool allowWrite;
        public:
            Result(bool allowWrite, std::shared_ptr<QtProcessInstance> &&proc) : process(
                    std::move(proc)), allowWrite(allowWrite)
            { }

            virtual ~Result()
            {
                receiver.disconnect();
            }

            void start() override
            {
                if (process->context.receiver->thread() == QThread::currentThread()) {
                    if (process->outputChannelClosed) {
                        ended();
                        return;
                    }
                    process->readOutput.connect(receiver, [this](const Util::ByteArray &data) {
                        read(data);
                    });
                    process->endOfOutput.connect(receiver, [this] {
                        ended();
                    });
                    return;
                }
                Threading::runBlockingFunctor(process->context.receiver, [this] {
                    if (process->outputChannelClosed) {
                        ended();
                        return;
                    }
                    process->readOutput.connect(receiver, [this](const Util::ByteArray &data) {
                        read(data);
                    });
                    process->endOfOutput.connect(receiver, [this] {
                        ended();
                    });
                });
            }

            void readStall(bool enable) override
            {
                if (process->context.receiver->thread() == QThread::currentThread())
                    return process->setReadStallOutput(enable);
                Threading::runBlockingFunctor(process->context.receiver, [this, enable] {
                    process->setReadStallOutput(enable);
                });
            }

            bool isEnded() const override
            {
                if (process->context.receiver->thread() == QThread::currentThread())
                    return process->outputChannelClosed;
                bool result = true;
                Threading::runBlockingFunctor(process->context.receiver, [this, &result] {
                    result = process->outputChannelClosed;
                });
                return result;
            }

            void write(const Util::ByteView &data) override
            {
                if (!allowWrite)
                    return;
                return process->streamWrite(data);
            }

            void write(Util::ByteArray &&data) override
            {
                if (!allowWrite)
                    return;
                return process->streamWrite(std::move(data));
            }

            void close() override
            {
                if (process->context.receiver->thread() == QThread::currentThread())
                    return process->closeOutput();
                Threading::runBlockingFunctor(process->context.receiver, [this] {
                    process->closeOutput();
                });
            }
        };
        return std::unique_ptr<Stream>(new Result(allowWrite, shared_from_this()));
    }

    std::unique_ptr<Stream> errorStream(bool allowWrite = false) override
    {
        class Result final : public Stream {
            std::shared_ptr<QtProcessInstance> process;
            Threading::Receiver receiver;
            bool allowWrite;
        public:
            Result(bool allowWrite, std::shared_ptr<QtProcessInstance> &&proc) : process(
                    std::move(proc)), allowWrite(allowWrite)
            { }

            virtual ~Result()
            {
                receiver.disconnect();
            }

            void start() override
            {
                if (process->context.receiver->thread() == QThread::currentThread()) {
                    if (process->errorChannelClosed) {
                        ended();
                        return;
                    }
                    process->readError.connect(receiver, [this](const Util::ByteArray &data) {
                        read(data);
                    });
                    process->endOfError.connect(receiver, [this] {
                        ended();
                    });
                    return;
                }
                Threading::runBlockingFunctor(process->context.receiver, [this] {
                    if (process->errorChannelClosed) {
                        ended();
                        return;
                    }
                    process->readError.connect(receiver, [this](const Util::ByteArray &data) {
                        read(data);
                    });
                    process->endOfError.connect(receiver, [this] {
                        ended();
                    });
                });
            }

            void readStall(bool enable) override
            {
                if (process->context.receiver->thread() == QThread::currentThread())
                    return process->setReadStallError(enable);
                Threading::runBlockingFunctor(process->context.receiver, [this, enable] {
                    process->setReadStallError(enable);
                });
            }

            bool isEnded() const override
            {
                if (process->context.receiver->thread() == QThread::currentThread())
                    return process->errorChannelClosed;
                bool result = true;
                Threading::runBlockingFunctor(process->context.receiver, [this, &result] {
                    result = process->errorChannelClosed;
                });
                return result;
            }

            void write(const Util::ByteView &data) override
            {
                if (!allowWrite)
                    return;
                return process->streamWrite(data);
            }

            void write(Util::ByteArray &&data) override
            {
                if (!allowWrite)
                    return;
                return process->streamWrite(std::move(data));
            }

            void close() override
            {
                if (process->context.receiver->thread() == QThread::currentThread())
                    return process->closeError();
                Threading::runBlockingFunctor(process->context.receiver, [this] {
                    process->closeError();
                });
            }
        };
        return std::unique_ptr<Stream>(new Result(allowWrite, shared_from_this()));
    }

    std::unique_ptr<Stream> combinedOutputStream(bool allowWrite = false) override
    {
        class Result final : public Stream {
            std::shared_ptr<QtProcessInstance> process;
            Threading::Receiver receiver;
            bool allowWrite;
        public:
            Result(bool allowWrite, std::shared_ptr<QtProcessInstance> &&proc) : process(
                    std::move(proc)), allowWrite(allowWrite)
            { }

            virtual ~Result()
            {
                receiver.disconnect();
            }

            void start() override
            {
                if (process->context.receiver->thread() == QThread::currentThread()) {
                    if (process->outputChannelClosed && process->errorChannelClosed) {
                        ended();
                        return;
                    }
                    process->readAny.connect(receiver, [this](const Util::ByteArray &data) {
                        read(data);
                    });
                    process->endOfAnyRead.connect(receiver, [this] {
                        ended();
                    });
                    return;
                }
                Threading::runBlockingFunctor(process->context.receiver, [this] {
                    if (process->outputChannelClosed && process->errorChannelClosed) {
                        ended();
                        return;
                    }
                    process->readAny.connect(receiver, [this](const Util::ByteArray &data) {
                        read(data);
                    });
                    process->endOfAnyRead.connect(receiver, [this] {
                        ended();
                    });
                });
            }

            void readStall(bool enable) override
            {
                if (process->context.receiver->thread() == QThread::currentThread())
                    return process->setReadStallAny(enable);
                Threading::runBlockingFunctor(process->context.receiver, [this, enable] {
                    process->setReadStallAny(enable);
                });
            }

            bool isEnded() const override
            {
                if (process->context.receiver->thread() == QThread::currentThread())
                    return process->outputChannelClosed && process->errorChannelClosed;
                bool result = true;
                Threading::runBlockingFunctor(process->context.receiver, [this, &result] {
                    result = process->outputChannelClosed && process->errorChannelClosed;
                });
                return result;
            }

            void write(const Util::ByteView &data) override
            {
                if (!allowWrite)
                    return;
                return process->streamWrite(data);
            }

            void write(Util::ByteArray &&data) override
            {
                if (!allowWrite)
                    return;
                return process->streamWrite(std::move(data));
            }

            void close() override
            {
                if (process->context.receiver->thread() == QThread::currentThread()) {
                    process->closeOutput();
                    process->closeError();
                    return;
                }
                Threading::runBlockingFunctor(process->context.receiver, [this] {
                    process->closeOutput();
                    process->closeError();
                });
            }
        };
        return std::unique_ptr<Stream>(new Result(allowWrite, shared_from_this()));
    }

    std::unique_ptr<Generic::Stream> stream() override
    {
        class Result final : public Stream {
            std::shared_ptr<QtProcessInstance> process;
            Threading::Receiver receiver;
        public:
            explicit Result(std::shared_ptr<QtProcessInstance> &&proc) : process(std::move(proc))
            { }

            virtual ~Result()
            {
                receiver.disconnect();
            }

            void start() override
            {
                if (process->context.receiver->thread() == QThread::currentThread()) {
                    if (process->outputChannelClosed && process->errorChannelClosed) {
                        ended();
                        return;
                    }
                    process->readAny.connect(receiver, [this](const Util::ByteArray &data) {
                        read(data);
                    });
                    process->endOfAnyRead.connect(receiver, [this] {
                        ended();
                    });
                    return;
                }
                Threading::runBlockingFunctor(process->context.receiver, [this] {
                    if (process->outputChannelClosed && process->errorChannelClosed) {
                        ended();
                        return;
                    }
                    process->readAny.connect(receiver, [this](const Util::ByteArray &data) {
                        read(data);
                    });
                    process->endOfAnyRead.connect(receiver, [this] {
                        ended();
                    });
                });
            }

            void readStall(bool enable) override
            {
                if (process->context.receiver->thread() == QThread::currentThread())
                    return process->setReadStallAny(enable);
                Threading::runBlockingFunctor(process->context.receiver, [this, enable] {
                    process->setReadStallAny(enable);
                });
            }

            bool isEnded() const override
            {
                if (process->context.receiver->thread() == QThread::currentThread())
                    return process->outputChannelClosed && process->errorChannelClosed;
                bool result = true;
                Threading::runBlockingFunctor(process->context.receiver, [this, &result] {
                    result = process->outputChannelClosed && process->errorChannelClosed;
                });
                return result;
            }

            void write(const Util::ByteView &data) override
            { return process->streamWrite(data); }

            void write(Util::ByteArray &&data) override
            { return process->streamWrite(std::move(data)); }

            void close() override
            {
                if (process->context.receiver->thread() == QThread::currentThread()) {
                    process->closeOutput();
                    process->closeError();
                    process->closeInput();
                    return;
                }
                Threading::runBlockingFunctor(process->context.receiver, [this] {
                    process->closeOutput();
                    process->closeError();
                    process->closeInput();
                });
            }
        };
        return std::unique_ptr<Stream>(new Result(shared_from_this()));
    }

    std::unique_ptr<Generic::Block> block() override
    { return {}; }
};

}


std::shared_ptr<Instance> Spawn::create() const
{ return std::make_shared<QtProcessInstance>(*this); }

Spawn Spawn::shell(const std::string &command)
{
#if defined(Q_OS_WIN32)
    return Spawn("PowerShell.exe", {"-Command", command});
#elif defined(Q_OS_UNIX)
    return Spawn("/bin/sh", {"-c", command});
#else
    return Spawn(command);
#endif
}

}
}
}