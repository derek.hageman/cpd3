/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <cstring>
#include <mutex>
#include <condition_variable>
#include <QLoggingCategory>

#include "access.hxx"
#include "core/qtcompat.hxx"

Q_LOGGING_CATEGORY(log_io_access, "cpd3.io.access", QtWarningMsg)

namespace CPD3 {
namespace IO {

static constexpr std::size_t stallThreshold = 4194304;

namespace Generic {

void Writable::write(const Util::ByteArray &data)
{ return write(Util::ByteView(data)); }

void Writable::write(Util::ByteArray &&data)
{ return write(Util::ByteView(data)); }

void Writable::write(const QByteArray &data)
{ return write(Util::ByteView(data)); }

void Writable::write(QByteArray &&data)
{ return write(Util::ByteView(data)); }

void Writable::write(const std::string &str)
{ return write(Util::ByteView(str.data(), str.size())); }

void Writable::write(const QString &str)
{ return write(str.toUtf8()); }

void Writable::write(const char *str)
{
    if (!str)
        return;
    return write(Util::ByteView(str, std::strlen(str)));
}

Stream::Stream() = default;

Stream::~Stream() = default;

void Stream::readStall(bool)
{ }

Util::ByteArray Stream::readAll(bool doStart)
{
    Util::ByteArray result;

    {
        std::mutex mutex;
        std::condition_variable cv;
        Threading::Receiver rx;
        bool haveEnded = false;

        read.connect(rx, [&result](const Util::ByteArray &data) {
            result += data;
        });
        ended.connect(rx, [&mutex, &cv, &haveEnded]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                haveEnded = true;
            }
            cv.notify_all();
        });

        if (doStart) {
            start();
        } else {
            if (isEnded()) {
                return result;
            }
        }

        std::unique_lock<std::mutex> lock(mutex);
        cv.wait(lock, [&haveEnded] { return haveEnded; });
    }

    return result;
}

std::unique_ptr<Stream> Stream::detachHeader(std::unique_ptr<Stream> &&stream,
                                             const std::function<bool(Util::ByteArray &)> &header,
                                             bool doStart)
{
    class Detached : public Stream {
        std::unique_ptr<Stream> original;
        std::mutex mutex;
        std::condition_variable notify;
        Util::ByteArray buffer;
        enum class State {
            WaitingForHeader,
            HeaderEnded,
            Unconnected,
            UnconnectedEnded,
            Connecting,
            Running,
            Ended,
        } state;
        bool externalStallSet;
    public:
        Detached(std::unique_ptr<Stream> &&stream, bool doStart) : original(std::move(stream)),
                                                                   state(State::WaitingForHeader),
                                                                   externalStallSet(false)
        {
            original->read.connect([this](const Util::ByteArray &data) {
                std::unique_lock<std::mutex> lock(mutex);
                for (;;) {
                    switch (state) {
                    case State::WaitingForHeader:
                        buffer += data;
                        lock.unlock();
                        notify.notify_all();
                        return;
                    case State::HeaderEnded:
                    case State::Unconnected:
                    case State::UnconnectedEnded:
                        buffer += data;
                        return;
                    case State::Connecting:
                        notify.wait(lock);
                        continue;
                    case State::Running:
                        lock.unlock();
                        read(data);
                        return;
                    case State::Ended:
                        return;
                    }
                }
            });
            original->ended.connect([this] {
                std::unique_lock<std::mutex> lock(mutex);
                for (;;) {
                    switch (state) {
                    case State::WaitingForHeader:
                        state = State::HeaderEnded;
                        lock.unlock();
                        notify.notify_all();
                        return;
                    case State::Unconnected:
                        state = State::UnconnectedEnded;
                        return;
                    case State::Connecting:
                        notify.wait(lock);
                        continue;
                    case State::Running:
                        state = State::Ended;
                        lock.unlock();
                        ended();
                        return;
                    case State::HeaderEnded:
                    case State::UnconnectedEnded:
                    case State::Ended:
                        return;
                    }
                }
            });
            writeStall = original->writeStall;

            if (doStart)
                original->start();
        }

        virtual ~Detached()
        {
            original.reset();
        }

        void start() override
        {
            std::unique_lock<std::mutex> lock(mutex);
            switch (state) {
            case State::WaitingForHeader:
            case State::HeaderEnded:
            case State::Connecting:
                Q_ASSERT(false);
                break;
            case State::Unconnected:
                state = State::Connecting;
                lock.unlock();
                if (!buffer.empty()) {
                    read(buffer);
                    buffer.clear();
                }
                if (!externalStallSet)
                    original->readStall(false);
                lock.lock();
                state = State::Running;
                lock.unlock();
                notify.notify_all();
                break;
            case State::UnconnectedEnded:
                state = State::Connecting;
                lock.unlock();
                if (!buffer.empty()) {
                    read(buffer);
                    buffer.clear();
                }
                ended();
                lock.lock();
                state = State::Ended;
                lock.unlock();
                notify.notify_all();
                break;
            case State::Running:
            case State::Ended:
                return;
            }
        }

        bool isEnded() const override
        {
            {
                std::lock_guard<std::mutex> lock(const_cast<Detached *>(this)->mutex);
                switch (state) {
                case State::WaitingForHeader:
                case State::Unconnected:
                    return false;
                case State::HeaderEnded:
                case State::UnconnectedEnded:
                case State::Connecting:
                case State::Running:
                case State::Ended:
                    break;
                }
            }
            return original->isEnded();
        }

        void readStall(bool enable) override
        {
            externalStallSet = true;
            original->readStall(enable);
        }

        void write(const Util::ByteView &data) override
        { return original->write(data); }

        void write(const Util::ByteArray &data) override
        { return original->write(data); }

        void write(Util::ByteArray &&data) override
        { return original->write(std::move(data)); }

        void write(const QByteArray &data) override
        { return original->write(data); }

        void write(QByteArray &&data) override
        { return original->write(std::move(data)); }

        bool waitForHeader(const std::function<bool(Util::ByteArray &)> &header)
        {
            std::unique_lock<std::mutex> lock(mutex);
            for (;;) {
                switch (state) {
                case State::WaitingForHeader:
                    if (header(buffer)) {
                        state = State::Unconnected;
                        lock.unlock();
                        original->readStall(true);
                        return true;
                    }
                    break;
                case State::HeaderEnded:
                    if (header(buffer)) {
                        state = State::UnconnectedEnded;
                        return true;
                    }
                    return false;
                case State::Unconnected:
                case State::UnconnectedEnded:
                case State::Connecting:
                case State::Running:
                case State::Ended:
                    Q_ASSERT(false);
                    return false;
                }

                notify.wait(lock);
            }
        }
    };

    std::unique_ptr<Detached> detached(new Detached(std::move(stream), doStart));
    if (!detached->waitForHeader(header))
        return {};
    return std::move(detached);
}

Stream::Iterator::Iterator(Stream &src, bool doStart) : source(src),
                                                        ended(false),
                                                        enableStalling(false),
                                                        wasStalled(false)
{
    source.read.connect(receiver, [this](const Util::ByteArray &data) {
        bool isStalled;
        bool oldStalled;
        {
            std::lock_guard<std::mutex> lock(mutex);
            contents += data;
            oldStalled = wasStalled;
            if (enableStalling) {
                isStalled = contents.size() > stallThreshold;
            } else {
                isStalled = wasStalled = false;
            }
            wasStalled = isStalled;
        }
        cv.notify_all();
        if (oldStalled != isStalled) {
            source.readStall(isStalled);
        }
    });
    source.ended.connect(receiver, [this]() {
        {
            std::lock_guard<std::mutex> lock(mutex);
            ended = true;
        }
        cv.notify_all();
    });

    if (doStart) {
        source.start();
    } else {
        if (source.isEnded()) {
            ended = true;
        }
    }
}

Stream::Iterator::~Iterator()
{ receiver.disconnect(); }

void Stream::Iterator::enableReadStall(bool enable)
{
    bool clearStall = false;
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (!enable) {
            if (wasStalled) {
                wasStalled = false;
                clearStall = true;
            }
        }
        enableStalling = enable;
    }
    if (clearStall) {
        source.readStall(false);
    }
}

bool Stream::Iterator::waitForEnd(double timeout)
{ return Threading::waitForTimeout(timeout, mutex, cv, [this] { return ended; }); }

Util::ByteArray Stream::Iterator::next(double timeout)
{
    if (FP::defined(timeout) && timeout <= 0.0) {
        std::lock_guard<std::mutex> lock(mutex);
        auto result = std::move(contents);
        contents.clear();
        return result;
    }

    std::unique_lock<std::mutex> lock(mutex);
    if (!FP::defined(timeout)) {
        cv.wait(lock, [this] { return ended || !contents.empty(); });
    } else {
        cv.wait_for(lock, std::chrono::duration<double>(timeout),
                    [this] { return ended || !contents.empty(); });
    }

    auto result = std::move(contents);
    contents.clear();
    return result;
}

Util::ByteArray Stream::Iterator::all(double timeout)
{
    if (FP::defined(timeout) && timeout <= 0.0) {
        std::lock_guard<std::mutex> lock(mutex);
        Util::ByteArray result = std::move(contents);
        contents.clear();
        return result;
    }

    std::unique_lock<std::mutex> lock(mutex);
    if (!FP::defined(timeout)) {
        cv.wait(lock, [this] { return ended; });
    } else {
        cv.wait_for(lock, std::chrono::duration<double>(timeout), [this] { return ended; });
    }

    auto result = std::move(contents);
    contents.clear();
    return result;
}

Block::Block() = default;

Block::~Block() = default;

void Block::read(QByteArray &target, int maximum)
{
    Util::ByteArray temp;
    read(temp, static_cast<std::size_t>(maximum));
    target += temp.toQByteArrayRef();
}


Stream::ByteArray::ByteArray(Util::ByteArray &backing) : backing(backing), haveSentEnd(false)
{ }

Stream::ByteArray::~ByteArray() = default;

void Stream::ByteArray::write(const Util::ByteView &data)
{ backing += data; }

void Stream::ByteArray::write(const Util::ByteArray &data)
{ backing += data; }

void Stream::ByteArray::write(Util::ByteArray &&data)
{ backing += std::move(data); }

void Stream::ByteArray::write(const QByteArray &data)
{ backing += data; }

void Stream::ByteArray::write(QByteArray &&data)
{ backing += std::move(data); }

void Stream::ByteArray::start()
{
    if (haveSentEnd)
        return;
    if (!backing.empty())
        read(backing);
    haveSentEnd = true;
    ended();
}

bool Stream::ByteArray::isEnded() const
{ return haveSentEnd; }

Stream::ByteView::ByteView(const Util::ByteView &backing) : backing(backing), haveSentEnd(false)
{ }

Stream::ByteView::~ByteView() = default;

void Stream::ByteView::write(const Util::ByteView &)
{ }

void Stream::ByteView::write(const Util::ByteArray &)
{ }

void Stream::ByteView::write(Util::ByteArray &&)
{ }

void Stream::ByteView::write(const QByteArray &)
{ }

void Stream::ByteView::write(QByteArray &&)
{ }

void Stream::ByteView::start()
{
    if (haveSentEnd)
        return;
    if (!backing.empty())
        read(Util::ByteArray(backing));
    haveSentEnd = true;
    ended();
}

bool Stream::ByteView::isEnded() const
{ return haveSentEnd; }


Block::ByteArray::ByteArray(Util::ByteArray &backing) : backing(backing), offset(0)
{ }

Block::ByteArray::~ByteArray() = default;

void Block::ByteArray::doWrite(const Util::ByteView &data)
{
    if (data.empty())
        return;
    auto requiredSize = offset + data.size();
    if (requiredSize > backing.size()) {
        backing.resize(requiredSize);
    }
    std::memcpy(backing.data(offset), data.data(), data.size());
    offset += data.size();
}

void Block::ByteArray::write(const Util::ByteView &data)
{ doWrite(data); }

void Block::ByteArray::write(Util::ByteArray &&data)
{
    if (offset == 0 && backing.empty()) {
        backing = std::move(data);
        offset = backing.size();
        return;
    }
    doWrite(data);
}

bool Block::ByteArray::inError() const
{ return false; }

std::string Block::ByteArray::describeError() const
{ return {}; }

bool Block::ByteArray::readFinished() const
{ return offset >= backing.size(); }

void Block::ByteArray::read(Util::ByteArray &target, std::size_t maximum)
{
    if (maximum == static_cast<std::size_t>(-1))
        maximum = backing.size();
    Util::ByteView add(backing, offset, maximum);
    offset += add.size();
    target += add;
}

void Block::ByteArray::read(QByteArray &target, int maximum)
{
    if (maximum == -1) {
        maximum = static_cast<int>(backing.size());
        Q_ASSERT(maximum > 0);
    }
    Util::ByteView add(backing, offset, maximum);
    offset += add.size();
    target.append(add.data<const char *>(), add.size());
}

void Block::ByteArray::seek(std::size_t absolute)
{
    if (absolute == static_cast<std::size_t>(-1))
        offset = backing.size();
    else
        offset = absolute;
}

void Block::ByteArray::move(std::ptrdiff_t delta)
{
    if (delta < 0) {
        if (static_cast<std::size_t>(-delta) > offset) {
            offset = 0;
            return;
        }
    }
    offset += delta;
}

std::size_t Block::ByteArray::tell() const
{ return offset; }

Block::ByteView::ByteView(const Util::ByteView &backing) : backing(backing), offset(0)
{ }

Block::ByteView::~ByteView() = default;

void Block::ByteView::write(const Util::ByteView &)
{ }

void Block::ByteView::write(const Util::ByteArray &)
{ }

void Block::ByteView::write(Util::ByteArray &&)
{ }

void Block::ByteView::write(const QByteArray &)
{ }

void Block::ByteView::write(QByteArray &&)
{ }

bool Block::ByteView::inError() const
{ return false; }

std::string Block::ByteView::describeError() const
{ return {}; }

bool Block::ByteView::readFinished() const
{ return offset >= backing.size(); }

void Block::ByteView::read(Util::ByteArray &target, std::size_t maximum)
{
    if (maximum == static_cast<std::size_t>(-1))
        maximum = backing.size();
    Util::ByteView add(backing, offset, maximum);
    offset += add.size();
    target += add;
}

void Block::ByteView::read(QByteArray &target, int maximum)
{
    if (maximum == -1) {
        maximum = static_cast<int>(backing.size());
        Q_ASSERT(maximum > 0);
    }
    Util::ByteView add(backing, offset, maximum);
    offset += add.size();
    target.append(add.data<const char *>(), add.size());
}

void Block::ByteView::seek(std::size_t absolute)
{
    if (absolute == static_cast<std::size_t>(-1))
        offset = backing.size();
    else
        offset = absolute;
}

void Block::ByteView::move(std::ptrdiff_t delta)
{
    if (delta < 0) {
        if (static_cast<std::size_t>(-delta) > offset) {
            offset = 0;
            return;
        }
    }
    offset += delta;
}

std::size_t Block::ByteView::tell() const
{ return offset; }


Block::StreamReader::StreamReader(Block &source, std::size_t chunkSize) : source(source),
                                                                          chunkSize(chunkSize),
                                                                          state(State::Initialize),
                                                                          pause(Pause::Run),
                                                                          readStallRequested(false)
{
    Q_ASSERT(chunkSize > 0);
}

Block::StreamReader::~StreamReader()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        state = State::Terminated;
    }
    cv.notify_all();
    if (thread.joinable())
        thread.join();
}

Block::StreamReader::PauseLock::PauseLock(Block::StreamReader &reader) : reader(reader)
{
    std::unique_lock<std::mutex> lock(reader.mutex);
    Q_ASSERT(reader.pause == Pause::Run);
    for (;;) {
        if (reader.pause == Pause::Paused)
            return;
        switch (reader.state) {
        case State::Initialize:
        case State::Complete:
            reader.pause = Pause::Paused;
            return;
        case State::Running:
        case State::Terminated:
            break;
        }
        reader.pause = Pause::Request;
        reader.cv.notify_all();
        reader.cv.wait(lock);
    }
}

Block::StreamReader::PauseLock::~PauseLock()
{
    {
        std::lock_guard<std::mutex> lock(reader.mutex);
        Q_ASSERT(reader.pause == Pause::Paused);
        reader.pause = Pause::Run;
    }
    reader.cv.notify_all();
}

void Block::StreamReader::write(const Util::ByteView &data)
{
    PauseLock lock(*this);
    source.write(data);
}

void Block::StreamReader::write(const Util::ByteArray &data)
{
    PauseLock lock(*this);
    source.write(data);
}

void Block::StreamReader::write(Util::ByteArray &&data)
{
    PauseLock lock(*this);
    source.write(std::move(data));
}

void Block::StreamReader::write(const QByteArray &data)
{
    PauseLock lock(*this);
    source.write(data);
}

void Block::StreamReader::write(QByteArray &&data)
{
    PauseLock lock(*this);
    source.write(std::move(data));
}

void Block::StreamReader::start()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state != State::Initialize)
            return;
        state = State::Running;
    }
    thread = std::thread(std::bind(&Block::StreamReader::run, this));
}

bool Block::StreamReader::isEnded() const
{
    std::lock_guard<std::mutex> lock(const_cast<StreamReader &>(*this).mutex);
    switch (state) {
    case State::Complete:
    case State::Terminated:
        return true;
    case State::Initialize:
    case State::Running:
        return false;
    }
    return false;
}

void Block::StreamReader::readStall(bool enable)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        readStallRequested = enable;
        if (enable)
            return;
    }
    cv.notify_all();
}

void Block::StreamReader::run()
{
    Util::ByteArray buffer;
    bool haveReadEnd = false;

    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!lock)
            lock.lock();

        switch (state) {
        case State::Initialize:
            Q_ASSERT(false);
            /* Fall through */
        case State::Terminated:
        case State::Complete:
            lock.unlock();
            ended();
            return;
        case State::Running:
            if (haveReadEnd) {
                state = State::Complete;
                lock.unlock();
                ended();
                cv.notify_all();
                return;
            }
            break;
        }

        switch (pause) {
        case Pause::Run:
            break;
        case Pause::Request:
            pause = Pause::Paused;
            cv.notify_all();
            /* Fall through */
        case Pause::Paused:
            cv.wait(lock);
            continue;
        }

        if (readStallRequested) {
            cv.wait(lock);
            continue;
        }

        lock.unlock();

        if (source.readFinished()) {
            haveReadEnd = true;
            continue;
        }

        buffer.clear();
        source.read(buffer, chunkSize);
        if (!buffer.empty()) {
            read(buffer);
        } else if (source.inError()) {
            qCDebug(log_io_access) << "Error during stream read:" << source.describeError();
            haveReadEnd = true;
        }
    }
}


Backing::Backing() = default;

Backing::~Backing() = default;

Util::ByteArray Block::readLine()
{
    Util::ByteArray line;
    while (!readFinished()) {
        read(line, 32);
        auto n = line.indexOf('\n');
        if (n == line.npos) {
            n = line.indexOf('\r');
            if (n == line.npos)
                continue;
        } else if (n != 0) {
            auto r = line.mid(0, n).indexOf('\r');
            if (r != line.npos) {
                Q_ASSERT(r < n);
                n = r;
            }
        }
        move(-static_cast<std::ptrdiff_t>(line.size() - (n + 1)));

        if (n == 0) {
            line.clear();
            continue;
        }
        line.resize(n);
        break;
    }
    return line;
}

std::unique_ptr<QIODevice> Backing::qioStream(std::unique_ptr<Stream> &&backend, QObject *parent)
{
    static constexpr std::size_t stallThreshold = 65536;

    class Device : public QIODevice {
        std::unique_ptr<Stream> stream;
        std::mutex mutex;
        std::condition_variable notify;
        Util::ByteArray availableReadData;
        bool haveReceivedEnd;

        bool isStalled() const
        { return availableReadData.size() >= stallThreshold; }

    public:
        Device(std::unique_ptr<Stream> &&stream, QObject *parent) : QIODevice(parent),
                                                                    stream(std::move(stream)),
                                                                    haveReceivedEnd(false)
        { }

        virtual ~Device() = default;

        bool open(OpenMode flags) override
        {
            if (!stream)
                return false;

            if ((flags & (Append | Truncate)) != 0)
                flags |= WriteOnly;

            if ((flags & (ReadOnly | WriteOnly)) == 0) {
                qCWarning(log_io_access) << "Stream device open mode not specified";
                return false;
            }

            if (flags & Truncate) {
                qCWarning(log_io_access) << "Generic stream truncation not supported";
            }

            if (!QIODevice::open(flags))
                return false;

            stream->read.connect([this](const Util::ByteArray &data) {
                if (data.empty())
                    return;
                bool wasStalled;
                bool nowStalled;
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    if (haveReceivedEnd)
                        return;
                    wasStalled = isStalled();
                    availableReadData += data;
                    nowStalled = isStalled();
                }
                if (!wasStalled && nowStalled) {
                    stream->readStall(true);
                }
                notify.notify_all();
                Threading::runQueuedFunctor(this, [this]() {
                    emit readyRead();
                });
            });
            stream->ended.connect([this]() {
                bool haveAnyData;
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    if (haveReceivedEnd)
                        return;
                    haveReceivedEnd = true;
                    haveAnyData = !availableReadData.empty();
                }

                notify.notify_all();
                Threading::runQueuedFunctor(this, [this, haveAnyData]() {
                    if (haveAnyData) {
                        emit readyRead();
                    }
                    emit readChannelFinished();
                });
            });

            stream->start();
            return true;
        }

        void close() override
        {
            if (!stream)
                return;
            QIODevice::close();
            stream.reset();

            {
                std::lock_guard<std::mutex> lock(mutex);
                if (haveReceivedEnd)
                    return;
                haveReceivedEnd = true;
            }
            notify.notify_all();
            emit readChannelFinished();
        }

        bool isSequential() const override
        { return true; }

        bool atEnd() const override
        {
            {
                std::lock_guard<std::mutex> lock(const_cast<Device *>(this)->mutex);
                if (!haveReceivedEnd)
                    return false;
                if (!availableReadData.empty())
                    return false;
            }
            return QIODevice::atEnd();
        }

        qint64 bytesAvailable() const override
        {
            qint64 n;
            {
                std::lock_guard<std::mutex> lock(const_cast<Device *>(this)->mutex);
                n = availableReadData.size();
            }
            return n + QIODevice::bytesAvailable();
        }

    protected:
        qint64 readData(char *data, qint64 len) override
        {
            bool wasStalled;
            bool nowStalled;
            {
                std::unique_lock<std::mutex> lock(mutex);
                notify.wait(lock, [this, len]() {
                    return !availableReadData.empty() || haveReceivedEnd || len <= 0;
                });

                len = std::min(static_cast<qint64>(availableReadData.size()), len);
                if (len <= 0) {
                    if (haveReceivedEnd)
                        return -1;
                    return 0;
                }
                wasStalled = isStalled();
                std::memcpy(data, availableReadData.data(), len);
                availableReadData.pop_front(len);
                nowStalled = isStalled();
            }

            if (wasStalled && !nowStalled && stream) {
                stream->readStall(false);
            }
            return len;
        }

        qint64 writeData(const char *data, qint64 len) override
        {
            if (!stream)
                return -1;
            stream->write(Util::ByteView(data, len));
            emit bytesWritten(len);
            return len;
        }
    };
    if (!backend)
        return {};
    return std::unique_ptr<QIODevice>(new Device(std::move(backend), parent));
}

std::unique_ptr<QIODevice> Backing::qioStream(QObject *parent)
{ return qioStream(stream(), parent); }

std::unique_ptr<QIODevice> Backing::qioBlock(std::unique_ptr<Block> &&backend, QObject *parent)
{
    class Device : public QIODevice {
        std::unique_ptr<Block> block;

    public:
        Device(std::unique_ptr<Block> &&block, QObject *parent) : QIODevice(parent),
                                                                  block(std::move(block))
        { }

        virtual ~Device() = default;

        bool open(OpenMode flags) override
        {
            if (!block)
                return false;
            if (block->inError()) {
                setErrorString(QString::fromStdString(block->describeError()));
                return false;
            }

            if ((flags & (Append | Truncate)) != 0)
                flags |= WriteOnly;

            if ((flags & (ReadOnly | WriteOnly)) == 0) {
                qCWarning(log_io_access) << "Stream device open mode not specified";
                return false;
            }

            if (flags & Truncate) {
                qCWarning(log_io_access) << "Generic stream truncation not supported";
            }

            return QIODevice::open(flags);
        }

        void close() override
        {
            if (!block)
                return;
            QIODevice::close();
            block.reset();
        }

        bool isSequential() const override
        { return false; }

        bool atEnd() const override
        {
            if (block && !block->readFinished())
                return false;
            return QIODevice::atEnd();
        }

        qint64 pos() const override
        {
            if (!block)
                return -1;
            return block->tell();
        }

        bool seek(qint64 pos) override
        {
            if (!block)
                return false;
            block->seek(pos);
            if (block->inError()) {
                setErrorString(QString::fromStdString(block->describeError()));
                return false;
            }
            return true;
        }

    protected:
        qint64 readData(char *data, qint64 len) override
        {
            if (!block)
                return -1;
            Util::ByteArray target;
            block->read(target, len);
            if (target.empty()) {
                if (block->inError()) {
                    setErrorString(QString::fromStdString(block->describeError()));
                    return -1;
                }
                if (block->readFinished())
                    return -1;
                return 0;
            }
            std::memcpy(data, target.data(), target.size());
            return target.size();
        }

        qint64 writeData(const char *data, qint64 len) override
        {
            if (!block)
                return -1;
            block->write(Util::ByteView(data, len));
            if (block->inError()) {
                setErrorString(QString::fromStdString(block->describeError()));
                return -1;
            }
            return len;
        }
    };

    if (!backend)
        return {};
    return std::unique_ptr<QIODevice>(new Device(std::move(backend), parent));
}

std::unique_ptr<QIODevice> Backing::qioBlock(QObject *parent)
{ return qioBlock(block(), parent); }

}


namespace Access {

Handle noop(bool endless)
{
    class Backing final : public Generic::Backing {
        bool endless;

        class Stream final : public Generic::Stream {
            bool endless;
        public:
            explicit Stream(bool endless) : endless(endless)
            { }

            virtual ~Stream() = default;

            void write(const Util::ByteView &) override
            { }

            void write(const Util::ByteArray &) override
            { }

            void write(Util::ByteArray &&) override
            { }

            void write(const QByteArray &) override
            { }

            void write(QByteArray &&) override
            { }

            void start() override
            {
                if (!endless)
                    ended();
            }

            bool isEnded() const override
            { return !endless; }
        };

        class Block final : public Generic::Block {
            bool endless;
        public:
            explicit Block(bool endless) : endless(endless)
            { }

            virtual ~Block() = default;

            void write(const Util::ByteView &) override
            { }

            void write(const Util::ByteArray &) override
            { }

            void write(Util::ByteArray &&) override
            { }

            void write(const QByteArray &) override
            { }

            void write(QByteArray &&) override
            { }

            bool inError() const override
            { return false; }

            std::string describeError() const override
            { return {}; }


            bool readFinished() const override
            { return !endless; }

            void read(Util::ByteArray &target, std::size_t maximum) override
            {
                if (!endless)
                    return;
                if (maximum == static_cast<std::size_t>(-1))
                    return;
                auto begin = target.size();
                target.resize(begin + maximum);
                std::memset(target.data(begin), 0, maximum);
            }

            void read(QByteArray &target, int maximum) override
            {
                if (!endless)
                    return;
                if (maximum <= 0)
                    return;
                auto begin = target.size();
                target.resize(begin + maximum);
                std::memset(target.data() + begin, 0, maximum);
            }


            void seek(std::size_t) override
            { }

            void move(std::ptrdiff_t) override
            { }

            std::size_t tell() const override
            { return static_cast<std::size_t>(-1); }
        };

    public:
        explicit Backing(bool endless) : endless(endless)
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        { return std::unique_ptr<Generic::Stream>(new Stream(endless)); }

        std::unique_ptr<Generic::Block> block() override
        { return std::unique_ptr<Generic::Block>(new Block(endless)); }
    };
    return std::make_shared<Backing>(endless);
}

Handle invalid()
{
    class Backing final : public Generic::Backing {
    public:
        Backing()
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        { return {}; }

        std::unique_ptr<Generic::Block> block() override
        { return {}; }
    };
    return std::make_shared<Backing>();
}

Handle loopback()
{
    class Backing final : public Generic::Backing, public std::enable_shared_from_this<Backing> {
        Threading::Signal<const Util::ByteArray &> streamRead;

        std::mutex blockMutex;
        Util::ByteArray blockContents;

        class Stream final : public Generic::Stream {
        public:
            explicit Stream(const Backing &context)
            {
                read = context.streamRead;
            }

            virtual ~Stream() = default;

            void write(const Util::ByteView &data) override
            {
                if (data.empty())
                    return;
                read(Util::ByteArray(data));
            }

            void write(const Util::ByteArray &data) override
            {
                if (data.empty())
                    return;
                read(data);
            }

            void write(Util::ByteArray &&data) override
            {
                if (data.empty())
                    return;
                read(std::move(data));
            }

            void start() override
            { }

            bool isEnded() const override
            { return false; }
        };

        friend class Stream;

        class Block final : public Generic::Block {
            std::shared_ptr<Backing> context;
            std::size_t offset;

            void doWrite(const Util::ByteView &data)
            {
                if (data.empty())
                    return;
                auto requiredSize = offset + data.size();
                if (requiredSize > context->blockContents.size()) {
                    context->blockContents.resize(requiredSize);
                }
                std::memcpy(context->blockContents.data(offset), data.data(), data.size());
                offset += data.size();
            }

        public:
            explicit Block(std::shared_ptr<Backing> context) : context(std::move(context)),
                                                               offset(0)
            { }

            virtual ~Block() = default;

            void write(const Util::ByteView &data) override
            { doWrite(data); }

            void write(Util::ByteArray &&data) override
            {
                std::lock_guard<std::mutex> lock(context->blockMutex);
                if (offset != 0 || !context->blockContents.empty())
                    return doWrite(data);
                offset = data.size();
                context->blockContents = std::move(data);
            }

            bool inError() const override
            { return false; }

            std::string describeError() const override
            { return {}; }


            bool readFinished() const override
            {
                std::lock_guard<std::mutex> lock(context->blockMutex);
                return offset >= context->blockContents.size();
            }

            void read(Util::ByteArray &target, std::size_t maximum) override
            {
                std::lock_guard<std::mutex> lock(context->blockMutex);
                if (maximum == static_cast<std::size_t>(-1))
                    maximum = context->blockContents.size();
                Util::ByteView add(context->blockContents, offset, maximum);
                offset += add.size();
                target += add;
            }


            void seek(std::size_t absolute) override
            {
                if (absolute == static_cast<std::size_t>(-1)) {
                    std::lock_guard<std::mutex> lock(context->blockMutex);
                    absolute = context->blockContents.size();
                } else {
                    offset = absolute;
                }
            }

            void move(std::ptrdiff_t delta) override
            {
                if (delta < 0) {
                    if (static_cast<std::size_t>(-delta) > offset) {
                        offset = 0;
                        return;
                    }
                }
                offset += delta;
            }

            std::size_t tell() const override
            { return offset; }
        };

        friend class Block;

    public:
        Backing() = default;

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        { return std::unique_ptr<Generic::Stream>(new Stream(*this)); }

        std::unique_ptr<Generic::Block> block() override
        { return std::unique_ptr<Generic::Block>(new Block(shared_from_this())); }
    };
    return std::make_shared<Backing>();
}

Handle buffer(Util::ByteArray contents)
{
    class Backing final : public Generic::Backing, public std::enable_shared_from_this<Backing> {
        Util::ByteArray contents;

        class Stream final : public Generic::Stream {
            std::shared_ptr<Backing> context;
        public:
            explicit Stream(std::shared_ptr<Backing> context) : context(std::move(context))
            { }

            virtual ~Stream() = default;

            void write(const Util::ByteView &) override
            { }

            void write(const Util::ByteArray &) override
            { }

            void write(Util::ByteArray &&) override
            { }

            void write(const QByteArray &) override
            { }

            void start() override
            {
                if (!context->contents.empty())
                    read(context->contents);
                ended();
            }

            bool isEnded() const override
            { return true; }
        };

        friend class Stream;

        class Block final : public Generic::Block {
            std::shared_ptr<Backing> context;
            std::size_t offset;

        public:
            explicit Block(std::shared_ptr<Backing> context) : context(std::move(context)),
                                                               offset(0)
            { }

            virtual ~Block() = default;

            void write(const Util::ByteView &) override
            { }

            void write(const Util::ByteArray &) override
            { }

            void write(Util::ByteArray &&) override
            { }

            void write(const QByteArray &) override
            { }

            bool inError() const override
            { return false; }

            std::string describeError() const override
            { return {}; }


            bool readFinished() const override
            { return offset >= context->contents.size(); }

            void read(Util::ByteArray &target, std::size_t maximum) override
            {
                if (maximum == static_cast<std::size_t>(-1))
                    maximum = context->contents.size();
                Util::ByteView add(context->contents, offset, maximum);
                offset += add.size();
                target += add;
            }


            void seek(std::size_t absolute) override
            {
                if (absolute == static_cast<std::size_t>(-1))
                    offset = context->contents.size();
                else
                    offset = absolute;
            }

            void move(std::ptrdiff_t delta) override
            {
                if (delta < 0) {
                    if (static_cast<std::size_t>(-delta) > offset) {
                        offset = 0;
                        return;
                    }
                }
                offset += delta;
            }

            std::size_t tell() const override
            { return offset; }
        };

        friend class Block;

    public:
        explicit Backing(Util::ByteArray &&contents) : contents(std::move(contents))
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        { return std::unique_ptr<Generic::Stream>(new Stream(shared_from_this())); }

        std::unique_ptr<Generic::Block> block() override
        { return std::unique_ptr<Generic::Block>(new Block(shared_from_this())); }
    };
    return std::make_shared<Backing>(std::move(contents));
}

Handle buffer(std::shared_ptr<Util::ByteArray> contents, bool endless)
{
    class Backing final : public Generic::Backing, public std::enable_shared_from_this<Backing> {
        std::shared_ptr<Util::ByteArray> contents;
        bool endless;

        class Stream final : public Generic::Stream {
            std::shared_ptr<Backing> context;
        public:
            explicit Stream(std::shared_ptr<Backing> context) : context(std::move(context))
            { }

            virtual ~Stream() = default;

            void write(const Util::ByteView &data) override
            { *(context->contents) += data; }

            void write(const Util::ByteArray &data) override
            { *(context->contents) += data; }

            void write(Util::ByteArray &&data) override
            { *(context->contents) += std::move(data); }

            void write(const QByteArray &data) override
            { *(context->contents) += data; }

            void write(QByteArray &&data) override
            { *(context->contents) += std::move(data); }

            void start() override
            {
                if (!context->contents->empty())
                    read(*(context->contents));
                if (!context->endless)
                    ended();
            }

            bool isEnded() const override
            { return !context->endless; }
        };

        friend class Stream;

        class Block final : public Generic::Block {
            std::shared_ptr<Backing> context;
            std::size_t offset;

            void doWrite(const Util::ByteView &data)
            {
                if (data.empty())
                    return;
                auto requiredSize = offset + data.size();
                if (requiredSize > context->contents->size()) {
                    context->contents->resize(requiredSize);
                }
                std::memcpy(context->contents->data(offset), data.data(), data.size());
                offset += data.size();
            }

        public:
            explicit Block(std::shared_ptr<Backing> context) : context(std::move(context)),
                                                               offset(0)
            { }

            virtual ~Block() = default;

            void write(const Util::ByteView &data) override
            { doWrite(data); }

            void write(Util::ByteArray &&data) override
            {
                if (offset != 0 || !context->contents->empty())
                    return doWrite(data);
                offset = data.size();
                *(context->contents) = std::move(data);
            }

            bool inError() const override
            { return false; }

            std::string describeError() const override
            { return {}; }


            bool readFinished() const override
            { return offset >= context->contents->size(); }

            void read(Util::ByteArray &target, std::size_t maximum) override
            {
                if (maximum == static_cast<std::size_t>(-1))
                    maximum = context->contents->size();
                Util::ByteView add(*(context->contents), offset, maximum);
                offset += add.size();
                target += add;
            }


            void seek(std::size_t absolute) override
            {
                if (absolute == static_cast<std::size_t>(-1))
                    offset = context->contents->size();
                else
                    offset = absolute;
            }

            void move(std::ptrdiff_t delta) override
            {
                if (delta < 0) {
                    if (static_cast<std::size_t>(-delta) > offset) {
                        offset = 0;
                        return;
                    }
                }
                offset += delta;
            }

            std::size_t tell() const override
            { return offset; }
        };

        friend class Block;

    public:
        Backing(std::shared_ptr<Util::ByteArray> &&contents, bool endless) : contents(
                std::move(contents)), endless(endless)
        { }

        virtual ~Backing() = default;

        std::unique_ptr<Generic::Stream> stream() override
        { return std::unique_ptr<Generic::Stream>(new Stream(shared_from_this())); }

        std::unique_ptr<Generic::Block> block() override
        { return std::unique_ptr<Generic::Block>(new Block(shared_from_this())); }
    };
    if (!contents)
        contents = std::make_shared<Util::ByteArray>();
    return std::make_shared<Backing>(std::move(contents), endless);
}

std::pair<std::unique_ptr<Generic::Stream>, std::unique_ptr<Generic::Stream>> pipe()
{
    struct Shared final {
        std::mutex mutex;
        std::condition_variable notify;

        struct Endpoint {
            Util::ByteArray buffer;
            bool isStarted;
            bool isEnded;
            bool bufferFlushing;

            Threading::Signal<const Util::ByteArray &> read;
            Threading::Signal<> ended;
            Threading::Signal<bool> writeStall;

            Endpoint() : isStarted(false), isEnded(false), bufferFlushing(false)
            { }
        };

        Endpoint first;
        Endpoint second;
    };

    class PipeStream final : public Generic::Stream {
        std::shared_ptr<Shared> shared;
        Shared::Endpoint &self;
        Shared::Endpoint &other;
    public:
        PipeStream(std::shared_ptr<Shared> shared, Shared::Endpoint &self, Shared::Endpoint &other)
                : shared(std::move(shared)), self(self), other(other)
        {
            read = self.read;
            ended = self.ended;
            writeStall = self.writeStall;
        }

        virtual ~PipeStream()
        {
            {
                std::lock_guard<std::mutex> lock(shared->mutex);
                other.isEnded = true;
                if (!other.isStarted)
                    return;
            }
            other.ended();
        }

        void write(const Util::ByteView &data) override
        {
            {
                std::unique_lock<std::mutex> lock(shared->mutex);
                shared->notify.wait(lock, [this] { return !other.bufferFlushing; });
                Q_ASSERT(!other.isEnded);
                if (!other.isStarted) {
                    other.buffer += data;
                    return;
                }
            }
            other.read(Util::ByteArray(data));
        }

        void write(const Util::ByteArray &data) override
        {
            {
                std::unique_lock<std::mutex> lock(shared->mutex);
                shared->notify.wait(lock, [this] { return !other.bufferFlushing; });
                Q_ASSERT(!other.isEnded);
                if (!other.isStarted) {
                    other.buffer += data;
                    return;
                }
            }
            other.read(data);
        }

        void write(Util::ByteArray &&data) override
        {
            {
                std::unique_lock<std::mutex> lock(shared->mutex);
                shared->notify.wait(lock, [this] { return !other.bufferFlushing; });
                Q_ASSERT(!other.isEnded);
                if (!other.isStarted) {
                    other.buffer += std::move(data);
                    return;
                }
            }
            other.read(data);
        }

        void start() override
        {
            {
                std::unique_lock<std::mutex> lock(shared->mutex);
                if (self.isStarted)
                    return;
                Q_ASSERT(!self.bufferFlushing);
                if (!self.buffer.empty()) {
                    self.bufferFlushing = true;
                    lock.unlock();
                    read(self.buffer);
                    lock.lock();
                    self.buffer = Util::ByteArray();
                    self.bufferFlushing = false;
                }
                self.isStarted = true;

                if (self.isEnded) {
                    lock.unlock();
                    ended();
                    lock.lock();
                }
            }
            shared->notify.notify_all();
        }

        bool isEnded() const override
        {
            std::lock_guard<std::mutex> lock(shared->mutex);
            return self.isStarted && self.isEnded;
        }

        /* Without locking here, this means it's only sane on one direction, with the
         * write end started/used first */
        void readStall(bool enable) override
        { other.writeStall(enable); }
    };

    auto shared = std::make_shared<Shared>();
    std::unique_ptr<Generic::Stream> first(new PipeStream(shared, shared->first, shared->second));
    std::unique_ptr<Generic::Stream> second(new PipeStream(shared, shared->second, shared->first));
    return {std::move(first), std::move(second)};
}

}

}
}