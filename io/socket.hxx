/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_SOCKET_HXX
#define CPD3IO_SOCKET_HXX

#include "core/first.hxx"

#include <vector>
#include <functional>
#include <QAbstractSocket>
#include <QLocalSocket>
#include <QMetaObject>
#include <QHostInfo>

#include "io.hxx"
#include "access.hxx"
#include "core/threading.hxx"
#include "drivers/qiowrapper.hxx"

namespace CPD3 {
namespace IO {
namespace Socket {

/**
 * A generic socket connection.
 */
class CPD3IO_EXPORT Connection : public Generic::Stream {
public:
    Connection();

    virtual ~Connection();

    /**
     * Get a human readable description of the peer (remote) end of the socket.
     *
     * @return  a description of the socket peer
     */
    virtual std::string describePeer() const = 0;

    class QtConnection;
};

/**
 * A socket connection backed by a Qt socket.  This must be run on a thread that will outlive it
 * (i.e. the thread is not destroyed until all connections are).
 */
class CPD3IO_EXPORT Connection::QtConnection : public Connection {
    QIOWrapper wrapper;
    QIODevice &device;

    void transferredWrite(Util::ByteArray &&data);

public:
    QtConnection() = delete;

    virtual ~QtConnection();

    using Connection::write;

    void write(const Util::ByteView &data) override;

    void write(Util::ByteArray &&data) override;

    void start() override;

    bool isEnded() const override;

    void readStall(bool enable) override;


protected:
    explicit QtConnection(QAbstractSocket &socket);

    explicit QtConnection(QLocalSocket &socket);

    /**
     * Destroy the socket connection, disconnecting it from the Qt interface backend.
     */
    void destroy();
};

/**
 * A generic socket server, which handles incoming connections.
 */
class CPD3IO_EXPORT Server {
public:
    /**
     * A function to call when an incoming connection is ready to use.
     */
    using IncomingConnection = std::function<void(std::unique_ptr<Connection> &&)>;

    Server() = delete;

    virtual ~Server();

    /**
     * Get a human readable description of the server and what it's listening on.
     *
     * @return  a description of server
     */
    virtual std::string describeServer() const = 0;

protected:
    /**
     * Create the server.
     *
     * @param connection    the incoming connection handler
     */
    explicit Server(const IncomingConnection &connection);
};


CPD3IO_EXPORT void queuedResolve(const std::string &name,
                                 QObject *context,
                                 const std::function<void(const QHostInfo &host)> &call);

namespace Internal {
/*
 * This exists because QHostInfo::lookupHost with a functor (rather than a named Qt slot)
 * does not actually do what the documentation says: it calls the functor directly from the
 * lookup pooled thread, instead of queuing it on the event loop of the context.
 */
class QueuedResolveCaller final : public QObject {
Q_OBJECT
    std::function<void(const QHostInfo &host)> call;
public:
    QueuedResolveCaller(QObject *context, const std::function<void(const QHostInfo &host)> &call);

    virtual ~QueuedResolveCaller();

public slots:

    void resultReady(const QHostInfo &host);
};
}

}
}
}

#endif //CPD3IO_SOCKET_HXX