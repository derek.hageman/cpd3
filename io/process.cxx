/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <chrono>
#include <algorithm>
#include <mutex>
#include <condition_variable>
#include <QDir>
#include <QGlobalStatic>
#include <QLoggingCategory>

#include "process.hxx"
#include "core/util.hxx"

Q_LOGGING_CATEGORY(log_io_process, "cpd3.io.process", QtWarningMsg)

namespace CPD3 {
namespace IO {
namespace Process {

Spawn::Spawn() : environment(Environment::Inherit),
                 inputStream(StreamMode::Capture),
                 outputStream(StreamMode::Capture),
                 errorStream(StreamMode::Forward)
{ }

Spawn::~Spawn() = default;

Spawn::Spawn(const std::string &parse) : environment(Environment::Inherit),
                                         inputStream(StreamMode::Capture),
                                         outputStream(StreamMode::Capture),
                                         errorStream(StreamMode::Forward)
{
    arguments = Util::split_quoted(parse);
    if (!arguments.empty()) {
        command = std::move(arguments.front());
        arguments.erase(arguments.begin());
    }
}

Spawn::Spawn(std::string command, std::vector<std::string> arguments) : command(std::move(command)),
                                                                        arguments(std::move(
                                                                                arguments)),
                                                                        environment(
                                                                                Environment::Inherit),
                                                                        inputStream(
                                                                                StreamMode::Capture),
                                                                        outputStream(
                                                                                StreamMode::Capture),
                                                                        errorStream(
                                                                                StreamMode::Forward)
{ }

Spawn::Spawn(const char *command, std::vector<std::string> arguments) : command(command),
                                                                        arguments(std::move(
                                                                                arguments)),
                                                                        environment(
                                                                                Environment::Inherit),
                                                                        inputStream(
                                                                                StreamMode::Capture),
                                                                        outputStream(
                                                                                StreamMode::Capture),
                                                                        errorStream(
                                                                                StreamMode::Forward)
{ }


Spawn::Spawn(const QString &parse) : Spawn(parse.toStdString())
{ }

Spawn::Spawn(const char *parse) : Spawn(std::string(parse))
{ }

static std::vector<std::string> argumentsFromQStringList(const QStringList &arguments)
{
    std::vector<std::string> result;
    for (const auto &str: arguments) {
        result.emplace_back(str.toStdString());
    }
    return result;
}

Spawn::Spawn(const QString &command, const QStringList &arguments) : Spawn(command.toStdString(),
                                                                           argumentsFromQStringList(
                                                                                   arguments))
{ }

Spawn Spawn::shell(const QString &command)
{ return shell(command.toStdString()); }

Spawn Spawn::shell(const char *command)
{ return shell(std::string(command)); }

bool Spawn::detach() const
{
    auto proc = create();
    if (!proc)
        return false;
    return proc->detach();
}

bool Spawn::run() const
{
    auto proc = create();
    if (!proc)
        return false;
    Instance::ExitMonitor em(*proc);
    if (!proc->start())
        return false;
    return em.success();
}

Spawn::Pipeline Spawn::pipeline(const std::vector<Spawn> &spawn)
{
    Pipeline result;
    for (const auto &s : spawn) {
        auto proc = s.create();
        if (!proc)
            return {};

        if (!result.processes.empty()) {
            result.processes.back()->pipeTo(proc);
        }

        result.processes.emplace_back(std::move(proc));
    }
    return result;
}

Spawn &Spawn::discard()
{
    inputStream = StreamMode::Discard;
    outputStream = StreamMode::Discard;
    errorStream = StreamMode::Discard;
    return *this;
}

Spawn &Spawn::forward()
{
    inputStream = StreamMode::Forward;
    outputStream = StreamMode::Forward;
    errorStream = StreamMode::Forward;
    return *this;
}

Spawn &Spawn::capture()
{
    inputStream = StreamMode::Capture;
    outputStream = StreamMode::Capture;
    errorStream = StreamMode::Capture;
    return *this;
}

Spawn &Spawn::operator+=(std::string argument)
{
    arguments.emplace_back(std::move(argument));
    return *this;
}

Spawn &Spawn::operator+=(const char *argument)
{
    arguments.emplace_back(argument);
    return *this;
}

Spawn &Spawn::operator+=(const QString &argument)
{
    arguments.emplace_back(argument.toStdString());
    return *this;
}

bool Spawn::Pipeline::start()
{
    for (const auto &proc : processes) {
        if (!proc->start())
            return false;
    }
    return true;
}

bool Spawn::Pipeline::detach()
{
    for (const auto &proc : processes) {
        if (!proc->detach())
            return false;
    }
    return true;
}

void Spawn::Pipeline::terminate()
{
    for (const auto &proc : processes) {
        proc->terminate();
    }
}

void Spawn::Pipeline::kill()
{
    for (const auto &proc : processes) {
        proc->kill();
    }
}

bool Spawn::Pipeline::wait(double timeout)
{
    if (!FP::defined(timeout)) {
        bool ok = true;
        for (const auto &proc : processes) {
            if (!proc->wait())
                ok = false;
        }
        return ok;
    }
    if (timeout <= 0.0) {
        for (const auto &proc : processes) {
            if (!proc->wait(0))
                return false;
        }
        return true;
    }

    using Clock = std::chrono::steady_clock;
    auto end = Clock::now() + std::chrono::duration<double>(timeout);
    for (const auto &proc : processes) {
        auto remaining =
                std::chrono::duration_cast<std::chrono::duration<double>>(end - Clock::now());
        if (remaining.count() <= 0) {
            if (!proc->wait(0))
                return false;
            continue;
        }
        if (!proc->wait(remaining.count()))
            return false;
    }
    return true;
}

Spawn::Pipeline::~Pipeline()
{
    /* Explicit ordering, so the input processes get destroyed first */
    for (auto &proc : processes) {
        proc.reset();
    }
}


Stream::Stream() = default;

Stream::~Stream() = default;

Instance::Instance() = default;

Instance::~Instance() = default;

bool Instance::wait(double timeout)
{
    if (FP::defined(timeout) && timeout <= 0.0)
        return !isRunning();

    std::mutex mutex;
    std::condition_variable cv;
    bool processRunning = true;

    {
        Threading::Receiver rx;
        exited.connect(rx, [&mutex, &cv, &processRunning](int, bool) {
            std::lock_guard<std::mutex> lock(mutex);
            processRunning = false;
            cv.notify_all();
        });

        if (!isRunning())
            return true;

        {
            std::unique_lock<std::mutex> lock(mutex);
            if (!FP::defined(timeout)) {
                cv.wait(lock, [&processRunning] { return !processRunning; });
            } else {
                cv.wait_for(lock, std::chrono::duration<double>(timeout),
                            [&processRunning] { return !processRunning; });
            }
        }
    }

    return !processRunning;
}

Instance::ExitMonitor::ExitMonitor(Instance &in) : process(in), exitCode(0), exitNormal(true)
{
    process.exited.connect(receiver, [this](int code, bool normal) {
        exitCode = code;
        exitNormal = normal;
    });
}

Instance::ExitMonitor::~ExitMonitor()
{ receiver.disconnect(); }

bool Instance::ExitMonitor::wait()
{ return process.wait(); }

bool Instance::ExitMonitor::isNormal()
{
    if (!wait())
        return false;
    return exitNormal;
}

int Instance::ExitMonitor::getExitCode()
{
    if (!wait())
        return -1;
    return exitCode;
}

bool Instance::ExitMonitor::success()
{
    if (!wait())
        return false;
    if (!exitNormal) {
        qCDebug(log_io_process) << "Process exited abnormally";
        return false;
    } else if (exitCode != 0) {
        qCDebug(log_io_process) << "Process exited with code:" << exitCode;
        return false;
    }
    return true;
}

void Instance::ExitMonitor::connectTerminate(Threading::Signal<> &source)
{
    source.connect(receiver, std::bind(&Instance::terminate, &process));
}


namespace {

class WorkingDirectoryShared final {
    std::mutex mutex;
    std::condition_variable cv;
    QString saved;
    bool held;
public:
    WorkingDirectoryShared() : held(false)
    { }

    ~WorkingDirectoryShared() = default;

    void acquire(const QString &directory)
    {
        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [this] { return !held; });
            Q_ASSERT(!held);
            held = true;
        }
        Q_ASSERT(saved.isEmpty());

        if (!directory.isEmpty()) {
            saved = QDir::currentPath();
            QDir::setCurrent(directory);
        }
    }

    void release()
    {
        if (!saved.isEmpty()) {
            QDir::setCurrent(saved);
            saved = QString();
        }

        {
            std::lock_guard<std::mutex> lock(mutex);
            Q_ASSERT(held);
            held = false;
        }
        cv.notify_all();
    }
};

Q_GLOBAL_STATIC(WorkingDirectoryShared, workingDirectoryShared)
}

WorkingDirectoryLock::WorkingDirectoryLock(const QString &directory)
{ workingDirectoryShared()->acquire(directory); }

WorkingDirectoryLock::WorkingDirectoryLock(const std::string &directory) : WorkingDirectoryLock(
        QString::fromStdString(directory))
{ }

WorkingDirectoryLock::WorkingDirectoryLock(const char *directory) : WorkingDirectoryLock(
        QString(directory))
{ }

WorkingDirectoryLock::~WorkingDirectoryLock()
{ workingDirectoryShared()->release(); }

}
}
}
