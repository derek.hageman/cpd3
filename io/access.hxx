/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_ACCESS_HXX
#define CPD3IO_ACCESS_HXX

#include "core/first.hxx"

#include <memory>
#include <mutex>
#include <condition_variable>
#include <QIODevice>

#include "io.hxx"
#include "core/threading.hxx"
#include "core/util.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace IO {

namespace Generic {

/**
 * A writable target for data.
 */
class CPD3IO_EXPORT Writable {
public:
    /**
     * Write data to the target.
     *
     * @param data  the data to write
     */
    virtual void write(const Util::ByteView &data) = 0;

    virtual void write(const Util::ByteArray &data);

    virtual void write(Util::ByteArray &&data);

    virtual void write(const QByteArray &data);

    virtual void write(QByteArray &&data);

    void write(const std::string &str);

    void write(const QString &str);

    void write(const char *str);
};

class CPD3IO_EXPORT Stream : public Writable {
public:
    Stream();

    virtual ~Stream();

    /**
     * Start the stream.  After this the read signals and end may be emitted.
     */
    virtual void start() = 0;

    /**
     * Test if the stream has ended.
     *
     * @return  true if the stream has ended
     */
    virtual bool isEnded() const = 0;

    /**
     * Request a read stall (stop emitting read data).  This is optional and may not be
     * respected or may take time to take effect.  So the read signal has to continue to be
     * handled.
     *
     * @param enable    enable the stall
     */
    virtual void readStall(bool enable);


    /**
     * Emitted when data have been read from the stream.
     */
    Threading::Signal<const Util::ByteArray &> read;

    /**
     * Emitted when the stream has ended (only after the start).
     */
    Threading::Signal<> ended;

    /**
     * Emitted when the write should stall, if possible.  The stream will
     * still accept data but this signal is used to indicate excessive buffering.
     */
    Threading::Signal<bool> writeStall;


    /**
     * Read the stream until it ends.
     *
     * @param doStart   call start() to start reading
     * @return          the contents read from the stream
     */
    Util::ByteArray readAll(bool doStart = true);

    /**
     * Detach a stream into a header and another stream that will read all data after
     * the header.
     * <br>
     * The handler is called with the contents read so far and should consume the header
     * and return true when the header is ready.
     *
     * @param stream    the input stream
     * @param header    the header handler
     * @param doStart   call start() to start reading
     * @return          a detached stream ready to be started or null if the header was not found
     */
    static std::unique_ptr<Stream> detachHeader(std::unique_ptr<Stream> &&stream,
                                                const std::function<
                                                        bool(Util::ByteArray & )> &header,
                                                bool doStart = true);

    class ByteArray;

    class ByteView;

    /**
     * A simple combination iterator and buffer that works on data streams.
     */
    class CPD3IO_EXPORT Iterator final {
        Stream &source;
        Threading::Receiver receiver;
        std::mutex mutex;
        std::condition_variable cv;
        Util::ByteArray contents;
        bool ended;
        bool enableStalling;
        bool wasStalled;
    public:
        Iterator() = delete;

        Iterator(const Iterator &) = delete;

        Iterator &operator=(const Iterator &) = delete;

        Iterator(Iterator &&) = delete;

        Iterator &operator=(Iterator &&) = delete;

        /**
         * Create the iterator.
         *
         * @param source    the source stream
         * @param doStart   start the stream
         */
        explicit Iterator(Stream &source, bool doStart = true);

        ~Iterator();

        /**
         * Enable read stalling of the source stream;
         *
         * @param enable    enable setting the read stall
         */
        void enableReadStall(bool enable = true);

        /**
         * Wait for the end of the stream.
         *
         * @param timeout   the maximum time to wait
         * @return          true if the stream has ended
         */
        bool waitForEnd(double timeout = FP::undefined());

        /**
         * Get the next contents of the stream.
         *
         * @param timeout   the maximum time to wait for the end or new data
         * @return          the result, or empty if the stream has ended or the timeout elapsed
         */
        Util::ByteArray next(double timeout = FP::undefined());

        /**
         * Wait for a specified amount of time and return all the contents of the iterator.
         *
         * @param timeout   the maximum time to wait for the end
         * @return          the result, or empty if the stream has ended or the timeout elapsed
         */
        Util::ByteArray all(double timeout = FP::undefined());
    };
};

/**
 * A wrapper around a byte array that implements the stream access interface.  When started
 * the whole contents are immediately emitted to read() followed by an end().  Writes are
 * always appended to the end.
 * <br>
 * NOTE: This does NOT provide thread synchronization, so all access must perform its
 * own locking.
 */
class CPD3IO_EXPORT Stream::ByteArray final : public Stream {
    Util::ByteArray &backing;
    bool haveSentEnd;
public:
    ByteArray() = delete;

    ByteArray(const ByteArray &) = delete;

    ByteArray &operator=(const ByteArray &) = delete;

    ByteArray(ByteArray &&) = delete;

    ByteArray &operator=(ByteArray &&) = delete;

    /**
     * Create the wrapper.
     *
     * @param backing   the backing data
     */
    explicit ByteArray(Util::ByteArray &backing);

    virtual ~ByteArray();

    using Stream::write;

    void write(const Util::ByteView &data) override;

    void write(const Util::ByteArray &data) override;

    void write(Util::ByteArray &&data) override;

    void write(const QByteArray &data) override;

    void write(QByteArray &&data) override;

    void start() override;

    bool isEnded() const override;
};

/**
 * A wrapper around a byte view that implements the stream access interface.  When started
 * the whole contents are immediately emitted to read() followed by an end().  All writes are
 * discarded.
 * <br>
 * NOTE: This does NOT provide thread synchronization, so all access must perform its
 * own locking.
 */
class CPD3IO_EXPORT Stream::ByteView final : public Stream {
    Util::ByteView backing;
    bool haveSentEnd;
public:
    ByteView() = delete;

    ByteView(const ByteView &) = delete;

    ByteView &operator=(const ByteView &) = delete;

    ByteView(ByteView &&) = delete;

    ByteView &operator=(ByteView &&) = delete;

    /**
     * Create the wrapper.
     *
     * @param backing   the backing data
     */
    explicit ByteView(const Util::ByteView &backing);

    virtual ~ByteView();

    using Stream::write;

    void write(const Util::ByteView &data) override;

    void write(const Util::ByteArray &data) override;

    void write(Util::ByteArray &&data) override;

    void write(const QByteArray &data) override;

    void write(QByteArray &&data) override;

    void start() override;

    bool isEnded() const override;
};

/**
 * A block/seekable access to a data target.
 */
class CPD3IO_EXPORT Block : public Writable {
public:

    Block();

    virtual ~Block();


    /**
     * Test if the access has encountered and error
     *
     * @return  true if there was an error
     */
    virtual bool inError() const = 0;

    /**
     * Get a human readable description of the latest error.
     *
     * @return  a description of the error
     */
    virtual std::string describeError() const = 0;


    /**
     * Test if reading has finished (end of file).
     *
     * @return  true if reading has finished
     */
    virtual bool readFinished() const = 0;

    /**
     * Read data into a target buffer.
     *
     * @param target    the target buffer
     * @param maximum   the maximum amount of data to read, or -1 for all remaining data
     */
    virtual void read(Util::ByteArray &target,
                      std::size_t maximum = static_cast<std::size_t>(-1)) = 0;

    virtual void read(QByteArray &target, int maximum = -1);


    /**
     * Seek to an absolute position.
     *
     * @param absolute  the absolute position, or -1 for the end of file
     */
    virtual void seek(std::size_t absolute) = 0;

    /**
     * Seek relative to the current position.
     *
     * @param delta the change in position
     */
    virtual void move(std::ptrdiff_t delta) = 0;

    /**
     * Get the current file position.
     *
     * @return the file position
     */
    virtual std::size_t tell() const = 0;

    /**
     * Read the next non-empty line.
     *
     * @return  the next line or empty on file end
     */
    Util::ByteArray readLine();

    class ByteArray;

    class ByteView;

    /**
     * A wrapper around a block access that streams the block read until it reaches the end of
     * data.  Write access is passed through.
     * <br>
     * The stream reads from a dedicated thread, so this can run asynchronously.  However, this
     * means that the block must not be accessed through the original reference while the
     * stream is running.
     */
    class CPD3IO_EXPORT StreamReader final : public Stream {
        Block &source;
        std::size_t chunkSize;
        std::thread thread;
        std::mutex mutex;
        std::condition_variable cv;
        enum class State {
            Initialize, Running, Complete, Terminated
        } state;
        enum class Pause {
            Run, Request, Paused
        } pause;
        bool readStallRequested;

        class PauseLock final {
            StreamReader &reader;
        public:
            explicit PauseLock(StreamReader &reader);

            ~PauseLock();
        };

        void run();

    public:
        StreamReader() = delete;

        StreamReader(const StreamReader &) = delete;

        StreamReader &operator=(const StreamReader &) = delete;

        StreamReader(StreamReader &&) = delete;

        StreamReader &operator=(StreamReader &&) = delete;

        /**
         * Create the stream reader.
         *
         * @param source    the source block access
         * @param chunkSize the size of chunks read at a time
         */
        explicit StreamReader(Block &source, std::size_t chunkSize = 65536);

        virtual ~StreamReader();

        void write(const Util::ByteView &data) override;

        void write(const Util::ByteArray &data) override;

        void write(Util::ByteArray &&data) override;

        void write(const QByteArray &data) override;

        void write(QByteArray &&data) override;

        void start() override;

        bool isEnded() const override;

        void readStall(bool enable) override;
    };
};

/**
 * A wrapper around a byte array that implements the block access interface.
 * <br>
 * NOTE: This does NOT provide thread synchronization, so all access must perform its
 * own locking.
 */
class CPD3IO_EXPORT Block::ByteArray final : public Block {
    Util::ByteArray &backing;
    std::size_t offset;

    void doWrite(const Util::ByteView &data);

public:
    ByteArray() = delete;

    ByteArray(const ByteArray &) = delete;

    ByteArray &operator=(const ByteArray &) = delete;

    ByteArray(ByteArray &&) = delete;

    ByteArray &operator=(ByteArray &&) = delete;

    /**
     * Create the wrapper.
     *
     * @param backing   the backing data
     */
    explicit ByteArray(Util::ByteArray &backing);

    virtual ~ByteArray();

    using Block::write;

    void write(const Util::ByteView &data) override;

    void write(Util::ByteArray &&data) override;

    bool inError() const override;

    std::string describeError() const override;

    bool readFinished() const override;

    void read(Util::ByteArray &target, std::size_t maximum = static_cast<std::size_t>(-1)) override;

    void read(QByteArray &target, int maximum = -1) override;

    void seek(std::size_t absolute) override;

    void move(std::ptrdiff_t delta) override;

    std::size_t tell() const override;
};

/**
 * A wrapper around a byte view that implements the block access interface.  All writes are
 * discarded.
 * <br>
 * NOTE: This does NOT provide thread synchronization, so all access must perform its
 * own locking.
 */
class CPD3IO_EXPORT Block::ByteView final : public Block {
    Util::ByteView backing;
    std::size_t offset;
public:
    ByteView() = delete;

    ByteView(const ByteView &) = delete;

    ByteView &operator=(const ByteView &) = delete;

    ByteView(ByteView &&) = delete;

    ByteView &operator=(ByteView &&) = delete;

    /**
     * Create the wrapper.
     *
     * @param backing   the backing data
     */
    explicit ByteView(const Util::ByteView &backing);

    virtual ~ByteView();

    using Block::write;

    void write(const Util::ByteView &data) override;

    void write(const Util::ByteArray &data) override;

    void write(Util::ByteArray &&data) override;

    void write(const QByteArray &data) override;

    void write(QByteArray &&data) override;

    bool inError() const override;

    std::string describeError() const override;

    bool readFinished() const override;

    void read(Util::ByteArray &target, std::size_t maximum = static_cast<std::size_t>(-1)) override;

    void read(QByteArray &target, int maximum = -1) override;

    void seek(std::size_t absolute) override;

    void move(std::ptrdiff_t delta) override;

    std::size_t tell() const override;
};

/**
 * A backing of data access.
 */
class CPD3IO_EXPORT Backing {
public:
    Backing();

    virtual ~Backing();


    /**
     * Create a stream mode access to the backed object.
     *
     * @return  a stream mode accessor
     */
    virtual std::unique_ptr<Stream> stream() = 0;

    /**
     * Create a block mode access to the backed object.
     *
     * @return  a block mode accessor
     */
    virtual std::unique_ptr<Block> block() = 0;

    /**
     * Get a QIODevice accessor to the object in stream mode.
     * <br>
     * The QIODevice is created on the current thread.
     *
     * @param stream    the stream device to wrap
     * @param parent    the parent of the device, if any
     * @return          a QIODevice accessor
     */
    static std::unique_ptr<QIODevice> qioStream(std::unique_ptr<Stream> &&stream,
                                                QObject *parent = nullptr);

    /**
     * Get a QIODevice accessor to the object in stream mode.
     * <br>
     * The QIODevice is created on the current thread.
     *
     * @param parent    the parent of the device, if any
     * @return          a QIODevice accessor
     */
    std::unique_ptr<QIODevice> qioStream(QObject *parent = nullptr);

    /**
     * Get a QIODevice accessor to the object in block mode.
     * <br>
     * The QIODevice is created on the current thread.
     *
     * @param block     the block device to wrap
     * @param parent    the parent of the device, if any
     * @return          a QIODevice accessor
     */
    static std::unique_ptr<QIODevice> qioBlock(std::unique_ptr<Block> &&block,
                                               QObject *parent = nullptr);

    /**
     * Get a QIODevice accessor to the object in block mode.
     * <br>
     * The QIODevice is created on the current thread.
     *
     * @param parent    the parent of the device, if any
     * @return          a QIODevice accessor
     */
    std::unique_ptr<QIODevice> qioBlock(QObject *parent = nullptr);
};

}

namespace Access {

/**
 * A generic access handle.
 */
typedef std::shared_ptr<Generic::Backing> Handle;

/**
 * Get a noop backend that discards all writes and reads nothing.
 *
 * @param endless   if set then the read is not considered to have ended ever
 * @return          a discard handler
 */
CPD3IO_EXPORT Handle noop(bool endless = false);

/**
 * Get a backend that always returns failure (nullptr) for blocks and streams.
 *
 * @return          an invalid handler
 */
CPD3IO_EXPORT Handle invalid();

/**
 * Get a backend that returns all data written as data read.
 *
 * @return      a loopback handler
 */
CPD3IO_EXPORT Handle loopback();

/**
 * Get a backend that allows read only access to some data contents.
 *
 * @param contents  the data to expose for reading
 * @return          a buffer read handler
 */
CPD3IO_EXPORT Handle buffer(Util::ByteArray contents);

/**
 * Get a backend that allows read and write access to a shared data buffer.
 * <br>
 * NOTE: This does NOT provide thread synchronization, so all access must perform its own locking.
 *
 * @param contents  the initial contents, created if missing
 * @param endless   if set then the read stream is not considered to have ended ever
 * @return          a read and write buffer handler
 */
CPD3IO_EXPORT Handle buffer(std::shared_ptr<Util::ByteArray> contents = {}, bool endless = false);

/**
 * Create a pipe from one stream to another.  Data written to one will be read from the other one,
 * after it has been started.
 *
 * @return  a pair of connected streams
 */
CPD3IO_EXPORT std::pair<std::unique_ptr<Generic::Stream>, std::unique_ptr<Generic::Stream>> pipe();

}


}
}

#endif //CPD3IO_ACCESS_HXX
