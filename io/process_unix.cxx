/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <cstring>
#include <atomic>
#include <memory>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <spawn.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <pthread.h>
#include <QLoggingCategory>
#include <QDir>
#include <QFileInfo>

#ifdef __GLIBC__

#include <gnu/libc-version.h>

#endif

#include "process.hxx"
#include "notifywake.hxx"
#include "drivers/file.hxx"
#include "core/qtcompat.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_io_process)


#if defined(__GLIBC__)
/*
 * Pre 2.26, glibc does not implement posix_spawn() with vfork() if any flags are specified,
 * so there's no benefit to using posix_spawn().
 */
# if __GLIBC__ > 2 || (__GLIBC__ == 2 && defined(__GLIBC_MINOR__) && __GLIBC_MINOR__ >= 26)
# define USE_POSIX_SPAWN
# endif
#else
# define USE_POSIX_SPAWN
#endif

#if defined(Q_OS_LINUX) || defined(Q_OS_DARWIN) || defined(Q_OS_BSD4)
#define USE_VFORK
#endif

#if defined(USE_VFORK) && defined(Q_OS_LINUX)
/*
 * vfork into posix_spawn (and any other vfork, presumably) fails on OSX and
 * presumably other BSD derivatives.  So just use it on Linux, where it's safe.
 */
#define USE_DETACH_VFORK
#endif

extern "C" {
extern char **environ;
}

namespace CPD3 {
namespace IO {
namespace Process {

namespace {

static bool makePipe(int &readEnd, int &writeEnd)
{
    int fds[2];

    NotifyWake::blockSigpipe();

#ifdef Q_OS_LINUX
    if (::pipe2(fds, O_CLOEXEC) != 0) {
        qCWarning(log_io_process) << "Pipe creation failed:" << std::strerror(errno);
        return false;
    }
#else
    if (::pipe(fds) != 0) {
        qCWarning(log_io_process) << "Pipe creation failed:" << std::strerror(errno);
        return false;
    }
    NotifyWake::setCloseOnExec(fds[0]);
    NotifyWake::setCloseOnExec(fds[1]);
#endif

    readEnd = fds[0];
    writeEnd = fds[1];

    return true;
}

class UnixProcess final : public Instance, public std::enable_shared_from_this<UnixProcess> {
    Spawn spawn;

    enum class State {
        Initialize, Running, Detached, Complete,
    } state;
    bool processRunning;

    int inputFD;
    int outputFD;
    int errorFD;

    int inputPipeFD;
    int outputPipeFD;

    NotifyWake wake;

    int pendingSignal;
    bool pendingInputClose;
    bool pendingOutputClose;
    bool pendingErrorClose;
    Util::ByteArray writeBuffer;
    bool readStallAny;
    bool readStallOutput;
    bool readStallError;

    std::thread thread;
    std::mutex mutex;
    std::condition_variable external;

    Threading::Signal<bool> writeStall;
    Threading::Signal<const Util::ByteArray &> readAny;
    Threading::Signal<const Util::ByteArray &> readOutput;
    Threading::Signal<const Util::ByteArray &> readError;
    Threading::Signal<> endOfWrite;
    Threading::Signal<> endOfAnyRead;
    Threading::Signal<> endOfOutput;
    Threading::Signal<> endOfError;

    struct LogHandler {
        std::string name;
        std::unique_ptr<QLoggingCategory> storage;

        const QLoggingCategory &operator()() const
        {
            if (storage)
                return *storage;
            return log_io_process();
        }
    };

    LogHandler log;

    void unrecoverableFailure(::pid_t pid)
    {
        inputEnded();
        outputEnded();
        errorEnded();

        if (pid != static_cast<::pid_t>(-1)) {
            while (::kill(pid, SIGKILL) == -1 && errno == EINTR) { }
            while (::waitpid(pid, nullptr, 0) <= 0 && errno == EINTR) { }
        }

        bool sendProcessEnd;
        {
            std::lock_guard<std::mutex> lock(mutex);
            sendProcessEnd = processRunning;
            processRunning = false;
        }
        if (sendProcessEnd) {
            exited(-1, false);
        }

        std::lock_guard<std::mutex> lock(mutex);
        state = State::Complete;
        external.notify_all();
    }

    void processEnded(int status)
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            processRunning = false;
        }
        external.notify_all();

        inputEnded();

        if (WIFEXITED(status)) {
            exited(WEXITSTATUS(status), true);
        } else {
            exited(-1, false);
        }
    }

    void inputEnded()
    {
        if (inputFD == -1)
            return;
        qCDebug(log) << "Process input read ending";
        NotifyWake::interruptLoop(::close, inputFD);
        {
            std::lock_guard<std::mutex> lock(mutex);
            inputFD = -1;
        }
        endOfWrite();
    }

    void outputEnded()
    {
        if (outputFD == -1)
            return;
        qCDebug(log) << "Process output write ending";
        NotifyWake::interruptLoop(::close, outputFD);
        {
            std::lock_guard<std::mutex> lock(mutex);
            outputFD = -1;
        }
        endOfOutput();
        if (errorFD == -1)
            endOfAnyRead();
    }

    void errorEnded()
    {
        if (errorFD == -1)
            return;
        qCDebug(log) << "Process error write ending";
        NotifyWake::interruptLoop(::close, errorFD);
        {
            std::lock_guard<std::mutex> lock(mutex);
            errorFD = -1;
        }
        endOfError();
        if (outputFD == -1)
            endOfAnyRead();
    }

    void outputDataAvailable(const Util::ByteArray &data)
    {
        readOutput(data);
        readAny(data);
    }

    void errorDataAvailable(const Util::ByteArray &data)
    {
        readError(data);
        readAny(data);
    }

    static bool pipeEndClosed(short int events)
    {
#ifdef POLLRDHUP
        if ((events & POLLRDHUP) != 0)
            return true;
#endif
        return (events & (POLLERR | POLLHUP | POLLNVAL)) != 0;
    }

    void run(::pid_t pid)
    {
        static constexpr std::size_t stallThreshold = 4194304;
        static constexpr std::size_t readBlockSize = 65536;

        std::vector<struct ::pollfd> pfds(1);
        std::memset(&pfds[0], 0, sizeof(struct ::pollfd));
        wake.install(pfds[0]);

        if (inputFD != -1) {
            NotifyWake::setNonblocking(inputFD);
        } else {
            endOfWrite();
        }
        if (outputFD != -1) {
            NotifyWake::setNonblocking(outputFD);
        } else {
            endOfOutput();
        }
        if (errorFD != -1) {
            NotifyWake::setNonblocking(errorFD);
        } else {
            endOfError();
        }
        if (outputFD == -1 && errorFD == -1) {
            endOfAnyRead();
        }

        if (pid != static_cast<::pid_t>(-1)) {
            log.name = "cpd3.io.process." + std::to_string(static_cast<std::uint_fast64_t>(pid));
            log.storage.reset(new QLoggingCategory(log.name.c_str(), QtWarningMsg));
            qCDebug(log) << "Process control thread started for PID"
                         << static_cast<std::uint_fast64_t>(pid);
        } else {
            qCDebug(log) << "Detached process control thread started";
        }

        Util::ByteArray dataToWrite;
        Util::ByteArray readBuffer;
        bool wasStalled = false;
        for (;;) {
            bool doOutputRead = true;
            bool doErrorRead = true;
            bool doInputClose = false;
            bool doOutputClose = false;
            bool doErrorClose = false;
            int sendSignal = 0;
            {
                std::lock_guard<std::mutex> lock(mutex);
                if (pid == static_cast<::pid_t>(-1) &&
                        inputFD == -1 &&
                        outputFD == -1 &&
                        errorFD == -1) {
                    qCDebug(log) << "Process control thread shutting down";
                    state = State::Complete;
                    external.notify_all();
                    return;
                }

                dataToWrite += std::move(writeBuffer);
                writeBuffer.clear();

                sendSignal = pendingSignal;
                pendingSignal = 0;

                doInputClose = pendingInputClose;
                doOutputClose = pendingOutputClose;
                doErrorClose = pendingErrorClose;

                doOutputRead = !readStallOutput && !readStallAny;
                doErrorRead = !readStallError && !readStallAny;
            }

            if (sendSignal != 0) {
                external.notify_all();
            }
            if (!wasStalled) {
                if (dataToWrite.size() > stallThreshold) {
                    wasStalled = true;
                    writeStall(true);
                }
            } else {
                if (dataToWrite.size() < stallThreshold) {
                    wasStalled = false;
                    writeStall(false);
                }
            }

            pfds.resize(1);
            int timeout = -1;

            std::size_t inputPoll = 0;
            if (inputFD != -1) {
                if (doInputClose && dataToWrite.empty()) {
                    inputEnded();
                    timeout = 0;
                } else {
                    inputPoll = pfds.size();
                    pfds.emplace_back();

                    auto &target = pfds.back();
                    std::memset(&target, 0, sizeof(struct ::pollfd));
                    target.fd = inputFD;
                    target.events = POLLERR | POLLHUP | POLLNVAL;

                    if (!dataToWrite.empty()) {
                        target.events |= POLLOUT;
                    }
                }
            } else {
                dataToWrite.clear();
            }

            std::size_t outputPoll = 0;
            if (outputFD != -1) {
                if (doOutputClose) {
                    outputEnded();
                    timeout = 0;
                } else if (doOutputRead) {
                    outputPoll = pfds.size();
                    pfds.emplace_back();

                    auto &target = pfds.back();
                    std::memset(&target, 0, sizeof(struct ::pollfd));
                    target.fd = outputFD;
                    target.events = POLLIN | POLLERR | POLLHUP | POLLNVAL;
#ifdef POLLRDHUP
                    target.events |= POLLRDHUP;
#endif
                }
            }

            std::size_t errorPoll = 0;
            if (errorFD != -1) {
                if (doErrorClose) {
                    errorEnded();
                    timeout = 0;
                } else if (doErrorRead) {
                    errorPoll = pfds.size();
                    pfds.emplace_back();

                    auto &target = pfds.back();
                    std::memset(&target, 0, sizeof(struct ::pollfd));
                    target.fd = errorFD;
                    target.events = POLLIN | POLLERR | POLLHUP | POLLNVAL;
#ifdef POLLRDHUP
                    target.events |= POLLRDHUP;
#endif
                }
            }

            if (pid != static_cast<::pid_t>(-1)) {
                Q_ASSERT(pid != static_cast<::pid_t>(-1));

                if (sendSignal != 0) {
                    while (::kill(pid, sendSignal) == -1 && errno == EINTR) { }
                }

                int status = 0;
                int rc = ::waitpid(pid, &status, WNOHANG);
                if (rc > 0) {
                    qCDebug(log) << "Process wait complete (" << status << ")";
                    processEnded(status);
                    pid = -1;
                    continue;
                } else if (rc < 0) {
                    int en = errno;
                    if (en != EINTR) {
                        qCDebug(log) << "Process wait failed:" << std::strerror(en);
                        processEnded(status);
                        pid = -1;
                        continue;
                    }
                }

                /*
                 * Don't try to be clever here and do something event driven to catch the exact
                 * moment the PID exits (between the waitpid() and the poll()), instead just
                 * poll at a reasonable interval.  Since we don't really care about having exact
                 * timing, this should be fine and it avoids all the race condition riddled
                 * weirdness that is SIGCHLD.
                 */
                timeout = 100;
            }

            int rc = ::poll(pfds.data(), pfds.size(), timeout);
            if (rc == 0) {
                continue;
            } else if (rc < 0) {
                int en = errno;
                if (en == EAGAIN || en == EINTR)
                    continue;
                qCDebug(log) << "Poll failed:" << std::strerror(en);
                return unrecoverableFailure(pid);
            }

            if ((pfds.front().revents & POLLIN) != 0) {
                if (!wake.clear()) {
                    return unrecoverableFailure(pid);
                }
            }

            if (inputPoll) {
                auto events = pfds[inputPoll].revents;
                if (!dataToWrite.empty() && (events & POLLOUT) != 0) {
                    Q_ASSERT(inputFD != -1);
                    auto n = ::write(inputFD, dataToWrite.data(), dataToWrite.size());
                    if (n < 0) {
                        int en = errno;
                        if (en == EPIPE) {
                            inputEnded();
                        } else if (en != EAGAIN && en != EINTR && en != EWOULDBLOCK) {
                            qCDebug(log) << "Write failed:" << std::strerror(en);
                            return unrecoverableFailure(pid);
                        }
                    } else if (n > 0) {
                        dataToWrite.pop_front(n);
                    } else if (pipeEndClosed(events)) {
                        inputEnded();
                    }
                } else if (pipeEndClosed(events)) {
                    inputEnded();
                }
            }

            if (outputPoll) {
                Q_ASSERT(doOutputRead);
                auto events = pfds[outputPoll].revents;

                if ((events & POLLIN) != 0) {
                    Q_ASSERT(outputFD != -1);
                    readBuffer.resize(readBlockSize);
                    auto n = ::read(outputFD, readBuffer.data(), readBuffer.size());
                    if (n < 0) {
                        int en = errno;
                        if (en != EAGAIN && en != EINTR && en != EWOULDBLOCK) {
                            qCDebug(log) << "Read failed:" << std::strerror(en);
                            return unrecoverableFailure(pid);
                        }
                    } else if (n == 0) {
                        outputEnded();
                    } else {
                        readBuffer.resize(n);
                        outputDataAvailable(readBuffer);
                    }
                } else if (pipeEndClosed(events)) {
                    outputEnded();
                }
            }

            if (errorPoll) {
                Q_ASSERT(doErrorRead);
                auto events = pfds[errorPoll].revents;

                if ((events & POLLIN) != 0) {
                    Q_ASSERT(errorFD != -1);
                    readBuffer.resize(readBlockSize);
                    auto n = ::read(errorFD, readBuffer.data(), readBuffer.size());
                    if (n < 0) {
                        int en = errno;
                        if (en != EAGAIN && en != EINTR && en != EWOULDBLOCK) {
                            qCDebug(log) << "Read failed:" << std::strerror(en);
                            return unrecoverableFailure(pid);
                        }
                    } else if (n == 0) {
                        errorEnded();
                    } else {
                        readBuffer.resize(n);
                        errorDataAvailable(readBuffer);
                    }
                } else if (pipeEndClosed(events)) {
                    errorEnded();
                }
            }
        }
    }

    class SpawnParameters final {
        UnixProcess &process;

        int inputRead;
        int inputWrite;
        bool attachInput;

        int outputRead;
        int outputWrite;
        bool attachOutput;

        int errorRead;
        int errorWrite;
        bool attachError;

        char *execFilename;
        std::string execFilenameBacking;
        std::vector<std::vector<char>> execArgsBacking;
        std::vector<char *> execArgs;
        std::vector<std::vector<char>> execEnvBacking;
        std::vector<char *> execEnv;

        bool setupCommand()
        {
            if (process.spawn.command.empty()) {
                qCDebug(log_io_process) << "No command specified";
                return false;
            }
            if (Util::contains(process.spawn.command, '/')) {
                execFilenameBacking = process.spawn.command;

                if (::access(execFilenameBacking.c_str(), F_OK | X_OK) != 0) {
                    qCDebug(log_io_process) << "No access to command" << process.spawn.command
                                            << ":" << std::strerror(errno);
                    return false;
                }
            } else {
                std::string search;
                if (auto path = ::getenv("PATH")) {
                    search = path;
                } else {
                    /* Returned length includes the null, so don't need the extra (just one for
                     * the empty path) */
                    auto len = ::confstr(_CS_PATH, nullptr, 0);
                    std::vector<char> buffer(len + 1);
                    buffer[0] = ':';
                    ::confstr(_CS_PATH, buffer.data() + 1, len);
                    search = buffer.data();
                }

                execFilenameBacking.clear();
                for (auto &path : Util::split_string(search, ':', true)) {
                    if (!path.empty()) {
                        if (path.back() != '/')
                            path.push_back('/');
                    }
                    path += process.spawn.command;
                    if (::access(path.c_str(), F_OK | X_OK) != 0)
                        continue;

                    execFilenameBacking = std::move(path);
                    break;
                }
                if (execFilenameBacking.empty()) {
                    qCDebug(log_io_process) << "Command" << process.spawn.command << "not found";
                    return false;
                }
            }

            execFilename = const_cast<char *>(execFilenameBacking.c_str());
            return true;
        }

        bool setupArguments()
        {
            {
                std::vector<char> raw(process.spawn.command.size() + 1, 0);
                std::memcpy(raw.data(), process.spawn.command.c_str(),
                            process.spawn.command.size());
                execArgsBacking.emplace_back(std::move(raw));
            }

            for (const auto &arg : process.spawn.arguments) {
                std::vector<char> raw(arg.size() + 1, 0);
                std::memcpy(raw.data(), arg.c_str(), arg.size());
                execArgsBacking.emplace_back(std::move(raw));
            }

            for (const auto &add : execArgsBacking) {
                execArgs.emplace_back(const_cast<char *>(add.data()));
            }
            execArgs.emplace_back(nullptr);

            return true;
        }

        bool setupEnvironment()
        {
            std::unordered_map<std::string, std::string> variables;

            switch (process.spawn.environment) {
            case Spawn::Environment::Inherit:
                for (auto ptr = environ; *ptr; ptr++) {
                    variables.emplace(Util::halves(std::string(*ptr), '='));
                }
                break;
            case Spawn::Environment::Empty:
                break;
            }

            for (const auto &add : process.spawn.environmentSet) {
                Util::insert_or_assign(variables, add.first, add.second);
            }
            for (const auto &rem : process.spawn.environmentClear) {
                variables.erase(rem);
            }

            for (const auto &var : variables) {
                std::string contents = var.first;
                if (!var.second.empty()) {
                    contents += "=";
                    contents += var.second;
                }
                std::vector<char> raw(contents.size() + 1, 0);
                std::memcpy(raw.data(), contents.c_str(), contents.size());
                execEnvBacking.emplace_back(std::move(raw));
            }

            for (const auto &add : execEnvBacking) {
                execEnv.emplace_back(const_cast<char *>(add.data()));
            }
            execEnv.emplace_back(nullptr);

            return true;
        }

        struct SpawnResult final {
            enum class Failure {
                None,

                error_posix_spawn_file_actions_init,
                error_posix_spawnattr_init,
                error_posix_spawnattr_setflags,
                error_posix_spawnattr_setsigmask,
                error_posix_spawn_dup2_stdin,
                error_posix_spawn_dup2_stdout,
                error_posix_spawn_dup2_stderr,
                error_posix_spawn,

                error_child_fork,
                error_dup2_stdin,
                error_dup2_stdout,
                error_dup2_stderr,
                error_execve,

                pipe_read,
            } failure;
            int err;

            SpawnResult() : failure(Failure::None), err(0)
            { }

            bool interpret(const SpawnParameters &params) const
            {
                switch (failure) {
                case Failure::None:
                    qCDebug(log_io_process) << "Process for" << params.execFilenameBacking
                                            << "created";
                    return true;
                case Failure::error_posix_spawn_file_actions_init:
                    qCDebug(log_io_process) << "Error starting" << params.execFilenameBacking
                                            << "in posix_spawn_file_actions_init:"
                                            << std::strerror(err);
                    break;
                case Failure::error_posix_spawnattr_init:
                    qCDebug(log_io_process) << "Error starting" << params.execFilenameBacking
                                            << "in posix_spawnattr_init:" << std::strerror(err);
                    break;
                case Failure::error_posix_spawnattr_setflags:
                    qCDebug(log_io_process) << "Error starting" << params.execFilenameBacking
                                            << "in posix_spawnattr_setflags:" << std::strerror(err);
                    break;
                case Failure::error_posix_spawnattr_setsigmask:
                    qCDebug(log_io_process) << "Error starting" << params.execFilenameBacking
                                            << "in posix_spawnattr_setsigmask:"
                                            << std::strerror(err);
                    break;
                case Failure::error_posix_spawn_dup2_stdin:
                    qCDebug(log_io_process) << "Error starting" << params.execFilenameBacking
                                            << "in posix_spawn dup2 stdin:" << std::strerror(err);
                    break;
                case Failure::error_posix_spawn_dup2_stdout:
                    qCDebug(log_io_process) << "Error starting" << params.execFilenameBacking
                                            << "in posix_spawn dup2 stdout:" << std::strerror(err);
                    break;
                case Failure::error_posix_spawn_dup2_stderr:
                    qCDebug(log_io_process) << "Error starting" << params.execFilenameBacking
                                            << "in posix_spawn dup2 stderr:" << std::strerror(err);
                    break;
                case Failure::error_posix_spawn:
                    qCDebug(log_io_process) << "Error starting" << params.execFilenameBacking << ":"
                                            << std::strerror(err);
                    break;
                case Failure::error_child_fork:
                    qCDebug(log_io_process) << "Error starting" << params.execFilenameBacking
                                            << "in child fork:" << std::strerror(err);
                    break;
                case Failure::error_dup2_stdin:
                    qCDebug(log_io_process) << "Error starting" << params.execFilenameBacking
                                            << "in dup2 stdin:" << std::strerror(err);
                    break;
                case Failure::error_dup2_stdout:
                    qCDebug(log_io_process) << "Error starting" << params.execFilenameBacking
                                            << "in dup2 stdout:" << std::strerror(err);
                    break;
                case Failure::error_dup2_stderr:
                    qCDebug(log_io_process) << "Error starting" << params.execFilenameBacking
                                            << "in dup2 stderr:" << std::strerror(err);
                    break;
                case Failure::error_execve:
                    qCDebug(log_io_process) << "Error execing" << params.execFilenameBacking << ":"
                                            << std::strerror(err);
                    break;
                case Failure::pipe_read:
                    qCDebug(log_io_process) << "Error reading from result pipe for"
                                            << params.execFilenameBacking << ":"
                                            << std::strerror(err);
                    break;
                default:
                    qCWarning(log_io_process) << "Corrupted spawn result";
                    break;
                }
                return false;
            }

#if !defined(USE_VFORK) || !defined(USE_DETACH_VFORK)
            void writeSelfToPipe(int pipeWrite) const
            {
                ::write(pipeWrite, reinterpret_cast<const uint8_t *>(this), sizeof(SpawnResult));
            }

            void resultSelfFromPipe(int pipeRead)
            {
                std::size_t totalRead = 0;
                while (totalRead < sizeof(SpawnResult)) {
                    std::uint8_t *ptr = reinterpret_cast<uint8_t *>(this) + totalRead;
                    auto n = ::read(pipeRead, ptr, sizeof(SpawnResult) - totalRead);
                    if (n < 0) {
                        int en = errno;
                        if (en != EINTR) {
                            err = errno;
                            failure = Failure::pipe_read;
                            return;
                        }
                        continue;
                    }
                    if (n == 0) {
                        /* At end of the pipe */
                        NotifyWake::interruptLoop(::close, pipeRead);
                        break;
                    }
                    totalRead += n;
                }
            }
#endif
        };

        static_assert(sizeof(SpawnResult) <= 512, "spawn result too large for pipe buffer");

#ifdef USE_POSIX_SPAWN

        void runPosixSpawn(SpawnResult &result, ::pid_t *pid = nullptr) const
        {
            /* We are in the parent process, so we should always return */

            ::posix_spawn_file_actions_t file_actions;
            ::posix_spawnattr_t attr;
            ::sigset_t mask;

            sigfillset(&mask);

            if (::posix_spawn_file_actions_init(&file_actions) != 0) {
                result.err = errno;
                result.failure = SpawnResult::Failure::error_posix_spawn_file_actions_init;
                goto out;
            }
            if (::posix_spawnattr_init(&attr) != 0) {
                result.err = errno;
                result.failure = SpawnResult::Failure::error_posix_spawnattr_init;
                goto out_file_actions;
            }

            if (::posix_spawnattr_setflags(&attr, POSIX_SPAWN_SETSIGDEF) != 0) {
                result.err = errno;
                result.failure = SpawnResult::Failure::error_posix_spawnattr_setflags;
                goto out_attr;
            }
            if (::posix_spawnattr_setsigmask(&attr, &mask) != 0) {
                result.err = errno;
                result.failure = SpawnResult::Failure::error_posix_spawnattr_setsigmask;
                goto out_attr;
            }

            if (inputRead != -1) {
                if (posix_spawn_file_actions_adddup2(&file_actions, inputRead, 0) != 0) {
                    result.err = errno;
                    result.failure = SpawnResult::Failure::error_posix_spawn_dup2_stdin;
                    goto out_attr;
                }
            }
            if (outputWrite != -1) {
                if (posix_spawn_file_actions_adddup2(&file_actions, outputWrite, 1) != 0) {
                    result.err = errno;
                    result.failure = SpawnResult::Failure::error_posix_spawn_dup2_stdout;
                    goto out_attr;
                }
            }
            if (errorWrite != -1) {
                if (posix_spawn_file_actions_adddup2(&file_actions, errorWrite, 2) != 0) {
                    result.err = errno;
                    result.failure = SpawnResult::Failure::error_posix_spawn_dup2_stderr;
                    goto out_attr;
                }
            }

            if (::posix_spawn(pid, execFilename, &file_actions, &attr, execArgs.data(),
                              execEnv.data()) != 0) {
                result.err = errno;
                result.failure = SpawnResult::Failure::error_posix_spawn;
                goto out_attr;
            }

out_attr:
            ::posix_spawnattr_destroy(&attr);
out_file_actions:
            ::posix_spawn_file_actions_destroy(&file_actions);
out:
            return;
        }

#else

        void runExec(SpawnResult &result) const
        {
            /* We are in the child process, so we do not return on success */

            ::sigset_t ss;

            /* Ignore all signals */
            sigfillset(&ss);
            ::sigprocmask(SIG_BLOCK, &ss, nullptr);


#ifdef NSIG
            static constexpr int totalSignals = NSIG;
#else
            static constexpr int totalSignals = SIGRTMAX;
#endif
            struct ::sigaction sa;
            std::memset(&sa, 0, sizeof(sa));
            sa.sa_handler = SIG_DFL;
            sigemptyset(&sa.sa_mask);
            for (int sig = 1; sig <= totalSignals; ++sig) {
                ::sigaction(sig, &sa, nullptr);
                /* Only EINVAL (expected) or EFAULT (impossible or expected for SIGKILL, SIGSTOP),
                 * so we don't have to check the result here */
            }

            if (inputRead != -1) {
                if (::dup2(inputRead, 0) == -1) {
                    result.err = errno;
                    result.failure = SpawnResult::Failure::error_dup2_stdin;
                    return;
                }
            }

            if (outputWrite != -1) {
                if (::dup2(outputWrite, 1) == -1) {
                    result.err = errno;
                    result.failure = SpawnResult::Failure::error_dup2_stdout;
                    return;
                }
            }

            if (errorWrite != -1) {
                if (::dup2(errorWrite, 2) == -1) {
                    result.err = errno;
                    result.failure = SpawnResult::Failure::error_dup2_stderr;
                    return;
                }
            }

            /* Unblock everything */
            ::sigprocmask(SIG_UNBLOCK, &ss, nullptr);

            /* All pipes, etc are close on exec, so we can just run this */
            ::execve(execFilename, execArgs.data(), execEnv.data());

            /* No return on success, so we've failed */
            result.err = errno;
            result.failure = SpawnResult::Failure::error_execve;
        }

#endif

        void waitForChildProcess(::pid_t pid) const
        {
            for (;;) {
                if (::waitpid(pid, nullptr, 0) != -1)
                    break;
                int en = errno;
                if (en != EINTR) {
                    qCWarning(log_io_process) << "Error waiting for child process"
                                              << execFilenameBacking << ":" << std::strerror(en);
                    break;
                }
            }
        }

        static int fileStream(const char *filename, int flags)
        {
            flags |= O_NOCTTY;
#ifdef O_CLOEXEC
            flags |= O_CLOEXEC;
#endif

            int fd = ::open(filename, flags, 0666);
            if (fd < 0)
                return -1;

#ifndef O_CLOEXEC
            NotifyWake::setCloseOnExec(fd);
#endif
            return fd;
        }

        static int fileStream(const std::string &filename, int flags)
        { return fileStream(filename.c_str(), flags); }

        static int discardStream(int flags)
        { return fileStream("/dev/null", flags); }

    public:
        explicit SpawnParameters(UnixProcess &process) : process(process),
                                                         inputRead(-1),
                                                         inputWrite(-1),
                                                         attachInput(true),
                                                         outputRead(-1),
                                                         outputWrite(-1),
                                                         attachOutput(true),
                                                         errorRead(-1),
                                                         errorWrite(-1),
                                                         attachError(true),
                                                         execFilename(nullptr)
        { }

        ~SpawnParameters()
        {
            if (inputRead != -1)
                NotifyWake::interruptLoop(::close, inputRead);
            if (inputWrite != -1)
                NotifyWake::interruptLoop(::close, inputWrite);

            if (outputRead != -1)
                NotifyWake::interruptLoop(::close, outputRead);
            if (outputWrite != -1)
                NotifyWake::interruptLoop(::close, outputWrite);

            if (errorRead != -1)
                NotifyWake::interruptLoop(::close, errorRead);
            if (errorWrite != -1)
                NotifyWake::interruptLoop(::close, errorWrite);
        }

        bool attach()
        {
            if (!setupCommand())
                return false;
            if (!setupArguments())
                return false;
            if (!setupEnvironment())
                return false;

            if (process.inputPipeFD != -1) {
                inputRead = process.inputPipeFD;
                process.inputPipeFD = -1;
                attachInput = false;
            } else {
                switch (process.spawn.inputStream) {
                case Spawn::StreamMode::Discard:
                    inputRead = discardStream(O_RDONLY);
                    if (inputRead == -1) {
                        qCDebug(log_io_process) << "Error discarding input stream:"
                                                << std::strerror(errno);
                        return false;
                    }
                    attachInput = false;
                    break;
                case Spawn::StreamMode::Forward:
                    attachInput = false;
                    break;
                case Spawn::StreamMode::Capture:
                    if (!makePipe(inputRead, inputWrite))
                        return false;
                    attachInput = true;
                    break;
                case Spawn::StreamMode::File:
                case Spawn::StreamMode::FileAppend:
                    if (process.spawn.inputFile.empty()) {
                        qCDebug(log_io_process) << "No input file name specified";
                        return false;
                    }
                    inputRead = fileStream(process.spawn.inputFile, O_RDONLY);
                    if (inputRead == -1) {
                        qCDebug(log_io_process) << "Error opening input file "
                                                << process.spawn.inputFile << ":"
                                                << std::strerror(errno);
                        return false;
                    }
                    attachInput = false;
                    break;
                }
            }

            if (process.outputPipeFD != -1) {
                outputWrite = process.outputPipeFD;
                process.outputPipeFD = -1;
                attachOutput = false;
            } else {
                switch (process.spawn.outputStream) {
                case Spawn::StreamMode::Discard:
                    outputWrite = discardStream(O_WRONLY);
                    if (outputWrite == -1) {
                        qCDebug(log_io_process) << "Error discarding output stream:"
                                                << std::strerror(errno);
                        return false;
                    }
                    attachOutput = false;
                    break;
                case Spawn::StreamMode::Forward:
                    attachOutput = false;
                    break;
                case Spawn::StreamMode::Capture:
                    if (!makePipe(outputRead, outputWrite))
                        return false;
                    attachOutput = true;
                    break;
                case Spawn::StreamMode::File:
                    if (process.spawn.outputFile.empty()) {
                        qCDebug(log_io_process) << "No output file name specified";
                        return false;
                    }
                    outputWrite =
                            fileStream(process.spawn.outputFile, O_WRONLY | O_CREAT | O_TRUNC);
                    if (outputWrite == -1) {
                        qCDebug(log_io_process) << "Error opening output file "
                                                << process.spawn.outputFile << ":"
                                                << std::strerror(errno);
                        return false;
                    }
                    attachOutput = false;
                    break;
                case Spawn::StreamMode::FileAppend:
                    if (process.spawn.outputFile.empty()) {
                        qCDebug(log_io_process) << "No output file name specified";
                        return false;
                    }
                    outputWrite =
                            fileStream(process.spawn.outputFile, O_WRONLY | O_CREAT | O_APPEND);
                    if (outputWrite == -1) {
                        qCDebug(log_io_process) << "Error opening output file "
                                                << process.spawn.outputFile << ":"
                                                << std::strerror(errno);
                        return false;
                    }
                    attachOutput = false;
                    break;
                }
            }

            switch (process.spawn.errorStream) {
            case Spawn::StreamMode::Discard:
                errorWrite = discardStream(O_WRONLY);
                if (errorWrite == -1) {
                    qCDebug(log_io_process) << "Error discarding error stream:"
                                            << std::strerror(errno);
                    return false;
                }
                attachError = false;
                break;
            case Spawn::StreamMode::Forward:
                attachError = false;
                break;
            case Spawn::StreamMode::Capture:
                if (!makePipe(errorRead, errorWrite))
                    return false;
                attachError = true;
                break;
            case Spawn::StreamMode::File:
                if (process.spawn.errorFile.empty()) {
                    qCDebug(log_io_process) << "No error file name specified";
                    return false;
                }
                errorWrite = fileStream(process.spawn.errorFile, O_WRONLY | O_CREAT | O_TRUNC);
                if (errorWrite == -1) {
                    qCDebug(log_io_process) << "Error opening error file "
                                            << process.spawn.errorFile << ":"
                                            << std::strerror(errno);
                    return false;
                }
                attachError = false;
                break;
            case Spawn::StreamMode::FileAppend:
                if (process.spawn.errorFile.empty()) {
                    qCDebug(log_io_process) << "No error file name specified";
                    return false;
                }
                errorWrite = fileStream(process.spawn.errorFile, O_WRONLY | O_CREAT | O_APPEND);
                if (errorWrite == -1) {
                    qCDebug(log_io_process) << "Error opening error file "
                                            << process.spawn.errorFile << ":"
                                            << std::strerror(errno);
                    return false;
                }
                attachError = false;
                break;
            }

            return true;
        }

        void complete()
        {
            if (inputRead != -1)
                NotifyWake::interruptLoop(::close, inputRead);
            if (outputWrite != -1)
                NotifyWake::interruptLoop(::close, outputWrite);
            if (errorWrite != -1)
                NotifyWake::interruptLoop(::close, errorWrite);

            if (attachInput && inputWrite != -1) {
                Q_ASSERT(process.inputFD == -1);
                process.inputFD = inputWrite;
                inputWrite = -1;
            }
            if (attachOutput && outputRead != -1) {
                Q_ASSERT(process.outputFD == -1);
                process.outputFD = outputRead;
                outputRead = -1;
            }
            if (attachError && errorRead != -1) {
                Q_ASSERT(process.errorFD == -1);
                process.errorFD = errorRead;
                errorRead = -1;
            }
        }

        bool startDetached()
        {
            SpawnResult result;
#ifdef USE_DETACH_VFORK
            ::pid_t pid = ::vfork();
#else
            int pipeRead;
            int pipeWrite;
            if (!makePipe(pipeRead, pipeWrite))
                return false;

            ::pid_t pid = ::fork();
#endif
            if (pid == static_cast<::pid_t>(-1)) {
                qCWarning(log_io_process) << "Fork failed:" << std::strerror(errno);
#ifndef USE_DETACH_VFORK
                NotifyWake::interruptLoop(::close, pipeRead);
                NotifyWake::interruptLoop(::close, pipeWrite);
#endif
                return false;
            }
            if (pid == static_cast<::pid_t>(0)) {
                ::setsid();

#ifdef USE_POSIX_SPAWN
                runPosixSpawn(result);
# ifndef USE_DETACH_VFORK
                if (result.failure != SpawnResult::Failure::None) {
                    result.writeSelfToPipe(pipeWrite);
                    ::_exit(1);
                    Q_UNREACHABLE();
                }
# endif
#else

# ifdef USE_VFORK
                /*
                 * Can use a vfork() here even if we can't use a detach one, since
                 * we're doing the normal pattern now.
                 */
                ::pid_t cpid = ::vfork();
# else
                ::pid_t cpid = ::fork();
# endif
                if (cpid == static_cast<::pid_t>(-1)) {
                    result.err = errno;
                    result.failure = SpawnResult::Failure::error_child_fork;
# ifndef USE_DETACH_VFORK
                    result.writeSelfToPipe(pipeWrite);
# endif
                    ::_exit(1);
                    Q_UNREACHABLE();
                }
                if (cpid == static_cast<::pid_t>(0)) {
                    runExec(result);
                    /* Exec failure */
# ifndef USE_DETACH_VFORK
                    result.writeSelfToPipe(pipeWrite);
# endif
                    ::_exit(2);
                    Q_UNREACHABLE();
                }
#endif

                /*
                 * If we get here, we either completed the vfork, which will have set the
                 * shared result/pipe or we're the parent of the second fork, so we can just
                 * let the exit close the pipe.
                 */
                ::_exit(0);
                Q_UNREACHABLE();
            }

#ifndef USE_DETACH_VFORK
            NotifyWake::interruptLoop(::close, pipeWrite);
#endif

            /* Don't read while waiting, just let the pipe buffer handle it if needed */
            waitForChildProcess(pid);

#ifndef USE_DETACH_VFORK
            result.resultSelfFromPipe(pipeRead);
            NotifyWake::interruptLoop(::close, pipeRead);
#endif

            return result.interpret(*this);
        }

        bool startOwned(::pid_t &childPid)
        {
            SpawnResult result;
            ::pid_t pid = 0;

#ifdef USE_POSIX_SPAWN
            runPosixSpawn(result, &pid);
            if (result.interpret(*this)) {
                childPid = pid;
                return true;
            }
            return false;
#else
# ifdef USE_VFORK
            pid = ::vfork();
# else
            int pipeRead;
            int pipeWrite;
            if (!makePipe(pipeRead, pipeWrite))
                return false;

            pid = ::fork();
# endif

            if (pid == static_cast<::pid_t>(-1)) {
                qCWarning(log_io_process) << "Fork failed:" << std::strerror(errno);
# ifndef USE_VFORK
                NotifyWake::interruptLoop(::close, pipeRead);
                NotifyWake::interruptLoop(::close, pipeWrite);
# endif
                return false;
            }

            if (pid == static_cast<::pid_t>(0)) {
                runExec(result);
                /* Exec failure */
# ifndef USE_VFORK
                result.writeSelfToPipe(pipeWrite);
# endif
                ::_exit(1);
                Q_UNREACHABLE();
            }

# ifndef USE_VFORK
            NotifyWake::interruptLoop(::close, pipeWrite);
            result.resultSelfFromPipe(pipeRead);
            NotifyWake::interruptLoop(::close, pipeRead);
# endif

            if (result.interpret(*this)) {
                childPid = pid;
                return true;
            }

            waitForChildProcess(pid);
            return false;
#endif
        }
    };

public:
    explicit UnixProcess(const Spawn &spawn) : spawn(spawn),
                                               state(State::Initialize),
                                               processRunning(false),
                                               inputFD(-1),
                                               outputFD(-1),
                                               errorFD(-1),
                                               inputPipeFD(-1),
                                               outputPipeFD(-1),
                                               pendingSignal(0),
                                               pendingInputClose(false),
                                               pendingOutputClose(false),
                                               pendingErrorClose(false),
                                               readStallAny(false),
                                               readStallOutput(false),
                                               readStallError(false)
    { }

    virtual ~UnixProcess()
    {
        {
            std::unique_lock<std::mutex> lock(mutex);
            pendingInputClose = true;
            pendingOutputClose = true;
            pendingErrorClose = true;
            if (state == State::Running) {
                external.wait(lock, [this] {
                    return state != State::Running || pendingSignal == 0;
                });
                pendingSignal = SIGTERM;
                wake();
                external.wait_for(lock, std::chrono::seconds(5),
                                  [this] { return !processRunning; });
                if (processRunning) {
                    external.wait(lock, [this] {
                        return state != State::Running || pendingSignal == 0;
                    });
                    pendingSignal = SIGKILL;
                    wake();
                }
            } else {
                wake();
            }
        }
        if (thread.joinable()) {
            thread.join();
        }
        if (inputFD != -1) {
            NotifyWake::interruptLoop(::close, inputFD);
        }
        if (outputFD != -1) {
            NotifyWake::interruptLoop(::close, outputFD);
        }
        if (errorFD != -1) {
            NotifyWake::interruptLoop(::close, errorFD);
        }
        if (inputPipeFD != -1) {
            NotifyWake::interruptLoop(::close, inputPipeFD);
        }
        if (outputPipeFD != -1) {
            NotifyWake::interruptLoop(::close, outputPipeFD);
        }
    }

    bool detach() override
    {
        Q_ASSERT(state == State::Initialize);

        SpawnParameters param(*this);
        if (!param.attach())
            return false;
        if (!param.startDetached())
            return false;

        param.complete();
        {
            std::lock_guard<std::mutex> lock(mutex);
            state = State::Detached;
        }
        thread = std::thread(&UnixProcess::run, this, static_cast<::pid_t>(-1));
        return true;
    }

    bool start() override
    {
        Q_ASSERT(state == State::Initialize);

        SpawnParameters param(*this);
        if (!param.attach())
            return false;
        ::pid_t pid = static_cast<::pid_t>(-1);
        if (!param.startOwned(pid))
            return false;

        param.complete();
        {
            std::lock_guard<std::mutex> lock(mutex);
            processRunning = true;
            state = State::Running;
        }
        thread = std::thread(&UnixProcess::run, this, pid);
        return true;
    }

    void pipeTo(const std::shared_ptr<Instance> &target) override
    {
        Q_ASSERT(state == State::Initialize);

        if (outputPipeFD != -1) {
            NotifyWake::interruptLoop(::close, outputPipeFD);
            outputPipeFD = -1;
        }

        auto other = std::dynamic_pointer_cast<UnixProcess>(target);;
        if (other) {
            Q_ASSERT(other->state == State::Initialize);
            if (other->inputPipeFD != -1) {
                NotifyWake::interruptLoop(::close, other->inputPipeFD);
            }

            int readEnd = -1;
            int writeEnd = -1;
            if (!makePipe(readEnd, writeEnd))
                return;

            outputPipeFD = writeEnd;
            other->inputPipeFD = readEnd;
        }
    }

    void terminate() override
    {
        {
            std::unique_lock<std::mutex> lock(mutex);
            Q_ASSERT(state != State::Initialize);
            Q_ASSERT(state != State::Detached);

            external.wait(lock, [this] {
                return state != State::Running || pendingSignal == 0;
            });
            pendingSignal = SIGTERM;
        }
        wake();
    }

    void kill() override
    {
        {
            std::unique_lock<std::mutex> lock(mutex);
            Q_ASSERT(state != State::Initialize);
            Q_ASSERT(state != State::Detached);

            external.wait(lock, [this] {
                return state != State::Running || pendingSignal == 0;
            });
            pendingSignal = SIGTERM;
        }
        wake();
    }

    bool isRunning() const override
    {
        std::lock_guard<std::mutex> lock(const_cast<UnixProcess *>(this)->mutex);
        return processRunning;
    }

    bool wait(double timeout = FP::undefined()) override
    {
        return Threading::waitForTimeout(timeout, mutex, external,
                                         [this] { return !processRunning; });
    }

    std::unique_ptr<Stream> inputStream() override
    {
        class Result final : public Stream {
            std::shared_ptr<UnixProcess> process;
            Threading::Receiver receiver;
        public:
            explicit Result(std::shared_ptr<UnixProcess> &&proc) : process(std::move(proc))
            { }

            virtual ~Result()
            {
                receiver.disconnect();
            }

            void start() override
            {
                std::unique_lock<std::mutex> lock(process->mutex);
                if (process->state != State::Initialize && process->inputFD == -1) {
                    lock.unlock();
                    ended();
                    return;
                }
                process->endOfWrite.connect(receiver, [this] {
                    ended();
                });
            }

            bool isEnded() const override
            {
                std::unique_lock<std::mutex> lock(process->mutex);
                if (process->state == State::Initialize)
                    return false;
                return process->inputFD == -1;
            }

            void write(const Util::ByteView &data) override
            {
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->writeBuffer += data;
                }
                process->wake();
            }

            void write(Util::ByteArray &&data) override
            {
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->writeBuffer += std::move(data);
                }
                process->wake();
            }

            void close() override
            {
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->pendingInputClose = true;
                }
                process->wake();
            }
        };
        return std::unique_ptr<Stream>(new Result(shared_from_this()));
    }

    std::unique_ptr<Stream> outputStream(bool allowWrite = false) override
    {
        class Result final : public Stream {
            std::shared_ptr<UnixProcess> process;
            Threading::Receiver receiver;
            bool allowWrite;
        public:
            Result(bool allowWrite, std::shared_ptr<UnixProcess> &&proc) : process(std::move(proc)),
                                                                           allowWrite(allowWrite)
            { }

            virtual ~Result()
            {
                receiver.disconnect();
            }

            void start() override
            {
                std::unique_lock<std::mutex> lock(process->mutex);
                if (process->state != State::Initialize && process->outputFD == -1) {
                    lock.unlock();
                    ended();
                    return;
                }
                process->readOutput.connect(receiver, [this](const Util::ByteArray &data) {
                    read(data);
                });
                process->endOfOutput.connect(receiver, [this] {
                    ended();
                });
            }

            void readStall(bool enable) override
            {
                {
                    std::unique_lock<std::mutex> lock(process->mutex);
                    process->readStallOutput = enable;
                }
                if (!enable)
                    process->wake();
            }

            bool isEnded() const override
            {
                std::unique_lock<std::mutex> lock(process->mutex);
                if (process->state == State::Initialize)
                    return false;
                return process->outputFD == -1;
            }

            void write(const Util::ByteView &data) override
            {
                if (!allowWrite)
                    return;
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->writeBuffer += data;
                }
                process->wake();
            }

            void write(Util::ByteArray &&data) override
            {
                if (!allowWrite)
                    return;
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->writeBuffer += std::move(data);
                }
                process->wake();
            }

            void close() override
            {
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->pendingOutputClose = true;
                }
                process->wake();
            }
        };
        return std::unique_ptr<Stream>(new Result(allowWrite, shared_from_this()));
    }

    std::unique_ptr<Stream> errorStream(bool allowWrite = false) override
    {
        class Result final : public Stream {
            std::shared_ptr<UnixProcess> process;
            Threading::Receiver receiver;
            bool allowWrite;
        public:
            Result(bool allowWrite, std::shared_ptr<UnixProcess> &&proc) : process(std::move(proc)),
                                                                           allowWrite(allowWrite)
            { }

            virtual ~Result()
            {
                receiver.disconnect();
            }

            void start() override
            {
                std::unique_lock<std::mutex> lock(process->mutex);
                if (process->state != State::Initialize && process->errorFD == -1) {
                    lock.unlock();
                    ended();
                    return;
                }
                process->readError.connect(receiver, [this](const Util::ByteArray &data) {
                    read(data);
                });
                process->endOfError.connect(receiver, [this] {
                    ended();
                });
            }

            void readStall(bool enable) override
            {
                {
                    std::unique_lock<std::mutex> lock(process->mutex);
                    process->readStallError = enable;
                }
                if (!enable)
                    process->wake();
            }

            bool isEnded() const override
            {
                std::unique_lock<std::mutex> lock(process->mutex);
                if (process->state == State::Initialize)
                    return false;
                return process->errorFD == -1;
            }

            void write(const Util::ByteView &data) override
            {
                if (!allowWrite)
                    return;
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->writeBuffer += data;
                }
                process->wake();
            }

            void write(Util::ByteArray &&data) override
            {
                if (!allowWrite)
                    return;
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->writeBuffer += std::move(data);
                }
                process->wake();
            }

            void close() override
            {
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->pendingErrorClose = true;
                }
                process->wake();
            }
        };
        return std::unique_ptr<Stream>(new Result(allowWrite, shared_from_this()));
    }

    std::unique_ptr<Stream> combinedOutputStream(bool allowWrite = false) override
    {
        class Result final : public Stream {
            std::shared_ptr<UnixProcess> process;
            Threading::Receiver receiver;
            bool allowWrite;
        public:
            Result(bool allowWrite, std::shared_ptr<UnixProcess> &&proc) : process(std::move(proc)),
                                                                           allowWrite(allowWrite)
            { }

            virtual ~Result()
            {
                receiver.disconnect();
            }

            void start() override
            {
                std::unique_lock<std::mutex> lock(process->mutex);
                if (process->state != State::Initialize &&
                        process->outputFD == -1 &&
                        process->errorFD == -1) {
                    lock.unlock();
                    ended();
                    return;
                }
                process->readAny.connect(receiver, [this](const Util::ByteArray &data) {
                    read(data);
                });
                process->endOfAnyRead.connect(receiver, [this] {
                    ended();
                });
            }

            void readStall(bool enable) override
            {
                {
                    std::unique_lock<std::mutex> lock(process->mutex);
                    process->readStallAny = enable;
                }
                if (!enable)
                    process->wake();
            }

            bool isEnded() const override
            {
                std::unique_lock<std::mutex> lock(process->mutex);
                if (process->state == State::Initialize)
                    return false;
                return process->outputFD == -1 && process->errorFD == -1;
            }

            void write(const Util::ByteView &data) override
            {
                if (!allowWrite)
                    return;
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->writeBuffer += data;
                }
                process->wake();
            }

            void write(Util::ByteArray &&data) override
            {
                if (!allowWrite)
                    return;
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->writeBuffer += std::move(data);
                }
                process->wake();
            }

            void close() override
            {
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->pendingOutputClose = true;
                    process->pendingErrorClose = true;
                }
                process->wake();
            }
        };
        return std::unique_ptr<Stream>(new Result(allowWrite, shared_from_this()));
    }

    std::unique_ptr<Generic::Stream> stream() override
    {
        class Result final : public Stream {
            std::shared_ptr<UnixProcess> process;
            Threading::Receiver receiver;
        public:
            explicit Result(std::shared_ptr<UnixProcess> &&proc) : process(std::move(proc))
            { }

            virtual ~Result()
            {
                receiver.disconnect();
            }

            void start() override
            {
                std::unique_lock<std::mutex> lock(process->mutex);
                if (process->state != State::Initialize &&
                        process->outputFD == -1 &&
                        process->errorFD == -1) {
                    lock.unlock();
                    ended();
                    return;
                }
                process->readAny.connect(receiver, [this](const Util::ByteArray &data) {
                    read(data);
                });
                process->endOfAnyRead.connect(receiver, [this] {
                    ended();
                });
            }

            void readStall(bool enable) override
            {
                {
                    std::unique_lock<std::mutex> lock(process->mutex);
                    process->readStallAny = enable;
                }
                if (!enable)
                    process->wake();
            }

            bool isEnded() const override
            {
                std::unique_lock<std::mutex> lock(process->mutex);
                if (process->state == State::Initialize)
                    return false;
                return process->outputFD == -1 && process->errorFD == -1;
            }

            void write(const Util::ByteView &data) override
            {
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->writeBuffer += data;
                }
                process->wake();
            }

            void write(Util::ByteArray &&data) override
            {
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->writeBuffer += std::move(data);
                }
                process->wake();
            }

            void close() override
            {
                {
                    std::lock_guard<std::mutex> lock(process->mutex);
                    process->pendingOutputClose = true;
                    process->pendingErrorClose = true;
                    process->pendingInputClose = true;
                }
                process->wake();
            }
        };
        return std::unique_ptr<Stream>(new Result(shared_from_this()));
    }

    std::unique_ptr<Generic::Block> block() override
    { return {}; }
};

}

std::shared_ptr<Instance> Spawn::create() const
{ return std::make_shared<UnixProcess>(*this); }

Spawn Spawn::shell(const std::string &command)
{ return Spawn("/bin/sh", {"-c", command}); }

void closeAllOpenFDs()
{
#if defined(Q_OS_LINUX)
    {
        QDir procFDs("/proc/self/fd");
        if (procFDs.exists()) {
            for (const auto &fd : procFDs.entryInfoList(QDir::NoDotAndDotDot)) {
                bool ok = false;
                int i = fd.fileName().toInt(&ok, 10);
                if (!ok || i < 3)
                    continue;
                ::close(i);
            }
            return;
        }
    }
#elif defined(Q_OS_DARWIN)
    {
        QDir devFDs("/dev/fd");
        if (devFDs.exists()) {
            for (const auto &fd : devFDs.entryInfoList(QDir::NoDotAndDotDot)) {
                bool ok = false;
                int i = fd.fileName().toInt(&ok, 10);
                if (!ok || i < 3)
                    continue;
                ::close(i);
            }
            return;
        }
    }
#endif

    int fd_max = 1024;
#ifdef OPEN_MAX
    fd_max = std::min<int>(static_cast<int>(::sysconf(OPEN_MAX)), 65536);
#endif
    for (int i = 3; i < fd_max; ++i) {
        if (::fcntl(i, F_GETFD) == -1 && errno == EBADF)
            continue;
        ::close(i);
    }
}

void executeDaemonize(bool keepStdio)
{
    if (!keepStdio) {
        ::putenv(const_cast<char *>("QT_LOGGING_RULES=*=false"));
    }

    if (::setsid() == -1) {
        ::fprintf(stderr, "Can't create new process group: %s\n", ::strerror(errno));
        ::_exit(1);
    }
    if (::chdir("/") == -1) {
        ::fprintf(stderr, "Can't change to root directory: %s\n", ::strerror(errno));
        ::_exit(2);
    }

    if (!keepStdio) {
        int flags = O_RDONLY | O_NOCTTY;
#ifdef O_CLOEXEC
        flags |= O_CLOEXEC;
#endif
        int fd = ::open("/dev/null", flags);
        if (fd < 0) {
            ::fprintf(stderr, "Can't open /dev/null for reading: %s\n", ::strerror(errno));
            ::_exit(3);
        }
        if (::dup2(fd, 0) == -1) {
            ::fprintf(stderr, "Can't dup2 stdin: %s\n", ::strerror(errno));
            ::_exit(4);
        }
        ::close(fd);

        flags = O_WRONLY | O_NOCTTY;
#ifdef O_CLOEXEC
        flags |= O_CLOEXEC;
#endif
        fd = ::open("/dev/null", flags);
        if (fd < 0) {
            ::fprintf(stderr, "Can't open /dev/null for writing: %s\n", ::strerror(errno));
            ::_exit(5);
        }
        if (::dup2(fd, 1) == -1) {
            ::fprintf(stderr, "Can't dup2 stdout: %s\n", ::strerror(errno));
            ::_exit(6);
        }
        if (::dup2(fd, 2) == -1) {
            ::fprintf(stderr, "Can't dup2 stderr: %s\n", ::strerror(errno));
            ::_exit(7);
        }
        ::close(fd);
    }
}

void setProcessName(const std::string &name, int &argc, char **&argv)
{
#if defined(Q_OS_LINUX)
    if (auto comm = Access::file("/proc/self/comm", File::Mode::writeOnly())->block()) {
        comm->write(name);
    }
#endif

#if defined(Q_OS_LINUX) || defined(Q_OS_DARWIN)

    /*
     * Not thread safe, but this whole function is per process, so re-entering it
     * is a bug anyway.
     */
    static char *argvBegin = nullptr;
    static std::size_t argvSize = 0;
    if (!argvBegin) {
        if (argc <= 0)
            return;
        argvBegin = argv[0];
        auto argvEnd = argv[argc - 1] + std::strlen(argv[argc - 1]);
        argvSize = argvEnd - argvBegin;
    }

    std::memset(argvBegin, 0, argvSize);
    std::memcpy(argvBegin, name.c_str(), std::min<std::size_t>(argvSize - 1, name.size()));

    argc = 1;
    argv[0] = argvBegin;
    argv[1] = nullptr;

#elif defined(Q_OS_BSD4)
    Q_UNUSED(argc);
    Q_UNUSED(argv);

    ::setproctitle(name.c_str());

#else
    Q_UNUSED(name);
    Q_UNUSED(argc);
    Q_UNUSED(argv);
#endif
}

}
}
}