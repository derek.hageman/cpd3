/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <cstring>
#include <atomic>
#include <QLoggingCategory>

#include "notifywake.hxx"
#include "core/qtcompat.hxx"

#ifdef HAVE_EVENTFD

#include <sys/eventfd.h>

#endif

#if defined(Q_OS_UNIX)

#include <unistd.h>
#include <poll.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <signal.h>

#elif defined(Q_OS_WIN32)

#include <Windows.h>

#endif


Q_LOGGING_CATEGORY(log_io_pollwake, "cpd3.io.pollwake", QtWarningMsg)

namespace CPD3 {
namespace IO {

#if defined(Q_OS_UNIX)

NotifyWake::NotifyWake()
{
#ifdef HAVE_EVENTFD
    if ((notifyEvent = ::eventfd(0, EFD_NONBLOCK | EFD_CLOEXEC)) == -1) {
        qCritical(log_io_pollwake) << "Failed to create notify FD:" << std::strerror(errno);
        return;
    }
#else
    notifyRead = -1;
    notifyWrite = -1;

    blockSigpipe();

    int fds[2];
#ifdef Q_OS_LINUX
    if (::pipe2(fds, O_NONBLOCK | O_CLOEXEC) != 0) {
        qCWarning(log_io_process) << "Pipe creation failed:" << std::strerror(errno);
        return false;
    }
#else
    if (::pipe(fds) != 0) {
        qCritical(log_io_pollwake) << "Failed to create notify pipe:" << std::strerror(errno);
        return;
    }
    setCloseOnExec(fds[0]);
    setCloseOnExec(fds[1]);
    setNonblocking(fds[0]);
    setNonblocking(fds[1]);
#endif

    notifyRead = fds[0];
    notifyWrite = fds[1];
#endif
}

NotifyWake::~NotifyWake()
{
#ifdef HAVE_EVENTFD
    if (notifyEvent != -1) {
        interruptLoop(::close, notifyEvent);
    }
#else
    if (notifyRead != -1) {
        interruptLoop(::close, notifyRead);
    }
    if (notifyWrite != -1) {
        interruptLoop(::close, notifyWrite);
    }
#endif
}

bool NotifyWake::setNonblocking(int fd)
{
    int flags;

    if ((flags = ::fcntl(fd, F_GETFL)) == -1) {
        qCWarning(log_io_pollwake) << "Can't get file flags:" << std::strerror(errno);
        return false;
    }

    flags |= O_NONBLOCK;

    if (::fcntl(fd, F_SETFL, flags) == -1) {
        qCWarning(log_io_pollwake) << "Can't set file flags:" << std::strerror(errno);
        return false;
    }

    return true;
}

bool NotifyWake::setCloseOnExec(int fd)
{
    int flags;

    if ((flags = ::fcntl(fd, F_GETFD)) == -1) {
        qCWarning(log_io_pollwake) << "Can't get file flags:" << std::strerror(errno);
        return false;
    }

    flags |= FD_CLOEXEC;

    if (::fcntl(fd, F_SETFD, flags) == -1) {
        qCWarning(log_io_pollwake) << "Can't set file flags:" << std::strerror(errno);
        return false;
    }

    return true;
}

void NotifyWake::blockSigpipe()
{
    static std::atomic_bool complete;
    if (complete.load(std::memory_order_relaxed))
        return;

    struct ::sigaction sa;
    std::memset(&sa, 0, sizeof(sa));
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = SIG_IGN;
    sa.sa_flags = 0;
    ::sigaction(SIGPIPE, &sa, NULL);

    complete.store(true, std::memory_order_relaxed);
}

bool NotifyWake::interruptLoop(const std::function<int(int fd)> &f, int fd)
{
    for (;;) {
        if (f(fd) != -1)
            return true;
        if (errno != EINTR)
            return false;
    }
}

void NotifyWake::wake()
{
#ifdef HAVE_EVENTFD
    ::eventfd_write(notifyEvent, 1);
#else
    char c = 1;
    ::write(notifyWrite, &c, 1);
#endif
}

bool NotifyWake::clear()
{
#ifdef HAVE_EVENTFD
    if (notifyEvent == -1)
        return false;
    ::eventfd_t target;
    if (::eventfd_read(notifyEvent, &target) < 0) {
        int en = errno;
        if (en != EAGAIN && en != EINTR && en != EWOULDBLOCK) {
            qCWarning(log_io_pollwake) << "Notify eventfd read failed:" << std::strerror(en);
            return false;
        }
    }
#else
    if (notifyRead == -1)
        return false;
    char dummy[64];
    if (::read(notifyRead, dummy, sizeof(dummy)) < 0) {
        int en = errno;
        if (en != EAGAIN && en != EINTR && en != EWOULDBLOCK) {
            qCWarning(log_io_pollwake) << "Notify pipe read failed:" << std::strerror(en);
            return false;
        }
    }
#endif
    return true;
}

bool NotifyWake::isOk() const
{
#ifdef HAVE_EVENTFD
    return notifyEvent != -1;
#else
    return notifyRead != -1 && notifyWrite != -1;
#endif
}

void NotifyWake::install(struct ::pollfd &target)
{
#ifdef HAVE_EVENTFD
    target.fd = notifyEvent;
#else
    target.fd = notifyRead;
#endif
    target.events = POLLIN;
}

#elif defined(Q_OS_WIN32)

NotifyWake::NotifyWake()
{
    notifyHandle = ::CreateEvent(0, TRUE, 0, 0);
    if (notifyHandle == NULL) {
        qCDebug(log_io_pollwake) << "Failed to create signal event:" << getWindowsError();
        return;
    }
}

NotifyWake::~NotifyWake()
{
    if (notifyHandle == NULL)
        return;
    ::CloseHandle(notifyHandle);
}

std::string NotifyWake::getWindowsError()
{
    DWORD code = ::GetLastError();
    LPTSTR winStr = NULL;
    ::FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                             FORMAT_MESSAGE_FROM_SYSTEM |
                             FORMAT_MESSAGE_IGNORE_INSERTS, NULL, code,
                     MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) & winStr, 0, NULL);
    QString result(QString::fromWCharArray(winStr));
    ::LocalFree(winStr);
    return result.toStdString();
}

void NotifyWake::wake()
{
    if (notifyHandle == NULL)
        return;
    ::SetEvent(notifyHandle);
}

bool NotifyWake::clear()
{
    if (notifyHandle == NULL)
        return false;
    ::ResetEvent(notifyHandle);
    return true;
}

bool NotifyWake::isOk() const
{ return notifyHandle != NULL; }

void NotifyWake::install(HANDLE &target)
{ target = notifyHandle; }

#endif

}
}