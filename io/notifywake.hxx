/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3IO_POLLWAKE_HXX
#define CPD3IO_POLLWAKE_HXX

#include "core/first.hxx"

#if defined(Q_OS_UNIX)

#include <functional>
#include <poll.h>

#elif defined(Q_OS_WIN32)

#include <Windows.h>

#include <QString>

#endif

#include "io.hxx"

namespace CPD3 {
namespace IO {

#if defined(Q_OS_UNIX)

#ifdef Q_OS_LINUX
#define HAVE_EVENTFD
#endif

/**
 * An implementation of a utility to wake from a poll(2) call.
 */
class CPD3IO_EXPORT NotifyWake final {
#ifdef HAVE_EVENTFD
    int notifyEvent;
#else
    int notifyRead;
    int notifyWrite;
#endif

public:
    NotifyWake();

    ~NotifyWake();

    /**
     * Test if the wake handler is operational.
     *
     * @return true if the handler is operational
     */
    bool isOk() const;

    /**
     * Set a file to non-blocking mode.
     *
     * @param fd    the file
     * @return      true on success
     */
    static bool setNonblocking(int fd);

    /**
     * Set a file to close on exec mode.
     *
     * @param fd    the file
     * @return      true on success
     */
    static bool setCloseOnExec(int fd);

    /**
     * Block SIGPIPE.
     */
    static void blockSigpipe();

    /**
     * Run a function in an interrupt loop until it returns
     * @param f     the function to call
     * @param fd    the file to call it with
     * @return true on success
     */
    static bool interruptLoop(const std::function<int(int fd)> &f, int fd);

    /**
     * Notify of a wakeup event pending.
     */
    void wake();

    /** @see wake() */
    inline void operator()()
    { return wake(); }

    /**
     * Service the backend, clearing a pending wake.
     *
     * @return true on success or false on error
     */
    bool clear();

    /**
     * Install the poll object on a poll set fd handler.
     *
     * @param target    the target handler
     */
    void install(struct ::pollfd &target);
};

#elif defined(Q_OS_WIN32)

/**
 * An implementation of a utility to wake from a WaitForMultipleObjects call.
 */
class CPD3IO_EXPORT NotifyWake final {
    HANDLE notifyHandle;
public:
    NotifyWake();

    ~NotifyWake();

    /**
     * Test if the wake handler is operational.
     *
     * @return true if the handler is operational
     */
    bool isOk() const;

    /**
     * Notify of a wakeup event pending.
     */
    void wake();

    /** @see wake() */
    inline void operator()()
    { return wake(); }

    /**
     * Service the backend, clearing a pending wake.
     *
     * @return true on success or false on error
     */
    bool clear();

    /**
     * Install the notifuaction handler on a wait target.
     *
     * @param target    the target handler
     */
    void install(HANDLE &target);

    /**
     * Get the last Windows error as a QString.
     *
     * @return  the last Windows error
     */
    static std::string getWindowsError();
};

#else

/**
 * A non-supported stub.
 */
class CPD3IO_EXPORT NotifyWake final {
public:
    inline void wake()
    { }

    inline void operator()()
    { }
};

#endif

}
}

#endif //CPD3IO_POLLWAKE_HXX
