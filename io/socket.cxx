/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <QThread>

#include "socket.hxx"
#include "core/threading.hxx"


namespace CPD3 {
namespace IO {
namespace Socket {

Connection::Connection() = default;

Connection::~Connection() = default;

/*
 * Since we require that the device outlives the connection, we can safely do queueing that
 * keeps reference to the wrapper directly.
 */

Connection::QtConnection::~QtConnection() = default;

Connection::QtConnection::QtConnection(QAbstractSocket &socket) : wrapper(socket), device(socket)
{
    read = wrapper.read;
    ended = wrapper.ended;
    writeStall = wrapper.writeStall;
}

Connection::QtConnection::QtConnection(QLocalSocket &socket) : wrapper(socket), device(socket)
{
    read = wrapper.read;
    ended = wrapper.ended;
    writeStall = wrapper.writeStall;
}

void Connection::QtConnection::transferredWrite(Util::ByteArray &&data)
{
    struct Local {
        QIOWrapper &wrapper;
        Util::ByteArray data;

        Local(QIOWrapper &wrapper, Util::ByteArray &&data) : wrapper(wrapper), data(std::move(data))
        { }
    };

    auto local = std::make_shared<Local>(wrapper, std::move(data));
    Threading::runQueuedFunctor(&device, [local]() {
        local->wrapper.write(std::move(local->data));
        local->wrapper.processWriteBuffer();
    });
}

void Connection::QtConnection::write(const Util::ByteView &data)
{ transferredWrite(Util::ByteArray(data)); }

void Connection::QtConnection::write(Util::ByteArray &&data)
{ transferredWrite(std::move(data)); }

void Connection::QtConnection::start()
{ Threading::runQueuedFunctor(&device, std::bind(&QIOWrapper::start, &wrapper)); }

bool Connection::QtConnection::isEnded() const
{
    if (device.thread() == QThread::currentThread())
        return wrapper.isEnded();
    bool result = false;
    Threading::runBlockingFunctor(&device, [this, &result]() {
        result = wrapper.isEnded();
    });
    return result;
}

void Connection::QtConnection::readStall(bool enable)
{ Threading::runQueuedFunctor(&device, std::bind(&QIOWrapper::readStall, &wrapper, enable)); }

void Connection::QtConnection::destroy()
{ wrapper.destroy(); }


Server::Server(const IncomingConnection &)
{ }

Server::~Server() = default;


void queuedResolve(const std::string &name,
                   QObject *context,
                   const std::function<void(const QHostInfo &host)> &call)
{
    QString str = QString::fromStdString(name);
    {
        QHostAddress raw(str);
        if (!raw.isNull()) {
            QHostInfo info;
            info.setAddresses({raw});
            Threading::runQueuedFunctor(context, std::bind(call, info));
            return;
        }
    }

    /* Have to do the start as a queued call so that the caller can be
     * created on the correct thread: we can't just do a moveToThread then setParent
     * because setParent isn't thread safe and accesses the parent data. */
    Threading::runQueuedFunctor(context, [str, context, call]() {
        auto caller = new Internal::QueuedResolveCaller(context, call);
        QHostInfo::lookupHost(str, caller, SLOT(resultReady(
                                                        const QHostInfo &)));
    });
}

namespace Internal {
QueuedResolveCaller::QueuedResolveCaller(QObject *context,
                                         const std::function<void(const QHostInfo &host)> &call)
        : QObject(context), call(call)
{ }

QueuedResolveCaller::~QueuedResolveCaller() = default;

void QueuedResolveCaller::resultReady(const QHostInfo &host)
{
    call(host);
    return deleteLater();
}
}

}
}
}