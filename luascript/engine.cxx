/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "engine.hxx"

#include <limits>
#include <QLoggingCategory>

#include "core/number.hxx"
#include "core/qtcompat.hxx"

#include "libs/time.hxx"
#include "libs/algorithms.hxx"
#include "libs/variant.hxx"
#include "libs/sequencename.hxx"
#include "libs/sequenceidentity.hxx"
#include "libs/streamvalue.hxx"
#include "libs/valuesegment.hxx"
#include "libs/sequencesegment.hxx"
#include "libs/archive.hxx"
#include "libs/csv.hxx"
#include "libs/segmentation.hxx"
#include "libs/regex.hxx"

extern "C" {
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}



Q_LOGGING_CATEGORY(log_luascript_engine, "cpd3.luascript.engine", QtWarningMsg)

Q_LOGGING_CATEGORY(log_luascript_engine_error, "cpd3.luascript.engine.error", QtWarningMsg)

namespace CPD3 {
namespace Lua {

static const std::string emptyString;
static const std::string sandboxRegistryName = "CPD3_engine_sandbox";
static const std::string dataGCRegistryName = "CPD3_engine_datagc";
static const std::string dataMetatableRegistryName = "CPD3_engine_datametatable";

class Engine::Context {
    friend class Engine::Reference;

    friend class Engine::Table;

    friend class Engine::Frame;

    friend class Engine::Call;

    friend class Engine::Assign;

    friend class Engine::Append;

    friend class Engine::Iterator;

    Engine &engine;
    ::lua_State *L;

    enum class ErrorState {
        Clear, PendingLUACatch, PendingLUAThrow, CaughtFromLUA, CaughtFromProtected,
    } errorState;
    std::string errorString;

    struct Entry {
        std::size_t depth;
        Frame *active;
        bool fromLUA;

        Entry() : depth(0), active(nullptr), fromLUA(false)
        { }

        explicit Entry(Frame *active, bool fromLUA) : depth(1), active(active), fromLUA(fromLUA)
        { }
    };

    std::vector<Entry> entries;

    bool entryFromLUA() const
    {
        if (entries.empty())
            return false;
        return entries.back().fromLUA;
    }

    void handleError(bool fromProtected = false)
    {
        if (errorState == ErrorState::PendingLUACatch && !fromProtected) {
            errorState = ErrorState::CaughtFromLUA;
            if (::lua_gettop(L) >= 1) {
                ::size_t length = 0;
                const char *ptr = ::lua_tolstring(L, -1, &length);
                if (ptr && length) {
                    errorString = std::string(ptr, ptr + length);
                } else {
                    errorString = "Unknown or internal error";
                }

                ::lua_pop(L, 1);
            }
            return;
        }
        if (::lua_gettop(L) < 1) {
            switch (errorState) {
            case ErrorState::Clear:
                if (entryFromLUA()) {
                    errorState = ErrorState::PendingLUAThrow;
                    if (fromProtected) {
                        errorString = "Unknown or internal error in protected mode";
                        qCDebug(log_luascript_engine_error)
                            << "Lua protected error with no stack contents";
                    } else {
                        errorString = "Unknown or internal error";
                        qCDebug(log_luascript_engine_error) << "Lua error with no stack contents";
                    }
                } else {
                    if (fromProtected) {
                        errorState = ErrorState::CaughtFromProtected;
                        qCDebug(log_luascript_engine_error)
                            << "Protected error with no stack contents";
                    } else {
                        errorState = ErrorState::CaughtFromLUA;
                        qCDebug(log_luascript_engine_error)
                            << "Lua call error with no stack contents";
                    }
                }
                break;
            case ErrorState::PendingLUACatch:
            case ErrorState::PendingLUAThrow:
            case ErrorState::CaughtFromLUA:
            case ErrorState::CaughtFromProtected:
                break;
            }
        } else {
            switch (errorState) {
            case ErrorState::Clear: {
                ::size_t length = 0;
                const char *ptr = ::lua_tolstring(L, -1, &length);
                if (ptr && length) {
                    errorString = std::string(ptr, ptr + length);
                } else {
                    errorString = "Unknown or internal error";
                }
                if (entryFromLUA()) {
                    errorState = ErrorState::PendingLUAThrow;
                    if (fromProtected) {
                        qCDebug(log_luascript_engine_error) << "Protected mode from Lua error:"
                                                            << errorString;
                    } else {
                        qCDebug(log_luascript_engine_error) << "Lua reentry error:" << errorString;
                    }
                } else {
                    if (fromProtected) {
                        errorState = ErrorState::CaughtFromProtected;
                        qCDebug(log_luascript_engine_error) << "Protected mode error:"
                                                            << errorString;
                    } else {
                        errorState = ErrorState::CaughtFromLUA;
                        qCDebug(log_luascript_engine_error) << "Lua error:" << errorString;
                    }
                }
                break;
            }
            case ErrorState::PendingLUACatch:
            case ErrorState::PendingLUAThrow:
            case ErrorState::CaughtFromLUA:
            case ErrorState::CaughtFromProtected:
                break;
            }

            ::lua_pop(L, 1);
        }
    }


    void createSandboxEnvironment()
    {
        Frame frame(this);
        auto env = pushSandboxEnvironment(frame);
        frame.registry().set(sandboxRegistryName, env);
    }

    void createDataGC()
    {
        struct Local {
            static int destructData(::lua_State *L)
            {
                int n = ::lua_gettop(L);
                if (n < 1)
                    return 0;
                if (::lua_type(L, 1) == LUA_TLIGHTUSERDATA)
                    return 0;
                void *ptr = ::lua_touserdata(L, 1);
                if (!ptr)
                    return 0;
                auto base = reinterpret_cast<BaseData *>(ptr);
                auto check = dynamic_cast<Data *>(base);
                if (!check)
                    return 0;
                check->~Data();
                /*::lua_pushnil(L);
                ::lua_setmetatable(L, 1);*/
                return 0;
            }
        };
        Frame frame(this);
        ::lua_pushcclosure(L, Local::destructData, 0);
        frame.pushed(1);
        auto gc = frame.back();
        frame.registry().set(dataGCRegistryName, gc);

        auto env = frame.pushTable();
        {
            Assign assign(frame, env, "__gc");
            assign.push(gc);
        }
        {
            Assign assign(frame, env, "__metatable");
            assign.pushTable();
        }

        frame.registry().set(dataMetatableRegistryName, env);
    }

    void openCPD3Libs()
    {
        Frame base(this);
        auto target = base.pushTable();
        base.global().set("CPD3", target);

        target.set("undefined", Output(FP::undefined()));
        {
            Call c(base);
            if (!c.pushChunk(R"EOF(
CPD3.defined = function(v)
    v = tonumber(v);
    if not v then return false; end
    local l = math.huge;
    return v > -l and v < l;
end
)EOF", false)) {
                qCWarning(log_luascript_engine) << "Error initializing CPD3.defined: "
                                                << errorDescription();
                clearError();
            } else if (!c.execute()) {
                qCWarning(log_luascript_engine) << "Error initializing CPD3.defined: "
                                                << errorDescription();
                clearError();
            }
        }

        {
            Frame f(base);
            Libs::Time::install(target, f);
        }
        {
            Frame f(base);
            Libs::Algorithms::install(target, f);
        }
        {
            Frame f(base);
            Libs::Variant::install(target, f);
        }
        {
            Frame f(base);
            Libs::SequenceName::install(target, f);
        }
        {
            Frame f(base);
            Libs::SequenceIdentity::install(target, f);
        }
        {
            Frame f(base);
            Libs::SequenceValue::install(target, f);
        }
        {
            Frame f(base);
            Libs::ArchiveValue::install(target, f);
        }
        {
            Frame f(base);
            Libs::ArchiveErasure::install(target, f);
        }
        {
            Frame f(base);
            Libs::ValueSegment::install(target, f);
        }
        {
            Frame f(base);
            Libs::SequenceSegment::install(target, f);
        }
        {
            Frame f(base);
            Libs::Archive::install(target, f);
        }
        {
            Frame f(base);
            Libs::CSV::install(target, f);
        }
        {
            Frame f(base);
            Libs::Segmentation::install(target, f);
        }
        {
            Frame f(base);
            Libs::Regex::install(target, f);
        }
    }

public:
    explicit Context(Engine &engine) : engine(engine), L(nullptr), errorState(ErrorState::Clear)
    {
        L = ::luaL_newstate();
        if (!L) {
            qFatal("Error creating Lua state: %s", ::strerror(errno));
            errorState = ErrorState::CaughtFromLUA;
            return;
        }
        ::luaL_openlibs(L);

        /* Don't let script code create user data objects */
        {
            Engine::Frame frame(this);
            frame.global().erase("newproxy");
        }

        createDataGC();
        openCPD3Libs();
        createSandboxEnvironment();
    }

    ~Context()
    {
        ::lua_close(L);
    }


    /* A bit complicated because we can't depend on exception safety, even in
     * LuaJIT since it may not be supported (i.e. LLVM).  So we have to call through
     * Lua protected mode, which for passing arguments means making copies of them. */

    void luaProtected(const std::function<void(::lua_State * )> &call)
    {
        struct Local {
            static int execute(::lua_State *L)
            {
                Q_ASSERT(::lua_gettop(L) == 1);
                void *ptr = ::lua_touserdata(L, 1);
                Q_ASSERT(ptr);
                ::lua_settop(L, 0);
                auto callable = reinterpret_cast<const std::function<void(::lua_State * )> *>(ptr);
                (*callable)(L);
                return 0;
            }
        };
        if (::lua_cpcall(L, Local::execute,
                         const_cast<void *>(reinterpret_cast<const void *>(&call)))) {
            handleError(true);
            return;
        } else {
            luaExitNoError();
        }
    }

    void luaProtected(const std::function<void(::lua_State * )> &call,
                      const std::vector<Reference> &parameters,
                      int results = 0)
    {
        struct Local {
            static int execute(::lua_State *L)
            {
                int n = ::lua_gettop(L);
                Q_ASSERT(n >= 1);
                void *ptr = ::lua_touserdata(L, -1);
                Q_ASSERT(ptr);
                ::lua_settop(L, n - 1);
                auto callable = reinterpret_cast<const std::function<void(::lua_State * )> *>(ptr);
                (*callable)(L);
                return ::lua_gettop(L);
            }
        };

        ::lua_checkstack(L, static_cast<int>(parameters.size() + 2));
        ::lua_pushcclosure(L, Local::execute, 0);
        for (const auto &ref : parameters) {
            Q_ASSERT(
                    ref.index != 0 && (ref.index < 0 || ref.index <= ::lua_gettop(ref.context->L)));
            ::lua_pushvalue(L, ref.index);
        }
        ::lua_pushlightuserdata(L, const_cast<void *>(reinterpret_cast<const void *>(&call)));
        if (::lua_pcall(L, static_cast<int>(parameters.size() + 1), static_cast<int>(results), 0)) {
            handleError(true);
            if (results != LUA_MULTRET) {
                ::lua_settop(L, static_cast<int>(::lua_gettop(L) + results));
            }
        } else {
            luaExitNoError();
        }
    }

    Table pushSandboxEnvironment(Frame &frame)
    {
        auto env = frame.pushTable();

        static const std::vector<std::string> fullCopy
                {"ipairs", "next", "pairs", "pcall", "tonumber", "tostring", "type", "unpack",
                 "error"};
        static const std::vector<std::pair<std::string, std::vector<std::string>>> subCopy
                {{"coroutine", {"create",    "resume",   "running", "status",     "wrap"}},
                 {"string",    {"byte",      "char",     "find",    "format",     "gmatch",  "gsub",         "len",              "lower",         "match",        "rep",             "reverse", "sub",          "upper"}},
                 {"table",     {"insert",    "maxn",     "remove",  "sort"}},
                 {"math",      {"abs",       "acos",     "asin",    "atan",       "atan2",   "ceil",         "cos",              "cosh",          "deg",          "exp",             "floor",   "fmod",         "frexp", "huge", "ldexp", "log", "log10", "max", "min", "modf", "pi", "pow", "rad", "random", "sin", "sinh", "sqrt", "tan", "tanh"}},
                 {"os",        {"clock",     "difftime", "time"}},

                 {"CPD3",      {"undefined", "defined",  "Time",    "Algorithms", "Variant", "SequenceName", "SequenceIdentity", "SequenceValue", "ValueSegment", "SequenceSegment", "CSV",     "Segmentation", "Regex"}}};

        auto globals = frame.global();
        for (const auto &c : fullCopy) {
            Assign assign(frame, env, c);
            assign.push(globals, c);
        }

        for (const auto &t : subCopy) {
            Assign assign(frame, env, t.first);
            auto targetTable = assign.pushTable();
            assign.push(globals, t.first);
            Table sourceTable(assign.back());
            for (const auto &c : t.second) {
                Assign copyAssign(assign, targetTable, c);
                copyAssign.push(sourceTable, c);
            }
            assign.pop();
        }

        return env;
    }

    void setDataMetatable(const Reference &self, const Table &table)
    {
        Q_ASSERT(self.context == this);
        Q_ASSERT(table.ref.context == this);

        Frame frame(this);

#ifndef NDEBUG
        {
            Frame check(frame);
            check.push(table, "__gc");
            auto tableGC = check.back();
            check.push(check.registry(), dataGCRegistryName);
            auto expectedGC = check.back();
            Q_ASSERT(expectedGC.rawEqual(tableGC));
        }
        {
            Frame check(frame);
            check.push(table, "__metatable");
            Q_ASSERT(check.back().getType() == Reference::Type::Table);
        }
#endif

        frame.push(table);
        ::lua_setmetatable(L, self.index);
        frame.popped(1);
    }

    static int basicEntry(::lua_State *L);

    class BasicEntryData : public Engine::Data {
        Context *ctx;
        std::function<void(Engine::Entry &)> call;
    public:
        explicit BasicEntryData(Context *context, std::function<void(Engine::Entry &)> &&call)
                : ctx(context), call(std::move(call))
        { }

        virtual ~BasicEntryData() = default;

        inline Context *context() const
        { return ctx; }

        inline void invoke(Engine::Entry &entry)
        { call(entry); }
    };

    static int referenceEntry(::lua_State *L);

    class ReferenceEntryData : public Engine::Data {
        Context *ctx;
        std::function<void(Engine::Entry &, const std::vector<Reference> &)> call;
        std::size_t refs;
    public:
        explicit ReferenceEntryData(Context *context,
                                    std::function<void(Engine::Entry &,
                                                       const std::vector<Reference> &)> &&call,
                                    std::size_t references) : ctx(context),
                                                              call(std::move(call)),
                                                              refs(references)
        { }

        virtual ~ReferenceEntryData() = default;

        inline Context *context() const
        { return ctx; }

        inline std::size_t references() const
        { return refs; }

        inline void invoke(Engine::Entry &entry, const std::vector<Reference> &r)
        { call(entry, r); }
    };

    inline bool inError() const
    { return errorState != ErrorState::Clear; }

    inline const std::string errorDescription() const
    { return errorString; }

    void throwError(const std::string &message)
    {
        switch (errorState) {
        case ErrorState::Clear: {
            errorString = message;
            if (entryFromLUA()) {
                errorState = Context::ErrorState::PendingLUAThrow;
                qCDebug(log_luascript_engine_error) << "Internal error in Lua call:" << message;
            } else {
                errorState = Context::ErrorState::CaughtFromProtected;
                qCDebug(log_luascript_engine_error) << "Internal error:" << message;
            }
            break;
        }
        case Context::ErrorState::PendingLUACatch:
        case Context::ErrorState::PendingLUAThrow:
        case Context::ErrorState::CaughtFromLUA:
        case Context::ErrorState::CaughtFromProtected:
            break;
        }
    }

    void clearError()
    { errorState = ErrorState::Clear; }

    void luaEntry()
    {
        /* A re-entry like this means the Lua code caught the error (i.e. a pcall), so just
         * clear it */
        if (errorState == ErrorState::PendingLUACatch) {
            errorState = ErrorState::Clear;
        }
    }

    void luaExitNoError()
    {
        /* An exit like this means the Lua code caught the error (i.e. a pcall), so just
         * clear it */
        if (errorState == ErrorState::PendingLUACatch) {
            errorState = ErrorState::Clear;
        }
    }

    static void debug(QDebug &debug, const Output &output)
    {
        debug << "Lua::Output(";
        switch (output.getType()) {
        case Output::Type::Nil:
            debug << "nil";
            break;
        case Output::Type::Data:
            debug << reinterpret_cast<const void *>(output.contents.ref<BaseData *>());
            break;
        case Output::Type::Number:
            debug << output.toInteger();
            break;
        case Output::Type::String:
            debug << output.toString();
            break;
        case Output::Type::Boolean:
            debug << output.toBoolean();
            break;
        }
        debug << ')';
    }

    static void debug(QDebug &debug, const Reference &ref)
    {
        debug << "Lua::Reference(";
        switch (ref.getType()) {
        case Reference::Type::Nil:
            debug << "nil";
            break;
        case Reference::Type::Boolean:
            debug << ref.toBoolean();
            break;
        case Reference::Type::Data:
            debug << reinterpret_cast<const void *>(::lua_topointer(ref.context->L, ref.index));
            break;
        case Reference::Type::Number:
            debug << ref.toInteger();
            break;
        case Reference::Type::String:
            debug << ref.toString();
            break;
        case Reference::Type::Table:
            debug << "table@"
                  << reinterpret_cast<const void *>(::lua_topointer(ref.context->L, ref.index));
            break;
        }
        debug << ')';
    }

    static void debug(QDebug &debug, const Table &table)
    {
        debug << "Lua::Table(";
        if (table.ref.getType() != Reference::Type::Table) {
            debug << "INVALID)";
            return;
        }
        {
            Engine::Frame local(table.ref.context);
            Engine::Iterator it(local, table.ref);
            while (it.next()) {
                debug << '{' << it.key() << ',' << it.value() << '}';
            }
        }
        debug << ')';
    }
};

int Engine::Context::basicEntry(::lua_State *L)
{
    int eref = lua_upvalueindex(1);
    if (::lua_type(L, eref) == LUA_TLIGHTUSERDATA) {
        ::lua_pushstring(L, "Invalid entry context");
        ::lua_error(L);
        return 0;
    }
    void *ptr = ::lua_touserdata(L, eref);
    if (!ptr) {
        ::lua_pushstring(L, "No entry context");
        ::lua_error(L);
        return 0;
    }
    auto base = reinterpret_cast<BaseData *>(ptr);
    auto edata = dynamic_cast<BasicEntryData *>(base);
    if (!edata) {
        ::lua_pushstring(L, "Corrupt entry context");
        ::lua_error(L);
        return 0;
    }
    auto context = edata->context();

    {
        context->luaEntry();

        Engine::Frame frame(context, true);
        Q_ASSERT(frame.origin == 0);
        {
            Engine::Entry entry(frame);
            /* All arguments on the stack */
            entry.origin = 0;

            edata->invoke(entry);

            /* Handle returns (don't pop them on frame destruct) */
            frame.origin = entry.origin;
        }
    }

    /* Can longjump now, so no complex types permitted */
    if (context->errorState == ErrorState::PendingLUAThrow) {
        context->errorState = ErrorState::PendingLUACatch;
        ::lua_settop(L, 0);
        ::lua_pushlstring(L, context->errorString.c_str(),
                          static_cast<::size_t>(context->errorString.size()));
        ::lua_error(L);
        /* Unreachable */
        Q_ASSERT(false);
        return 0;
    }

    return ::lua_gettop(L);
}

int Engine::Context::referenceEntry(::lua_State *L)
{
    int eref = lua_upvalueindex(1);
    if (::lua_type(L, eref) == LUA_TLIGHTUSERDATA) {
        ::lua_pushstring(L, "Invalid entry context");
        ::lua_error(L);
        return 0;
    }
    void *ptr = ::lua_touserdata(L, eref);
    if (!ptr) {
        ::lua_pushstring(L, "No entry context");
        ::lua_error(L);
        return 0;
    }
    auto base = reinterpret_cast<BaseData *>(ptr);
    auto edata = dynamic_cast<ReferenceEntryData *>(base);
    if (!edata) {
        ::lua_pushstring(L, "Corrupt entry context");
        ::lua_error(L);
        return 0;
    }
    auto context = edata->context();

    {
        context->luaEntry();

        Engine::Frame frame(context, true);
        Q_ASSERT(frame.origin == 0);
        {
            Engine::Entry entry(frame);
            /* All arguments on the stack */
            entry.origin = 0;

            {
                std::vector<Reference> refs;
                for (std::size_t i = 0; i < edata->references(); i++) {
                    refs.emplace_back(
                            Reference(context, static_cast<int>(lua_upvalueindex(2 + i))));
                }
                edata->invoke(entry, refs);
            }

            /* Handle returns (don't pop them on frame destruct) */
            frame.origin = entry.origin;
        }
    }

    /* Can longjump now, so no complex types permitted */
    if (context->errorState == ErrorState::PendingLUAThrow) {
        context->errorState = ErrorState::PendingLUACatch;
        ::lua_settop(L, 0);
        ::lua_pushlstring(L, context->errorString.c_str(),
                          static_cast<::size_t>(context->errorString.size()));
        ::lua_error(L);
        /* Unreachable */
        Q_ASSERT(false);
        return 0;
    }

    return ::lua_gettop(L);
}


Engine::Engine() : context(new Context(*this))
{ }

Engine::~Engine() = default;

Engine::Table Engine::global() const
{ return Table(Reference(context.get(), LUA_GLOBALSINDEX)); }

Engine::Table Engine::registry() const
{ return Table(Reference(context.get(), LUA_REGISTRYINDEX)); }

bool Engine::inError() const
{ return context->inError(); }

std::string Engine::errorDescription() const
{ return context->errorDescription(); }

void Engine::throwError(const std::string &message)
{ context->throwError(message); }

void Engine::clearError()
{ context->clearError(); }


Engine::BaseData::BaseData() = default;

Engine::BaseData::~BaseData() = default;

void Engine::BaseData::initialize(const Reference &, Frame &)
{ }

Engine::Data::Data() = default;

Engine::Data::~Data() = default;

void Engine::Data::setMetatable(const Reference &self, const Table &table)
{ self.context->setDataMetatable(self, table); }

Engine::Table Engine::Data::pushMetatable(Frame &frame)
{
    auto mt = frame.pushTable();
    {
        Assign assign(frame, mt, "__gc");
        assign.push(assign.registry(), dataGCRegistryName);
    }
    {
        Assign assign(frame, mt, "__metatable");
        assign.pushTable();
    }
    return std::move(mt);
}

bool Engine::Data::methodIndexHandler(Entry &entry, const Table &methods)
{
    Q_ASSERT(entry.size() >= 2);

    entry.pushRaw(methods, entry[1]);
    if (entry.back().isNil()) {
        entry.pop();
        return false;
    }
    entry.replaceWithBack(0);
    entry.propagate(1);
    return true;
}


Engine::PersistentData::PersistentData() = default;

Engine::PersistentData::~PersistentData() = default;

void Engine::PersistentData::ready(Engine &engine)
{
    Frame frame(engine.context.get());
    frame.push(this);
    auto self = frame.back();
    initialize(self, frame);
}

void Engine::PersistentData::ready(Frame &current)
{
    Frame frame(current);
    frame.push(this);
    auto self = frame.back();
    initialize(self, frame);
}


Engine::Reference::Reference() noexcept : context(nullptr), index(0)
{ }

Engine::Reference::Reference(Context *context, int index) : context(context), index(index)
{ }

Engine::Output Engine::Reference::get() const
{
    Q_ASSERT(context);
    Q_ASSERT(index != 0 && (index < 0 || index <= ::lua_gettop(context->L)));

    /* The conversion functions shouldn't throw (i.e. they don't call metamethods) so
     * directly calling in non-protected mode should be safe */

    switch (::lua_type(context->L, index)) {
    case LUA_TNIL:
        return Output();
    case LUA_TNUMBER:
        return Output(toNumber());
    case LUA_TBOOLEAN:
        return Output(toBoolean());
    case LUA_TSTRING:
        return Output(toString());
    case LUA_TUSERDATA: {
        /* Do not access through an detatched output */
        Output result;
        result.type = Output::Type::Data;
        result.contents.construct<BaseData *>(nullptr);
        return std::move(result);
    }
    case LUA_TLIGHTUSERDATA: {
        Output result;
        result.type = Output::Type::Data;
        result.contents.construct<BaseData *>(toData<PersistentData>());
        return std::move(result);
    }
    default:
        break;
    }

    return Engine::Output();
}

Engine::Reference::Type Engine::Reference::getType() const
{
    Q_ASSERT(context);
    Q_ASSERT(index != 0 && (index < 0 || index <= ::lua_gettop(context->L)));

    switch (::lua_type(context->L, index)) {
    case LUA_TNIL:
        return Engine::Reference::Type::Nil;
    case LUA_TNUMBER:
        return Engine::Reference::Type::Number;
    case LUA_TBOOLEAN:
        return Engine::Reference::Type::Boolean;
    case LUA_TSTRING:
        return Engine::Reference::Type::String;
    case LUA_TTABLE:
        return Engine::Reference::Type::Table;
    case LUA_TUSERDATA:
    case LUA_TLIGHTUSERDATA:
        return Engine::Reference::Type::Data;
    default:
        break;
    }
    return Engine::Reference::Type::Nil;
}

bool Engine::Reference::isNil() const
{
    Q_ASSERT(context);
    Q_ASSERT(index != 0 && (index < 0 || index <= ::lua_gettop(context->L)));

    return ::lua_type(context->L, index) == LUA_TNIL;
}

std::int_fast64_t Engine::Reference::toInteger() const
{
    Q_ASSERT(context);
    Q_ASSERT(index != 0 && (index < 0 || index <= ::lua_gettop(context->L)));

    if (!::lua_isnumber(context->L, index))
        return INTEGER::undefined();
    double v = ::lua_tonumber(context->L, index);
    if (!FP::defined(v))
        return INTEGER::undefined();
    return static_cast<std::int_fast64_t>(v);
}

double Engine::Reference::toNumber() const
{
    Q_ASSERT(context);
    Q_ASSERT(index != 0 && (index < 0 || index <= ::lua_gettop(context->L)));

    if (!::lua_isnumber(context->L, index))
        return FP::undefined();
    return static_cast<double>(::lua_tonumber(context->L, index));
}

std::string Engine::Reference::toString() const
{
    Q_ASSERT(context);
    Q_ASSERT(index != 0 && (index < 0 || index <= ::lua_gettop(context->L)));

    if (!::lua_isstring(context->L, index))
        return std::string();
    ::size_t length = 0;
    const char *ptr = ::lua_tolstring(context->L, index, &length);
    if (ptr && length)
        return std::string(ptr, ptr + length);
    return std::string();
}

QString Engine::Reference::toQString() const
{ return QString::fromStdString(toString()); }

std::string Engine::Reference::toOutputString() const
{
    Q_ASSERT(context);
    Q_ASSERT(index != 0 && (index < 0 || index <= ::lua_gettop(context->L)));

    if (context->inError())
        return std::string();

    Engine::Frame local(context);
    Engine::Call call(local);
    call.push(call.global(), "tostring");
    call.push(*this);
    if (!call.execute(1)) {
        std::string result = "ERROR:";
        result += call.errorDescription();
        call.clearError();
        return std::move(result);
    }
    return call.back().toString();
}

bool Engine::Reference::toBoolean() const
{
    Q_ASSERT(context);
    Q_ASSERT(index != 0 && (index < 0 || index <= ::lua_gettop(context->L)));
    return ::lua_toboolean(context->L, index) != 0;
}

void *Engine::Reference::toUserData() const
{
    Q_ASSERT(context);
    Q_ASSERT(index != 0 && (index < 0 || index <= ::lua_gettop(context->L)));
    return ::lua_touserdata(context->L, index);
}

bool Engine::Reference::rawEqual(const Reference &other) const
{
    Q_ASSERT(context);
    Q_ASSERT(index != 0 && (index < 0 || index <= ::lua_gettop(context->L)));
    return ::lua_rawequal(context->L, index, other.index) != 0;
}

bool Engine::Reference::operator==(const Reference &other) const
{
    Q_ASSERT(context);
    bool result = false;
    context->luaProtected([&](::lua_State *L) {
        result = ::lua_equal(L, 1, 2) != 0;
    }, {*this, other});
    return result;
}

bool Engine::Reference::operator<(const Reference &other) const
{
    Q_ASSERT(context);
    bool result = false;
    context->luaProtected([&](::lua_State *L) {
        result = ::lua_lessthan(L, 1, 2) != 0;
    }, {*this, other});
    return result;
}


Engine::Output::Output() : type(Type::Nil)
{ }

Engine::Output::~Output()
{
    switch (type) {
    case Type::Nil:
    case Type::Data:
    case Type::Number:
    case Type::Boolean:
        break;
    case Type::String: {
        contents.destruct<std::string>();
        break;
    }
    }
}

Engine::Output::Output(const Output &other) : type(other.type)
{
    switch (type) {
    case Type::Nil:
        break;
    case Type::Data:
        contents.construct<BaseData *>(other.contents.ref<BaseData *>());
        break;
    case Type::Number:
        contents.construct<double>(other.contents.ref<double>());
        break;
    case Type::String:
        contents.construct<std::string>(other.contents.ref<std::string>());
        break;
    case Type::Boolean:
        contents.construct<bool>(other.contents.ref<bool>());
        break;
    }
}

Engine::Output &Engine::Output::operator=(const Output &other)
{
    if (other.type == type) {
        switch (type) {
        case Type::Nil:
            break;
        case Type::Data:
            contents.ref<BaseData *>() = other.contents.ref<BaseData *>();
            break;
        case Type::Number:
            contents.ref<double>() = other.contents.ref<double>();
            break;
        case Type::String:
            contents.ref<std::string>() = other.contents.ref<std::string>();
            break;
        case Type::Boolean:
            contents.ref<bool>() = other.contents.ref<bool>();
            break;
        }
        return *this;
    }

    Q_ASSERT(&other != this);

    switch (type) {
    case Type::Nil:
    case Type::Data:
    case Type::Number:
    case Type::Boolean:
        break;
    case Type::String: {
        contents.destruct<std::string>();
        break;
    }
    }

    type = other.type;
    switch (other.type) {
    case Type::Nil:
        break;
    case Type::Data:
        contents.construct<BaseData *>(other.contents.ref<BaseData *>());
        break;
    case Type::Number:
        contents.construct<double>(other.contents.ref<double>());
        break;
    case Type::String:
        contents.construct<std::string>(other.contents.ref<std::string>());
        break;
    case Type::Boolean:
        contents.construct<bool>(other.contents.ref<bool>());
        break;
    }
    return *this;
}

Engine::Output::Output(Output &&other) : type(other.type)
{
    switch (type) {
    case Type::Nil:
        break;
    case Type::Data:
        contents.construct<BaseData *>(std::move(other.contents.ref<BaseData *>()));
        break;
    case Type::Number:
        contents.construct<double>(std::move(other.contents.ref<double>()));
        break;
    case Type::String:
        contents.construct<std::string>(std::move(other.contents.ref<std::string>()));
        break;
    case Type::Boolean:
        contents.construct<bool>(std::move(other.contents.ref<bool>()));
        break;
    }
}

Engine::Output &Engine::Output::operator=(Output &&other)
{
    if (other.type == type) {
        switch (type) {
        case Type::Nil:
            break;
        case Type::Data:
            contents.ref<BaseData *>() = std::move(other.contents.ref<BaseData *>());
            break;
        case Type::Number:
            contents.ref<double>() = std::move(other.contents.ref<double>());
            break;
        case Type::String:
            contents.ref<std::string>() = std::move(other.contents.ref<std::string>());
            break;
        case Type::Boolean:
            contents.ref<bool>() = std::move(other.contents.ref<bool>());
            break;
        }
        return *this;
    }

    Q_ASSERT(&other != this);

    switch (type) {
    case Type::Nil:
    case Type::Data:
    case Type::Number:
    case Type::Boolean:
        break;
    case Type::String: {
        contents.destruct<std::string>();
        break;
    }
    }

    type = other.type;
    switch (other.type) {
    case Type::Nil:
        break;
    case Type::Data:
        contents.construct<BaseData *>(std::move(other.contents.ref<BaseData *>()));
        break;
    case Type::Number:
        contents.construct<double>(std::move(other.contents.ref<double>()));
        break;
    case Type::String:
        contents.construct<std::string>(std::move(other.contents.ref<std::string>()));
        break;
    case Type::Boolean:
        contents.construct<bool>(std::move(other.contents.ref<bool>()));
        break;
    }
    return *this;
}

Engine::Output::Output(double value) : type(Type::Number)
{ contents.construct<double>(value); }

Engine::Output::Output(std::int_fast64_t value) : type(Type::Number)
{
    if (!INTEGER::defined(value)) {
        contents.construct<double>(FP::undefined());
    } else {
        contents.construct<double>(value);
    }
}

Engine::Output::Output(bool value) : type(Type::Boolean)
{ contents.construct<bool>(value); }

Engine::Output::Output(const std::string &value) : type(Type::String)
{ contents.construct<std::string>(value); }

Engine::Output::Output(std::string &&value) : type(Type::String)
{ contents.construct<std::string>(std::move(value)); }

Engine::Output::Output(const char *value) : type(Type::String)
{ contents.construct<std::string>(value); }

Engine::Output::Output(const QString &value) : Output(value.toStdString())
{ }

std::int_fast64_t Engine::Output::toInteger() const
{
    double v = toNumber();
    if (!FP::defined(v))
        return INTEGER::undefined();
    return static_cast<std::int_fast64_t>(v);
}

double Engine::Output::toNumber() const
{
    switch (type) {
    case Type::Nil:
    case Type::Data:
    case Type::Boolean:
        return FP::undefined();
    case Type::String: {
        try {
            return std::stod(contents.ref<std::string>());
        } catch (std::exception &) {
        }
        return FP::undefined();
    }
    case Type::Number:
        return contents.ref<double>();
    }
    Q_ASSERT(false);
    return FP::undefined();
}

const std::string &Engine::Output::toString() const
{
    switch (type) {
    case Type::Nil:
    case Type::Data:
    case Type::Number:
    case Type::Boolean:
        return emptyString;
    case Type::String:
        return contents.ref<std::string>();
    }
    Q_ASSERT(false);
    return emptyString;
}

QString Engine::Output::toQString() const
{ return QString::fromStdString(toString()); }

bool Engine::Output::toBoolean() const
{
    switch (type) {
    case Type::Nil:
        return false;
    case Type::Data:
    case Type::Number:
    case Type::String:
        /* Conform to lua definitions that everything not false/nil is true */
        return true;
    case Type::Boolean:
        return contents.ref<bool>();
    }
    Q_ASSERT(false);
    return false;
}


void Engine::Table::set(const Output &key, const Output &value)
{
    Frame frame(ref.context);
    frame.push(key);
    frame.push(value);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_settable(L, 1);
    }, {ref, frame[0], frame[1]});
}

void Engine::Table::set(const Reference &key, const Output &value)
{
    Frame frame(ref.context);
    frame.push(value);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_settable(L, 1);
    }, {ref, key, frame[0]});
}

void Engine::Table::set(const std::string &key, const Output &value)
{
    Frame frame(ref.context);
    frame.push(key);
    frame.push(value);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_settable(L, 1);
    }, {ref, frame[0], frame[1]});
}

void Engine::Table::set(const char *key, const Output &value)
{
    Frame frame(ref.context);
    frame.push(key);
    frame.push(value);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_settable(L, 1);
    }, {ref, frame[0], frame[1]});
}

void Engine::Table::set(std::size_t key, const Output &value)
{
    Frame frame(ref.context);
    frame.push(static_cast<std::int_fast64_t>(key));
    frame.push(value);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_settable(L, 1);
    }, {ref, frame[0], frame[1]});
}

void Engine::Table::set(const Output &key, const Reference &value)
{
    Frame frame(ref.context);
    frame.push(key);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_settable(L, 1);
    }, {ref, frame.front(), value});
}

void Engine::Table::set(const Reference &key, const Reference &value)
{
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_settable(L, 1);
    }, {ref, key, value});
}

void Engine::Table::set(const std::string &key, const Reference &value)
{
    Frame frame(ref.context);
    frame.push(key);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_settable(L, 1);
    }, {ref, frame.front(), value});
}

void Engine::Table::set(const char *key, const Reference &value)
{
    Frame frame(ref.context);
    frame.push(key);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_settable(L, 1);
    }, {ref, frame.front(), value});
}

void Engine::Table::set(std::size_t key, const Reference &value)
{
    Frame frame(ref.context);
    frame.push(static_cast<std::int_fast64_t>(key));
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_settable(L, 1);
    }, {ref, frame.front(), value});
}

void Engine::Table::erase(const Output &key)
{
    Frame frame(ref.context);
    frame.push(key);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_pushnil(L);
        ::lua_settable(L, 1);
    }, {ref, frame.front()});
}

void Engine::Table::erase(const Reference &key)
{
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_pushnil(L);
        ::lua_settable(L, 1);
    }, {ref, key});
}

void Engine::Table::erase(const std::string &key)
{
    Frame frame(ref.context);
    frame.push(key);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_pushnil(L);
        ::lua_settable(L, 1);
    }, {ref, frame.front()});
}

void Engine::Table::erase(const char *key)
{
    Frame frame(ref.context);
    frame.push(key);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_pushnil(L);
        ::lua_settable(L, 1);
    }, {ref, frame.front()});
}

void Engine::Table::erase(std::size_t key)
{
    Frame frame(ref.context);
    frame.push(static_cast<std::int_fast64_t>(key));
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_pushnil(L);
        ::lua_settable(L, 1);
    }, {ref, frame.front()});
}

Engine::Output Engine::Table::get(const Output &key) const
{
    Frame frame(ref.context);
    frame.push(key);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_gettable(L, 1);
        ::lua_remove(L, 1);
    }, {ref, frame.front()}, 1);
    frame.pushed(1);
    return frame.back().get();
}

Engine::Output Engine::Table::get(const Reference &key) const
{
    Frame frame(ref.context);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_gettable(L, 1);
        ::lua_remove(L, 1);
    }, {ref, key}, 1);
    frame.pushed(1);
    return frame.back().get();
}

Engine::Output Engine::Table::get(const std::string &key) const
{
    Frame frame(ref.context);
    frame.push(key);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_gettable(L, 1);
        ::lua_remove(L, 1);
    }, {ref, frame.front()}, 1);
    frame.pushed(1);
    return frame.back().get();
}

Engine::Output Engine::Table::get(const char *key) const
{
    Frame frame(ref.context);
    frame.push(key);
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_gettable(L, 1);
        ::lua_remove(L, 1);
    }, {ref, frame.front()}, 1);
    frame.pushed(1);
    return frame.back().get();
}

Engine::Output Engine::Table::get(std::size_t key) const
{
    Frame frame(ref.context);
    frame.push(static_cast<std::int_fast64_t>(key));
    ref.context->luaProtected([](::lua_State *L) {
        ::lua_gettable(L, 1);
        ::lua_remove(L, 1);
    }, {ref, frame.front()}, 1);
    frame.pushed(1);
    return frame.back().get();
}

std::size_t Engine::Table::length() const
{
    std::size_t len = 0;
    ref.context->luaProtected([&len](::lua_State *L) {
        if (::lua_type(L, 1) != LUA_TTABLE)
            return;
        len = static_cast<std::size_t>(::lua_objlen(L, 1));
    }, {ref});
    return len;
}

void Engine::Table::rawSet(const Engine::Reference &key, const Engine::Reference &value)
{
    Q_ASSERT(ref.index != 0 && (ref.index < 0 || ref.index <= ::lua_gettop(ref.context->L)));

    Frame frame(ref.context);
    frame.push(key);
    frame.push(value);
    Q_ASSERT(::lua_type(ref.context->L, ref.index) == LUA_TTABLE);
    ::lua_rawset(ref.context->L, ref.index);
    frame.popped(2);
}

void Engine::Table::rawSet(std::size_t key, const Engine::Reference &value)
{
    Q_ASSERT(ref.index != 0 && (ref.index < 0 || ref.index <= ::lua_gettop(ref.context->L)));

    Frame frame(ref.context);
    frame.push(value);
    Q_ASSERT(::lua_type(ref.context->L, ref.index) == LUA_TTABLE);
    ::lua_rawseti(ref.context->L, ref.index, static_cast<int>(key));
    frame.popped(1);
}

void Engine::Table::rawErase(const Engine::Reference &key)
{
    Q_ASSERT(ref.index != 0 && (ref.index < 0 || ref.index <= ::lua_gettop(ref.context->L)));

    Frame frame(ref.context);
    frame.push(key);
    frame.pushNil();
    Q_ASSERT(::lua_type(ref.context->L, ref.index) == LUA_TTABLE);
    ::lua_rawset(ref.context->L, ref.index);
    frame.popped(2);
}

void Engine::Table::rawErase(std::size_t key)
{
    Q_ASSERT(ref.index != 0 && (ref.index < 0 || ref.index <= ::lua_gettop(ref.context->L)));

    Frame frame(ref.context);
    frame.pushNil();
    Q_ASSERT(::lua_type(ref.context->L, ref.index) == LUA_TTABLE);
    ::lua_rawseti(ref.context->L, ref.index, static_cast<int>(key));
    frame.popped(1);
}

Engine::Output Engine::Table::rawGet(const Engine::Reference &key) const
{
    Q_ASSERT(ref.index != 0 && (ref.index < 0 || ref.index <= ::lua_gettop(ref.context->L)));

    Frame frame(ref.context);
    frame.push(key);
    Q_ASSERT(::lua_type(ref.context->L, ref.index) == LUA_TTABLE);
    ::lua_rawget(ref.context->L, ref.index);
    return frame.back().get();
}

Engine::Output Engine::Table::rawGet(std::size_t key) const
{
    Q_ASSERT(ref.index != 0 && (ref.index < 0 || ref.index <= ::lua_gettop(ref.context->L)));

    Frame frame(ref.context);
    Q_ASSERT(::lua_type(ref.context->L, ref.index) == LUA_TTABLE);
    ::lua_rawgeti(ref.context->L, ref.index, static_cast<int>(key));
    frame.pushed(1);
    return frame.back().get();
}

void Engine::Table::setMetatable(const Table &table)
{
    Q_ASSERT(ref.index != 0 && (ref.index < 0 || ref.index <= ::lua_gettop(ref.context->L)));

    Frame frame(ref.context);
    frame.push(table);
    ::lua_setmetatable(ref.context->L, ref.index);
    frame.popped(1);
}

void Engine::Table::clearMetatable()
{
    Q_ASSERT(ref.index != 0 && (ref.index < 0 || ref.index <= ::lua_gettop(ref.context->L)));

    Frame frame(ref.context);
    frame.pushNil();
    ::lua_setmetatable(ref.context->L, ref.index);
    frame.popped(1);
}

void Engine::Table::setMetamethod(const std::string &method, const Reference &value)
{
    Frame frame(ref.context);
    auto mt = pushMetatable(frame);
    frame.push(method);
    frame.push(value);
    Q_ASSERT(::lua_type(ref.context->L, mt.ref.index) == LUA_TTABLE);
    ::lua_rawset(ref.context->L, mt.ref.index);
    frame.popped(2);
}

Engine::Table Engine::Table::pushMetatable(Frame &frame) const
{
    Q_ASSERT(ref.index != 0 && (ref.index < 0 || ref.index <= ::lua_gettop(ref.context->L)));

    if (::lua_getmetatable(ref.context->L, ref.index)) {
        frame.pushed(1);
        if (frame.back().getType() == Reference::Type::Table)
            return Table(frame.back());
        frame.pop();
    }
    auto mt = frame.pushTable();
    frame.push(mt);
    ::lua_setmetatable(ref.context->L, ref.index);
    frame.popped(1);
    return std::move(mt);
}


Engine::Frame::Frame(Engine::Context *context) : context(context),
                                                 parent(nullptr),
                                                 origin(static_cast<std::uint_fast32_t>(::lua_gettop(
                                                         context->L)))
{
    if (context->entries.empty()) {
        context->entries.emplace_back(this, false);
        Q_ASSERT(origin == 0);
    } else {
        context->entries.back().depth++;
        parent = context->entries.back().active;
        context->entries.back().active = this;
    }
}

Engine::Frame::Frame(Engine::Context *context, bool fromLUA) : context(context),
                                                               parent(nullptr),
                                                               origin(0)
{
    context->entries.emplace_back(this, fromLUA);
}

Engine::Frame::Frame(Engine &engine) : context(engine.context.get()), parent(nullptr), origin(0)
{
    Q_ASSERT(context->entries.empty());
    Q_ASSERT(::lua_gettop(context->L) == 0);

    context->entries.emplace_back(this, false);
}


Engine::Frame::Frame(Engine::Frame &parent) : context(parent.context),
                                              parent(&parent),
                                              origin(static_cast<std::uint_fast32_t>(::lua_gettop(
                                                      context->L)))
{
    Q_ASSERT(!context->entries.empty());
    Q_ASSERT(context->entries.back().active == this->parent);

    context->entries.back().depth++;
    context->entries.back().active = this;
}

Engine::Frame::~Frame()
{
    Q_ASSERT(context->entries.back().active == this);

    context->entries.back().depth--;
    if (context->entries.back().depth == 0) {
        Q_ASSERT(context->entries.back().fromLUA || origin == 0);
        context->entries.pop_back();
    } else {
        context->entries.back().active = parent;
    }

    Q_ASSERT(::lua_gettop(context->L) >= static_cast<int>(origin));
    ::lua_settop(context->L, static_cast<int>(origin));
}

std::size_t Engine::Frame::size() const
{
    auto active = context->entries.back().active;
    if (active == this) {
        Q_ASSERT(::lua_gettop(context->L) >= static_cast<int>(origin));
        return static_cast<std::size_t>(::lua_gettop(context->L) - origin);
    }

    for (;;) {
        Q_ASSERT(active);
        auto effectiveTop = active->origin;
        active = active->parent;
        if (!active)
            break;
        if (active == this) {
            return static_cast<std::size_t>(effectiveTop - origin);
        }
    }
    Q_ASSERT(false);
    return 0;
}

void Engine::Frame::pushed(std::size_t added)
{
    Q_ASSERT(context->entries.back().active == this);
    Q_ASSERT(context->inError() ||
                     static_cast<std::size_t>(::lua_gettop(context->L) - origin) >= added);
}

void Engine::Frame::popped(std::size_t removed)
{
    Q_ASSERT(context->entries.back().active == this);
    Q_ASSERT(context->inError() || ::lua_gettop(context->L) - static_cast<int>(origin) >= 0);
}


static bool readChunk(::lua_State *L, const std::string &contents, const std::string &name)
{
    struct Data {
        const std::string *contents;

        static const char *loader(::lua_State *L, void *parameter, ::size_t *size)
        {
            Data *data = reinterpret_cast<Data *>(parameter);
            if (!data->contents) {
                *size = 0;
                return nullptr;
            }
            *size = data->contents->size();
            const char *result = data->contents->c_str();
            data->contents = nullptr;
            return result;
        }

        explicit Data(const std::string &contents) : contents(&contents)
        { }
    };
    Data d(contents);
    return ::lua_load(L, Data::loader, &d, name.c_str()) == 0;
}

bool Engine::Frame::pushChunk(const std::string &contents, bool sandbox, const std::string &name)
{
    if (sandbox && !contents.empty() && contents.front() == 27) {
        throwError("Cannot sandbox byte compiled code");
        return false;
    }
    ::lua_checkstack(context->L, 1);
    if (!readChunk(context->L, contents, name)) {
        context->handleError(true);
        return false;
    }
    pushed(1);

    if (sandbox) {
        push(registry(), sandboxRegistryName);
        ::lua_setfenv(context->L, -2);
        popped(1);
    }
    return true;
}

bool Engine::Frame::pushFile(const std::string &filename, bool sandbox)
{
    ::lua_checkstack(context->L, 1);
    if (::luaL_loadfile(context->L, filename.c_str()) != 0) {
        context->handleError(true);
        return false;
    }
    pushed(1);

    if (sandbox) {
        push(registry(), sandboxRegistryName);
        ::lua_setfenv(context->L, -2);
        popped(1);
    }
    return true;
}

bool Engine::Frame::pushChunk(const std::string &contents,
                              const Engine::Table &environment,
                              const std::string &name)
{
    ::lua_checkstack(context->L, 1);
    if (!readChunk(context->L, contents, name)) {
        context->handleError(true);
        return false;
    }
    pushed(1);

    push(environment);
    ::lua_setfenv(context->L, -2);
    popped(1);
    return true;
}

bool Engine::Frame::pushFile(const std::string &filename, const Engine::Table &environment)
{
    ::lua_checkstack(context->L, 1);
    if (::luaL_loadfile(context->L, filename.c_str()) != 0) {
        context->handleError(true);
        return false;
    }
    pushed(1);

    push(environment);
    ::lua_setfenv(context->L, -2);
    popped(1);
    return true;
}

void *Engine::Frame::allocateUserData(std::size_t size)
{
    Q_ASSERT(size);
    Q_ASSERT(size >= sizeof(Data));
    ::lua_checkstack(context->L, 1);
    void *raw = ::lua_newuserdata(context->L, static_cast<::size_t>(size));
    Q_ASSERT(raw);
    pushed(1);

    push(registry(), dataMetatableRegistryName);
    ::lua_setmetatable(context->L, -2);
    popped(1);

    return raw;
}

Engine::Table Engine::Frame::pushTable()
{
    ::lua_newtable(context->L);
    pushed(1);
    return Table(back());
}

Engine::Table Engine::Frame::pushSandboxEnvironment()
{ return context->pushSandboxEnvironment(*this); }

void Engine::Frame::pushNil()
{
    ::lua_pushnil(context->L);
    pushed(1);
}

void Engine::Frame::push(std::function<void(Entry & )> call)
{
    pushData<Context::BasicEntryData>(context, std::move(call));
    ::lua_pushcclosure(context->L, Context::basicEntry, 1);
}

// @formatter:off
void Engine::Frame::push(std::function<void(Entry &, const std::vector<Reference> &)> call,
                         std::size_t upvalues)
{
    Q_ASSERT(upvalues < 254);
    Q_ASSERT(size() >= upvalues);

    pushData<Context::ReferenceEntryData>(context, std::move(call), upvalues);
    moveBackTo((size() - 1) - upvalues);
    ::lua_pushcclosure(context->L, Context::referenceEntry, static_cast<int>(1 + upvalues));
    popped(upvalues);
}

void Engine::Frame::push(std::function<void(Entry &, const std::vector<Reference> &)> call,
                         const std::vector<Reference> &upvalues)
{
    Q_ASSERT(upvalues.size() < 254);

    ::lua_checkstack(context->L, static_cast<int>(upvalues.size() + 2));

    pushData<Context::ReferenceEntryData>(context, std::move(call), upvalues.size());
    for (const auto &v : upvalues) {
        push(v);
    }
    ::lua_pushcclosure(context->L, Context::referenceEntry, static_cast<int>(1 + upvalues.size()));
    popped(upvalues.size());
}
// @formatter:on

void Engine::Frame::push(PersistentData *value)
{
    ::lua_checkstack(context->L, 1);
    ::lua_pushlightuserdata(context->L, reinterpret_cast<void *>(value));
    pushed(1);
}

void Engine::Frame::push(double value)
{
    ::lua_checkstack(context->L, 1);
    if (!FP::defined(value)) {
        ::lua_pushnumber(context->L, std::numeric_limits<lua_Number>::quiet_NaN());
    } else {
        ::lua_pushnumber(context->L, static_cast<lua_Number>(value));
    }
    pushed(1);
}

void Engine::Frame::push(std::int_fast64_t value)
{
    ::lua_checkstack(context->L, 1);
    if (!INTEGER::defined(value)) {
        ::lua_pushnumber(context->L, std::numeric_limits<lua_Number>::quiet_NaN());
    } else {
        ::lua_pushinteger(context->L, static_cast<lua_Integer>(value));
    }
    pushed(1);
}

void Engine::Frame::push(bool value)
{
    ::lua_checkstack(context->L, 1);
    ::lua_pushboolean(context->L, value ? 1 : 0);
    pushed(1);
}

void Engine::Frame::push(const std::string &value)
{
    ::lua_checkstack(context->L, 1);
    ::lua_pushlstring(context->L, value.c_str(), static_cast<::size_t>(value.size()));
    pushed(1);
}

void Engine::Frame::push(const char *value)
{
    ::lua_checkstack(context->L, 1);
    ::lua_pushstring(context->L, value);
    pushed(1);
}

void Engine::Frame::push(const Output &value)
{
    switch (value.getType()) {
    case Output::Type::Nil:
        return pushNil();
    case Output::Type::Data:
        return push(value.toData<PersistentData>());
    case Output::Type::Number:
        return push(value.toNumber());
    case Output::Type::String:
        return push(value.toString());
    case Output::Type::Boolean:
        return push(value.toBoolean());
    }
    Q_ASSERT(false);
}

void Engine::Frame::push(const Reference &ref)
{
    ::lua_checkstack(context->L, 1);
    ::lua_pushvalue(context->L, ref.index);
    pushed(1);
}

void Engine::Frame::push(const Table &table, const Output &key)
{
    push(key);
    context->luaProtected([](::lua_State *L) {
        ::lua_gettable(L, 1);
        ::lua_remove(L, 1);
    }, {table, back()}, 1);
    pushed(1);
    replaceWithBack(size() - 2);
}

void Engine::Frame::push(const Table &table, const Reference &key)
{
    context->luaProtected([](::lua_State *L) {
        ::lua_gettable(L, 1);
        ::lua_remove(L, 1);
    }, {table, key}, 1);
    pushed(1);
}

void Engine::Frame::push(const Table &table, const std::string &key)
{
    context->luaProtected([&key](::lua_State *L) {
        ::lua_pushlstring(L, key.c_str(), static_cast<::size_t>(key.size()));
        ::lua_gettable(L, 1);
        ::lua_remove(L, 1);
    }, {table}, 1);
    pushed(1);
}

void Engine::Frame::push(const Table &table, const char *key)
{
    context->luaProtected([key](::lua_State *L) {
        ::lua_pushstring(L, key);
        ::lua_gettable(L, 1);
        ::lua_remove(L, 1);
    }, {table}, 1);
    pushed(1);
}

void Engine::Frame::push(const Table &table, std::size_t key)
{
    context->luaProtected([key](::lua_State *L) {
        ::lua_pushinteger(L, static_cast<lua_Integer>(key));
        ::lua_gettable(L, 1);
        ::lua_remove(L, 1);
    }, {table}, 1);
    pushed(1);
}

void Engine::Frame::pushRaw(const Table &table, const Reference &key)
{
    push(key);
    Q_ASSERT(::lua_type(context->L, table.reference().index) == LUA_TTABLE);
    ::lua_rawget(context->L, table.reference().index);
}

void Engine::Frame::pushRaw(const Table &table, const std::size_t &key)
{
    Q_ASSERT(::lua_type(context->L, table.reference().index) == LUA_TTABLE);
    ::lua_rawgeti(context->L, table.reference().index, static_cast<int>(key));
}

void Engine::Frame::replaceWithBack(std::size_t index)
{
    Q_ASSERT(context->entries.back().active == this);
    Q_ASSERT(index < size());
    if (index >= size() - 1)
        return;
    ::lua_replace(context->L, static_cast<int>(origin + index + 1));
    popped(1);
}

void Engine::Frame::moveBackTo(std::size_t index)
{
    Q_ASSERT(context->entries.back().active == this);
    Q_ASSERT(index < size());
    if (index >= size() - 1)
        return;
    ::lua_insert(context->L, static_cast<int>(origin + index + 1));
}

void Engine::Frame::erase(std::size_t index)
{
    Q_ASSERT(context->entries.back().active == this);
    Q_ASSERT(index < size());
    if (index == size() - 1) {
        ::lua_pop(context->L, 1);
        popped(1);
        return;
    }
    ::lua_remove(context->L, static_cast<int>(origin + index + 1));
    popped(1);
}

void Engine::Frame::pop(std::size_t count)
{
    Q_ASSERT(context->entries.back().active == this);
    Q_ASSERT(size() >= count);
    ::lua_pop(context->L, static_cast<int>(count));
    popped(count);
}

void Engine::Frame::clear()
{
    Q_ASSERT(context->entries.back().active == this);
    Q_ASSERT(::lua_gettop(context->L) >= static_cast<int>(origin));
    ::lua_settop(context->L, static_cast<int>(origin));
}

void Engine::Frame::resize(std::size_t size)
{
    Q_ASSERT(context->entries.back().active == this);
    Q_ASSERT(::lua_gettop(context->L) >= static_cast<int>(origin));
    ::lua_settop(context->L, static_cast<int>(origin + size));
}

Engine::Reference Engine::Frame::back() const
{
    auto active = context->entries.back().active;
    if (active == this) {
        Q_ASSERT(!empty());
        return Engine::Reference(context, ::lua_gettop(context->L));
    }

    for (;;) {
        Q_ASSERT(active);
        auto effectiveTop = active->origin;
        active = active->parent;
        if (!active)
            break;
        if (active == this) {
            Q_ASSERT(effectiveTop != 0);
            return Engine::Reference(context, static_cast<int>(effectiveTop));
        }
    }
    Q_ASSERT(false);
    return Engine::Reference();
}

Engine::Reference Engine::Frame::front() const
{ return Engine::Reference(context, static_cast<int>(origin + 1)); }

Engine::Reference Engine::Frame::operator[](std::size_t index) const
{ return Engine::Reference(context, static_cast<int>(origin + index + 1)); }

Engine::Reference Engine::Frame::fromBack(std::size_t distance) const
{
    auto active = context->entries.back().active;
    if (active == this) {
        Q_ASSERT(static_cast<int>(distance) < ::lua_gettop(context->L));
        return Engine::Reference(context, static_cast<int>(::lua_gettop(context->L) - distance));
    }

    for (;;) {
        Q_ASSERT(active);
        auto effectiveTop = active->origin;
        active = active->parent;
        if (!active)
            break;
        if (active == this) {
            Q_ASSERT(distance >= effectiveTop);
            effectiveTop -= distance;
            Q_ASSERT(effectiveTop > origin);
            return Engine::Reference(context, static_cast<int>(effectiveTop));
        }
    }
    Q_ASSERT(false);
    return Engine::Reference();
}

void Engine::Frame::propagate()
{
    Q_ASSERT(parent);
    Q_ASSERT(context->entries.back().active == this);
    Q_ASSERT(static_cast<std::uint_fast32_t>(::lua_gettop(context->L)) >= origin);
    origin = static_cast<std::uint_fast32_t>(::lua_gettop(context->L));
}

void Engine::Frame::propagate(std::size_t count)
{
    Q_ASSERT(parent);
    Q_ASSERT(context->entries.back().active == this);
    Q_ASSERT(static_cast<std::size_t>(::lua_gettop(context->L) - origin) >= count);
    origin += count;
}

Engine::Table Engine::Frame::global() const
{ return Table(Reference(context, LUA_GLOBALSINDEX)); }

Engine::Table Engine::Frame::registry() const
{ return Table(Reference(context, LUA_REGISTRYINDEX)); }

bool Engine::Frame::inError() const
{ return context->inError(); }

std::string Engine::Frame::errorDescription() const
{ return context->errorDescription(); }

void Engine::Frame::throwError(const std::string &message)
{ context->throwError(message); }

void Engine::Frame::clearError()
{ context->clearError(); }


Engine::Entry::Entry(Engine::Frame &parent) : Frame(parent)
{ }

Engine::Entry::~Entry() = default;


Engine::Call::~Call() = default;

Engine::Call::Call(Engine &engine) : Frame(engine)
{ }

Engine::Call::Call(Engine::Frame &parent) : Frame(parent)
{ }

Engine::Call::Call(Engine::Frame &parent, const Engine::Reference &target) : Frame(parent)
{ push(target); }

bool Engine::Call::execute(std::size_t results)
{
    Q_ASSERT(size() >= 1);
    if (::lua_pcall(context->L, static_cast<int>(size() - 1), static_cast<int>(results), 0)) {
        context->handleError();
        ::lua_settop(context->L, static_cast<int>(origin + results));
        return false;
    }

    Q_ASSERT(::lua_gettop(context->L) >= static_cast<int>(origin + results));

    context->luaExitNoError();

    return true;
}

bool Engine::Call::executeVariable()
{
    auto vars = size();
    Q_ASSERT(vars >= 1);
    if (::lua_pcall(context->L, static_cast<int>(vars - 1), LUA_MULTRET, 0)) {
        context->handleError();
        ::lua_settop(context->L, static_cast<int>(origin));
        return false;
    }

    Q_ASSERT(::lua_gettop(context->L) >= static_cast<int>(origin));

    context->luaExitNoError();

    return true;

}


Engine::Assign::Assign(Frame &parent, Table &table, const Output &key) : Frame(parent),
                                                                         table(table.reference())
{ push(key); }

Engine::Assign::Assign(Frame &parent, Table &table, const Reference &key) : Frame(parent),
                                                                            table(table.reference())
{ push(key); }

Engine::Assign::Assign(Frame &parent, Table &table, const std::string &key) : Frame(parent),
                                                                              table(table.reference())
{ push(key); }

Engine::Assign::Assign(Frame &parent, Table &table, const QString &key) : Assign(parent, table,
                                                                                 key.toStdString())
{ }

Engine::Assign::Assign(Frame &parent, Table &table, const char *key) : Frame(parent),
                                                                       table(table.reference())
{ push(key); }

Engine::Assign::Assign(Frame &parent, Table &table, std::size_t key) : Frame(parent),
                                                                       table(table.reference())
{ push(static_cast<std::int_fast64_t>(key)); }

Engine::Assign::Assign(Frame &parent, Table &&table, const Output &key) : Frame(parent),
                                                                          table(std::move(table))
{ push(key); }

Engine::Assign::Assign(Frame &parent, Table &&table, const Reference &key) : Frame(parent),
                                                                             table(std::move(table))
{ push(key); }

Engine::Assign::Assign(Frame &parent, Table &&table, const std::string &key) : Frame(parent),
                                                                               table(std::move(
                                                                                       table))
{ push(key); }

Engine::Assign::Assign(Frame &parent, Table &&table, const QString &key) : Assign(parent, table,
                                                                                  key.toStdString())
{ }

Engine::Assign::Assign(Frame &parent, Table &&table, const char *key) : Frame(parent),
                                                                        table(std::move(table))
{ push(key); }

Engine::Assign::Assign(Frame &parent, Table &&table, std::size_t key) : Frame(parent),
                                                                        table(std::move(table))
{ push(static_cast<std::int_fast64_t>(key)); }

Engine::Assign::~Assign()
{
    Q_ASSERT(size() == 2);
    context->luaProtected([](::lua_State *L) {
        ::lua_settable(L, -3);
    }, {table, front(), back()});
}


Engine::Append::Append(Frame &parent, Table &table, bool raw) : Frame(parent),
                                                                table(table.reference()),
                                                                raw(raw)
{ }


Engine::Append::Append(Frame &parent, Table &&table, bool raw) : Frame(parent),
                                                                 table(std::move(table)),
                                                                 raw(raw)
{ }

Engine::Append::~Append()
{
    if (empty())
        return;
    std::vector<Reference> args{table};
    for (std::size_t i = 0, s = size(); i < s; i++) {
        args.emplace_back((*this)[i]);
    }
    bool rawMode = raw;
    context->luaProtected([rawMode](::lua_State *L) {
        auto idx = ::lua_objlen(L, 1) + 1;
        if (rawMode) {
            auto total = ::lua_gettop(L);
            idx += (total - 2);
            Q_ASSERT(::lua_type(L, 1) == LUA_TTABLE);
            for (; total >= 2; --total, --idx) {
                ::lua_rawseti(L, 1, static_cast<int>(idx));
            }
            return;
        }
        for (auto total = ::lua_gettop(L), add = 2; add <= total; ++add, ++idx) {
            ::lua_pushinteger(L, static_cast<lua_Integer>(idx));
            ::lua_pushvalue(L, add);
            ::lua_settable(L, 1);
        }
    }, args);
}


Engine::Iterator::~Iterator() = default;

Engine::Iterator::Iterator(Frame &parent, const Reference &ref) : Frame(parent), ref(ref)
{
    pushNil();
}

Engine::Iterator::Iterator(Frame &parent, const Table &table) : Frame(parent), ref(table)
{
    pushNil();
}

bool Engine::Iterator::next()
{
    if (inError())
        return false;

    Q_ASSERT(size() >= 1);
    ::lua_settop(context->L, static_cast<int>(origin + 1));
    Q_ASSERT(size() == 1);

    context->luaProtected([](::lua_State *L) {
        if (::lua_type(L, 1) != LUA_TTABLE) {
            ::lua_settop(L, 0);
            return;
        }
        ::lua_next(L, 1);
        ::lua_remove(L, 1);
    }, {ref, back()}, LUA_MULTRET);
    if (inError())
        return false;

    /* Remove the original key */
    ::lua_remove(context->L, static_cast<int>(origin + 1));

    Q_ASSERT(size() == 0 || size() == 2);

    if (size() == 2) {
        pushed(1);
    } else {
        popped(1);
    }

    return size() == 2;
}

Engine::Reference Engine::Iterator::key() const
{ return front(); }

Engine::Reference Engine::Iterator::value() const
{ return (*this)[1]; }

void Engine::Iterator::restart()
{
    ::lua_settop(context->L, static_cast<int>(origin));
    pushNil();
}


QDebug operator<<(QDebug stream, const Engine::Output &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    Engine::Context::debug(stream, v);
    return stream;
}

QDebug operator<<(QDebug stream, const Engine::Reference &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    Engine::Context::debug(stream, v);
    return stream;
}

QDebug operator<<(QDebug stream, const Engine::Table &v)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    Engine::Context::debug(stream, v);
    return stream;
}

}
}