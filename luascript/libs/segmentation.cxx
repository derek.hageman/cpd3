/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "segmentation.hxx"
#include "time.hxx"
#include "core/number.hxx"
#include "core/range.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {

static const std::string metatableRegistryName = "CPD3_segmentation_metatablebase";

static Engine::Table pushBuffer(Engine::Frame &frame, const Engine::Reference &self)
{
    {
        Engine::Frame local(frame);
        auto mt = Engine::Table(self).pushMetatable(local);
        local.push(mt, "buffer");
        local.replaceWithBack(0);
        local.propagate(1);
    }
    return frame.back();
}

void Segmentation::install(Engine::Table &target, Engine::Frame &frame)
{
    auto mt = frame.pushTable();

    {
        Engine::Assign assign(frame, mt, "__call");
        pushMethod<Segmentation>(assign, &Segmentation::meta_call);
    }
    {
        Engine::Assign assign(frame, mt, "__len");
        pushMethod<Segmentation>(assign, &Segmentation::meta_len);
    }
    {
        Engine::Assign assign(frame, mt, "__tostring");
        pushMethod<Segmentation>(assign, &Segmentation::meta_tostring);
    }
    {
        Engine::Assign assign(frame, mt, "__newindex");
        pushMethod<Segmentation>(assign, &Segmentation::meta_newindex);
    }

    {
        Engine::Assign assign(frame, mt, "__index");
        auto methods = assign.pushTable();

        {
            Engine::Assign ma(assign, methods, "iterate");
            pushMethod<Segmentation>(ma, &Segmentation::method_iterate);
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Segmentation>(mf, &Segmentation::method_overlay);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "overlay");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "add");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "clear");
            pushMethod<Segmentation>(ma, &Segmentation::method_clear);
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Segmentation>(mf, &Segmentation::method_find);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "find");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "intersecting");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "point");
            pushMethod<Segmentation>(ma, &Segmentation::method_point);
        }
        {
            Engine::Assign ma(assign, methods, "shift");
            pushMethod<Segmentation>(ma, &Segmentation::method_shift);
        }
        {
            Engine::Assign ma(assign, methods, "index");
            pushMethod<Segmentation>(ma, &Segmentation::method_lookup);
        }

        pushMethod<Segmentation>(assign, &Segmentation::meta_index, 1);
    }

    frame.registry().set(metatableRegistryName, mt);

    {
        Engine::Frame local(frame);
        local.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            auto created = entry.pushData<Segmentation>();
            auto self = entry.back();
            if (!entry.empty()) {
                if (auto other = entry[0].toData<Segmentation>()) {
                    Engine::Frame local(entry);
                    for (const auto &add : other->contents) {
                        created->contents.emplace_back();
                        auto &c = created->contents.back();
                        c.setStart(add.getStart());
                        c.setEnd(add.getEnd());
                        c.context = &created->context;
                        c.data = add.data;
                    }
                    created->context = other->context;

                    auto target = pushBuffer(local, self);
                    auto source = pushBuffer(local, entry[0]);
                    for (const auto &add : other->context.references) {
                        Engine::Frame assign(local);
                        assign.pushRaw(source, add.first);
                        target.rawSet(add.first, assign.back());
                    }
                }
            }
            entry.replaceWithBack(0);
            entry.propagate(1);
        }));
        target.set("Segmentation", local.back());
    }
}

void Segmentation::initialize(const Engine::Reference &self, Engine::Frame &frame)
{
    auto mt = Engine::Data::pushMetatable(frame);

    {
        frame.push(frame.registry(), metatableRegistryName);
        Engine::Iterator it(frame, frame.back());
        while (it.next()) {
            mt.set(it.key(), it.value());
        }
    }

    {
        Engine::Assign assign(frame, mt, "buffer");
        assign.pushTable();
    }

    setMetatable(self, mt);
}

Segmentation::Segmentation() : iterationIndex(0)
{ }

Segmentation::~Segmentation()
{
    for (auto &c : contents) {
        c.context = nullptr;
    }
    contents.clear();
}

void Segmentation::meta_tostring(Engine::Entry &entry)
{
    std::string result = "[";
    auto buffer = pushBuffer(entry, entry[0]);
    bool first = true;
    for (const auto &add : contents) {
        if (!first)
            result += ",";
        first = false;
        result += "{";
        if (!FP::defined(add.getStart())) {
            result += "-INFINITY";
        } else if (add.getStart() < 1E6) {
            result += QString::number(add.getStart(), 'f', 0).toStdString();
        } else {
            result += CPD3::Time::toISO8601(add.getStart()).toStdString();
        }
        result += ",";
        if (!FP::defined(add.getEnd())) {
            result += "+INFINITY";
        } else if (add.getEnd() < 1E6) {
            result += QString::number(add.getEnd(), 'f', 0).toStdString();
        } else {
            result += CPD3::Time::toISO8601(add.getEnd()).toStdString();
        }
        result += ",";
        {
            Engine::Frame local(entry);
            add.pushData(local, buffer);
            result += local.back().toOutputString();
        }
        result += "}";
    }

    result += "]";

    entry.clear();
    entry.push(std::move(result));
    entry.propagate(1);
}

void Segmentation::meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid index");
        return;
    }
    if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
        return;

    if (entry[1].getType() == Engine::Reference::Type::Number) {
        auto index = entry[1].toInteger();
        if (!INTEGER::defined(index) || index == 0 || contents.empty()) {
            entry.clear();
            entry.pushNil();
            entry.propagate(1);
            return;
        }
        if (index < 0) {
            index = contents.size() + index;
            if (index < 0) {
                entry.clear();
                entry.pushNil();
                entry.propagate(1);
                return;
            }
        } else {
            --index;
        }
        if (static_cast<std::size_t>(index) >= contents.size()) {
            entry.clear();
            entry.pushNil();
            entry.propagate(1);
            return;
        }
        auto buffer = pushBuffer(entry, entry[0]);
        contents[index].pushData(entry, buffer);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    }

    auto bounds = Time::extractBounds(entry, entry[1]);
    auto hit = Range::findIntersecting(contents.begin(), contents.end(), bounds.getStart(),
                                       bounds.getEnd());
    if (hit == contents.end()) {
        entry.clear();
        entry.pushNil();
        entry.propagate(1);
        return;
    }

    auto buffer = pushBuffer(entry, entry[0]);
    hit->pushData(entry, buffer);
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Segmentation::meta_newindex(Engine::Entry &entry)
{
    if (entry.size() < 3) {
        entry.throwError("Invalid assignment");
        return;
    }
    if (entry[1].getType() == Engine::Reference::Type::Number) {
        auto index = entry[1].toInteger();
        if (!INTEGER::defined(index) || index == 0) {
            entry.throwError("Invalid assignment");
            return;
        }
        if (contents.empty()) {
            entry.throwError("Assignment is only allowed to existing segments");
            return;
        }
        if (index < 0) {
            index = contents.size() + index;
            if (index < 0) {
                entry.throwError("Invalid assignment");
                return;
            }
        } else {
            --index;
        }
        if (static_cast<std::size_t>(index) >= contents.size()) {
            entry.throwError("Assignment is only allowed to existing segments");
            return;
        }
        auto buffer = pushBuffer(entry, entry[0]);
        buffer.rawSet(contents[index].data, entry[2]);
        return;
    }

    entry.throwError("Invalid assignment");
}

void Segmentation::meta_len(Engine::Entry &entry)
{
    auto n = contents.size();
    entry.clear();
    entry.push(static_cast<std::int_fast64_t>(n));
    entry.propagate(1);
}

void Segmentation::meta_call(Engine::Entry &entry)
{
    if (iterationIndex >= contents.size()) {
        iterationIndex = 0;

        entry.clear();
        entry.pushNil();
        entry.propagate(1);
        return;
    }

    entry.resize(1);
    auto buffer = pushBuffer(entry, entry[0]);
    auto &output = contents[iterationIndex];
    ++iterationIndex;
    output.pushLookup(entry, buffer);
    entry.erase(0);
    entry.erase(0);
    entry.propagate(3);
}

void Segmentation::method_iterate(Engine::Entry &entry)
{
    struct Context {
        std::size_t index;
        std::size_t max;
        std::vector<CPD3::Time::Bounds> bounds;

        explicit Context(std::size_t max) : index(0), max(max)
        { }
    };
    auto context = std::make_shared<Context>(contents.size());
    auto buffer = pushBuffer(entry, entry[0]);
    auto local = entry.pushTable();
    {
        Engine::Append append(entry, local, true);
        for (const auto &add : contents) {
            add.pushData(append, buffer);
            context->bounds.emplace_back(add);
        }
    }
    entry.push(std::function<void(Engine::Entry &, const std::vector<Engine::Reference> &)>(
            [context](Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues) {
                if (context->index >= context->max) {
                    entry.clear();
                    entry.pushNil();
                    entry.propagate(1);
                    return;
                }
                entry.clear();
                auto idx = context->index;
                ++(context->index);
                entry.pushRaw(upvalues.front(), idx + 1);
                entry.push(context->bounds[idx].getStart());
                entry.push(context->bounds[idx].getEnd());
                entry.propagate(3);
            }), 1);
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Segmentation::method_overlay(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }
    auto source = entry[1];

    double segmentStart = FP::undefined();
    double segmentEnd = FP::undefined();
    {
        Engine::Frame local(entry);
        auto start = Time::pushStart(local, source);
        auto end = Time::pushEnd(local, source);

        if (!start.isNil()) {
            if (!end.isNil()) {
                auto bounds = Time::extractBounds(local, start, end);
                segmentStart = bounds.start;
                segmentEnd = bounds.end;
            } else {
                segmentStart = Time::extractSingle(local, start);
            }
        } else if (!end.isNil()) {
            segmentEnd = Time::extractSingle(local, end);
        }
    }

    Engine::Reference overlay;
    Engine::Reference convert;
    if (entry.size() > 2) {
        auto start = Time::pushStart(entry, entry[2]);
        auto end = Time::pushEnd(entry, entry[2]);

        if (!start.isNil()) {
            if (!end.isNil()) {
                auto bounds = Time::extractBounds(entry, start, end);
                segmentStart = bounds.start;
                segmentEnd = bounds.end;
            } else {
                segmentStart = Time::extractSingle(entry, start);
            }
        } else if (!end.isNil()) {
            segmentEnd = Time::extractSingle(entry, end);
        }

        entry.push(entry[2], "overlay");
        if (!entry.back().isNil()) {
            overlay = entry.back();
        }

        entry.push(entry[2], "convert");
        if (!entry.back().isNil()) {
            convert = entry.back();
        }
    }

    auto buffer = pushBuffer(entry, entry[0]);
    Range::overlayFragmenting(contents,
                              Overlay(segmentStart, segmentEnd, context, entry, buffer, source,
                                      overlay, convert));

    context.releasePending(entry, buffer);
}

void Segmentation::method_clear(Engine::Entry &entry)
{
    for (auto &c : contents) {
        c.context = nullptr;
    }
    contents.clear();
    context.reset();

    auto mt = Engine::Table(entry[0]).pushMetatable(entry);
    {
        Engine::Assign assign(entry, mt, "buffer");
        assign.pushTable();
    }
}

void Segmentation::method_find(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }
    switch (entry[1].getType()) {
    case Engine::Reference::Type::Number:
    case Engine::Reference::Type::String:
        return method_point(entry);
    default:
        break;
    }

    auto bounds = Time::extractBounds(entry, entry[1]);

    auto hit = Range::findIntersecting(contents.begin(), contents.end(), bounds.getStart(),
                                       bounds.getEnd());
    if (hit == contents.end()) {
        entry.clear();
        entry.pushNil();
        entry.propagate(1);
        return;
    }

    if (entry.size() > 2 && entry[2].toBoolean()) {
        entry.push(static_cast<std::int_fast64_t>(hit - contents.begin()) + 1);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    }

    entry.resize(1);
    auto buffer = pushBuffer(entry, entry[0]);
    hit->pushLookup(entry, buffer);
    entry.erase(0);
    entry.erase(0);
    entry.propagate(3);
}

void Segmentation::method_point(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }


    auto time = Time::extractSingle(entry, entry[1]);
    if (!FP::defined(time)) {
        entry.clear();
        entry.pushNil();
        entry.propagate(1);
        return;
    }

    auto hit = Range::findIntersecting(contents.begin(), contents.end(), time);
    if (hit == contents.end()) {
        entry.clear();
        entry.pushNil();
        entry.propagate(1);
        return;
    }

    if (entry.size() > 2 && entry[2].toBoolean()) {
        entry.push(static_cast<std::int_fast64_t>(hit - contents.begin()) + 1);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    }

    entry.resize(1);
    auto buffer = pushBuffer(entry, entry[0]);
    hit->pushLookup(entry, buffer);
    entry.erase(0);
    entry.erase(0);
    entry.propagate(3);
}

void Segmentation::method_shift(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }

    switch (entry[1].getType()) {
    case Engine::Reference::Type::Number:
    case Engine::Reference::Type::String: {
        auto time = Time::extractSingle(entry, entry[1]);
        if (!FP::defined(time)) {
            entry.clear();
            entry.pushNil();
            entry.propagate(1);
            return;
        }
        entry.resize(1);
        auto buffer = pushBuffer(entry, entry[0]);
        if (!Range::intersectShift(contents, time)) {
            context.releasePending(entry, buffer);
            entry.clear();
            entry.pushNil();
            entry.propagate(1);
            return;
        }
        context.releasePending(entry, buffer);

        Q_ASSERT(!contents.empty());
        contents.front().pushLookup(entry, buffer);
        entry.erase(0);
        entry.erase(0);
        entry.propagate(3);
        return;
    }
    default:
        break;
    }

    auto bounds = Time::extractBounds(entry, entry[1]);

    entry.resize(1);
    auto buffer = pushBuffer(entry, entry[0]);
    if (!Range::intersectShift(contents, bounds.getStart(), bounds.getEnd())) {
        context.releasePending(entry, buffer);
        entry.clear();
        entry.pushNil();
        entry.propagate(1);
        return;
    }
    context.releasePending(entry, buffer);

    Q_ASSERT(!contents.empty());
    contents.front().pushLookup(entry, buffer);
    entry.erase(0);
    entry.erase(0);
    entry.propagate(3);
}

void Segmentation::method_lookup(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }

    auto index = entry[1].toInteger();
    if (!INTEGER::defined(index) || index == 0) {
        index = 0;
    } else if (index < 0) {
        index = contents.size() + index;
        if (index < 0)
            return;
    } else {
        --index;
    }
    if (static_cast<std::size_t>(index) >= contents.size())
        return;

    entry.resize(1);
    auto buffer = pushBuffer(entry, entry[0]);
    auto &output = contents[index];
    output.pushLookup(entry, buffer);
    entry.erase(0);
    entry.erase(0);
    entry.propagate(3);
}


Segmentation::BufferContext::BufferContext() : next(1)
{ }

Segmentation::BufferContext::~BufferContext() = default;

void Segmentation::BufferContext::releasePending(Engine::Frame &frame,
                                                 const Engine::Reference &buffer)
{
    if (released.empty())
        return;
    Engine::Table t(buffer);
    for (const auto &rel : released) {
        t.rawErase(rel);
    }
    released.clear();
    if (references.empty()) {
        next = 1;
    }
}

void Segmentation::BufferContext::reset()
{
    references.clear();
    released.clear();
    next = 1;
}

void Segmentation::BufferContext::deref(std::size_t data)
{
    auto check = references.find(data);
    Q_ASSERT(check != references.end());
    if (check->second > 1) {
        --(check->second);
        return;
    }
    released.emplace_back(check->first);
    references.erase(check);
}

void Segmentation::BufferContext::ref(std::size_t data)
{
    auto check = references.find(data);
    Q_ASSERT(check != references.end());
    ++(check->second);
}

std::size_t Segmentation::BufferContext::acquire(Engine::Frame &frame,
                                                 const Engine::Reference &buffer,
                                                 const Engine::Reference &data)
{
    std::size_t index = next;
    next = (static_cast<std::uint_fast32_t>(next) + 1) & 0x7FFFFFFFU;
    Q_ASSERT(references.find(index) == references.end());

    references[index] = 1;
    Engine::Table(buffer).rawSet(index, data);
    return index;
}

Segmentation::Overlay::Overlay(double start,
                               double end,
                               Segmentation::BufferContext &context,
                               Engine::Frame &frame,
                               const Engine::Reference &buffer,
                               const Engine::Reference &data,
                               const Engine::Reference &overlay,
                               const Engine::Reference &convert) : CPD3::Time::Bounds(start, end),
                                                                   context(context),
                                                                   frame(frame),
                                                                   buffer(buffer),
                                                                   data(data),
                                                                   overlay(overlay),
                                                                   convert(convert)
{ }

Segmentation::Overlay::~Overlay() = default;

Segmentation::Entry::Entry() : context(nullptr), data(0)
{ }

Segmentation::Entry::~Entry()
{
    if (context)
        context->deref(data);
}

Segmentation::Entry::Entry(Segmentation::Entry &&other) : CPD3::Time::Bounds(other),
                                                          context(other.context),
                                                          data(other.data)
{
    other.context = nullptr;
    other.data = 0;
}

Segmentation::Entry &Segmentation::Entry::operator=(Segmentation::Entry &&other)
{
    using std::swap;
    swap(other.context, context);
    swap(other.data, data);
    CPD3::Time::Bounds::operator=(std::move(other));
    return *this;
}

Segmentation::Entry::Entry(const Entry &other) : CPD3::Time::Bounds(other),
                                                 context(other.context),
                                                 data(other.data)
{
    if (context)
        context->ref(data);
}

Segmentation::Entry &Segmentation::Entry::operator=(const Segmentation::Entry &other)
{
    if (other.context)
        other.context->ref(other.data);
    if (context)
        context->deref(data);
    CPD3::Time::Bounds::operator=(other);
    context = other.context;
    data = other.data;
    return *this;
}

Segmentation::Entry::Entry(const Overlay &overlay) : CPD3::Time::Bounds(overlay),
                                                     context(&overlay.context)
{
    if (!overlay.convert.isAssigned() || overlay.convert.isNil()) {
        data = context->acquire(overlay.frame, overlay.buffer, overlay.data);
        return;
    }

    {
        Engine::Call call(overlay.frame, overlay.convert);
        call.push(overlay.data);
        call.push(start);
        call.push(end);
        if (!call.execute(1)) {
            context = nullptr;
            return;
        }
        data = context->acquire(call, overlay.buffer, call.front());
    }
}

Segmentation::Entry::Entry(const Entry &other, double start, double end) : CPD3::Time::Bounds(start,
                                                                                              end),
                                                                           context(other.context),
                                                                           data(other.data)
{
    if (context)
        context->ref(data);
}

Segmentation::Entry::Entry(const Overlay &overlay, double start, double end) : CPD3::Time::Bounds(
        start, end), context(&overlay.context)
{
    if (!overlay.convert.isAssigned() || overlay.convert.isNil()) {
        data = context->acquire(overlay.frame, overlay.buffer, overlay.data);
        return;
    }

    {
        Engine::Call call(overlay.frame, overlay.convert);
        call.push(overlay.data);
        call.push(start);
        call.push(end);
        if (!call.execute(1)) {
            context = nullptr;
            return;
        }
        data = context->acquire(call, overlay.buffer, call.front());
    }
}

Segmentation::Entry::Entry(const Entry &under, const Overlay &over, double start, double end)
        : CPD3::Time::Bounds(start, end), context(&over.context)
{
    if (!under.data || !under.context || !over.overlay.isAssigned() || over.overlay.isNil()) {
        data = context->acquire(over.frame, over.buffer, over.data);
        return;
    }

    {
        Engine::Call call(over.frame, over.overlay);
        under.pushData(call, over.buffer);
        call.push(over.data);
        call.push(start);
        call.push(end);
        if (!call.execute(1)) {
            context = nullptr;
            return;
        }
        data = context->acquire(call, over.buffer, call.front());
    }
}

void Segmentation::Entry::pushData(Engine::Frame &frame, const Engine::Reference &buffer) const
{
    if (!context) {
        frame.pushNil();
        return;
    }
    frame.pushRaw(buffer, data);
}

void Segmentation::Entry::pushLookup(Engine::Frame &frame, const Engine::Reference &buffer) const
{
    if (!context) {
        frame.pushNil();
    } else {
        frame.pushRaw(buffer, data);
    }
    frame.push(getStart());
    frame.push(getEnd());
}

}
}
}