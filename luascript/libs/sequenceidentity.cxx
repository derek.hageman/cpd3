/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "sequenceidentity.hxx"
#include "sequencename.hxx"
#include "time.hxx"
#include "core/timeutils.hxx"


namespace CPD3 {
namespace Lua {
namespace Libs {

static const std::string metatableRegistryName = "CPD3_sequenceidentity_metatable";

void SequenceIdentity::install(Engine::Table &target, Engine::Frame &frame)
{
    auto mt = pushMetatable(frame);

    {
        Engine::Assign assign(frame, mt, "__eq");
        pushMethod<SequenceIdentity>(assign, &SequenceIdentity::meta_eq);
    }
    {
        Engine::Assign assign(frame, mt, "__tostring");
        pushMethod<SequenceIdentity>(assign, &SequenceIdentity::meta_tostring);
    }
    {
        Engine::Assign assign(frame, mt, "__newindex");
        pushMethod<SequenceIdentity>(assign, &SequenceIdentity::meta_newindex);
    }

    {
        Engine::Assign assign(frame, mt, "__index");
        auto methods = assign.pushTable();

        {
            Engine::Assign ma(assign, methods, "overlayLessThan");
            pushMethod<SequenceIdentity>(ma, &SequenceIdentity::method_overlayLessThan);
        }
        {
            Engine::Assign ma(assign, methods, "timeLessThan");
            pushMethod<SequenceIdentity>(ma, &SequenceIdentity::method_timeLessThan);
        }

        pushMethod<SequenceIdentity>(assign, &SequenceIdentity::meta_index, 1);
    }

    frame.registry().set(metatableRegistryName, mt);
    {
        Engine::Frame local(frame);
        local.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            auto value = constructFromFrame(entry);
            entry.clear();
            entry.pushData<SequenceIdentity>(std::move(value));
            entry.propagate(1);
        }));
        target.set("SequenceIdentity", local.back());
    }
}

void SequenceIdentity::initialize(const Engine::Reference &self, Engine::Frame &frame)
{
    frame.push(frame.registry(), metatableRegistryName);
    setMetatable(self, frame.back());
}

SequenceIdentity::SequenceIdentity() : detachedName(false)
{ }

SequenceIdentity::~SequenceIdentity() = default;

SequenceIdentity::SequenceIdentity(const CPD3::Data::SequenceIdentity &identity) : identity(
        identity), detachedName(false)
{ }

SequenceIdentity::SequenceIdentity(CPD3::Data::SequenceIdentity &&identity) : identity(
        std::move(identity)), detachedName(false)
{ }

void SequenceIdentity::assignFromTable(CPD3::Data::SequenceIdentity &identity,
                                       Engine::Frame &frame,
                                       const Engine::Table &table)
{
    SequenceName::assignFromTable(identity.getName(), frame, table);
    {
        Engine::Frame local(frame);
        local.push(table, "name");
        switch (local.back().getType()) {
        case Engine::Reference::Type::Nil:
        case Engine::Reference::Type::Boolean:
        case Engine::Reference::Type::Number:
            break;
        case Engine::Reference::Type::Data:
        case Engine::Reference::Type::String:
            identity.setName(SequenceName::extract(local, local.back()));
            break;
        case Engine::Reference::Type::Table:
            SequenceName::assignFromTable(identity.getName(), local, local.back());
            break;
        }
    }

    {
        Engine::Frame local(frame);
        auto start = Time::pushStart(local, table);
        auto end = Time::pushEnd(local, table);

        if (!start.isNil()) {
            if (!end.isNil()) {
                auto bounds = Time::extractBounds(local, start, end);
                identity.setStart(bounds.start);
                identity.setEnd(bounds.end);
            } else {
                identity.setStart(Time::extractSingle(local, start));
            }
        } else if (!end.isNil()) {
            identity.setEnd(Time::extractSingle(local, end));
        }
    }

    {
        auto check = table.get("priority");
        if (check.getType() == Engine::Output::Type::Number) {
            auto i = check.toInteger();
            if (INTEGER::defined(i))
                identity.setPriority(static_cast<int>(i));
        }
    }
}

CPD3::Data::SequenceIdentity SequenceIdentity::constructFromFrame(Engine::Frame &frame)
{
    std::size_t n = frame.size();
    switch (n) {
    case 0:
        return CPD3::Data::SequenceIdentity();
    case 1:
        return extract(frame, frame.front());
    default:
        break;
    }

    if (n < 3) {
        frame.throwError("Insufficient arguments to SequenceIdentity constructor");
        return CPD3::Data::SequenceIdentity();
    }

    Q_ASSERT(n >= 3);

    int priority = 0;
    if (n >= 4) {
        auto i = frame[3].toInteger();
        if (INTEGER::defined(i))
            priority = static_cast<int>(i);
    }


    return CPD3::Data::SequenceIdentity(SequenceName::extract(frame, frame[0]), frame[1].toReal(),
                                        frame[2].toReal(), priority);
}

CPD3::Data::SequenceIdentity SequenceIdentity::extract(Engine::Frame &frame,
                                                       const Engine::Reference &ref)
{
    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Number:
    case Engine::Reference::Type::String:
        return CPD3::Data::SequenceIdentity();
    case Engine::Reference::Type::Data: {
        if (auto check = ref.toData<SequenceIdentity>()) {
            return check->get(frame, ref);
        } else if (auto check = ref.toData<SequenceName>()) {
            return CPD3::Data::SequenceIdentity(check->get());
        } else {
            /* Use table conversion */
            break;
        }
    }
    default:
        break;
    }

    CPD3::Data::SequenceIdentity result;
    assignFromTable(result, frame, ref);
    return result;
}

void SequenceIdentity::fetchDetachedName(Engine::Frame &frame, const Engine::Reference &self) const
{
    Q_ASSERT(hasDetachedName());

    Engine::Frame local(frame);
    auto mt = Engine::Table(self).pushMetatable(local);
    local.push(mt, "name");
    local.moveBackTo(0);
    local.propagate(1);
}

CPD3::Data::SequenceIdentity SequenceIdentity::get(Engine::Frame &frame,
                                                   const Engine::Reference &self) const
{
    if (!hasDetachedName())
        return identity;

    Engine::Frame local(frame);
    fetchDetachedName(local, self);
    if (auto check = local.back().toData<SequenceName>()) {
        return CPD3::Data::SequenceIdentity(check->get(), identity.getStart(), identity.getEnd(),
                                            identity.getPriority());
    } else {
        frame.throwError("Invalid detached SequenceName");
    }
    return identity;
}

void SequenceIdentity::detachName(Engine::Frame &frame,
                                  const Engine::Reference &self,
                                  const Engine::Reference &name)
{
    if (!detachedName) {
        detachedName = true;
        Engine::Frame local(frame);
        auto mt = local.pushTable();
        mt.set("name", name);

        /* Can't just set the __index metamethod on the metatable, because our
         * value assignment relies on the self reference also being the table (first
         * parameter to __index and __newindex). */
        local.push(frame.registry(), metatableRegistryName);
        auto base = local.back();
        {
            Engine::Iterator it(local, base);
            while (it.next()) {
                mt.set(it.key(), it.value());
            }
        }

        setMetatable(self, mt);
    } else {
        Engine::Frame local(frame);
        auto mt = Engine::Table(self).pushMetatable(local);
        mt.set("name", name);
    }
}

void SequenceIdentity::pushDetachedName(Engine::Frame &frame, const Engine::Reference &self)
{
    if (hasDetachedName())
        return fetchDetachedName(frame, self);
    frame.pushData<SequenceName>(identity.getName());
    detachName(frame, self, frame.back());
}

void SequenceIdentity::meta_eq(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid equality arguments");
        return;
    }
    auto check = entry[1].toData<SequenceIdentity>();
    if (!check) {
        entry.clear();
        entry.push(false);
        entry.propagate(1);
        return;
    }

    bool result;
    if (!hasDetachedName()) {
        if (!check->hasDetachedName()) {
            result = (identity == check->identity);
        } else {
            result = (identity == check->get(entry, entry[1]));
        }
    } else {
        if (!check->hasDetachedName()) {
            result = (get(entry, entry[0]) == check->identity);
        } else {
            result = (get(entry, entry[0]) == check->get(entry, entry[1]));
        }
    }

    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

template<typename Functor>
static void operateOnName(SequenceIdentity *identity, Engine::Entry &entry, Functor functor)
{
    if (!identity->hasDetachedName()) {
        functor(entry, identity->get().getName());
        return;
    }

    Q_ASSERT(!entry.empty());

    SequenceName *name = nullptr;
    {
        Engine::Frame local(entry);
        identity->pushDetachedName(local, entry.front());
        name = local.back().toData<SequenceName>();
    }
    if (!name) {
        entry.throwError("Invalid name reference");
        return;
    }
    functor(entry, name->get());
}

void SequenceIdentity::meta_index(Engine::Entry &entry,
                                  const std::vector<Engine::Reference> &upvalues)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid index");
        return;
    }
    if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
        return;
    auto key = entry[1].toString();
    if (key == "station") {
        operateOnName(this, entry, [](Engine::Entry &entry, const CPD3::Data::SequenceName &name) {
            entry.push(name.getStation());
            entry.replaceWithBack(0);
            entry.propagate(1);
        });
        return;
    } else if (key == "archive") {
        operateOnName(this, entry, [](Engine::Entry &entry, const CPD3::Data::SequenceName &name) {
            entry.push(name.getArchive());
            entry.replaceWithBack(0);
            entry.propagate(1);
        });
        return;
    } else if (key == "variable") {
        operateOnName(this, entry, [](Engine::Entry &entry, const CPD3::Data::SequenceName &name) {
            entry.push(name.getVariable());
            entry.replaceWithBack(0);
            entry.propagate(1);
        });
        return;
    } else if (key == "flavors") {
        operateOnName(this, entry, [](Engine::Entry &entry, const CPD3::Data::SequenceName &name) {
            SequenceName::pushFlavors(entry, name.getFlavors());
            entry.replaceWithBack(0);
            entry.propagate(1);
        });
        return;
    } else if (Util::equal_insensitive(key, "start", "front")) {
        entry.push(identity.getStart());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (Util::equal_insensitive(key, "end", "back")) {
        entry.push(identity.getEnd());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "priority") {
        entry.push(identity.getPriority());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "name") {
        pushDetachedName(entry, entry.front());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else {
        entry.clear();
        entry.pushNil();
        entry.propagate(1);
        return;
    }
}

void SequenceIdentity::meta_newindex(Engine::Entry &entry)
{
    if (entry.size() < 3) {
        entry.throwError("Invalid assignment");
        return;
    }
    auto key = entry[1].toString();
    if (key == "station") {
        operateOnName(this, entry, [](Engine::Entry &entry, CPD3::Data::SequenceName &name) {
            name.setStation(entry[2].toString());
        });
    } else if (key == "archive") {
        operateOnName(this, entry, [](Engine::Entry &entry, CPD3::Data::SequenceName &name) {
            name.setArchive(entry[2].toString());
        });
    } else if (key == "variable") {
        operateOnName(this, entry, [](Engine::Entry &entry, CPD3::Data::SequenceName &name) {
            name.setVariable(entry[2].toString());
        });
    } else if (key == "flavors") {
        operateOnName(this, entry, [](Engine::Entry &entry, CPD3::Data::SequenceName &name) {
            name.setFlavors(SequenceName::convertFlavors(entry, entry[2]));
        });
    } else if (Util::equal_insensitive(key, "start", "front")) {
        identity.setStart(Time::extractSingle(entry, entry[2]));
    } else if (Util::equal_insensitive(key, "end", "back")) {
        identity.setEnd(Time::extractSingle(entry, entry[2]));
    } else if (key == "priority") {
        auto i = entry[2].toInteger();
        if (INTEGER::defined(i))
            identity.setPriority(static_cast<int>(i));
    } else if (key == "name") {
        if (entry[2].toData<SequenceName>()) {
            detachName(entry, entry[0], entry[2]);
        } else {
            entry.pushData<SequenceName>(SequenceName::extract(entry, entry[2]));
            detachName(entry, entry[0], entry.back());
        }
    } else {
        entry.throwError("Invalid assignment");
        return;
    }
}

void SequenceIdentity::method_overlayLessThan(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto check = entry[1].toData<SequenceIdentity>();
    if (!check) {
        entry.throwError("Cannot compare with an unknown type");
        return;
    }
    bool result = CPD3::Data::SequenceIdentity::OrderOverlay()(get(entry, entry[0]),
                                                               check->get(entry, entry[1]));
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void SequenceIdentity::method_timeLessThan(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto check = entry[1].toData<SequenceIdentity>();
    if (!check) {
        entry.throwError("Cannot compare with an unknown type");
        return;
    }
    bool result = CPD3::Data::SequenceIdentity::OrderTime()(get(entry, entry[0]),
                                                            check->get(entry, entry[1]));
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

std::string SequenceIdentity::describe(const CPD3::Data::SequenceIdentity &identity)
{
    std::string result;
    result += identity.getName().describe().toStdString();
    result += ",(";
    if (!FP::defined(identity.getStart())) {
        result += "-INFINITY";
    } else if (identity.getStart() < 1E6) {
        result += QString::number(identity.getStart(), 'f', 0).toStdString();
    } else {
        result += CPD3::Time::toISO8601(identity.getStart()).toStdString();
    }
    result += ",";
    if (!FP::defined(identity.getEnd())) {
        result += "+INFINITY";
    } else if (identity.getEnd() < 1E6) {
        result += QString::number(identity.getEnd(), 'f', 0).toStdString();
    } else {
        result += CPD3::Time::toISO8601(identity.getEnd()).toStdString();
    }
    result += "),";
    result += QString::number(identity.getPriority()).toStdString();
    return result;
}

void SequenceIdentity::meta_tostring(Engine::Entry &entry)
{
    auto result = describe(get(entry, entry[0]));
    entry.clear();
    entry.push(std::move(result));
    entry.propagate(1);
}

}
}
}