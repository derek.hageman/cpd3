/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <unordered_set>
#include <QRegularExpression>

#include "sequencesegment.hxx"
#include "sequencename.hxx"
#include "sequenceidentity.hxx"
#include "streamvalue.hxx"
#include "variant.hxx"
#include "time.hxx"
#include "datacore/sequencematch.hxx"


namespace CPD3 {
namespace Lua {
namespace Libs {

static const std::string metatableRegistryName = "CPD3_sequencesegment_metatable";

static CPD3::Data::SequenceName::Map<std::vector<CPD3::Data::SequenceValue>> assembleFromTable(
        Engine::Frame &frame,
        const Engine::Reference &table,
        bool ignoreStartEnd = false)
{
    CPD3::Data::SequenceName::Map<std::vector<CPD3::Data::SequenceValue>> result;

    Engine::Iterator it(frame, table);
    while (it.next()) {
        auto key = it.key();

        switch (key.getType()) {
        case Engine::Reference::Type::Nil:
        case Engine::Reference::Type::Boolean:
            break;
        case Engine::Reference::Type::Number: {
            auto value = SequenceValue::extract(it, it.key());
            if (!value.getName().isValid())
                continue;
            auto &target = result[value.getName()];
            target.emplace_back(std::move(value));
            continue;
        }
        case Engine::Reference::Type::String: {
            if (ignoreStartEnd) {
                auto str = key.toString();
                if (str == "start" || str == "end" || str == "front" || str == "back")
                    continue;
            }
            /* Fall through */
        }
        case Engine::Reference::Type::Data:
        case Engine::Reference::Type::Table:
            break;
        }

        auto parsedKey = SequenceName::extract(it, key);
        if (!parsedKey.isValid())
            continue;

        auto value = Variant::extract(it, it.value());

        auto &target = result[parsedKey];
        target.emplace_back(CPD3::Data::SequenceIdentity(std::move(parsedKey), FP::undefined(),
                                                         FP::undefined()),
                            CPD3::Data::Variant::Root(value));
    }

    return result;
}

static CPD3::Data::SequenceSegment constructFromFrame(Engine::Frame &frame)
{
    std::size_t n = frame.size();
    switch (n) {
    case 0:
        return CPD3::Data::SequenceSegment();
    case 1:
        return SequenceSegment::extract(frame, frame.front());
    default:
        break;
    }

    if (n < 3) {
        frame.throwError("Insufficient arguments to SequenceSegment constructor");
        return CPD3::Data::SequenceSegment();
    }

    Q_ASSERT(n >= 3);

    double start = FP::undefined();
    double end = FP::undefined();
    if (!frame[0].isNil()) {
        if (!frame[1].isNil()) {
            auto bounds = Time::extractBounds(frame, frame[0], frame[1]);
            start = bounds.start;
            end = bounds.end;
        } else {
            start = Time::extractSingle(frame, frame[0]);
        }
    } else if (!frame[1].isNil()) {
        end = Time::extractSingle(frame, frame[1]);
    }

    return CPD3::Data::SequenceSegment(start, end, assembleFromTable(frame, frame[2]));
}

void SequenceSegment::install(Engine::Table &target, Engine::Frame &frame)
{
    auto mt = pushMetatable(frame);


    {
        Engine::Assign assign(frame, mt, "__tostring");
        pushMethod<SequenceSegment>(assign, &SequenceSegment::meta_tostring);
    }

    {
        Engine::Assign assign(frame, mt, "__newindex");
        pushMethod<SequenceSegment>(assign, &SequenceSegment::meta_newindex);
    }

    {
        Engine::Assign assign(frame, mt, "__index");
        auto methods = assign.pushTable();

        {
            Engine::Frame mf(assign);
            pushMethod<SequenceSegment>(mf, &SequenceSegment::method_set);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "set");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "setValue");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceSegment>(mf, &SequenceSegment::method_get);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "get");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "getValue");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "lookup");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "exists");
            pushMethod<SequenceSegment>(ma, &SequenceSegment::method_exists);
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceSegment>(mf, &SequenceSegment::method_getNames);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "getNames");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "getUnits");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "find");
            pushMethod<SequenceSegment>(ma, &SequenceSegment::method_find);
        }

        pushMethod<SequenceSegment>(assign, &SequenceSegment::meta_index, 1);
    }

    frame.registry().set(metatableRegistryName, mt);
    {
        Engine::Frame local(frame);
        local.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            auto value = constructFromFrame(entry);
            entry.clear();
            entry.pushData<SequenceSegment>(std::move(value));
            entry.propagate(1);
        }));
        target.set("SequenceSegment", local.back());
    }
}

void SequenceSegment::initialize(const Engine::Reference &self, Engine::Frame &frame)
{
    frame.push(frame.registry(), metatableRegistryName);
    setMetatable(self, frame.back());
}

SequenceSegment::SequenceSegment() = default;

SequenceSegment::~SequenceSegment() = default;

SequenceSegment::SequenceSegment(const CPD3::Data::SequenceSegment &segment) : seg(segment)
{ }

SequenceSegment::SequenceSegment(CPD3::Data::SequenceSegment &&segment) : seg(std::move(segment))
{ }

CPD3::Data::SequenceSegment SequenceSegment::extract(Engine::Frame &frame,
                                                     const Engine::Reference &ref)
{
    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Number:
    case Engine::Reference::Type::String:
        return CPD3::Data::SequenceSegment();
    case Engine::Reference::Type::Data: {
        if (auto check = ref.toData<SequenceSegment>()) {
            return check->get();
        } else {
            /* Use table conversion */
            break;
        }
    }
    default:
        break;
    }

    Engine::Table table(ref);

    double segmentStart = FP::undefined();
    double segmentEnd = FP::undefined();
    {
        Engine::Frame local(frame);
        auto start = Time::pushStart(local, table);
        auto end = Time::pushEnd(local, table);

        if (!start.isNil()) {
            if (!end.isNil()) {
                auto bounds = Time::extractBounds(local, start, end);
                segmentStart = bounds.start;
                segmentEnd = bounds.end;
            } else {
                segmentStart = Time::extractSingle(local, start);
            }
        } else if (!end.isNil()) {
            segmentEnd = Time::extractSingle(local, end);
        }
    }

    return CPD3::Data::SequenceSegment(segmentStart, segmentEnd,
                                       assembleFromTable(frame, ref, true));
}

namespace {
class LookupData {
    CPD3::Data::SequenceName::Component station;
    CPD3::Data::SequenceName::Component archive;
    CPD3::Data::SequenceName::Component variable;
    CPD3::Data::SequenceName::Flavors exactFlavors;
    CPD3::Data::SequenceName::Flavors hasFlavors;
    CPD3::Data::SequenceName::Flavors lacksFlavors;

    static void assignComponent(CPD3::Data::SequenceName::Component &component,
                                Engine::Frame &frame,
                                const Engine::Reference &ref)
    {
        if (ref.getType() != Engine::Reference::Type::String) {
            component.clear();
            return;
        }
        component = ref.toString();
    }

    CPD3::Data::SequenceMatch::Element assembleInexact(bool exactStation,
                                                       bool exactArchive,
                                                       bool exactVariable)
    {
        CPD3::Data::SequenceMatch::Element::PatternList listHas;
        for (const auto &add : hasFlavors) {
            listHas.push_back(QString::fromStdString(add));
        }
        CPD3::Data::SequenceMatch::Element::PatternList listLacks;
        for (const auto &add : lacksFlavors) {
            listLacks.push_back(QString::fromStdString(add));
        }
        return CPD3::Data::SequenceMatch::Element(
                exactStation ? CPD3::Data::SequenceMatch::Element::Pattern()
                             : QString::fromStdString(station),
                exactArchive ? CPD3::Data::SequenceMatch::Element::Pattern()
                             : QString::fromStdString(archive),
                exactVariable ? CPD3::Data::SequenceMatch::Element::Pattern()
                              : QString::fromStdString(variable), listHas, listLacks);
    }

    CPD3::Data::SequenceMatch::Element assembleExact(bool useExactFlavors)
    {
        if (useExactFlavors) {
            return CPD3::Data::SequenceMatch::Element(station, archive, variable, exactFlavors);
        } else {
            CPD3::Data::SequenceMatch::Element::PatternList listHas;
            for (const auto &add : hasFlavors) {
                listHas.push_back(QString::fromStdString(add));
            }
            CPD3::Data::SequenceMatch::Element::PatternList listLacks;
            for (const auto &add : lacksFlavors) {
                listLacks.push_back(QString::fromStdString(add));
            }
            return CPD3::Data::SequenceMatch::Element(QString::fromStdString(station),
                                                      QString::fromStdString(archive),
                                                      QString::fromStdString(variable), listHas,
                                                      listLacks);
        }
    }

    static bool isExactMatcher(const CPD3::Data::SequenceName::Component &check)
    {
        return !check.empty() &&
                QRegularExpression::escape(QString::fromStdString(check)).toStdString() == check;
    }

public:
    LookupData() = default;

    void assignFromString(const std::string &raw)
    {
        auto components = Util::split_string(raw, ':', true);
        switch (components.size()) {
        case 0:
            return;
        case 1:
            variable = std::move(components[0]);
            return;
        case 2:
            archive = std::move(components[0]);
            variable = std::move(components[1]);
            return;
        default:
            station = std::move(components[0]);
            archive = std::move(components[1]);
            variable = std::move(components[2]);
            break;
        }

        for (std::size_t i = 3, max = components.size(); i < max; i++) {
            auto &check = components[i];
            if (check.empty())
                continue;
            if (check.front() == '+') {
                exactFlavors.clear();
                hasFlavors.insert(check.substr(1));
            } else if (check.front() == '-') {
                exactFlavors.clear();
                lacksFlavors.insert(check.substr(1));
            } else if (check.front() == '=') {
                hasFlavors.clear();
                lacksFlavors.clear();
                exactFlavors.insert(check.substr(1));
            } else {
                exactFlavors.clear();
                hasFlavors.insert(std::move(check));
            }
        }
    }

    void incomingStation(Engine::Frame &frame, const Engine::Reference &ref)
    { assignComponent(station, frame, ref); }

    void incomingArchive(Engine::Frame &frame, const Engine::Reference &ref)
    { assignComponent(archive, frame, ref); }

    void incomingVariable(Engine::Frame &frame, const Engine::Reference &ref)
    { assignComponent(variable, frame, ref); }

    void incomingFlavor(Engine::Frame &frame, const Engine::Reference &ref)
    {
        if (ref.getType() != Engine::Reference::Type::String)
            return;
        auto data = ref.toString();
        if (data.empty())
            return;
        if (data.front() == '+') {
            hasFlavors.insert(data.substr(1));
        } else if (data.front() == '-') {
            lacksFlavors.insert(data.substr(1));
        } else if (data.front() == '=') {
            exactFlavors.insert(data.substr(1));
        } else {
            hasFlavors.insert(std::move(data));
        }
    }

    void incomingExactFlavors(Engine::Frame &frame, const Engine::Reference &ref)
    {
        exactFlavors.clear();
        hasFlavors.clear();
        lacksFlavors.clear();
        Util::merge(SequenceName::convertFlavors(frame, ref), exactFlavors);
    }

    void incomingHasFlavors(Engine::Frame &frame, const Engine::Reference &ref)
    {
        exactFlavors.clear();
        hasFlavors.clear();
        Util::merge(SequenceName::convertFlavors(frame, ref), hasFlavors);
    }

    void incomingLacksFlavors(Engine::Frame &frame, const Engine::Reference &ref)
    {
        exactFlavors.clear();
        lacksFlavors.clear();
        Util::merge(SequenceName::convertFlavors(frame, ref), lacksFlavors);
    }

    CPD3::Data::SequenceName execute(const CPD3::Data::SequenceSegment &segment)
    {
        bool exactStation = isExactMatcher(station);
        bool exactArchive = isExactMatcher(archive);
        bool exactVariable = isExactMatcher(variable);
        bool haveExactFlavors = false;
        bool useExactFlavors = !exactFlavors.empty();
        if (exactFlavors.size() == 1 && exactFlavors.begin()->empty()) {
            exactFlavors.clear();
            haveExactFlavors = true;
        } else if (!exactFlavors.empty() && hasFlavors.empty() && lacksFlavors.empty()) {
            haveExactFlavors = true;
            for (const auto &check : exactFlavors) {
                if (QRegularExpression::escape(QString::fromStdString(check)).toStdString() ==
                        check)
                    continue;
                haveExactFlavors = false;
                break;
            }
        }

        if (exactStation && exactArchive && exactVariable && haveExactFlavors) {
            return CPD3::Data::SequenceName(std::move(station), std::move(archive),
                                            std::move(variable), std::move(exactFlavors));
        }

        /* No exact match, so we have to try and assemble one.  This involves inspecting
         * the existing possible names in the segment.  We only accept ones that match
         * something, if we've got a matcher.  We then prioritize ones that actually
         * match all components, but we'll accept partial component matches if we need
         * to.  This effectively allows existing parts to serve as defaults to fill
         * out the other components of the name while also allowing an exact match to
         * write to the correct target.  Transient ambiguity is permitted, but must be
         * resolved (a full match) in an existing component before completion. */


        auto locateInexact = assembleInexact(exactStation, exactArchive, exactVariable);
        auto locateExact = assembleExact(useExactFlavors);

        struct Matcher {
            CPD3::Data::SequenceMatch::Element &locateExact;
            enum class State {
                Unknown, Inexact, Ambiguous, Exact,
            } state;
            bool originalExact;

            Matcher(CPD3::Data::SequenceMatch::Element &locateExact, bool exact) : locateExact(
                    locateExact), state(exact ? State::Exact : State::Unknown), originalExact(exact)
            { }

            virtual ~Matcher() = default;

            virtual void apply(const CPD3::Data::SequenceName &name) = 0;

            virtual bool equal(const CPD3::Data::SequenceName &name) = 0;

            virtual bool integrate(const CPD3::Data::SequenceName &name)
            {
                switch (state) {
                case State::Unknown:
                    state = locateExact.matches(name) ? State::Exact : State::Inexact;
                    apply(name);
                    return true;
                case State::Inexact:
                case State::Ambiguous:
                    if (locateExact.matches(name)) {
                        state = State::Exact;
                        apply(name);
                        return true;
                    } else if (!equal(name)) {
                        state = State::Ambiguous;
                    }
                    break;
                case State::Exact:
                    if (originalExact)
                        break;
                    if (!equal(name)) {
                        if (locateExact.matches(name))
                            return false;
                    }
                    break;
                }
                return true;
            }

            bool isValid() const
            {
                switch (state) {
                case State::Unknown:
                    return false;
                case State::Inexact:
                    return true;
                case State::Ambiguous:
                    return false;
                case State::Exact:
                    return true;
                }
                Q_ASSERT(false);
                return false;
            }
        };

        struct MatchStation : public Matcher {
            CPD3::Data::SequenceName::Component &station;

            MatchStation(CPD3::Data::SequenceMatch::Element &locateExact,
                         bool exact,
                         CPD3::Data::SequenceName::Component &station) : Matcher(locateExact,
                                                                                 exact),
                                                                         station(station)
            { }

            virtual ~MatchStation() = default;

            virtual bool integrate(const CPD3::Data::SequenceName &name)
            {
                if (name.isDefaultStation())
                    return true;
                return Matcher::integrate(name);
            }

            virtual void apply(const CPD3::Data::SequenceName &name)
            { station = name.getStation(); }

            virtual bool equal(const CPD3::Data::SequenceName &name)
            { return station == name.getStation(); }
        };

        struct MatchArchive : public Matcher {
            CPD3::Data::SequenceName::Component &archive;

            MatchArchive(CPD3::Data::SequenceMatch::Element &locateExact,
                         bool exact,
                         CPD3::Data::SequenceName::Component &archive) : Matcher(locateExact,
                                                                                 exact),
                                                                         archive(archive)
            { }

            virtual ~MatchArchive() = default;

            virtual void apply(const CPD3::Data::SequenceName &name)
            { archive = name.getArchive(); }

            virtual bool equal(const CPD3::Data::SequenceName &name)
            { return archive == name.getArchive(); }
        };

        struct MatchVariable : public Matcher {
            CPD3::Data::SequenceName::Component &variable;

            MatchVariable(CPD3::Data::SequenceMatch::Element &locateExact,
                          bool exact,
                          CPD3::Data::SequenceName::Component &variable) : Matcher(locateExact,
                                                                                   exact),
                                                                           variable(variable)
            { }

            virtual ~MatchVariable() = default;

            virtual void apply(const CPD3::Data::SequenceName &name)
            { variable = name.getVariable(); }

            virtual bool equal(const CPD3::Data::SequenceName &name)
            { return variable == name.getVariable(); }
        };

        struct MatchFlavors : public Matcher {
            CPD3::Data::SequenceName::Flavors &flavors;

            MatchFlavors(CPD3::Data::SequenceMatch::Element &locateExact,
                         bool exact,
                         CPD3::Data::SequenceName::Flavors &flavors) : Matcher(locateExact, exact),
                                                                       flavors(flavors)
            { }

            virtual ~MatchFlavors() = default;

            virtual void apply(const CPD3::Data::SequenceName &name)
            { flavors = name.getFlavors(); }

            virtual bool equal(const CPD3::Data::SequenceName &name)
            { return flavors == name.getFlavors(); }
        };


        auto names = segment.getUnits(true);
        {
            /* First pass, try without any metadata to see if we hit an existing value */

            MatchStation matchStation(locateExact, exactStation, station);
            MatchArchive matchArchive(locateExact, exactArchive, archive);
            MatchVariable matchVariable(locateExact, exactVariable, variable);
            MatchFlavors matchFlavors(locateExact, haveExactFlavors, exactFlavors);

            bool haveAnyMeta = false;
            for (const auto &name : names) {
                if (!locateInexact.matches(name))
                    continue;
                if (name.isMeta()) {
                    haveAnyMeta = true;
                    continue;
                }

                if (!matchStation.integrate(name))
                    return CPD3::Data::SequenceName();
                if (!matchArchive.integrate(name))
                    return CPD3::Data::SequenceName();
                if (!matchVariable.integrate(name))
                    return CPD3::Data::SequenceName();
                if (!matchFlavors.integrate(name))
                    return CPD3::Data::SequenceName();
            }

            if (matchStation.isValid() &&
                    matchArchive.isValid() &&
                    matchVariable.isValid() &&
                    matchFlavors.isValid()) {
                return CPD3::Data::SequenceName(std::move(station), std::move(archive),
                                                std::move(variable), std::move(exactFlavors));
            }

            /* No metadata possible, so we're done */
            if (!haveAnyMeta)
                return CPD3::Data::SequenceName();
        }

        {
            /* Now try again with metadata, to see if the selection can disambiguate properly */

            MatchStation matchStation(locateExact, exactStation, station);
            MatchArchive matchArchive(locateExact, exactArchive, archive);
            MatchVariable matchVariable(locateExact, exactVariable, variable);
            MatchFlavors matchFlavors(locateExact, haveExactFlavors, exactFlavors);

            for (const auto &name : names) {
                if (!locateInexact.matches(name))
                    continue;

                if (!matchStation.integrate(name))
                    return CPD3::Data::SequenceName();
                if (!matchArchive.integrate(name))
                    return CPD3::Data::SequenceName();
                if (!matchVariable.integrate(name))
                    return CPD3::Data::SequenceName();
                if (!matchFlavors.integrate(name))
                    return CPD3::Data::SequenceName();
            }

            if (matchStation.isValid() &&
                    matchArchive.isValid() &&
                    matchVariable.isValid() &&
                    matchFlavors.isValid()) {
                return CPD3::Data::SequenceName(std::move(station), std::move(archive),
                                                std::move(variable), std::move(exactFlavors));
            }
        }

        return CPD3::Data::SequenceName();
    }
};
}

static CPD3::Data::SequenceName lookup(const CPD3::Data::SequenceSegment &segment,
                                       Engine::Frame &frame,
                                       const Engine::Reference &ref)
{
    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Number:
        return CPD3::Data::SequenceName();
    case Engine::Reference::Type::String: {
        LookupData data;
        data.assignFromString(ref.toString());
        return data.execute(segment);
    }
    case Engine::Reference::Type::Data:
        if (auto check = ref.toData<SequenceName>()) {
            return check->get();
        } else if (auto check = ref.toData<SequenceIdentity>()) {
            return check->get(frame, ref).getName();
        } else if (auto check = ref.toData<SequenceValue>()) {
            return check->getName(frame, ref);
        } else if (auto check = ref.toData<ArchiveErasure>()) {
            return check->getName(frame, ref);
        } else {
            /* Use table conversion */
            break;
        }
    default:
        break;
    }

    LookupData data;
    {
        Engine::Iterator it(frame, ref);
        while (it.next()) {
            auto key = it.key();

            switch (key.getType()) {
            case Engine::Reference::Type::Number: {
                switch (it.key().toInteger()) {
                case 0:
                    break;
                case 1:
                    data.incomingStation(it, it.value());
                    break;
                case 2:
                    data.incomingArchive(it, it.value());
                    break;
                case 3:
                    data.incomingVariable(it, it.value());
                    break;
                default:
                    data.incomingFlavor(it, it.value());
                    break;
                }
                break;
            }
            default:
                break;
            }
        }
    }

    {
        Engine::Frame local(frame);
        local.push(ref, "station");
        auto check = local.back();
        if (!check.isNil()) {
            data.incomingStation(local, check);
        }
    }
    {
        Engine::Frame local(frame);
        local.push(ref, "archive");
        auto check = local.back();
        if (!check.isNil()) {
            data.incomingArchive(local, check);
        }
    }
    {
        Engine::Frame local(frame);
        local.push(ref, "variable");
        auto check = local.back();
        if (!check.isNil()) {
            data.incomingVariable(local, check);
        }
    }
    {
        Engine::Frame local(frame);
        local.push(ref, "flavors");
        auto check = local.back();
        if (!check.isNil()) {
            data.incomingExactFlavors(local, check);
        }
    }
    {
        Engine::Frame local(frame);
        local.push(ref, "exactFlavors");
        auto check = local.back();
        if (!check.isNil()) {
            data.incomingExactFlavors(local, check);
        }
    }
    {
        Engine::Frame local(frame);
        local.push(ref, "hasFlavors");
        auto check = local.back();
        if (!check.isNil()) {
            data.incomingHasFlavors(local, check);
        }
    }
    {
        Engine::Frame local(frame);
        local.push(ref, "lacksFlavors");
        auto check = local.back();
        if (!check.isNil()) {
            data.incomingLacksFlavors(local, check);
        }
    }

    return data.execute(segment);
}

static CPD3::Data::SequenceName lookup(const CPD3::Data::SequenceSegment &segment,
                                       Engine::Frame &frame,
                                       std::size_t begin = 1)
{
    auto sz = frame.size();
    if (sz <= begin)
        return CPD3::Data::SequenceName();
    if (sz == begin + 1)
        return lookup(segment, frame, frame[begin]);

    LookupData data;
    if (sz == begin + 2) {
        data.incomingArchive(frame, frame[begin]);
        data.incomingVariable(frame, frame[begin + 1]);
    } else {
        Q_ASSERT(sz >= begin + 3);

        data.incomingStation(frame, frame[begin]);
        data.incomingArchive(frame, frame[begin + 1]);
        data.incomingVariable(frame, frame[begin + 2]);

        for (std::size_t i = begin + 3; i < sz; ++i) {
            data.incomingFlavor(frame, frame[i]);
        }
    }

    return data.execute(segment);
}

void SequenceSegment::meta_index(Engine::Entry &entry,
                                 const std::vector<Engine::Reference> &upvalues)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid index");
        return;
    }
    if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
        return;
    auto key = entry[1].toString();
    if (Util::equal_insensitive(key, "start", "front")) {
        entry.push(seg.getStart());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (Util::equal_insensitive(key, "end", "back")) {
        entry.push(seg.getEnd());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    }

    auto name = lookup(seg, entry);
    if (!name.isValid()) {
        entry.clear();
        entry.pushNil();
        entry.propagate(1);
        return;
    }

    Variant::pushLookup(seg.getValue(name), entry);
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void SequenceSegment::meta_newindex(Engine::Entry &entry)
{
    if (entry.size() < 3) {
        entry.throwError("Invalid assignment");
        return;
    }
    auto key = entry[1].toString();
    if (Util::equal_insensitive(key, "start", "front")) {
        seg.setStart(Time::extractSingle(entry, entry[2]));
        return;
    } else if (Util::equal_insensitive(key, "end", "back")) {
        seg.setEnd(Time::extractSingle(entry, entry[2]));
        return;
    }

    auto name = lookup(seg, entry, entry[1]);
    if (!name.isValid()) {
        entry.throwError("Unable to resolve target name");
        return;
    }

    auto target = seg.getValue(name);
    Variant::set(target, entry, entry[2]);
}

void SequenceSegment::method_set(Engine::Entry &entry)
{
    if (entry.size() < 3) {
        entry.throwError("Invalid arguments");
        return;
    }

    auto name = lookup(seg, entry, 2);
    if (!name.isValid()) {
        entry.throwError("Unable to resolve target name");
        return;
    }

    auto target = seg.getValue(name);
    Variant::set(target, entry, entry[1]);
}

void SequenceSegment::method_get(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }

    auto name = lookup(seg, entry);
    if (!name.isValid()) {
        entry.clear();
        entry.pushNil();
        entry.propagate(1);
        return;
    }

    entry.pushData<Variant>(seg.getValue(name));
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void SequenceSegment::method_exists(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }

    auto name = lookup(seg, entry);
    if (!name.isValid()) {
        entry.clear();
        entry.pushNil();
        entry.propagate(1);
        return;
    }

    entry.push(seg.exists(name));
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void SequenceSegment::method_getNames(Engine::Entry &entry)
{
    bool includeDefault = false;
    if (entry.size() >= 2)
        includeDefault = entry[1].toBoolean();

    std::vector<CPD3::Data::SequenceName> sorted;
    Util::append(seg.getUnits(includeDefault), sorted);
    std::sort(sorted.begin(), sorted.end(), CPD3::Data::SequenceName::OrderLogical());

    entry.clear();
    auto result = entry.pushTable();
    {
        Engine::Append append(entry, result, true);
        for (const auto &name : sorted) {
            append.pushData<SequenceName>(name);
        }
    }
    entry.propagate(1);
}

void SequenceSegment::method_find(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }

    auto name = lookup(seg, entry);
    if (!name.isValid()) {
        entry.clear();
        entry.pushNil();
        entry.propagate(1);
        return;
    }

    entry.pushData<SequenceName>(std::move(name));
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void SequenceSegment::meta_tostring(Engine::Entry &entry)
{
    std::string result;
    if (!FP::defined(seg.getStart())) {
        result += "-INFINITY";
    } else if (seg.getStart() < 1E6) {
        result += QString::number(seg.getStart(), 'f', 0).toStdString();
    } else {
        result += CPD3::Time::toISO8601(seg.getStart()).toStdString();
    }
    result += ",";
    if (!FP::defined(seg.getEnd())) {
        result += "+INFINITY";
    } else if (seg.getEnd() < 1E6) {
        result += QString::number(seg.getEnd(), 'f', 0).toStdString();
    } else {
        result += CPD3::Time::toISO8601(seg.getEnd()).toStdString();
    }
    result += ",(";
    bool first = true;
    for (const auto &name : seg.getUnits(true)) {
        if (!first)
            result += ",";
        first = false;
        result += name.describe().toStdString();
        result += "=";
        result += seg.getValue(name).describe();
    }
    result += ")";

    entry.clear();
    entry.push(std::move(result));
    entry.propagate(1);
}

}
}
}