/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "streamvalue.hxx"
#include "sequencename.hxx"
#include "sequenceidentity.hxx"
#include "variant.hxx"
#include "time.hxx"


namespace CPD3 {
namespace Lua {
namespace Libs {

Engine::Table StreamValue::pushMetatable(Engine::Frame &frame,
                                         const std::function<void(Engine::Frame &,
                                                                  Engine::Table &)> &addMethods)
{
    auto mt = Engine::Data::pushMetatable(frame);

    {
        Engine::Assign assign(frame, mt, "__tostring");
        pushMethod<StreamValue>(assign, [](StreamValue *value, Engine::Entry &entry) {
            return value->meta_tostring(entry);
        });
    }
    {
        Engine::Assign assign(frame, mt, "__newindex");
        pushMethod<StreamValue>(assign, [](StreamValue *value, Engine::Entry &entry) {
            if (!value->meta_newindex(entry)) {
                entry.throwError("Invalid assignment");
            }
        });
    }

    {
        Engine::Assign assign(frame, mt, "__index");
        {
            auto methods = assign.pushTable();

            {
                Engine::Assign ma(assign, methods, "overlayLessThan");
                pushMethod<StreamValue>(ma, &StreamValue::method_overlayLessThan);
            }
            {
                Engine::Assign ma(assign, methods, "timeLessThan");
                pushMethod<StreamValue>(ma, &StreamValue::method_timeLessThan);
            }

            if (addMethods)
                addMethods(assign, methods);
        }
        pushMethod<StreamValue>(assign, [](StreamValue *value,
                                           Engine::Entry &entry,
                                           const std::vector<Engine::Reference> &upvalues) {
            if (entry.size() < 2) {
                entry.throwError("Invalid index");
                return;
            }
            if (!value->meta_index(entry, upvalues)) {
                entry.clear();
                entry.pushNil();
                entry.propagate(1);
            }
        }, 1);
    }

    return mt;
}

StreamValue::StreamValue() : identity(), detachState(DetachState::AllAttached)
{ }

StreamValue::~StreamValue() = default;

StreamValue::StreamValue(const CPD3::Data::SequenceIdentity &identity) : identity(identity),
                                                                         detachState(
                                                                                 DetachState::AllAttached)
{ }

StreamValue::StreamValue(CPD3::Data::SequenceIdentity &&identity) : identity(std::move(identity)),
                                                                    detachState(
                                                                            DetachState::AllAttached)
{ }

void StreamValue::assignFromTable(CPD3::Data::SequenceIdentity &identity,
                                  Engine::Frame &frame,
                                  const Engine::Table &table)
{
    SequenceIdentity::assignFromTable(identity, frame, table);
    {
        Engine::Frame local(frame);
        local.push(table, "identity");
        switch (local.back().getType()) {
        case Engine::Reference::Type::Nil:
        case Engine::Reference::Type::Boolean:
        case Engine::Reference::Type::Number:
            break;
        case Engine::Reference::Type::Data:
        case Engine::Reference::Type::String:
            identity = SequenceIdentity::extract(local, local.back());
            break;
        case Engine::Reference::Type::Table:
            SequenceIdentity::assignFromTable(identity, local, local.back());
            break;
        }
    }
    {
        Engine::Frame local(frame);
        local.push(table, "name");
        switch (local.back().getType()) {
        case Engine::Reference::Type::Nil:
        case Engine::Reference::Type::Boolean:
        case Engine::Reference::Type::Number:
            break;
        case Engine::Reference::Type::Data:
        case Engine::Reference::Type::String:
            identity.setName(SequenceName::extract(local, local.back()));
            break;
        case Engine::Reference::Type::Table:
            SequenceName::assignFromTable(identity.getName(), local, local.back());
            break;
        }
    }
}

void StreamValue::fetchDetachedName(Engine::Frame &frame, const Engine::Reference &self) const
{
    Q_ASSERT(hasDetachedName());

    Engine::Frame local(frame);
    auto mt = Engine::Table(self).pushMetatable(local);
    local.push(mt, "name");
    local.moveBackTo(0);
    local.propagate(1);
}

void StreamValue::fetchDetachedIdentity(Engine::Frame &frame, const Engine::Reference &self) const
{
    Q_ASSERT(hasDetachedIdentity());

    Engine::Frame local(frame);
    auto mt = Engine::Table(self).pushMetatable(local);
    local.push(mt, "identity");
    local.moveBackTo(0);
    local.propagate(1);
}

CPD3::Data::SequenceIdentity StreamValue::getIdentity(Engine::Frame &frame,
                                                      const Engine::Reference &self) const
{
    if (!hasDetachedIdentity()) {
        if (hasDetachedName()) {
            return CPD3::Data::SequenceIdentity(getName(frame, self), identity.getStart(),
                                                identity.getEnd(), identity.getPriority());
        }
        return identity;
    }

    Engine::Frame local(frame);
    fetchDetachedIdentity(local, self);
    if (auto check = local.back().toData<SequenceIdentity>()) {
        return check->get(local, local.back());
    } else {
        frame.throwError("Invalid detached SequenceIdentity");
    }
    return identity;
}

CPD3::Data::SequenceName StreamValue::getName(Engine::Frame &frame,
                                              const Engine::Reference &self) const
{
    if (!hasDetachedName()) {
        if (hasDetachedIdentity()) {
            return getIdentity(frame, self).getName();
        }
        return identity.getName();
    }

    Engine::Frame local(frame);
    fetchDetachedName(local, self);
    if (auto check = local.back().toData<SequenceName>()) {
        return check->get();
    } else {
        frame.throwError("Invalid detached SequenceName");
    }
    return identity.getName();
}

double StreamValue::getStart(Engine::Frame &frame, const Engine::Reference &self) const
{
    if (!hasDetachedIdentity()) {
        return identity.getStart();
    }
    Engine::Frame local(frame);
    fetchDetachedIdentity(local, self);
    if (auto check = local.back().toData<SequenceIdentity>()) {
        return check->get(local, local.back()).getStart();
    } else {
        frame.throwError("Invalid detached SequenceIdentity");
    }
    return identity.getStart();
}

static Engine::Table detachMetatable(Engine::Frame &frame, const Engine::Reference &self)
{
    auto mt = frame.pushTable();

    Engine::Table(self).pushMetatable(frame);
    auto base = frame.back();
    {
        Engine::Iterator it(frame, base);
        while (it.next()) {
            mt.set(it.key(), it.value());
        }
    }
    frame.pop();

    return std::move(mt);
}

void StreamValue::assignName(Engine::Frame &frame,
                             const Engine::Reference &self,
                             const Engine::Reference &name)
{
    switch (detachState) {
    case DetachState::AllAttached: {
        Engine::Frame local(frame);
        auto mt = detachMetatable(local, self);
        mt.set("name", name);
        setMetatable(self, mt);

        detachState = DetachState::NameDetached;
        break;
    }
    case DetachState::IdentityDetached: {
        Engine::Frame local(frame);
        auto mt = Engine::Table(self).pushMetatable(local);
        mt.set("name", name);

        fetchDetachedIdentity(local, self);
        if (auto check = local.back().toData<SequenceIdentity>()) {
            check->detachName(local, local.back(), name);
        } else {
            local.throwError("Invalid detached SequenceIdentity");
        }

        detachState = DetachState::AllDetached;
        break;
    }
    case DetachState::NameDetached: {
        Engine::Frame local(frame);
        auto mt = Engine::Table(self).pushMetatable(local);
        mt.set("name", name);
        break;
    }
    case DetachState::AllDetached: {
        Engine::Frame local(frame);
        auto mt = Engine::Table(self).pushMetatable(local);
        mt.set("name", name);

        fetchDetachedIdentity(local, self);
        if (auto check = local.back().toData<SequenceIdentity>()) {
            check->detachName(local, local.back(), name);
        } else {
            local.throwError("Invalid detached SequenceIdentity");
        }
        break;
    }
    }
}

void StreamValue::assignIdentity(Engine::Frame &frame,
                                 const Engine::Reference &self,
                                 const Engine::Reference &identity)
{
    switch (detachState) {
    case DetachState::AllAttached: {
        Engine::Frame local(frame);
        auto mt = detachMetatable(local, self);
        mt.set("identity", identity);

        if (auto check = identity.toData<SequenceIdentity>()) {
            if (check->hasDetachedName()) {
                detachState = DetachState::AllDetached;
                check->pushDetachedName(local, identity);
                mt.set("name", local.back());
            }
        } else {
            local.throwError("Invalid detached SequenceIdentity");
        }

        setMetatable(self, mt);
        detachState = DetachState::IdentityDetached;
        break;
    }
    case DetachState::NameDetached: {
        Engine::Frame local(frame);
        auto mt = Engine::Table(self).pushMetatable(local);
        mt.set("identity", identity);

        if (auto check = identity.toData<SequenceIdentity>()) {
            check->pushDetachedName(local, identity);
            mt.set("name", local.back());
        } else {
            local.throwError("Invalid detached SequenceIdentity");
        }

        detachState = DetachState::AllDetached;
        break;
    }
    case DetachState::IdentityDetached: {
        Engine::Frame local(frame);
        auto mt = Engine::Table(self).pushMetatable(local);
        mt.set("identity", identity);

        if (auto check = identity.toData<SequenceIdentity>()) {
            if (check->hasDetachedName()) {
                detachState = DetachState::AllDetached;
                check->pushDetachedName(local, identity);
                mt.set("name", local.back());
            }
        } else {
            local.throwError("Invalid detached SequenceIdentity");
        }
        break;
    }
    case DetachState::AllDetached: {
        Engine::Frame local(frame);
        auto mt = Engine::Table(self).pushMetatable(local);
        mt.set("identity", identity);

        if (auto check = identity.toData<SequenceIdentity>()) {
            check->pushDetachedName(local, identity);
            mt.set("name", local.back());
        } else {
            local.throwError("Invalid detached SequenceIdentity");
        }
        break;
    }
    }
}

void StreamValue::pushDetachedName(Engine::Frame &frame, const Engine::Reference &self)
{
    switch (detachState) {
    case DetachState::NameDetached:
    case DetachState::AllDetached:
        fetchDetachedName(frame, self);
        break;
    case DetachState::IdentityDetached: {
        Engine::Frame local(frame);
        fetchDetachedIdentity(local, self);
        if (auto check = local.back().toData<SequenceIdentity>()) {
            check->pushDetachedName(local, local.back());
        } else {
            local.throwError("Invalid detached SequenceIdentity");
        }
        local.replaceWithBack(0);

        auto mt = Engine::Table(self).pushMetatable(local);
        mt.set("name", local.front());

        local.propagate(1);

        detachState = DetachState::AllDetached;
        break;
    }
    case DetachState::AllAttached:
        frame.pushData<SequenceName>(identity.getName());
        assignName(frame, self, frame.back());
        break;
    }
}

void StreamValue::pushDetachedIdentity(Engine::Frame &frame, const Engine::Reference &self)
{
    switch (detachState) {
    case DetachState::IdentityDetached:
    case DetachState::AllDetached:
        fetchDetachedIdentity(frame, self);
        break;
    case DetachState::NameDetached: {
        Engine::Frame local(frame);
        fetchDetachedName(local, self);
        auto name = local.back();
        if (auto check = name.toData<SequenceName>()) {
            auto id = local.pushData<SequenceIdentity>(
                    CPD3::Data::SequenceIdentity(check->get(), identity.getStart(),
                                                 identity.getEnd(), identity.getPriority()));
            id->detachName(local, local.back(), name);
        } else {
            local.throwError("Invalid detached SequenceIdentity");
        }
        local.replaceWithBack(0);

        auto mt = Engine::Table(self).pushMetatable(local);
        mt.set("identity", local.front());

        local.propagate(1);

        detachState = DetachState::AllDetached;
        break;
    }
    case DetachState::AllAttached:
        frame.pushData<SequenceIdentity>(identity);
        assignIdentity(frame, self, frame.back());
        break;
    }
}

template<typename Functor>
static void operateOnName(StreamValue *value, Engine::Entry &entry, Functor functor)
{
    if (!value->hasDetachedName()) {
        if (!value->hasDetachedIdentity()) {
            functor(entry, value->getIdentity().getName());
            return;
        }

        Q_ASSERT(!entry.empty());

        SequenceIdentity *identity = nullptr;
        {
            Engine::Frame local(entry);
            value->pushDetachedIdentity(local, entry.front());
            identity = local.back().toData<SequenceIdentity>();
        }
        if (!identity) {
            entry.throwError("Invalid identity reference");
            return;
        }
        if (!identity->hasDetachedName()) {
            functor(entry, identity->get().getName());
            return;
        }
    }

    Q_ASSERT(!entry.empty());

    SequenceName *name = nullptr;
    {
        Engine::Frame local(entry);
        value->pushDetachedName(local, entry.front());
        name = local.back().toData<SequenceName>();
    }
    if (!name) {
        entry.throwError("Invalid name reference");
        return;
    }
    functor(entry, name->get());
}

template<typename Functor>
static void operateOnIdentity(StreamValue *value, Engine::Entry &entry, Functor functor)
{
    if (!value->hasDetachedIdentity()) {
        functor(entry, value->getIdentityAttached());
        return;
    }

    Q_ASSERT(!entry.empty());

    SequenceIdentity *identity = nullptr;
    {
        Engine::Frame local(entry);
        value->pushDetachedIdentity(local, entry.front());
        identity = local.back().toData<SequenceIdentity>();
    }
    if (!identity) {
        entry.throwError("Invalid identity reference");
        return;
    }
    functor(entry, identity->getAttached());
}

bool StreamValue::meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
{
    if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
        return true;
    auto key = entry[1].toString();
    if (key == "name") {
        pushDetachedName(entry, entry.front());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return true;
    } else if (Util::equal_insensitive(key, "start", "front")) {
        operateOnIdentity(this, entry,
                          [](Engine::Entry &entry, const CPD3::Data::SequenceIdentity &identity) {
                              entry.push(identity.getStart());
                              entry.replaceWithBack(0);
                              entry.propagate(1);
                          });
        return true;
    } else if (Util::equal_insensitive(key, "end", "back")) {
        operateOnIdentity(this, entry,
                          [](Engine::Entry &entry, const CPD3::Data::SequenceIdentity &identity) {
                              entry.push(identity.getEnd());
                              entry.replaceWithBack(0);
                              entry.propagate(1);
                          });
        return true;
    } else if (key == "station") {
        operateOnName(this, entry, [](Engine::Entry &entry, const CPD3::Data::SequenceName &name) {
            entry.push(name.getStation());
            entry.replaceWithBack(0);
            entry.propagate(1);
        });
        return true;
    } else if (key == "archive") {
        operateOnName(this, entry, [](Engine::Entry &entry, const CPD3::Data::SequenceName &name) {
            entry.push(name.getArchive());
            entry.replaceWithBack(0);
            entry.propagate(1);
        });
        return true;
    } else if (key == "variable") {
        operateOnName(this, entry, [](Engine::Entry &entry, const CPD3::Data::SequenceName &name) {
            entry.push(name.getVariable());
            entry.replaceWithBack(0);
            entry.propagate(1);
        });
        return true;
    } else if (key == "flavors") {
        operateOnName(this, entry, [](Engine::Entry &entry, const CPD3::Data::SequenceName &name) {
            SequenceName::pushFlavors(entry, name.getFlavors());
            entry.replaceWithBack(0);
            entry.propagate(1);
        });
        return true;
    } else if (key == "priority") {
        operateOnIdentity(this, entry,
                          [](Engine::Entry &entry, const CPD3::Data::SequenceIdentity &identity) {
                              entry.push(identity.getPriority());
                              entry.replaceWithBack(0);
                              entry.propagate(1);
                          });
        return true;
    } else if (key == "identity") {
        pushDetachedIdentity(entry, entry.front());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return true;
    }

    return false;
}

bool StreamValue::meta_newindex(Engine::Entry &entry)
{
    if (entry.size() < 3) {
        entry.throwError("Invalid assignment");
        return true;
    }
    auto key = entry[1].toString();
    if (Util::equal_insensitive(key, "start", "front")) {
        operateOnIdentity(this, entry,
                          [](Engine::Entry &entry, CPD3::Data::SequenceIdentity &identity) {
                              identity.setStart(Time::extractSingle(entry, entry[2]));
                          });
        return true;
    } else if (Util::equal_insensitive(key, "end", "back")) {
        operateOnIdentity(this, entry,
                          [](Engine::Entry &entry, CPD3::Data::SequenceIdentity &identity) {
                              identity.setEnd(Time::extractSingle(entry, entry[2]));
                          });
        return true;
    } else if (key == "station") {
        operateOnName(this, entry, [](Engine::Entry &entry, CPD3::Data::SequenceName &name) {
            name.setStation(entry[2].toString());
        });
        return true;
    } else if (key == "archive") {
        operateOnName(this, entry, [](Engine::Entry &entry, CPD3::Data::SequenceName &name) {
            name.setArchive(entry[2].toString());
        });
        return true;
    } else if (key == "variable") {
        operateOnName(this, entry, [](Engine::Entry &entry, CPD3::Data::SequenceName &name) {
            name.setVariable(entry[2].toString());
        });
        return true;
    } else if (key == "flavors") {
        operateOnName(this, entry, [](Engine::Entry &entry, CPD3::Data::SequenceName &name) {
            name.setFlavors(SequenceName::convertFlavors(entry, entry[2]));
        });
        return true;
    } else if (key == "priority") {
        operateOnIdentity(this, entry,
                          [](Engine::Entry &entry, CPD3::Data::SequenceIdentity &identity) {
                              auto i = entry[2].toInteger();
                              if (INTEGER::defined(i))
                                  identity.setPriority(static_cast<int>(i));
                          });
        return true;
    } else if (key == "name") {
        if (entry[2].toData<SequenceName>()) {
            assignName(entry, entry[0], entry[2]);
        } else {
            entry.pushData<SequenceName>(SequenceName::extract(entry, entry[2]));
            assignName(entry, entry[0], entry.back());
        }
        return true;
    } else if (key == "identity") {
        if (entry[2].toData<SequenceIdentity>()) {
            assignIdentity(entry, entry[0], entry[2]);
        } else {
            entry.pushData<SequenceIdentity>(SequenceIdentity::extract(entry, entry[2]));
            assignIdentity(entry, entry[0], entry.back());
        }
        return true;
    }

    return false;
}

void StreamValue::method_overlayLessThan(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto check = entry[1].toData<StreamValue>();
    if (!check) {
        entry.throwError("Cannot compare with an unknown type");
        return;
    }
    bool result = CPD3::Data::SequenceIdentity::OrderOverlay()(getIdentity(entry, entry[0]),
                                                               check->getIdentity(entry, entry[1]));
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void StreamValue::method_timeLessThan(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto check = entry[1].toData<StreamValue>();
    if (!check) {
        entry.throwError("Cannot compare with an unknown type");
        return;
    }
    bool result = CPD3::Data::SequenceIdentity::OrderTime()(getIdentity(entry, entry[0]),
                                                            check->getIdentity(entry, entry[1]));
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}


static const std::string metatableSequenceValueRegistryName = "CPD3_sequencevalue_metatable";

Engine::Table SequenceValue::pushMetatable(Engine::Frame &target,
                                           const std::function<void(Engine::Frame &,
                                                                    Engine::Table &)> &addMethods)
{
    return StreamValue::pushMetatable(target,
                                      [&addMethods](Engine::Frame &frame, Engine::Table &methods) {

                                          {
                                              Engine::Assign ma(frame, methods, "detach");
                                              pushMethod<SequenceValue>(ma,
                                                                        &SequenceValue::method_detach);
                                          }
                                          {
                                              Engine::Assign ma(frame, methods, "set");
                                              pushMethod<SequenceValue>(ma,
                                                                        &SequenceValue::method_set);
                                          }

                                          if (addMethods)
                                              addMethods(frame, methods);
                                      });
}

void SequenceValue::install(Engine::Table &target, Engine::Frame &frame)
{
    auto mt = pushMetatable(frame);
    frame.registry().set(metatableSequenceValueRegistryName, mt);

    {
        Engine::Frame local(frame);
        local.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            auto value = constructFromFrame(entry);
            entry.clear();
            entry.pushData<SequenceValue>(std::move(value));
            entry.propagate(1);
        }));
        target.set("SequenceValue", local.back());
    }
}

void SequenceValue::initialize(const Engine::Reference &self, Engine::Frame &frame)
{
    frame.push(frame.registry(), metatableSequenceValueRegistryName);
    setMetatable(self, frame.back());
}

SequenceValue::SequenceValue() : contents(CPD3::Data::Variant::Write::empty())
{ }

SequenceValue::~SequenceValue() = default;

SequenceValue::SequenceValue(const CPD3::Data::SequenceValue &value) : StreamValue(
        value.getIdentity())
{
    CPD3::Data::Variant::Root temp(value.root());
    contents = temp.write();
}

SequenceValue::SequenceValue(CPD3::Data::SequenceValue &&value) : StreamValue(
        std::move(value.getIdentity())), contents(value.write())
{ }

void SequenceValue::assignFromTable(CPD3::Data::SequenceValue &value,
                                    Engine::Frame &frame,
                                    const Engine::Table &table)
{
    StreamValue::assignFromTable(value.getIdentity(), frame, table);
    {
        Engine::Frame local(frame);
        local.push(table, "value");
        if (!local.back().isNil()) {
            auto wr = value.write();
            Variant::set(wr, local, local.back());
        }
    }
}

CPD3::Data::SequenceValue SequenceValue::constructFromFrame(Engine::Frame &frame)
{
    std::size_t n = frame.size();
    switch (n) {
    case 0:
        return CPD3::Data::SequenceValue();
    case 1:
        return extract(frame, frame.front());
    default:
        break;
    }

    if (n < 2) {
        frame.throwError("Insufficient arguments to SequenceValue constructor");
        return CPD3::Data::SequenceValue();
    }

    Q_ASSERT(n >= 2);

    return CPD3::Data::SequenceValue(SequenceIdentity::extract(frame, frame[0]),
                                     CPD3::Data::Variant::Root(Variant::extract(frame, frame[1])));
}

CPD3::Data::SequenceValue SequenceValue::get(Engine::Frame &frame,
                                             const Engine::Reference &self) const
{
    if (hasDetachedIdentity() || hasDetachedName()) {
        return CPD3::Data::SequenceValue(getIdentity(frame, self),
                                         CPD3::Data::Variant::Root(contents));
    }
    return CPD3::Data::SequenceValue(getIdentity(), CPD3::Data::Variant::Root(contents));
}

CPD3::Data::SequenceValue SequenceValue::extract(Engine::Frame &frame, const Engine::Reference &ref)
{
    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Number:
    case Engine::Reference::Type::String:
        return CPD3::Data::SequenceValue();
    case Engine::Reference::Type::Data: {
        if (auto check = ref.toData<SequenceValue>()) {
            return check->get(frame, ref);
        } else if (auto check = ref.toData<ArchiveValue>()) {
            return CPD3::Data::SequenceValue(check->get(frame, ref));
        } else if (auto check = ref.toData<ArchiveErasure>()) {
            return CPD3::Data::SequenceValue(check->get(frame, ref));
        } else if (auto check = ref.toData<SequenceIdentity>()) {
            return CPD3::Data::SequenceValue(check->get());
        } else if (auto check = ref.toData<SequenceName>()) {
            return CPD3::Data::SequenceValue(check->get());
        } else {
            /* Use table conversion */
            break;
        }
    }
    default:
        break;
    }

    CPD3::Data::SequenceValue result;
    assignFromTable(result, frame, ref);
    return result;
}

void SequenceValue::method_detach(Engine::Entry &entry)
{
    contents.detachFromRoot();
}

void SequenceValue::method_set(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    Variant::set(contents, entry, entry[1]);
}

void SequenceValue::meta_tostring(Engine::Entry &entry)
{
    std::string result;
    result += "(";
    result += SequenceIdentity::describe(getIdentity(entry, entry[0]));
    result += "),";
    result += contents.describe();

    entry.clear();
    entry.push(std::move(result));
    entry.propagate(1);
}

bool SequenceValue::meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
{
    if (StreamValue::meta_index(entry, upvalues))
        return true;
    auto key = entry.back().toString();
    if (key == "value") {
        entry.pushData<Variant>(contents);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return true;
    }
    return false;
}

bool SequenceValue::meta_newindex(Engine::Entry &entry)
{
    if (StreamValue::meta_newindex(entry))
        return true;
    auto key = entry[1].toString();
    if (key == "value") {
        contents = Variant::extract(entry, entry[2]);
        return true;
    }
    return false;
}


static const std::string metatableArchiveValueRegistryName = "CPD3_archivevalue_metatable";

void ArchiveValue::install(Engine::Table &target, Engine::Frame &frame)
{
    auto mt = SequenceValue::pushMetatable(frame);
    frame.registry().set(metatableArchiveValueRegistryName, mt);

    {
        Engine::Frame local(frame);
        local.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            auto value = constructFromFrame(entry);
            entry.clear();
            entry.pushData<ArchiveValue>(std::move(value));
            entry.propagate(1);
        }));
        target.set("ArchiveValue", local.back());
    }
}

void ArchiveValue::initialize(const Engine::Reference &self, Engine::Frame &frame)
{
    frame.push(frame.registry(), metatableArchiveValueRegistryName);
    setMetatable(self, frame.back());
}

ArchiveValue::ArchiveValue() : modified(FP::undefined()), remoteReferenced(false)
{ }

ArchiveValue::~ArchiveValue() = default;

ArchiveValue::ArchiveValue(const CPD3::Data::ArchiveValue &value) : SequenceValue(value),
                                                                    modified(value.getModified()),
                                                                    remoteReferenced(
                                                                            value.isRemoteReferenced())
{ }

ArchiveValue::ArchiveValue(CPD3::Data::ArchiveValue &&value) : SequenceValue(std::move(value)),
                                                               modified(value.getModified()),
                                                               remoteReferenced(
                                                                       value.isRemoteReferenced())
{ }

void ArchiveValue::assignFromTable(CPD3::Data::ArchiveValue &value,
                                   Engine::Frame &frame,
                                   const Engine::Table &table)
{
    SequenceValue::assignFromTable(value, frame, table);
    {
        Engine::Frame local(frame);
        local.push(table, "modified");
        if (!local.back().isNil())
            value.setModified(Time::extractSingle(local, local.back()));
    }
}

CPD3::Data::ArchiveValue ArchiveValue::constructFromFrame(Engine::Frame &frame)
{
    std::size_t n = frame.size();
    switch (n) {
    case 0:
        return CPD3::Data::ArchiveValue();
    case 1:
        return extract(frame, frame.front());
    default:
        break;
    }

    if (n < 2) {
        frame.throwError("Insufficient arguments to ArchiveValue constructor");
        return CPD3::Data::ArchiveValue();
    }

    Q_ASSERT(n >= 2);

    double modified = FP::undefined();
    if (n > 2) {
        modified = Time::extractSingle(frame, frame[2]);
    }

    return CPD3::Data::ArchiveValue(SequenceIdentity::extract(frame, frame[0]),
                                    CPD3::Data::Variant::Root(Variant::extract(frame, frame[1])),
                                    modified);
}

CPD3::Data::ArchiveValue ArchiveValue::extract(Engine::Frame &frame, const Engine::Reference &ref)
{
    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Number:
    case Engine::Reference::Type::String:
        return CPD3::Data::ArchiveValue();
    case Engine::Reference::Type::Data: {
        if (auto check = ref.toData<ArchiveValue>()) {
            return check->get(frame, ref);
        } else if (auto check = ref.toData<SequenceValue>()) {
            return CPD3::Data::ArchiveValue(check->get(frame, ref));
        } else if (auto check = ref.toData<ArchiveErasure>()) {
            return CPD3::Data::ArchiveValue(check->get(frame, ref));
        } else if (auto check = ref.toData<SequenceIdentity>()) {
            return CPD3::Data::ArchiveValue(check->get());
        } else if (auto check = ref.toData<SequenceName>()) {
            return CPD3::Data::ArchiveValue(check->get());
        } else {
            /* Use table conversion */
            break;
        }
    }
    default:
        break;
    }

    CPD3::Data::ArchiveValue result;
    assignFromTable(result, frame, ref);
    return result;
}

CPD3::Data::ArchiveValue ArchiveValue::get(Engine::Frame &frame,
                                           const Engine::Reference &self) const
{
    auto base = SequenceValue::get(frame, self);
    return CPD3::Data::ArchiveValue(std::move(base.getIdentity()), std::move(base.root()), modified,
                                    remoteReferenced);
}


void ArchiveValue::meta_tostring(Engine::Entry &entry)
{
    std::string result;
    result += "(";
    result += SequenceIdentity::describe(getIdentity(entry, entry[0]));
    result += "),";
    result += getContents().describe();
    result += ",";

    if (!FP::defined(modified)) {
        result += "UNKNOWN";
    } else if (modified < 1E6) {
        result += QString::number(modified, 'f', 0).toStdString();
    } else {
        result += CPD3::Time::toISO8601(modified).toStdString();
    }

    if (remoteReferenced) {
        result += ",REMOTE";
    } else {
        result += ",LOCAL";
    }

    entry.clear();
    entry.push(std::move(result));
    entry.propagate(1);
}

bool ArchiveValue::meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
{
    if (SequenceValue::meta_index(entry, upvalues))
        return true;
    auto key = entry.back().toString();
    if (key == "modified") {
        entry.push(modified);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return true;
    } else if (key == "remote") {
        entry.push(remoteReferenced);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return true;
    }
    return false;
}

bool ArchiveValue::meta_newindex(Engine::Entry &entry)
{
    if (SequenceValue::meta_newindex(entry))
        return true;
    auto key = entry[1].toString();
    if (key == "modified") {
        modified = Time::extractSingle(entry, entry[2]);
        return true;
    }
    return false;
}


static const std::string metatableArchiveErasureRegistryName = "CPD3_archiveerasure_metatable";

void ArchiveErasure::install(Engine::Table &target, Engine::Frame &frame)
{
    auto mt = StreamValue::pushMetatable(frame);
    frame.registry().set(metatableArchiveErasureRegistryName, mt);

    {
        Engine::Frame local(frame);
        local.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            auto value = constructFromFrame(entry);
            entry.clear();
            entry.pushData<ArchiveErasure>(std::move(value));
            entry.propagate(1);
        }));
        target.set("ArchiveErasure", local.back());
    }
}

void ArchiveErasure::initialize(const Engine::Reference &self, Engine::Frame &frame)
{
    frame.push(frame.registry(), metatableArchiveErasureRegistryName);
    setMetatable(self, frame.back());
}

ArchiveErasure::ArchiveErasure() : modified(FP::undefined())
{ }

ArchiveErasure::~ArchiveErasure() = default;

ArchiveErasure::ArchiveErasure(const CPD3::Data::ArchiveErasure &value) : StreamValue(
        value.getIdentity()), modified(value.getModified())
{ }

ArchiveErasure::ArchiveErasure(CPD3::Data::ArchiveErasure &&value) : StreamValue(
        std::move(value.getIdentity())), modified(value.getModified())
{ }

void ArchiveErasure::assignFromTable(CPD3::Data::ArchiveErasure &value,
                                     Engine::Frame &frame,
                                     const Engine::Table &table)
{
    StreamValue::assignFromTable(value.getIdentity(), frame, table);
    {
        Engine::Frame local(frame);
        local.push(table, "modified");
        if (!local.back().isNil())
            value.setModified(Time::extractSingle(local, local.back()));
    }
}

CPD3::Data::ArchiveErasure ArchiveErasure::constructFromFrame(Engine::Frame &frame)
{
    std::size_t n = frame.size();
    switch (n) {
    case 0:
        return CPD3::Data::ArchiveErasure();
    case 1:
        return extract(frame, frame.front());
    default:
        break;
    }

    if (n < 2) {
        frame.throwError("Insufficient arguments to ArchiveErasure constructor");
        return CPD3::Data::ArchiveErasure();
    }

    Q_ASSERT(n >= 2);

    return CPD3::Data::ArchiveErasure(SequenceIdentity::extract(frame, frame[0]),
                                      Time::extractSingle(frame, frame[1]));
}

CPD3::Data::ArchiveErasure ArchiveErasure::get(Engine::Frame &frame,
                                               const Engine::Reference &self) const
{
    if (hasDetachedIdentity() || hasDetachedName()) {
        return CPD3::Data::ArchiveErasure(getIdentity(frame, self), modified);
    }
    return CPD3::Data::ArchiveErasure(getIdentity(), modified);
}

CPD3::Data::ArchiveErasure ArchiveErasure::extract(Engine::Frame &frame,
                                                   const Engine::Reference &ref)
{
    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Number:
    case Engine::Reference::Type::String:
        return CPD3::Data::ArchiveErasure();
    case Engine::Reference::Type::Data: {
        if (auto check = ref.toData<ArchiveErasure>()) {
            return check->get(frame, ref);
        } else if (auto check = ref.toData<ArchiveValue>()) {
            return CPD3::Data::ArchiveErasure(check->get(frame, ref));
        } else if (auto check = ref.toData<SequenceValue>()) {
            return CPD3::Data::ArchiveErasure(check->get(frame, ref));
        } else if (auto check = ref.toData<SequenceIdentity>()) {
            return CPD3::Data::ArchiveErasure(check->get());
        } else if (auto check = ref.toData<SequenceName>()) {
            return CPD3::Data::ArchiveErasure(check->get());
        } else {
            /* Use table conversion */
            break;
        }
    }
    default:
        break;
    }

    CPD3::Data::ArchiveErasure result;
    assignFromTable(result, frame, ref);
    return result;
}

void ArchiveErasure::meta_tostring(Engine::Entry &entry)
{
    std::string result;
    result += "(";
    result += SequenceIdentity::describe(getIdentity(entry, entry[0]));
    result += "),";
    if (!FP::defined(modified)) {
        result += "UNKNOWN";
    } else if (modified < 1E6) {
        result += QString::number(modified, 'f', 0).toStdString();
    } else {
        result += CPD3::Time::toISO8601(modified).toStdString();
    }

    entry.clear();
    entry.push(std::move(result));
    entry.propagate(1);
}

bool ArchiveErasure::meta_index(Engine::Entry &entry,
                                const std::vector<Engine::Reference> &upvalues)
{
    if (StreamValue::meta_index(entry, upvalues))
        return true;
    auto key = entry.back().toString();
    if (key == "modified") {
        entry.push(modified);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return true;
    }
    return false;
}

bool ArchiveErasure::meta_newindex(Engine::Entry &entry)
{
    if (StreamValue::meta_newindex(entry))
        return true;
    auto key = entry[1].toString();
    if (key == "modified") {
        modified = Time::extractSingle(entry, entry[2]);
        return true;
    }
    return false;
}

}
}
}
