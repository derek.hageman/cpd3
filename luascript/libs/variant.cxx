/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <cmath>

#include "variant.hxx"
#include "valuesegment.hxx"
#include "streamvalue.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/variant/composite.hxx"
#include "core/util.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {

static const std::string metatableRegistryName = "CPD3_variant_metatable";

void Variant::pushBinaryOp(Engine::Frame &frame,
                           const std::function<void(Engine::Entry &, Variant *, Variant *)> &first,
                           const std::function<void(Engine::Entry &,
                                                    Variant *,
                                                    const Engine::Reference &)> &second,
                           const std::function<void(Engine::Entry &,
                                                    const Engine::Reference &,
                                                    Variant *)> &third)
{
    return frame.push(
            std::function<void(Engine::Entry &)>([first, second, third](Engine::Entry &entry) {
                if (entry.size() != 2) {
                    entry.throwError("Invalid arguments to variant binary operation");
                    return;
                }
                auto va = entry.front().toData<Variant>();
                if (!va) {
                    auto vb = entry.back().toData<Variant>();
                    if (!vb) {
                        entry.throwError("No variant operand in binary operation");
                        return;
                    }
                    return third(entry, entry.front(), vb);
                }
                auto vb = entry.back().toData<Variant>();
                if (!vb) {
                    return second(entry, va, entry.back());
                }
                return first(entry, va, vb);
            }));
}

void Variant::pushArithmeticOp(Engine::Frame &frame,
                               const std::function<double(double, double)> &op)
{
    return pushBinaryOp(frame, [op](Engine::Entry &entry, Variant *a, Variant *b) {
        double va = CPD3::Data::Variant::Composite::toNumber(a->var, true);
        if (!FP::defined(va)) {
            entry.clear();
            entry.push(std::numeric_limits<double>::quiet_NaN());
            entry.propagate(1);
            return;
        }
        double vb = CPD3::Data::Variant::Composite::toNumber(b->var, true);
        if (!FP::defined(vb)) {
            entry.clear();
            entry.push(std::numeric_limits<double>::quiet_NaN());
            entry.propagate(1);
            return;
        }
        entry.clear();
        entry.push(op(va, vb));
        entry.propagate(1);
    }, [op](Engine::Entry &entry, Variant *a, const Engine::Reference &b) {
        double va = CPD3::Data::Variant::Composite::toNumber(a->var, true);
        if (!FP::defined(va)) {
            entry.clear();
            entry.push(std::numeric_limits<double>::quiet_NaN());
            entry.propagate(1);
            return;
        }
        double vb = b.toReal();
        if (!FP::defined(vb)) {
            entry.clear();
            entry.push(std::numeric_limits<double>::quiet_NaN());
            entry.propagate(1);
            return;
        }
        entry.clear();
        entry.push(op(va, vb));
        entry.propagate(1);
    }, [op](Engine::Entry &entry, const Engine::Reference &a, Variant *b) {
        double va = a.toReal();
        if (!FP::defined(va)) {
            entry.clear();
            entry.push(std::numeric_limits<double>::quiet_NaN());
            entry.propagate(1);
            return;
        }
        double vb = CPD3::Data::Variant::Composite::toNumber(b->var, true);
        if (!FP::defined(vb)) {
            entry.clear();
            entry.push(std::numeric_limits<double>::quiet_NaN());
            entry.propagate(1);
            return;
        }
        entry.clear();
        entry.push(op(va, vb));
        entry.propagate(1);
    });
}

void Variant::install(Engine::Table &target, Engine::Frame &frame)
{
    auto mt = pushMetatable(frame);

    {
        Engine::Assign assign(frame, mt, "__add");
        pushArithmeticOp(assign, [](double a, double b) { return a + b; });
    }
    {
        Engine::Assign assign(frame, mt, "__sub");
        pushArithmeticOp(assign, [](double a, double b) { return a - b; });
    }
    {
        Engine::Assign assign(frame, mt, "__mul");
        pushArithmeticOp(assign, [](double a, double b) { return a * b; });
    }
    {
        Engine::Assign assign(frame, mt, "__div");
        pushArithmeticOp(assign, [](double a, double b) {
            if (b == 0.0)
                return std::numeric_limits<double>::quiet_NaN();
            return a / b;
        });
    }
    {
        Engine::Assign assign(frame, mt, "__mod");
        pushArithmeticOp(assign, [](double a, double b) {
            if (b == 0.0)
                return std::numeric_limits<double>::quiet_NaN();
            return std::fmod(a, b);
        });
    }
    {
        Engine::Assign assign(frame, mt, "__pow");
        pushArithmeticOp(assign, [](double a, double b) { return std::pow(a, b); });
    }
    {
        Engine::Assign assign(frame, mt, "__umn");
        pushMethod<Variant>(assign, &Variant::meta_umn);
    }
    {
        Engine::Assign assign(frame, mt, "__eq");
        pushMethod<Variant>(assign, &Variant::meta_eq);
    }
    {
        Engine::Assign assign(frame, mt, "__tostring");
        pushMethod<Variant>(assign, &Variant::method_describe);
    }
    {
        Engine::Assign assign(frame, mt, "__newindex");
        pushMethod<Variant>(assign, &Variant::meta_newindex);
    }


    {
        Engine::Assign assign(frame, mt, "__index");
        auto methods = assign.pushTable();

        {
            Engine::Assign ma(assign, methods, "getPath");
            pushMethod<Variant>(ma, &Variant::method_getPath);
        }
        {
            Engine::Assign ma(assign, methods, "child");
            pushMethod<Variant>(ma, &Variant::method_child);
        }
        {
            Engine::Assign ma(assign, methods, "hash");
            pushMethod<Variant>(ma, &Variant::method_hash);
        }
        {
            Engine::Assign ma(assign, methods, "array");
            pushMethod<Variant>(ma, &Variant::method_array);
        }
        {
            Engine::Assign ma(assign, methods, "arraySize");
            pushMethod<Variant>(ma, &Variant::method_arraySize);
        }
        {
            Engine::Assign ma(assign, methods, "arraySize");
            pushMethod<Variant>(ma, &Variant::method_arraySize);
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Variant>(mf, &Variant::method_afterBack);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "afterBack");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "after_back");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "matrix");
            pushMethod<Variant>(ma, &Variant::method_matrix);
        }
        {
            Engine::Assign ma(assign, methods, "matrixShape");
            pushMethod<Variant>(ma, &Variant::method_matrixShape);
        }
        {
            Engine::Assign ma(assign, methods, "matrixReshape");
            pushMethod<Variant>(ma, &Variant::method_matrixReshape);
        }
        {
            Engine::Assign ma(assign, methods, "keyframe");
            pushMethod<Variant>(ma, &Variant::method_keyframe);
        }
        {
            Engine::Assign ma(assign, methods, "metadata");
            pushMethod<Variant>(ma, &Variant::method_metadata);
        }
        {
            Engine::Assign ma(assign, methods, "metadataReal");
            pushMethod<Variant>(ma, &Variant::method_metadataReal);
        }
        {
            Engine::Assign ma(assign, methods, "metadataInteger");
            pushMethod<Variant>(ma, &Variant::method_metadataInteger);
        }
        {
            Engine::Assign ma(assign, methods, "metadataBoolean");
            pushMethod<Variant>(ma, &Variant::method_metadataBoolean);
        }
        {
            Engine::Assign ma(assign, methods, "metadataString");
            pushMethod<Variant>(ma, &Variant::method_metadataString);
        }
        {
            Engine::Assign ma(assign, methods, "metadataBytes");
            pushMethod<Variant>(ma, &Variant::method_metadataBytes);
        }
        {
            Engine::Assign ma(assign, methods, "metadataFlags");
            pushMethod<Variant>(ma, &Variant::method_metadataFlags);
        }
        {
            Engine::Assign ma(assign, methods, "metadataArray");
            pushMethod<Variant>(ma, &Variant::method_metadataArray);
        }
        {
            Engine::Assign ma(assign, methods, "metadataMatrix");
            pushMethod<Variant>(ma, &Variant::method_metadataMatrix);
        }
        {
            Engine::Assign ma(assign, methods, "metadataHash");
            pushMethod<Variant>(ma, &Variant::method_metadataHash);
        }
        {
            Engine::Assign ma(assign, methods, "metadataKeyframe");
            pushMethod<Variant>(ma, &Variant::method_metadataKeyframe);
        }
        {
            Engine::Assign ma(assign, methods, "metadataHashChild");
            pushMethod<Variant>(ma, &Variant::method_metadataHashChild);
        }
        {
            Engine::Assign ma(assign, methods, "metadataSingleFlag");
            pushMethod<Variant>(ma, &Variant::method_metadataSingleFlag);
        }
        {
            Engine::Assign ma(assign, methods, "detachFromRoot");
            pushMethod<Variant>(ma, &Variant::method_detachFromRoot);
        }
        {
            Engine::Assign ma(assign, methods, "describe");
            pushMethod<Variant>(ma, &Variant::method_describe);
        }
        {
            Engine::Assign ma(assign, methods, "getType");
            pushMethod<Variant>(ma, &Variant::method_getType);
        }
        {
            Engine::Assign ma(assign, methods, "setType");
            pushMethod<Variant>(ma, &Variant::method_setType);
        }
        {
            Engine::Assign ma(assign, methods, "setEmpty");
            pushMethod<Variant>(ma, &Variant::method_setEmpty);
        }
        {
            Engine::Assign ma(assign, methods, "exists");
            pushMethod<Variant>(ma, &Variant::method_exists);
        }
        {
            Engine::Assign ma(assign, methods, "isMetadata");
            pushMethod<Variant>(ma, &Variant::method_isMetadata);
        }
        {
            Engine::Assign ma(assign, methods, "toArray");
            pushMethod<Variant>(ma, &Variant::method_toArray);
        }
        {
            Engine::Assign ma(assign, methods, "toHash");
            pushMethod<Variant>(ma, &Variant::method_toHash);
        }
        {
            Engine::Assign ma(assign, methods, "toNumber");
            pushMethod<Variant>(ma, &Variant::method_toNumber);
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Variant>(mf, &Variant::method_toReal);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "toReal");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "toDouble");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Variant>(mf, &Variant::method_setReal);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "setReal");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "setDouble");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "toInteger");
            pushMethod<Variant>(ma, &Variant::method_toInteger);
        }
        {
            Engine::Assign ma(assign, methods, "setInteger");
            pushMethod<Variant>(ma, &Variant::method_setInteger);
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Variant>(mf, &Variant::method_toBoolean);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "toBoolean");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "toBool");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Variant>(mf, &Variant::method_setBoolean);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "setBoolean");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "setBool");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "toString");
            pushMethod<Variant>(ma, &Variant::method_toString);
        }
        {
            Engine::Assign ma(assign, methods, "setString");
            pushMethod<Variant>(ma, &Variant::method_setString);
        }
        {
            Engine::Assign ma(assign, methods, "toDisplayString");
            pushMethod<Variant>(ma, &Variant::method_toDisplayString);
        }
        {
            Engine::Assign ma(assign, methods, "setDisplayString");
            pushMethod<Variant>(ma, &Variant::method_setDisplayString);
        }
        {
            Engine::Assign ma(assign, methods, "allLocalizedStrings");
            pushMethod<Variant>(ma, &Variant::method_allLocalizedStrings);
        }
        {
            Engine::Assign ma(assign, methods, "toBytes");
            pushMethod<Variant>(ma, &Variant::method_toBytes);
        }
        {
            Engine::Assign ma(assign, methods, "setBytes");
            pushMethod<Variant>(ma, &Variant::method_setBytes);
        }
        {
            Engine::Assign ma(assign, methods, "toFlags");
            pushMethod<Variant>(ma, &Variant::method_toFlags);
        }
        {
            Engine::Assign ma(assign, methods, "setFlags");
            pushMethod<Variant>(ma, &Variant::method_setFlags);
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Variant>(mf, &Variant::method_addFlags);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "addFlags");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "addFlag");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "applyFlag");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "applyFlags");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Variant>(mf, &Variant::method_removeFlags);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "removeFlags");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "removeFlag");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "clearFlag");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "clearFlags");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Variant>(mf, &Variant::method_testFlags);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "testFlags");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "testFlag");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "hasFlag");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "hasFlags");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Variant>(mf, &Variant::method_testAnyFlags);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "testAnyFlags");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "hasAnyFlags");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "setOverlay");
            pushMethod<Variant>(ma, &Variant::method_setOverlay);
        }
        {
            Engine::Assign ma(assign, methods, "clear");
            pushMethod<Variant>(ma, &Variant::method_clear);
        }
        {
            Engine::Assign ma(assign, methods, "remove");
            pushMethod<Variant>(ma, &Variant::method_remove);
        }
        {
            Engine::Assign ma(assign, methods, "set");
            pushMethod<Variant>(ma, &Variant::method_set);
        }

        pushMethod<Variant>(assign, &Variant::meta_index, 1);
    }

    frame.registry().set(metatableRegistryName, mt);
    {
        Engine::Frame local(frame);
        local.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            auto added = entry.pushData<Variant>();
            if (entry.size() > 1) {
                /* Can't use extract because we need it detached */
                set(added->var, entry, entry.front());
            }
            entry.replaceWithBack(0);
            entry.propagate(1);
        }));
        target.set("Variant", local.back());
    }
}

void Variant::initialize(const Engine::Reference &self, Engine::Frame &frame)
{
    frame.push(frame.registry(), metatableRegistryName);
    setMetatable(self, frame.back());
}

Variant::Variant() : var(CPD3::Data::Variant::Write::empty())
{ }

Variant::~Variant() = default;

Variant::Variant(const CPD3::Data::Variant::Write &var) : var(var)
{ }

Variant::Variant(CPD3::Data::Variant::Write &&var) : var(std::move(var))
{ }

Variant::Variant(CPD3::Data::Variant::Root &root) : var(root.write())
{ }

CPD3::Data::Variant::Write Variant::extract(Engine::Frame &frame, const Engine::Reference &ref)
{

    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
        return CPD3::Data::Variant::Write::empty();
    case Engine::Reference::Type::Boolean:
        return CPD3::Data::Variant::Root(ref.toBoolean()).write();
    case Engine::Reference::Type::Data: {
        if (auto check = ref.toData<Variant>()) {
            return check->var;
        } else if (auto check = ref.toData<ValueSegment>()) {
            return check->getVariant();
        } else if (auto check = ref.toData<SequenceValue>()) {
            return check->getVariant();
        } else {
            frame.throwError("Cannot convert unrecognized data into a variant");
        }
        return CPD3::Data::Variant::Write::empty();
    }
    case Engine::Reference::Type::Number:
        return CPD3::Data::Variant::Root(ref.toNumber()).write();
    case Engine::Reference::Type::String:
        return CPD3::Data::Variant::Root(ref.toString()).write();
    case Engine::Reference::Type::Table:
        break;
    }

    CPD3::Data::Variant::Write target = CPD3::Data::Variant::Write::empty();
    {
        Engine::Iterator it(frame, ref);
        while (it.next()) {
            if (it.key().getType() == Engine::Reference::Type::Number) {
                auto i = it.key().toInteger();
                if (INTEGER::defined(i) && i > 0) {
                    target.array(static_cast<std::size_t>(i - 1)).set(extract(it, it.value()));
                    continue;
                }
            }
            target.getPath(it.key().toString()).set(extract(it, it.value()));
        }
    }
    return target;
}

void Variant::set(CPD3::Data::Variant::Write &target,
                  Engine::Frame &frame,
                  const Engine::Reference &ref)
{
    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
        target.remove();
        return;
    case Engine::Reference::Type::Boolean:
        target.setBoolean(ref.toBoolean());
        return;
    case Engine::Reference::Type::Data: {
        if (auto check = ref.toData<Variant>()) {
            target.set(check->var);
            return;
        } else {
            frame.throwError("Cannot convert unrecognized data into a variant");
            target.remove();
        }
        return;
    }
    case Engine::Reference::Type::Number: {
        double raw = ref.toNumber();
        if (target.getType() == CPD3::Data::Variant::Type::Integer) {
            if (std::floor(raw) == std::ceil(raw)) {
                target.setInteger(static_cast<std::int_fast64_t>(raw));
                return;
            }
        }
        target.setReal(raw);
        return;
    }
    case Engine::Reference::Type::String:
        target.setString(ref.toString());
        return;
    case Engine::Reference::Type::Table:
        break;
    }

    target.remove();
    {
        Engine::Iterator it(frame, ref);
        while (it.next()) {
            target.getPath(it.key().toString()).set(extract(it, it.value()));
        }
    }
}


void Variant::meta_umn(Engine::Entry &entry)
{
    double va = CPD3::Data::Variant::Composite::toNumber(var, true);
    if (!FP::defined(va)) {
        entry.clear();
        entry.push(std::numeric_limits<double>::quiet_NaN());
        entry.propagate(1);
        return;
    }
    entry.clear();
    entry.push(-va);
    entry.propagate(1);
}

void Variant::meta_eq(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid equality arguments");
        return;
    }
    auto check = entry[1].toData<Variant>();
    if (!check) {
        entry.clear();
        entry.push(false);
        entry.propagate(1);
        return;
    }
    bool result = (var == check->var);
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void Variant::pushLookup(CPD3::Data::Variant::Write source, Engine::Frame &frame)
{
    /* When using implicit indexing, if we're referencing a Lua convertible primitive type,
     * then do the dereference here, rather than keeping a variant handle around.  This allows
     * the result to be simply used with normal comparison, etc. */
    switch (source.getType()) {
    case CPD3::Data::Variant::Type::Empty:
        frame.pushNil();
        return;
    case CPD3::Data::Variant::Type::Real: {
        auto v = source.toReal();
        if (!FP::defined(v)) {
            frame.push(std::numeric_limits<double>::quiet_NaN());
        } else {
            frame.push(v);
        }
        return;
    }
    case CPD3::Data::Variant::Type::Integer: {
        auto v = source.toInteger();
        if (!INTEGER::defined(v)) {
            frame.push(std::numeric_limits<double>::quiet_NaN());
        } else {
            frame.push(v);
        }
        return;
    }
    case CPD3::Data::Variant::Type::Boolean:
        frame.push(source.toBoolean());
        return;
    case CPD3::Data::Variant::Type::String:
        frame.push(source.toString());
        return;

    default:
        break;
    }

    frame.pushData<Variant>(std::move(source));
}

void Variant::meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid index");
        return;
    }
    if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
        return;
    if (entry[1].getType() == Engine::Reference::Type::Number) {
        auto n = entry[1].toNumber();
        if (n >= 1.0 && std::ceil(n) == std::floor(n)) {
            pushLookup(var.array(static_cast<std::size_t>(n) - 1), entry);
            entry.replaceWithBack(0);
            entry.propagate(1);
            return;
        }
    }
    pushLookup(var.getPath(entry[1].toString()), entry);
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Variant::meta_newindex(Engine::Entry &entry)
{
    if (entry.size() < 3) {
        entry.throwError("Invalid assignment");
        return;
    }
    if (entry[1].getType() == Engine::Reference::Type::Number) {
        auto n = entry[1].toNumber();
        if (n >= 1.0 && std::ceil(n) == std::floor(n)) {
            auto target = var.array(static_cast<std::size_t>(n) - 1);
            set(target, entry, entry[2]);
            return;
        }
    }
    auto target = var.getPath(entry[1].toString());
    set(target, entry, entry[2]);
}


void Variant::method_getPath(Engine::Entry &entry)
{
    auto result = var;
    for (std::size_t index = 1, max = entry.size(); index < max; index++) {
        result = result.getPath(entry[index].toString());
    }
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_child(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.child(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_hash(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.hash(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_array(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto idx = entry[1].toInteger();
    if (!INTEGER::defined(idx))
        idx = 0;
    auto result = var.array(static_cast<std::size_t>(idx));
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_arraySize(Engine::Entry &entry)
{
    auto result = var.toArray().size();
    entry.clear();
    entry.push(static_cast<std::int_fast64_t>(result));
    entry.propagate(1);
}

void Variant::method_afterBack(Engine::Entry &entry)
{
    auto result = var.toArray().after_back();
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_matrix(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    CPD3::Data::Variant::PathElement::MatrixIndex goal;
    for (std::size_t index = 1, max = entry.size(); index < max; index++) {
        auto e = entry[index];
        if (e.getType() == Engine::Reference::Type::Number) {
            auto idx = e.toInteger();
            if (!INTEGER::defined(idx))
                idx = 0;
            goal.push_back(static_cast<std::size_t>(idx));
            continue;
        }

        Engine::Iterator add(entry, e);
        while (add.next()) {
            auto idx = add.value().toInteger();
            if (!INTEGER::defined(idx))
                idx = 0;
            goal.push_back(static_cast<std::size_t>(idx));
        }
    }
    if (goal.size() > 31) {
        entry.throwError("Matrix index overflow");
        return;
    }
    auto result = var.matrix(std::move(goal));
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_matrixShape(Engine::Entry &entry)
{
    auto input = var.toMatrix().shape();
    entry.clear();
    auto result = entry.pushTable();
    {
        Engine::Append add(entry, result, true);
        for (const auto &idx : input) {
            add.push(static_cast<std::int_fast64_t>(idx));
        }
    }
    entry.propagate(1);
}

void Variant::method_matrixReshape(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    CPD3::Data::Variant::PathElement::MatrixIndex goal;
    for (std::size_t index = 1, max = entry.size(); index < max; index++) {
        auto e = entry[index];
        if (e.getType() == Engine::Reference::Type::Number) {
            auto idx = e.toInteger();
            if (!INTEGER::defined(idx) || idx < 1)
                idx = 1;
            goal.push_back(static_cast<std::size_t>(idx));
            continue;
        }

        Engine::Iterator add(entry, e);
        while (add.next()) {
            auto idx = add.value().toInteger();
            if (!INTEGER::defined(idx) || idx < 1)
                idx = 1;
            goal.push_back(static_cast<std::size_t>(idx));
        }
    }
    if (goal.size() > 31) {
        entry.throwError("Matrix shape overflow");
        return;
    }
    var.toMatrix().reshape(goal);
}

void Variant::method_keyframe(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto idx = entry[1].toReal();
    if (!FP::defined(idx))
        idx = 0;
    auto result = var.keyframe(idx);
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_metadata(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.metadata(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_metadataReal(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.metadataReal(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_metadataInteger(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.metadataInteger(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_metadataBoolean(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.metadataBoolean(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_metadataString(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.metadataString(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_metadataBytes(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.metadataBytes(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_metadataFlags(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.metadataFlags(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_metadataArray(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.metadataArray(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_metadataMatrix(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.metadataMatrix(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_metadataHash(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.metadataHash(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_metadataKeyframe(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.metadataKeyframe(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_metadataHashChild(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.metadataHashChild(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_metadataSingleFlag(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = var.metadataSingleFlag(entry[1].toString());
    entry.clear();
    entry.pushData<Variant>(std::move(result));
    entry.propagate(1);
}

void Variant::method_detachFromRoot(Engine::Entry &entry)
{
    var.detachFromRoot();
}

void Variant::method_describe(Engine::Entry &entry)
{
    bool includePath = true;
    if (entry.size() >= 2) {
        includePath = entry[1].toBoolean();
    }
    auto result = var.describe(includePath);
    entry.clear();
    entry.push(std::move(result));
    entry.propagate(1);
}

void Variant::method_getType(Engine::Entry &entry)
{
    auto t = var.getType();
    entry.clear();
    switch (t) {
    case CPD3::Data::Variant::Type::Empty:
        entry.push("Empty");
        break;
    case CPD3::Data::Variant::Type::Real:
        entry.push("Real");
        break;
    case CPD3::Data::Variant::Type::Integer:
        entry.push("Integer");
        break;
    case CPD3::Data::Variant::Type::Boolean:
        entry.push("Boolean");
        break;
    case CPD3::Data::Variant::Type::String:
        entry.push("String");
        break;
    case CPD3::Data::Variant::Type::Bytes:
        entry.push("Bytes");
        break;
    case CPD3::Data::Variant::Type::Flags:
        entry.push("Flags");
        break;
    case CPD3::Data::Variant::Type::Hash:
        entry.push("Hash");
        break;
    case CPD3::Data::Variant::Type::Array:
        entry.push("Array");
        break;
    case CPD3::Data::Variant::Type::Matrix:
        entry.push("Matrix");
        break;
    case CPD3::Data::Variant::Type::Keyframe:
        entry.push("Keyframe");
        break;
    case CPD3::Data::Variant::Type::MetadataReal:
        entry.push("MetadataReal");
        break;
    case CPD3::Data::Variant::Type::MetadataInteger:
        entry.push("MetadataInteger");
        break;
    case CPD3::Data::Variant::Type::MetadataBoolean:
        entry.push("MetadataBoolean");
        break;
    case CPD3::Data::Variant::Type::MetadataString:
        entry.push("MetadataString");
        break;
    case CPD3::Data::Variant::Type::MetadataBytes:
        entry.push("MetadataBytes");
        break;
    case CPD3::Data::Variant::Type::MetadataFlags:
        entry.push("MetadataFlags");
        break;
    case CPD3::Data::Variant::Type::MetadataHash:
        entry.push("MetadataHash");
        break;
    case CPD3::Data::Variant::Type::MetadataArray:
        entry.push("MetadataArray");
        break;
    case CPD3::Data::Variant::Type::MetadataMatrix:
        entry.push("MetadataMatrix");
        break;
    case CPD3::Data::Variant::Type::MetadataKeyframe:
        entry.push("MetadataKeyframe");
        break;
    case CPD3::Data::Variant::Type::Overlay:
        entry.push("Overlay");
        break;
    }
    entry.propagate(1);
}

void Variant::method_setType(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto type = entry[1].toString();
    if (Util::equal_insensitive(type, "Empty", "none", "nil")) {
        var.setType(CPD3::Data::Variant::Type::Empty);
    } else if (Util::equal_insensitive(type, "Real", "number", "double")) {
        var.setType(CPD3::Data::Variant::Type::Real);
    } else if (Util::equal_insensitive(type, "Integer", "int")) {
        var.setType(CPD3::Data::Variant::Type::Integer);
    } else if (Util::equal_insensitive(type, "Boolean", "bool")) {
        var.setType(CPD3::Data::Variant::Type::Boolean);
    } else if (Util::equal_insensitive(type, "String", "str")) {
        var.setType(CPD3::Data::Variant::Type::String);
    } else if (Util::equal_insensitive(type, "Bytes", "Binary")) {
        var.setType(CPD3::Data::Variant::Type::Bytes);
    } else if (Util::equal_insensitive(type, "Flags", "Flag")) {
        var.setType(CPD3::Data::Variant::Type::Flags);
    } else if (Util::equal_insensitive(type, "Hash", "table")) {
        var.setType(CPD3::Data::Variant::Type::Hash);
    } else if (Util::equal_insensitive(type, "Array")) {
        var.setType(CPD3::Data::Variant::Type::Array);
    } else if (Util::equal_insensitive(type, "Matrix")) {
        var.setType(CPD3::Data::Variant::Type::Matrix);
    } else if (Util::equal_insensitive(type, "Keyframe")) {
        var.setType(CPD3::Data::Variant::Type::Keyframe);
    } else if (Util::equal_insensitive(type, "MetadataReal", "MetaReal")) {
        var.setType(CPD3::Data::Variant::Type::MetadataReal);
    } else if (Util::equal_insensitive(type, "MetadataInteger", "MetadataInt", "MetaInteger",
                                       "MetaInt")) {
        var.setType(CPD3::Data::Variant::Type::MetadataInteger);
    } else if (Util::equal_insensitive(type, "MetadataBoolean", "MetadataBool", "MetaBoolean",
                                       "MetaBool")) {
        var.setType(CPD3::Data::Variant::Type::MetadataBoolean);
    } else if (Util::equal_insensitive(type, "MetadataString", "MetaString")) {
        var.setType(CPD3::Data::Variant::Type::MetadataString);
    } else if (Util::equal_insensitive(type, "MetadataBytes", "MetaBytes")) {
        var.setType(CPD3::Data::Variant::Type::MetadataBytes);
    } else if (Util::equal_insensitive(type, "MetadataFlags", "MetaFlags")) {
        var.setType(CPD3::Data::Variant::Type::MetadataFlags);
    } else if (Util::equal_insensitive(type, "MetadataHash", "MetadataTable", "MetaHash",
                                       "MetaTable")) {
        var.setType(CPD3::Data::Variant::Type::MetadataHash);
    } else if (Util::equal_insensitive(type, "MetadataArray", "MetaArray")) {
        var.setType(CPD3::Data::Variant::Type::MetadataArray);
    } else if (Util::equal_insensitive(type, "MetadataMatrix", "MetaMatrix")) {
        var.setType(CPD3::Data::Variant::Type::MetadataMatrix);
    } else if (Util::equal_insensitive(type, "MetadataKeyframe", "MetaKeyframe")) {
        var.setType(CPD3::Data::Variant::Type::MetadataKeyframe);
    } else if (Util::equal_insensitive(type, "Overlay")) {
        var.setType(CPD3::Data::Variant::Type::Overlay);
    } else {
        entry.throwError("Unrecognized variant type: " + type);
    }
}

void Variant::method_setEmpty(Engine::Entry &entry)
{
    var.setEmpty();
}

void Variant::method_exists(Engine::Entry &entry)
{
    auto result = var.exists();
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void Variant::method_isMetadata(Engine::Entry &entry)
{
    auto result = var.isMetadata();
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void Variant::method_toArray(Engine::Entry &entry)
{
    auto result = entry.pushTable();
    for (auto add : var.toArray()) {
        Engine::Append append(entry, result, true);
        pushLookup(std::move(add), append);
    }
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Variant::method_toHash(Engine::Entry &entry)
{
    auto result = entry.pushTable();
    for (auto add : var.toHash()) {
        Engine::Assign assign(entry, result, add.first);
        pushLookup(std::move(add.second), assign);
    }
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Variant::method_toNumber(Engine::Entry &entry)
{
    bool convertStrings = false;
    if (entry.size() >= 2) {
        convertStrings = entry[1].toBoolean();
    }
    entry.push(CPD3::Data::Variant::Composite::toNumber(var, convertStrings));
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Variant::method_toReal(Engine::Entry &entry)
{
    entry.push(var.toReal());
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Variant::method_setReal(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    var.setReal(entry[1].toReal());
}

void Variant::method_toInteger(Engine::Entry &entry)
{
    entry.push(var.toInteger());
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Variant::method_setInteger(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    /* Do the integer parse manually, since Lua may convert it through 32-bits or a double
     * (53 bits) */
    if (entry[1].getType() == Engine::Reference::Type::String) {
        try {
            auto i = std::stoll(entry[1].toString(), nullptr, 0);
            var.setInteger(i);
            return;
        } catch (std::exception &) {
        }
    }
    var.setInteger(entry[1].toInteger());
}

void Variant::method_toBoolean(Engine::Entry &entry)
{
    entry.push(var.toBoolean());
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Variant::method_setBoolean(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    var.setBoolean(entry[1].toBoolean());
}

void Variant::method_toString(Engine::Entry &entry)
{
    entry.push(var.toString());
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Variant::method_setString(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    var.setString(entry[1].toString());
}

void Variant::method_toDisplayString(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.push(var.toDisplayString().toStdString());
    } else {
        entry.push(var.toDisplayString(QString::fromStdString(entry[1].toString())).toStdString());
    }
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Variant::method_setDisplayString(Engine::Entry &entry)
{
    if (entry.size() < 3) {
        entry.throwError("Invalid arguments");
        return;
    }
    var.setDisplayString(QString::fromStdString(entry[1].toString()),
                         QString::fromStdString(entry[2].toString()));
}

void Variant::method_allLocalizedStrings(Engine::Entry &entry)
{
    auto result = entry.pushTable();
    for (const auto &add : var.allLocalizedStrings()) {
        result.set(add.first.toStdString(), Engine::Output(add.second.toStdString()));
    }
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Variant::method_toBytes(Engine::Entry &entry)
{
    entry.push(var.toBytes().toStdString());
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Variant::method_setBytes(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    var.setBytes(QByteArray::fromStdString(entry[1].toString()));
}

void Variant::method_toFlags(Engine::Entry &entry)
{
    bool keyed = true;
    if (entry.size() >= 2) {
        keyed = entry[1].toBoolean();
    }

    auto result = entry.pushTable();
    if (!keyed) {
        Engine::Append append(entry, result, true);
        for (const auto &add : var.toFlags()) {
            append.push(add);
        }
    } else {
        for (const auto &add : var.toFlags()) {
            Engine::Assign assign(entry, result, add);
            assign.push(true);
        }
    }
    entry.replaceWithBack(0);
    entry.propagate(1);
}

static CPD3::Data::Variant::Flags convertFlags(Engine::Entry &entry)
{
    CPD3::Data::Variant::Flags flags;
    for (std::size_t index = 1, max = entry.size(); index < max; index++) {
        auto e = entry[index];
        if (e.getType() == Engine::Reference::Type::String) {
            flags.emplace(e.toString());
            continue;
        }

        Engine::Iterator add(entry, e);
        while (add.next()) {
            if (add.key().getType() == Engine::Reference::Type::String) {
                if (!add.value().toBoolean())
                    continue;
                auto flag = add.key().toString();
                if (flag.empty())
                    continue;
                flags.emplace(std::move(flag));
                continue;
            }
            auto flag = add.value().toString();
            if (flag.empty())
                continue;
            flags.emplace(std::move(flag));
        }
    }
    return flags;
}

void Variant::method_setFlags(Engine::Entry &entry)
{
    var.setFlags(convertFlags(entry));
}

void Variant::method_addFlags(Engine::Entry &entry)
{
    var.addFlags(convertFlags(entry));
}

void Variant::method_removeFlags(Engine::Entry &entry)
{
    var.removeFlags(convertFlags(entry));
}

void Variant::method_testFlags(Engine::Entry &entry)
{
    entry.push(var.testFlags(convertFlags(entry)));
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Variant::method_testAnyFlags(Engine::Entry &entry)
{
    entry.push(var.testAnyFlags(convertFlags(entry)));
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Variant::method_setOverlay(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    var.setOverlay(entry[1].toString());
}

void Variant::method_clear(Engine::Entry &entry)
{
    var.clear();
}

void Variant::method_remove(Engine::Entry &entry)
{
    bool createParents = true;
    if (entry.size() >= 2) {
        createParents = entry[1].toBoolean();
    }
    var.remove(createParents);
}

void Variant::method_set(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    if (entry.size() == 2) {
        set(var, entry, entry[1]);
    } else {
        std::vector<CPD3::Data::Variant::Root> elements;
        for (std::size_t i = 1, max = entry.size(); i < max; i++) {
            elements.emplace_back(extract(entry, entry[i]));
        }
        var.set(CPD3::Data::Variant::Root::overlay(elements.begin(), elements.end()));
    }
}

}
}
}