/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "valuesegment.hxx"
#include "streamvalue.hxx"
#include "variant.hxx"
#include "time.hxx"


namespace CPD3 {
namespace Lua {
namespace Libs {

static const std::string metatableRegistryName = "CPD3_valuesegment_metatable";

static CPD3::Data::ValueSegment constructFromFrame(Engine::Frame &frame)
{
    std::size_t n = frame.size();
    switch (n) {
    case 0:
        return CPD3::Data::ValueSegment();
    case 1:
        return ValueSegment::extract(frame, frame.front());
    default:
        break;
    }

    if (n < 3) {
        frame.throwError("Insufficient arguments to ValueSegment constructor");
        return CPD3::Data::ValueSegment();
    }

    Q_ASSERT(n >= 3);

    double start = FP::undefined();
    double end = FP::undefined();
    if (!frame[0].isNil()) {
        if (!frame[1].isNil()) {
            auto bounds = Time::extractBounds(frame, frame[0], frame[1]);
            start = bounds.start;
            end = bounds.end;
        } else {
            start = Time::extractSingle(frame, frame[0]);
        }
    } else if (!frame[1].isNil()) {
        end = Time::extractSingle(frame, frame[1]);
    }

    return CPD3::Data::ValueSegment(start, end,
                                    CPD3::Data::Variant::Root(Variant::extract(frame, frame[2])));
}

void ValueSegment::install(Engine::Table &target, Engine::Frame &frame)
{
    auto mt = pushMetatable(frame);


    {
        Engine::Assign assign(frame, mt, "__tostring");
        pushMethod<ValueSegment>(assign, &ValueSegment::meta_tostring);
    }
    {
        Engine::Assign assign(frame, mt, "__newindex");
        pushMethod<ValueSegment>(assign, &ValueSegment::meta_newindex);
    }

    {
        Engine::Assign assign(frame, mt, "__index");
        auto methods = assign.pushTable();

        {
            Engine::Assign ma(assign, methods, "detach");
            pushMethod<ValueSegment>(ma, &ValueSegment::method_detach);
        }
        {
            Engine::Assign ma(assign, methods, "set");
            pushMethod<ValueSegment>(ma, &ValueSegment::method_set);
        }

        pushMethod<ValueSegment>(assign, &ValueSegment::meta_index, 1);
    }

    frame.registry().set(metatableRegistryName, mt);
    {
        Engine::Frame local(frame);
        local.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            auto value = constructFromFrame(entry);
            entry.clear();
            entry.pushData<ValueSegment>(std::move(value));
            entry.propagate(1);
        }));
        target.set("ValueSegment", local.back());
    }
}

void ValueSegment::initialize(const Engine::Reference &self, Engine::Frame &frame)
{
    frame.push(frame.registry(), metatableRegistryName);
    setMetatable(self, frame.back());
}

ValueSegment::ValueSegment() : start(FP::undefined()),
                               end(FP::undefined()),
                               contents(CPD3::Data::Variant::Write::empty())
{ }

ValueSegment::~ValueSegment() = default;

ValueSegment::ValueSegment(const CPD3::Data::ValueSegment &segment) : start(segment.getStart()),
                                                                      end(segment.getEnd())
{
    CPD3::Data::Variant::Root temp(segment.root());
    contents = temp.write();
}

ValueSegment::ValueSegment(CPD3::Data::ValueSegment &&segment) : start(segment.getStart()),
                                                                 end(segment.getEnd()),
                                                                 contents(segment.write())
{ }

CPD3::Data::ValueSegment ValueSegment::extract(Engine::Frame &frame, const Engine::Reference &ref)
{
    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Number:
    case Engine::Reference::Type::String:
        return CPD3::Data::ValueSegment();
    case Engine::Reference::Type::Data: {
        if (auto check = ref.toData<ValueSegment>()) {
            return check->get();
        } else if (auto check = ref.toData<SequenceValue>()) {
            auto v = check->get(frame, ref);
            return CPD3::Data::ValueSegment(v.getStart(), v.getEnd(), std::move(v.root()));
        } else if (ref.toData<Variant>()) {
            return CPD3::Data::ValueSegment(FP::undefined(), FP::undefined(),
                                            CPD3::Data::Variant::Root(
                                                    Variant::extract(frame, ref)));
        } else {
            /* Use table conversion */
            break;
        }
    }
    default:
        break;
    }

    CPD3::Data::ValueSegment result;
    Engine::Table table(ref);

    {
        Engine::Frame local(frame);
        auto start = Time::pushStart(local, table);
        auto end = Time::pushEnd(local, table);

        if (!start.isNil()) {
            if (!end.isNil()) {
                auto bounds = Time::extractBounds(local, start, end);
                result.setStart(bounds.start);
                result.setEnd(bounds.end);
            } else {
                result.setStart(Time::extractSingle(local, start));
            }
        } else if (!end.isNil()) {
            result.setEnd(Time::extractSingle(local, end));
        }
    }

    {
        Engine::Frame local(frame);
        local.push(table, "value");
        if (!local.back().isNil()) {
            auto wr = result.write();
            Variant::set(wr, local, local.back());
        }
    }

    return result;
}

CPD3::Data::ValueSegment ValueSegment::get() const
{ return CPD3::Data::ValueSegment(start, end, CPD3::Data::Variant::Root(contents)); }

void ValueSegment::meta_tostring(Engine::Entry &entry)
{
    std::string result;
    if (!FP::defined(start)) {
        result += "-INFINITY";
    } else if (start < 1E6) {
        result += QString::number(start, 'f', 0).toStdString();
    } else {
        result += CPD3::Time::toISO8601(start).toStdString();
    }
    result += ",";
    if (!FP::defined(end)) {
        result += "+INFINITY";
    } else if (end < 1E6) {
        result += QString::number(end, 'f', 0).toStdString();
    } else {
        result += CPD3::Time::toISO8601(end).toStdString();
    }
    result += ",";
    result += contents.describe();

    entry.clear();
    entry.push(std::move(result));
    entry.propagate(1);
}

void ValueSegment::meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid index");
        return;
    }
    if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
        return;
    auto key = entry[1].toString();
    if (Util::equal_insensitive(key, "start", "front")) {
        entry.push(start);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (Util::equal_insensitive(key, "end", "back")) {
        entry.push(end);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "value") {
        entry.pushData<Variant>(contents);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    }
    Variant::pushLookup(contents.getPath(std::move(key)), entry);
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void ValueSegment::meta_newindex(Engine::Entry &entry)
{
    if (entry.size() < 3) {
        entry.throwError("Invalid assignment");
        return;
    }
    auto key = entry[1].toString();
    if (Util::equal_insensitive(key, "start", "front")) {
        start = Time::extractSingle(entry, entry[2]);
        return;
    } else if (Util::equal_insensitive(key, "end", "back")) {
        end = Time::extractSingle(entry, entry[2]);
        return;
    } else if (key == "value") {
        contents = Variant::extract(entry, entry[2]);
        return;
    }

    auto target = contents.getPath(entry[1].toString());
    Variant::set(target, entry, entry[2]);
}

void ValueSegment::method_detach(Engine::Entry &entry)
{
    contents.detachFromRoot();
}

void ValueSegment::method_set(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    Variant::set(contents, entry, entry[1]);
}

}
}
}