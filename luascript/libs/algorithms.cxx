/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "algorithms.hxx"
#include "core/util.hxx"
#include "algorithms/rayleigh.hxx"
#include "algorithms/linearint.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {
namespace Algorithms {

static void pushRayleigh(Engine::Frame &frame)
{
    auto target = frame.pushTable();
    {
        Engine::Assign assign(frame, target, "scattering");
        assign.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Invalid arguments");
                return;
            }
            double wavelength = entry[0].toReal();

            CPD3::Algorithms::Rayleigh::Gas type = CPD3::Algorithms::Rayleigh::Air;
            double t = 0.0;
            double p = 1013.25;

            if (entry.size() > 1)
                type = CPD3::Algorithms::Rayleigh::gasFromString(entry[1].toQString());
            if (entry.size() > 2)
                t = entry[2].toReal();
            if (entry.size() > 3)
                p = entry[3].toReal();

            entry.clear();
            entry.push(CPD3::Algorithms::Rayleigh::scattering(wavelength, type, t, p));
            entry.propagate(1);
        }));
    }
    {
        Engine::Assign assign(frame, target, "angleScattering");
        assign.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Invalid arguments");
                return;
            }
            double wavelength = entry[0].toReal();

            double startAngle = 0;
            double endAngle = 180.0;
            CPD3::Algorithms::Rayleigh::Gas type = CPD3::Algorithms::Rayleigh::Air;
            double t = 0.0;
            double p = 1013.25;

            if (entry.size() > 1)
                startAngle = entry[1].toReal();
            if (entry.size() > 2)
                endAngle = entry[2].toReal();
            if (entry.size() > 3)
                type = CPD3::Algorithms::Rayleigh::gasFromString(entry[3].toQString());
            if (entry.size() > 4)
                t = entry[4].toReal();
            if (entry.size() > 5)
                p = entry[5].toReal();

            entry.clear();
            entry.push(CPD3::Algorithms::Rayleigh::angleScattering(wavelength, startAngle, endAngle,
                                                                   type, t, p));
            entry.propagate(1);
        }));
    }
}

static void pushInterpolation(Engine::Frame &frame)
{
    auto target = frame.pushTable();
    {
        Engine::Assign assign(frame, target, "linear");
        assign.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Invalid arguments");
                return;
            }
            QVector<double> x;
            QVector<double> y;
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                Engine::Table point(entry[i]);
                x.append(point.get(1).toReal());
                y.append(point.get(2).toReal());
            }
            entry.clear();

            auto interpolator = std::make_shared<CPD3::Algorithms::LinearInterpolator>(x, y);
            entry.push(std::function<void(Engine::Entry &)>([interpolator](Engine::Entry &ce) {
                for (std::size_t i = 0, max = ce.size(); i < max; i++) {
                    ce.push(interpolator->oneToOne(ce[i].toReal()));
                    ce.replaceWithBack(i);
                }
                ce.propagate();
            }));
            entry.propagate(1);
        }));
    }
}

void install(Engine::Table &target, Engine::Frame &frame)
{
    Engine::Frame local(frame);
    auto table = local.pushTable();
    {
        Engine::Assign assign(local, table, "Rayleigh");
        pushRayleigh(assign);
    }
    {
        Engine::Assign assign(local, table, "Interpolation");
        pushInterpolation(assign);
    }
    target.set("Algorithms", table);
}

}
}
}
}