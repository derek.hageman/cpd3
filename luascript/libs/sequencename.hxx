/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUASCRIPTLIBS_SEQUENCENAME_HXX
#define CPD3LUASCRIPTLIBS_SEQUENCENAME_HXX

#include "core/first.hxx"

#include "luascript/luascript.hxx"
#include "luascript/engine.hxx"
#include "datacore/stream.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {

/**
 * A wrapper around a SequenceName CPD3 value.  The Lua API follows the C++ one, with
 * the exception that table-like access to the components (station, archive, variable, flavors)
 * is also permitted.
 */
class CPD3LUASCRIPT_EXPORT SequenceName : public Engine::Data {
    CPD3::Data::SequenceName name;

    static CPD3::Data::SequenceName fromString(const std::string &raw);

    static CPD3::Data::SequenceName constructFromFrame(Engine::Frame &frame);

    void meta_eq(Engine::Entry &entry);

    void meta_lt(Engine::Entry &entry);

    void meta_le(Engine::Entry &entry);

    void meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues);

    void meta_newindex(Engine::Entry &entry);

    void method_toDefaultStation(Engine::Entry &entry);

    void method_isDefaultStation(Engine::Entry &entry);

    void method_setDefaultStation(Engine::Entry &entry);

    void method_toMeta(Engine::Entry &entry);

    void method_fromMeta(Engine::Entry &entry);

    void method_isMeta(Engine::Entry &entry);

    void method_setMeta(Engine::Entry &entry);

    void method_clearMeta(Engine::Entry &entry);

    void method_hasFlavor(Engine::Entry &entry);

    void method_hasAnyFlavor(Engine::Entry &entry);

    void method_lacksFlavor(Engine::Entry &entry);

    void method_withFlavor(Engine::Entry &entry);

    void method_withoutFlavor(Engine::Entry &entry);

    void method_withoutAllFlavors(Engine::Entry &entry);

    void method_addFlavor(Engine::Entry &entry);

    void method_removeFlavor(Engine::Entry &entry);

    void method_clearFlavors(Engine::Entry &entry);

    void meta_tostring(Engine::Entry &entry);

    void method_displayLessThan(Engine::Entry &entry);

public:
    SequenceName();

    virtual ~SequenceName();

    explicit SequenceName(const CPD3::Data::SequenceName &name);

    explicit SequenceName(CPD3::Data::SequenceName &&name);

    /**
     * Install the library on an engine target.
     *
     * @param target    the target table to put symbols in
     * @param frame     a frame to create in
     */
    static void install(Engine::Table &target, Engine::Frame &frame);

    /**
     * Extract a value from Lua code.
     *
     * @param frame     the frame to work in
     * @param ref       the reference to read from
     * @return          the extracted value
     */
    static CPD3::Data::SequenceName extract(Engine::Frame &frame, const Engine::Reference &ref);

    /**
     * Get the current contents of the sequence name.
     *
     * @return  the sequence name
     */
    inline const CPD3::Data::SequenceName &get() const
    { return name; }

    /**
     * Get the current contents of the sequence name.
     *
     * @return  the sequence name
     */
    inline CPD3::Data::SequenceName &get()
    { return name; }

    /**
     * Assign a name from a table.
     *
     * @param name  the target name
     * @param frame the working frame
     * @param table the source table
     */
    static void assignFromTable(CPD3::Data::SequenceName &name,
                                Engine::Frame &frame,
                                const Engine::Table &table);

    /**
     * Convert a reference to a flavors set.
     *
     * @param frame the working frame
     * @param ref   the source reference
     * @return      the converted flavors
     */
    static CPD3::Data::SequenceName::Flavors convertFlavors(Engine::Frame &frame,
                                                            const Engine::Reference &ref);

    /**
     * Push a Lua representation of flavors on to a frame.
     *
     * @param frame     the target frame
     * @param flavors   the flavors
     */
    static void pushFlavors(Engine::Frame &frame, const CPD3::Data::SequenceName::Flavors &flavors);

protected:
    void initialize(const Engine::Reference &self, Engine::Frame &frame) override;
};

}
}
}

#endif //CPD3LUASCRIPTLIBS_SEQUENCENAME_HXX
