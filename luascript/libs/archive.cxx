/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "archive.hxx"
#include "time.hxx"
#include "sequencename.hxx"
#include "sequenceidentity.hxx"
#include "streamvalue.hxx"
#include "sequencesegment.hxx"
#include "valuesegment.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"
#include "core/threadpool.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {
namespace Archive {

static const std::string metatableArchiveSelectionRegistryName = "CPD3_archiveselection_metatable";

void Selection::install(Engine::Table &target, Engine::Frame &frame)
{
    auto mt = pushMetatable(frame);

    {
        Engine::Assign assign(frame, mt, "__tostring");
        pushMethod<Selection>(assign, &Selection::meta_tostring);
    }
    {
        Engine::Assign assign(frame, mt, "__newindex");
        pushMethod<Selection>(assign, &Selection::meta_newindex);
    }

    {
        Engine::Assign assign(frame, mt, "__index");
        auto methods = assign.pushTable();

        {
            Engine::Assign ma(assign, methods, "withDefaultStation");
            pushMethod<Selection>(ma, &Selection::method_withDefaultStation);
        }
        {
            Engine::Assign ma(assign, methods, "withMetaArchive");
            pushMethod<Selection>(ma, &Selection::method_withMetaArchive);
        }

        pushMethod<Selection>(assign, &Selection::meta_index, 1);
    }

    frame.registry().set(metatableArchiveSelectionRegistryName, mt);
    {
        Engine::Frame local(frame);
        local.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            auto value = constructFromFrame(entry);
            entry.clear();
            entry.pushData<Selection>(std::move(value));
            entry.propagate(1);
        }));
        target.set("Selection", local.back());
    }
}

void Selection::initialize(const Engine::Reference &self, Engine::Frame &frame)
{
    frame.push(frame.registry(), metatableArchiveSelectionRegistryName);
    setMetatable(self, frame.back());
}

Selection::Selection() = default;

Selection::~Selection() = default;

Selection::Selection(const CPD3::Data::Archive::Selection &selection) : selection(selection)
{ }

Selection::Selection(CPD3::Data::Archive::Selection &&selection) : selection(std::move(selection))
{ }

static CPD3::Data::Archive::Selection::Match toSelectionMatcher(Engine::Frame &frame,
                                                                const Engine::Reference &ref)
{
    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Number:
        return {};
    case Engine::Reference::Type::String:
        return {ref.toString()};
    case Engine::Reference::Type::Table:
    case Engine::Reference::Type::Data:
        break;
    }

    CPD3::Data::Archive::Selection::Match result;
    Engine::Iterator it(frame, ref);
    while (it.next()) {
        if (it.key().getType() == Engine::Reference::Type::String) {
            if (!it.value().toBoolean())
                continue;
            result.emplace_back(it.key().toString());
            continue;
        }
        result.emplace_back(it.value().toString());
    }
    return result;
}

static void appendSelectionMatcher(Engine::Frame &frame,
                                   const Engine::Table &table,
                                   const char *lookup,
                                   CPD3::Data::Archive::Selection::Match &target)
{
    Engine::Frame local(frame);
    local.push(table, lookup);
    Util::append(toSelectionMatcher(local, local.back()), target);
}

static void setIfAvailable(Engine::Frame &frame,
                           const Engine::Table &table,
                           const char *lookup,
                           double &target)
{
    Engine::Frame local(frame);
    local.push(table, lookup);
    if (local.back().isNil())
        return;
    target = Time::extractSingle(local, local.back());
}

static void setIfAvailable(Engine::Frame &frame,
                           const Engine::Table &table,
                           const char *lookup,
                           bool &target)
{
    Engine::Frame local(frame);
    local.push(table, lookup);
    if (local.back().isNil())
        return;
    target = local.back().toBoolean();
}

CPD3::Data::Archive::Selection Selection::constructFromFrame(Engine::Frame &frame)
{
    std::size_t n = frame.size();
    switch (n) {
    case 0:
        return {};
    case 1:
        return extract(frame, frame.front());
    default:
        break;
    }

    if (n < 2) {
        frame.throwError("Insufficient arguments to ArchiveSelection constructor");
        return CPD3::Data::Archive::Selection();
    }

    Q_ASSERT(n >= 2);

    double start = FP::undefined();
    double end = FP::undefined();
    if (!frame[0].isNil()) {
        if (!frame[1].isNil()) {
            auto bounds = Time::extractBounds(frame, frame[0], frame[1]);
            start = bounds.start;
            end = bounds.end;
        } else {
            start = Time::extractSingle(frame, frame[0]);
        }
    } else if (!frame[1].isNil()) {
        end = Time::extractSingle(frame, frame[1]);
    }

    CPD3::Data::Archive::Selection::Match stations;
    if (n > 2)
        stations = toSelectionMatcher(frame, frame[2]);
    CPD3::Data::Archive::Selection::Match archives;
    if (n > 3)
        archives = toSelectionMatcher(frame, frame[3]);
    CPD3::Data::Archive::Selection::Match variables;
    if (n > 4)
        variables = toSelectionMatcher(frame, frame[4]);
    CPD3::Data::Archive::Selection::Match hasFlavors;
    if (n > 5)
        hasFlavors = toSelectionMatcher(frame, frame[5]);
    CPD3::Data::Archive::Selection::Match lacksFlavors;
    if (n > 6)
        lacksFlavors = toSelectionMatcher(frame, frame[6]);
    CPD3::Data::Archive::Selection::Match exactFlavors;
    if (n > 7)
        exactFlavors = toSelectionMatcher(frame, frame[7]);
    double modifiedAfter = FP::undefined();
    if (n > 8) {
        modifiedAfter = Time::extractSingle(frame, frame[8]);
    }

    return {start, end, std::move(stations), std::move(archives), std::move(variables),
            std::move(hasFlavors), std::move(lacksFlavors), std::move(exactFlavors), modifiedAfter};
}

CPD3::Data::Archive::Selection Selection::extract(Engine::Frame &frame,
                                                  const Engine::Reference &ref)
{
    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Number:
    case Engine::Reference::Type::String:
        return CPD3::Data::Archive::Selection();
    case Engine::Reference::Type::Data: {
        if (auto check = ref.toData<Selection>()) {
            return check->get();
        } else if (auto check = ref.toData<SequenceName>()) {
            return CPD3::Data::Archive::Selection(check->get());
        } else if (auto check = ref.toData<SequenceIdentity>()) {
            auto id = check->get(frame, ref);
            return CPD3::Data::Archive::Selection(id.getName(), id.getStart(), id.getEnd());
        } else if (auto check = ref.toData<StreamValue>()) {
            auto id = check->getIdentity(frame, ref);
            return CPD3::Data::Archive::Selection(id.getName(), id.getStart(), id.getEnd());
        } else {
            /* Use table conversion */
            break;
        }
    }
    default:
        break;
    }

    CPD3::Data::Archive::Selection result;
    Engine::Table table(ref);

    {
        Engine::Frame local(frame);
        auto start = Time::pushStart(local, table);
        auto end = Time::pushEnd(local, table);

        if (!start.isNil()) {
            if (!end.isNil()) {
                auto bounds = Time::extractBounds(local, start, end);
                result.setStart(bounds.start);
                result.setEnd(bounds.end);
            } else {
                result.setStart(Time::extractSingle(local, start));
            }
        } else if (!end.isNil()) {
            result.setEnd(Time::extractSingle(local, end));
        }
    }

    appendSelectionMatcher(frame, table, "station", result.stations);
    appendSelectionMatcher(frame, table, "stations", result.stations);
    appendSelectionMatcher(frame, table, "archive", result.archives);
    appendSelectionMatcher(frame, table, "archives", result.archives);
    appendSelectionMatcher(frame, table, "variable", result.variables);
    appendSelectionMatcher(frame, table, "variables", result.variables);
    appendSelectionMatcher(frame, table, "hasFlavors", result.hasFlavors);
    appendSelectionMatcher(frame, table, "has_flavors", result.hasFlavors);
    appendSelectionMatcher(frame, table, "lacksFlavors", result.lacksFlavors);
    appendSelectionMatcher(frame, table, "lacks_flavors", result.lacksFlavors);
    appendSelectionMatcher(frame, table, "exactFlavors", result.exactFlavors);
    appendSelectionMatcher(frame, table, "exact_flavors", result.exactFlavors);
    appendSelectionMatcher(frame, table, "flavors", result.exactFlavors);
    setIfAvailable(frame, table, "modifiedAfter", result.modifiedAfter);
    setIfAvailable(frame, table, "modified_after", result.modifiedAfter);
    setIfAvailable(frame, table, "modified", result.modifiedAfter);
    setIfAvailable(frame, table, "includeDefaultStation", result.includeDefaultStation);
    setIfAvailable(frame, table, "includeMetaArchive", result.includeMetaArchive);

    return result;
}

static std::string describeMatcher(const CPD3::Data::Archive::Selection::Match &matcher)
{
    if (matcher.empty())
        return {};
    std::string result;
    for (const auto &add : matcher) {
        if (!result.empty())
            result += ",";
        result += add;
    }
    return result;
}

void Selection::meta_tostring(Engine::Entry &entry)
{
    std::string result;
    if (!FP::defined(selection.start)) {
        result += "-INFINITY";
    } else if (selection.start < 1E6) {
        result += QString::number(selection.start, 'f', 0).toStdString();
    } else {
        result += CPD3::Time::toISO8601(selection.start).toStdString();
    }
    result += ",";
    if (!FP::defined(selection.end)) {
        result += "+INFINITY";
    } else if (selection.end < 1E6) {
        result += QString::number(selection.end, 'f', 0).toStdString();
    } else {
        result += CPD3::Time::toISO8601(selection.end).toStdString();
    }

    result += ",[";
    result += describeMatcher(selection.stations);
    result += "],[";
    result += describeMatcher(selection.archives);
    result += "],[";
    result += describeMatcher(selection.variables);
    result += "],hasFlavors=[";
    result += describeMatcher(selection.hasFlavors);
    result += "],lacksFlavors=[";
    result += describeMatcher(selection.lacksFlavors);
    result += "],exactFlavors=[";
    result += describeMatcher(selection.exactFlavors);
    result += "]";
    if (FP::defined(selection.modifiedAfter)) {
        result += ",modifiedAfter=";
        if (selection.modifiedAfter < 1E6) {
            result += QString::number(selection.modifiedAfter, 'f', 0).toStdString();
        } else {
            result += CPD3::Time::toISO8601(selection.modifiedAfter).toStdString();
        }
    }
    if (!selection.includeDefaultStation && !selection.includeMetaArchive) {
        result += ",NoDefaultStation|NoMetaArchive";
    } else if (!selection.includeDefaultStation) {
        result += ",NoDefaultStation";
    } else if (!selection.includeMetaArchive) {
        result += ",NoMetaArchive";
    }

    entry.clear();
    entry.push(std::move(result));
    entry.propagate(1);
}

static void pushSelectionMatcher(Engine::Frame &target,
                                 const CPD3::Data::Archive::Selection::Match &source)
{
    auto table = target.pushTable();
    Engine::Append append(target, table, true);
    for (const auto &add : source) {
        append.push(add);
    }
}

void Selection::meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid index");
        return;
    }
    if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
        return;
    auto key = entry[1].toString();
    if (Util::equal_insensitive(key, "start", "front")) {
        entry.push(selection.start);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (Util::equal_insensitive(key, "end", "back")) {
        entry.push(selection.end);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "station") {
        if (selection.stations.size() != 1) {
            entry.clear();
            entry.pushNil();
            entry.propagate(1);
            return;
        }
        entry.push(selection.stations.front());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "stations") {
        pushSelectionMatcher(entry, selection.stations);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "archive") {
        if (selection.archives.size() != 1) {
            entry.clear();
            entry.pushNil();
            entry.propagate(1);
            return;
        }
        entry.push(selection.archives.front());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "archives") {
        pushSelectionMatcher(entry, selection.archives);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "variable") {
        if (selection.variables.size() != 1) {
            entry.clear();
            entry.pushNil();
            entry.propagate(1);
            return;
        }
        entry.push(selection.variables.front());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "variables") {
        pushSelectionMatcher(entry, selection.variables);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "hasFlavors") {
        pushSelectionMatcher(entry, selection.hasFlavors);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "lacksFlavors") {
        pushSelectionMatcher(entry, selection.lacksFlavors);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "exactFlavors") {
        pushSelectionMatcher(entry, selection.exactFlavors);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "modifiedAfter") {
        entry.push(selection.modifiedAfter);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "includeDefaultStation") {
        entry.push(selection.includeDefaultStation);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "includeMetaArchive") {
        entry.push(selection.includeMetaArchive);
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else {
        entry.clear();
        entry.pushNil();
        entry.propagate(1);
        return;
    }
}

void Selection::meta_newindex(Engine::Entry &entry)
{
    if (entry.size() < 3) {
        entry.throwError("Invalid assignment");
        return;
    }
    auto key = entry[1].toString();
    if (key == "station" || key == "stations") {
        selection.stations.clear();
        Util::append(toSelectionMatcher(entry, entry[2]), selection.stations);
    } else if (key == "archive" || key == "archives") {
        selection.archives.clear();
        Util::append(toSelectionMatcher(entry, entry[2]), selection.archives);
    } else if (key == "variable" || key == "variables") {
        selection.variables.clear();
        Util::append(toSelectionMatcher(entry, entry[2]), selection.variables);
    } else if (key == "hasFlavors") {
        selection.hasFlavors.clear();
        selection.exactFlavors.clear();
        Util::append(toSelectionMatcher(entry, entry[2]), selection.hasFlavors);
    } else if (key == "lacksFlavors") {
        selection.lacksFlavors.clear();
        selection.exactFlavors.clear();
        Util::append(toSelectionMatcher(entry, entry[2]), selection.lacksFlavors);
    } else if (key == "exactFlavors") {
        selection.exactFlavors.clear();
        selection.hasFlavors.clear();
        selection.lacksFlavors.clear();
        Util::append(toSelectionMatcher(entry, entry[2]), selection.exactFlavors);
    } else if (key == "modifiedAfter") {
        selection.modifiedAfter = Time::extractSingle(entry, entry[2]);
    } else if (key == "includeDefaultStation") {
        selection.includeDefaultStation = entry[2].toBoolean();
    } else if (key == "includeMetaArchive") {
        selection.includeMetaArchive = entry[2].toBoolean();
    } else if (Util::equal_insensitive(key, "start", "front")) {
        selection.setStart(Time::extractSingle(entry, entry[2]));
    } else if (Util::equal_insensitive(key, "end", "back")) {
        selection.setEnd(Time::extractSingle(entry, entry[2]));
    } else {
        entry.throwError("Invalid assignment");
        return;
    }
}

void Selection::method_withDefaultStation(Engine::Entry &entry)
{
    bool include = true;
    if (entry.size() > 1)
        include = entry[1].toBoolean();
    auto result = selection.withDefaultStation(include);
    entry.clear();
    entry.pushData<Selection>(std::move(result));
    entry.propagate(1);
}

void Selection::method_withMetaArchive(Engine::Entry &entry)
{
    bool include = true;
    if (entry.size() > 1)
        include = entry[1].toBoolean();
    auto result = selection.withMetaArchive(include);
    entry.clear();
    entry.pushData<Selection>(std::move(result));
    entry.propagate(1);
}


namespace {
class ReadLock : public Engine::Data {
    std::shared_ptr<CPD3::Data::Archive::Access> access;
    std::unique_ptr<CPD3::Data::Archive::Access::ReadLock> lock;

    void meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid index");
            return;
        }
        if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
            return;
    }

    void method_release(Engine::Entry &entry)
    { lock.reset(); }

    static const std::string metatableRegistryName;

public:
    ReadLock(const std::shared_ptr<CPD3::Data::Archive::Access> &access) : access(access),
                                                                           lock(new CPD3::Data::Archive::Access::ReadLock(
                                                                                   *this->access))
    { }

    virtual ~ReadLock() = default;

    static void install(Engine::Table &target, Engine::Frame &frame)
    {
        auto mt = pushMetatable(frame);

        {
            Engine::Assign assign(frame, mt, "__index");
            auto methods = assign.pushTable();

            {
                Engine::Assign ma(assign, methods, "release");
                pushMethod<ReadLock>(ma, &ReadLock::method_release);
            }
        }

        frame.registry().set(metatableRegistryName, mt);
    }

protected:
    void initialize(const Engine::Reference &self, Engine::Frame &frame) override
    {
        frame.push(frame.registry(), metatableRegistryName);
        setMetatable(self, frame.back());
    }
};

const std::string ReadLock::metatableRegistryName = "CPD3_archiveaccessreadlock_metatable";


class WriteLock : public Engine::Data {
    std::shared_ptr<CPD3::Data::Archive::Access> access;
    std::unique_ptr<CPD3::Data::Archive::Access::WriteLock> lock;

    void meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid index");
            return;
        }
        if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
            return;
    }

    void method_commit(Engine::Entry &entry)
    {
        if (!lock) {
            entry.clear();
            entry.push(false);
            entry.propagate(1);
            return;
        }

        bool result = lock->commit();
        lock.reset();
        entry.clear();
        entry.push(result);
        entry.propagate(1);
    }

    void method_abort(Engine::Entry &entry)
    {
        if (!lock)
            return;
        lock->abort();
        lock.reset();
    }

    static const std::string metatableRegistryName;

public:
    WriteLock(const std::shared_ptr<CPD3::Data::Archive::Access> &access) : access(access),
                                                                            lock(new CPD3::Data::Archive::Access::WriteLock(
                                                                                    *this->access))
    { }

    virtual ~WriteLock() = default;

    static void install(Engine::Table &target, Engine::Frame &frame)
    {
        auto mt = pushMetatable(frame);

        {
            Engine::Assign assign(frame, mt, "__index");
            auto methods = assign.pushTable();

            {
                Engine::Assign ma(assign, methods, "commit");
                pushMethod<WriteLock>(ma, &WriteLock::method_commit);
            }
            {
                Engine::Assign ma(assign, methods, "abort");
                pushMethod<WriteLock>(ma, &WriteLock::method_abort);
            }
        }

        frame.registry().set(metatableRegistryName, mt);
    }

protected:
    void initialize(const Engine::Reference &self, Engine::Frame &frame) override
    {
        frame.push(frame.registry(), metatableRegistryName);
        setMetatable(self, frame.back());
    }
};

const std::string WriteLock::metatableRegistryName = "CPD3_archiveaccesswritelock_metatable";


class BaseStream : public Engine::Data {
    void meta_call(Engine::Entry &entry)
    {
        entry.resize(1);
        if (pushNext(entry)) {
            entry.erase(0);
            entry.propagate();
            return;
        }

        entry.clear();
        entry.pushNil();
        entry.propagate(1);
    }

    static const std::string metatableRegistryName;

public:
    BaseStream() = default;

    virtual ~BaseStream() = default;

    static void install(Engine::Table &target, Engine::Frame &frame)
    {
        auto mt = pushMetatable(frame);

        {
            Engine::Assign assign(frame, mt, "__call");
            pushMethod<BaseStream>(assign, &BaseStream::meta_call);
        }

        frame.registry().set(metatableRegistryName, mt);
    }

protected:
    void initialize(const Engine::Reference &self, Engine::Frame &frame) override
    {
        frame.push(frame.registry(), metatableRegistryName);
        setMetatable(self, frame.back());
    }

    virtual bool pushNext(Engine::Frame &target) = 0;
};

const std::string BaseStream::metatableRegistryName = "CPD3_archiveaccessbasestream_metatable";

}

static const std::string metatableArchiveAccessRegistryName = "CPD3_archiveaccess_metatable";

void Access::install(Engine::Table &target, Engine::Frame &frame)
{
    auto mt = pushMetatable(frame);

    {
        Engine::Assign assign(frame, mt, "__index");
        auto methods = assign.pushTable();

        {
            Engine::Frame mf(assign);
            pushMethod<Access>(mf, &Access::method_read_lock);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "read_lock");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "ReadLock");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Access>(mf, &Access::method_write_lock);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "write_lock");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "WriteLock");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Access>(mf, &Access::method_write);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "write");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "modify");
                ma.push(m);
            }
        }

        {
            Engine::Assign ma(assign, methods, "add");
            pushMethod<Access>(ma, &Access::method_add);
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Access>(mf, &Access::method_erase);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "erase");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "remove");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Access>(mf, &Access::method_read_SequenceValue);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "read_SequenceValue");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "read_ArchiveValue");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "read_ValueSegment");
            pushMethod<Access>(ma, &Access::method_read_ValueSegment);
        }
        {
            Engine::Assign ma(assign, methods, "read_StreamSegment");
            pushMethod<Access>(ma, &Access::method_read_StreamSegment);
        }
        {
            Engine::Assign ma(assign, methods, "read_ArchiveErasure");
            pushMethod<Access>(ma, &Access::method_read_ArchiveErasure);
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Access>(mf, &Access::method_stream_SequenceValue);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "stream_SequenceValue");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "stream_ArchiveValue");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "stream_ValueSegment");
            pushMethod<Access>(ma, &Access::method_stream_ValueSegment);
        }
        {
            Engine::Assign ma(assign, methods, "stream_StreamSegment");
            pushMethod<Access>(ma, &Access::method_stream_StreamSegment);
        }
        {
            Engine::Assign ma(assign, methods, "stream_ArchiveErasure");
            pushMethod<Access>(ma, &Access::method_stream_ArchiveErasure);
        }

        {
            Engine::Assign ma(assign, methods, "exists");
            pushMethod<Access>(ma, &Access::method_exists);
        }

        pushMethod<Access>(assign, &Access::meta_index, 1);
    }

    ReadLock::install(target, frame);
    WriteLock::install(target, frame);
    BaseStream::install(target, frame);

    frame.registry().set(metatableArchiveAccessRegistryName, mt);
    {
        Engine::Frame local(frame);
        local.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            entry.clear();
            entry.pushData<Access>();
            entry.propagate(1);
        }));
        target.set("Access", local.back());
    }
}

void Access::initialize(const Engine::Reference &self, Engine::Frame &frame)
{
    frame.push(frame.registry(), metatableArchiveAccessRegistryName);
    setMetatable(self, frame.back());
}

Access::Access() : access(std::make_shared<CPD3::Data::Archive::Access>())
{ }

void Access::meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid index");
        return;
    }
    if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
        return;
}

void Access::method_read_lock(Engine::Entry &entry)
{
    entry.pushData<ReadLock>(access);
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Access::method_write_lock(Engine::Entry &entry)
{
    entry.pushData<WriteLock>(access);
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Access::method_write(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }

    CPD3::Data::ArchiveValue::Transfer add;
    {
        Engine::Iterator it(entry, entry[1]);
        while (it.next()) {
            add.emplace_back(ArchiveValue::extract(it, it.value()));
        }
    }

    CPD3::Data::ArchiveErasure::Transfer remove;
    if (entry.size() > 2) {
        Engine::Iterator it(entry, entry[2]);
        while (it.next()) {
            remove.emplace_back(ArchiveErasure::extract(it, it.value()));
        }
    }

    access->writeSynchronous(add, remove);
}

void Access::method_add(Engine::Entry &entry)
{
    CPD3::Data::ArchiveValue::Transfer add;
    for (std::size_t i = 1, max = entry.size(); i < max; i++) {
        add.emplace_back(ArchiveValue::extract(entry, entry[i]));
    }
    if (add.empty())
        return;
    access->writeSynchronous(add, false);
}

void Access::method_erase(Engine::Entry &entry)
{
    CPD3::Data::ArchiveErasure::Transfer remove;
    for (std::size_t i = 1, max = entry.size(); i < max; i++) {
        remove.emplace_back(ArchiveErasure::extract(entry, entry[i]));
    }
    if (remove.empty())
        return;
    access->writeSynchronous(remove, false);
}

void Access::method_read_SequenceValue(Engine::Entry &entry)
{
    CPD3::Data::Archive::Selection::List selections;
    for (std::size_t i = 1, max = entry.size(); i < max; i++) {
        selections.emplace_back(Libs::Archive::Selection::extract(entry, entry[i]));
    }
    if (selections.empty()) {
        entry.throwError("No selections provided");
        return;
    }

    CPD3::Data::ArchiveSink::Iterator incoming;
    access->readArchive(selections, &incoming)->detach();

    auto result = entry.pushTable();
    for (;;) {
        auto add = incoming.all();
        if (add.empty())
            break;
        {
            Engine::Append append(entry, result, true);
            for (auto &v : add) {
                append.pushData<ArchiveValue>(std::move(v));
            }
        }
    }
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Access::method_read_ValueSegment(Engine::Entry &entry)
{
    CPD3::Data::Archive::Selection::List selections;
    for (std::size_t i = 1, max = entry.size(); i < max; i++) {
        selections.emplace_back(Libs::Archive::Selection::extract(entry, entry[i]));
        selections.back().includeMetaArchive = false;
    }
    if (selections.empty()) {
        entry.throwError("No selections provided");
        return;
    }

    CPD3::Data::StreamSink::Iterator incoming;
    access->readStream(selections, &incoming)->detach();

    auto result = entry.pushTable();
    CPD3::Data::ValueSegment::Stream reader;
    for (;;) {
        auto add = incoming.all();
        if (add.empty())
            break;
        {
            Engine::Append append(entry, result, true);
            for (auto &v : reader.add(add)) {
                append.pushData<ValueSegment>(std::move(v));
            }
        }
    }
    {
        Engine::Append append(entry, result, true);
        for (auto &v : reader.finish()) {
            append.pushData<ValueSegment>(std::move(v));
        }
    }
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Access::method_read_StreamSegment(Engine::Entry &entry)
{
    CPD3::Data::Archive::Selection::List selections;
    for (std::size_t i = 1, max = entry.size(); i < max; i++) {
        selections.emplace_back(Libs::Archive::Selection::extract(entry, entry[i]));
    }
    if (selections.empty()) {
        entry.throwError("No selections provided");
        return;
    }

    CPD3::Data::StreamSink::Iterator incoming;
    access->readStream(selections, &incoming)->detach();

    auto result = entry.pushTable();
    CPD3::Data::SequenceSegment::Stream reader;
    for (;;) {
        auto add = incoming.all();
        if (add.empty())
            break;
        {
            Engine::Append append(entry, result, true);
            for (auto &v : reader.add(add)) {
                append.pushData<SequenceSegment>(std::move(v));
            }
        }
    }
    {
        Engine::Append append(entry, result, true);
        for (auto &v : reader.finish()) {
            append.pushData<SequenceSegment>(std::move(v));
        }
    }
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Access::method_read_ArchiveErasure(Engine::Entry &entry)
{
    CPD3::Data::Archive::Selection::List selections;
    for (std::size_t i = 1, max = entry.size(); i < max; i++) {
        selections.emplace_back(Libs::Archive::Selection::extract(entry, entry[i]));
    }
    if (selections.empty()) {
        entry.throwError("No selections provided");
        return;
    }

    CPD3::Data::ErasureSink::Iterator incoming;
    access->readErasure(selections, &incoming)->detach();

    auto erasureResult = entry.pushTable();
    auto archiveResult = entry.pushTable();
    for (;;) {
        auto add = incoming.all();
        if (add.first.empty() && add.second.empty())
            break;
        {
            Engine::Append append(entry, archiveResult, true);
            for (auto &v : add.first) {
                append.pushData<ArchiveValue>(std::move(v));
            }
        }
        {
            Engine::Append append(entry, erasureResult, true);
            for (auto &v : add.second) {
                append.pushData<ArchiveErasure>(std::move(v));
            }
        }
    }
    entry.replaceWithBack(0);
    entry.replaceWithBack(1);
    entry.propagate(2);
}

void Access::method_stream_SequenceValue(Engine::Entry &entry)
{
    CPD3::Data::Archive::Selection::List selections;
    for (std::size_t i = 1, max = entry.size(); i < max; i++) {
        selections.emplace_back(Libs::Archive::Selection::extract(entry, entry[i]));
    }
    if (selections.empty()) {
        entry.throwError("No selections provided");
        return;
    }

    class Stream : public BaseStream {
        std::shared_ptr<CPD3::Data::Archive::Access> access;
        CPD3::Data::ArchiveSink::Iterator incoming;
        CPD3::Data::Archive::Access::ArchiveHandle stream;
    public:
        Stream(const std::shared_ptr<CPD3::Data::Archive::Access> &access,
               const CPD3::Data::Archive::Selection::List &selections) : access(access),
                                                                         incoming(),
                                                                         stream(this->access
                                                                                    ->readArchive(
                                                                                            selections,
                                                                                            &incoming))
        { }

        ~Stream()
        {
            stream->signalTerminate();
            stream->wait();
        }

        virtual bool pushNext(Engine::Frame &target)
        {
            if (!incoming.hasNext())
                return false;
            target.pushData<ArchiveValue>(incoming.next());
            return true;
        }
    };

    entry.pushData<Stream>(access, selections);
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Access::method_stream_ValueSegment(Engine::Entry &entry)
{
    CPD3::Data::Archive::Selection::List selections;
    for (std::size_t i = 1, max = entry.size(); i < max; i++) {
        selections.emplace_back(Libs::Archive::Selection::extract(entry, entry[i]));
        selections.back().includeMetaArchive = false;
    }
    if (selections.empty()) {
        entry.throwError("No selections provided");
        return;
    }

    class Stream : public BaseStream {
        std::shared_ptr<CPD3::Data::Archive::Access> access;
        CPD3::Data::StreamSink::Iterator incoming;
        CPD3::Data::Archive::Access::StreamHandle stream;
        bool finished;
        std::deque<CPD3::Data::ValueSegment> pending;
        CPD3::Data::ValueSegment::Stream reader;
    public:
        Stream(const std::shared_ptr<CPD3::Data::Archive::Access> &access,
               const CPD3::Data::Archive::Selection::List &selections) : access(access),
                                                                         incoming(),
                                                                         stream(this->access
                                                                                    ->readStream(
                                                                                            selections,
                                                                                            &incoming)),
                                                                         finished(false)
        { }

        ~Stream()
        {
            stream->signalTerminate();
            stream->wait();
        }

        virtual bool pushNext(Engine::Frame &target)
        {
            for (;;) {
                if (!pending.empty()) {
                    target.pushData<ValueSegment>(std::move(pending.front()));
                    pending.pop_front();
                    return true;
                }
                if (finished)
                    return false;
                auto add = incoming.all();
                if (add.empty()) {
                    finished = true;
                    Util::append(reader.finish(), pending);
                    continue;
                }
                Util::append(reader.add(std::move(add)), pending);
            }
            Q_ASSERT(false);
            return false;
        }
    };

    entry.pushData<Stream>(access, selections);
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Access::method_stream_StreamSegment(Engine::Entry &entry)
{
    CPD3::Data::Archive::Selection::List selections;
    for (std::size_t i = 1, max = entry.size(); i < max; i++) {
        selections.emplace_back(Libs::Archive::Selection::extract(entry, entry[i]));
    }
    if (selections.empty()) {
        entry.throwError("No selections provided");
        return;
    }

    class Stream : public BaseStream {
        std::shared_ptr<CPD3::Data::Archive::Access> access;
        CPD3::Data::StreamSink::Iterator incoming;
        CPD3::Data::Archive::Access::StreamHandle stream;
        bool finished;
        std::deque<CPD3::Data::SequenceSegment> pending;
        CPD3::Data::SequenceSegment::Stream reader;
    public:
        Stream(const std::shared_ptr<CPD3::Data::Archive::Access> &access,
               const CPD3::Data::Archive::Selection::List &selections) : access(access),
                                                                         incoming(),
                                                                         stream(this->access
                                                                                    ->readStream(
                                                                                            selections,
                                                                                            &incoming)),
                                                                         finished(false)
        { }

        ~Stream()
        {
            stream->signalTerminate();
            stream->wait();
        }

        virtual bool pushNext(Engine::Frame &target)
        {
            for (;;) {
                if (!pending.empty()) {
                    target.pushData<SequenceSegment>(std::move(pending.front()));
                    pending.pop_front();
                    return true;
                }
                if (finished)
                    return false;
                auto add = incoming.all();
                if (add.empty()) {
                    finished = true;
                    Util::append(reader.finish(), pending);
                    continue;
                }
                Util::append(reader.add(std::move(add)), pending);
            }
            Q_ASSERT(false);
            return false;
        }
    };

    entry.pushData<Stream>(access, selections);
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Access::method_stream_ArchiveErasure(Engine::Entry &entry)
{
    CPD3::Data::Archive::Selection::List selections;
    for (std::size_t i = 1, max = entry.size(); i < max; i++) {
        selections.emplace_back(Libs::Archive::Selection::extract(entry, entry[i]));
    }
    if (selections.empty()) {
        entry.throwError("No selections provided");
        return;
    }

    class Stream : public BaseStream {
        std::shared_ptr<CPD3::Data::Archive::Access> access;
        CPD3::Data::ErasureSink::Iterator incoming;
        CPD3::Data::Archive::Access::ErasureHandle stream;
    public:
        Stream(const std::shared_ptr<CPD3::Data::Archive::Access> &access,
               const CPD3::Data::Archive::Selection::List &selections) : access(access),
                                                                         incoming(),
                                                                         stream(this->access
                                                                                    ->readErasure(
                                                                                            selections,
                                                                                            &incoming))
        { }

        ~Stream()
        {
            stream->signalTerminate();
            stream->wait();
        }

        virtual bool pushNext(Engine::Frame &target)
        {
            if (!incoming.hasNext())
                return false;
            if (incoming.isNextErasure()) {
                target.pushData<ArchiveErasure>(incoming.erasureNext());
                target.push(true);
            } else {
                target.pushData<ArchiveValue>(incoming.archiveNext());
                target.push(false);
            }
            return true;
        }
    };

    entry.pushData<Stream>(access, selections);
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Access::method_exists(Engine::Entry &entry)
{
    CPD3::Data::Archive::Access::AvailableGranularity
            granularity = CPD3::Data::Archive::Access::SpanningOnly;
    CPD3::Data::Archive::Selection::List selections;
    for (std::size_t i = 1, max = entry.size(); i < max; i++) {
        auto arg = entry[i];

        selections.emplace_back(Libs::Archive::Selection::extract(entry, arg));
        selections.back().includeMetaArchive = false;

        {
            Engine::Frame frame(entry);
            frame.push(arg, "granularity");
            auto check = frame.back();
            if (!check.isNil()) {
                auto str = check.toString();
                if (Util::equal_insensitive(str, "all", "readall")) {
                    granularity = CPD3::Data::Archive::Access::ReadAll;
                } else if (Util::equal_insensitive(str, "index", "indexonly")) {
                    granularity = CPD3::Data::Archive::Access::IndexOnly;
                } else {
                    granularity = CPD3::Data::Archive::Access::SpanningOnly;
                }
            }
        }
    }
    if (selections.empty()) {
        entry.throwError("No selections provided");
        return;
    }

    entry.clear();
    entry.push(access->availableExists(selections, granularity));
    entry.propagate(1);
}

Access::~Access() = default;


void install(Engine::Table &target, Engine::Frame &frame)
{
    Engine::Frame local(frame);
    auto table = local.pushTable();
    Selection::install(table, local);
    Access::install(table, local);
    target.set("Archive", table);
}

}
}
}
}