/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "csv.hxx"
#include "core/csv.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {
namespace CSV {

void install(Engine::Table &target, Engine::Frame &frame)
{
    Engine::Frame local(frame);
    auto table = local.pushTable();
    {
        Engine::Assign assign(local, table, "split");
        assign.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Insufficient arguments");
                return;
            }

            auto input = entry[0].toQString();
            QString separator(',');
            if (entry.size() > 1) {
                separator = entry[1].toQString();
                if (separator.isEmpty())
                    separator = ",";
            }
            QString quote('\"');
            if (entry.size() > 2) {
                quote = entry[2].toQString();
                if (quote.isEmpty())
                    quote = "\"";
            }
            QString quoteEscape("\"\"\"");
            if (entry.size() > 3) {
                quoteEscape = entry[3].toQString();
                if (quoteEscape.isEmpty())
                    quoteEscape = "\"\"\"";
            }

            entry.clear();
            for (const auto &p : CPD3::CSV::split(input, separator, quote, quoteEscape)) {
                entry.push(p);
            }
            entry.propagate();
        }));
    }
    {
        Engine::Assign assign(local, table, "join");
        assign.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Insufficient arguments");
                return;
            }


            QStringList fields;
            {
                Engine::Iterator it(entry, entry[0]);
                while (it.next()) {
                    fields.append(it.value().toQString());
                }
            }

            auto input = entry[0].toQString();
            QString separator(',');
            if (entry.size() > 1) {
                separator = entry[1].toQString();
                if (separator.isEmpty())
                    separator = ",";
            }
            QString quote('\"');
            if (entry.size() > 2) {
                quote = entry[2].toQString();
                if (quote.isEmpty())
                    quote = "\"";
            }
            QString quoteEscape("\"\"\"");
            if (entry.size() > 3) {
                quoteEscape = entry[3].toQString();
                if (quoteEscape.isEmpty())
                    quoteEscape = "\"\"\"";
            }

            entry.clear();
            entry.push(CPD3::CSV::join(fields, separator, quote, quoteEscape));
            entry.propagate(1);
        }));
    }
    target.set("CSV", table);
}

}
}
}
}
