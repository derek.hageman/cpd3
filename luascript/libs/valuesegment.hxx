/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUASCRIPTLIBS_VALUESEGMENT_HXX
#define CPD3LUASCRIPTLIBS_VALUESEGMENT_HXX

#include "core/first.hxx"

#include "luascript/luascript.hxx"
#include "luascript/engine.hxx"
#include "datacore/segment.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {


/**
 * A wrapper around a ValueSegment CPD3 value.  The Lua API follows the C++ one, with
 * the exception that table-like access to the components (start/front, end/back, and value)
 * is also permitted.  Any other table-like access is treated as a path access within
 * the backing variant.
 */
class CPD3LUASCRIPT_EXPORT ValueSegment : public Engine::Data {
    double start;
    double end;
    CPD3::Data::Variant::Write contents;

    void meta_tostring(Engine::Entry &entry);

    void meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues);

    void meta_newindex(Engine::Entry &entry);

    void method_detach(Engine::Entry &entry);

    void method_set(Engine::Entry &entry);

public:
    ValueSegment();

    virtual ~ValueSegment();

    explicit ValueSegment(const CPD3::Data::ValueSegment &segment);

    explicit ValueSegment(CPD3::Data::ValueSegment &&segment);

    /**
     * Install the library on an engine target.
     *
     * @param target    the target table to put symbols in
     * @param frame     a frame to create in
     */
    static void install(Engine::Table &target, Engine::Frame &frame);

    /**
     * Extract a value from Lua code.  This detaches the value from
     * the script references.
     *
     * @param frame     the frame to work in
     * @param ref       the reference to read from
     * @return          the extracted value
     */
    static CPD3::Data::ValueSegment extract(Engine::Frame &frame, const Engine::Reference &ref);

    /**
     * Get the current contents of the value.  This detaches the value from
     * the script references.
     *
     * @return  the segment
     */
    CPD3::Data::ValueSegment get() const;

    /**
     * Get the contents of the value.
     *
     * @return  the contents
     */
    const CPD3::Data::Variant::Write &getVariant() const
    { return contents; }

protected:
    void initialize(const Engine::Reference &self, Engine::Frame &frame) override;
};

}
}
}

#endif //CPD3LUASCRIPTLIBS_VALUESEGMENT_HXX
