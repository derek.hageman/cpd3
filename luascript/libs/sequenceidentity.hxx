/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUASCRIPTLIBS_SEQUENCEIDENTITY_HXX
#define CPD3LUASCRIPTLIBS_SEQUENCEIDENTITY_HXX

#include "core/first.hxx"

#include "luascript/luascript.hxx"
#include "luascript/engine.hxx"
#include "datacore/stream.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {

/**
 * A wrapper around a SequenceIdentity CPD3 value.  The Lua API follows the C++ one, with
 * the exception that table-like access to the components (station, archive, variable, flavors,
 * start/front, end/back, priority, name) is also permitted.
 */
class CPD3LUASCRIPT_EXPORT SequenceIdentity : public Engine::Data {
    CPD3::Data::SequenceIdentity identity;
    bool detachedName;

    void meta_eq(Engine::Entry &entry);

    void meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues);

    void meta_newindex(Engine::Entry &entry);

    void method_overlayLessThan(Engine::Entry &entry);

    void method_timeLessThan(Engine::Entry &entry);

    void meta_tostring(Engine::Entry &entry);

    static CPD3::Data::SequenceIdentity constructFromFrame(Engine::Frame &frame);

    void fetchDetachedName(Engine::Frame &frame, const Engine::Reference &self) const;

public:
    SequenceIdentity();

    virtual ~SequenceIdentity();

    explicit SequenceIdentity(const CPD3::Data::SequenceIdentity &identity);

    explicit SequenceIdentity(CPD3::Data::SequenceIdentity &&identity);

    /**
     * Install the library on an engine target.
     *
     * @param target    the target table to put symbols in
     * @param frame     a frame to create in
     */
    static void install(Engine::Table &target, Engine::Frame &frame);

    /**
     * Extract a value from Lua code.
     *
     * @param frame     the frame to work in
     * @param ref       the reference to read from
     * @return          the extracted value
     */
    static CPD3::Data::SequenceIdentity extract(Engine::Frame &frame, const Engine::Reference &ref);

    /**
     * Assign an identity from a table.
     *
     * @param identity  the target identity
     * @param frame     the working frame
     * @param table     the source table
     */
    static void assignFromTable(CPD3::Data::SequenceIdentity &identity,
                                Engine::Frame &frame,
                                const Engine::Table &table);


    /**
     * Change the wrapper to reference a detached SequenceName instead of the internally
     * stored one.  This is used to inform the wrapper of a name that has already been
     * detached by other means.
     *
     * @param frame     a working frame
     * @param self      a reference to the Lua object
     * @param name      a reference to the detached name
     */
    void detachName(Engine::Frame &frame,
                    const Engine::Reference &self,
                    const Engine::Reference &name);


    /**
     * Test if the wrapper has a detached name reference.
     *
     * @return  true if the wrapper has detached the name
     */
    bool hasDetachedName() const
    { return detachedName; }

    /**
     * Get the current contents of the sequence identity.
     *
     * @return  the sequence identity
     */
    inline const CPD3::Data::SequenceIdentity &get() const
    {
        Q_ASSERT(!hasDetachedName());
        return identity;
    }

    /**
     * Get the current contents of the sequence identity.
     *
     * @return  the sequence identity
     */
    inline CPD3::Data::SequenceIdentity &get()
    {
        Q_ASSERT(!hasDetachedName());
        return identity;
    }

    /**
     * Get the current contents of the sequence identity.
     * <br>
     * This variant does not require an attached name, but it is only safe to operate
     * on the other components (start, end, priority).
     *
     * @return  the sequence identity
     */
    inline const CPD3::Data::SequenceIdentity &getAttached() const
    { return identity; }

    /**
     * Get the current contents of the sequence identity.
     * <br>
     * This variant does not require an attached name, but it is only safe to operate
     * on the other components (start, end, priority).
     *
     * @return  the sequence identity
     */
    inline CPD3::Data::SequenceIdentity &getAttached()
    { return identity; }

    /**
     * Get the current contents of the sequence identity.
     *
     * @param frame the working frame
     * @param self  a reference to the Lua object
     * @return      the assembled identity
     */
    CPD3::Data::SequenceIdentity get(Engine::Frame &frame, const Engine::Reference &self) const;

    /**
     * Push the detached name, creating it if required.
     *
     * @param frame     the target frame
     * @param self      a reference to the Lua object
     */
    void pushDetachedName(Engine::Frame &frame, const Engine::Reference &self);

    /**
     * Get a description of an identity.
     *
     * @param identity  the identity to describe
     * @return          the descriptive string
     */
    static std::string describe(const CPD3::Data::SequenceIdentity &identity);

protected:
    void initialize(const Engine::Reference &self, Engine::Frame &frame) override;
};

}
}
}

#endif //CPD3LUASCRIPTLIBS_SEQUENCEIDENTITY_HXX
