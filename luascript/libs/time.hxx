/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUASCRIPTLIBS_TIME_HXX
#define CPD3LUASCRIPTLIBS_TIME_HXX

#include "core/first.hxx"

#include "luascript/luascript.hxx"
#include "luascript/engine.hxx"
#include "core/timeutils.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {
namespace Time {

/**
 * Extract a single time from a Lua value.
 *
 * @param frame the working frame
 * @param ref   the Lua reference
 * @return      a time
 */
CPD3LUASCRIPT_EXPORT double extractSingle(Engine::Frame &frame, const Engine::Reference &ref);

/**
 * Extract a pair of bounds from a Lua value.
 *
 * @param frame the working frame
 * @param ref   the Lua reference
 * @return      the bounds
 */
CPD3LUASCRIPT_EXPORT CPD3::Time::Bounds extractBounds(Engine::Frame &frame,
                                                      const Engine::Reference &ref);

/**
 * Extract a pair of bounds from a Lua value.
 *
 * @param frame the working frame
 * @param start the Lua start time
 * @param end   the Lua end time
 * @return      the bounds
 */
CPD3LUASCRIPT_EXPORT CPD3::Time::Bounds extractBounds(Engine::Frame &frame,
                                                      const Engine::Reference &start,
                                                      const Engine::Reference &end);

/**
 * Extract a logical time unit.
 *
 * @param frame the working frame
 * @param unit  the Lua unit
 * @return      the unit
 */
CPD3LUASCRIPT_EXPORT CPD3::Time::LogicalTimeUnit extractUnit(Engine::Frame &frame,
                                                             const Engine::Reference &unit);

/**
 * Lookup a start time from a member of a table.
 *
 * @param frame     the working frame
 * @param origin    the originating table
 * @return          a reference to a possible start or nil if none was found
 */
CPD3LUASCRIPT_EXPORT Engine::Reference pushStart(Engine::Frame &frame,
                                                 const Engine::Reference &origin);
/**
 * Lookup a end time from a member of a table.
 *
 * @param frame     the working frame
 * @param origin    the originating table
 * @return          a reference to a possible end or nil if none was found
 */
CPD3LUASCRIPT_EXPORT Engine::Reference pushEnd(Engine::Frame &frame,
                                               const Engine::Reference &origin);

/**
 * Install the library on an engine target.
 *
 * @param target    the target table to put symbols in
 * @param frame     a frame to create in
 */
CPD3LUASCRIPT_EXPORT void install(Engine::Table &target, Engine::Frame &frame);

}
}
}
}

#endif //CPD3LUASCRIPTLIBS_TIME_HXX
