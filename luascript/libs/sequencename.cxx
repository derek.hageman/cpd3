/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "sequencename.hxx"
#include "sequenceidentity.hxx"
#include "streamvalue.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {

static const std::string metatableRegistryName = "CPD3_sequencename_metatable";

void SequenceName::install(Engine::Table &target, Engine::Frame &frame)
{
    auto mt = pushMetatable(frame);

    {
        Engine::Assign assign(frame, mt, "__eq");
        pushMethod<SequenceName>(assign, &SequenceName::meta_eq);
    }
    {
        Engine::Assign assign(frame, mt, "__lt");
        pushMethod<SequenceName>(assign, &SequenceName::meta_lt);
    }
    {
        Engine::Assign assign(frame, mt, "__le");
        pushMethod<SequenceName>(assign, &SequenceName::meta_le);
    }
    {
        Engine::Assign assign(frame, mt, "__tostring");
        pushMethod<SequenceName>(assign, &SequenceName::meta_tostring);
    }
    {
        Engine::Assign assign(frame, mt, "__newindex");
        pushMethod<SequenceName>(assign, &SequenceName::meta_newindex);
    }

    {
        Engine::Assign assign(frame, mt, "__index");
        auto methods = assign.pushTable();

        {
            Engine::Assign ma(assign, methods, "toDefaultStation");
            pushMethod<SequenceName>(ma, &SequenceName::method_toDefaultStation);
        }
        {
            Engine::Assign ma(assign, methods, "isDefaultStation");
            pushMethod<SequenceName>(ma, &SequenceName::method_isDefaultStation);
        }
        {
            Engine::Assign ma(assign, methods, "setDefaultStation");
            pushMethod<SequenceName>(ma, &SequenceName::method_setDefaultStation);
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceName>(mf, &SequenceName::method_toMeta);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "toMeta");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "toMetadata");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceName>(mf, &SequenceName::method_fromMeta);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "fromMeta");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "fromMetadata");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceName>(mf, &SequenceName::method_isMeta);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "isMeta");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "isMetadata");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceName>(mf, &SequenceName::method_setMeta);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "setMeta");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "setMetadata");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceName>(mf, &SequenceName::method_clearMeta);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "clearMeta");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "clearMetadata");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceName>(mf, &SequenceName::method_hasFlavor);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "hasFlavor");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "hasFlavors");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceName>(mf, &SequenceName::method_hasAnyFlavor);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "hasAnyFlavor");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "hasAnyFlavors");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceName>(mf, &SequenceName::method_lacksFlavor);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "lacksFlavor");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "lacksFlavors");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceName>(mf, &SequenceName::method_withFlavor);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "withFlavor");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "withFlavors");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceName>(mf, &SequenceName::method_withoutFlavor);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "withoutFlavor");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "withoutFlavors");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "withoutAllFlavors");
            pushMethod<SequenceName>(ma, &SequenceName::method_withoutAllFlavors);
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceName>(mf, &SequenceName::method_addFlavor);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "addFlavor");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "addFlavors");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceName>(mf, &SequenceName::method_removeFlavor);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "removeFlavor");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "removeFlavors");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<SequenceName>(mf, &SequenceName::method_clearFlavors);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "clearFlavors");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "removeAllFlavors");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "displayLessThan");
            pushMethod<SequenceName>(ma, &SequenceName::method_displayLessThan);
        }

        pushMethod<SequenceName>(assign, &SequenceName::meta_index, 1);
    }

    frame.registry().set(metatableRegistryName, mt);
    {
        Engine::Frame local(frame);
        local.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            auto value = constructFromFrame(entry);
            entry.clear();
            entry.pushData<SequenceName>(std::move(value));
            entry.propagate(1);
        }));
        target.set("SequenceName", local.back());
    }
}

void SequenceName::initialize(const Engine::Reference &self, Engine::Frame &frame)
{
    frame.push(frame.registry(), metatableRegistryName);
    setMetatable(self, frame.back());
}

SequenceName::SequenceName() = default;

SequenceName::~SequenceName() = default;

SequenceName::SequenceName(const CPD3::Data::SequenceName &name) : name(name)
{ }

SequenceName::SequenceName(CPD3::Data::SequenceName &&name) : name(std::move(name))
{ }

CPD3::Data::SequenceName SequenceName::fromString(const std::string &raw)
{
    auto components = Util::split_string(raw, ':', true);
    switch (components.size()) {
    case 0:
        return CPD3::Data::SequenceName();
    case 1:
        return CPD3::Data::SequenceName({}, {}, std::move(components[0]));
    case 2:
        return CPD3::Data::SequenceName({}, std::move(components[0]), std::move(components[1]));
    case 3:
        return CPD3::Data::SequenceName(std::move(components[0]), std::move(components[1]),
                                        std::move(components[2]));
    default:
        break;
    }

    CPD3::Data::SequenceName::Flavors flavors;
    for (std::size_t i = 3, max = components.size(); i < max; i++) {
        if (components[i].empty())
            continue;
        flavors.insert(std::move(components[i]));
    }

    return CPD3::Data::SequenceName(std::move(components[0]), std::move(components[1]),
                                    std::move(components[2]), std::move(flavors));
}

CPD3::Data::SequenceName::Flavors SequenceName::convertFlavors(Engine::Frame &frame,
                                                               const Engine::Reference &ref)
{
    CPD3::Data::SequenceName::Flavors flavors;
    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Number:
        break;
    case Engine::Reference::Type::String: {
        auto flavor = ref.toString();
        if (flavor.empty())
            break;
        flavors.insert(std::move(flavor));
        break;
    }
    default: {
        Engine::Iterator it(frame, ref);
        while (it.next()) {
            if (it.key().getType() == Engine::Reference::Type::String) {
                if (!it.value().toBoolean())
                    continue;
                auto flavor = it.key().toString();
                if (flavor.empty())
                    continue;
                flavors.insert(std::move(flavor));
                continue;
            }
            auto flavor = it.value().toString();
            if (flavor.empty())
                continue;
            flavors.insert(std::move(flavor));
            continue;
        }
        break;
    }
    }
    return flavors;
}

static CPD3::Data::SequenceName::Flavors convertFlavors(Engine::Frame &frame,
                                                        std::size_t firstIndex)
{
    CPD3::Data::SequenceName::Flavors flavors;
    for (std::size_t i = firstIndex, max = frame.size(); i < max; i++) {
        Util::merge(SequenceName::convertFlavors(frame, frame[i]), flavors);
    }
    return flavors;
}

void SequenceName::assignFromTable(CPD3::Data::SequenceName &name,
                                   Engine::Frame &frame,
                                   const Engine::Table &table)
{
    {
        Engine::Iterator it(frame, table);
        bool arrayFlavors = false;
        CPD3::Data::SequenceName::Flavors assignFlavors;
        while (it.next()) {
            auto key = it.key();

            switch (key.getType()) {
            case Engine::Reference::Type::Number: {
                switch (it.key().toInteger()) {
                case 0:
                    break;
                case 1:
                    if (it.value().getType() == Engine::Reference::Type::String)
                        name.setStation(it.value().toString());
                    break;
                case 2:
                    if (it.value().getType() == Engine::Reference::Type::String)
                        name.setArchive(it.value().toString());
                    break;
                case 3:
                    if (it.value().getType() == Engine::Reference::Type::String)
                        name.setVariable(it.value().toString());
                    break;
                default:
                    arrayFlavors = true;
                    Util::merge(convertFlavors(it, it.value()), assignFlavors);
                    break;
                }
                break;
            }
            default:
                break;
            }
        }
        if (arrayFlavors) {
            name.setFlavors(std::move(assignFlavors));
        }
    }

    {
        auto check = table.get("station");
        if (check.getType() == Engine::Output::Type::String) {
            name.setStation(check.toString());
        }
    }
    {
        auto check = table.get("archive");
        if (check.getType() == Engine::Output::Type::String) {
            name.setArchive(check.toString());
        }
    }
    {
        auto check = table.get("variable");
        if (check.getType() == Engine::Output::Type::String) {
            name.setVariable(check.toString());
        }
    }
    {
        Engine::Frame local(frame);
        local.push(table, "flavors");
        auto check = local.back();
        if (!check.isNil()) {
            name.setFlavors(convertFlavors(local, check));
        }
    }
}

CPD3::Data::SequenceName SequenceName::constructFromFrame(Engine::Frame &frame)
{
    std::size_t n = frame.size();
    switch (n) {
    case 0:
        return CPD3::Data::SequenceName();
    case 1:
        return extract(frame, frame.front());
    default:
        break;
    }

    if (n < 3) {
        frame.throwError("Insufficient arguments to SequenceName constructor");
        return CPD3::Data::SequenceName();
    }

    Q_ASSERT(n >= 3);

    return CPD3::Data::SequenceName(frame[0].toString(), frame[1].toString(), frame[2].toString(),
                                    Libs::convertFlavors(frame, 3));
}

CPD3::Data::SequenceName SequenceName::extract(Engine::Frame &frame, const Engine::Reference &ref)
{
    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Number:
        return CPD3::Data::SequenceName();
    case Engine::Reference::Type::Data: {
        if (auto check = ref.toData<SequenceName>()) {
            return check->name;
        } else if (auto check = ref.toData<SequenceIdentity>()) {
            return check->get(frame, ref).getName();
        } else if (auto check = ref.toData<SequenceValue>()) {
            return check->getName(frame, ref);
        } else if (auto check = ref.toData<ArchiveErasure>()) {
            return check->getName(frame, ref);
        } else {
            /* Use table conversion */
            break;
        }
    }
    case Engine::Reference::Type::String:
        return fromString(ref.toString());
    default:
        break;
    }

    CPD3::Data::SequenceName result;
    assignFromTable(result, frame, ref);
    return result;
}

void SequenceName::meta_eq(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid equality arguments");
        return;
    }
    auto check = entry[1].toData<SequenceName>();
    if (!check) {
        entry.clear();
        entry.push(false);
        entry.propagate(1);
        return;
    }
    bool result = (name == check->name);
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void SequenceName::meta_lt(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid less than arguments");
        return;
    }
    auto check = entry[1].toData<SequenceName>();
    if (!check) {
        entry.throwError("Cannot compare with an unknown type");
        return;
    }
    bool result = CPD3::Data::SequenceName::OrderLogical()(name, check->name);
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void SequenceName::meta_le(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid less than arguments");
        return;
    }
    auto check = entry[1].toData<SequenceName>();
    if (!check) {
        entry.throwError("Cannot compare with an unknown type");
        return;
    }
    bool result =
            (name == check->name) || CPD3::Data::SequenceName::OrderLogical()(name, check->name);
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void SequenceName::pushFlavors(Engine::Frame &frame,
                               const CPD3::Data::SequenceName::Flavors &flavors)
{
    auto result = frame.pushTable();
    for (const auto &f : flavors) {
        Engine::Assign assign(frame, result, f);
        assign.push(true);
    }
}

void SequenceName::meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid index");
        return;
    }
    if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
        return;
    auto key = entry[1].toString();
    if (key == "station") {
        entry.push(name.getStation());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "archive") {
        entry.push(name.getArchive());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "variable") {
        entry.push(name.getVariable());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else if (key == "flavors") {
        pushFlavors(entry, name.getFlavors());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    } else {
        entry.clear();
        entry.pushNil();
        entry.propagate(1);
        return;
    }
}

void SequenceName::meta_newindex(Engine::Entry &entry)
{
    if (entry.size() < 3) {
        entry.throwError("Invalid assignment");
        return;
    }
    auto key = entry[1].toString();
    if (key == "station") {
        name.setStation(entry[2].toString());
    } else if (key == "archive") {
        name.setArchive(entry[2].toString());
    } else if (key == "variable") {
        name.setVariable(entry[2].toString());
    } else if (key == "flavors") {
        name.setFlavors(convertFlavors(entry, entry[2]));
    } else {
        entry.throwError("Invalid assignment");
        return;
    }
}

void SequenceName::method_toDefaultStation(Engine::Entry &entry)
{
    auto result = name.toDefaultStation();
    entry.clear();
    entry.pushData<SequenceName>(std::move(result));
    entry.propagate(1);
}

void SequenceName::method_isDefaultStation(Engine::Entry &entry)
{
    auto result = name.isDefaultStation();
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void SequenceName::method_setDefaultStation(Engine::Entry &entry)
{
    name = name.toDefaultStation();
}

void SequenceName::method_toMeta(Engine::Entry &entry)
{
    auto result = name.toMeta();
    entry.clear();
    entry.pushData<SequenceName>(std::move(result));
    entry.propagate(1);
}

void SequenceName::method_fromMeta(Engine::Entry &entry)
{
    auto result = name.fromMeta();
    entry.clear();
    entry.pushData<SequenceName>(std::move(result));
    entry.propagate(1);
}

void SequenceName::method_isMeta(Engine::Entry &entry)
{
    auto result = name.isMeta();
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void SequenceName::method_setMeta(Engine::Entry &entry)
{
    name.setMeta();
}

void SequenceName::method_clearMeta(Engine::Entry &entry)
{
    name.clearMeta();
}

void SequenceName::method_hasFlavor(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    bool result = true;
    for (const auto &f : Libs::convertFlavors(entry, 1)) {
        if (name.hasFlavor(f))
            continue;
        result = false;
        break;
    }
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void SequenceName::method_hasAnyFlavor(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    bool result = false;
    for (const auto &f : Libs::convertFlavors(entry, 1)) {
        if (!name.hasFlavor(f))
            continue;
        result = true;
        break;
    }
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void SequenceName::method_lacksFlavor(Engine::Entry &entry)
{
    bool result = true;
    for (const auto &f : Libs::convertFlavors(entry, 1)) {
        if (name.lacksFlavor(f))
            continue;
        result = false;
        break;
    }
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void SequenceName::method_withFlavor(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = name;
    auto flavors = result.getFlavors();
    Util::merge(Libs::convertFlavors(entry, 1), flavors);
    result.setFlavors(std::move(flavors));
    entry.clear();
    entry.pushData<SequenceName>(std::move(result));
    entry.propagate(1);
}

void SequenceName::method_withoutFlavor(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto result = name;
    for (const auto &f : Libs::convertFlavors(entry, 1)) {
        result = result.withoutFlavor(f);
    }
    entry.clear();
    entry.pushData<SequenceName>(std::move(result));
    entry.propagate(1);
}

void SequenceName::method_withoutAllFlavors(Engine::Entry &entry)
{
    auto result = name.withoutAllFlavors();
    entry.clear();
    entry.pushData<SequenceName>(std::move(result));
    entry.propagate(1);
}

void SequenceName::method_addFlavor(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    name.addFlavors(Libs::convertFlavors(entry, 1));
}

void SequenceName::method_removeFlavor(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    name.removeFlavors(Libs::convertFlavors(entry, 1));
}

void SequenceName::method_clearFlavors(Engine::Entry &entry)
{
    name.clearFlavors();
}

void SequenceName::meta_tostring(Engine::Entry &entry)
{
    auto result = name.describe();
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void SequenceName::method_displayLessThan(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid arguments");
        return;
    }
    auto check = entry[1].toData<SequenceName>();
    if (!check) {
        entry.throwError("Cannot compare with an unknown type");
        return;
    }
    bool result = CPD3::Data::SequenceName::OrderDisplay()(name, check->name);
    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

}
}
}