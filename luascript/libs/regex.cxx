/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <memory>
#include <unordered_map>

#include <QRegularExpressionMatchIterator>
#include <QStringList>

#include "regex.hxx"
#include "core/number.hxx"
#include "core/textsubstitution.hxx"


namespace CPD3 {
namespace Lua {
namespace Libs {

static const std::string metatableRegistryName = "CPD3_regex_metatable";

static const std::unordered_map<std::string, QRegularExpression::PatternOptions> optionMapping
        {{"icase",       QRegularExpression::CaseInsensitiveOption},
         {"ignore_case", QRegularExpression::CaseInsensitiveOption},
         {"multiline",   QRegularExpression::MultilineOption},};

void Regex::install(Engine::Table &target, Engine::Frame &frame)
{
    auto mt = pushMetatable(frame);

    {
        Engine::Assign assign(frame, mt, "__call");
        pushMethod<Regex>(assign, &Regex::method_exact_match);
    }
    {
        Engine::Assign assign(frame, mt, "__len");
        pushMethod<Regex>(assign, &Regex::meta_len);
    }
    {
        Engine::Assign assign(frame, mt, "__tostring");
        pushMethod<Regex>(assign, &Regex::meta_tostring);
    }
    {
        Engine::Assign assign(frame, mt, "__newindex");
        pushMethod<Regex>(assign, &Regex::meta_newindex);
    }

    {
        Engine::Assign assign(frame, mt, "__index");
        auto methods = assign.pushTable();

        {
            Engine::Frame mf(assign);
            pushMethod<Regex>(mf, &Regex::method_match);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "find");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "match");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Regex>(mf, &Regex::method_exact_match);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "exact_match");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "exactMatch");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "exact");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Regex>(mf, &Regex::method_global_match);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "global_match");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "globalMatch");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "gmatch");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<Regex>(mf, &Regex::method_replace);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "replace");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "gsub");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "split");
            pushMethod<Regex>(ma, &Regex::method_split);
        }
        {
            Engine::Assign ma(assign, methods, "escape");
            pushMethod<Regex>(ma, &Regex::method_escape);
        }

        pushMethod<Regex>(assign, &Regex::meta_index, 1);
    }

    frame.registry().set(metatableRegistryName, mt);

    {
        Engine::Frame local(frame);
        local.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Insufficient arguments");
                return;
            }

            if (auto existing = entry[0].toData<Regex>()) {
                entry.pushData<Regex>(existing->pattern);
                entry.replaceWithBack(0);
                entry.propagate(1);
                return;
            }

            QRegularExpression::PatternOptions options = QRegularExpression::NoPatternOption;
            if (entry.size() > 1) {
                Engine::Iterator add(entry, entry[1]);
                while (add.next()) {
                    if (add.key().getType() == Engine::Reference::Type::String) {
                        auto check = optionMapping.find(add.key().toString());
                        if (check == optionMapping.end())
                            continue;
                        if (add.value().toBoolean()) {
                            options |= check->second;
                        } else {
                            options &= ~check->second;
                        }
                        continue;
                    }
                    auto check = optionMapping.find(add.value().toString());
                    if (check == optionMapping.end())
                        continue;
                    options |= check->second;
                }
            }

            entry.pushData<Regex>(QRegularExpression(entry[0].toQString(), options));
            entry.replaceWithBack(0);
            entry.propagate(1);
        }));
        target.set("Regex", local.back());
    }
}

void Regex::initialize(const Engine::Reference &self, Engine::Frame &frame)
{
    frame.push(frame.registry(), metatableRegistryName);
    setMetatable(self, frame.back());
}

Regex::Regex() = default;

Regex::~Regex() = default;

Regex::Regex(const QRegularExpression &pattern) : pattern(pattern)
{ }

Regex::Regex(QRegularExpression &&pattern) : pattern(std::move(pattern))
{ }

void Regex::meta_tostring(Engine::Entry &entry)
{
    entry.push("/" + pattern.pattern() + "/");
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void Regex::meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid index");
        return;
    }
    if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
        return;

    auto key = entry[1].toString();
    if (key == "pattern") {
        entry.push(pattern.pattern());
        entry.replaceWithBack(0);
        entry.propagate(1);
        return;
    }

    auto check = optionMapping.find(key);
    if (check != optionMapping.end()) {
        bool present = !!(pattern.patternOptions() & check->second);
        entry.clear();
        entry.push(present);
        entry.propagate(1);
        return;
    }

    entry.clear();
    entry.pushNil();
    entry.propagate(1);
}

void Regex::meta_newindex(Engine::Entry &entry)
{
    if (entry.size() < 3) {
        entry.throwError("Invalid assignment");
        return;
    }

    auto key = entry[1].toString();

    auto check = optionMapping.find(key);
    if (check != optionMapping.end()) {
        auto options = pattern.patternOptions();
        if (entry[2].toBoolean()) {
            options |= check->second;
        } else {
            options &= ~check->second;
        }
        pattern.setPatternOptions(options);
        return;
    }

    entry.throwError("Invalid assignment");
}

void Regex::meta_len(Engine::Entry &entry)
{
    int n = pattern.captureCount();
    entry.clear();
    entry.push(n);
    entry.propagate(1);
}

void Regex::method_match(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Subject string required");
        return;
    }
    auto str = entry[1].toQString();

    int offset = 0;
    if (entry.size() > 2) {
        auto check = entry[2].toInteger();
        if (INTEGER::defined(check) && check != 0) {
            if (check > 0) {
                offset = static_cast<int>(check) - 1;
            } else {
                offset = str.size() - static_cast<int>(check);
            }
            if (offset < 0)
                offset = 0;
        }
    }

    bool capturesOnly = false;
    if (entry.size() > 3) {
        capturesOnly = entry[3].toBoolean();
    }

    auto r = pattern.match(str, offset);
    if (!r.hasMatch())
        return;

    entry.clear();
    if (!capturesOnly) {
        entry.push(r.capturedStart() + 1);
        entry.push(r.capturedEnd());
    } else {
        entry.push(r.captured());
    }
    auto caps = r.capturedTexts();
    if (caps.size() > 1) {
        for (auto cap = caps.cbegin() + 1, endCap = caps.cend(); cap != endCap; ++cap) {
            entry.push(*cap);
        }
    }
    entry.propagate();
}

void Regex::method_exact_match(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Subject string required");
        return;
    }
    auto str = entry[1].toQString();

    auto r = pattern.match(str, 0, QRegularExpression::NormalMatch,
                           QRegularExpression::AnchoredMatchOption);
    if (!r.hasMatch())
        return;
    if (r.capturedLength() != str.length())
        return;

    entry.clear();
    for (const auto &cap : r.capturedTexts()) {
        entry.push(cap);
    }
    entry.propagate();
}

void Regex::method_global_match(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Subject string required");
        return;
    }
    auto str = entry[1].toQString();

    struct State {
        QRegularExpressionMatchIterator iterator;
        bool capturesOnly;

        State(QRegularExpressionMatchIterator &&iterator, bool capturesOnly) : iterator(
                std::move(iterator)), capturesOnly(capturesOnly)
        { }
    };

    bool capturesOnly = true;
    if (entry.size() > 2) {
        capturesOnly = entry[2].toBoolean();
    }

    auto state = std::make_shared<State>(pattern.globalMatch(str), capturesOnly);
    entry.clear();
    entry.push(std::function<void(Engine::Entry &)>([state](Engine::Entry &entry) {
        if (!state->iterator.hasNext())
            return;
        auto r = state->iterator.next();

        entry.clear();
        if (!state->capturesOnly) {
            entry.push(r.capturedStart() + 1);
            entry.push(r.capturedEnd());
        } else {
            entry.push(r.captured());
        }
        auto caps = r.capturedTexts();
        if (caps.size() > 1) {
            for (auto cap = caps.cbegin() + 1, endCap = caps.cend(); cap != endCap; ++cap) {
                entry.push(*cap);
            }
        }
        entry.propagate();
    }));
    entry.propagate(1);
}

static QString applyReplacement(Engine::Frame &frame,
                                const Engine::Reference &rep,
                                const QStringList &captures)
{
    if (rep.isNil())
        return {};
    switch (rep.getType()) {
    case Engine::Reference::Type::Boolean:
        if (!rep.toBoolean() || captures.empty())
            return {};
        return captures.front();
    case Engine::Reference::Type::String:
        return NumberedSubstitution(captures).apply(rep.toQString());
    case Engine::Reference::Type::Number: {
        auto index = rep.toInteger();
        if (!INTEGER::defined(index) || index <= 0 || index > captures.size())
            return {};
        --index;
        return captures[static_cast<int>(index)];
    }
    case Engine::Reference::Type::Table: {
        QString key;
        if (captures.size() > 1)
            key = captures[1];
        else if (captures.size() > 0)
            key = captures.front();
        return Engine::Table(rep).get(key).toQString();
    }
    default:
        break;
    }

    Engine::Call call(frame, rep);
    for (const auto &cap : captures) {
        call.push(cap);
    }
    if (!call.execute(1)) {
        call.clearError();
        return {};
    }
    return call.front().toQString();
}

void Regex::method_replace(Engine::Entry &entry)
{
    if (entry.size() < 3) {
        entry.throwError("Subject string and replacement required");
        return;
    }
    auto str = entry[1].toQString();
    auto rep = entry[2];

    int limit = -1;
    if (entry.size() > 3) {
        auto check = entry[3].toInteger();
        if (INTEGER::defined(check) && check > 0)
            limit = static_cast<int>(check);
    }

    QString result;
    int endOfLastMatch = 0;

    for (;;) {
        auto r = pattern.match(str, endOfLastMatch);
        if (!r.hasMatch())
            break;

        auto begin = r.capturedStart();
        auto end = r.capturedEnd();

        if (begin > endOfLastMatch) {
            result += str.mid(endOfLastMatch, (begin - endOfLastMatch));
        }

        result += applyReplacement(entry, rep, r.capturedTexts());

        endOfLastMatch = end;
        if (limit > 0) {
            --limit;
            if (!limit)
                break;
        }
    }

    if (endOfLastMatch < str.length()) {
        result += str.mid(endOfLastMatch);
    }

    entry.clear();
    entry.push(result);
    entry.propagate(1);
}

void Regex::method_split(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Subject string required");
        return;
    }
    auto str = entry[1].toQString();
    auto keepEmpty = true;

    if (entry.size() > 2) {
        keepEmpty = entry[2].toBoolean();
    }

    auto parts = str.split(pattern, keepEmpty ? QString::KeepEmptyParts : QString::SkipEmptyParts);
    entry.clear();
    for (const auto &p : parts) {
        entry.push(p);
    }
    entry.propagate();
}

void Regex::method_escape(Engine::Entry &entry)
{
    if (entry.size() > 1) {
        entry.push(QRegularExpression::escape(entry[1].toQString()));
    } else {
        entry.push(QRegularExpression::escape(pattern.pattern()));
    }
    entry.replaceWithBack(0);
    entry.propagate(1);
}

}
}
}