/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QDateTime>

#include "time.hxx"
#include "core/number.hxx"
#include "core/timeparse.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {
namespace Time {

double extractSingle(Engine::Frame &frame, const Engine::Reference &ref)
{
    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Data:
        return FP::undefined();
    case Engine::Reference::Type::Number:
        return ref.toReal();
    case Engine::Reference::Type::String: {
        try {
            return TimeParse::parseTime(ref.toQString());
        } catch (TimeParsingException &tpe) {
            frame.throwError(tpe.getDescription().toStdString());
        }
        return FP::undefined();
    }
    case Engine::Reference::Type::Table:
        break;
    }

    QStringList list;
    {
        Engine::Iterator it(frame, ref);
        while (it.next()) {
            list.push_back(it.value().toQString());
        }
    }

    try {
        return TimeParse::parseListSingle(list);
    } catch (TimeParsingException &tpe) {
        frame.throwError(tpe.getDescription().toStdString());
    }

    return FP::undefined();
}

CPD3::Time::Bounds extractBounds(Engine::Frame &frame, const Engine::Reference &ref)
{
    switch (ref.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Data:
    case Engine::Reference::Type::Number:
        return CPD3::Time::Bounds();
    case Engine::Reference::Type::String: {
        try {
            return TimeParse::parseListBounds(QStringList(ref.toQString()), true, true);
        } catch (TimeParsingException &tpe) {
            frame.throwError(tpe.getDescription().toStdString());
        }
        return {};
    }
    case Engine::Reference::Type::Table:
        break;
    }

    QStringList list;
    std::string haveTableStart;
    std::string haveTableEnd;
    {
        Engine::Iterator it(frame, ref);
        while (it.next()) {
            auto key = it.key();
            if (key.getType() == Engine::Reference::Type::String) {
                auto check = key.toString();
                if (Util::equal_insensitive(check, "start", "front")) {
                    haveTableStart = std::move(check);
                    continue;
                } else if (Util::equal_insensitive(check, "end", "back")) {
                    haveTableEnd = std::move(check);
                    continue;
                }
            }

            list.push_back(it.value().toQString());
        }
    }

    if (!haveTableStart.empty() && !haveTableEnd.empty()) {
        Engine::Frame local(frame);
        local.push(ref, haveTableStart);
        local.push(ref, haveTableEnd);
        return extractBounds(local, local[0], local[1]);
    }

    try {
        return TimeParse::parseListBounds(list, true, true);
    } catch (TimeParsingException &tpe) {
        frame.throwError(tpe.getDescription().toStdString());
    }

    return {};
}

CPD3::Time::Bounds extractBounds(Engine::Frame &frame,
                                 const Engine::Reference &start,
                                 const Engine::Reference &end)
{
    auto tfirst = start.getType();
    auto tsecond = end.getType();

    if (tfirst == Engine::Reference::Type::String && tsecond == Engine::Reference::Type::String) {
        try {
            return TimeParse::parseTimeBounds(start.toQString(), end.toQString(), true);
        } catch (TimeParsingException &tpe) {
            frame.throwError(tpe.getDescription().toStdString());
        }
        return CPD3::Time::Bounds();
    }

    class EndRef : public TimeReference {
        Engine::Frame &frame;
        const Engine::Reference &end;
    public:
        EndRef(Engine::Frame &frame, const Engine::Reference &end) : frame(frame), end(end)
        { }

        virtual ~EndRef() = default;

        double getReference() noexcept(false) override
        { return extractSingle(frame, end); }

        bool isLeading() noexcept(false) override
        { return true; }

        double getUndefinedValue() noexcept(false) override
        { return FP::undefined(); }
    };
    class StartRef : public TimeReference {
        Engine::Frame &frame;
        const Engine::Reference &start;
    public:
        StartRef(Engine::Frame &frame, const Engine::Reference &start) : frame(frame), start(start)
        { }

        virtual ~StartRef() = default;

        double getReference() noexcept(false) override
        { return extractSingle(frame, start); }

        bool isLeading() noexcept(false) override
        { return false; }

        double getUndefinedValue() noexcept(false) override
        { return FP::undefined(); }
    };

    CPD3::Time::Bounds result;

    switch (start.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Data:
        break;
    case Engine::Reference::Type::Number:
        result.start = start.toReal();
        break;
    case Engine::Reference::Type::String: {
        try {
            EndRef ref(frame, end);
            result.start = TimeParse::parseTime(start.toQString(), &ref);
        } catch (TimeParsingException &tpe) {
            frame.throwError(tpe.getDescription().toStdString());
            return CPD3::Time::Bounds();
        }
        break;
    }
    case Engine::Reference::Type::Table:
        QStringList list;
        {
            Engine::Iterator it(frame, start);
            while (it.next()) {
                list.push_back(it.value().toQString());
            }
        }

        try {
            EndRef ref(frame, end);
            TimeParseNOOPListHandler handler;
            result.end = TimeParse::parseListSingle(list, &ref, &handler, true);
        } catch (TimeParsingException &tpe) {
            frame.throwError(tpe.getDescription().toStdString());
        }
        break;
    }

    switch (end.getType()) {
    case Engine::Reference::Type::Nil:
    case Engine::Reference::Type::Boolean:
    case Engine::Reference::Type::Data:
        break;
    case Engine::Reference::Type::Number:
        result.end = end.toReal();
        break;
    case Engine::Reference::Type::String: {
        try {
            StartRef ref(frame, start);
            result.end = TimeParse::parseTime(end.toQString(), &ref);
        } catch (TimeParsingException &tpe) {
            frame.throwError(tpe.getDescription().toStdString());
            return CPD3::Time::Bounds();
        }
        break;
    }
    case Engine::Reference::Type::Table:
        QStringList list;
        {
            Engine::Iterator it(frame, start);
            while (it.next()) {
                list.push_back(it.value().toQString());
            }
        }

        try {
            StartRef ref(frame, start);
            TimeParseNOOPListHandler handler;
            result.end = TimeParse::parseListSingle(list, &ref, &handler, true);
        } catch (TimeParsingException &tpe) {
            frame.throwError(tpe.getDescription().toStdString());
        }
        break;
    }

    return result;
}

CPD3::Time::LogicalTimeUnit extractUnit(Engine::Frame &frame, const Engine::Reference &raw)
{
    auto unit = raw.toString();
    if (Util::equal_insensitive(unit, "millisecond", "milliseconds", "msec"))
        return CPD3::Time::Millisecond;
    if (Util::equal_insensitive(unit, "second", "seconds", "sec", "secs", "s"))
        return CPD3::Time::Second;
    if (Util::equal_insensitive(unit, "minute", "minutes", "min", "mins", "m", "mi"))
        return CPD3::Time::Minute;
    if (Util::equal_insensitive(unit, "hour", "hours", "h"))
        return CPD3::Time::Hour;
    if (Util::equal_insensitive(unit, "day", "days", "d"))
        return CPD3::Time::Day;
    if (Util::equal_insensitive(unit, "week", "weeks", "w"))
        return CPD3::Time::Week;
    if (Util::equal_insensitive(unit, "month", "months", "mon", "mons", "mo"))
        return CPD3::Time::Month;
    if (Util::equal_insensitive(unit, "quarter", "quarters", "qtr", "qtrs", "q"))
        return CPD3::Time::Quarter;
    if (Util::equal_insensitive(unit, "year", "years", "y"))
        return CPD3::Time::Year;
    return CPD3::Time::Second;
}

Engine::Reference pushStart(Engine::Frame &frame, const Engine::Reference &origin)
{
    frame.push(origin, "start");
    auto check = frame.back();
    if (!check.isNil())
        return check;

    frame.pop();
    frame.push(origin, "front");
    check = frame.back();
    if (!check.isNil())
        return check;

    frame.pop();
    frame.push(origin, "START");
    check = frame.back();
    if (!check.isNil())
        return check;

    frame.pop();
    frame.push(origin, "FRONT");
    check = frame.back();
    if (!check.isNil())
        return check;

    if (origin.getType() != Engine::Reference::Type::Table)
        return check;

    frame.pop();
    bool found = false;
    {
        Engine::Iterator it(frame, origin);
        while (it.next()) {
            auto key = it.key();
            if (key.getType() == Engine::Reference::Type::String) {
                if (Util::equal_insensitive(key.toString(), "start", "front")) {
                    it.replaceWithBack(0);
                    it.propagate(1);
                    found = true;
                    break;
                }
            }
        }
    }

    if (!found)
        frame.pushNil();
    return frame.back();
}

Engine::Reference pushEnd(Engine::Frame &frame, const Engine::Reference &origin)
{
    frame.push(origin, "end");
    auto check = frame.back();
    if (!check.isNil())
        return check;

    frame.pop();
    frame.push(origin, "back");
    check = frame.back();
    if (!check.isNil())
        return check;

    frame.pop();
    frame.push(origin, "END");
    check = frame.back();
    if (!check.isNil())
        return check;

    frame.pop();
    frame.push(origin, "BACK");
    check = frame.back();
    if (!check.isNil())
        return check;

    if (origin.getType() != Engine::Reference::Type::Table)
        return check;

    frame.pop();
    bool found = false;
    {
        Engine::Iterator it(frame, origin);
        while (it.next()) {
            auto key = it.key();
            if (key.getType() == Engine::Reference::Type::String) {
                if (Util::equal_insensitive(key.toString(), "end", "back")) {
                    it.replaceWithBack(0);
                    it.propagate(1);
                    found = true;
                    break;
                }
            }
        }
    }

    if (!found)
        frame.pushNil();
    return frame.back();
}

void install(Engine::Table &target, Engine::Frame &frame)
{
    Engine::Frame local(frame);
    auto table = local.pushTable();
    {
        Engine::Assign assign(local, table, "single");
        assign.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Invalid arguments");
                return;
            }
            double t = extractSingle(entry, entry[0]);
            entry.clear();
            entry.push(t);
            entry.propagate(1);
        }));
    }
    {
        Engine::Assign assign(local, table, "bounds");
        assign.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Invalid arguments");
                return;
            }
            if (entry.size() == 1) {
                auto b = extractBounds(entry, entry[0]);
                entry.clear();
                entry.push(b.start);
                entry.push(b.end);
                entry.propagate(2);
            } else {
                auto b = extractBounds(entry, entry[0], entry[1]);
                entry.clear();
                entry.push(b.start);
                entry.push(b.end);
                entry.propagate(2);
            }
        }));
    }
    {
        Engine::Assign assign(local, table, "now");
        assign.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            entry.clear();
            entry.push(CPD3::Time::time());
            entry.propagate(1);
        }));
    }
    {
        Engine::Assign assign(local, table, "split");
        assign.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Invalid arguments");
                return;
            }
            double t = extractSingle(entry, entry[0]);
            entry.clear();
            if (!FP::defined(t)) {
                entry.pushNil();
            } else {
                auto input = CPD3::Time::toDateTime(t, false);
                auto output = entry.pushTable();
                output.set("year", Engine::Output(input.date().year()));
                output.set("month", Engine::Output(input.date().month()));
                output.set("day", Engine::Output(input.date().day()));
                output.set("hour", Engine::Output(input.time().hour()));
                output.set("minute", Engine::Output(input.time().minute()));
                output.set("min", Engine::Output(input.time().minute()));
                auto s = Engine::Output(
                        static_cast<double>(input.time().second()) + (t - std::floor(t)));
                output.set("second", s);
                output.set("sec", s);
            }
            entry.propagate(1);
        }));
    }
    {
        Engine::Assign assign(local, table, "join");
        assign.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Invalid arguments");
                return;
            }
            if (entry[0].getType() != Engine::Reference::Type::Table) {
                double t = extractSingle(entry, entry[0]);
                entry.clear();
                entry.push(t);
                entry.propagate(1);
                return;
            }

            Engine::Table input(entry[0]);

            int year = 0;
            {
                auto i = input.get("year").toInteger();
                if (!INTEGER::defined(i) || i < 1970 || i > 2999) {
                    entry.throwError("Invalid year");
                    return;
                }
                year = static_cast<int>(i);
            }
            int month = 1;
            {
                auto i = input.get("month").toInteger();
                if (INTEGER::defined(i) && i >= 1 && i <= 12)
                    month = static_cast<int>(i);
            }
            int day = 1;
            {
                auto i = input.get("day").toInteger();
                if (INTEGER::defined(i) && i >= 1 && i <= 31)
                    day = static_cast<int>(i);
            }
            int hour = 0;
            {
                auto i = input.get("hour").toInteger();
                if (INTEGER::defined(i) && i >= 0 && i <= 23)
                    hour = static_cast<int>(i);
            }
            int minute = 0;
            {
                auto i = input.get("minute").toInteger();
                if (!INTEGER::defined(i))
                    i = input.get("min").toInteger();
                if (INTEGER::defined(i) && i >= 0 && i <= 59)
                    minute = static_cast<int>(i);
            }
            int second = 0;
            double fractional = 0;
            {
                auto f = input.get("second").toReal();
                if (!FP::defined(f))
                    f = input.get("sec").toReal();
                if (FP::defined(f) && f >= 0.0 && f <= 60.0) {
                    fractional = std::floor(f);
                    second = static_cast<int>(fractional);
                    fractional = f - fractional;
                }
            }

            entry.clear();
            entry.push(CPD3::Time::fromDateTime(
                    QDateTime(QDate(year, month, day), QTime(hour, minute, second), Qt::UTC)) +
                               fractional);
            entry.propagate(1);
        }));
    }
    {
        Engine::Assign assign(local, table, "doy");
        assign.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Invalid arguments");
                return;
            }
            double t = extractSingle(entry, entry[0]);
            entry.clear();
            if (!FP::defined(t)) {
                entry.pushNil();
                entry.pushNil();
            } else {
                QDateTime ft(CPD3::Time::toDateTime(t));
                int year = ft.date().year();
                double yearStart = CPD3::Time::fromDateTime(
                        QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
                double doy = (t - yearStart) / 86400.0 + 1.0;

                entry.push(doy);
                entry.push(year);
            }
            entry.propagate(2);
        }));
    }
    {
        Engine::Assign assign(local, table, "week");
        assign.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Invalid arguments");
                return;
            }
            double t = extractSingle(entry, entry[0]);
            entry.clear();
            if (!FP::defined(t)) {
                entry.pushNil();
                entry.pushNil();
                entry.pushNil();
            } else {
                int year = 0;
                int week = CPD3::Time::toDateTime(t).date().weekNumber(&year);
                double start = CPD3::Time::weekStart(year, week);

                entry.push(week);
                entry.push(year);
                entry.push(start);
            }
            entry.propagate(3);
        }));
    }
    target.set("Time", table);
}

}
}
}
}