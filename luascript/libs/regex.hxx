/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUASCRIPTLIBS_REGEX_HXX
#define CPD3LUASCRIPTLIBS_REGEX_HXX

#include "core/first.hxx"

#include <QRegularExpression>

#include "luascript/luascript.hxx"
#include "luascript/engine.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {


/**
 * A wrapper that creates an interface to a full regular expression engine, since the
 * built in one in Lua is fairly limited.
 */
class CPD3LUASCRIPT_EXPORT Regex : public Engine::Data {
    QRegularExpression pattern;

    void meta_tostring(Engine::Entry &entry);

    void meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues);

    void meta_newindex(Engine::Entry &entry);

    void meta_len(Engine::Entry &entry);

    void method_match(Engine::Entry &entry);

    void method_exact_match(Engine::Entry &entry);

    void method_global_match(Engine::Entry &entry);

    void method_replace(Engine::Entry &entry);

    void method_split(Engine::Entry &entry);

    void method_escape(Engine::Entry &entry);

public:
    Regex();

    explicit Regex(const QRegularExpression &pattern);

    explicit Regex(QRegularExpression &&pattern);

    virtual ~Regex();

    /**
     * Install the library on an engine target.
     *
     * @param target    the target table to put symbols in
     * @param frame     a frame to create in
     */
    static void install(Engine::Table &target, Engine::Frame &frame);

protected:
    void initialize(const Engine::Reference &self, Engine::Frame &frame) override;
};

}
}
}

#endif //CPD3LUASCRIPTLIBS_REGEX_HXX
