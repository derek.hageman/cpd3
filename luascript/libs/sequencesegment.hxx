/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUASCRIPTLIBS_SEQUENCESEGMENT_HXX
#define CPD3LUASCRIPTLIBS_SEQUENCESEGMENT_HXX

#include "core/first.hxx"

#include "luascript/luascript.hxx"
#include "luascript/engine.hxx"
#include "datacore/segment.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {

/**
 * A wrapper around a SequenceSegment CPD3 value.  The Lua API follows the C++ one, with
 * the exception that table-like access to the components (start/front and end/back)
 * is also permitted.  Any other table-like access is treated as a lookup in the segment
 */
class CPD3LUASCRIPT_EXPORT SequenceSegment : public Engine::Data {
    CPD3::Data::SequenceSegment seg;

    void meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues);

    void meta_newindex(Engine::Entry &entry);

    void method_set(Engine::Entry &entry);

    void method_get(Engine::Entry &entry);

    void method_exists(Engine::Entry &entry);

    void method_getNames(Engine::Entry &entry);

    void method_find(Engine::Entry &entry);

    void meta_tostring(Engine::Entry &entry);

public:
    SequenceSegment();

    virtual ~SequenceSegment();

    explicit SequenceSegment(const CPD3::Data::SequenceSegment &segment);

    explicit SequenceSegment(CPD3::Data::SequenceSegment &&segment);

    /**
     * Install the library on an engine target.
     *
     * @param target    the target table to put symbols in
     * @param frame     a frame to create in
     */
    static void install(Engine::Table &target, Engine::Frame &frame);

    /**
     * Extract a value from Lua code.  This detaches the value from
     * the script references.
     *
     * @param frame     the frame to work in
     * @param ref       the reference to read from
     * @return          the extracted value
     */
    static CPD3::Data::SequenceSegment extract(Engine::Frame &frame, const Engine::Reference &ref);

    /**
     * Get the segment contents.
     *
     * @return
     */
    inline const CPD3::Data::SequenceSegment &get() const
    { return seg; }

    inline CPD3::Data::SequenceSegment &get()
    { return seg; }

protected:
    void initialize(const Engine::Reference &self, Engine::Frame &frame) override;
};

}
}
}

#endif //CPD3LUASCRIPTLIBS_SEQUENCESEGMENT_HXX
