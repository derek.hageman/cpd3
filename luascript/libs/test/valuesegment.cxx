/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "luascript/engine.hxx"
#include "luascript/libs/valuesegment.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestLuaValueSegment : public QObject {
Q_OBJECT

private slots:

    void memberAccess()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(

vglobal.front = vglobal.front + 2;
vglobal.back = vglobal.back + 3;

local ref = vglobal.value;
ref.a = 2.0;
vglobal.value = ref;
vglobal.value:getPath('b'):setReal(vglobal.value.b + 1.0);
vglobal.c = vglobal.a + 3.0;

local v1 = CPD3.ValueSegment(300, 400, 42.0);

return v1;
)EOF", false);
        QVERIFY(ok);

        {
            auto g = c.global();
            Engine::Assign a(c, g, "vglobal");

            CPD3::Data::Variant::Root r;
            r["b"] = 3.0;
            a.pushData<Libs::ValueSegment>(CPD3::Data::ValueSegment(100.0, 200.0, std::move(r)));
        }

        QVERIFY(c.executeVariable());
        QCOMPARE((int) c.size(), 1);

        auto ex = Libs::ValueSegment::extract(c, c.front());
        QCOMPARE(ex.getStart(), 300.0);
        QCOMPARE(ex.getEnd(), 400.0);
        {
            CPD3::Data::Variant::Root r(42.0);
            QCOMPARE(ex.root().read(), r.read());
        }

        c.push(c.global(), "vglobal");
        ex = Libs::ValueSegment::extract(c, c.back());
        QCOMPARE(ex.getStart(), 102.0);
        QCOMPARE(ex.getEnd(), 203.0);
        {
            CPD3::Data::Variant::Root r;
            r["a"] = 2.0;
            r["b"] = 4.0;
            r["c"] = 5.0;
            QCOMPARE(ex.root().read(), r.read());
        }
    }

    void methods()
    {
        Engine e;

        {
            auto g = e.global();
            Engine::Frame f(e);
            {
                Engine::Assign a(f, g, "v1");
                a.pushData<Libs::ValueSegment>(
                        CPD3::Data::ValueSegment(100.0, 200.0, CPD3::Data::Variant::Root(2.0)));
            }
        }

        {
            Engine::Call c(e);
            c.pushChunk("v1:set(3.0); return v1.value:toReal();", false);
            QVERIFY(c.execute(1));
            QCOMPARE(c.front().toReal(), 3.0);
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "local prior = v1.value; v1:detach(); prior:setReal(99); return v1.value:toReal(), prior:toReal();",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toReal(), 3.0);
            QCOMPARE(c.back().toReal(), 99.0);
        }
        {
            Engine::Call c(e);
            c.pushChunk("return tostring(v1)", false);
            QVERIFY(c.execute(1));
            QVERIFY(!c.front().toString().empty());
        }
    }

};

QTEST_APPLESS_MAIN(TestLuaValueSegment)

#include "valuesegment.moc"
