/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "luascript/engine.hxx"
#include "luascript/libs/sequencesegment.hxx"
#include "luascript/libs/sequencename.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestLuaSequenceSegment : public QObject {
Q_OBJECT

private slots:

    void memberAccess()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(

vglobal.front = vglobal.front + 2;
vglobal.back = vglobal.back + 3;

vglobal.BaB_A11 = 98.0;
vglobal.BsG_S11 = vglobal.BsG_S11 + 4.0;
vglobal.BsB_S11 = 99.0;

local v1 = CPD3.SequenceSegment(300, 400, {
    [CPD3.SequenceName("alt", "clean", "BsR_S11")] = 42.0,
    ['alt:raw:Q_Q11'] = 43.0
});

return v1;
)EOF", false);
        QVERIFY(ok);

        {
            auto g = c.global();
            Engine::Assign a(c, g, "vglobal");

            CPD3::Data::SequenceSegment seg;
            seg.setStart(100.0);
            seg.setEnd(200.0);
            seg.setValue({"bnd", "raw", "BsG_S11"}, CPD3::Data::Variant::Root(2.0));
            a.pushData<Libs::SequenceSegment>(std::move(seg));
        }

        QVERIFY(c.executeVariable());
        QCOMPARE((int) c.size(), 1);

        auto ex = Libs::SequenceSegment::extract(c, c.front());
        QCOMPARE(ex.getStart(), 300.0);
        QCOMPARE(ex.getEnd(), 400.0);
        QCOMPARE(ex.value({"alt", "clean", "BsR_S11"}).toReal(), 42.0);
        QCOMPARE(ex.value({"alt", "raw", "Q_Q11"}).toReal(), 43.0);

        c.push(c.global(), "vglobal");
        ex = Libs::SequenceSegment::extract(c, c.back());
        QCOMPARE(ex.getStart(), 102.0);
        QCOMPARE(ex.getEnd(), 203.0);
        QCOMPARE(ex.value({"bnd", "raw", "BsG_S11"}).toReal(), 6.0);
        QCOMPARE(ex.value({"bnd", "raw", "BsB_S11"}).toReal(), 99.0);
        QCOMPARE(ex.value({"bnd", "raw", "BaB_A11"}).toReal(), 98.0);
    }

    void methods()
    {
        Engine e;

        {
            auto g = e.global();
            Engine::Frame f(e);
            {
                Engine::Assign a(f, g, "v1");
                CPD3::Data::SequenceSegment seg;
                seg.setStart(100.0);
                seg.setEnd(200.0);
                seg.setValue({"bnd", "raw", "BsG_S11"}, CPD3::Data::Variant::Root(2.0));
                a.pushData<Libs::SequenceSegment>(std::move(seg));
            }
        }

        {
            Engine::Call c(e);
            c.pushChunk("v1:set(3.0, 'bnd', 'raw', 'BsG_S11'); v1:set(4.0, 'BsB_S11');", false);
            QVERIFY(c.execute());

            c.push(c.global(), "v1");
            auto ex = Libs::SequenceSegment::extract(c, c.front());
            QCOMPARE(ex.value({"bnd", "raw", "BsG_S11"}).toReal(), 3.0);
            QCOMPARE(ex.value({"bnd", "raw", "BsB_S11"}).toReal(), 4.0);
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return v1:get('bnd', 'raw', 'BsG_S11'):toReal(), v1:get('BsB_S11'):toReal();",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toReal(), 3.0);
            QCOMPARE(c.back().toReal(), 4.0);
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1:exists('bnd', 'raw', 'BsG_S11'), v1:exists('BsR_S11');", false);
            QVERIFY(c.execute(2));
            QVERIFY(c.front().toBoolean());
            QVERIFY(!c.back().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1:getNames();", false);
            QVERIFY(c.execute(1));
            QCOMPARE((int) Engine::Table(c.front()).length(), 2);
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1:find('raw', 'BsG_S11'), v1:find('Bs[BGR]_S11');", false);
            QVERIFY(c.execute(2));
            auto check = c.front().toData<Libs::SequenceName>();
            QVERIFY(check);
            QCOMPARE(check->get(), CPD3::Data::SequenceName("bnd", "raw", "BsG_S11"));
            QVERIFY(c.back().isNil());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return tostring(v1)", false);
            QVERIFY(c.execute(1));
            QVERIFY(!c.front().toString().empty());
        }
        {
            Engine::Call c(e);
            c.pushChunk("v1:set(5.0, CPD3.SequenceName('alt', 'raw', 'BsG_S11', 'pm1'));", false);
            QVERIFY(c.execute());

            c.push(c.global(), "v1");
            auto ex = Libs::SequenceSegment::extract(c, c.front());
            QCOMPARE(ex.value({"bnd", "raw", "BsG_S11"}).toReal(), 3.0);
            QCOMPARE(ex.value({"bnd", "raw", "BsB_S11"}).toReal(), 4.0);
            QCOMPARE(ex.value({"alt", "raw", "BsG_S11", {"pm1"}}).toReal(), 5.0);
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1:find('BsB_S11'), v1:find('BsG_S11');", false);
            QVERIFY(c.execute(2));
            auto check = c.front().toData<Libs::SequenceName>();
            QVERIFY(check);
            QCOMPARE(check->get(), CPD3::Data::SequenceName("bnd", "raw", "BsB_S11"));
            QVERIFY(c.back().isNil());
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return v1:find('bn[d]', nil, 'BsG_S11'), v1:find(nil, nil, 'BsG_S11', 'pm1');",
                    false);
            QVERIFY(c.execute(2));
            auto check = c.front().toData<Libs::SequenceName>();
            QVERIFY(check);
            QCOMPARE(check->get(), CPD3::Data::SequenceName("bnd", "raw", "BsG_S11"));
            check = c.back().toData<Libs::SequenceName>();
            QVERIFY(check);
            QCOMPARE(check->get(), CPD3::Data::SequenceName("alt", "raw", "BsG_S11", {"pm1"}));
        }
    }

};

QTEST_APPLESS_MAIN(TestLuaSequenceSegment)

#include "sequencesegment.moc"
