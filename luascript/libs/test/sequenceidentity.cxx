/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "luascript/engine.hxx"
#include "luascript/libs/sequenceidentity.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestLuaSequenceIdentity : public QObject {
Q_OBJECT

private slots:

    void memberAccess()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(

vglobal.front = vglobal.front + 2;
vglobal.back = vglobal.back + 3;
vglobal.priority = vglobal.priority + 4;

vglobal.station = vglobal.station .. "ONE";
vglobal.archive = "one" .. vglobal.archive;
vglobal.variable = vglobal.variable .. "ONE";
local f = vglobal.flavors;
f['added'] = true;
vglobal.flavors = f;

local name = vglobal.name;
name.station = name.station .. "two";
vglobal.station = vglobal.station .. "three";
vglobal.name = name;
vglobal.archive = vglobal.archive .. "three";
vglobal.variable = vglobal.variable .. "three";
local f = vglobal.flavors;
f['another'] = true;
vglobal.flavors = f;

local v1 = CPD3.SequenceIdentity(CPD3.SequenceName('bnd', 'raw', 'BsG_S11'), 300, 400, 99);

return v1;
)EOF", false);
        QVERIFY(ok);

        {
            auto g = c.global();
            Engine::Assign a(c, g, "vglobal");
            a.pushData<Libs::SequenceIdentity>(
                    CPD3::Data::SequenceIdentity("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3));
        }

        QVERIFY(c.executeVariable());
        QCOMPARE((int) c.size(), 1);

        QCOMPARE(Libs::SequenceIdentity::extract(c, c.front()),
                 CPD3::Data::SequenceIdentity("bnd", "raw", "BsG_S11", 300.0, 400.0, 99));

        c.push(c.global(), "vglobal");
        Libs::SequenceIdentity::extract(c, c.back());
        QCOMPARE(Libs::SequenceIdentity::extract(c, c.back()),
                 CPD3::Data::SequenceIdentity("brwonetwothree", "onerawthree", "testONEthree",
                                              {"f1", "added", "another"}, 102.0, 203.0, 7));
    }

    void comparison()
    {
        Engine e;

        {
            auto g = e.global();
            Engine::Frame f(e);
            {
                Engine::Assign a(f, g, "v1");
                a.pushData<Libs::SequenceIdentity>(
                        CPD3::Data::SequenceIdentity(CPD3::Data::SequenceName("a", "b", "v1"),
                                                     100.0, 200.0));
            }
            {
                Engine::Assign a(f, g, "v1alt");
                a.pushData<Libs::SequenceIdentity>(
                        CPD3::Data::SequenceIdentity(CPD3::Data::SequenceName("a", "b", "v1"),
                                                     100.0, 200.0));
            }
            {
                Engine::Assign a(f, g, "v2");
                a.pushData<Libs::SequenceIdentity>(
                        CPD3::Data::SequenceIdentity(CPD3::Data::SequenceName("a", "b", "v2"),
                                                     101.0, 201.0, 1));
            }
        }

        {
            Engine::Call c(e);
            c.pushChunk("return v1 == v2", false);
            QVERIFY(c.execute(1));
            QVERIFY(!c.front().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1 == v1alt", false);
            QVERIFY(c.execute(1));
            QVERIFY(c.front().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1 == v1", false);
            QVERIFY(c.execute(1));
            QVERIFY(c.front().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1 ~= v2", false);
            QVERIFY(c.execute(1));
            QVERIFY(c.front().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1 ~= v1alt", false);
            QVERIFY(c.execute(1));
            QVERIFY(!c.front().toBoolean());
        }
    }

    void methods()
    {
        Engine e;

        {
            auto g = e.global();
            Engine::Frame f(e);
            {
                Engine::Assign a(f, g, "v1");
                a.pushData<Libs::SequenceIdentity>(
                        CPD3::Data::SequenceIdentity(CPD3::Data::SequenceName("a", "b", "c"), 100.0,
                                                     200.0, 1));
            }
            {
                Engine::Assign a(f, g, "v2");
                a.pushData<Libs::SequenceIdentity>(
                        CPD3::Data::SequenceIdentity(CPD3::Data::SequenceName("d", "e", "f"), 101.0,
                                                     202.0));
            }
        }

        {
            Engine::Call c(e);
            c.pushChunk("return v1:overlayLessThan(v2), v2:overlayLessThan(v1)", false);
            QVERIFY(c.execute(2));
            QVERIFY(!c.front().toBoolean());
            QVERIFY(c.back().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1:timeLessThan(v2), v2:timeLessThan(v1)", false);
            QVERIFY(c.execute(2));
            QVERIFY(c.front().toBoolean());
            QVERIFY(!c.back().toBoolean());
        }
    }

    void assignFailure()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(

local v1 = CPD3.SequenceIdentity({
    FRONT = 100.0,
    BACK = 200.0,
});

return v1;
)EOF", false);
        QVERIFY(ok);

        QVERIFY(c.executeVariable());
        QCOMPARE((int) c.size(), 1);

        QCOMPARE(Libs::SequenceIdentity::extract(c, c.front()),
                 CPD3::Data::SequenceIdentity({}, {}, {}, 100.0, 200.0, 0));
    }
};

QTEST_APPLESS_MAIN(TestLuaSequenceIdentity)

#include "sequenceidentity.moc"
