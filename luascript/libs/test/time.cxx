/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "luascript/engine.hxx"
#include "luascript/libs/time.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestLuaTime : public QObject {
Q_OBJECT

private slots:

    void basic()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(
local single = CPD3.Time.single("2018-05-01");
local START, END = CPD3.Time.bounds("2018-03-01", "2d");
local doy, doyyear = CPD3.Time.doy(1357797600);
local week, weekyear, weekstart = CPD3.Time.week("2014-02-03T00:00:00Z");
return single, START, END, CPD3.Time.now(), doy, doyyear, week, weekyear, weekstart;
)EOF", false);
        QVERIFY(ok);

        QVERIFY(c.execute(9));
        QCOMPARE(c[0].toNumber(), 1525132800.0);
        QCOMPARE(c[1].toNumber(), 1519862400.0);
        QCOMPARE(c[2].toNumber(), 1520035200.0);
        QVERIFY(c[3].toNumber() > 0.0);
        QCOMPARE(c[4].toNumber(), 10.25);
        QCOMPARE(c[5].toNumber(), 2013.0);
        QCOMPARE(c[6].toNumber(), 6.0);
        QCOMPARE(c[7].toNumber(), 2014.0);
        QCOMPARE(c[8].toNumber(), 1391385600.0);
    }

    void breakdown()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(

local s = CPD3.Time.split("2018-05-01T10:11:12.250Z");

local j = CPD3.Time.join({
    year = 2015,
    month = 5,
    day = 4,
    hour = 3,
    minute = 2,
    second = 1.5,
});

return s, j;
)EOF", false);
        QVERIFY(ok);

        QVERIFY(c.execute(2));
        QCOMPARE(c[1].toReal(), 1430708521.5);
        Engine::Table t(c[0]);
        QCOMPARE(t.get("year").toReal(), 2018.0);
        QCOMPARE(t.get("month").toReal(), 5.0);
        QCOMPARE(t.get("day").toReal(), 1.0);
        QCOMPARE(t.get("hour").toReal(), 10.0);
        QCOMPARE(t.get("minute").toReal(), 11.0);
        QCOMPARE(t.get("min").toReal(), 11.0);
        QCOMPARE(t.get("second").toReal(), 12.25);
        QCOMPARE(t.get("sec").toReal(), 12.25);
    }

};

QTEST_APPLESS_MAIN(TestLuaTime)

#include "time.moc"
