/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QRegularExpression>
#include <QString>

#include "luascript/engine.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestLuaRegex : public QObject {
Q_OBJECT

private slots:

    void basic()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(
re = CPD3.Regex("(1234)");
local s = tostring(re);

re.icase = true;
if not re.ignore_case then error(); end
if re.pattern ~= "(1234)" then error(); end
if #re ~= 1 then error(); end
if not re:find("12345") then error(); end

return re:escape(), re:escape("asd");
)EOF", false);
        QVERIFY(ok);

        QVERIFY(c.execute(2));
        QCOMPARE(c[0].toQString(), QRegularExpression::escape("(1234)"));
        QCOMPARE(c[1].toQString(), QString("asd"));
    }

    void find()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(
re = CPD3.Regex("a(bc)", { icase=true });

s, e, c1 = re:find("ABcd");
if s ~= 1 then error(); end
if e ~= 3 then error(); end
if c1 ~= "Bc" then error(); end

c0, c1 = re:match("abcd", 1, true);
if c0 ~= "abc" then error(); end
if c1 ~= "bc" then error(); end

if re:match("abcd", 2) then error(); end
if not re:match("aaaabcd", 2) then error(); end

)EOF", false);
        QVERIFY(ok);

        QVERIFY(c.execute());
    }

    void exactMatch()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(
re = CPD3.Regex("a(bc)");

c0, c1 = re:exact_match("abc");
if c0 ~= "abc" then error(); end
if c1 ~= "bc" then error(); end

if re:exactMatch("abcd") then error(); end
if re:exactMatch("1abc") then error(); end
if re:exactMatch("1abc2") then error(); end
if re:exact("ABC") then error(); end

re.icase = true;
if not re:exact("abC") then error(); end

)EOF", false);
        QVERIFY(ok);

        QVERIFY(c.execute());
    }

    void globalMatch()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(
re = CPD3.Regex("a(bc)", { "icase" });

it = re:gmatch("abc abCd", false);

s, e, c1 = it();
if s ~= 1 then error(); end
if e ~= 3 then error(); end
if c1 ~= "bc" then error(); end

s, e, c1 = it();
if s ~= 5 then error(); end
if e ~= 7 then error(); end
if c1 ~= "bC" then error(); end

if it() then error() end


it = re:global_match("ABc aBc");

c0, c1 = it();
if c0 ~= "ABc" then error(); end
if c1 ~= "Bc" then error(); end

c0, c1 = it();
if c0 ~= "aBc" then error(); end
if c1 ~= "Bc" then error(); end

if it() then error() end


it = re:globalMatch("zzzz");
if it() then error() end

)EOF", false);
        QVERIFY(ok);

        QVERIFY(c.execute());
    }

    void replace()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(
re = CPD3.Regex('a(bc)');

return re:replace('abcdef', 'z'), re:gsub('zzabc', function(c0, c1) return c1 end),
       re:replace('zzzzz', 'q'), re:gsub('abc', '${1}'),
       re:replace('111abc222', { bc = '9999', q = '42' }),
       re:replace('', '123');

)EOF", false);
        QVERIFY(ok);

        QVERIFY(c.execute(6));
        QCOMPARE(c[0].toQString(), QString("zdef"));
        QCOMPARE(c[1].toQString(), QString("zzbc"));
        QCOMPARE(c[2].toQString(), QString("zzzzz"));
        QCOMPARE(c[3].toQString(), QString("bc"));
        QCOMPARE(c[4].toQString(), QString("1119999222"));
        QCOMPARE(c[5].toQString(), QString(""));
    }

    void split()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(
re = CPD3.Regex("[ \t]+");

return re:split("a bbb\t c   d1234");

)EOF", false);
        QVERIFY(ok);

        QVERIFY(c.executeVariable());
        QCOMPARE((int) c.size(), 4);
        QCOMPARE(c[0].toQString(), QString("a"));
        QCOMPARE(c[1].toQString(), QString("bbb"));
        QCOMPARE(c[2].toQString(), QString("c"));
        QCOMPARE(c[3].toQString(), QString("d1234"));
    }
};

QTEST_APPLESS_MAIN(TestLuaRegex)

#include "regex.moc"
