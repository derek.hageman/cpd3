/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "luascript/engine.hxx"
#include "luascript/libs/sequencename.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestLuaSequenceName : public QObject {
Q_OBJECT

private slots:

    void memberAccess()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(

vglobal.station = vglobal.station .. "ADD";
vglobal.archive = "add" .. vglobal.archive;
vglobal.variable = vglobal.variable .. "ADD";
f = vglobal.flavors;
f['added'] = true;
vglobal.flavors = f;

local v1 = CPD3.SequenceName('bnd', 'raw', 'BsG_S11');
local v2 = CPD3.SequenceName('alt', 'clean', 'BsB_S11', 'pm1');

return v1, v2;
)EOF", false);
        QVERIFY(ok);

        {
            auto g = c.global();
            Engine::Assign a(c, g, "vglobal");
            a.pushData<Libs::SequenceName>(CPD3::Data::SequenceName("brw", "raw", "test", {"f1"}));
        }

        QVERIFY(c.executeVariable());
        QCOMPARE((int) c.size(), 2);

        QCOMPARE(Libs::SequenceName::extract(c, c.front()),
                 CPD3::Data::SequenceName("bnd", "raw", "BsG_S11"));
        QCOMPARE(Libs::SequenceName::extract(c, c.back()),
                 CPD3::Data::SequenceName("alt", "clean", "BsB_S11", {"pm1"}));

        c.push(c.global(), "vglobal");
        Libs::SequenceName::extract(c, c.back());
        QCOMPARE(Libs::SequenceName::extract(c, c.back()),
                 CPD3::Data::SequenceName("brwadd", "addraw", "testADD", {"f1", "added"}));
    }

    void comparison()
    {
        Engine e;

        {
            auto g = e.global();
            Engine::Frame f(e);
            {
                Engine::Assign a(f, g, "v1");
                a.pushData<Libs::SequenceName>(CPD3::Data::SequenceName("a", "b", "v1"));
            }
            {
                Engine::Assign a(f, g, "v1alt");
                a.pushData<Libs::SequenceName>(CPD3::Data::SequenceName("a", "b", "v1"));
            }
            {
                Engine::Assign a(f, g, "v2");
                a.pushData<Libs::SequenceName>(CPD3::Data::SequenceName("a", "b", "v2"));
            }
        }

        {
            Engine::Call c(e);
            c.pushChunk("return v1 == v2", false);
            QVERIFY(c.execute(1));
            QVERIFY(!c.front().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1 == v1alt", false);
            QVERIFY(c.execute(1));
            QVERIFY(c.front().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1 < v2", false);
            QVERIFY(c.execute(1));
            QVERIFY(c.front().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1 < v1alt", false);
            QVERIFY(c.execute(1));
            QVERIFY(!c.front().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1 <= v2", false);
            QVERIFY(c.execute(1));
            QVERIFY(c.front().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1 <= v1alt", false);
            QVERIFY(c.execute(1));
            QVERIFY(c.front().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v2 <= v1", false);
            QVERIFY(c.execute(1));
            QVERIFY(!c.front().toBoolean());
        }

        {
            Engine::Call c(e);
            c.pushChunk("return v1:displayLessThan(v2)", false);
            QVERIFY(c.execute(1));
            QVERIFY(c.front().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1:displayLessThan(v1)", false);
            QVERIFY(c.execute(1));
            QVERIFY(!c.front().toBoolean());
        }
    }

    void methods()
    {
        Engine e;

        Libs::SequenceName *v1;
        Libs::SequenceName *v2;
        {
            auto g = e.global();
            Engine::Frame f(e);
            {
                Engine::Assign a(f, g, "v1");
                v1 = a.pushData<Libs::SequenceName>(CPD3::Data::SequenceName("a", "b_meta", "v1"));
            }
            {
                Engine::Assign a(f, g, "v2");
                v2 = a.pushData<Libs::SequenceName>(
                        CPD3::Data::SequenceName("_", "b", "v2", {"pm1"}));
            }
        }

        {
            Engine::Call c(e);
            c.pushChunk("return v1:isDefaultStation(), v2:isDefaultStation()", false);
            QVERIFY(c.execute(2));
            QVERIFY(!c.front().toBoolean());
            QVERIFY(c.back().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1:isMeta(), v2:isMeta()", false);
            QVERIFY(c.execute(2));
            QVERIFY(c.front().toBoolean());
            QVERIFY(!c.back().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1:hasFlavor('pm1'), v2:hasFlavor('pm1')", false);
            QVERIFY(c.execute(2));
            QVERIFY(!c.front().toBoolean());
            QVERIFY(c.back().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1:hasFlavor('pm1', 'pm10'), v2:hasFlavor('pm1', 'pm10')", false);
            QVERIFY(c.execute(2));
            QVERIFY(!c.front().toBoolean());
            QVERIFY(!c.back().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1:hasAnyFlavor('pm1', 'pm10'), v2:hasAnyFlavor('pm1', 'pm10')",
                        false);
            QVERIFY(c.execute(2));
            QVERIFY(!c.front().toBoolean());
            QVERIFY(c.back().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1:lacksFlavor('pm1'), v2:lacksFlavor('pm1')", false);
            QVERIFY(c.execute(2));
            QVERIFY(c.front().toBoolean());
            QVERIFY(!c.back().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk("return tostring(v1), tostring(v2)", false);
            QVERIFY(c.execute(2));
            QVERIFY(!c.front().toString().empty());
            QVERIFY(!c.back().toString().empty());
            QVERIFY(c.front().toString() != c.back().toString());
        }

        {
            Engine::Call c(e);
            c.pushChunk("return v1:fromMeta(), v2:toMeta()", false);
            QVERIFY(c.execute(2));
            {
                auto check = c.front().toData<Libs::SequenceName>();
                QVERIFY(check);
                QCOMPARE(check->get(), v1->get().fromMeta());
            }
            {
                auto check = c.back().toData<Libs::SequenceName>();
                QVERIFY(check);
                QCOMPARE(check->get(), v2->get().toMeta());
            }
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1:withFlavor('pm10'), v2:withoutFlavor('pm1')", false);
            QVERIFY(c.execute(2));
            {
                auto check = c.front().toData<Libs::SequenceName>();
                QVERIFY(check);
                QCOMPARE(check->get(), v1->get().withFlavor("pm10"));
            }
            {
                auto check = c.back().toData<Libs::SequenceName>();
                QVERIFY(check);
                QCOMPARE(check->get(), v2->get().withoutFlavor("pm1"));
            }
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1:withoutAllFlavors(), v2:withoutAllFlavors()", false);
            QVERIFY(c.execute(2));
            {
                auto check = c.front().toData<Libs::SequenceName>();
                QVERIFY(check);
                QCOMPARE(check->get(), v1->get());
            }
            {
                auto check = c.back().toData<Libs::SequenceName>();
                QVERIFY(check);
                QCOMPARE(check->get(), v2->get().withoutAllFlavors());
            }
        }


        {
            QVERIFY(!v1->get().hasFlavor("pm10"));
            Engine::Call c(e);
            c.pushChunk("v1:addFlavor('pm10')", false);
            QVERIFY(c.execute());
            QVERIFY(v1->get().hasFlavor("pm10"));
        }
        {
            QVERIFY(v1->get().hasFlavor("pm10"));
            Engine::Call c(e);
            c.pushChunk("v1:removeFlavor('pm10')", false);
            QVERIFY(c.execute());
            QVERIFY(!v1->get().hasFlavor("pm10"));
        }
        {
            QVERIFY(v2->get().hasFlavor("pm1"));
            Engine::Call c(e);
            c.pushChunk("v2:clearFlavors()", false);
            QVERIFY(c.execute());
            QVERIFY(!v2->get().hasFlavor("pm1"));
        }
        {
            QVERIFY(!v1->get().isDefaultStation());
            Engine::Call c(e);
            c.pushChunk("v1:setDefaultStation()", false);
            QVERIFY(c.execute());
            QVERIFY(v1->get().isDefaultStation());
        }
        {
            QVERIFY(!v2->get().isMeta());
            Engine::Call c(e);
            c.pushChunk("v2:setMeta()", false);
            QVERIFY(c.execute());
            QVERIFY(v2->get().isMeta());
        }
        {
            QVERIFY(v1->get().isMeta());
            Engine::Call c(e);
            c.pushChunk("v1:clearMeta()", false);
            QVERIFY(c.execute());
            QVERIFY(!v1->get().isMeta());
        }
    }
};

QTEST_APPLESS_MAIN(TestLuaSequenceName)

#include "sequencename.moc"
