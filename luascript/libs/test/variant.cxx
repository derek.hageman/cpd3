/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "luascript/engine.hxx"
#include "luascript/libs/variant.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestLuaVariant : public QObject {
Q_OBJECT

private slots:

    void basic()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(

vglobal.key_add = "added";

rglobal1 = vglobal:getPath("key_integer") + 1;
rglobal2 = 4 * vglobal:getPath("key_integer");
rglobal3 = vglobal:getPath("key_integer") + vglobal:getPath("key_integer");
rglobal4 = vglobal.key_integer > 0;

local v1 = CPD3.Variant({
    key1 = "value",
    ["key2"] = vglobal.key_integer,
    ["key3"] = vglobal:getPath("key_add"),
    key4 = vglobal.key_flags
});

return v1;
)EOF", false);
        QVERIFY(ok);


        auto vglobal = Data::Variant::Write::empty();
        vglobal.hash("key_integer").setInteger(1);
        vglobal.hash("key_flags").setFlags({"flag1"});

        {
            auto g = c.global();
            Engine::Assign a(c, g, "vglobal");
            a.pushData<Libs::Variant>(vglobal);
        }

        QVERIFY(c.executeVariable());
        QCOMPARE((int) c.size(), 1);

        auto v1 = Libs::Variant::extract(c, c.front());
        {
            auto check = Data::Variant::Write::empty();
            check["key1"].setString("value");
            check["key2"].setReal(1);
            check["key3"].setString("added");
            check["key4"].setFlags({"flag1"});
            QCOMPARE(v1, check);
        }

        QCOMPARE(c.global().get("rglobal1").toNumber(), 2.0);
        QCOMPARE(c.global().get("rglobal2").toNumber(), 4.0);
        QCOMPARE(c.global().get("rglobal3").toNumber(), 2.0);
        QVERIFY(c.global().get("rglobal4").toBoolean());
    }

    void readAccess()
    {
        auto vglobal = Data::Variant::Write::empty();
        vglobal["empty"].setEmpty();
        vglobal["real"].setReal(2.5);
        vglobal["integer"].setInteger(3);
        vglobal["boolean"].setBoolean(true);
        vglobal["overlay"].setOverlay("ovl");
        vglobal["str"].setString("s1");
        vglobal["str"].setDisplayString("es", "s2");
        vglobal["bytes"].setBytes(CPD3::Data::Variant::Bytes("abcd"));
        vglobal["flags"].setFlags({"f1"});
        vglobal["hash/key"].setString("hash_key");
        vglobal["array/#1"].setString("array_key");
        vglobal["matrix/[1,2]"].setString("matrix_key");
        vglobal["keyframe/@1.25"].setString("keyframe_key");
        vglobal["metareal/*rkey"].setString("metareal_key");
        vglobal["metaint/*ikey"].setString("metaint_key");
        vglobal["metabool/*bkey"].setString("metabool_key");
        vglobal["metastring/*skey"].setString("metastring_key");
        vglobal["metabytes/*nkey"].setString("metabytes_key");
        vglobal["metaarray/*akey"].setString("metaarray_key");
        vglobal["metamatrix/*tkey"].setString("metamatrix_key");
        vglobal["metakeyframe/*ekey"].setString("metakeyframe_key");
        vglobal["metahash/*hkey"].setString("metahash_key");
        vglobal["metahash/*cchild"].setString("metahash_child");
        vglobal["metaflags/*fkey"].setString("metaflags_key");
        vglobal["metaflags/*lsingle"].setString("metaflags_single");

        Engine e;
        {
            auto g = e.global();
            Engine::Frame f(e);
            Engine::Assign a(f, g, "vglobal");
            a.pushData<Libs::Variant>(vglobal);
        }

        {
            Engine::Call c(e);
            c.pushChunk("return vglobal:getPath('empty'):getType()", false);
            QVERIFY(c.execute(1));
            QCOMPARE(c.front().toString(), std::string("Empty"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('real'):getType(), vglobal:getPath('real'):toReal()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("Real"));
            QCOMPARE(c.back().toNumber(), 2.5);
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('integer'):getType(), vglobal:getPath('integer'):toInteger()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("Integer"));
            QCOMPARE(c.back().toNumber(), 3.0);
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('boolean'):getType(), vglobal:getPath('boolean'):toBoolean()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("Boolean"));
            QVERIFY(c.back().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('overlay'):getType(), vglobal:getPath('overlay'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("Overlay"));
            QCOMPARE(c.back().toString(), std::string("ovl"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('str'):getType(), vglobal:getPath('str'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("String"));
            QCOMPARE(c.back().toString(), std::string("s1"));
        }
        {
            Engine::Call c(e);
            c.pushChunk("return vglobal:getPath('str'):toDisplayString('es')", false);
            QVERIFY(c.execute(1));
            QCOMPARE(c.back().toString(), std::string("s2"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('bytes'):getType(), vglobal:getPath('bytes'):toBytes()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("Bytes"));
            QCOMPARE(c.back().toString(), std::string("abcd"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('flags'):getType(), vglobal:getPath('flags'):toFlags()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("Flags"));
            QVERIFY(Engine::Table(c.back()).get("f1").toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('hash'):getType(), vglobal:getPath('hash'):hash('key'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("Hash"));
            QCOMPARE(c.back().toString(), std::string("hash_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('array'):getType(), vglobal:getPath('array'):array(1):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("Array"));
            QCOMPARE(c.back().toString(), std::string("array_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('matrix'):getType(), vglobal:getPath('matrix'):matrix(1, 2):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("Matrix"));
            QCOMPARE(c.back().toString(), std::string("matrix_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('keyframe'):getType(), vglobal:getPath('keyframe'):keyframe(1.25):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("Keyframe"));
            QCOMPARE(c.back().toString(), std::string("keyframe_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('metareal'):getType(), vglobal:getPath('metareal'):metadataReal('key'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("MetadataReal"));
            QCOMPARE(c.back().toString(), std::string("metareal_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('metaint'):getType(), vglobal:getPath('metaint'):metadataInteger('key'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("MetadataInteger"));
            QCOMPARE(c.back().toString(), std::string("metaint_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('metabool'):getType(), vglobal:getPath('metabool'):metadataBoolean('key'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("MetadataBoolean"));
            QCOMPARE(c.back().toString(), std::string("metabool_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('metastring'):getType(), vglobal:getPath('metastring'):metadataString('key'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("MetadataString"));
            QCOMPARE(c.back().toString(), std::string("metastring_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('metabytes'):getType(), vglobal:getPath('metabytes'):metadataBytes('key'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("MetadataBytes"));
            QCOMPARE(c.back().toString(), std::string("metabytes_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('metaarray'):getType(), vglobal:getPath('metaarray'):metadataArray('key'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("MetadataArray"));
            QCOMPARE(c.back().toString(), std::string("metaarray_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('metamatrix'):getType(), vglobal:getPath('metamatrix'):metadataMatrix('key'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("MetadataMatrix"));
            QCOMPARE(c.back().toString(), std::string("metamatrix_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('metakeyframe'):getType(), vglobal:getPath('metakeyframe'):metadataKeyframe('key'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("MetadataKeyframe"));
            QCOMPARE(c.back().toString(), std::string("metakeyframe_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('metahash'):getType(), vglobal:getPath('metahash'):metadataHash('key'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("MetadataHash"));
            QCOMPARE(c.back().toString(), std::string("metahash_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk("return vglobal:getPath('metahash'):metadataHashChild('child'):toString()",
                        false);
            QVERIFY(c.execute(1));
            QCOMPARE(c.front().toString(), std::string("metahash_child"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('metaflags'):getType(), vglobal:getPath('metaflags'):metadataFlags('key'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("MetadataFlags"));
            QCOMPARE(c.back().toString(), std::string("metaflags_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('metaflags'):metadataSingleFlag('single'):toString()",
                    false);
            QVERIFY(c.execute(1));
            QCOMPARE(c.front().toString(), std::string("metaflags_single"));
        }

        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:child('hash'):getPath('key'):toString(), vglobal:getPath('metahash'):child('child'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("hash_key"));
            QCOMPARE(c.back().toString(), std::string("metahash_child"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('metareal'):metadata('key'):toString(), vglobal:getPath('metaint'):metadata('key'):toString()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toString(), std::string("metareal_key"));
            QCOMPARE(c.back().toString(), std::string("metaint_key"));
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('sdfsdfsd'):exists(), vglobal:getPath('real'):exists()",
                    false);
            QVERIFY(c.execute(2));
            QVERIFY(!c.front().toBoolean());
            QVERIFY(c.back().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('metareal'):isMetadata(),  vglobal:getPath('hash'):isMetadata()",
                    false);
            QVERIFY(c.execute(2));
            QVERIFY(c.front().toBoolean());
            QVERIFY(!c.back().toBoolean());
        }

        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('real'):toNumber(), vglobal:getPath('integer'):toNumber()",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toNumber(), 2.5);
            QCOMPARE(c.back().toNumber(), 3.0);
        }

        {
            Engine::Call c(e);
            c.pushChunk("return vglobal:child('array'):arraySize()", false);
            QVERIFY(c.execute(1));
            QCOMPARE((int) c.front().toInteger(), 2);
        }
        {
            Engine::Call c(e);
            c.pushChunk("return vglobal:child('matrix'):matrixShape()", false);
            QVERIFY(c.execute(1));
            QCOMPARE((int) Engine::Table(c.front()).get(1).toInteger(), 2);
            QCOMPARE((int) Engine::Table(c.front()).get(2).toInteger(), 3);
        }

        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('flags'):testFlags({'f1'}), vglobal:getPath('flags'):testFlags({['f1']=true, ['f2']=true})",
                    false);
            QVERIFY(c.execute(2));
            QVERIFY(c.front().toBoolean());
            QVERIFY(!c.back().toBoolean());
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "return vglobal:getPath('flags'):testAnyFlags({'f1'}), vglobal:getPath('flags'):testAnyFlags({['f1']=true, ['f2']=true})",
                    false);
            QVERIFY(c.execute(2));
            QVERIFY(c.front().toBoolean());
            QVERIFY(c.back().toBoolean());
        }
    }

    void writeAccess()
    {
        Engine e;

        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value'):setEmpty(); vglobal:getPath('value'):setType('empty')",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::Empty);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value"].setInteger(3);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value'):setReal(3.5); vglobal:getPath('value'):setType('Real')",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::Real);
            QCOMPARE(vglobal["value"].toReal(), 3.5);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value'):setInteger(3); vglobal:getPath('value'):setType('Integer')",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::Integer);
            QCOMPARE((int) vglobal["value"].toInteger(), 3);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value'):setInteger('0x4000000000000001'); vglobal:getPath('value'):setType('Integer')",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::Integer);
            QCOMPARE((qint64) vglobal["value"].toInteger(), Q_INT64_C(0x4000000000000001));
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value'):setBoolean(true); vglobal:getPath('value'):setType('Boolean')",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::Boolean);
            QVERIFY(vglobal["value"].toBool());
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value'):setString('asdf'); vglobal:getPath('value'):setType('String'); vglobal:getPath('value'):setDisplayString('es', '1234');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::String);
            QCOMPARE(vglobal["value"].toString(), std::string("asdf"));
            QCOMPARE(vglobal["value"].toDisplayString("es"), QString("1234"));
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value'):setBytes('qwert'); vglobal:getPath('value'):setType('Bytes')",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::Bytes);
            QCOMPARE(vglobal["value"].toBytes(), Data::Variant::Bytes("qwert"));
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value'):setFlags('f1', 'f2'); vglobal:getPath('value'):setType('Flags'); vglobal:getPath('value'):addFlag({'f3'}); vglobal:getPath('value'):removeFlag({'f1'})",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::Flags);
            QCOMPARE(vglobal["value"].toFlags(), (Data::Variant::Flags{"f2", "f3"}));
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value/first"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value').second = 3.0; vglobal:getPath('value'):setType('Hash');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value/first"].toReal(), 2.0);
            QCOMPARE(vglobal["value/second"].toReal(), 3.0);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value/#0"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value'):afterBack():setReal(3.0); vglobal:getPath('value'):setType('Array');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::Array);
            QCOMPARE(vglobal["value/#0"].toReal(), 2.0);
            QCOMPARE(vglobal["value/#1"].toReal(), 3.0);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value/[1,2]"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value/[0,2]'):setReal(3.0); vglobal:getPath('value'):setType('Matrix'); vglobal:getPath('value'):matrixReshape(3,3);",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::Matrix);
            QCOMPARE(vglobal["value/[1,2]"].toReal(), 2.0);
            QCOMPARE(vglobal["value/[0,2]"].toReal(), 3.0);
            QCOMPARE(vglobal["value"].toMatrix().shape(),
                     (Data::Variant::PathElement::MatrixIndex{3, 3}));
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value/@1.0"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value/@2.0'):setReal(3.0); vglobal:getPath('value'):setType('Keyframe');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::Keyframe);
            QCOMPARE(vglobal["value/@1.0"].toReal(), 2.0);
            QCOMPARE(vglobal["value/@2.0"].toReal(), 3.0);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value/*rfirst"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value/*rsecond'):setReal(3.0); vglobal:getPath('value'):setType('MetadataReal');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::MetadataReal);
            QCOMPARE(vglobal["value/*rfirst"].toReal(), 2.0);
            QCOMPARE(vglobal["value/*rsecond"].toReal(), 3.0);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value/*ifirst"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value/*isecond'):setReal(3.0); vglobal:getPath('value'):setType('MetadataInteger');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::MetadataInteger);
            QCOMPARE(vglobal["value/*ifirst"].toReal(), 2.0);
            QCOMPARE(vglobal["value/*isecond"].toReal(), 3.0);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value/*bfirst"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value/*bsecond'):setReal(3.0); vglobal:getPath('value'):setType('MetadataBoolean');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::MetadataBoolean);
            QCOMPARE(vglobal["value/*bfirst"].toReal(), 2.0);
            QCOMPARE(vglobal["value/*bsecond"].toReal(), 3.0);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value/*sfirst"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value/*ssecond'):setReal(3.0); vglobal:getPath('value'):setType('MetadataString');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::MetadataString);
            QCOMPARE(vglobal["value/*sfirst"].toReal(), 2.0);
            QCOMPARE(vglobal["value/*ssecond"].toReal(), 3.0);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value/*nfirst"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value/*nsecond'):setReal(3.0); vglobal:getPath('value'):setType('MetadataBytes');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::MetadataBytes);
            QCOMPARE(vglobal["value/*nfirst"].toReal(), 2.0);
            QCOMPARE(vglobal["value/*nsecond"].toReal(), 3.0);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value/*afirst"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value/*asecond'):setReal(3.0); vglobal:getPath('value'):setType('MetadataArray');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::MetadataArray);
            QCOMPARE(vglobal["value/*afirst"].toReal(), 2.0);
            QCOMPARE(vglobal["value/*asecond"].toReal(), 3.0);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value/*tfirst"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value/*tsecond'):setReal(3.0); vglobal:getPath('value'):setType('MetadataMatrix');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::MetadataMatrix);
            QCOMPARE(vglobal["value/*tfirst"].toReal(), 2.0);
            QCOMPARE(vglobal["value/*tsecond"].toReal(), 3.0);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value/*efirst"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value/*esecond'):setReal(3.0); vglobal:getPath('value'):setType('MetadataKeyframe');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::MetadataKeyframe);
            QCOMPARE(vglobal["value/*efirst"].toReal(), 2.0);
            QCOMPARE(vglobal["value/*esecond"].toReal(), 3.0);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value/*hfirst"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value/*csecond'):setReal(3.0); vglobal:getPath('value'):setType('MetadataHash');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::MetadataHash);
            QCOMPARE(vglobal["value/*hfirst"].toReal(), 2.0);
            QCOMPARE(vglobal["value/*csecond"].toReal(), 3.0);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value/*ffirst"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value/*lsecond'):setReal(3.0); vglobal:getPath('value'):setType('MetadataFlags');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::MetadataFlags);
            QCOMPARE(vglobal["value/*ffirst"].toReal(), 2.0);
            QCOMPARE(vglobal["value/*lsecond"].toReal(), 3.0);
        }
        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["value"].setReal(2.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('value'):setOverlay('xxx'); vglobal:getPath('value'):setType('Overlay');",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            QCOMPARE(vglobal["value"].getType(), Data::Variant::Type::Overlay);
            QCOMPARE(vglobal["value"].toString(), std::string("xxx"));
        }

        {
            auto vglobal = Data::Variant::Write::empty();
            vglobal["first"].setReal(2.0);
            vglobal["second"].setReal(3.0);
            vglobal["third/#0"].setReal(4.0);
            Engine::Call c(e);
            {
                auto g = e.global();
                Engine::Assign a(c, g, "vglobal");
                a.pushData<Libs::Variant>(vglobal);
            }
            c.pushChunk(
                    "vglobal:getPath('first'):remove(); vglobal:getPath('second'):set({qwer='asdf'}); vglobal:getPath('third'):clear()",
                    false);
            QVERIFY(c.execute());
            QCOMPARE(vglobal.getType(), Data::Variant::Type::Hash);
            {
                auto check = Data::Variant::Write::empty();
                check["second/qwer"].setString("asdf");
                check["third"].setType(Data::Variant::Type::Array);
                QCOMPARE(vglobal, check);
            }
        }
    }
};

QTEST_APPLESS_MAIN(TestLuaVariant)

#include "variant.moc"
