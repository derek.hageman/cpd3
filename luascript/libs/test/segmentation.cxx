/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "luascript/engine.hxx"
#include "luascript/libs/segmentation.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestLuaSegmentation : public QObject {
Q_OBJECT

private slots:

    void basic()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(
s = CPD3.Segmentation();

s:add({
    START = 1000,
    END = 2000,
    value = 1,
});
s:add({
    START = 3000,
    END = 4000,
    value = 3,
});
s:add({
    START = 2000,
    END = 3000,
    value = 2,
});

if s:shift(100) ~= nil then error(); end

if #s ~= 3 then error(); end

data, START, END = s:shift(1100);
if data.value ~= 1 then error(); end
if START ~= 1000 then error(); end
if END ~= 2000 then error(); end

if #s ~= 3 then error(); end

data, START, END = s();
if data.value ~= 1 then error(); end
if START ~= 1000 then error(); end
if END ~= 2000 then error(); end

data, START, END = s();
if data.value ~= 2 then error(); end
if START ~= 2000 then error(); end
if END ~= 3000 then error(); end

data, START, END = s();
if data.value ~= 3 then error(); end
if START ~= 3000 then error(); end
if END ~= 4000 then error(); end

check = s();
if check ~= nil then error(); end

data = s[2];
if data.value ~= 2 then error(); end

data = s[-1];
if data.value ~= 3 then error(); end

s[2] = { value = 99 };

data, START, END = s:shift(2100, 2300);
if data.value ~= 99 then error(); end
if START ~= 2000 then error(); end
if END ~= 3000 then error(); end

it = s:iterate();

data, START, END = it();
if data.value ~= 99 then error(); end
if START ~= 2000 then error(); end
if END ~= 3000 then error(); end

data, START, END = it();
if data.value ~= 3 then error(); end
if START ~= 3000 then error(); end
if END ~= 4000 then error(); end

check = it();
if check ~= nil then error(); end

data = s[{ START = 3100 }];
if data.value ~= 99 then error(); end

data, START, END = s:find({ END = 2500 });
if data.value ~= 99 then error(); end
if START ~= 2000 then error(); end
if END ~= 3000 then error(); end
if s:find({ START = 1000, END = 2500 }, true) ~= 1 then error(); end

data, START, END = s:point(2500);
if data.value ~= 99 then error(); end
if START ~= 2000 then error(); end
if END ~= 3000 then error(); end
if s:point(3500, true) ~= 2 then error(); end

data, START, END = s:index(2);
if data.value ~= 3 then error(); end
if START ~= 3000 then error(); end
if END ~= 4000 then error(); end

save = CPD3.Segmentation(s);

s:clear();
if #s ~= 0 then error(); end

if #save ~= 2 then error(); end
data, START, END = save:index(2);
if data.value ~= 3 then error(); end
if START ~= 3000 then error(); end
if END ~= 4000 then error(); end

save:overlay({
    START = 3500,
    END = 5000,
    value = 4,
});
if #save ~= 4 then error(); end

data, START, END = save:index(1);
if data.value ~= 99 then error(); end
if START ~= 2000 then error(); end
if END ~= 3000 then error(); end

data, START, END = save:index(2);
if data.value ~= 3 then error(); end
if START ~= 3000 then error(); end
if END ~= 3500 then error(); end

data, START, END = save:index(3);
if data.value ~= 4 then error(); end
if START ~= 3500 then error(); end
if END ~= 4000 then error(); end

data, START, END = save:index(4);
if data.value ~= 4 then error(); end
if START ~= 4000 then error(); end
if END ~= 5000 then error(); end

save:clear();
if #save ~= 0 then error(); end

)EOF", false);
        QVERIFY(ok);

        QVERIFY(c.executeVariable());
    }

};

QTEST_APPLESS_MAIN(TestLuaSegmentation)

#include "segmentation.moc"
