/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QTemporaryFile>

#include "luascript/engine.hxx"
#include "luascript/libs/archive.hxx"
#include "luascript/libs/streamvalue.hxx"
#include "luascript/libs/valuesegment.hxx"
#include "luascript/libs/sequencesegment.hxx"
#include "datacore/archive/access.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

namespace CPD3 {
namespace Data {
namespace Archive {
bool operator==(const Selection &a, const Selection &b)
{
    if (!FP::equal(a.start, b.start))
        return false;
    if (!FP::equal(a.end, b.end))
        return false;
    if (!FP::equal(a.modifiedAfter, b.modifiedAfter))
        return false;
    if (a.includeDefaultStation != b.includeDefaultStation)
        return false;
    if (a.includeMetaArchive != b.includeMetaArchive)
        return false;

    auto al = a.stations;
    std::sort(al.begin(), al.end());
    auto bl = b.stations;
    std::sort(bl.begin(), bl.end());
    if (al != bl)
        return false;

    al = a.archives;
    std::sort(al.begin(), al.end());
    bl = b.archives;
    std::sort(bl.begin(), bl.end());
    if (al != bl)
        return false;

    al = a.variables;
    std::sort(al.begin(), al.end());
    bl = b.variables;
    std::sort(bl.begin(), bl.end());
    if (al != bl)
        return false;

    al = a.hasFlavors;
    std::sort(al.begin(), al.end());
    bl = b.hasFlavors;
    std::sort(bl.begin(), bl.end());
    if (al != bl)
        return false;

    al = a.lacksFlavors;
    std::sort(al.begin(), al.end());
    bl = b.lacksFlavors;
    std::sort(bl.begin(), bl.end());
    if (al != bl)
        return false;

    al = a.exactFlavors;
    std::sort(al.begin(), al.end());
    bl = b.exactFlavors;
    std::sort(bl.begin(), bl.end());
    if (al != bl)
        return false;

    return true;
}
}
}
}

class TestLuaArchive : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);

        CPD3::Data::Archive::Access access(databaseFile);
    }

    void selectionMemberAccess()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(

function add(table, v)
    table[2] = v;
    return table;
end

vglobal.front = vglobal.front + 2;
vglobal.back = vglobal.back + 3;
vglobal.stations = add(vglobal.stations, "s2");
vglobal.archive = add(vglobal.archives, "a2");
vglobal.variables = add(vglobal.variables, "v2");
vglobal.hasFlavors = "f2";
vglobal.lacksFlavors = {"f3", "f4"};
vglobal.modifiedAfter = vglobal.modifiedAfter + 4;

if not vglobal.includeDefaultStation then error() end
if not vglobal.includeMetaArchive then error() end

local v1 = CPD3.Archive.Selection(300, 400, "ls1", {"la1"}, "lv1");
local v2 = {
    START = 400,
    END = 500,
    station = "ls2",
    archive = "la2",
    variable = "lv2",
};
local v3 = {
    exactFlavors = {"lf3"},
    modifiedAfter = 2000,
};

return v1, v2, v3;
)EOF", false);
        QVERIFY(ok);

        {
            auto g = c.global();
            Engine::Assign a(c, g, "vglobal");
            a.pushData<Libs::Archive::Selection>(
                    CPD3::Data::Archive::Selection(100.0, 200.0, {"s1"}, {"a1"}, {"v1"}, {"hf1"},
                                                   {"lf1"}, {"ef1"}, 1000.0));
        }

        QVERIFY(c.executeVariable());
        QCOMPARE((int) c.size(), 3);

        auto ex = Libs::Archive::Selection::extract(c, c[0]);
        QCOMPARE(ex, CPD3::Data::Archive::Selection(300, 400, {"ls1"}, {"la1"}, {"lv1"}));
        ex = Libs::Archive::Selection::extract(c, c[1]);
        QCOMPARE(ex, CPD3::Data::Archive::Selection(400, 500, {"ls2"}, {"la2"}, {"lv2"}));
        ex = Libs::Archive::Selection::extract(c, c[2]);
        QCOMPARE(ex,
                 CPD3::Data::Archive::Selection(FP::undefined(), FP::undefined(), {}, {}, {}, {},
                                                {}, {"lf3"}, 2000.0));

        c.push(c.global(), "vglobal");
        ex = Libs::Archive::Selection::extract(c, c.back());
        QCOMPARE(ex,
                 CPD3::Data::Archive::Selection(102, 203, {"s1", "s2"}, {"a1", "a2"}, {"v1", "v2"},
                                                {"f2"}, {"f3", "f4"}, {}, 1004.0));
    }

    void selectionMethods()
    {
        Engine e;

        auto base = CPD3::Data::Archive::Selection(100.0, 200.0, {"s1"}, {"a1"}, {"v1"});
        {
            auto g = e.global();
            Engine::Frame f(e);
            {
                Engine::Assign a(f, g, "v1");
                a.pushData<Libs::Archive::Selection>(base);
            }
        }

        {
            Engine::Call c(e);
            c.pushChunk("return v1:withMetaArchive(false);", false);
            QVERIFY(c.execute(1));
            QCOMPARE(Libs::Archive::Selection::extract(c, c.front()), base.withMetaArchive(false));
        }
        {
            Engine::Call c(e);
            c.pushChunk("return v1:withDefaultStation(false);", false);
            QVERIFY(c.execute(1));
            QCOMPARE(Libs::Archive::Selection::extract(c, c.front()),
                     base.withDefaultStation(false));
        }
    }

    void archiveWrite()
    {
        Data::Archive::Access(databaseFile).writeSynchronous(CPD3::Data::SequenceValue::Transfer{
                CPD3::Data::SequenceValue({"bnd", "raw", "BsG_S11"}, CPD3::Data::Variant::Root(1.0),
                                          100, 200),
                CPD3::Data::SequenceValue({"bnd", "raw", "BsG_S11"}, CPD3::Data::Variant::Root(2.0),
                                          200, 300),
                CPD3::Data::SequenceValue({"bnd", "raw", "BsG_S11"}, CPD3::Data::Variant::Root(3.0),
                                          300, 400)});

        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(

local access = CPD3.Archive.Access();

access:erase(CPD3.ArchiveErasure(CPD3.SequenceIdentity({"bnd", "raw", "BsG_S11"}, 100, 200)));

repeat
    local lock = access:WriteLock();
    access:write(
        { CPD3.SequenceValue(CPD3.SequenceIdentity({"bnd", "raw", "BsG_S11"}, 400, 500), 4.0) },
        { CPD3.SequenceValue(CPD3.SequenceIdentity({"bnd", "raw", "BsG_S11"}, 200, 300), 2.0) }
    );
    access:add(CPD3.SequenceValue(CPD3.SequenceIdentity({"bnd", "raw", "BsG_S11"}, 500, 600), 5.0));
until lock:commit()

)EOF", false);
        QVERIFY(ok);

        QVERIFY(c.execute());

        auto result = CPD3::Data::Archive::Access(databaseFile).readSynchronous(
                CPD3::Data::Archive::Selection());
        QCOMPARE((int) result.size(), 3);
        CPD3::Data::SequenceValue::ValueEqual cmp;
        QVERIFY(cmp(result[0], CPD3::Data::SequenceValue({"bnd", "raw", "BsG_S11"},
                                                         CPD3::Data::Variant::Root(3.0), 300,
                                                         400)));
        QVERIFY(cmp(result[1], CPD3::Data::SequenceValue({"bnd", "raw", "BsG_S11"},
                                                         CPD3::Data::Variant::Root(4.0), 400,
                                                         500)));
        QVERIFY(cmp(result[2], CPD3::Data::SequenceValue({"bnd", "raw", "BsG_S11"},
                                                         CPD3::Data::Variant::Root(5.0), 500,
                                                         600)));
    }

    void archiveRead()
    {
        Data::Archive::Access(databaseFile).writeSynchronous(CPD3::Data::SequenceValue::Transfer{
                CPD3::Data::SequenceValue({"bnd", "raw", "BsG_S11"}, CPD3::Data::Variant::Root(1.0),
                                          100, 200),
                CPD3::Data::SequenceValue({"bnd", "raw", "BsG_S11"}, CPD3::Data::Variant::Root(2.0),
                                          200, 300),
                CPD3::Data::SequenceValue({"bnd", "raw", "BsG_S11"}, CPD3::Data::Variant::Root(3.0),
                                          300, 400)});

        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(

local access = CPD3.Archive.Access();

local r1 = access:read_SequenceValue({});
local r2 = {};
for v in access:stream_SequenceValue({}) do table.insert(r2, v); end

local lock = access:ReadLock();
local r3 = access:read_ValueSegment({});
local r4 = {};
for v in access:stream_ValueSegment({}) do table.insert(r4, v); end
lock:release();

local r5 = access:read_StreamSegment({});
local r6 = {};
for v in access:stream_StreamSegment({}) do table.insert(r6, v); end

local r7 = access:read_ArchiveErasure({});
local r8 = {};
for v in access:stream_ArchiveErasure({}) do table.insert(r8, v); end

local r9 = access:exists({ variables = "BsG_S11" });
local r10 = access:exists({ variables = "BsB_S11" });

return r1, r2, r3, r4, r5, r6, r7, r8, r9, r10;
)EOF", false);
        QVERIFY(ok);

        QVERIFY(c.execute(10));

        for (int i : std::initializer_list<int>{0, 1, 6, 7}) {
            Engine::Iterator it(c, c[i]);
            CPD3::Data::SequenceValue::ValueEqual cmp;

            QVERIFY(it.next());
            QVERIFY(cmp(Libs::SequenceValue::extract(it, it.value()),
                        CPD3::Data::SequenceValue({"bnd", "raw", "BsG_S11"},
                                                  CPD3::Data::Variant::Root(1.0), 100, 200)));
            QVERIFY(it.next());
            QVERIFY(cmp(Libs::SequenceValue::extract(it, it.value()),
                        CPD3::Data::SequenceValue({"bnd", "raw", "BsG_S11"},
                                                  CPD3::Data::Variant::Root(2.0), 200, 300)));
            QVERIFY(it.next());
            QVERIFY(cmp(Libs::SequenceValue::extract(it, it.value()),
                        CPD3::Data::SequenceValue({"bnd", "raw", "BsG_S11"},
                                                  CPD3::Data::Variant::Root(3.0), 300, 400)));
            QVERIFY(!it.next());
        }

        for (int i : std::initializer_list<int>{2, 3}) {
            Engine::Iterator it(c, c[i]);

            QVERIFY(it.next());
            auto v = Libs::ValueSegment::extract(it, it.value());
            QCOMPARE(v.getStart(), 100.0);
            QCOMPARE(v.getEnd(), 200.0);
            QCOMPARE(v.read().toReal(), 1.0);

            QVERIFY(it.next());
            v = Libs::ValueSegment::extract(it, it.value());
            QCOMPARE(v.getStart(), 200.0);
            QCOMPARE(v.getEnd(), 300.0);
            QCOMPARE(v.read().toReal(), 2.0);

            QVERIFY(it.next());
            v = Libs::ValueSegment::extract(it, it.value());
            QCOMPARE(v.getStart(), 300.0);
            QCOMPARE(v.getEnd(), 400.0);
            QCOMPARE(v.read().toReal(), 3.0);

            QVERIFY(!it.next());
        }

        for (int i : std::initializer_list<int>{4, 5}) {
            Engine::Iterator it(c, c[i]);

            QVERIFY(it.next());
            auto v = Libs::SequenceSegment::extract(it, it.value());
            QCOMPARE(v.getStart(), 100.0);
            QCOMPARE(v.getEnd(), 200.0);
            QCOMPARE(v.value({"bnd", "raw", "BsG_S11"}).toReal(), 1.0);

            QVERIFY(it.next());
            v = Libs::SequenceSegment::extract(it, it.value());
            QCOMPARE(v.getStart(), 200.0);
            QCOMPARE(v.getEnd(), 300.0);
            QCOMPARE(v.value({"bnd", "raw", "BsG_S11"}).toReal(), 2.0);

            QVERIFY(it.next());
            v = Libs::SequenceSegment::extract(it, it.value());
            QCOMPARE(v.getStart(), 300.0);
            QCOMPARE(v.getEnd(), 400.0);
            QCOMPARE(v.value({"bnd", "raw", "BsG_S11"}).toReal(), 3.0);

            QVERIFY(!it.next());
        }

        QVERIFY(c[8].toBoolean());
        QVERIFY(!c[9].toBoolean());
    }
};

QTEST_APPLESS_MAIN(TestLuaArchive)

#include "archive.moc"
