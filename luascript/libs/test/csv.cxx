/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "luascript/engine.hxx"
#include "luascript/libs/csv.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestLuaCSV : public QObject {
Q_OBJECT

private slots:

    void join()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(
return CPD3.CSV.join({"aa", "bb", "c,c"});
)EOF", false);
        QVERIFY(ok);

        QVERIFY(c.executeVariable());
        QCOMPARE((int) c.size(), 1);
        QCOMPARE(c[0].toQString(), QString("aa,bb,\"c,c\""));
    }

    void split()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(
return CPD3.CSV.split('aa,bb,"c,c"');
)EOF", false);
        QVERIFY(ok);

        QVERIFY(c.executeVariable());
        QCOMPARE((int) c.size(), 3);
        QCOMPARE(c[0].toQString(), QString("aa"));
        QCOMPARE(c[1].toQString(), QString("bb"));
        QCOMPARE(c[2].toQString(), QString("c,c"));
    }

};

QTEST_APPLESS_MAIN(TestLuaCSV)

#include "csv.moc"
