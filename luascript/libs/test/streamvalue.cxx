/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <luascript/libs/sequenceidentity.hxx>

#include "luascript/engine.hxx"
#include "luascript/libs/streamvalue.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestLuaStreamValue : public QObject {
Q_OBJECT

    using SeqId = CPD3::Data::SequenceIdentity;
    using SeqVal = CPD3::Data::SequenceValue;
    using ArcVal = CPD3::Data::ArchiveValue;
    using ArcEr = CPD3::Data::ArchiveErasure;
    using VRoot = CPD3::Data::Variant::Root;

private slots:

    void memberAccess()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(

vglobal.value = vglobal.value:toReal() + 1.0;

vglobal.front = vglobal.front + 2;
vglobal.back = vglobal.back + 3;
vglobal.priority = vglobal.priority + 4;

vglobal.station = vglobal.station .. "ONE";
vglobal.archive = "one" .. vglobal.archive;
vglobal.variable = vglobal.variable .. "ONE";
local f = vglobal.flavors;
f['added'] = true;
vglobal.flavors = f;

local name = vglobal.name;
name.station = name.station .. "two";
vglobal.station = vglobal.station .. "three";
vglobal.name = name;
vglobal.archive = vglobal.archive .. "three";
vglobal.variable = vglobal.variable .. "three";
local f = vglobal.flavors;
f['another'] = true;
vglobal.flavors = f;

local identity = vglobal.identity;
identity.front = identity.front + 1;
vglobal.identity = identity;
vglobal.front = vglobal.front + 2;

name.station = vglobal.station .. "four";

local v1 = CPD3.SequenceValue(CPD3.SequenceIdentity(CPD3.SequenceName('bnd', 'raw', 'BsG_S11'), 300, 400, 99), 23.0);

return v1;
)EOF", false);
        QVERIFY(ok);

        {
            auto g = c.global();
            Engine::Assign a(c, g, "vglobal");
            a.pushData<Libs::SequenceValue>(
                    SeqVal(SeqId("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3), VRoot(10.0)));
        }

        QVERIFY(c.executeVariable());
        QCOMPARE((int) c.size(), 1);

        QVERIFY(SeqVal::ValueEqual()(Libs::SequenceValue::extract(c, c.front()),
                                     SeqVal(SeqId("bnd", "raw", "BsG_S11", 300.0, 400.0, 99),
                                            VRoot(23.0))));

        c.push(c.global(), "vglobal");
        Libs::SequenceValue::extract(c, c.back());
        QVERIFY(SeqVal::ValueEqual()(Libs::SequenceValue::extract(c, c.back()),
                                     SeqVal(SeqId("brwonetwothreefour", "onerawthree",
                                                  "testONEthree", {"f1", "added", "another"}, 105.0,
                                                  203.0, 7), VRoot(11.0))));
    }

    void detachOrdering()
    {
        Engine e;

        {
            Engine::Call c(e);
            SeqVal base(SeqId("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3), VRoot(10.0));

            {
                auto g = e.global();
                Engine::Frame f(c);
                {
                    Engine::Assign a(f, g, "v1");
                    a.pushData<Libs::SequenceValue>(base);
                }
            }
            c.pushChunk(
                    "v1.variable = '1234'; v1.name = CPD3.SequenceName('bnd', 'raw', 'BsG_S11'); v1.station = 'alt'; v1.name.archive = 'clean'; v1.start = 101;",
                    false);
            QVERIFY(c.execute());

            c.push(c.global(), "v1");
            QVERIFY(SeqVal::ValueEqual()(Libs::SequenceValue::extract(c, c.back()),
                                         SeqVal(SeqId("alt", "clean", "BsG_S11", 101.0, 200.0, 3),
                                                VRoot(10.0))));
        }

        {
            Engine::Call c(e);
            SeqVal base(SeqId("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3), VRoot(10.0));

            {
                auto g = e.global();
                Engine::Frame f(c);
                {
                    Engine::Assign a(f, g, "v1");
                    a.pushData<Libs::SequenceValue>(base);
                }
                {
                    Engine::Assign a(f, g, "v2");
                    a.pushData<Libs::SequenceIdentity>(
                            SeqId("bnd", "raw", "BsG_S11", 100.0, 200.0, 4));
                }
            }
            c.pushChunk(
                    "v1.start = 0.0; v1.identity = v2; v1.station = 'alt'; v1.name.archive = 'clean'; v1.start = 101;",
                    false);
            QVERIFY(c.execute());

            c.push(c.global(), "v1");
            QVERIFY(SeqVal::ValueEqual()(Libs::SequenceValue::extract(c, c.back()),
                                         SeqVal(SeqId("alt", "clean", "BsG_S11", 101.0, 200.0, 4),
                                                VRoot(10.0))));
        }

        {
            Engine::Call c(e);
            SeqVal base(SeqId("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3), VRoot(10.0));

            {
                auto g = e.global();
                Engine::Frame f(c);
                {
                    Engine::Assign a(f, g, "v1");
                    a.pushData<Libs::SequenceValue>(base);
                }
            }
            c.pushChunk(
                    "v1.start = 0.0; v1.identity = CPD3.SequenceIdentity(CPD3.SequenceName('bnd', 'raw', 'BsG_S11'), 100, 200, 4); v1.station = 'alt'; v1.name.archive = 'clean'; v1.start = 101;",
                    false);
            QVERIFY(c.execute());

            c.push(c.global(), "v1");
            QVERIFY(SeqVal::ValueEqual()(Libs::SequenceValue::extract(c, c.back()),
                                         SeqVal(SeqId("alt", "clean", "BsG_S11", 101.0, 200.0, 4),
                                                VRoot(10.0))));
        }

        {
            Engine::Call c(e);
            SeqVal base(SeqId("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3), VRoot(10.0));

            {
                auto g = e.global();
                Engine::Frame f(c);
                {
                    Engine::Assign a(f, g, "v1");
                    a.pushData<Libs::SequenceValue>(base);
                }
            }
            c.pushChunk(
                    "v1.variable = '1234'; v1.name = CPD3.SequenceName('bnd', 'raw', 'BsG_S11'); v1.station = 'alt'; v1.identity.start = 101;",
                    false);
            QVERIFY(c.execute());

            c.push(c.global(), "v1");
            QVERIFY(SeqVal::ValueEqual()(Libs::SequenceValue::extract(c, c.back()),
                                         SeqVal(SeqId("alt", "raw", "BsG_S11", 101.0, 200.0, 3),
                                                VRoot(10.0))));
        }

        {
            Engine::Call c(e);
            SeqVal base(SeqId("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3), VRoot(10.0));

            {
                auto g = e.global();
                Engine::Frame f(c);
                {
                    Engine::Assign a(f, g, "v1");
                    a.pushData<Libs::SequenceValue>(base);
                }
                {
                    Engine::Assign a(f, g, "v2");
                    a.pushData<Libs::SequenceIdentity>(
                            SeqId("bnd", "raw", "BsG_S11", 100.0, 200.0, 4));
                }
            }
            c.pushChunk(
                    "v1.start = 0.0; v1.identity = v2; v1.name = CPD3.SequenceName('bnd', 'raw', 'BsG_S11'); v1.station = 'alt'; v1.name.archive = 'clean'; v2.start = 101;",
                    false);
            QVERIFY(c.execute());

            c.push(c.global(), "v1");
            QVERIFY(SeqVal::ValueEqual()(Libs::SequenceValue::extract(c, c.back()),
                                         SeqVal(SeqId("alt", "clean", "BsG_S11", 101.0, 200.0, 4),
                                                VRoot(10.0))));
        }
    }

    void sequenceValue()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(

vglobal.value = vglobal.value:toReal() + 1.0;

local v1 = CPD3.SequenceValue(CPD3.SequenceIdentity(CPD3.SequenceName('bnd', 'raw', 'BsG_S11'), 300, 400, 99), 23.0);

return v1;
)EOF", false);

        QVERIFY(ok);

        {
            auto g = c.global();
            Engine::Assign a(c, g, "vglobal");
            a.pushData<Libs::SequenceValue>(
                    SeqVal(SeqId("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3), VRoot(10.0)));
        }

        QVERIFY(c.executeVariable());
        QCOMPARE((int) c.size(), 1);

        QVERIFY(SeqVal::ValueEqual()(Libs::SequenceValue::extract(c, c.front()),
                                     SeqVal(SeqId("bnd", "raw", "BsG_S11", 300.0, 400.0, 99),
                                            VRoot(23.0))));

        c.push(c.global(), "vglobal");
        QVERIFY(SeqVal::ValueEqual()(Libs::SequenceValue::extract(c, c.back()),
                                     SeqVal(SeqId("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3),
                                            VRoot(11.0))));
    }

    void sequenceValueMethods()
    {
        Engine e;

        {
            auto g = e.global();
            Engine::Frame f(e);
            {
                Engine::Assign a(f, g, "v1");
                a.pushData<Libs::SequenceValue>(
                        SeqVal(SeqId("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3), VRoot(2.0)));
            }
        }

        {
            Engine::Call c(e);
            c.pushChunk("v1:set(3.0); return v1.value:toReal();", false);
            QVERIFY(c.execute(1));
            QCOMPARE(c.front().toReal(), 3.0);
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "local prior = v1.value; v1:detach(); prior:setReal(99); return v1.value:toReal(), prior:toReal();",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toReal(), 3.0);
            QCOMPARE(c.back().toReal(), 99.0);
        }
        {
            Engine::Call c(e);
            c.pushChunk("return tostring(v1)", false);
            QVERIFY(c.execute(1));
            QVERIFY(!c.front().toString().empty());
        }
    }

    void archiveValue()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(

vglobal.value = vglobal.value:toReal() + 1.0;
vglobal.modified = vglobal.modified + 2.0;
if not vglobal.remote then error(); end

local v1 = CPD3.ArchiveValue(CPD3.SequenceIdentity(CPD3.SequenceName('bnd', 'raw', 'BsG_S11'), 300, 400, 99), 23.0, 500);

return v1;
)EOF", false);

        QVERIFY(ok);

        {
            auto g = c.global();
            Engine::Assign a(c, g, "vglobal");
            a.pushData<Libs::ArchiveValue>(
                    ArcVal(SeqId("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3), VRoot(10.0), 250.0,
                           true));
        }

        QVERIFY(c.executeVariable());
        QCOMPARE((int) c.size(), 1);

        QVERIFY(ArcVal::ExactlyEqual()(Libs::ArchiveValue::extract(c, c.front()),
                                       ArcVal(SeqId("bnd", "raw", "BsG_S11", 300.0, 400.0, 99),
                                              VRoot(23.0), 500.0)));

        c.push(c.global(), "vglobal");
        QVERIFY(ArcVal::ExactlyEqual()(Libs::ArchiveValue::extract(c, c.back()),
                                       ArcVal(SeqId("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3),
                                              VRoot(11.0), 252.0, true)));
    }

    void archiveValueMethods()
    {
        Engine e;

        {
            auto g = e.global();
            Engine::Frame f(e);
            {
                Engine::Assign a(f, g, "v1");
                a.pushData<Libs::ArchiveValue>(
                        ArcVal(SeqId("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3), VRoot(2.0)));
            }
        }

        {
            Engine::Call c(e);
            c.pushChunk("v1:set(3.0); return v1.value:toReal();", false);
            QVERIFY(c.execute(1));
            QCOMPARE(c.front().toReal(), 3.0);
        }
        {
            Engine::Call c(e);
            c.pushChunk(
                    "local prior = v1.value; v1:detach(); prior:setReal(99); return v1.value:toReal(), prior:toReal();",
                    false);
            QVERIFY(c.execute(2));
            QCOMPARE(c.front().toReal(), 3.0);
            QCOMPARE(c.back().toReal(), 99.0);
        }
        {
            Engine::Call c(e);
            c.pushChunk("return tostring(v1)", false);
            QVERIFY(c.execute(1));
            QVERIFY(!c.front().toString().empty());
        }
    }

    void archiveErasure()
    {
        Engine e;
        Engine::Call c(e);
        bool ok = c.pushChunk(R"EOF(

vglobal.modified = vglobal.modified + 2.0;

local v1 = CPD3.ArchiveErasure(CPD3.SequenceIdentity(CPD3.SequenceName('bnd', 'raw', 'BsG_S11'), 300, 400, 99), 500);

return v1;
)EOF", false);

        QVERIFY(ok);

        {
            auto g = c.global();
            Engine::Assign a(c, g, "vglobal");
            a.pushData<Libs::ArchiveErasure>(
                    ArcEr(SeqId("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3), 250.0));
        }

        QVERIFY(c.executeVariable());
        QCOMPARE((int) c.size(), 1);

        QVERIFY(ArcEr::ExactlyEqual()(Libs::ArchiveErasure::extract(c, c.front()),
                                      ArcEr(SeqId("bnd", "raw", "BsG_S11", 300.0, 400.0, 99),
                                            500.0)));

        c.push(c.global(), "vglobal");
        QVERIFY(ArcEr::ExactlyEqual()(Libs::ArchiveErasure::extract(c, c.back()),
                                      ArcEr(SeqId("brw", "raw", "test", {"f1"}, 100.0, 200.0, 3),
                                            252.0)));
    }
};

QTEST_APPLESS_MAIN(TestLuaStreamValue)

#include "streamvalue.moc"
