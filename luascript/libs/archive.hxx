/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUASCRIPTLIBS_ARCHIVE_HXX
#define CPD3LUASCRIPTLIBS_ARCHIVE_HXX

#include "core/first.hxx"

#include "luascript/luascript.hxx"
#include "luascript/engine.hxx"
#include "datacore/archive/selection.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {
namespace Archive {

/**
 * Install the library on an engine target.
 *
 * @param target    the target table to put symbols in
 * @param frame     a frame to create in
 */
CPD3LUASCRIPT_EXPORT void install(Engine::Table &target, Engine::Frame &frame);

/**
 * A wrapper around a Archive::Selection CPD3 value.  The Lua API follows the C++ one, with
 * the exception that table-like access to the components is also permitted.
 */
class CPD3LUASCRIPT_EXPORT Selection : public Engine::Data {
    CPD3::Data::Archive::Selection selection;

    void meta_tostring(Engine::Entry &entry);

    void meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues);

    void meta_newindex(Engine::Entry &entry);

    void method_withDefaultStation(Engine::Entry &entry);

    void method_withMetaArchive(Engine::Entry &entry);

    static CPD3::Data::Archive::Selection constructFromFrame(Engine::Frame &frame);

public:
    Selection();

    virtual ~Selection();

    explicit Selection(const CPD3::Data::Archive::Selection &selection);

    explicit Selection(CPD3::Data::Archive::Selection &&selection);

    /**
     * Install the library on an engine target.
     *
     * @param target    the target table to put symbols in
     * @param frame     a frame to create in
     */
    static void install(Engine::Table &target, Engine::Frame &frame);

    /**
     * Extract a value from Lua code.
     *
     * @param frame     the frame to work in
     * @param ref       the reference to read from
     * @return          the extracted value
     */
    static CPD3::Data::Archive::Selection extract(Engine::Frame &frame,
                                                  const Engine::Reference &ref);

    /**
     * Get the current contents of the sequence name.
     *
     * @return  the sequence name
     */
    inline const CPD3::Data::Archive::Selection &get() const
    { return selection; }

    /**
     * Get the current contents of the sequence name.
     *
     * @return  the sequence name
     */
    inline CPD3::Data::Archive::Selection &get()
    { return selection; }

protected:
    void initialize(const Engine::Reference &self, Engine::Frame &frame) override;
};

/**
 * A wrapper around a CPD3 archive access context.
 */
class CPD3LUASCRIPT_EXPORT Access : public Engine::Data {
    std::shared_ptr<CPD3::Data::Archive::Access> access;

    void meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues);

    void method_read_lock(Engine::Entry &entry);

    void method_write_lock(Engine::Entry &entry);

    void method_write(Engine::Entry &entry);

    void method_add(Engine::Entry &entry);

    void method_erase(Engine::Entry &entry);

    void method_read_SequenceValue(Engine::Entry &entry);

    void method_read_ValueSegment(Engine::Entry &entry);

    void method_read_StreamSegment(Engine::Entry &entry);

    void method_read_ArchiveErasure(Engine::Entry &entry);

    void method_stream_SequenceValue(Engine::Entry &entry);

    void method_stream_ValueSegment(Engine::Entry &entry);

    void method_stream_StreamSegment(Engine::Entry &entry);

    void method_stream_ArchiveErasure(Engine::Entry &entry);

    void method_exists(Engine::Entry &entry);

public:
    Access();

    virtual ~Access();

    /**
     * Install the library on an engine target.
     *
     * @param target    the target table to put symbols in
     * @param frame     a frame to create in
     */
    static void install(Engine::Table &target, Engine::Frame &frame);

protected:
    void initialize(const Engine::Reference &self, Engine::Frame &frame) override;
};

}
}
}
}

#endif //CPD3LUASCRIPTLIBS_ARCHIVE_HXX
