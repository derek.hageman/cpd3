/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUASCRIPTLIBS_STREAMVALUE_HXX
#define CPD3LUASCRIPTLIBS_STREAMVALUE_HXX

#include "core/first.hxx"

#include "luascript/luascript.hxx"
#include "luascript/engine.hxx"
#include "datacore/stream.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {

/**
 * A wrapper around a stream type CPD3 value.  The Lua API follows the C++ one, with
 * the exception that table-like access to the components (station, archive, variable, flavors,
 * start/front, end/back, priority, name, identity) is also permitted.
 */
class CPD3LUASCRIPT_EXPORT StreamValue : public Engine::Data {
    CPD3::Data::SequenceIdentity identity;
    enum class DetachState {
        AllAttached, IdentityDetached, NameDetached, AllDetached
    };
    DetachState detachState;

    void method_overlayLessThan(Engine::Entry &entry);

    void method_timeLessThan(Engine::Entry &entry);

    void fetchDetachedName(Engine::Frame &frame, const Engine::Reference &self) const;

    void fetchDetachedIdentity(Engine::Frame &frame, const Engine::Reference &self) const;

    void assignName(Engine::Frame &frame,
                    const Engine::Reference &self,
                    const Engine::Reference &name);

    void assignIdentity(Engine::Frame &frame,
                        const Engine::Reference &self,
                        const Engine::Reference &identity);

public:
    StreamValue();

    virtual ~StreamValue();

    explicit StreamValue(const CPD3::Data::SequenceIdentity &identity);

    explicit StreamValue(CPD3::Data::SequenceIdentity &&identity);

    /**
     * Test if the wrapper has a detached name reference.
     *
     * @return  true if the wrapper has detached the name
     */
    bool hasDetachedName() const
    {
        switch (detachState) {
        case DetachState::NameDetached:
        case DetachState::AllDetached:
            return true;
        default:
            break;
        }
        return false;
    }

    /**
     * Test if the wrapper has a detached name reference.
     *
     * @return  true if the wrapper has detached the name
     */
    bool hasDetachedIdentity() const
    {
        switch (detachState) {
        case DetachState::IdentityDetached:
        case DetachState::AllDetached:
            return true;
        default:
            break;
        }
        return false;
    }

    /**
     * Get the current contents of the sequence name.
     *
     * @return  the sequence name
     */
    inline const CPD3::Data::SequenceIdentity &getIdentity() const
    {
        Q_ASSERT(!hasDetachedIdentity() && !hasDetachedName());
        return identity;
    }

    /**
     * Get the current contents of the sequence name.
     *
     * @return  the sequence name
     */
    inline CPD3::Data::SequenceIdentity &getIdentity()
    {
        Q_ASSERT(!hasDetachedIdentity() && !hasDetachedName());
        return identity;
    }

    /**
     * Get the current contents of the sequence identity.
     * <br>
     * This variant does not require an attached name, but it is only safe to operate
     * on the other components (start, end, priority).
     *
     * @return  the sequence name
     */
    inline const CPD3::Data::SequenceIdentity &getIdentityAttached() const
    {
        Q_ASSERT(!hasDetachedIdentity());
        return identity;
    }

    /**
     * Get the current contents of the sequence identity.
     * <br>
     * This variant does not require an attached name, but it is only safe to operate
     * on the other components (start, end, priority).
     *
     * @return  the sequence name
     */
    inline CPD3::Data::SequenceIdentity &getIdentityAttached()
    {
        Q_ASSERT(!hasDetachedIdentity());
        return identity;
    }


    /**
     * Get the current current identity the value contains.
     *
     * @param frame the working frame
     * @param self  a reference to the Lua object
     * @return      the assembled identity
     */
    CPD3::Data::SequenceIdentity getIdentity(Engine::Frame &frame,
                                             const Engine::Reference &self) const;

    /**
     * Push the detached identity, creating it if required.
     *
     * @param frame     the target frame
     * @param self      a reference to the Lua object
     */
    void pushDetachedIdentity(Engine::Frame &frame, const Engine::Reference &self);

    /**
     * Get the current contents of the sequence name.
     *
     * @return  the sequence name
     */
    inline const CPD3::Data::SequenceName &getName() const
    {
        Q_ASSERT(!hasDetachedName() && !hasDetachedName());
        return identity.getName();
    }

    /**
     * Get the current contents of the sequence name.
     *
     * @return  the sequence name
     */
    inline CPD3::Data::SequenceName &getName()
    {
        Q_ASSERT(!hasDetachedName() && !hasDetachedName());
        return identity.getName();
    }


    /**
     * Get the current current name the value contains.
     *
     * @param frame the working frame
     * @param self  a reference to the Lua object
     * @return      the assembled name
     */
    CPD3::Data::SequenceName getName(Engine::Frame &frame, const Engine::Reference &self) const;

    /**
     * Push the detached name, creating it if required.
     *
     * @param frame     the target frame
     * @param self      a reference to the Lua object
     */
    void pushDetachedName(Engine::Frame &frame, const Engine::Reference &self);

    /**
     * Get the start time of the stream value.
     *
     * @param frame the working frame
     * @param self  a reference to the Lua object
     * @return      the start time of the value
     */
    double getStart(Engine::Frame &frame, const Engine::Reference &self) const;

protected:
    /**
     * Push the metatable for stream values.
     *
     * @param target            the target frame
     * @param addMethods        a call to add any additional methods to a lookup table
     * @return                  the metatable
     */
    static Engine::Table pushMetatable(Engine::Frame &target,
                                       const std::function<void(Engine::Frame &,
                                                                Engine::Table &)> &addMethods = std::function<
                                               void(Engine::Frame &, Engine::Table &)>());

    /**
     * Assign a value from a table.
     *
     * @param identity  the target identity
     * @param frame     the working frame
     * @param table     the source table
     */
    static void assignFromTable(CPD3::Data::SequenceIdentity &identity,
                                Engine::Frame &frame,
                                const Engine::Table &table);

    /**
     * The method called for the __index metamethod.
     *
     * @param entry     the frame entry
     * @param upvalues  the stored upvalues
     * @return          true if the index was consumed
     */
    virtual bool meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues);

    /**
     * The method called for the __newindex metamethod.
     *
     * @param entry     the frame entry
     * @return          true if the index was consumed
     */
    virtual bool meta_newindex(Engine::Entry &entry);

    /**
     * The method called for the :describe method.
     *
     * @param entry     the frame entry
     */
    virtual void meta_tostring(Engine::Entry &entry) = 0;
};

/**
 * A wrapper around a SequenceValue CPD3 value.
 */
class CPD3LUASCRIPT_EXPORT SequenceValue : public StreamValue {
    CPD3::Data::Variant::Write contents;

    static CPD3::Data::SequenceValue constructFromFrame(Engine::Frame &frame);

    void method_detach(Engine::Entry &entry);

    void method_set(Engine::Entry &entry);

public:
    SequenceValue();

    virtual ~SequenceValue();

    explicit SequenceValue(const CPD3::Data::SequenceValue &value);

    explicit SequenceValue(CPD3::Data::SequenceValue &&value);

    /**
     * Install the library on an engine target.
     *
     * @param target    the target table to put symbols in
     * @param frame     a frame to create in
     */
    static void install(Engine::Table &target, Engine::Frame &frame);

    /**
     * Extract a value from Lua code.
     *
     * @param frame     the frame to work in
     * @param ref       the reference to read from
     * @return          the extracted value
     */
    static CPD3::Data::SequenceValue extract(Engine::Frame &frame, const Engine::Reference &ref);

    /**
     * Extract a copy of the value from the wrapper.  Since Lua references
     * to the wrapper are uncontrollable this cannot return a direct reference
     * to it (since Lua could change it at any time).
     *
     * @return  a copy of the value
     */
    CPD3::Data::Variant::Root extractValue() const
    { return CPD3::Data::Variant::Root(contents); }

    /**
     * Get the contents of the value.
     *
     * @return  the contents
     */
    const CPD3::Data::Variant::Write &getVariant() const
    { return contents; }

    /**
     * Get the current contents of the sequence value.
     *
     * @param frame the working frame
     * @param self  a reference to the Lua object
     * @return      the assembled identity
     */
    CPD3::Data::SequenceValue get(Engine::Frame &frame, const Engine::Reference &self) const;

protected:
    /**
     * Assign a value from a table.
     *
     * @param identity  the target identity
     * @param value     the target value
     * @param frame     the working frame
     * @param table     the source table
     */
    static void assignFromTable(CPD3::Data::SequenceValue &value,
                                Engine::Frame &frame,
                                const Engine::Table &table);

    static Engine::Table pushMetatable(Engine::Frame &target,
                                       const std::function<void(Engine::Frame &,
                                                                Engine::Table &)> &addMethods = std::function<
                                               void(Engine::Frame &, Engine::Table &)>());

    void initialize(const Engine::Reference &self, Engine::Frame &frame) override;

    void meta_tostring(Engine::Entry &entry) override;

    bool meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues) override;

    bool meta_newindex(Engine::Entry &entry) override;

    /**
     * Get the current contents of the value.
     *
     * @return  the contents handle
     */
    const CPD3::Data::Variant::Write &getContents() const
    { return contents; }
};

/**
 * A wrapper around an ArchiveValue CPD3 value.
 */
class CPD3LUASCRIPT_EXPORT ArchiveValue : public SequenceValue {
    double modified;
    bool remoteReferenced;

    static CPD3::Data::ArchiveValue constructFromFrame(Engine::Frame &frame);

    static void assignFromTable(CPD3::Data::ArchiveValue &value,
                                Engine::Frame &frame,
                                const Engine::Table &table);

public:
    ArchiveValue();

    virtual ~ArchiveValue();

    explicit ArchiveValue(const CPD3::Data::ArchiveValue &value);

    explicit ArchiveValue(CPD3::Data::ArchiveValue &&value);

    /**
     * Install the library on an engine target.
     *
     * @param target    the target table to put symbols in
     * @param frame     a frame to create in
     */
    static void install(Engine::Table &target, Engine::Frame &frame);

    /**
     * Extract a value from Lua code.
     *
     * @param frame     the frame to work in
     * @param ref       the reference to read from
     * @return          the extracted value
     */
    static CPD3::Data::ArchiveValue extract(Engine::Frame &frame, const Engine::Reference &ref);

    /**
     * Get the current contents of the sequence value.
     *
     * @param frame the working frame
     * @param self  a reference to the Lua object
     * @return      the assembled identity
     */
    CPD3::Data::ArchiveValue get(Engine::Frame &frame, const Engine::Reference &self) const;

protected:

    void initialize(const Engine::Reference &self, Engine::Frame &frame) override;

    void meta_tostring(Engine::Entry &entry) override;

    bool meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues) override;

    bool meta_newindex(Engine::Entry &entry) override;
};

/**
 * A wrapper around an ArchiveErasure CPD3 value.
 */
class CPD3LUASCRIPT_EXPORT ArchiveErasure : public StreamValue {
    double modified;

    static CPD3::Data::ArchiveErasure constructFromFrame(Engine::Frame &frame);

    static void assignFromTable(CPD3::Data::ArchiveErasure &value,
                                Engine::Frame &frame,
                                const Engine::Table &table);

public:
    ArchiveErasure();

    virtual ~ArchiveErasure();

    explicit ArchiveErasure(const CPD3::Data::ArchiveErasure &value);

    explicit ArchiveErasure(CPD3::Data::ArchiveErasure &&value);

    /**
     * Install the library on an engine target.
     *
     * @param target    the target table to put symbols in
     * @param frame     a frame to create in
     */
    static void install(Engine::Table &target, Engine::Frame &frame);

    /**
     * Extract a value from Lua code.
     *
     * @param frame     the frame to work in
     * @param ref       the reference to read from
     * @return          the extracted value
     */
    static CPD3::Data::ArchiveErasure extract(Engine::Frame &frame, const Engine::Reference &ref);

    /**
     * Get the current contents of the sequence value.
     *
     * @param frame the working frame
     * @param self  a reference to the Lua object
     * @return      the assembled identity
     */
    CPD3::Data::ArchiveErasure get(Engine::Frame &frame, const Engine::Reference &self) const;

protected:

    void initialize(const Engine::Reference &self, Engine::Frame &frame) override;

    void meta_tostring(Engine::Entry &entry) override;

    bool meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues) override;

    bool meta_newindex(Engine::Entry &entry) override;
};

}
}
}

#endif //CPD3LUASCRIPTLIBS_STREAMVALUE_HXX
