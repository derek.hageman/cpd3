/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUASCRIPTLIBS_VARIANT_HXX
#define CPD3LUASCRIPTLIBS_VARIANT_HXX

#include "core/first.hxx"

#include <functional>

#include "luascript/luascript.hxx"
#include "luascript/engine.hxx"
#include "datacore/variant/handle.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {

/**
 * A wrapper around a (write) variant handle.  The Lua API mirrors the C++ API for
 * CPD3::Data::Variant::Write with the addition that direct "table-like" access
 * is like doing a getPath(...) for non-primitive types but for primitives it does the same
 * and then returns the Lua converted primitive value.
 */
class CPD3LUASCRIPT_EXPORT Variant : public Engine::Data {
    CPD3::Data::Variant::Write var;

    void method_getPath(Engine::Entry &entry);

    void method_child(Engine::Entry &entry);

    void method_hash(Engine::Entry &entry);

    void method_array(Engine::Entry &entry);

    void method_arraySize(Engine::Entry &entry);

    void method_afterBack(Engine::Entry &entry);

    void method_matrix(Engine::Entry &entry);

    void method_matrixShape(Engine::Entry &entry);

    void method_matrixReshape(Engine::Entry &entry);

    void method_keyframe(Engine::Entry &entry);

    void method_metadata(Engine::Entry &entry);

    void method_metadataReal(Engine::Entry &entry);

    void method_metadataInteger(Engine::Entry &entry);

    void method_metadataBoolean(Engine::Entry &entry);

    void method_metadataString(Engine::Entry &entry);

    void method_metadataBytes(Engine::Entry &entry);

    void method_metadataFlags(Engine::Entry &entry);

    void method_metadataArray(Engine::Entry &entry);

    void method_metadataMatrix(Engine::Entry &entry);

    void method_metadataHash(Engine::Entry &entry);

    void method_metadataKeyframe(Engine::Entry &entry);

    void method_metadataHashChild(Engine::Entry &entry);

    void method_metadataSingleFlag(Engine::Entry &entry);

    void method_detachFromRoot(Engine::Entry &entry);

    void method_describe(Engine::Entry &entry);

    void method_getType(Engine::Entry &entry);

    void method_setType(Engine::Entry &entry);

    void method_setEmpty(Engine::Entry &entry);

    void method_exists(Engine::Entry &entry);

    void method_isMetadata(Engine::Entry &entry);

    void method_toArray(Engine::Entry &entry);

    void method_toHash(Engine::Entry &entry);

    void method_toNumber(Engine::Entry &entry);

    void method_toReal(Engine::Entry &entry);

    void method_setReal(Engine::Entry &entry);

    void method_toInteger(Engine::Entry &entry);

    void method_setInteger(Engine::Entry &entry);

    void method_toBoolean(Engine::Entry &entry);

    void method_setBoolean(Engine::Entry &entry);

    void method_toString(Engine::Entry &entry);

    void method_setString(Engine::Entry &entry);

    void method_toDisplayString(Engine::Entry &entry);

    void method_setDisplayString(Engine::Entry &entry);

    void method_allLocalizedStrings(Engine::Entry &entry);

    void method_toBytes(Engine::Entry &entry);

    void method_setBytes(Engine::Entry &entry);

    void method_toFlags(Engine::Entry &entry);

    void method_setFlags(Engine::Entry &entry);

    void method_addFlags(Engine::Entry &entry);

    void method_removeFlags(Engine::Entry &entry);

    void method_testFlags(Engine::Entry &entry);

    void method_testAnyFlags(Engine::Entry &entry);

    void method_setOverlay(Engine::Entry &entry);

    void method_clear(Engine::Entry &entry);

    void method_remove(Engine::Entry &entry);

    void method_set(Engine::Entry &entry);

    static void pushBinaryOp(Engine::Frame &frame,
                             const std::function<
                                     void(CPD3::Lua::Engine::Entry &, Variant *, Variant *)> &first,
                             const std::function<void(CPD3::Lua::Engine::Entry &,
                                                      Variant *,
                                                      const Engine::Reference &)> &second,
                             const std::function<void(CPD3::Lua::Engine::Entry &,
                                                      const Engine::Reference &,
                                                      Variant *)> &third);

    static void pushArithmeticOp(Engine::Frame &frame,
                                 const std::function<double(double, double)> &op);

    void meta_umn(Engine::Entry &entry);

    void meta_eq(Engine::Entry &entry);

    void meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues);

    void meta_newindex(Engine::Entry &entry);

public:
    Variant();

    virtual ~Variant();

    explicit Variant(const CPD3::Data::Variant::Write &var);

    explicit Variant(CPD3::Data::Variant::Write &&var);

    explicit Variant(CPD3::Data::Variant::Root &root);

    /**
     * Install the library on an engine target.
     *
     * @param target    the target table to put symbols in
     * @param frame     a frame to create in
     */
    static void install(Engine::Table &target, Engine::Frame &frame);

    /**
     * Extract a value from Lua code.  This does not detach the variant if the
     * reference refers to one (i.e. script code modifications will propagate).
     *
     * @param frame     the frame to work in
     * @param ref       the reference to read from
     * @return          the extracted variant
     */
    static CPD3::Data::Variant::Write extract(Engine::Frame &frame, const Engine::Reference &ref);

    /**
     * Set a value from a Lua reference.  The Lua reference is not attached to
     * the value.
     *
     * @param target    the target to set to
     * @param frame     the frame to work in
     * @param ref       the reference to read from
     */
    static void set(CPD3::Data::Variant::Write &target,
                    Engine::Frame &frame,
                    const Engine::Reference &ref);

    /**
     * Perform the lookup de-referencing used when doing a table-like access.  This
     * handles the conversion of simple types to Lua primitives, so they work natively
     * with normal number/string access.
     *
     * @param source    the source value
     * @param frame     the frame to push on
     */
    static void pushLookup(CPD3::Data::Variant::Write source, Engine::Frame &frame);

protected:
    void initialize(const Engine::Reference &self, Engine::Frame &frame) override;
};

}
}
}

#endif //CPD3LUASCRIPTLIBS_VARIANT_HXX
