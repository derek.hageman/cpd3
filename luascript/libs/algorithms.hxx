/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUASCRIPTLIBS_ALGORITHMS_HXX
#define CPD3LUASCRIPTLIBS_ALGORITHMS_HXX

#include "core/first.hxx"

#include "luascript/luascript.hxx"
#include "luascript/engine.hxx"
#include "core/timeutils.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {
namespace Algorithms {

/**
 * Install the library on an engine target.
 *
 * @param target    the target table to put symbols in
 * @param frame     a frame to create in
 */
CPD3LUASCRIPT_EXPORT void install(Engine::Table &target, Engine::Frame &frame);

}
}
}
}

#endif //CPD3LUASCRIPTLIBS_ALGORITHMS_HXX
