/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUASCRIPTLIBS_SEGMENTATION_HXX
#define CPD3LUASCRIPTLIBS_SEGMENTATION_HXX

#include "core/first.hxx"

#include <memory>
#include <deque>
#include <unordered_map>

#include "luascript/luascript.hxx"
#include "luascript/engine.hxx"
#include "core/timeutils.hxx"

namespace CPD3 {
namespace Lua {
namespace Libs {


/**
 * A wrapper that handles generic segmentation management.  This includes
 * overlay and intersection routines.
 */
class CPD3LUASCRIPT_EXPORT Segmentation : public Engine::Data {

    struct BufferContext {
        std::unordered_map<std::size_t, std::size_t> references;
        std::vector<std::size_t> released;
        std::size_t next;

        BufferContext();

        ~BufferContext();

        void releasePending(Engine::Frame &frame, const Engine::Reference &buffer);

        void reset();

        void deref(std::size_t data);

        void ref(std::size_t data);

        std::size_t acquire(Engine::Frame &frame,
                            const Engine::Reference &buffer,
                            const Engine::Reference &data);
    };

    BufferContext context;

    struct Overlay : public CPD3::Time::Bounds {
        BufferContext &context;
        Engine::Frame &frame;
        const Engine::Reference &buffer;
        const Engine::Reference &data;
        const Engine::Reference &overlay;
        const Engine::Reference &convert;

        Overlay() = delete;

        Overlay(double start,
                double end,
                BufferContext &context,
                Engine::Frame &frame,
                const Engine::Reference &buffer,
                const Engine::Reference &data,
                const Engine::Reference &overlay,
                const Engine::Reference &convert);

        ~Overlay();
    };

    struct Entry : public CPD3::Time::Bounds {
        BufferContext *context;
        std::size_t data;

        Entry();

        ~Entry();

        Entry(Entry &&other);

        Entry &operator=(Entry &&other);

        Entry(const Entry &other);

        Entry &operator=(const Entry &other);

        Entry(const Overlay &overlay);

        Entry(const Entry &other, double start, double end);

        Entry(const Overlay &overlay, double start, double end);

        Entry(const Entry &under, const Overlay &over, double start, double end);

        void pushData(Engine::Frame &frame, const Engine::Reference &buffer) const;

        void pushLookup(Engine::Frame &frame, const Engine::Reference &buffer) const;
    };

    std::deque<Entry> contents;

    std::size_t iterationIndex;


    void meta_tostring(Engine::Entry &entry);

    void meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues);

    void meta_newindex(Engine::Entry &entry);

    void meta_len(Engine::Entry &entry);

    void meta_call(Engine::Entry &entry);

    void method_iterate(Engine::Entry &entry);

    void method_overlay(Engine::Entry &entry);

    void method_clear(Engine::Entry &entry);

    void method_find(Engine::Entry &entry);

    void method_point(Engine::Entry &entry);

    void method_shift(Engine::Entry &entry);

    void method_lookup(Engine::Entry &entry);

public:
    Segmentation();

    virtual ~Segmentation();

    /**
     * Install the library on an engine target.
     *
     * @param target    the target table to put symbols in
     * @param frame     a frame to create in
     */
    static void install(Engine::Table &target, Engine::Frame &frame);

protected:
    void initialize(const Engine::Reference &self, Engine::Frame &frame) override;
};

}
}
}

#endif //CPD3LUASCRIPTLIBS_SEGMENTATION_HXX
