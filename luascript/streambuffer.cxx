/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <deque>

#include "streambuffer.hxx"
#include "core/range.hxx"
#include "libs/streamvalue.hxx"
#include "libs/sequencesegment.hxx"


namespace CPD3 {
namespace Lua {

static Engine::Table pushBuffer(Engine::Frame &frame, const Engine::Reference &self)
{
    {
        Engine::Frame local(frame);
        auto mt = Engine::Table(self).pushMetatable(local);
        local.push(mt, "buffer");
        local.replaceWithBack(0);
        local.propagate(1);
    }
    return frame.back();
}

class StreamBuffer::Context {
    StreamBuffer *parent;
    double latestReleased;
    double latestAdvanced;

    enum class State {
        Active, ExternalEnded, OutputEnded, BothEnded, Finished,
    };
    State state;
    double autoreleaseThreshold;

    std::size_t nextIndex;
    std::deque<std::size_t> indices;

    struct BufferComparator {
        StreamBuffer *parent;
        Engine::Frame &frame;
        Engine::Table &buffer;

        BufferComparator(StreamBuffer *parent, Engine::Frame &frame, Engine::Table &buffer)
                : parent(parent), frame(frame), buffer(buffer)
        { }

        bool operator()(double a, double b) const
        { return Range::compareStart(a, b) < 0; }

        bool operator()(std::size_t a, double b) const
        {
            Engine::Frame local(frame);
            local.pushRaw(buffer, a);
            return Range::compareStart(parent->getStart(local, local.back()), b) < 0;
        }

        bool operator()(double a, std::size_t b) const
        {
            Engine::Frame local(frame);
            local.pushRaw(buffer, b);
            return Range::compareStart(a, parent->getStart(local, local.back())) < 0;
        }

        bool operator()(std::size_t a, std::size_t b) const
        {
            Engine::Frame local(frame);
            local.pushRaw(buffer, a);
            local.pushRaw(buffer, b);
            return Range::compareStart(parent->getStart(local, local.front()),
                                       parent->getStart(local, local.back())) < 0;
        }
    };

    void performInsert(Engine::Frame &frame,
                       Engine::Table &buffer,
                       const Engine::Reference &value,
                       double start)
    {
        Q_ASSERT(parent);
        Q_ASSERT(Range::compareStart(start, latestReleased) >= 0);

        if (indices.empty()) {
            nextIndex = 2;
            indices.emplace_back(1);
            buffer.rawSet(1, value);
            return;
        }

        std::size_t insertIndex = nextIndex;
        nextIndex = (static_cast<std::uint_fast32_t>(nextIndex) + 1) & 0x7FFFFFFFU;

        /* Common case, it's just at the back */
        {
            Engine::Frame local(frame);
            local.pushRaw(buffer, indices.back());
            double checkTime = parent->getStart(local, local.back());
            if (Range::compareStart(checkTime, start) <= 0) {
                indices.emplace_back(insertIndex);
                buffer.rawSet(insertIndex, value);
                return;
            }
        }

        auto target = std::upper_bound(indices.begin(), indices.end(), start,
                                       BufferComparator(parent, frame, buffer));
        indices.emplace(target, insertIndex);
        buffer.rawSet(insertIndex, value);
    }

    void releaseBefore(Engine::Frame &frame, Engine::Table &buffer, double time)
    {
        Q_ASSERT(FP::defined(time));
        Q_ASSERT(Range::compareStart(latestReleased, time) <= 0);

        switch (state) {
        case State::OutputEnded:
        case State::BothEnded:
        case State::Finished:
            latestReleased = time;
            return;
        case State::Active:
        case State::ExternalEnded:
            break;
        }

        if (!parent) {
            latestReleased = time;
            return;
        }

        /* Only release values with times strictly less than the threshold, so that
         * we know we retain the active one while we're operating on it */

        while (!indices.empty()) {
            Engine::Frame local(frame);
            local.pushRaw(buffer, indices.front());
            double start = parent->getStart(local, local.back());
            if (FP::defined(start) && start >= time)
                break;

            buffer.rawErase(indices.front());
            indices.pop_front();

            if (Range::compareStart(latestReleased, start) > 0)
                continue;
            latestReleased = start;

            parent->outputReady(local, local.back());
        }

        latestReleased = time;
        parent->advanceReady(time);
    }

    void releaseAll(Engine::Frame &frame, Engine::Table &buffer)
    {
        Q_ASSERT(parent);

        while (!indices.empty()) {
            Engine::Frame local(frame);
            local.pushRaw(buffer, indices.front());
            buffer.rawErase(indices.front());
            indices.pop_front();

            double start = parent->getStart(local, local.back());
            if (Range::compareStart(latestReleased, start) > 0)
                continue;
            latestReleased = start;

            parent->outputReady(local, local.back());
        }
    }

public:
    explicit Context(StreamBuffer *parent) : parent(parent),
                                             latestReleased(FP::undefined()),
                                             latestAdvanced(FP::undefined()),
                                             state(State::Active),
                                             autoreleaseThreshold(0),
                                             nextIndex(1)
    { }

    ~Context() = default;

    void decouple()
    { parent = nullptr; }

    inline double getAutorelease() const
    { return autoreleaseThreshold; }

    void setAutorelease(double seconds)
    { autoreleaseThreshold = seconds; }

    inline double getAdvancedTime() const
    { return latestAdvanced; }

    void advanceTo(Engine::Frame &frame, Engine::Table &buffer, double time)
    {
        Q_ASSERT(FP::defined(time));
        Q_ASSERT(Range::compareStart(time, latestAdvanced) >= 0);
        latestAdvanced = time;

        if (!FP::defined(autoreleaseThreshold))
            return;

        time -= autoreleaseThreshold;
        if (Range::compareStartEnd(latestReleased, time) > 0)
            time = latestReleased;

        releaseBefore(frame, buffer, time);
    }


    bool processNext(Engine::Frame &target,
                     const Engine::Reference &controller,
                     bool ignoreEnd = false)
    {
        for (;;) {
            if (target.inError())
                return false;
            switch (state) {
            case State::BothEnded:
            case State::Finished:
            case State::ExternalEnded:
                return false;
            case State::Active:
            case State::OutputEnded:
                break;
            }

            Engine::Frame local(target);
            auto buffer = pushBuffer(local, controller);
            if (!parent->pushNext(local)) {
                if (ignoreEnd)
                    return false;
                switch (state) {
                case State::BothEnded:
                case State::Finished:
                case State::ExternalEnded:
                    Q_ASSERT(false);
                case State::Active:
                    state = State::ExternalEnded;
                    break;
                case State::OutputEnded:
                    state = State::BothEnded;
                    break;
                }
                return false;
            }
            if (local.back().getType() == Engine::Reference::Type::Number) {
                double time = local.back().toReal();
                if (Range::compareStart(time, latestAdvanced) <= 0)
                    continue;
                advanceTo(local, buffer, time);
                continue;
            }

            double start = parent->getStart(local, local.back());
            if (Range::compareStart(start, latestReleased) < 0) {
                switch (state) {
                case State::BothEnded:
                case State::Finished:
                case State::ExternalEnded:
                    Q_ASSERT(false);
                case State::Active:
                    state = State::ExternalEnded;
                    break;
                case State::OutputEnded:
                    state = State::BothEnded;
                    break;
                }

                local.throwError("Incoming value before the latest released one");
                return false;
            }

            performInsert(local, buffer, local.back(), start);
            if (Range::compareStart(start, latestAdvanced) > 0) {
                advanceTo(local, buffer, start);
            }

            local.replaceWithBack(0);
            local.propagate(1);
            return true;
        }
    }

    double finish(Engine::Frame &frame, const Engine::Reference &controller)
    {
        switch (state) {
        case State::Finished:
            return latestReleased;
        case State::BothEnded:
        case State::OutputEnded:
            state = State::Finished;
            return latestReleased;
        case State::ExternalEnded:
        case State::Active:
            break;
        }

        if (!parent)
            return latestReleased;
        if (frame.inError()) {
            parent->endReady();
            return latestReleased;
        }

        {
            Engine::Frame local(frame);
            auto buffer = pushBuffer(local, controller);
            releaseAll(local, buffer);
        }
        parent->endReady();

        return latestReleased;
    }


    void convertBack(Engine::Frame &frame)
    {
        if (!parent)
            return;
        parent->convertFromLua(frame);
    }


    bool insertFromLua(Engine::Frame &frame, Engine::Table &buffer, const Engine::Reference &value)
    {
        switch (state) {
        case State::Active:
        case State::ExternalEnded:
            break;
        case State::OutputEnded:
        case State::BothEnded:
        case State::Finished:
            frame.throwError("Cannot add values to a completed buffer");
            return false;
        }

        if (!parent)
            return false;

        double start = parent->getStart(frame, value);
        if (Range::compareStart(start, latestReleased) < 0) {
            frame.throwError("Cannot add a value before the latest released one");
            return false;
        }
        performInsert(frame, buffer, value, start);
        if (Range::compareStart(start, latestAdvanced) > 0) {
            advanceTo(frame, buffer, start);
            return true;
        }
        return false;
    }

    void indexFromLua(Engine::Frame &frame, Engine::Table &buffer, std::int_fast64_t index) const
    {
        if (indices.empty() || index == 0) {
            frame.pushNil();
            return;
        }
        if (index < 0) {
            index = indices.size() + index;
        } else {
            index--;
        }
        if (index < 0 || static_cast<std::size_t>(index) >= indices.size()) {
            frame.pushNil();
            return;
        }

        frame.pushRaw(buffer, indices[index]);
    }

    bool assignFromLua(Engine::Frame &frame, Engine::Table &buffer, std::int_fast64_t index)
    {
        if (!parent)
            return false;
        indexFromLua(frame, buffer, index);
        if (frame.back().isNil())
            return false;
        return parent->bufferAssigned(frame, frame.back(), frame[2]);
    }

    void releaseFromLua(Engine::Frame &frame, Engine::Table &buffer, double time)
    {
        switch (state) {
        case State::Active:
        case State::ExternalEnded:
            break;
        case State::OutputEnded:
        case State::BothEnded:
        case State::Finished:
            return;
        }
        if (!FP::defined(time))
            return;
        if (FP::defined(latestReleased) && time <= latestReleased)
            return;
        releaseBefore(frame, buffer, time);
    }

    void releaseFromLua(Engine::Frame &frame, Engine::Table &buffer, const Engine::Reference &value)
    {
        switch (state) {
        case State::Active:
        case State::ExternalEnded:
            break;
        case State::OutputEnded:
        case State::BothEnded:
        case State::Finished:
            return;
        }
        if (!parent)
            return;

        double start = parent->getStart(frame, value);
        if (Range::compareStart(start, latestReleased) < 0) {
            frame.throwError("Cannot release a value before the latest released one");
            return;
        }

        /* Release any values before this one */
        if (!FP::defined(latestReleased) || start > latestReleased)
            releaseBefore(frame, buffer, start);

        /* Find the specific value and release it */
        for (auto index = indices.begin(), end = indices.end(); index != end; ++index) {
            Engine::Frame local(frame);
            local.pushRaw(buffer, *index);

            if (local.back().rawEqual(value)) {
                buffer.rawErase(*index);
                indices.erase(index);

                parent->outputReady(local, local.back());
                break;
            }

            double check = parent->getStart(local, local.back());
            if (Range::compareStart(check, start) > 0)
                break;
        }
    }

    void eraseFromLua(Engine::Frame &frame, Engine::Table &buffer, const Engine::Reference &value)
    {
        if (indices.empty())
            return;
        if (!parent)
            return;

        double start = parent->getStart(frame, value);
        auto endIndex = indices.end();
        auto index = std::lower_bound(indices.begin(), endIndex, start,
                                      BufferComparator(parent, frame, buffer));
        for (; index != endIndex; ++index) {
            Engine::Frame local(frame);
            local.pushRaw(buffer, *index);

            if (local.back().rawEqual(value)) {
                buffer.rawErase(*index);
                indices.erase(index);
                break;
            }

            double check = parent->getStart(local, local.back());
            if (Range::compareStart(check, start) > 0)
                break;
        }
    }

    void endInput(Engine::Frame &frame, Engine::Table &buffer, bool dump)
    {
        switch (state) {
        case State::Active:
            state = State::ExternalEnded;
            break;
        case State::OutputEnded:
            state = State::BothEnded;
            return;
        case State::ExternalEnded:
            break;
        case State::BothEnded:
        case State::Finished:
            return;
        }

        if (!dump)
            return;

        releaseAll(frame, buffer);
    }

    void endOutput(Engine::Frame &frame)
    {
        switch (state) {
        case State::Active:
            state = State::OutputEnded;
            break;
        case State::ExternalEnded:
            state = State::BothEnded;
            break;
        case State::OutputEnded:
        case State::BothEnded:
        case State::Finished:
            return;
        }

        if (!parent)
            return;

        parent->endReady();
    }

    inline std::int_fast64_t bufferLength() const
    { return indices.size(); }

    inline bool bufferEmpty() const
    { return indices.empty(); }

    inline void clear()
    { indices.clear(); }

    std::string toString(Engine::Frame &frame, Engine::Table &buffer) const
    {
        std::string result = "[";

        bool first = true;
        for (auto i : indices) {
            if (!first)
                result += ",";
            first = false;

            result += "(";

            Engine::Frame local(frame);
            local.pushRaw(buffer, i);
            auto item = local.back();

            result += item.toOutputString();

            result += ")";
        }

        result += "]";
        return result;
    }
};

class StreamBuffer::BaseController : public Engine::Data {
    void method_endInput(Engine::Entry &entry)
    {
        if (auto ctx = context.lock()) {
            bool dump = true;
            if (entry.size() > 1)
                dump = entry[1].toBoolean();

            auto buffer = pushBuffer(entry, entry[0]);
            ctx->endInput(entry, buffer, dump);
        }
    }

    void method_endOutput(Engine::Entry &entry)
    {
        if (auto ctx = context.lock()) {
            ctx->endOutput(entry);
        }
    }

    void method_end(Engine::Entry &entry)
    {
        if (auto ctx = context.lock()) {
            bool dump = true;
            if (entry.size() > 1)
                dump = entry[1].toBoolean();

            auto buffer = pushBuffer(entry, entry[0]);
            ctx->endInput(entry, buffer, dump);
            ctx->endOutput(entry);
        }
    }

    void method_clear(Engine::Entry &entry)
    {
        if (auto ctx = context.lock()) {
            if (ctx->bufferEmpty())
                return;
            ctx->clear();
        }

        auto mt = Engine::Table(entry[0]).pushMetatable(entry);
        Engine::Assign assign(entry, mt, "buffer");
        assign.pushTable();
    }

    void method_release(Engine::Entry &entry)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid arguments");
            return;
        }

        if (entry[1].getType() == Engine::Reference::Type::Number) {
            double time = entry[1].toReal();
            if (!FP::defined(time))
                return;
            if (auto ctx = context.lock()) {
                auto buffer = pushBuffer(entry, entry[0]);
                ctx->releaseFromLua(entry, buffer, time);
            }
            return;
        }

        if (auto ctx = context.lock()) {
            auto buffer = pushBuffer(entry, entry[0]);
            ctx->releaseFromLua(entry, buffer, entry[1]);
        }
    }

    void method_erase(Engine::Entry &entry)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid arguments");
            return;
        }

        if (auto ctx = context.lock()) {
            auto buffer = pushBuffer(entry, entry[0]);
            ctx->eraseFromLua(entry, buffer, entry[1]);
        }
    }

    void meta_tostring(Engine::Entry &entry)
    {
        if (auto ctx = context.lock()) {
            auto buffer = pushBuffer(entry, entry[0]);
            entry.push(ctx->toString(entry, buffer));
            entry.replaceWithBack(0);
            entry.propagate(1);
            return;
        }
        entry.clear();
        entry.push("DELETED_BUFFER");
        entry.propagate(1);
    }

    void meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid index");
            return;
        }
        if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
            return;
        if (entry[1].getType() == Engine::Reference::Type::Number) {
            auto index = entry[1].toInteger();
            if (!INTEGER::defined(index) || index == 0) {
                entry.clear();
                entry.pushNil();
                entry.propagate(1);
                return;
            }
            if (auto ctx = context.lock()) {
                auto buffer = pushBuffer(entry, entry[0]);
                ctx->indexFromLua(entry, buffer, index);
                entry.replaceWithBack(0);
                entry.propagate(1);
                return;
            }
            return;
        }

        auto key = entry[1].toString();
        if (key == "autorelease") {
            if (auto ctx = context.lock()) {
                double autorelease = ctx->getAutorelease();
                if (!FP::defined(autorelease))
                    entry.pushNil();
                else
                    entry.push(autorelease);
                entry.replaceWithBack(0);
                entry.propagate(1);
            }
            return;
        }

        entry.clear();
        entry.pushNil();
        entry.propagate(1);
    }

    void meta_newindex(Engine::Entry &entry)
    {
        if (entry.size() < 3) {
            entry.throwError("Invalid assignment");
            return;
        }
        if (entry[1].getType() == Engine::Reference::Type::Number) {
            auto index = entry[1].toInteger();
            if (!INTEGER::defined(index) || index == 0) {
                entry.throwError("Invalid assignment");
                return;
            }
            if (auto ctx = context.lock()) {
                Engine::Frame local(entry);
                auto buffer = pushBuffer(local, entry[0]);
                if (ctx->assignFromLua(local, buffer, index))
                    return;
            }
            return;
        }

        auto key = entry[1].toString();
        if (key == "autorelease") {
            if (auto ctx = context.lock()) {
                double autorelease = entry[2].toReal();
                if (FP::defined(autorelease)) {
                    if (autorelease < 0.0) {
                        entry.throwError("Autorelease threshold cannot be negative");
                        return;
                    }
                    ctx->setAutorelease(autorelease);
                    double time = ctx->getAdvancedTime();
                    if (FP::defined(time)) {
                        auto buffer = pushBuffer(entry, entry[0]);
                        ctx->advanceTo(entry, buffer, time);
                    }
                } else {
                    ctx->setAutorelease(FP::undefined());
                }
            }
            return;
        }

        entry.throwError("Invalid assignment");
    }

    void meta_len(Engine::Entry &entry)
    {
        if (auto ctx = context.lock()) {
            entry.clear();
            entry.push(ctx->bufferLength());
            entry.propagate(1);
            return;
        }
    }

    void updateAdvanced(Engine::Frame &frame, const Engine::Reference &self)
    {

    }

protected:
    std::weak_ptr<Context> context;
public:
    explicit BaseController(const std::shared_ptr<Context> &context) : context(context)
    { }

    virtual ~BaseController() = default;


    void initialize(const Engine::Reference &self, Engine::Frame &frame) override
    {
        auto mt = Engine::Data::pushMetatable(frame);

        {
            Engine::Assign assign(frame, mt, "__call");
            pushMethod<BaseController>(assign, [](BaseController *value, Engine::Entry &entry) {
                value->meta_call(entry);
            });
        }
        {
            Engine::Assign assign(frame, mt, "__len");
            pushMethod<BaseController>(assign, &BaseController::meta_len);
        }
        {
            Engine::Assign assign(frame, mt, "__tostring");
            pushMethod<BaseController>(assign, &BaseController::meta_tostring);
        }
        {
            Engine::Assign assign(frame, mt, "__newindex");
            pushMethod<BaseController>(assign, &BaseController::meta_newindex);
        }

        {
            Engine::Assign assign(frame, mt, "__index");
            {
                auto methods = assign.pushTable();
                assignMethods(assign, methods);
            }
            pushMethod<BaseController>(assign, &BaseController::meta_index, 1);
        }

        {
            Engine::Assign assign(frame, mt, "buffer");
            assign.pushTable();
        }

        setMetatable(self, mt);
    }

    virtual void assignMethods(Engine::Frame &frame, Engine::Table &methods)
    {
        {
            Engine::Assign assign(frame, methods, "endInput");
            pushMethod<BaseController>(assign, &BaseController::method_endInput);
        }
        {
            Engine::Assign assign(frame, methods, "endOutput");
            pushMethod<BaseController>(assign, &BaseController::method_endOutput);
        }
        {
            Engine::Frame local(frame);
            pushMethod<BaseController>(local, &BaseController::method_end);
            auto m = local.back();

            {
                Engine::Assign assign(local, methods, "endBoth");
                assign.push(m);
            }
            {
                Engine::Assign assign(local, methods, "end");
                assign.push(m);
            }
            {
                Engine::Assign assign(local, methods, "finish");
                assign.push(m);
            }
        }
        {
            Engine::Assign assign(frame, methods, "clear");
            pushMethod<BaseController>(assign, &BaseController::method_clear);
        }
        {
            Engine::Assign assign(frame, methods, "add");
            pushMethod<BaseController>(assign, &BaseController::method_add);
        }
        {
            Engine::Assign assign(frame, methods, "erase");
            pushMethod<BaseController>(assign, &BaseController::method_erase);
        }
        {
            Engine::Frame local(frame);
            pushMethod<BaseController>(local, &BaseController::method_release);
            auto m = local.back();

            {
                Engine::Assign assign(local, methods, "release");
                assign.push(m);
            }
            {
                Engine::Assign assign(local, methods, "advance");
                assign.push(m);
            }
        }
    }

    virtual void meta_call(Engine::Entry &entry) = 0;

    void method_add(Engine::Entry &entry)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid addition");
            return;
        }
        entry.pop(entry.size() - 2);
        if (auto ctx = context.lock()) {
            ctx->convertBack(entry);
            auto buffer = pushBuffer(entry, entry[0]);
            if (ctx->insertFromLua(entry, buffer, entry[1]))
                updateAdvanced(entry, entry[0]);
        }
    }
};

class StreamBuffer::LuaController : public BaseController {
    void method_next(Engine::Entry &entry)
    {
        if (auto ctx = context.lock()) {
            if (ctx->processNext(entry, entry[0])) {
                entry.replaceWithBack(0);
                entry.propagate(1);
                return;
            }
        }

        entry.clear();
        entry.pushNil();
        entry.propagate(1);
    }

public:
    explicit LuaController(const std::shared_ptr<Context> &context) : BaseController(context)
    { }

    virtual ~LuaController() = default;

protected:
    void assignMethods(Engine::Frame &frame, Engine::Table &methods) override
    {
        BaseController::assignMethods(frame, methods);
        {
            Engine::Assign assign(frame, methods, "next");
            pushMethod<LuaController>(assign, &LuaController::method_next);
        }
    }

    void meta_call(Engine::Entry &entry) override
    { method_next(entry); }
};

class StreamBuffer::ExternalController : public BaseController {
public:
    explicit ExternalController(const std::shared_ptr<Context> &context) : BaseController(context)
    { }

    virtual ~ExternalController() = default;

protected:
    void meta_call(Engine::Entry &entry) override
    { method_add(entry); }
};

StreamBuffer::StreamBuffer() : context(std::make_shared<Context>(this))
{ }

StreamBuffer::~StreamBuffer()
{ context->decouple(); }

void StreamBuffer::pushLuaController(Engine::Frame &target)
{ target.pushData<LuaController>(context); }

void StreamBuffer::pushExternalController(Engine::Frame &target)
{ target.pushData<ExternalController>(context); }

bool StreamBuffer::pushExternal(Engine::Frame &target,
                                const Engine::Reference &controller,
                                bool ignoreEnd)
{ return context->processNext(target, controller, ignoreEnd); }

double StreamBuffer::finish(Engine::Frame &frame, const Engine::Reference &controller)
{ return context->finish(frame, controller); }

void StreamBuffer::convertFromLua(Engine::Frame &)
{ }

void StreamBuffer::advanceReady(double)
{ }

bool StreamBuffer::bufferAssigned(Engine::Frame &,
                                  const Engine::Reference &,
                                  const Engine::Reference &)
{ return false; }


StreamBuffer::Value::Value() = default;

StreamBuffer::Value::~Value() = default;

double StreamBuffer::Value::getStart(Engine::Frame &frame, const Engine::Reference &ref)
{
    auto value = ref.toData<Libs::SequenceValue>();
    if (!value)
        return FP::undefined();
    return value->getStart(frame, ref);
}

void StreamBuffer::Value::convertFromLua(Engine::Frame &frame)
{
    if (frame.back().toData<Libs::SequenceValue>())
        return;
    auto value = Libs::SequenceValue::extract(frame, frame.back());
    frame.pop();
    frame.pushData<Libs::SequenceValue>(std::move(value));
}

Data::SequenceValue StreamBuffer::Value::extract(Engine::Frame &frame, const Engine::Reference &ref)
{ return Libs::SequenceValue::extract(frame, ref); }


StreamBuffer::Segment::Segment() = default;

StreamBuffer::Segment::~Segment() = default;

double StreamBuffer::Segment::getStart(Engine::Frame &frame, const Engine::Reference &ref)
{
    auto value = ref.toData<Libs::SequenceSegment>();
    if (!value)
        return FP::undefined();
    return value->get().getStart();
}

void StreamBuffer::Segment::convertFromLua(Engine::Frame &frame)
{
    if (frame.back().toData<Lua::Libs::SequenceSegment>())
        return;
    auto value = Libs::SequenceSegment::extract(frame, frame.back());
    frame.pop();
    frame.pushData<Libs::SequenceSegment>(std::move(value));
}

Data::SequenceValue::Transfer StreamBuffer::Segment::extract(Engine::Frame &frame,
                                                             const Engine::Reference &ref,
                                                             const Data::SequenceName::Set &outputs,
                                                             bool requireExists)
{
    auto value = ref.toData<Libs::SequenceSegment>();
    if (!value)
        return {};
    auto &extract = value->get();

    Data::SequenceValue::Transfer result;
    for (const auto &add : outputs) {
        auto v = extract.getValue(add);
        if (requireExists && !v.exists())
            continue;
        result.emplace_back(add, Data::Variant::Root(v), extract.getStart(), extract.getEnd());
    }
    return result;
}


}
}