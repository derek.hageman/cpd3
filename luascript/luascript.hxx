/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3LUASCRIPT_HXX
#define CPD3LUASCRIPT_HXX

#include <QtCore/QtGlobal>

#if defined(cpd3luascript_EXPORTS)
#   define CPD3LUASCRIPT_EXPORT Q_DECL_EXPORT
#else
#   define CPD3LUASCRIPT_EXPORT Q_DECL_IMPORT
#endif

#endif
