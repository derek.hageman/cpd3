/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUASCRIPT_FANOUTCONTROLLER_HXX
#define CPD3LUASCRIPT_FANOUTCONTROLLER_HXX

#include "core/first.hxx"

#include <memory>
#include <vector>
#include <unordered_set>

#include "luascript.hxx"
#include "engine.hxx"
#include "datacore/stream.hxx"

namespace CPD3 {
namespace Lua {

/**
 * A controller to handle fanout and dispatch based on sequence names.
 * <br>
 * The general structure is that the controller manages creation of targets and routes
 * dispatched input to those targets.  This is done by calling a Lua function that
 * itself returns a closure.  The initial Lua function is called when a new target
 * is being created and the closure it returns are used by the target associated
 * with it to handle the actual fanned out data.
 * <br>
 * So the common case is that the Lua function will be called with an argument that is
 * a stream buffer of SequenceSegments.  The closure it returns is the handler that
 * processes a single segment.  The target itself then calls the closure whenever the
 * C++ end of the stream buffer produces a segment.
 */
class CPD3LUASCRIPT_EXPORT FanoutController {
    class Context;

    friend class FanoutController::Context;

    std::shared_ptr<FanoutController::Context> context;

    class Controller;

public:

    FanoutController();

    virtual ~FanoutController();

    FanoutController(const FanoutController &) = delete;

    FanoutController &operator=(const FanoutController &) = delete;

    FanoutController(FanoutController &&) = delete;

    FanoutController &operator=(FanoutController &&) = delete;


    /**
     * Push the Lua controller on to the target frame.
     *
     * @param target    the target frame
     */
    void pushController(Engine::Frame &target);


    /**
     * The base class for targets of the fanout controller.  A target is what the
     * controller routes data to.  Generally there are two classes of target: the
     * conventional data ones and the background ones for default station data.
     */
    class CPD3LUASCRIPT_EXPORT Target {
        friend class FanoutController::Context;

        std::size_t savedIndex;
    public:
        Target();

        virtual ~Target();

    protected:
        /**
         * Push a saved value on to the given frame.
         *
         * @param frame         the target frame
         * @param controller    the controller
         * @param index         the saved index
         */
        void pushSaved(Engine::Frame &frame,
                       const Engine::Reference &controller,
                       std::size_t index = 0) const;

        /**
         * Initialize the target for the closure call.
         *
         * @param arguments     the arguments to call with, propagate to the actual calling frame
         * @param controller    the controller being used
         * @param key           the key the target was initialized by
         * @param background    any background targets the key is associated with
         * @param context       a local context table used during the initialization
         * @return              true to actually call the initializer (e.x. false for background targets)
         */
        virtual bool initializeCall(Engine::Frame &arguments,
                                    const Engine::Reference &controller,
                                    const Engine::Reference &key,
                                    const std::vector<std::shared_ptr<Target>> &background,
                                    Engine::Table &context);

        /**
         * Process the saved values after the call.  If the call was made then the saved
         * frame contains everything it returned.  If it was NOT made, then the frame contains
         * the create value that would have been called (i.e. what the controller was set to).
         * <br>
         * After this function returns, all values on the frame are saved with the target
         * and can be accessed by pushSaved(Engine::Frame &, const Engine::Reference &,
         * std::size_t) const.
         *
         * @param saved         the frame containing the input and output saved values
         * @param controller    the controller being used
         * @param key           the key the target was initialized by
         * @param background    any background targets the key is associated with
         * @param context       a local context table used during the initialization
         */
        virtual void processSaved(Engine::Frame &saved,
                                  const Engine::Reference &controller,
                                  const Engine::Reference &key,
                                  const std::vector<std::shared_ptr<Target>> &background,
                                  Engine::Table &context);
    };

    /**
     * Get the dispatch targets for a given sequence name.
     *
     * @param frame             the active stack frame
     * @param controller        the controller being used
     * @param target            the target name to dispatch for
     * @return                  the targets the name hits
     */
    const std::vector<std::shared_ptr<Target>> &dispatch(Engine::Frame &frame,
                                                         const Engine::Reference &controller,
                                                         const Data::SequenceName &target);

    /**
     * Get all targets currently managed by the controller.
     *
     * @return                  all active targets
     */
    const std::unordered_set<std::shared_ptr<Target>> &allTargets() const;

protected:

    /**
     * Create a target.  This is called by the dispatch when new targets need to be created.
     * The type is either the value from the Lua key generator type result or set to true if the
     * target is for the default station and fanning it out, or false otherwise.
     *
     * @param type  the type either as returned by the Lua call or set if it is the default station
     * @return      a new target
     */
    virtual std::shared_ptr<Target> createTarget(const Engine::Output &type) = 0;

    /**
     * Called when the controller finishes creating a new dispatch for a name.
     *
     * @param frame         the active stack frame
     * @param controller    the controller being used
     * @param name          the name being dispatched
     * @param targets       the targets the name hits
     */
    virtual void addedDispatch(Engine::Frame &frame,
                               const Engine::Reference &controller,
                               const Data::SequenceName &name,
                               std::vector<std::shared_ptr<Target>> &targets);
};

}
}

#endif //CPD3LUASCRIPT_FANOUTCONTROLLER_HXX
