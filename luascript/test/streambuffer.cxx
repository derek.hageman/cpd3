/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "luascript/streambuffer.hxx"
#include "core/timeutils.hxx"
#include "core/qtcompat.hxx"
#include "core/range.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestBuffer : public StreamBuffer {
public:
    struct DataType : public Time::Bounds {
        int value;

        DataType(double start, double end, int value = 0) : Time::Bounds(start, end), value(value)
        { }
    };

    std::deque<DataType> input;
    std::vector<DataType> output;
    std::vector<double> outputAdvanced;
    bool outputEnded;

    TestBuffer() : outputAdvanced(), outputEnded(false)
    { }

    virtual ~TestBuffer() = default;

protected:
    bool pushNext(Engine::Frame &target) override
    {
        if (input.empty())
            return false;
        if (input.front().value == -1) {
            target.push(input.front().getStart());
            input.pop_front();
            return true;
        }

        auto result = target.pushTable();
        result.set("front", Engine::Output(input.front().getStart()));
        result.set("back", Engine::Output(input.front().getEnd()));
        result.set("value", Engine::Output(input.front().value));
        input.pop_front();
        return true;
    }

    double getStart(Engine::Frame &frame, const Engine::Reference &ref) override
    {
        Q_ASSERT(!ref.isNil());
        Engine::Table value(ref);
        Q_ASSERT(!value.get("front").isNil());
        Q_ASSERT(!value.get("back").isNil());
        Q_ASSERT(!value.get("value").isNil());
        return value.get("front").toReal();
    }

    void convertFromLua(Engine::Frame &frame) override
    {
        Q_ASSERT(!frame.back().isNil());
        Engine::Table value(frame.back());
        Q_ASSERT(!value.get("front").isNil());
        Q_ASSERT(!value.get("back").isNil());
        Q_ASSERT(!value.get("value").isNil());
    }

    void outputReady(Engine::Frame &frame, const Engine::Reference &ref) override
    {
        Q_ASSERT(!outputEnded);
        Q_ASSERT(!ref.isNil());
        Engine::Table value(ref);
        Q_ASSERT(outputAdvanced.empty() ||
                         Range::compareStart(value.get("front").toReal(), outputAdvanced.back()) >=
                                 0);
        Q_ASSERT(output.empty() ||
                         Range::compareStart(value.get("front").toReal(),
                                             output.back().getStart()) >= 0);
        output.emplace_back(value.get("front").toReal(), value.get("back").toReal(),
                            value.get("value").toInteger());
    }

    void advanceReady(double time) override
    {
        Q_ASSERT(!outputEnded);
        Q_ASSERT(outputAdvanced.empty() || Range::compareStart(time, outputAdvanced.back()) >= 0);
        outputAdvanced.push_back(time);
    }

    void endReady() override
    {
        Q_ASSERT(!outputEnded);
        outputEnded = true;
    }
};

class TestStreamBuffer : public QObject {
Q_OBJECT

private slots:

    void basic()
    {
        Engine e;
        Engine::Frame base(e);
        TestBuffer buffer;

        buffer.input.emplace_back(100, 200, 1);
        buffer.input.emplace_back(100, 200, 2);
        buffer.input.emplace_back(150, 200, -1);
        buffer.input.emplace_back(200, 300, 3);

        buffer.pushExternalController(base);
        auto controller = base.back();

        {
            {
                Engine::Assign assign(base, e.global(), "data");
                QVERIFY(buffer.pushExternal(assign, controller));
            }
            Engine::Call c(base);
            c.pushChunk("return data.front, data.back, data.value", false);
            QVERIFY(c.execute(3));
            QCOMPARE(c[0].toReal(), 100.0);
            QCOMPARE(c[1].toReal(), 200.0);
            QCOMPARE(c[2].toReal(), 1.0);
            QVERIFY(!buffer.outputEnded);
            QCOMPARE((int) buffer.output.size(), 0);
            QCOMPARE((int) buffer.outputAdvanced.size(), 1);
        }
        {
            {
                Engine::Assign assign(base, e.global(), "data");
                QVERIFY(buffer.pushExternal(assign, controller));
            }
            Engine::Call c(base);
            c.pushChunk("return data.front, data.back, data.value", false);
            QVERIFY(c.execute(3));
            QCOMPARE(c[0].toReal(), 100.0);
            QCOMPARE(c[1].toReal(), 200.0);
            QCOMPARE(c[2].toReal(), 2.0);
            QVERIFY(!buffer.outputEnded);
            QCOMPARE((int) buffer.output.size(), 0);
            QCOMPARE((int) buffer.outputAdvanced.size(), 1);
        }
        {
            {
                Engine::Assign assign(base, e.global(), "data");
                QVERIFY(buffer.pushExternal(assign, controller));
            }
            Engine::Call c(base);
            c.pushChunk("return data.front, data.back, data.value", false);
            QVERIFY(c.execute(3));
            QCOMPARE(c[0].toReal(), 200.0);
            QCOMPARE(c[1].toReal(), 300.0);
            QCOMPARE(c[2].toReal(), 3.0);
            QVERIFY(!buffer.outputEnded);
            QCOMPARE((int) buffer.output.size(), 2);
            QCOMPARE((int) buffer.outputAdvanced.size(), 3);
        }

        QVERIFY(!buffer.pushExternal(base, controller));
        buffer.finish(base, controller);
        QVERIFY(buffer.outputEnded);

        QCOMPARE(buffer.outputAdvanced, (std::vector<double>{100.0, 150.0, 200.0}));
        QCOMPARE(buffer.output, (std::vector<TestBuffer::DataType>{{100.0, 200.0, 1},
                                                                   {100.0, 200.0, 2},
                                                                   {200.0, 300.0, 3},}));
    }

    void bufferOps()
    {
        Engine e;
        Engine::Frame base(e);
        TestBuffer buffer;

        buffer.input.emplace_back(100, 200, 1);
        buffer.input.emplace_back(100, 200, 2);
        buffer.input.emplace_back(300, 400, 3);

        buffer.pushExternalController(base);
        auto controller = base.back();
        {
            Engine::Assign assign(base, e.global(), "control");
            assign.push(controller);
        }

        {
            {
                Engine::Assign assign(base, e.global(), "data");
                QVERIFY(buffer.pushExternal(assign, controller));
            }
            Engine::Call c(base);
            c.pushChunk(R"EOF(

control.autorelease = nil;
control:add({front=150, back=250, value=30});

local v1 = control[1].value;
local v2 = control[2].value;
local v3 = control[-1].value;

control.autorelease = 0;
control.autorelease = nil;

return v1, v2, v3;
)EOF", false);
            QVERIFY(c.execute(3));
            QCOMPARE(c[0].toReal(), 1.0);
            QCOMPARE(c[1].toReal(), 30.0);
            QCOMPARE(c[2].toReal(), 30.0);
            QVERIFY(!buffer.outputEnded);
            QCOMPARE((int) buffer.output.size(), 1);
            QCOMPARE((int) buffer.outputAdvanced.size(), 2);
        }
        {
            Engine::Call c(base);
            c.pushChunk(R"EOF(

control:add({front=450, back=550, value=40});

return #control;
)EOF", false);
            QVERIFY(c.execute(1));
            QCOMPARE((int) c[0].toInteger(), 2);
            QVERIFY(!buffer.outputEnded);
            QCOMPARE((int) buffer.output.size(), 1);
            QCOMPARE((int) buffer.outputAdvanced.size(), 2);
        }
        {
            Engine::Call c(base);
            c.pushChunk(R"EOF(

local added = {front=450, back=550, value=50};
control:add(added);
control:release(175);
control:release(added);
control:endInput(false);

control.autorelease = 1000;

local temp = {front=550, back=650, value=99};
control(temp);
control:erase(temp);

control:add({front=450, back=550, value=60});

)EOF", false);
            QVERIFY(c.execute());
            QVERIFY(!buffer.outputEnded);
            QCOMPARE((int) buffer.output.size(), 3);
            QCOMPARE((int) buffer.outputAdvanced.size(), 6);
        }

        QVERIFY(!buffer.pushExternal(base, controller));
        {
            Engine::Call c(base);
            c.pushChunk("control:finish()", false);
            QVERIFY(c.execute());
            QVERIFY(buffer.outputEnded);
            QCOMPARE((int) buffer.output.size(), 5);
            QCOMPARE((int) buffer.outputAdvanced.size(), 6);
        }

        buffer.finish(base, controller);
        QVERIFY(buffer.outputEnded);
        QCOMPARE((int) buffer.output.size(), 5);
        QCOMPARE((int) buffer.outputAdvanced.size(), 6);

        QCOMPARE(buffer.outputAdvanced,
                 (std::vector<double>{100.0, 150.0, 175.0, 450.0, 450.0, 450.0}));
        QCOMPARE(buffer.output, (std::vector<TestBuffer::DataType>{{100.0, 200.0, 1},
                                                                   {150.0, 250.0, 30},
                                                                   {450.0, 550.0, 50},
                                                                   {450.0, 550.0, 40},
                                                                   {450.0, 550.0, 60}}));
    }

    void luaLoop()
    {
        Engine e;
        Engine::Frame base(e);
        TestBuffer buffer;

        buffer.input.emplace_back(100, 200, 1);
        buffer.input.emplace_back(100, 200, 2);
        buffer.input.emplace_back(150, 200, -1);
        buffer.input.emplace_back(200, 300, 3);

        buffer.pushLuaController(base);
        auto controller = base.back();
        {
            Engine::Assign assign(base, e.global(), "data");
            assign.push(controller);
        }

        {
            Engine::Call c(base);
            c.pushChunk(R"EOF(
index = 1
for val in data do
    if index == 1 then
        if val.front ~= 100 then error(); end
        if val.back ~= 200 then error(); end
        if val.value ~= 1 then error(); end
        val.value = 99;
    elseif index == 2 then
        if val.front ~= 100 then error(); end
        if val.back ~= 200 then error(); end
        if val.value ~= 2 then error(); end
    elseif index == 3 then
        if val.front ~= 200 then error(); end
        if val.back ~= 300 then error(); end
        if val.value ~= 3 then error(); end
    else
        error();
    end
    index = index + 1;
end
if index ~= 4 then error(); end
)EOF", false);
            QVERIFY(c.execute());
            buffer.finish(c, controller);
        }

        QVERIFY(buffer.outputEnded);
        QCOMPARE((int) buffer.output.size(), 3);
        QCOMPARE((int) buffer.outputAdvanced.size(), 3);

        QCOMPARE(buffer.outputAdvanced, (std::vector<double>{100.0, 150.0, 200.0}));
        QCOMPARE(buffer.output, (std::vector<TestBuffer::DataType>{{100.0, 200.0, 99},
                                                                   {100.0, 200.0, 2},
                                                                   {200.0, 300.0, 3}}));
    }

    void luaGenerator()
    {
        Engine e;
        Engine::Frame base(e);
        TestBuffer buffer;

        buffer.pushExternalController(base);
        auto controller = base.back();
        {
            Engine::Assign assign(base, e.global(), "control");
            assign.push(controller);
        }

        {
            Engine::Call c(base);
            c.pushChunk(R"EOF(
control({front=100, back=200, value=1});
control({front=100, back=200, value=2});
control({front=200, back=300, value=3});
control:finish();
)EOF", false);
            QVERIFY(c.execute());
            buffer.finish(c, controller);
        }

        QVERIFY(buffer.outputEnded);
        QCOMPARE((int) buffer.output.size(), 3);
        QCOMPARE((int) buffer.outputAdvanced.size(), 2);

        QCOMPARE(buffer.outputAdvanced, (std::vector<double>{100.0, 200.0}));
        QCOMPARE(buffer.output, (std::vector<TestBuffer::DataType>{{100.0, 200.0, 1},
                                                                   {100.0, 200.0, 2},
                                                                   {200.0, 300.0, 3}}));
    }

    void autoreleaseTimeAdjust()
    {
        Engine e;
        Engine::Frame base(e);
        TestBuffer buffer;

        buffer.input.emplace_back(100, 200, 1);
        buffer.input.emplace_back(100, 200, 2);
        buffer.input.emplace_back(150, 200, -1);
        buffer.input.emplace_back(200, 300, 3);

        buffer.pushLuaController(base);
        auto controller = base.back();
        {
            Engine::Assign assign(base, e.global(), "data");
            assign.push(controller);
        }

        {
            Engine::Call c(base);
            c.pushChunk(R"EOF(
data.autorelease = 10;
for val in data do
    val.front = val.front - 10;
    val.back = val.back + 20.0;
end
)EOF", false);
            QVERIFY(c.execute());
            buffer.finish(c, controller);
        }

        QVERIFY(buffer.outputEnded);
        QCOMPARE((int) buffer.output.size(), 3);
        QCOMPARE((int) buffer.outputAdvanced.size(), 3);

        QCOMPARE(buffer.outputAdvanced, (std::vector<double>{90.0, 140.0, 190.0}));
        QCOMPARE(buffer.output, (std::vector<TestBuffer::DataType>{{90.0,  220.0, 1},
                                                                   {90.0,  220.0, 2},
                                                                   {190.0, 320.0, 3}}));
    }
};

QTEST_APPLESS_MAIN(TestStreamBuffer)

#include "streambuffer.moc"
