/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "luascript/engine.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestEngineBasic : public QObject {
Q_OBJECT

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void empty()
    {
        {
            Engine e;
        }
        {
            Engine e;
            QVERIFY(!e.inError());
            Engine::Frame f(e);
            QVERIFY(!e.inError());
            QVERIFY(f.empty());
        }
    }

    void error()
    {
        Engine e;
        {
            Engine::Frame f1(e);
            {
                Engine::Frame f2(f1);
                QVERIFY(!e.inError());
                f2.throwError("stuff");
            }
        }
        QVERIFY(e.inError());
        e.clearError();
        QVERIFY(!e.inError());
        e.throwError("other");
        QVERIFY(e.inError());
        QVERIFY(!e.errorDescription().empty());
    }

    void frameSimple()
    {
        using DataType = Engine::PersistentData;
        DataType pd;

        Engine e;
        Engine::Frame f(e);

        f.pushNil();
        QCOMPARE((int) f.size(), 1);
        QVERIFY(f.back().isNil());
        QVERIFY(f.front().get().isNil());
        f.pop();
        QVERIFY(f.empty());

        f.push(1.0);
        f.push(2.0);
        QCOMPARE((int) f.size(), 2);
        QVERIFY(!f.back().isNil());
        QCOMPARE(f.front().get().toNumber(), 1.0);
        QCOMPARE((int) f.back().toInteger(), 2);
        f.pop(2);
        QVERIFY(f.empty());

        f.push(true);
        f.push(3);
        f.push(false);
        QCOMPARE((int) f.size(), 3);
        QVERIFY(f.front().toBoolean());
        QVERIFY(!f.back().toBoolean());
        QVERIFY(f[1].toBoolean());
        QVERIFY(!f[2].get().toBoolean());
        f.replaceWithBack(1);
        QCOMPARE((int) f.size(), 2);
        QVERIFY(f.front().toBoolean());
        QVERIFY(!f.back().toBoolean());
        f.pop();
        f.pop();

        f.push("str1");
        f.push(false);
        f.push(std::string("string2"));
        QCOMPARE((int) f.size(), 3);
        QCOMPARE(f.front().get().toString(), std::string("str1"));
        QCOMPARE(f.back().toString(), std::string("string2"));
        QCOMPARE(f.fromBack(0).toString(), std::string("string2"));
        f.moveBackTo(1);
        QVERIFY(!f.back().toBoolean());
        f.pop();
        QCOMPARE(f.back().toString(), std::string("string2"));
        f.pop(2);

        f.push(&pd);
        QCOMPARE((int) f.size(), 1);
        QCOMPARE(f.front().toData<DataType>(), &pd);
        QCOMPARE(f.back().get().toData<DataType>(), &pd);
        f.pop();

        f.push(Engine::Output(2.0));
        f.push(f.back());
        QCOMPARE((int) f.size(), 2);
        QCOMPARE(f.front().toNumber(), 2.0);
        QCOMPARE(f.back().toNumber(), 2.0);
        f.pop();
        QCOMPARE(f.front().toNumber(), 2.0);
        QCOMPARE(f.back().toNumber(), 2.0);
        {
            auto r = f.front();
            f.push(Engine::Output());
            f.push(Engine::Output("asdf"));
            f.push(Engine::Output(false));
            f.push(&pd);
            f.push(f.back().get());
            f.push(r);
        }
        QCOMPARE((int) f.size(), 7);
        QCOMPARE(f.back().toNumber(), 2.0);
        f.pop();
        QCOMPARE(f.back().toData<DataType>(), &pd);
        QCOMPARE(f.fromBack(1).toData<DataType>(), &pd);
        f.pop(2);
        QVERIFY(!f.back().toBoolean());
        QVERIFY(!f[3].get().toBoolean());
        f.pop();
        f.erase(0);
        QCOMPARE((int) f.size(), 2);
        QCOMPARE(f.back().toString(), std::string("asdf"));
        f.pop();
        QVERIFY(f.back().isNil());
        QCOMPARE((int) f.size(), 1);
    }

    void frameNest()
    {
        Engine e;

        {
            Engine::Frame f1(e);
            f1.push(1);
            {
                Engine::Frame f2(f1);
                f2.push(2);
                f2.push(3);
                QCOMPARE((int) f2.size(), 2);
            }
            QCOMPARE((int) f1.size(), 1);
        }
        {
            Engine::Frame f1(e);
            f1.push(1);
            {
                Engine::Frame f2(f1);
                f2.push(2);
                f2.push(3);
                QCOMPARE((int) f2.size(), 2);
                f2.propagate();
            }
            QCOMPARE((int) f1.size(), 3);
        }
        {
            Engine::Frame f1(e);
            f1.push(1);
            f1.push(2);
            {
                Engine::Frame f2(f1);
                f2.push(3);
                f2.push(4);
                f2.push(5);
                QCOMPARE((int) f2.size(), 3);
                f2.propagate(2);
            }
            QCOMPARE((int) f1.size(), 4);
        }
    }

    void tables()
    {
        Engine e;
        Engine::Frame f(e);

        auto t = f.pushTable();
        QVERIFY(f.front() == f.back());

        t.set(Engine::Output("key1"), Engine::Output("value1"));
        QCOMPARE(t.get(Engine::Output("key1")).toString(), std::string("value1"));
        {
            Engine::Frame f2(f);
            f2.push(1.5);
            t.set(Engine::Output("key1"), f2.back());
        }
        QCOMPARE(t.get("key1").toNumber(), 1.5);

        {
            f.push("key2");
            auto k2 = f.back();
            t.set(k2, Engine::Output("value2"));
            QCOMPARE(t.get("key1").toNumber(), 1.5);
            {
                Engine::Frame f2(f);
                f2.push(2.5);
                t.set(k2, f2.back());
            }
            QCOMPARE(t.get(k2).toNumber(), 2.5);
            QCOMPARE(t.get("key1").toNumber(), 1.5);
            f.pop();
            QCOMPARE(t.get("key2").toNumber(), 2.5);
        }
        QVERIFY(!t.get("key1").isNil());
        t.erase(Engine::Output("key1"));
        QVERIFY(t.get("key1").isNil());

        t.set(std::string("key1"), Engine::Output("value3"));
        QCOMPARE(t.get(std::string("key1")).toString(), std::string("value3"));
        {
            Engine::Frame f2(f);
            f2.push(3.5);
            t.set(std::string("key1"), f2.back());
        }
        QCOMPARE(t.get("key1").toNumber(), 3.5);
        {
            Engine::Frame f2(f);
            f2.push("key1");
            f2.pushRaw(t, f2.back());
            QCOMPARE(f2.back().toNumber(), 3.5);
        }
        QVERIFY(!t.get("key1").isNil());
        t.erase(std::string("key1"));
        QVERIFY(t.get("key1").isNil());

        t.set("key1", Engine::Output("value4"));
        QCOMPARE(t.get("key1").toString(), std::string("value4"));
        {
            Engine::Frame f2(f);
            f2.push(4.5);
            t.set("key1", f2.back());
        }
        QCOMPARE(t.get(std::string("key1")).toNumber(), 4.5);
        QVERIFY(!t.get(std::string("key1")).isNil());
        t.erase("key1");
        QVERIFY(t.get(std::string("key1")).isNil());
        t.set("key1", Engine::Output(4.5));

        t.set(1, Engine::Output("value5"));
        QCOMPARE(t.get(1).toString(), std::string("value5"));
        QCOMPARE(t.get(std::string("key1")).toNumber(), 4.5);
        {
            Engine::Frame f2(f);
            f2.push(5.5);
            t.set(1, f2.back());
        }
        QCOMPARE(t.get(1).toNumber(), 5.5);
        QVERIFY(!t.get(1).isNil());
        t.erase(1);
        QVERIFY(t.get(1).isNil());

        t.set(1, Engine::Output("zzzz"));
        QVERIFY(!t.get(1).isNil());
        t.rawErase(1);
        QVERIFY(t.get(1).isNil());
        t.set(1, Engine::Output("zzzz"));

        {
            Engine::Frame f2(f);
            f2.push(6.5);
            t.rawSet(1, f2.back());
        }
        QCOMPARE(t.rawGet(1).toNumber(), 6.5);
        {
            Engine::Frame f2(f);
            f2.push("key1");
            f2.push(8.5);
            t.rawSet(f2.front(), f2.back());
            QCOMPARE(t.rawGet(f2.front()).toNumber(), 8.5);
        }
        QCOMPARE(t.get(std::string("key1")).toNumber(), 8.5);

        QVERIFY(!t.get(std::string("key1")).isNil());
        {
            Engine::Frame f2(f);
            f2.push("key1");
            t.rawErase(f2.back());
        }
        QVERIFY(t.get(std::string("key1")).isNil());

        {
            Engine::Assign a(f, t, Engine::Output("key1"));
            a.push(9.0);
        }
        QCOMPARE(t.get("key1").toNumber(), 9.0);

        {
            Engine::Assign a(f, t, std::string("key1"));
            a.push(10.0);
        }
        QCOMPARE(t.get("key1").toNumber(), 10.0);

        {
            Engine::Assign a(f, t, "key1");
            a.push(11.0);
        }
        QCOMPARE(t.get(std::string("key1")).toNumber(), 11.0);

        {
            Engine::Frame f2(f);
            f2.push("key1");
            Engine::Assign a(f2, t, f2.back());
            a.push(12.0);
        }
        QCOMPARE(t.get("key1").toNumber(), 12.0);

        {
            Engine::Assign a(f, t, 1);
            a.push(12.0);
        }
        QCOMPARE(t.get(1).toNumber(), 12.0);

        QVERIFY(t.get("empty").isNil());
        auto mt = t.pushMetatable(f);
        {
            Engine::Frame f2(f);
            auto i = f2.pushTable();
            i.set("empty", Engine::Output("first"));
            mt.set("__index", i);
        }
        QVERIFY(!t.get("empty").isNil());
        QCOMPARE(t.get("empty").toString(), std::string("first"));
        f.pop();
        mt = t.pushMetatable(f);
        {
            Engine::Frame f2(f);
            auto i = f2.pushTable();
            i.set("empty", Engine::Output("second"));
            mt.set("__index", i);
        }
        f.pop();
        QCOMPARE(t.get("empty").toString(), std::string("second"));
        {
            Engine::Frame f2(f);
            auto i = f2.pushTable();
            i.set(99, Engine::Output("third"));
            t.setMetamethod("__index", i);
        }
        QCOMPARE(t.get(99).toString(), std::string("third"));
        QVERIFY(t.rawGet(99).isNil());

        {
            Engine::Frame f2(f);
            auto i = f2.pushTable();
            t.setMetatable(i);
        }
        QVERIFY(t.get("empty").isNil());
        QVERIFY(t.get(99).isNil());

        t.clearMetatable();
        QVERIFY(t.get("empty").isNil());
        QVERIFY(t.get(99).isNil());
    }

    void iterator()
    {
        Engine e;
        Engine::Frame f(e);
        auto t = f.pushTable();

        {
            Engine::Append a(f, t);
            a.push("first");
            a.push("second");
            a.push("third");
        }
        {
            Engine::Append a(f, t, true);
            a.push("fourth");
            a.push("fifth");
            a.push("sixth");
        }
        QCOMPARE((int) t.length(), 6);

        QCOMPARE(t.get(2).toString(), std::string("second"));
        QCOMPARE(t.get(4).toString(), std::string("fourth"));

        {
            Engine::Iterator it(f, t);

            QVERIFY(it.next());
            QCOMPARE((int) it.key().toInteger(), 1);
            QCOMPARE(it.value().toString(), std::string("first"));

            QVERIFY(it.next());
            QCOMPARE((int) it.key().toInteger(), 2);
            QCOMPARE(it.value().toString(), std::string("second"));

            QVERIFY(it.next());
            QCOMPARE((int) it.key().toInteger(), 3);
            QCOMPARE(it.value().toString(), std::string("third"));

            QVERIFY(it.next());
            QCOMPARE((int) it.key().toInteger(), 4);
            QCOMPARE(it.value().toString(), std::string("fourth"));

            QVERIFY(it.next());
            QCOMPARE((int) it.key().toInteger(), 5);
            QCOMPARE(it.value().toString(), std::string("fifth"));

            QVERIFY(it.next());
            QCOMPARE((int) it.key().toInteger(), 6);
            QCOMPARE(it.value().toString(), std::string("sixth"));

            QVERIFY(!it.next());
        }

        class Invalid : public Engine::PersistentData {
        public:
            Invalid() = default;

            virtual ~Invalid() = default;
        };
        Invalid invalid;
        invalid.ready(f);
        //f.push(&invalid);
        f.push(1.0);
        {
            Engine::Iterator it(f, f.back());
            QVERIFY(!it.next());
        }
    }

    void managedData()
    {
        static int init1 = 0;
        static int dtor1 = 0;
        init1 = 0;
        dtor1 = 0;
        class MD1 : public Engine::Data {
        public:
            MD1() = default;

            virtual ~MD1()
            { dtor1++; }

        protected:
            virtual void initialize(const Engine::Reference &, Engine::Frame &)
            { init1++; }
        };

        {
            Engine e;
            Engine::Frame f(e);

            f.pushData<MD1>();
            QCOMPARE((int) f.size(), 1);
            QCOMPARE(init1, 1);

            f.pushData<MD1>();
            QCOMPARE((int) f.size(), 2);
            QCOMPARE(init1, 2);

            QVERIFY(f.back() != f.front());
        }
        QCOMPARE(dtor1, 2);


        static int init2 = 0;
        static int dtor2 = 0;
        init2 = 0;
        dtor2 = 0;
        class MD2 : public Engine::Data {
        public:
            int value;

            MD2() : value()
            { }

            virtual ~MD2()
            { dtor2++; }

            void fromLUA(Engine::Entry &entry)
            {
                int old = value;
                if (entry.size() != 2) {
                    qWarning() << "Invalid entry";
                }
                value += (int) entry.back().toInteger();
                entry.clear();
                entry.push(old);
                entry.propagate(1);
            }

        protected:
            virtual void initialize(const Engine::Reference &self, Engine::Frame &frame)
            {
                init2++;

                auto mt = pushMetatable(frame);
                {
                    auto idx = frame.pushTable();
                    pushMethod<MD2>(frame, &MD2::fromLUA);
                    idx.set("fromLUA", frame.back());
                    mt.set("__index", idx);
                }

                setMetatable(self, mt);
            }
        };

        {
            Engine e;
            Engine::Frame f(e);

            MD2 *ptr = f.pushData<MD2>();
            QCOMPARE((int) f.size(), 1);
            QCOMPARE(init2, 1);
            auto md = f.back();

            f.push(md, "fromLUA");

            {
                Engine::Call c(f, f.back());
                c.push(md);
                c.push(2);
                QVERIFY(c.execute());
            }
            QCOMPARE(ptr->value, 2);
            {
                Engine::Call c(f, f.back());
                c.push(md);
                c.push(3);
                QVERIFY(c.execute(1));
                QCOMPARE((int) c.size(), 1);
                QCOMPARE((int) c.back().toInteger(), 2);
            }
            QCOMPARE(ptr->value, 5);
        }
        QCOMPARE(dtor2, 1);
    }
};

QTEST_APPLESS_MAIN(TestEngineBasic)

#include "engine_basic.moc"
