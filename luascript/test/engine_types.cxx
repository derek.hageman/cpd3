/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "luascript/engine.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestEngineTypes : public QObject {
Q_OBJECT

private slots:

    void outputNil()
    {
        Engine::Output o1;
        QCOMPARE(o1.getType(), Engine::Output::Type::Nil);
        QVERIFY(o1.isNil());
        QVERIFY(!o1.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o1.toInteger()));
        QVERIFY(!FP::defined(o1.toReal()));
        QVERIFY(o1.toString().empty());
        QVERIFY(!o1.toBoolean());

        Engine::Output o2(o1);
        Engine::Output o3(std::move(o1));
        QCOMPARE(o2.getType(), Engine::Output::Type::Nil);
        QVERIFY(o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o2.toInteger()));
        QVERIFY(!FP::defined(o2.toReal()));
        QVERIFY(o2.toString().empty());
        QVERIFY(!o2.toBoolean());
        QCOMPARE(o3.getType(), Engine::Output::Type::Nil);
        QVERIFY(o3.isNil());
        QVERIFY(!o3.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o3.toInteger()));
        QVERIFY(!FP::defined(o3.toReal()));
        QVERIFY(o3.toString().empty());
        QVERIFY(!o3.toBoolean());

        o2 = o3;
        QCOMPARE(o2.getType(), Engine::Output::Type::Nil);
        QVERIFY(o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o2.toInteger()));
        QVERIFY(!FP::defined(o2.toReal()));
        QVERIFY(o2.toString().empty());
        QVERIFY(!o2.toBoolean());

        o2 = std::move(o3);
        QCOMPARE(o2.getType(), Engine::Output::Type::Nil);
        QVERIFY(o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o2.toInteger()));
        QVERIFY(!FP::defined(o2.toReal()));
        QVERIFY(o2.toString().empty());
        QVERIFY(!o2.toBoolean());

        o3 = o2;
        o2 = Engine::Output(1.0);
        o2 = std::move(o3);
        QCOMPARE(o2.getType(), Engine::Output::Type::Nil);
        QVERIFY(o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o2.toInteger()));
        QVERIFY(!FP::defined(o2.toReal()));
        QVERIFY(o2.toString().empty());
        QVERIFY(!o2.toBoolean());
    }

    void outputNumber()
    {
        Engine::Output o1(3.0);
        QCOMPARE(o1.getType(), Engine::Output::Type::Number);
        QVERIFY(!o1.isNil());
        QVERIFY(!o1.toData<Engine::BaseData>());
        QCOMPARE((int) o1.toInteger(), 3);
        QCOMPARE(o1.toReal(), 3.0);
        QVERIFY(o1.toString().empty());
        QVERIFY(o1.toBoolean());

        Engine::Output o2(o1);
        Engine::Output o3(std::move(o1));
        QCOMPARE(o2.getType(), Engine::Output::Type::Number);
        QVERIFY(!o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QCOMPARE((int) o2.toInteger(), 3);
        QCOMPARE(o2.toReal(), 3.0);
        QVERIFY(o2.toString().empty());
        QVERIFY(o2.toBoolean());
        QCOMPARE(o3.getType(), Engine::Output::Type::Number);
        QVERIFY(!o3.isNil());
        QVERIFY(!o3.toData<Engine::BaseData>());
        QCOMPARE((int) o3.toInteger(), 3);
        QCOMPARE(o3.toReal(), 3.0);
        QVERIFY(o3.toString().empty());
        QVERIFY(o3.toBoolean());

        o2 = o3;
        QCOMPARE(o2.getType(), Engine::Output::Type::Number);
        QVERIFY(!o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QCOMPARE((int) o2.toInteger(), 3);
        QCOMPARE(o2.toReal(), 3.0);
        QVERIFY(o2.toString().empty());
        QVERIFY(o2.toBoolean());

        o2 = std::move(o3);
        QCOMPARE(o2.getType(), Engine::Output::Type::Number);
        QVERIFY(!o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QCOMPARE((int) o2.toInteger(), 3);
        QCOMPARE(o2.toReal(), 3.0);
        QVERIFY(o2.toString().empty());
        QVERIFY(o2.toBoolean());

        o3 = o2;
        o2 = Engine::Output(1.0);
        o2 = std::move(o3);
        QCOMPARE(o2.getType(), Engine::Output::Type::Number);
        QVERIFY(!o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QCOMPARE((int) o2.toInteger(), 3);
        QCOMPARE(o2.toReal(), 3.0);
        QVERIFY(o2.toString().empty());
        QVERIFY(o2.toBoolean());
    }

    void outputString()
    {
        Engine::Output o1("Abcd");
        QCOMPARE(o1.getType(), Engine::Output::Type::String);
        QVERIFY(!o1.isNil());
        QVERIFY(!o1.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o1.toInteger()));
        QVERIFY(!FP::defined(o1.toReal()));
        QCOMPARE(o1.toString(), std::string("Abcd"));
        QVERIFY(o1.toBoolean());

        Engine::Output o2(o1);
        Engine::Output o3(std::move(o1));
        QCOMPARE(o2.getType(), Engine::Output::Type::String);
        QVERIFY(!o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o2.toInteger()));
        QVERIFY(!FP::defined(o2.toReal()));
        QCOMPARE(o2.toString(), std::string("Abcd"));
        QVERIFY(o2.toBoolean());
        QCOMPARE(o3.getType(), Engine::Output::Type::String);
        QVERIFY(!o3.isNil());
        QVERIFY(!o3.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o3.toInteger()));
        QVERIFY(!FP::defined(o3.toReal()));
        QCOMPARE(o3.toString(), std::string("Abcd"));
        QVERIFY(o3.toBoolean());

        o2 = o3;
        QCOMPARE(o2.getType(), Engine::Output::Type::String);
        QVERIFY(!o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o2.toInteger()));
        QVERIFY(!FP::defined(o2.toReal()));
        QCOMPARE(o2.toString(), std::string("Abcd"));
        QVERIFY(o2.toBoolean());

        o2 = std::move(o3);
        QCOMPARE(o2.getType(), Engine::Output::Type::String);
        QVERIFY(!o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o2.toInteger()));
        QVERIFY(!FP::defined(o2.toReal()));
        QCOMPARE(o2.toString(), std::string("Abcd"));
        QVERIFY(o2.toBoolean());

        o3 = o2;
        o2 = Engine::Output(1.0);
        o2 = std::move(o3);
        QCOMPARE(o2.getType(), Engine::Output::Type::String);
        QVERIFY(!o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o2.toInteger()));
        QVERIFY(!FP::defined(o2.toReal()));
        QCOMPARE(o2.toString(), std::string("Abcd"));
        QVERIFY(o2.toBoolean());
    }

    void outputBoolean()
    {
        Engine::Output o1(true);
        QCOMPARE(o1.getType(), Engine::Output::Type::Boolean);
        QVERIFY(!o1.isNil());
        QVERIFY(!o1.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o1.toInteger()));
        QVERIFY(!FP::defined(o1.toReal()));
        QVERIFY(o1.toString().empty());
        QVERIFY(o1.toBoolean());

        Engine::Output o2(o1);
        Engine::Output o3(std::move(o1));
        QCOMPARE(o2.getType(), Engine::Output::Type::Boolean);
        QVERIFY(!o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o2.toInteger()));
        QVERIFY(!FP::defined(o2.toReal()));
        QVERIFY(o2.toString().empty());
        QVERIFY(o2.toBoolean());
        QCOMPARE(o3.getType(), Engine::Output::Type::Boolean);
        QVERIFY(!o3.isNil());
        QVERIFY(!o3.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o3.toInteger()));
        QVERIFY(!FP::defined(o3.toReal()));
        QVERIFY(o3.toString().empty());
        QVERIFY(o3.toBoolean());

        o2 = o3;
        QCOMPARE(o2.getType(), Engine::Output::Type::Boolean);
        QVERIFY(!o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o2.toInteger()));
        QVERIFY(!FP::defined(o2.toReal()));
        QVERIFY(o2.toString().empty());
        QVERIFY(o2.toBoolean());

        o2 = std::move(o3);
        QCOMPARE(o2.getType(), Engine::Output::Type::Boolean);
        QVERIFY(!o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o2.toInteger()));
        QVERIFY(!FP::defined(o2.toReal()));
        QVERIFY(o2.toString().empty());
        QVERIFY(o2.toBoolean());

        o3 = o2;
        o2 = Engine::Output(1.0);
        o2 = std::move(o3);
        QCOMPARE(o2.getType(), Engine::Output::Type::Boolean);
        QVERIFY(!o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o2.toInteger()));
        QVERIFY(!FP::defined(o2.toReal()));
        QVERIFY(o2.toString().empty());
        QVERIFY(o2.toBoolean());

        o2 = Engine::Output(false);
        QCOMPARE(o2.getType(), Engine::Output::Type::Boolean);
        QVERIFY(!o2.isNil());
        QVERIFY(!o2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(o2.toInteger()));
        QVERIFY(!FP::defined(o2.toReal()));
        QVERIFY(o2.toString().empty());
        QVERIFY(!o2.toBoolean());
    }

    void referenceNil()
    {
        Engine e;
        Engine::Frame f1(e);
        f1.pushNil();

        auto r1 = f1.back();
        QVERIFY(r1.isNil());
        QCOMPARE(r1.getType(), Engine::Reference::Type::Nil);
        QVERIFY(!r1.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(r1.toInteger()));
        QVERIFY(!FP::defined(r1.toReal()));
        QVERIFY(r1.toString().empty());
        QVERIFY(!r1.toBoolean());

        {
            auto o = r1.get();
            QCOMPARE(o.getType(), Engine::Output::Type::Nil);
        }

        Engine::Frame f2(f1);
        f2.pushNil();
        auto r2 = f2.back();
        QVERIFY(r2.isNil());
        QCOMPARE(r2.getType(), Engine::Reference::Type::Nil);
        QVERIFY(!r2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(r2.toInteger()));
        QVERIFY(!FP::defined(r2.toReal()));
        QVERIFY(r2.toString().empty());
        QVERIFY(!r2.toBoolean());
        QVERIFY(r1.rawEqual(r2));
        QVERIFY(r1 == r2);

        r2 = r1;
        QVERIFY(r2.isNil());
        QCOMPARE(r2.getType(), Engine::Reference::Type::Nil);
        QVERIFY(!r2.toData<Engine::BaseData>());
        QVERIFY(!INTEGER::defined(r2.toInteger()));
        QVERIFY(!FP::defined(r2.toReal()));
        QVERIFY(r2.toString().empty());
        QVERIFY(!r2.toBoolean());
        QVERIFY(r2.rawEqual(r1));
        QVERIFY(r2 == r1);
    }

    void referenceData()
    {
        using DataType = Engine::PersistentData;

        DataType pd;
        Engine e;
        pd.ready(e);
        Engine::Frame f1(e);
        f1.push(&pd);

        auto r1 = f1.back();
        QVERIFY(!r1.isNil());
        QCOMPARE(r1.getType(), Engine::Reference::Type::Data);
        QCOMPARE(r1.toData<DataType>(), &pd);
        QVERIFY(!INTEGER::defined(r1.toInteger()));
        QVERIFY(!FP::defined(r1.toReal()));
        QVERIFY(r1.toString().empty());
        QVERIFY(r1.toBoolean());

        {
            auto o = r1.get();
            QCOMPARE(o.getType(), Engine::Output::Type::Data);
            QCOMPARE(o.toData<DataType>(), &pd);
        }

        Engine::Frame f2(f1);
        f2.push(&pd);
        auto r2 = f2.back();
        QVERIFY(!r2.isNil());
        QCOMPARE(r2.getType(), Engine::Reference::Type::Data);
        QCOMPARE(r2.toData<DataType>(), &pd);
        QVERIFY(!INTEGER::defined(r2.toInteger()));
        QVERIFY(!FP::defined(r2.toReal()));
        QVERIFY(r2.toString().empty());
        QVERIFY(r2.toBoolean());
        QVERIFY(r1.rawEqual(r2));
        QVERIFY(r1 == r2);

        r2 = r1;
        QVERIFY(!r2.isNil());
        QCOMPARE(r2.getType(), Engine::Reference::Type::Data);
        QCOMPARE(r2.toData<DataType>(), &pd);
        QVERIFY(!INTEGER::defined(r2.toInteger()));
        QVERIFY(!FP::defined(r2.toReal()));
        QVERIFY(r2.toString().empty());
        QVERIFY(r2.toBoolean());
        QVERIFY(r2.rawEqual(r1));
        QVERIFY(r2 == r1);
    }

    void referenceNumber()
    {
        Engine e;
        Engine::Frame f1(e);
        f1.push(2.0);

        auto r1 = f1.back();
        QVERIFY(!r1.isNil());
        QCOMPARE(r1.getType(), Engine::Reference::Type::Number);
        QVERIFY(!r1.toData<Engine::BaseData>());
        QCOMPARE((int) r1.toInteger(), 2);
        QCOMPARE(r1.toReal(), 2.0);
        QVERIFY(r1.toBoolean());

        {
            auto o = r1.get();
            QCOMPARE(o.getType(), Engine::Output::Type::Number);
            QCOMPARE(o.toReal(), 2.0);
        }

        Engine::Frame f2(f1);
        f2.push(2.0);
        auto r2 = f2.back();
        QVERIFY(!r2.isNil());
        QCOMPARE(r2.getType(), Engine::Reference::Type::Number);
        QVERIFY(!r2.toData<Engine::BaseData>());
        QCOMPARE((int) r2.toInteger(), 2);
        QCOMPARE(r2.toReal(), 2.0);
        QVERIFY(r2.toBoolean());
        QVERIFY(r1.rawEqual(r2));
        QVERIFY(r1 == r2);
        QVERIFY(!(r1 < r2));

        r2 = r1;
        QVERIFY(!r2.isNil());
        QCOMPARE(r2.getType(), Engine::Reference::Type::Number);
        QVERIFY(!r2.toData<Engine::BaseData>());
        QCOMPARE((int) r2.toInteger(), 2);
        QCOMPARE(r2.toReal(), 2.0);
        QVERIFY(r2.toBoolean());
        QVERIFY(r2.rawEqual(r1));
        QVERIFY(r2 == r1);
        QVERIFY(!(r1 < r2));

        f2.pop();
        f2.push(3.0);
        r2 = f2.back();
        QVERIFY(!r1.rawEqual(r2));
        QVERIFY(r1 != r2);
        QVERIFY(r1 < r2);
    }

    void referenceString()
    {
        Engine e;
        Engine::Frame f1(e);
        f1.push("Abcd");

        auto r1 = f1.back();
        QVERIFY(!r1.isNil());
        QCOMPARE(r1.getType(), Engine::Reference::Type::String);
        QVERIFY(!r1.toData<Engine::BaseData>());
        QCOMPARE(r1.toString(), std::string("Abcd"));
        QVERIFY(r1.toBoolean());

        {
            auto o = r1.get();
            QCOMPARE(o.getType(), Engine::Output::Type::String);
            QCOMPARE(o.toString(), std::string("Abcd"));
        }

        Engine::Frame f2(f1);
        f2.push(std::string("Abcd"));
        auto r2 = f2.back();
        QVERIFY(!r2.isNil());
        QCOMPARE(r2.getType(), Engine::Reference::Type::String);
        QVERIFY(!r2.toData<Engine::BaseData>());
        QCOMPARE(r2.toString(), std::string("Abcd"));
        QVERIFY(r2.toBoolean());
        QVERIFY(r1.rawEqual(r2));
        QVERIFY(r1 == r2);
        QVERIFY(!(r1 < r2));

        r2 = r1;
        QVERIFY(!r2.isNil());
        QCOMPARE(r2.getType(), Engine::Reference::Type::String);
        QVERIFY(!r2.toData<Engine::BaseData>());
        QCOMPARE(r2.toString(), std::string("Abcd"));
        QVERIFY(r2.toBoolean());
        QVERIFY(r2.rawEqual(r1));
        QVERIFY(r2 == r1);
        QVERIFY(!(r1 < r2));
    }

    void referenceBoolean()
    {
        Engine e;
        Engine::Frame f1(e);
        f1.push(true);

        auto r1 = f1.back();
        QVERIFY(!r1.isNil());
        QCOMPARE(r1.getType(), Engine::Reference::Type::Boolean);
        QVERIFY(!r1.toData<Engine::BaseData>());
        QVERIFY(r1.toBoolean());

        {
            auto o = r1.get();
            QCOMPARE(o.getType(), Engine::Output::Type::Boolean);
            QVERIFY(o.toBoolean());
        }

        Engine::Frame f2(f1);
        f2.push(true);
        auto r2 = f2.back();
        QVERIFY(!r2.isNil());
        QCOMPARE(r2.getType(), Engine::Reference::Type::Boolean);
        QVERIFY(!r2.toData<Engine::BaseData>());
        QVERIFY(r2.toBoolean());
        QVERIFY(r1.rawEqual(r2));
        QVERIFY(r1 == r2);

        r2 = r1;
        QVERIFY(!r2.isNil());
        QCOMPARE(r2.getType(), Engine::Reference::Type::Boolean);
        QVERIFY(!r2.toData<Engine::BaseData>());
        QVERIFY(r2.toBoolean());
        QVERIFY(r2.rawEqual(r1));
        QVERIFY(r2 == r1);

        f2.pop();
        f2.push(false);
        r2 = f2.back();
        QCOMPARE(r2.getType(), Engine::Reference::Type::Boolean);
        QVERIFY(!r2.toBoolean());
        QVERIFY(!r2.rawEqual(r1));
        QVERIFY(r2 != r1);
    }
};

QTEST_APPLESS_MAIN(TestEngineTypes)

#include "engine_types.moc"
