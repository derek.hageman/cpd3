/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QTemporaryFile>

#include "luascript/engine.hxx"
#include "luascript/libs/regex.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestEngineCall : public QObject {
Q_OBJECT

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void error()
    {
        Engine e;

        {
            Engine::Call c(e);
            c.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
                entry.throwError("an error");
            }));
            QVERIFY(!c.execute());
            QVERIFY(e.inError());
        }

        e.clearError();
        QVERIFY(!e.inError());
        {
            Engine::Call c(e);
            c.push([](Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues) {
                entry.throwError("another error");
            }, 0);
            QVERIFY(!c.executeVariable());
            QVERIFY(e.inError());
        }
    }

    void errorLuaCatch()
    {
        Engine e;

        {
            Engine::Frame f(e);
            Engine::Assign c(f, e.global(), "cf");
            c.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
                entry.throwError("an error");
            }));
        }
        {
            Engine::Frame f(e);
            Engine::Assign c(f, e.global(), "of");
            c.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
                return;
            }));
        }

        {
            Engine::Call c(e);
            QVERIFY(c.pushChunk("pcall(cf); of();", false));
            QVERIFY(c.execute());
            QVERIFY(!e.inError());
        }
        {
            Engine::Frame f(e);
            f.pushData<Libs::Regex>(QRegularExpression("\\s+"));
        }

        {
            Engine::Call c(e);
            QVERIFY(c.pushChunk("pcall(cf)", false));
            QVERIFY(c.execute());
            QVERIFY(!e.inError());
        }
        {
            Engine::Frame f(e);
            f.pushData<Libs::Regex>(QRegularExpression("\\s+"));
        }
    }

    void simpleCall()
    {
        Engine e;

        {
            int counter = 0;
            Engine::Call c(e);
            c.push([&counter](Engine::Entry &entry) {
                counter += (int) entry.back().toInteger();
                entry.pop();
            });
            c.push(2);
            QVERIFY(c.execute());
            QCOMPARE(counter, 2);
        }

        {
            int counter = 0;
            Engine::Frame f(e);
            f.push([&counter](Engine::Entry &entry) {
                counter += (int) entry.back().toInteger();
            });
            {
                Engine::Call c(f, f.back());
                c.push(1);
                QVERIFY(c.execute());
                QCOMPARE(counter, 1);
            }
            {
                Engine::Call c(f, f.back());
                c.push(2);
                QVERIFY(c.execute());
                QCOMPARE(counter, 3);
            }
        }

        {
            Engine::Call c(e);
            c.push(std::function<void(Engine::Entry &)>([](Engine::Entry &entry) {
                entry.push(2.5);
                entry.propagate();
            }));
            QVERIFY(c.execute(1));
            QCOMPARE((int) c.size(), 1);
            QCOMPARE(c.back().toReal(), 2.5);
        }
    }

    void upvalue()
    {
        Engine e;

        {
            int counter = 0;
            Engine::Call c(e);
            c.push(1);
            c.push(2);
            c.push([&counter](Engine::Entry &entry,
                              const std::vector<Engine::Reference> &upvalues) {
                counter += (int) entry.back().toInteger();
                counter += upvalues[0].toInteger();
                counter += upvalues[1].toInteger();
            }, 2);
            c.push(3);
            QVERIFY(c.execute());
            QCOMPARE(counter, 6);
        }

        {
            Engine::Frame f(e);

            f.push(2);
            auto r = f.back();

            {
                int counter = 0;
                Engine::Call c(f);
                c.push([&counter](Engine::Entry &entry,
                                  const std::vector<Engine::Reference> &upvalues) {
                    counter += (int) entry.front().toInteger();
                    counter += upvalues[0].toInteger();
                    entry.pop();
                    entry.push(99);
                    entry.propagate(1);
                }, {r});
                c.push(3);
                QVERIFY(c.executeVariable());
                QCOMPARE(counter, 5);
                c.propagate();
            }

            QCOMPARE((int) f.size(), 2);
            QCOMPARE((int) f.back().toInteger(), 99);
        }

    }

    void parse()
    {
        static const char *safe = R"EOF(
if test_global then
    test_global = test_global + 1;
end
return 42;
)EOF";
        static const char *unsafe = R"EOF(
os.getenv("CPD3");
if test_global then
    test_global = test_global + 1;
end
return 43;
)EOF";

        QTemporaryFile safeFile;
        QVERIFY(safeFile.open());
        safeFile.write(safe);
        safeFile.flush();

        QTemporaryFile unsafeFile;
        QVERIFY(unsafeFile.open());
        unsafeFile.write(unsafe);
        unsafeFile.flush();

        {
            Engine e;

            e.global().set("test_global", Engine::Output(10));

            {
                Engine::Call c(e);
                QVERIFY(c.pushChunk(safe, false));
                QVERIFY(c.execute(1));
                QVERIFY(!e.inError());
                QCOMPARE((int) c.size(), 1);
                QCOMPARE((int) c.back().toInteger(), 42);
            }
            {
                Engine::Call c(e);
                QVERIFY(c.pushChunk(unsafe, false));
                QVERIFY(c.execute(1));
                QVERIFY(!e.inError());
                QCOMPARE((int) c.size(), 1);
                QCOMPARE((int) c.back().toInteger(), 43);
            }

            {
                Engine::Call c(e);
                QVERIFY(c.pushFile(safeFile.fileName().toStdString(), false));
                QVERIFY(c.execute(1));
                QVERIFY(!e.inError());
                QCOMPARE((int) c.size(), 1);
                QCOMPARE((int) c.back().toInteger(), 42);
            }
            {
                Engine::Call c(e);
                QVERIFY(c.pushFile(unsafeFile.fileName().toStdString(), false));
                QVERIFY(c.execute(1));
                QVERIFY(!e.inError());
                QCOMPARE((int) c.size(), 1);
                QCOMPARE((int) c.back().toInteger(), 43);
            }

            {
                Engine::Frame f(e);
                auto env = f.pushSandboxEnvironment();
                env.set("test_global", Engine::Output(20));

                {
                    Engine::Call c(f);
                    QVERIFY(c.pushChunk(safe, env));
                    QVERIFY(c.execute(1));
                    QVERIFY(!e.inError());
                    QCOMPARE((int) c.size(), 1);
                    QCOMPARE((int) c.back().toInteger(), 42);
                }
                {
                    Engine::Call c(f);
                    QVERIFY(c.pushChunk(unsafe, env));
                    QVERIFY(!c.execute(1));
                    QVERIFY(e.inError());
                    e.clearError();
                }
                {
                    Engine::Call c(f);
                    QVERIFY(c.pushFile(safeFile.fileName().toStdString(), env));
                    QVERIFY(c.execute(1));
                    QVERIFY(!e.inError());
                    QCOMPARE((int) c.size(), 1);
                    QCOMPARE((int) c.back().toInteger(), 42);
                }
                {
                    Engine::Call c(f);
                    QVERIFY(c.pushFile(unsafeFile.fileName().toStdString(), env));
                    QVERIFY(!c.execute(1));
                    QVERIFY(e.inError());
                    e.clearError();
                }

                QCOMPARE((int) env.get("test_global").toInteger(), 22);
            }

            QCOMPARE((int) e.global().get("test_global").toInteger(), 14);
        }
        {
            Engine e;

            e.global().set("test_global", Engine::Output(10));

            {
                Engine::Call c(e);
                QVERIFY(c.pushChunk(safe, true));
                QVERIFY(c.execute(1));
                QVERIFY(!e.inError());
                QCOMPARE((int) c.size(), 1);
                QCOMPARE((int) c.back().toInteger(), 42);
            }
            {
                Engine::Call c(e);
                QVERIFY(c.pushChunk(unsafe, true));
                QVERIFY(!c.execute(1));
                QVERIFY(e.inError());
                e.clearError();
            }

            {
                Engine::Call c(e);
                QVERIFY(c.pushFile(safeFile.fileName().toStdString(), true));
                QVERIFY(c.execute(1));
                QVERIFY(!e.inError());
                QCOMPARE((int) c.size(), 1);
                QCOMPARE((int) c.back().toInteger(), 42);
            }
            {
                Engine::Call c(e);
                QVERIFY(c.pushFile(unsafeFile.fileName().toStdString(), true));
                QVERIFY(!c.execute(1));
                QVERIFY(e.inError());
                e.clearError();
            }

            QCOMPARE((int) e.global().get("test_global").toInteger(), 10);
        }

    }

    void libs()
    {
        Engine e;

        {
            Engine::Call c(e);
            QVERIFY(c.pushChunk(
                    "return CPD3.defined(1.0), CPD3.defined(nil), CPD3.defined(CPD3.undefined), CPD3.undefined",
                    true));
            QVERIFY(c.execute(4));
            QVERIFY(!e.inError());
            QCOMPARE((int) c.size(), 4);
            QVERIFY(c[0].toBoolean());
            QVERIFY(!c[1].toBoolean());
            QVERIFY(!c[2].toBoolean());
            QCOMPARE(c[3].getType(), Engine::Reference::Type::Number);
            QVERIFY(!FP::defined(c[3].toNumber()));
        }
    }
};

QTEST_APPLESS_MAIN(TestEngineCall)

#include "engine_call.moc"
