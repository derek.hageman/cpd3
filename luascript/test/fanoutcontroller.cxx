/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>

#include "luascript/fanoutcontroller.hxx"
#include "core/timeutils.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Lua;

class TestFanout : public FanoutController {
public:
    Engine::Output targetArg;

    TestFanout() = default;

    virtual ~TestFanout() = default;

    class TestTarget : public FanoutController::Target {
        TestFanout &parent;
    public:
        Engine::Output type;
        std::vector<std::shared_ptr<Target>> background;

        explicit TestTarget(TestFanout &parent, const Engine::Output &type) : parent(parent),
                                                                              type(type)
        { }

        virtual ~TestTarget() = default;

        bool run(Engine::Frame &frame, const Engine::Reference &controller)
        {
            Engine::Call call(frame);
            pushSaved(call, controller);
            return call.execute();
        }

    protected:
        bool initializeCall(Engine::Frame &arguments,
                            const Engine::Reference &controller,
                            const Engine::Reference &key,
                            const std::vector<std::shared_ptr<Target>> &background,
                            Engine::Table &) override
        {
            this->background = background;

            arguments.push(key);
            arguments.push(parent.targetArg);
            arguments.propagate(2);
            return true;
        }


        void processSaved(Engine::Frame &saved,
                          const Engine::Reference &controller,
                          const Engine::Reference &key,
                          const std::vector<std::shared_ptr<Target>> &background,
                          Engine::Table &) override
        {
            Q_ASSERT(saved.size() == 1);
            Q_ASSERT(background == this->background);
        }
    };

protected:

    std::shared_ptr<Target> createTarget(const Engine::Output &type) override
    { return std::shared_ptr<Target>(new TestTarget(*this, type)); }

    void addedDispatch(Engine::Frame &,
                       const Engine::Reference &,
                       const Data::SequenceName &,
                       std::vector<std::shared_ptr<Target>> &targets) override
    {
        Q_ASSERT(!targets.empty());
    }
};

class TestFanoutController : public QObject {
Q_OBJECT

private slots:

    void basic()
    {
        Engine e;
        Engine::Frame base(e);
        TestFanout fanout;

        fanout.pushController(base);
        auto controller = base.back();
        e.global().set("fanout", controller);

        {
            Engine::Call init(base);
            init.pushChunk(R"EOF(
create_counter = 0;
call_counter = {};
fanout.fanout = {
    station = false,
};
fanout(function(key, arg)
    if key.archive ~= "raw" then error(); end
    create_counter = create_counter + 1;
    return function()
       call_counter[arg] = (call_counter[arg] or 0) + 1;
    end
end);
)EOF", false);
            QVERIFY(init.execute());
        }

        {
            fanout.targetArg = Engine::Output(1);
            auto d = fanout.dispatch(base, controller, Data::SequenceName("brw", "raw", "BsG_S11"));
            QCOMPARE((int) d.size(), 1);
            QVERIFY(!fanout.allTargets().empty());

            QCOMPARE((int) e.global().get("create_counter").toInteger(), 1);
            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                check.push(check.back(), 1);
                QVERIFY(check.back().isNil());
            }

            auto t = dynamic_cast<TestFanout::TestTarget *>(d[0].get());
            QVERIFY(t != nullptr);
            QVERIFY(!t->type.toBoolean());
            QVERIFY(t->background.empty());
            QVERIFY(t->run(base, controller));

            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 1);
            }
        }
        {
            auto d = fanout.dispatch(base, controller, Data::SequenceName("brw", "raw", "BsB_S11"));
            QCOMPARE((int) d.size(), 1);

            QCOMPARE((int) e.global().get("create_counter").toInteger(), 1);

            auto t = dynamic_cast<TestFanout::TestTarget *>(d[0].get());
            QVERIFY(t != nullptr);
            QVERIFY(t->run(base, controller));
            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
            }
        }

        {
            fanout.targetArg = Engine::Output(2);
            auto d = fanout.dispatch(base, controller,
                                     Data::SequenceName("brw", "raw", "BsG_S11", {"pm1"}));
            QCOMPARE((int) d.size(), 1);

            QCOMPARE((int) e.global().get("create_counter").toInteger(), 2);
            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                check.push(check.back(), 2);
                QVERIFY(check.back().isNil());
            }

            auto t = dynamic_cast<TestFanout::TestTarget *>(d[0].get());
            QVERIFY(t != nullptr);
            QVERIFY(!t->type.toBoolean());
            QVERIFY(t->background.empty());
            QVERIFY(t->run(base, controller));

            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 1);
            }
        }

        QVERIFY(!e.inError());
    }

    void nameFanout()
    {
        Engine e;
        Engine::Frame base(e);
        TestFanout fanout;

        fanout.pushController(base);
        auto controller = base.back();
        e.global().set("fanout", controller);

        {
            Engine::Call init(base);
            init.pushChunk(R"EOF(
create_counter = 0;
call_counter = {};
fanout(function(key, arg)
    create_counter = create_counter + 1;
    return function()
       call_counter[arg] = (call_counter[arg] or 0) + 1;
    end
end);
)EOF", false);
            QVERIFY(init.execute());
        }

        {
            fanout.targetArg = Engine::Output(1);
            auto d = fanout.dispatch(base, controller, Data::SequenceName("_", "raw", "BsG_S11"));
            QCOMPARE((int) d.size(), 1);

            QCOMPARE((int) e.global().get("create_counter").toInteger(), 1);
            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                check.push(check.back(), 1);
                QVERIFY(check.back().isNil());
            }

            auto t = dynamic_cast<TestFanout::TestTarget *>(d[0].get());
            QVERIFY(t != nullptr);
            QVERIFY(t->type.toBoolean());
            QVERIFY(t->background.empty());
            QVERIFY(t->run(base, controller));

            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 1);
            }
        }
        {
            fanout.targetArg = Engine::Output(2);
            auto d = fanout.dispatch(base, controller, Data::SequenceName("brw", "raw", "BsG_S11"));
            QCOMPARE((int) d.size(), 1);

            QCOMPARE((int) e.global().get("create_counter").toInteger(), 2);
            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 1);
                check.push(check.back(), 2);
                QVERIFY(check.back().isNil());
            }

            auto t = dynamic_cast<TestFanout::TestTarget *>(d[0].get());
            QVERIFY(t != nullptr);
            QVERIFY(!t->type.toBoolean());
            QCOMPARE((int) t->background.size(), 1);
            QVERIFY(t->run(base, controller));

            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 1);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 1);
            }
        }
        {
            auto d = fanout.dispatch(base, controller, Data::SequenceName("_", "raw", "BsG_S11"));
            QCOMPARE((int) d.size(), 2);

            QCOMPARE((int) e.global().get("create_counter").toInteger(), 2);

            TestFanout::TestTarget *haveTrue = nullptr;
            TestFanout::TestTarget *haveFalse = nullptr;
            for (const auto &check : d) {
                auto t = dynamic_cast<TestFanout::TestTarget *>(check.get());
                QVERIFY(t != nullptr);
                if (t->type.toBoolean()) {
                    QVERIFY(!haveTrue);
                    haveTrue = t;
                } else {
                    QVERIFY(!haveFalse);
                    haveFalse = t;
                }
                QVERIFY(t->run(base, controller));
            }
            QVERIFY(haveTrue);
            QVERIFY(haveFalse);
            QVERIFY(haveTrue->background.empty());
            QCOMPARE((int) haveFalse->background.size(), 1);
            QCOMPARE(haveFalse->background[0].get(), haveTrue);

            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 2);
            }
        }

        {
            fanout.targetArg = Engine::Output(3);
            auto d = fanout.dispatch(base, controller,
                                     Data::SequenceName("brw", "clean", "BsG_S11"));
            QCOMPARE((int) d.size(), 1);

            QCOMPARE((int) e.global().get("create_counter").toInteger(), 3);
            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 2);
                check.push(check.back(), 3);
                QVERIFY(check.back().isNil());
            }

            auto t = dynamic_cast<TestFanout::TestTarget *>(d[0].get());
            QVERIFY(t != nullptr);
            QVERIFY(!t->type.toBoolean());
            QCOMPARE((int) t->background.size(), 0);
            QVERIFY(t->run(base, controller));

            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(3).toInteger(), 1);
            }
        }
        {
            fanout.targetArg = Engine::Output(4);
            auto d = fanout.dispatch(base, controller, Data::SequenceName("_", "clean", "BsG_S11"));
            QCOMPARE((int) d.size(), 2);

            QCOMPARE((int) e.global().get("create_counter").toInteger(), 4);
            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(3).toInteger(), 1);
                check.push(check.back(), 4);
                QVERIFY(check.back().isNil());
            }

            TestFanout::TestTarget *haveTrue = nullptr;
            TestFanout::TestTarget *haveFalse = nullptr;
            for (const auto &check : d) {
                auto t = dynamic_cast<TestFanout::TestTarget *>(check.get());
                QVERIFY(t != nullptr);
                if (t->type.toBoolean()) {
                    QVERIFY(!haveTrue);
                    haveTrue = t;
                } else {
                    QVERIFY(!haveFalse);
                    haveFalse = t;
                }
                QVERIFY(t->run(base, controller));
            }
            QVERIFY(haveTrue);
            QVERIFY(haveFalse);
            QVERIFY(haveTrue->background.empty());
            QVERIFY(haveFalse->background.empty());

            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(3).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(4).toInteger(), 1);
            }
        }
        {
            fanout.targetArg = Engine::Output(5);
            auto d = fanout.dispatch(base, controller,
                                     Data::SequenceName("brw", "clean", "BsB_S11"));
            QCOMPARE((int) d.size(), 1);

            QCOMPARE((int) e.global().get("create_counter").toInteger(), 4);
            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(3).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(4).toInteger(), 1);
                check.push(check.back(), 5);
                QVERIFY(check.back().isNil());
            }

            auto t = dynamic_cast<TestFanout::TestTarget *>(d[0].get());
            QVERIFY(t != nullptr);
            QVERIFY(!t->type.toBoolean());
            QCOMPARE((int) t->background.size(), 0);
            QVERIFY(t->run(base, controller));

            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(3).toInteger(), 3);
                QCOMPARE((int) Engine::Table(check.back()).get(4).toInteger(), 1);
                check.push(check.back(), 5);
                QVERIFY(check.back().isNil());
            }
        }

        QVERIFY(!e.inError());
    }

    void luaFanout()
    {
        Engine e;
        Engine::Frame base(e);
        TestFanout fanout;

        fanout.pushController(base);
        auto controller = base.back();
        e.global().set("fanout", controller);

        {
            Engine::Call init(base);
            init.pushChunk(R"EOF(
create_counter = {};
call_counter = {};
fanout.key = function(name)
   if name.archive == "def" then
      return "def_" .. name.station, "background";
   else
      return name.station, "foreground", "def_" .. name.station;
   end
end
fanout(function(key, arg)
    create_counter[key] = (create_counter[key] or 0) + 1;
    return function()
       call_counter[arg] = (call_counter[arg] or 0) + 1;
    end
end);
)EOF", false);
            QVERIFY(init.execute());
        }

        {
            fanout.targetArg = Engine::Output(1);
            auto d = fanout.dispatch(base, controller, Data::SequenceName("first", "def", "1"));
            QCOMPARE((int) d.size(), 1);

            {
                Engine::Frame check(base);
                check.push(e.global(), "create_counter");
                QCOMPARE((int) Engine::Table(check.back()).get("def_first").toInteger(), 1);
                check.push(check.back(), "first");
                QVERIFY(check.back().isNil());
            }
            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                check.push(check.back(), 1);
                QVERIFY(check.back().isNil());
            }

            auto t = dynamic_cast<TestFanout::TestTarget *>(d[0].get());
            QVERIFY(t != nullptr);
            QCOMPARE(t->type.toString(), std::string("background"));
            QVERIFY(t->background.empty());
            QVERIFY(t->run(base, controller));

            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 1);
            }
        }
        {
            fanout.targetArg = Engine::Output(2);
            auto d = fanout.dispatch(base, controller, Data::SequenceName("first", "ndef", "1"));
            QCOMPARE((int) d.size(), 1);

            {
                Engine::Frame check(base);
                check.push(e.global(), "create_counter");
                QCOMPARE((int) Engine::Table(check.back()).get("def_first").toInteger(), 1);
                QCOMPARE((int) Engine::Table(check.back()).get("first").toInteger(), 1);
            }
            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 1);
                check.push(check.back(), 2);
                QVERIFY(check.back().isNil());
            }

            auto t = dynamic_cast<TestFanout::TestTarget *>(d[0].get());
            QVERIFY(t != nullptr);
            QCOMPARE(t->type.toString(), std::string("foreground"));
            QCOMPARE((int) t->background.size(), 1);
            QVERIFY(t->run(base, controller));

            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 1);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 1);
            }
        }
        {
            auto d = fanout.dispatch(base, controller, Data::SequenceName("first", "def", "2"));
            QCOMPARE((int) d.size(), 2);

            {
                Engine::Frame check(base);
                check.push(e.global(), "create_counter");
                QCOMPARE((int) Engine::Table(check.back()).get("def_first").toInteger(), 1);
                QCOMPARE((int) Engine::Table(check.back()).get("first").toInteger(), 1);
            }
            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 1);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 1);
            }

            TestFanout::TestTarget *haveBackground = nullptr;
            TestFanout::TestTarget *haveForeground = nullptr;
            for (const auto &check : d) {
                auto t = dynamic_cast<TestFanout::TestTarget *>(check.get());
                QVERIFY(t != nullptr);
                if (t->type.toString() == "background") {
                    QVERIFY(!haveBackground);
                    haveBackground = t;
                } else if (t->type.toString() == "foreground") {
                    QVERIFY(!haveForeground);
                    haveForeground = t;
                } else {
                    QFAIL("Invalid type");
                }
                QVERIFY(t->run(base, controller));
            }
            QVERIFY(haveBackground);
            QVERIFY(haveForeground);
            QVERIFY(haveBackground->background.empty());
            QCOMPARE((int) haveForeground->background.size(), 1);
            QCOMPARE(haveForeground->background[0].get(), haveBackground);

            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 2);
            }
        }

        {
            fanout.targetArg = Engine::Output(3);
            auto d = fanout.dispatch(base, controller, Data::SequenceName("second", "ndef", "1"));
            QCOMPARE((int) d.size(), 1);

            {
                Engine::Frame check(base);
                check.push(e.global(), "create_counter");
                QCOMPARE((int) Engine::Table(check.back()).get("def_first").toInteger(), 1);
                QCOMPARE((int) Engine::Table(check.back()).get("first").toInteger(), 1);
                QCOMPARE((int) Engine::Table(check.back()).get("second").toInteger(), 1);
                check.push(check.back(), "def_second");
                QVERIFY(check.back().isNil());
            }
            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 2);
                check.push(check.back(), 3);
                QVERIFY(check.back().isNil());
            }

            auto t = dynamic_cast<TestFanout::TestTarget *>(d[0].get());
            QVERIFY(t != nullptr);
            QCOMPARE(t->type.toString(), std::string("foreground"));
            QVERIFY(t->background.empty());
            QVERIFY(t->run(base, controller));

            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(3).toInteger(), 1);
            }
        }
        {
            fanout.targetArg = Engine::Output(4);
            auto d = fanout.dispatch(base, controller, Data::SequenceName("second", "def", "1"));
            QCOMPARE((int) d.size(), 2);

            {
                Engine::Frame check(base);
                check.push(e.global(), "create_counter");
                QCOMPARE((int) Engine::Table(check.back()).get("def_first").toInteger(), 1);
                QCOMPARE((int) Engine::Table(check.back()).get("first").toInteger(), 1);
                QCOMPARE((int) Engine::Table(check.back()).get("def_second").toInteger(), 1);
                QCOMPARE((int) Engine::Table(check.back()).get("second").toInteger(), 1);
            }
            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(3).toInteger(), 1);
                check.push(check.back(), 4);
                QVERIFY(check.back().isNil());
            }

            TestFanout::TestTarget *haveBackground = nullptr;
            TestFanout::TestTarget *haveForeground = nullptr;
            for (const auto &check : d) {
                auto t = dynamic_cast<TestFanout::TestTarget *>(check.get());
                QVERIFY(t != nullptr);
                if (t->type.toString() == "background") {
                    QVERIFY(!haveBackground);
                    haveBackground = t;
                } else if (t->type.toString() == "foreground") {
                    QVERIFY(!haveForeground);
                    haveForeground = t;
                } else {
                    QFAIL("Invalid type");
                }
                QVERIFY(t->run(base, controller));
            }
            QVERIFY(haveBackground);
            QVERIFY(haveForeground);
            QVERIFY(haveBackground->background.empty());
            QVERIFY(haveForeground->background.empty());

            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(3).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(4).toInteger(), 1);
            }
        }
        {
            fanout.targetArg = Engine::Output(5);
            auto d = fanout.dispatch(base, controller, Data::SequenceName("second", "ndef", "2"));
            QCOMPARE((int) d.size(), 1);

            {
                Engine::Frame check(base);
                check.push(e.global(), "create_counter");
                QCOMPARE((int) Engine::Table(check.back()).get("def_first").toInteger(), 1);
                QCOMPARE((int) Engine::Table(check.back()).get("first").toInteger(), 1);
                QCOMPARE((int) Engine::Table(check.back()).get("def_second").toInteger(), 1);
                QCOMPARE((int) Engine::Table(check.back()).get("second").toInteger(), 1);
            }
            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(3).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(4).toInteger(), 1);
                check.push(check.back(), 5);
                QVERIFY(check.back().isNil());
            }

            auto t = dynamic_cast<TestFanout::TestTarget *>(d[0].get());
            QVERIFY(t != nullptr);
            QCOMPARE(t->type.toString(), std::string("foreground"));
            QVERIFY(t->background.empty());
            QVERIFY(t->run(base, controller));

            {
                Engine::Frame check(base);
                check.push(e.global(), "call_counter");
                QCOMPARE((int) Engine::Table(check.back()).get(1).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(2).toInteger(), 2);
                QCOMPARE((int) Engine::Table(check.back()).get(3).toInteger(), 3);
                QCOMPARE((int) Engine::Table(check.back()).get(4).toInteger(), 1);
                check.push(check.back(), 5);
                QVERIFY(check.back().isNil());
            }
        }

        QVERIFY(!e.inError());
    }
};

QTEST_APPLESS_MAIN(TestFanoutController)

#include "fanoutcontroller.moc"
