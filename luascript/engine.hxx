/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUASCRIPT_ENGINE_HXX
#define CPD3LUASCRIPT_ENGINE_HXX

#include "core/first.hxx"

#include <memory>
#include <functional>
#include <cstdint>
#include <type_traits>
#include <string>

#include <QString>
#include <QDebug>

#include "luascript.hxx"
#include "core/util.hxx"

class QLoggingCategory;

namespace CPD3 {
namespace Lua {

/**
 * A C++ based interface to a Lua interpreter.
 */
class CPD3LUASCRIPT_EXPORT Engine final {
    class Context;

    std::unique_ptr<Context> context;

public:

    Engine();

    ~Engine();

    Engine(const Engine &) = delete;

    Engine &operator=(const Engine &) = delete;

    Engine(Engine &&) = delete;

    Engine &operator=(Engine &&) = delete;


    /**
     * Test if the interpreter is currently in an error state.
     *
     * @return  true if an error has been detected
     */
    bool inError() const;

    /**
     * Get the description of the active error.
     *
     * @return  a description of the active error
     */
    std::string errorDescription() const;

    /**
     * Throw an error in the interpreter.  After calling this the
     * interpreter will be in an error state.
     *
     * @param message   the message about the error
     */
    void throwError(const std::string &message = std::string());

    /**
     * Clear any active errors in the interpreter.
     */
    void clearError();


    class Frame;

    class Entry;

    class Call;

    class Assign;

    class Append;

    class Iterator;

    class Reference;

    class Table;

    class BaseData;

    class Data;

    class PersistentData;


    /**
     * A detached output of the interpreter.  This is used to store values that are not
     * attached to garbage collectible values in the script.
     */
    class CPD3LUASCRIPT_EXPORT Output final {
    public:
        /** The possible types the value can store/ */
        enum class Type {
            /** A Lua nil value */
                    Nil,

            /** A reference to a persistent user data */
                    Data,

            /** A Lua number */
                    Number,

            /** A Lua string */
                    String,

            /** A Lua boolean value */
                    Boolean,
        };
    private:
        Type type;
        Util::ExplicitUnion<BaseData *, double, std::string, bool> contents;

        friend class Engine::Context;

        friend class Engine::Reference;

    public:
        Output();

        ~Output();

        Output(const Output &other);

        Output &operator=(const Output &other);

        Output(Output &&other);

        Output &operator=(Output &&other);

        explicit Output(double value);

        explicit Output(std::int_fast64_t value);

        template<class Integer, class = typename std::enable_if<
                ((std::is_same<Integer, int>::value || std::is_same<Integer, qint64>::value) &&
                        !std::is_same<std::int_fast64_t, Integer>::value)>::type>
        inline explicit Output(Integer value) : Output(static_cast<std::int_fast64_t>(value))
        { }

        explicit Output(bool value);

        explicit Output(const std::string &value);

        explicit Output(std::string &&value);

        explicit Output(const char *value);

        explicit Output(const QString &value);


        /**
         * Get the type of the value.
         *
         * @return  the value type
         */
        inline Type getType() const
        { return type; }

        /**
         * Test if the value is nil.
         *
         * @return  true if the value is nil
         */
        bool isNil() const
        { return type == Type::Nil; }

        /**
         * Convert the value to a data type.
         *
         * @tparam DataType the target data type
         * @return          the converted value of nullptr if conversion is not possible
         */
        template<typename DataType>
        DataType *toData() const
        {
            static_assert(std::is_base_of<Engine::BaseData, DataType>::value, "invalid data type");
            static_assert(!std::is_base_of<Engine::Data, DataType>::value,
                          "cannot access Lua managed data through output values, use a reference");
            if (type != Type::Data)
                return nullptr;
            return dynamic_cast<DataType *>(contents.ref<BaseData *>());
        }

        /**
         * Convert the value to a number.
         *
         * @return          the value as a number or undefined if conversion fails
         */
        double toNumber() const;

        /** @see toNumber() const */
        inline double toReal() const
        { return toNumber(); }

        /**
         * Convert the value to an integer.
         *
         * @return          the value as an integer or undefined if conversion fails
         */
        std::int_fast64_t toInteger() const;

        /**
         * Convert the value to a string.
         *
         * @return          the value as a string
         */
        const std::string &toString() const;

        /** @see toString() const */
        QString toQString() const;

        /**
         * Convert the value to a boolean value.  This follows Lua conversion rules, so anything
         * not nil or false is true.
         *
         * @return      the value as a boolean
         */
        bool toBoolean() const;
    };

    /**
     * A reference to a value on the interpreter stack.  Access to references is only
     * valid while the originating stack frame is valid and the reference itself is still
     * on the stack.
     */
    class CPD3LUASCRIPT_EXPORT Reference final {
        Engine::Context *context;
        int index;

        friend class Engine;

        friend class Engine::Context;

        friend class Engine::Table;

        friend class Engine::Data;

        void *toUserData() const;

        Reference(Engine::Context *context, int index);

    public:
        Reference() noexcept;

        inline Reference(const Reference &) noexcept = default;

        inline Reference &operator=(const Reference &) noexcept = default;

        inline Reference(Reference &&) noexcept = default;

        inline Reference &operator=(Reference &&) noexcept = default;

        /**
         * Test if the reference is assigned (i.e. not default constructed).
         *
         * @return  true if the reference is assigned
         */
        inline bool isAssigned() const
        { return context != nullptr; }

        /**
         * Convert the reference to a detached output value.
         *
         * @return   the de-referenced value
         */
        Output get() const;

        /**
         * The possible types a reference can refer to.
         */
        enum class Type {
            /** A nil value */
                    Nil,

            /** A boolean value */
                    Boolean,

            /** A data pointer */
                    Data,

            /** A number */
                    Number,

            /** A string */
                    String,

            /** A table */
                    Table
        };

        /**
         * Get the type of the reference.
         *
         * @return  the type
         */
        Type getType() const;

        /**
         * Test if the reference is nil.
         *
         * @return  true if the value is nil
         */
        bool isNil() const;

        /**
         * Attempt conversion of the reference to a data pointer.  The pointer is only valid
         * as long as the reference is and remains the same type (otherwise it may be garbage
         * collected by Lua).
         *
         * @tparam DataType the data type
         * @return          the converted value or nullptr if conversion fails
         */
        template<typename DataType>
        DataType *toData() const
        {
            static_assert(std::is_base_of<Engine::BaseData, DataType>::value, "invalid data type");
            BaseData *ptr = reinterpret_cast<BaseData *>(toUserData());
            if (!ptr)
                return nullptr;
            return dynamic_cast<DataType *>(ptr);
        }

        /**
         * Convert the reference to a number.
         *
         * @return  the converted number or undefined if conversion fails
         */
        double toNumber() const;

        /**
         * Convert the reference to an integer.
         *
         * @return  the converted integer or undefined if conversion fails
         */
        std::int_fast64_t toInteger() const;

        /** @see toNumber() const */
        inline double toReal() const
        { return toNumber(); }

        /**
         * Convert the reference to a string.  Note that this alters the data type if the
         * reference was a number.
         *
         * @return  the converted string
         */
        std::string toString() const;

        /** @see toString() const */
        QString toQString() const;

        /**
         * Convert the reference to a string using Lua's tostring() function.
         *
         * @return  the converted string
         */
        std::string toOutputString() const;

        /**
         * Convert the reference to a boolean value.  Any value not nil or false is considered
         * true by Lua.
         *
         * @return  the reference as a boolean value
         */
        bool toBoolean() const;


        /**
         * Perform a raw equality test with another reference.  This bypasses metatable lookups
         * and so should not throw errors.
         *
         * @param other the other reference
         * @return      true if the references are equal
         */
        bool rawEqual(const Reference &other) const;


        bool operator==(const Reference &other) const;

        inline bool operator!=(const Reference &other) const
        { return !(*this == other); }

        bool operator<(const Reference &other) const;

        inline bool operator<=(const Reference &other) const
        { return (*this < other) || (*this == other); }

        bool operator>(const Reference &other) const
        { return !(*this <= other); }

        bool operator>=(const Reference &other) const
        { return !(*this < other); }
    };

    /**
     * A handle to a table in Lua.  This has all the same restrictions as accessing a
     * reference.
     */
    class CPD3LUASCRIPT_EXPORT Table final {
        Reference ref;

        friend class Engine::Context;

        friend class Engine::Assign;

        friend class Engine::Append;

    public:
        inline Table() noexcept = default;

        Table(const Table &) = delete;

        Table &operator=(const Table &) = delete;

        inline Table(Table &&) noexcept = default;

        inline Table &operator=(Table &&) noexcept = default;


        inline Table(const Reference &ref) noexcept : ref(ref)
        { }

        inline Table(Reference &&ref) noexcept : ref(std::move(ref))
        { }

        /**
         * Test if the table is assigned (i.e. not default constructed).
         *
         * @return  true if the table is assigned
         */
        inline bool isAssigned() const
        { return ref.isAssigned(); }

        /**
         * Get a reference to the table.
         *
         * @return  a reference of the table
         */
        inline const Reference &reference() const
        { return ref; }

        inline operator const Reference &() const
        { return ref; }


        /**
         * Set a value in the table.
         *
         * @param key       the key to set
         * @param value     the value to set it to
         */
        void set(const Output &key, const Output &value);

        /** @see set(const Output &, const Output &) */
        void set(const Reference &key, const Output &value);

        /** @see set(const Output &, const Output &) */
        void set(const std::string &key, const Output &value);

        /** @see set(const Output &, const Output &) */
        inline void set(const QString &key, const Output &value)
        { return set(key.toStdString(), value); }

        /** @see set(const Output &, const Output &) */
        void set(const char *key, const Output &value);

        /** @see set(const Output &, const Output &) */
        void set(std::size_t key, const Output &value);


        /** @see set(const Output &, const Output &) */
        void set(const Output &key, const Reference &value);

        /** @see set(const Output &, const Output &) */
        void set(const Reference &key, const Reference &value);

        /** @see set(const Output &, const Output &) */
        void set(const std::string &key, const Reference &value);

        /** @see set(const Output &, const Output &) */
        void set(const QString &key, const Reference &value)
        { return set(key.toStdString(), value); }

        /** @see set(const Output &, const Output &) */
        void set(const char *key, const Reference &value);

        /** @see set(const Output &, const Output &) */
        void set(std::size_t key, const Reference &value);

        /**
         * Erase a value in the table.
         *
         * @param key       the key to erase
         */
        void erase(const Output &key);

        /** @see erase(const Output &) */
        void erase(const Reference &key);

        /** @see erase(const Output &) */
        void erase(const std::string &key);

        /** @see erase(const Output &) */
        inline void erase(const QString &key)
        { return erase(key.toStdString()); }

        /** @see erase(const Output &, const Output &) */
        void erase(const char *key);

        /** @see erase(const Output &, const Output &) */
        void erase(std::size_t key);


        /**
         * Fetch a value from the table.
         *
         * @param key       the key to lookup
         * @return          the de-referenced value
         */
        Output get(const Output &key) const;

        /** @see get(const Output &) const */
        Output get(const Reference &key) const;

        /** @see get(const Output &) const */
        Output get(const std::string &key) const;

        /** @see get(const Output &) const */
        Output get(const QString &key) const
        { return get(key.toStdString()); }

        /** @see get(const Output &) const */
        Output get(const char *key) const;

        /** @see get(const Output &) const */
        Output get(std::size_t key) const;


        /**
         * Perform the "#" (length) Lua operator on the table.
         *
         * @return  the "length" of the table
         */
        std::size_t length() const;


        /**
         * Perform a raw set on the table, bypassing metamethods.
         *
         * @param key   the key to set
         * @param value the value to set it to
         */
        void rawSet(const Reference &key, const Reference &value);

        /** @see rawSet(const Reference &, const Reference &) */
        void rawSet(std::size_t key, const Reference &value);

        /**
         * Do a raw erase of an element in the table, bypassing metamethods.
         * 
         * @param key   the key to clear 
         */
        void rawErase(const Reference &key);

        /** @see rawClear(const Reference & */
        void rawErase(std::size_t key);

        /**
         * Perform a raw get on the metatable, bypassing metamethods.
         *
         * @param key   the key to lookup
         * @return      the de-referenced value
         */
        Output rawGet(const Reference &key) const;

        /** @see rawGet(const Reference &) const */
        Output rawGet(std::size_t key) const;


        /**
         * Set the metatable for the table.
         *
         * @param table the metatable
         */
        void setMetatable(const Table &table);

        /**
         * Remove the tables metatable, if any.
         */
        void clearMetatable();

        /**
         * Set a metaemethod in the tables metatable, creating it if required.
         *
         * @param method    the method name to set
         * @param value     the value of the metamethod
         */
        void setMetamethod(const std::string &method, const Reference &value);

        /**
         * Push the metatable for the table on to a frame, creating it if required.
         *
         * @param frame     the frame target
         * @return          the metatable pushed
         */
        Table pushMetatable(Frame &frame) const;
    };

    /**
     * Get the table of globals.
     *
     * @return  the globals table
     */
    Table global() const;

    /**
     * Get the registry table.
     *
     * @return  the registry table
     */
    Table registry() const;


    /**
     * A stack frame.  Stack frames reset the Lua stack to where it was when they are
     * where constructed on their destruction.  That is, they allow for RAII type use
     * of the Lua stack.  The exception is propagate (return) which allows for values
     * from the FRONT of the stack to be pushed onto the parent frame and moves the
     * front of the frame itself up.
     */
    class CPD3LUASCRIPT_EXPORT Frame {
        Engine::Context *context;
        Frame *parent;
        std::uint_fast32_t origin;

        friend class Engine::Context;

        friend class Engine::PersistentData;

        friend class Engine::Reference;

        friend class Engine::Table;

        friend class Engine::Call;

        friend class Engine::Assign;

        friend class Engine::Append;

        friend class Engine::Iterator;

        void pushed(std::size_t added);

        void popped(std::size_t removed);

        explicit Frame(Engine::Context *context);

        void *allocateUserData(std::size_t size);

    protected:

        Frame(Engine::Context *context, bool fromLUA);

    public:
        Frame() = delete;

        /**
         * Create a frame on an engine.  This can only be used if there is currently
         * active frame.
         *
         * @param engine    the engine
         */
        explicit Frame(Engine &engine);

        /**
         * Create a child frame from a parent one.
         *
         * @param parent    the currently active frame
         */
        explicit Frame(Frame &parent);

        virtual ~Frame();

        Frame(const Frame &) = delete;

        Frame &operator=(const Frame &) = delete;

        Frame(Frame &&) = delete;

        Frame &operator=(Frame &&) = delete;


        /**
         * Get the number of elements on the active frame.
         *
         * @return  the size of the stack frame
         */
        std::size_t size() const;

        /**
         * Test if the currently active stack frame is empty
         *
         * @return  true if the stack frame is empty
         */
        inline bool empty() const
        { return size() == 0; }


        /**
         * Push a callable chunk of Lua code on to the frame.
         *
         * @param contents  the Lua code to parse
         * @param sandbox   true if the code should be sandboxed
         * @param name      the "name" of the chunk for identifying it
         * @return          true if the chunk was pushed without errors
         */
        bool pushChunk(const std::string &contents,
                       bool sandbox = true,
                       const std::string &name = std::string());

        /**
         * Push a callable chunk of Lua code on to the frame.
         *
         * @param filename  the file to read from
         * @param sandbox   true if the code should be sandboxed
         * @return          true if the chunk was pushed without errors
         */
        bool pushFile(const std::string &filename, bool sandbox = true);

        /**
         * Push a callable chunk of Lua code on to the frame.
         *
         * @param contents      the Lua code to parse
         * @param environment   the global environment associated with the chunk
         * @param name          the "name" of the chunk for identifying it
         * @return              true if the chunk was pushed without errors
         */
        bool pushChunk(const std::string &contents,
                       const Table &environment,
                       const std::string &name = std::string());

        /**
         * Push a callable chunk of Lua code on to the frame.
         *
         * @param filename      the file to read from
         * @param environment   the global environment associated with the chunk
         * @return              true if the chunk was pushed without errors
         */
        bool pushFile(const std::string &filename, const Table &environment);


        /**
         * Construct and push a Lua managed data value and push the Lua reference on to the frame.
         *
         * @tparam AddType      the data type to construct
         * @tparam Args         the argument types for construction
         * @param args          the arguments for construction
         * @return              a pointer to the constructed value
         */
        template<typename AddType, typename...Args>
        AddType *pushData(Args &&...args)
        {
            static_assert(std::is_base_of<Data, AddType>::value, "invalid data type");
            void *ptr = allocateUserData(sizeof(AddType));
            Q_ASSERT(ptr);
            new(ptr) AddType(std::forward<Args>(args)...);
            auto t = reinterpret_cast<AddType *>(ptr);
            {
                auto self = back();
                Frame iframe(*this);
                static_cast<Data *>(t)->initialize(self, iframe);
            }
            return t;
        }

        /**
         * Push a new table on to the frame.
         *
         * @return  a table
         */
        Table pushTable();

        /**
         * Push a modifiable copy of the sandbox environment on to the frame.
         *
         * @return  a copy of the sandbox
         */
        Table pushSandboxEnvironment();

        /**
         * Push a nil on to the frame.
         */
        void pushNil();

        /**
         * Push a callable function on to the frame.
         *
         * @param call  the function to call when invoked from Lua
         */
        void push(std::function<void(Entry &)> call);

        /**
         * Push a callable function on to the frame after popping and storing some
         * number of upvalues for it.  Upvalues are associated with the callable
         * function in Lua and are always available to it.
         *
         * @param call      the function to call when invoked from Lua
         * @param upvalues  the number of upvalues to pop and store
         */
        void push(std::function<void(Entry &, const std::vector<Reference> &)> call,
                  std::size_t upvalues);

        /**
         * Push a callable function on to the frame and associate some number of upvalues
         * with it.  Upvalues are associated with the callable function in Lua and are
         * always available to it.
         *
         * @param call      the function to call when invoked from Lua
         * @param upvalues  the upvalues to associate with it
         */
        void push(std::function<void(Entry &, const std::vector<Reference> &)> call,
                  const std::vector<Reference> &upvalues);

        /**
         * Push a Lua reference to a data value on to the frame.  This reference is NOT
         * managed by Lua and so must remain valid for the lifetime of anything that
         * could get a reference to it.
         *
         * @param value     the value to add
         */
        void push(PersistentData *value);

        /**
         * Push a number on to the frame.
         *
         * @param value     the value to add
         */
        void push(double value);

        /**
         * Push a number on to the frame.
         *
         * @param value     the value to add
         */
        void push(std::int_fast64_t value);

        template<class Integer, class = typename std::enable_if<
                ((std::is_same<Integer, int>::value || std::is_same<Integer, qint64>::value) &&
                        !std::is_same<std::int_fast64_t, Integer>::value)>::type>
        inline void push(Integer value)
        { return push(static_cast<std::int_fast64_t>(value)); }

        /**
         * Push a boolean value on to the frame.
         *
         * @param value     the value to add
         */
        void push(bool value);

        /**
         * Push a string value on to the frame.
         *
         * @param value     the value to add
         */
        void push(const std::string &value);

        /** @see push(const std::string &) */
        inline void push(const QString &value)
        { return push(value.toStdString()); }

        /** @see push(const std::string &) */
        void push(const char *value);


        /**
         * Push a value on to the frame.  The type pushed depends on the contents
         * of the value.
         *
         * @param value     the value to add
         */
        void push(const Output &value);

        /**
         * Push a copy of a reference on to the frame.  This will copy primitive types but
         * simply add another reference for complex ones.
         *
         * @param ref       the value to add
         */
        void push(const Reference &ref);

        inline void push(const Table &table)
        { return push(table.reference()); }


        /**
         * Push a reference to a key in a table on to the frame.
         *
         * @param table     the table to read from
         * @param key       the key in the table
         */
        void push(const Table &table, const Output &key);

        /** @see push(const Table &, const Output &) */
        void push(const Table &table, const Reference &key);

        /** @see push(const Table &, const Output &) */
        void push(const Table &table, const std::string &key);

        /** @see push(const Table &, const Output &) */
        void push(const Table &table, const QString &key)
        { return push(table, key.toStdString()); }

        /** @see push(const Table &, const Output &) */
        void push(const Table &table, const char *key);

        /** @see push(const Table &, const Output &) */
        void push(const Table &table, std::size_t key);

        /**
         * Push a reference to a key in a table on to the frame, bypassing metamethods.
         *
         * @param table     the table to read from
         * @param key       the key in the table
         */
        void pushRaw(const Table &table, const Reference &key);

        /**
         * Push a reference to a key in a table on to the frame, bypassing metamethods.
         *
         * @param table     the table to read from
         * @param key       the key in the table
         */
        void pushRaw(const Table &table, const std::size_t &key);


        /**
         * Move the back of the frame to the given index, shifting other values towards
         * the back to make room.
         *
         * @param index the target index
         */
        void moveBackTo(std::size_t index);

        /**
         * Replace the target index with the value on the back of the stack, and remove it
         * from the back.  The stack shrinks by one if it contains more than one value and
         * no other values are changed.
         *
         * @param index the target index
         */
        void replaceWithBack(std::size_t index);

        /**
         * Remove the value at the target index.  Values after it are shifted towards the front.
         *
         * @param index the target index
         */
        void erase(std::size_t index);


        /**
         * Pop values from the back of the stack, removing references to them.
         *
         * @param count the number of values to pop
         */
        void pop(std::size_t count = 1);

        /**
         * Clear the frame, removing all values pushed on to it.
         */
        void clear();

        /**
         * Set the frame to a given size, adding nils or popping values as needed.
         *
         * @param size  the new size
         */
        void resize(std::size_t size);

        /**
         * Get a reference to the back (most recently added) value of the frame.
         *
         * @return  a reference to the most recently added value
         */
        Reference back() const;

        /**
         * Get a reference to the front (least recently added) value of the frame
         *
         * @return  a reference to the least recently added value
         */
        Reference front() const;

        /**
         * Get a reference to a value at a specific index in the frame.
         *
         * @param index the index from the front of the frame
         * @return      a reference to a value in the frame
         */
        Reference operator[](std::size_t index) const;

        /**
         * Get a reference to a value some distance from the back of the frame.
         *
         * @param distance  the distance from the back of the frame
         * @return          a reference to a value in the frame
         */
        Reference fromBack(std::size_t distance) const;


        /**
         * Propagate all values in the frame to its parent.  This effectively shifts
         * the front of the frame to its back, pushing all values on to the parent and
         * popping all off the frame itself.
         */
        void propagate();

        /**
         * Propagate a specific number of values from the front of the frame to its
         * parent.  This removes the number of values from the front of the frame (shifting
         * it towards the back) and pushes them on to the parent.
         *
         * @param count the number of values to propagate
         */
        void propagate(std::size_t count);


        /**
         * Get the effective global table.
         *
         * @return  the global table
         */
        Table global() const;

        /**
         * Get the registry table.
         *
         * @return the registry table
         */
        Table registry() const;

        /**
         * Test if the engine is currently in an error state.
         *
         * @return  true if an error has been detected
         */
        bool inError() const;

        /**
         * Get the description of the active error.
         *
         * @return  a description of the active error
         */
        std::string errorDescription() const;

        /**
         * Throw an error to the engine.
         *
         * @param message   the error message
         */
        void throwError(const std::string &message = std::string());

        /**
         * Clear any active errors in the engine.
         */
        void clearError();
    };

    friend class Engine::Frame;

    /**
     * A frame type used for function entry from Lua.
     */
    class CPD3LUASCRIPT_EXPORT Entry : public Frame {
        friend class Engine;

        friend class Engine::Context;

        explicit Entry(Engine::Frame &parent);

    public:
        Entry() = delete;

        virtual ~Entry();

        Entry(const Entry &) = delete;

        Entry &operator=(const Entry &) = delete;

        Entry(Entry &&) = delete;

        Entry &operator=(Entry &&) = delete;
    };

    /**
     * A frame type used for invoking a callable object.  The ABI is that the callable
     * object is the first element of the frame with all parameters in order after it.
     * After execution the object and all parameters are popped while any results are
     * pushed in order of return.
     */
    class CPD3LUASCRIPT_EXPORT Call : public Frame {
    public:
        Call() = delete;

        virtual ~Call();

        Call(const Call &) = delete;

        Call &operator=(const Call &) = delete;

        Call(Call &&) = delete;

        Call &operator=(Call &&) = delete;

        /**
         * Create a callable frame on an engine.  This can only be used if there is no currently
         * active frame.
         *
         * @param engine    the engine
         */
        explicit Call(Engine &engine);

        /**
         * Create a callable frame from a given parent frame.
         *
         * @param parent    the parent frame
         */
        explicit Call(Frame &parent);

        /**
         * Create a callable frame from a given parent frame and push a callable
         * reference on to it to begin with.
         *
         * @param parent    the parent frame
         * @param target    the target to call.
         */
        explicit Call(Frame &parent, const Reference &target);

        /**
         * Invoke the callable object at the front of the frame.
         *
         * @param results   the number of results to push
         * @return          true if the callable was invoked without errors
         */
        bool execute(std::size_t results = 0);

        /**
         * Invoke a callable object, pushing all results it returns.
         *
         * @return          true if the callable was invoked without errors
         */
        bool executeVariable();
    };

    /**
     * A RAII table assignment type frame.  The value to assign is pushed on
     * to the frame and the assignment done on destruction.  There must be exactly
     * two values in the frame (the key and value) on destruction, so generally
     * exactly one value should be pushed.
     */
    class CPD3LUASCRIPT_EXPORT Assign : public Frame {
        Table table;
    public:
        Assign() = delete;

        virtual ~Assign();

        Assign(const Assign &) = delete;

        Assign &operator=(const Assign &) = delete;

        Assign(Assign &&) = delete;

        Assign &operator=(Assign &&) = delete;

        /**
         * Create the assignment frame.
         *
         * @param parent    the parent frame
         * @param table     the table to assign to
         * @param key       the key in the table to set
         */
        Assign(Frame &parent, Table &table, const Output &key);

        /** @see Assign(Frame &, Table &, const Output &) */
        Assign(Frame &parent, Table &table, const Reference &key);

        /** @see Assign(Frame &, Table &, const Output &) */
        Assign(Frame &parent, Table &table, const std::string &key);

        /** @see Assign(Frame &, Table &, const Output &) */
        Assign(Frame &parent, Table &table, const QString &key);

        /** @see Assign(Frame &, Table &, const Output &) */
        Assign(Frame &parent, Table &table, const char *key);

        /** @see Assign(Frame &, Table &, const Output &) */
        Assign(Frame &parent, Table &table, std::size_t key);

        /** @see Assign(Frame &, Table &, const Output &) */
        Assign(Frame &parent, Table &&table, const Output &key);

        /** @see Assign(Frame &, Table &, const Output &) */
        Assign(Frame &parent, Table &&table, const Reference &key);

        /** @see Assign(Frame &, Table &, const Output &) */
        Assign(Frame &parent, Table &&table, const std::string &key);

        /** @see Assign(Frame &, Table &, const Output &) */
        Assign(Frame &parent, Table &&table, const QString &key);

        /** @see Assign(Frame &, Table &, const Output &) */
        Assign(Frame &parent, Table &&table, const char *key);

        /** @see Assign(Frame &, Table &, const Output &) */
        Assign(Frame &parent, Table &&table, std::size_t key);
    };

    /** A RAII table append type frame.  All values in the frame are
     * appended to the table on destruction.
     */
    class CPD3LUASCRIPT_EXPORT Append : public Frame {
        Table table;
        bool raw;
    public:
        Append() = delete;

        virtual ~Append();

        Append(const Append &) = delete;

        Append &operator=(const Append &) = delete;

        Append(Append &&) = delete;

        Append &operator=(Append &&) = delete;

        /**
         * Create the append frame.
         *
         * @param parent        the parent frame
         * @param table         the target table
         * @param raw           use raw index assignment
         */
        Append(Frame &parent, Table &table, bool raw = false);

        /** @see Append(Frame &, Table &, bool) */
        Append(Frame &parent, Table &&table, bool raw = false);
    };

    /**
     * A frame that implements an iterator through Lua objects.  Each iteration
     * pushes the key and value on to the frame and they should remain there until
     * the next cycle.
     */
    class CPD3LUASCRIPT_EXPORT Iterator : public Frame {
        Reference ref;
    public:
        virtual ~Iterator();

        Iterator() = delete;

        Iterator(const Iterator &) = delete;

        Iterator &operator=(const Iterator &) = delete;

        Iterator(Iterator &&) = delete;

        Iterator &operator=(Iterator &&) = delete;

        /**
         * Create an iterator frame.
         *
         * @param parent    the parent frame
         * @param ref       the reference to iterate
         */
        Iterator(Frame &parent, const Reference &ref);

        /**
         * Create an iterator frame.
         *
         * @param parent    the parent frame
         * @param table     the table to iterate
         */
        Iterator(Frame &parent, const Table &table);

        /**
         * Advance the iterator.  This is usually used in the condition of
         * a while loop to iterate through all keys.
         *
         * @return  true if the iterator was advanced
         */
        bool next();

        /**
         * Get the key being iterated on.
         *
         * @return  the iterated key
         */
        Reference key() const;

        /**
         * Get the value being iterated on.
         *
         * @return  the value
         */
        Reference value() const;

        /**
         * Restart the iterator on the next cycle.
         */
        void restart();
    };


    /**
     * The base type for all data in the Lua interpreter.
     */
    class CPD3LUASCRIPT_EXPORT BaseData {
        friend class Engine::Frame;

        friend class Engine::Context;

        friend class Engine::Output;

        friend class Engine::Reference;

    public:
        BaseData();

        virtual ~BaseData();

    protected:
        /**
         * Call on data addition to the engine.
         *
         * @param self  a reference to the Lua representation of the data
         * @param frame a stack frame that is used for initialization
         */
        virtual void initialize(const Reference &self, Frame &frame);
    };

    /**
     * The base type for all data managed by the Lua interpreter.  Object of this type
     * are destroyed by Lua when it collects garbage and so should not be deleted by
     * C++ code.  They can also only be safely referenced when their Lua object is also
     * on the stack.
     */
    class CPD3LUASCRIPT_EXPORT Data : public BaseData {
    public:
        Data();

        Data(const Data &) = delete;

        Data &operator=(const Data &) = delete;

        Data(Data &&) = delete;

        Data &operator=(Data &&) = delete;

        virtual ~Data();

        /**
         * Push a modifiable copy of the metatable required to be used for
         * all data object.  Any data object metatable must start with a table
         * derived from this one.
         *
         * @param frame     the frame to push on
         * @return          the table pushed
         */
        static Table pushMetatable(Frame &frame);

        /**
         * Push a method entry callable on to a frame.
         *
         * @tparam SelfType the type of the data
         * @param frame     the frame to push on
         * @param method    the method to invoke on call
         * @see Frame::push(std::function<void(Entry &)>)
         */
        template<typename SelfType>
        static void pushMethod(Frame &frame, const std::function<void(SelfType *, Entry &)> &method)
        {
            static_assert(std::is_base_of<Engine::Data, SelfType>::value, "invalid data type");
            return frame.push(std::function<void(Entry &)>([method](Entry &entry) {
                if (entry.empty()) {
                    entry.throwError("Invalid method call");
                    return;
                }
                auto self = entry.front().toData<SelfType>();
                if (!self) {
                    entry.throwError("Invalid self argument for method");
                    return;
                }
                return method(self, entry);
            }));
        }

        /**
         * Push a method entry callable on to a frame after popping and storing some
         * number of upvalues for it.  Upvalues are associated with the method
         * in Lua and are always available to it.
         *
         * @tparam SelfType the type of the data
         * @param frame     the frame to push on
         * @param method    the method to invoke on call
         * @param upvalues  the number of upvalues to pop and store
         * @see Frame::push(std::function<void(Entry &, const std::vector<Reference> &)>, std::size_t)
         */
        template<typename SelfType>
        static void pushMethod(Frame &frame,
                               const std::function<void(SelfType *,
                                                        Entry &,
                                                        const std::vector<Reference> &)> &method,
                               std::size_t upvalues)
        {
            static_assert(std::is_base_of<Engine::Data, SelfType>::value, "invalid data type");
            return frame.push([method](Entry &entry, const std::vector<Reference> &uv) {
                if (entry.empty()) {
                    entry.throwError("Invalid method call");
                    return;
                }
                auto self = entry.front().toData<SelfType>();
                if (!self) {
                    entry.throwError("Invalid self argument for method");
                    return;
                }
                return method(self, entry, uv);
            }, upvalues);
        }

        /**
         * Push a method entry callable on to a frame and associate some number of upvalues
         * with it.  Upvalues are associated with the callable function in Lua and are
         * always available to it.
         *
         * @tparam SelfType     the type of the data
         * @param frame         the frame to push on
         * @param method        the method to invoke on call
         * @param upvalues      the upvalues to store
         * @see push(std::function<void(Entry &, const std::vector<Reference> &)>, const std::vector<Reference> &)
         */
        template<typename SelfType>
        static void pushMethod(Frame &frame,
                               const std::function<void(SelfType *,
                                                        Entry &,
                                                        const std::vector<Reference> &)> &method,
                               const std::vector<Reference> &upvalues)
        {
            static_assert(std::is_base_of<Engine::Data, SelfType>::value, "invalid data type");
            return frame.push([method](Entry &entry, const std::vector<Reference> &uv) {
                if (entry.empty()) {
                    entry.throwError("Invalid method call");
                    return;
                }
                auto self = entry.front().toData<SelfType>();
                if (!self) {
                    entry.throwError("Invalid self argument for method");
                    return;
                }
                return method(self, entry, uv);
            }, upvalues);
        }

    protected:
        /**
         * Assign the metatable for the data.  The metatable must be derived from
         * one created with pushMetatable(Frame &).
         *
         * @param self  the Lua object to assign on
         * @param table the metatable to assign
         */
        void setMetatable(const Reference &self, const Table &table);

        /**
         * A handler for lookup up methods within a method table, generally used from
         * within a __index metamethod.
         *
         * @param entry     the entry frame
         * @param methods   the table of methods
         * @return          true if the method was pushed and the caller should return
         */
        static bool methodIndexHandler(Entry &entry, const Table &methods);
    };

    friend class Data;

    /**
     * The base type for persistent data objects.
     */
    class CPD3LUASCRIPT_EXPORT PersistentData : public BaseData {
    public:
        PersistentData();

        virtual ~PersistentData();

        /**
         * Inform the data that is is ready and to initialize it on the engine.
         *
         * @param engine    the engine to initialize on
         */
        void ready(Engine &engine);

        /**
         * Inform the data that is is ready and to initialize it on a frame.
         *
         * @param current   the current frame
         */
        void ready(Frame &current);
    };

    friend class PersistentData;

    friend CPD3LUASCRIPT_EXPORT QDebug operator<<(QDebug stream, const Output &v);

    friend CPD3LUASCRIPT_EXPORT QDebug operator<<(QDebug stream, const Reference &v);

    friend CPD3LUASCRIPT_EXPORT QDebug operator<<(QDebug stream, const Table &v);
};

CPD3LUASCRIPT_EXPORT QDebug operator<<(QDebug stream, const Engine::Output &v);

CPD3LUASCRIPT_EXPORT QDebug operator<<(QDebug stream, const Engine::Reference &v);

CPD3LUASCRIPT_EXPORT QDebug operator<<(QDebug stream, const Engine::Table &v);

}
}

#endif //CPD3LUASCRIPT_ENGINE_HXX
