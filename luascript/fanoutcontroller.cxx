/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <unordered_set>

#include "fanoutcontroller.hxx"
#include "libs/sequencename.hxx"


namespace CPD3 {
namespace Lua {

class FanoutController::Context {
    FanoutController *parent;

    std::size_t nextSavedIndex;
    std::size_t nextResponseIndex;

    std::unordered_set<std::shared_ptr<Target>> targets;
    Data::SequenceName::Map<std::vector<std::shared_ptr<Target>>> dispatchLookup;

    struct FanoutKeyData {
        std::shared_ptr<Target> target;
        std::vector<std::weak_ptr<Target>> foreground;
        Data::SequenceName::Set dispatched;
    };

    /* Has to be pointers, since we write to it while we hold references (during
     * foreground insertion) */
    Data::SequenceName::Map<std::unique_ptr<FanoutKeyData>> nameKeyLookup;

    class LuaKeyData : public Engine::Data {
        FanoutKeyData data;
    public:
        LuaKeyData() = default;

        virtual ~LuaKeyData() = default;

        FanoutKeyData &get()
        { return data; }
    };

    struct StandardFanout {
        bool station;
        bool archive;
        bool variable;
        bool flavors;

        bool mergedMetadata;
        bool mergedCutSizes;

        bool ignoreMetadata;
        bool ignoreDefaultFlavors;

        StandardFanout() : station(true),
                           archive(true),
                           variable(false),
                           flavors(true),
                           mergedMetadata(true),
                           mergedCutSizes(false),
                           ignoreMetadata(true),
                           ignoreDefaultFlavors(true)
        { }

        bool flatten(Data::SequenceName &target) const
        {
            if (!station)
                target.setStation(std::string());

            if (ignoreMetadata) {
                if (target.isMeta())
                    return false;
            }

            if (!archive) {
                target.setArchive(std::string());
            } else {
                if (mergedMetadata)
                    target.clearMeta();
            }

            if (!variable)
                target.setVariable(std::string());

            if (ignoreDefaultFlavors) {
                for (const auto &f : Data::SequenceName::defaultLacksFlavors()) {
                    if (target.hasFlavor(f))
                        return false;
                }
            }

            if (!flavors) {
                target.clearFlavors();
            } else {
                if (mergedCutSizes)
                    target.removeFlavors(Data::SequenceName::cutSizeFlavors());
            }

            return true;
        }

        static void maybeSet(bool &target, const Engine::Output &origin)
        {
            if (origin.isNil())
                return;
            target = origin.toBoolean();
        }

        void overlay(const Engine::Table &settings)
        {
            maybeSet(station, settings.get("station"));
            maybeSet(archive, settings.get("archive"));
            maybeSet(variable, settings.get("variable"));
            maybeSet(flavors, settings.get("flavors"));
            maybeSet(mergedMetadata, settings.get("mergedMetadata"));
            maybeSet(mergedCutSizes, settings.get("mergedCutSizes"));
            maybeSet(ignoreMetadata, settings.get("ignoreMetadata"));
            maybeSet(ignoreDefaultFlavors, settings.get("ignoreDefaultFlavors"));
        }
    };

    StandardFanout standardFanout;


    void initializeTarget(Engine::Frame &frame,
                          const Engine::Reference &controller,
                          const Engine::Reference &key,
                          const std::vector<std::shared_ptr<Target>> &background,
                          Target *target)
    {
        Engine::Frame local(frame);
        auto mt = Engine::Table(controller).pushMetatable(local);
        auto saved = local.pushTable();
        auto context = local.pushTable();

        {
            Engine::Frame globalContext(local);
            globalContext.push(mt, "saved");
            Engine::Table(globalContext.back()).rawSet(nextSavedIndex, saved);
            target->savedIndex = nextSavedIndex;
            ++nextSavedIndex;
        }

        {
            Engine::Call initCall(local);
            initCall.push(mt, "create");
            bool doInvoke = false;
            {
                Engine::Frame arguments(initCall);
                doInvoke = target->initializeCall(arguments, controller, key, background, context);
            }
            if (doInvoke) {
                if (!initCall.executeVariable()) {
                    initCall.clearError();
                    initCall.clear();
                    /* Always do the processing, so the target is set up regardless */
                }
            }

            target->processSaved(initCall, controller, key, background, context);

            for (std::size_t i = 0, max = initCall.size(); i < max; i++) {
                saved.rawSet(i + 1, initCall[i]);
            }
        }

        {
            Engine::Frame temp(local);
            auto mt = Engine::Table(controller).pushMetatable(temp);
            temp.push(mt, "saved");
            temp.pushRaw(temp.back(), target->savedIndex);
        }
    }

    FanoutKeyData &getNameKeyData(const Data::SequenceName &name)
    {
        auto check = nameKeyLookup.find(name);
        if (check != nameKeyLookup.end())
            return *(check->second);
        return *(nameKeyLookup.emplace(name, std::unique_ptr<FanoutKeyData>(new FanoutKeyData()))
                              .first
                              ->second);
    }

    FanoutKeyData &handleName(Engine::Frame &frame,
                              const Engine::Reference &controller,
                              const Engine::Reference &key,
                              const Engine::Output &type,
                              const std::vector<std::shared_ptr<Target>> &background,
                              const Data::SequenceName &name)
    {
        Q_ASSERT(parent);
        auto &data = getNameKeyData(name);
        if (data.target)
            return data;

        data.target = parent->createTarget(type);
        Q_ASSERT(data.target);
        initializeTarget(frame, controller, key, background, data.target.get());
        return data;
    }

    FanoutKeyData &getLuaKeyData(Engine::Frame &frame,
                                 const Engine::Reference &controller,
                                 const Engine::Reference &key)
    {
        Engine::Frame local(frame);
        auto mt = Engine::Table(controller).pushMetatable(local);
        local.push(mt, "translate");
        local.push(local.back(), key);
        if (auto check = local.back().toData<LuaKeyData>()) {
            /* Since we never overwrite entries and the metatable is only accessible by
             * us, it should be save to return a pointer to the key data.  That is, the GC
             * should never delete the key data while we keep this pointer around. */
            return check->get();
        }
        local.pop();
        Engine::Assign assign(local, local.back(), key);
        return assign.pushData<LuaKeyData>()->get();
    }

    FanoutKeyData &handleLua(Engine::Frame &frame,
                             const Engine::Reference &controller,
                             const Engine::Reference &key,
                             const Engine::Output &type,
                             const std::vector<std::shared_ptr<Target>> &background)
    {
        Q_ASSERT(parent);
        auto &data = getLuaKeyData(frame, controller, key);
        if (data.target)
            return data;

        data.target = parent->createTarget(type);
        Q_ASSERT(data.target);
        initializeTarget(frame, controller, key, background, data.target.get());
        return data;
    }

    FanoutKeyData &getKeyData(Engine::Frame &frame,
                              const Engine::Reference &controller,
                              const Engine::Reference &key)
    {
        if (auto check = key.toData<Libs::SequenceName>())
            return getNameKeyData(check->get());
        return getLuaKeyData(frame, controller, key);
    }

    FanoutKeyData &handleKey(Engine::Frame &frame,
                             const Engine::Reference &controller,
                             const Engine::Reference &key,
                             const Engine::Output &type,
                             const std::vector<std::shared_ptr<Target>> &background)
    {
        if (auto check = key.toData<Libs::SequenceName>())
            return handleName(frame, controller, key, type, background, check->get());
        return handleLua(frame, controller, key, type, background);
    }

    static void attachBackground(FanoutKeyData &data,
                                 std::unordered_set<FanoutKeyData *> &backgroundData,
                                 std::vector<std::shared_ptr<Target>> &backgroundTargets)
    {
        backgroundData.insert(&data);
        if (data.target)
            backgroundTargets.emplace_back(data.target);
    }

    void attachForeground(FanoutKeyData &data,
                          const std::unordered_set<FanoutKeyData *> &backgroundData,
                          std::vector<std::shared_ptr<Target>> &result)
    {
        /* Add to result */
        result.emplace_back(data.target);

        /* Add any foreground targets already known to the result */
        for (auto &check : data.foreground) {
            auto v = check.lock();
            if (!v)
                continue;
            result.emplace_back(std::move(v));
        }

        /* Attach to background */
        for (auto bd : backgroundData) {
            bd->foreground.emplace_back(data.target);

            for (const auto &add : bd->dispatched) {
                dispatchLookup[add].emplace_back(data.target);
            }
        }
    }

public:
    explicit Context(FanoutController *parent) : parent(parent),
                                                 nextSavedIndex(1),
                                                 nextResponseIndex(1)
    { }

    ~Context() = default;

    void decouple()
    { parent = nullptr; }

    const std::vector<std::shared_ptr<Target>> &dispatch(Engine::Frame &frame,
                                                         const Engine::Reference &controller,
                                                         const Data::SequenceName &target)
    {
        Q_ASSERT(parent);

        /* If we've already seen it, then just return the targets */
        {
            auto check = dispatchLookup.find(target);
            if (check != dispatchLookup.end()) {
                return check->second;
            }
        }

        auto &result = dispatchLookup.emplace(target, std::vector<std::shared_ptr<Target>>())
                                     .first
                                     ->second;

        {
            Engine::Call keyInvoke(frame);
            auto mt = Engine::Table(controller).pushMetatable(keyInvoke);
            keyInvoke.push(mt, "key");
            if (!keyInvoke.back().toBoolean()) {
                auto flat = target;
                if (standardFanout.flatten(flat)) {
                    keyInvoke.pushData<Libs::SequenceName>(flat);
                    auto key = keyInvoke.back();

                    if (!standardFanout.station) {
                        /* When not fanning out stations, there are no background targets */
                        auto &data =
                                handleName(keyInvoke, controller, key, Engine::Output(false), {},
                                           flat);
                        result.emplace_back(data.target);
                        data.dispatched.insert(target);
                    } else if (flat.isDefaultStation()) {
                        /* Create or get the existing background target */
                        auto &data =
                                handleName(keyInvoke, controller, key, Engine::Output(true), {},
                                           flat);
                        result.emplace_back(data.target);
                        data.dispatched.insert(target);

                        /* Add any foreground targets associated with it to the dispatch
                         * result so they also receive background data */
                        for (auto &check : data.foreground) {
                            auto v = check.lock();
                            if (!v)
                                continue;
                            result.emplace_back(std::move(v));
                        }
                    } else {
                        /* Get any existing background (but do not create it, if
                         * it doesn't already exist */
                        std::vector<std::shared_ptr<Target>> background;
                        auto &backgroundData = getNameKeyData(flat.toDefaultStation());
                        if (backgroundData.target) {
                            background.emplace_back(backgroundData.target);
                        }

                        /* Get or create the foreground target */
                        auto &data = handleName(keyInvoke, controller, key, Engine::Output(false),
                                                background, flat);
                        result.emplace_back(data.target);
                        data.dispatched.insert(target);

                        /* Associate it with the background */
                        backgroundData.foreground.emplace_back(data.target);
                        for (const auto &add : backgroundData.dispatched) {
                            dispatchLookup[add].emplace_back(data.target);
                        }
                    }
                }
            } else {
                keyInvoke.replaceWithBack(0);
                keyInvoke.pushData<Libs::SequenceName>(target);
                if (!keyInvoke.execute(3)) {
                    keyInvoke.clearError();
                    parent->addedDispatch(keyInvoke, controller, target, result);
                    Util::merge(result, targets);
                    return result;
                }
                auto createKeys = keyInvoke[0];
                auto createType = keyInvoke[1];
                auto backgroundKeys = keyInvoke[2];

                /* Lookup and initialize the background */
                std::unordered_set<FanoutKeyData *> backgroundData;
                std::vector<std::shared_ptr<Target>> backgroundTargets;
                switch (backgroundKeys.getType()) {
                case Engine::Reference::Type::Nil:
                    break;
                case Engine::Reference::Type::Boolean:
                    if (!backgroundKeys.toBoolean())
                        break;
                    /* Fall through */
                case Engine::Reference::Type::Number:
                case Engine::Reference::Type::String: {
                    auto &data = getLuaKeyData(keyInvoke, controller, backgroundKeys);
                    attachBackground(data, backgroundData, backgroundTargets);
                    break;
                }
                case Engine::Reference::Type::Data: {
                    auto &data = getKeyData(keyInvoke, controller, backgroundKeys);
                    attachBackground(data, backgroundData, backgroundTargets);
                    break;
                }
                case Engine::Reference::Type::Table: {
                    Engine::Iterator it(keyInvoke, backgroundKeys);
                    while (it.next()) {
                        auto &data = getKeyData(it, controller, it.value());
                        attachBackground(data, backgroundData, backgroundTargets);
                    }
                    break;
                }
                }

                /* Create the foreground, and associate it with the background */
                switch (createKeys.getType()) {
                case Engine::Reference::Type::Nil:
                    break;
                case Engine::Reference::Type::Boolean:
                    if (!createKeys.toBoolean())
                        break;
                    /* Fall through */
                case Engine::Reference::Type::Number:
                case Engine::Reference::Type::String: {
                    auto &data = handleLua(keyInvoke, controller, createKeys, createType.get(),
                                           backgroundTargets);
                    attachForeground(data, backgroundData, result);
                    break;
                }
                case Engine::Reference::Type::Table: {
                    Engine::Iterator it(keyInvoke, createKeys);
                    Engine::Output type;
                    bool dynamicType = (createType.getType() == Engine::Reference::Type::Table);
                    if (!dynamicType)
                        type = createType.get();
                    while (it.next()) {
                        if (dynamicType)
                            type = Engine::Table(createType).get(it.key());

                        auto &data = handleKey(it, controller, it.value(), type, backgroundTargets);
                        attachForeground(data, backgroundData, result);
                    }
                    break;
                }
                case Engine::Reference::Type::Data: {
                    auto &data = handleKey(keyInvoke, controller, createKeys, createType.get(),
                                           backgroundTargets);
                    attachForeground(data, backgroundData, result);
                    break;
                }
                }
            }
        }

        parent->addedDispatch(frame, controller, target, result);
        Util::merge(result, targets);
        return result;
    }

    const std::unordered_set<std::shared_ptr<Target>> &allTargets() const
    { return targets; }

    void setFanout(const Engine::Table &settings)
    {
        standardFanout.overlay(settings);
    }

    void applyFlatten(Engine::Entry &frame)
    {
        auto input = Libs::SequenceName::extract(frame, frame[1]);
        StandardFanout settings = standardFanout;
        if (frame.size() > 2)
            settings.overlay(frame[2]);
        settings.flatten(input);
        frame.pushData<Libs::SequenceName>(std::move(input));
        frame.replaceWithBack(0);
        frame.propagate(1);
    }
};

class FanoutController::Controller : public Engine::Data {
    std::weak_ptr<Context> context;

    void meta_call(Engine::Entry &entry)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid arguments");
            return;
        }
        auto mt = Engine::Table(entry.front()).pushMetatable(entry);
        mt.set("create", entry[1]);
    }

    void meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid index");
            return;
        }
        if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
            return;

        auto key = entry[1].toString();
        if (key == "key") {
            auto mt = Engine::Table(entry.front()).pushMetatable(entry);
            entry.push(mt, "key");
            entry.replaceWithBack(0);
            entry.propagate(1);
            return;
        } else if (key == "create") {
            auto mt = Engine::Table(entry.front()).pushMetatable(entry);
            entry.push(mt, "create");
            entry.replaceWithBack(0);
            entry.propagate(1);
            return;
        }

        entry.clear();
        entry.pushNil();
        entry.propagate(1);
    }

    void meta_newindex(Engine::Entry &entry)
    {
        if (entry.size() < 3) {
            entry.throwError("Invalid assignment");
            return;
        }
        auto key = entry[1].toString();
        if (key == "key") {
            auto mt = Engine::Table(entry.front()).pushMetatable(entry);
            mt.set("key", entry[2]);
            return;
        } else if (key == "create") {
            auto mt = Engine::Table(entry.front()).pushMetatable(entry);
            mt.set("create", entry[2]);
            return;
        } else if (key == "fanout") {
            {
                auto mt = Engine::Table(entry.front()).pushMetatable(entry);
                Engine::Assign assign(entry, mt, "key");
                assign.pushNil();
            }
            if (auto ctx = context.lock()) {
                ctx->setFanout(entry[2]);
            }
            return;
        }

        entry.throwError("Invalid assignment");
    }

    void method_configure(Engine::Entry &entry)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid arguments");
            return;
        }
        {
            auto mt = Engine::Table(entry.front()).pushMetatable(entry);
            Engine::Assign assign(entry, mt, "key");
            assign.pushNil();
        }
        if (auto ctx = context.lock()) {
            ctx->setFanout(entry[1]);
        }
    }

    void method_applyFlatten(Engine::Entry &entry)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid arguments");
            return;
        }
        if (auto ctx = context.lock()) {
            ctx->applyFlatten(entry);
        }
    }

public:
    explicit Controller(const std::shared_ptr<Context> &context) : context(context)
    { }

    virtual ~Controller() = default;

protected:
    void initialize(const Engine::Reference &self, Engine::Frame &frame) override
    {
        auto mt = Engine::Data::pushMetatable(frame);

        {
            Engine::Assign assign(frame, mt, "translate");
            assign.pushTable();
        }
        {
            Engine::Assign assign(frame, mt, "saved");
            assign.pushTable();
        }
        {
            Engine::Assign assign(frame, mt, "key");
            assign.pushNil();
        }
        {
            Engine::Assign assign(frame, mt, "create");
            assign.pushNil();
        }

        {
            Engine::Assign assign(frame, mt, "__call");
            pushMethod<Controller>(assign, &Controller::meta_call);
        }
        {
            Engine::Assign assign(frame, mt, "__newindex");
            pushMethod<Controller>(assign, &Controller::meta_newindex);
        }
        {
            Engine::Assign assign(frame, mt, "__index");
            {
                auto methods = assign.pushTable();

                {
                    Engine::Assign ma(assign, methods, "setCreate");
                    pushMethod<Controller>(ma, &Controller::meta_call);
                }
                {
                    Engine::Assign ma(assign, methods, "configure");
                    pushMethod<Controller>(ma, &Controller::method_configure);
                }
                {
                    Engine::Assign ma(assign, methods, "applyFlatten");
                    pushMethod<Controller>(ma, &Controller::method_applyFlatten);
                }
            }
            pushMethod<Controller>(assign, &Controller::meta_index, 1);
        }

        setMetatable(self, mt);
    }
};

FanoutController::FanoutController() : context(std::make_shared<Context>(this))
{ }

FanoutController::~FanoutController()
{ context->decouple(); }

void FanoutController::pushController(Engine::Frame &target)
{ target.pushData<Controller>(context); }

const std::vector<
        std::shared_ptr<FanoutController::Target>> &FanoutController::dispatch(Engine::Frame &frame,
                                                                               const Engine::Reference &controller,
                                                                               const Data::SequenceName &target)
{ return context->dispatch(frame, controller, target); }

const std::unordered_set<
        std::shared_ptr<FanoutController::Target>> &FanoutController::allTargets() const
{ return context->allTargets(); }

void FanoutController::addedDispatch(Engine::Frame &,
                                     const Engine::Reference &,
                                     const Data::SequenceName &,
                                     std::vector<std::shared_ptr<Target>> &)
{ }


FanoutController::Target::Target() : savedIndex(0)
{ }

FanoutController::Target::~Target() = default;

void FanoutController::Target::pushSaved(Engine::Frame &frame,
                                         const Engine::Reference &controller,
                                         std::size_t index) const
{
    Q_ASSERT(savedIndex != 0);

    Engine::Frame result(frame);
    auto mt = Engine::Table(controller).pushMetatable(result);
    result.push(mt, "saved");
    result.pushRaw(result.back(), savedIndex);
    result.pushRaw(result.back(), index + 1);
    result.replaceWithBack(0);
    result.propagate(1);
}

void FanoutController::Target::processSaved(Engine::Frame &,
                                            const Engine::Reference &,
                                            const Engine::Reference &,
                                            const std::vector<std::shared_ptr<Target>> &,
                                            Engine::Table &)
{ }

bool FanoutController::Target::initializeCall(Engine::Frame &,
                                              const Engine::Reference &,
                                              const Engine::Reference &,
                                              const std::vector<std::shared_ptr<Target>> &,
                                              Engine::Table &)
{ return true; }

}
}