/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <mutex>
#include <QVector>
#include <QRunnable>
#include <QWaitCondition>
#include <QDebugStateSaver>

#include "core/threadpool.hxx"
#include "core/util.hxx"
#include "algorithms/model.hxx"
#include "algorithms/logwrapper.hxx"
#include "algorithms/cspline.hxx"
#include "algorithms/linearint.hxx"
#include "algorithms/leastsquares.hxx"
#include "algorithms/npolynomial.hxx"
#include "algorithms/cdf.hxx"
#include "algorithms/allan.hxx"
#include "algorithms/gaussiandensity.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Algorithms::Internal;

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/model.hxx
 * This is the general interface to various models.  A model is an algorithm
 * that transforms some number of floating point inputs into a set of outputs.
 * This is usually used to provide a model of a (possibly unknown) transform.
 */


/**
 * Create a model from the given configuration value.
 * 
 * @param configuration the configuration of the model
 * @param parameters    the model parameters
 * @return              a newly allocated model
 */
Model *Model::fromConfiguration(const Variant::Read &configuration,
                                const ModelParameters &parameters)
{
    return fromConfiguration(configuration, QList<ModelInputConstraints>(),
                             QList<ModelOutputConstraints>(), parameters);
}

/**
 * Create a model from the given configuration value that should support the
 * given number of inputs translated to the given number of outputs.
 * 
 * @param configuration the configuration of the model
 * @param parameters    the model parameters
 * @param nInput        the number of inputs
 * @param nOutput       the number of outputs
 * @return              a newly allocated model
 */
Model *Model::fromConfiguration(const Variant::Read &configuration,
                                int nInput,
                                int nOutput,
                                const ModelParameters &parameters)
{
    QList<ModelInputConstraints> inputs;
    while (nInput-- > 0) { inputs.append(ModelInputConstraints()); }
    QList<ModelOutputConstraints> outputs;
    while (nOutput-- > 0) { outputs.append(ModelOutputConstraints()); }
    return fromConfiguration(configuration, inputs, outputs, parameters);
}

/**
 * Create a model from the given configuration value that should support the
 * given outputs and inputs.
 * 
 * @param configuration the configuration of the model
 * @param parameters    the model parameters
 * @param inputs        the inputs
 * @param outputs       the outputs
 * @return              a newly allocated model
 */
Model *Model::fromConfiguration(const Variant::Read &configuration,
                                const QList<ModelInputConstraints> &inputs,
                                const QList<ModelOutputConstraints> &outputs,
                                const ModelParameters &parameters)
{
    return fromConfiguration(configuration, inputs, outputs, parameters, std::string());
}

Model *Model::fromConfiguration(const Variant::Read &configuration,
                                const QList<ModelInputConstraints> &inputs,
                                const QList<ModelOutputConstraints> &outputs,
                                const ModelParameters &parameters, const std::string &overrideType)
{
    std::string type;
    if (!overrideType.empty())
        type = overrideType;
    else
        type = configuration["Type"].toString();

    if (Util::equal_insensitive(type, "Composite")) {
        return new ModelSingleComposite(configuration, inputs, outputs, parameters);
    }

    if (Util::equal_insensitive(type, "CSpline", "CublicSpline")) {
        if (inputs.size() > 1 || outputs.size() > 1) {
            return new ModelSingleComposite(configuration, inputs, outputs, parameters, "cspline");
        }

        double dXBoth = configuration["Clamp"].toDouble();
        double dX0;
        double dXN;
        if (configuration["ClampStart"].exists()) {
            dX0 = configuration["ClampStart"].toDouble();
        } else {
            dX0 = dXBoth;
        }
        if (configuration["ClampEnd"].exists()) {
            dXN = configuration["ClampEnd"].toDouble();
        } else {
            dXN = dXBoth;
        }

        QVector<double> y(parameters.getDimension(1));
        ModelOutputConstraints c;
        if (!outputs.isEmpty()) {
            c = outputs.at(0);
            applyInterpolateConstraints(c, y.begin(), y.end());
        }

        return CSplineInterpolator::create(parameters.getDimension(0), y, dX0, dXN, c);
    }

    if (Util::equal_insensitive(type, "Linear")) {
        if (inputs.size() > 1 || outputs.size() > 1) {
            return new ModelSingleComposite(configuration, inputs, outputs, parameters, "linear");
        }

        QVector<double> y(parameters.getDimension(1));
        ModelOutputConstraints c;
        if (!outputs.isEmpty()) {
            c = outputs.at(0);
            applyInterpolateConstraints(c, y.begin(), y.end());
        }

        const auto &transferType = configuration["Transfer"].toString();
        LinearInterpolator::LinearModelType mt;
        if (Util::equal_insensitive(transferType, "sin"))
            mt = LinearInterpolator::Type_Sin;
        else if (Util::equal_insensitive(transferType, "cos"))
            mt = LinearInterpolator::Type_Cos;
        else if (Util::equal_insensitive(transferType, "sigmoid"))
            mt = LinearInterpolator::Type_Sigmoid;
        else
            mt = LinearInterpolator::Type_Linear;

        return LinearInterpolator::create(parameters.getDimension(0), y, c, mt,
                                          configuration["TransferParameter"].toDouble());
    }

    if (Util::equal_insensitive(type, "LSQ", "LeastSquares")) {
        if (inputs.size() > 1 || outputs.size() > 1) {
            return new ModelSingleComposite(configuration, inputs, outputs, parameters, "lsq");
        }

        QVector<double> y(parameters.getDimension(1));
        ModelOutputConstraints c;
        if (!outputs.isEmpty()) {
            c = outputs.at(0);
            applyInterpolateConstraints(c, y.begin(), y.end());
        }

        return new BasicOrdinaryLeastSquares(parameters.getDimension(0), y, c);
    }
    if (Util::equal_insensitive(type, "ZLSQ", "ZeroLeastSquares")) {
        if (inputs.size() > 1 || outputs.size() > 1) {
            return new ModelSingleComposite(configuration, inputs, outputs, parameters, "zlsq");
        }

        QVector<double> y(parameters.getDimension(1));
        ModelOutputConstraints c;
        if (!outputs.isEmpty()) {
            c = outputs.at(0);
            applyInterpolateConstraints(c, y.begin(), y.end());
        }

        return new ZeroOrdinaryLeastSquares(parameters.getDimension(0), y, c);
    }

    if (Util::equal_insensitive(type, "NPoly", "NPolynomial")) {
        if (inputs.size() > 1 || outputs.size() > 1) {
            return new ModelSingleComposite(configuration, inputs, outputs, parameters, "npoly");
        }

        QVector<double> y(parameters.getDimension(1));
        ModelOutputConstraints c;
        if (!outputs.isEmpty()) {
            c = outputs.at(0);
            applyInterpolateConstraints(c, y.begin(), y.end());
        }

        qint64 n = configuration["Order"].toInt64();
        if (INTEGER::defined(n) || n < 1 || n > 65535)
            n = 2;

        return new NPolynomialLMA(parameters.getDimension(0), y, (int) n, c);
    }
    if (Util::equal_insensitive(type, "ZNPoly", "ZeroNPolynomial")) {
        if (inputs.size() > 1 || outputs.size() > 1) {
            return new ModelSingleComposite(configuration, inputs, outputs, parameters, "znpoly");
        }

        QVector<double> y(parameters.getDimension(1));
        ModelOutputConstraints c;
        if (!outputs.isEmpty()) {
            c = outputs.at(0);
            applyInterpolateConstraints(c, y.begin(), y.end());
        }

        qint64 n = configuration["Order"].toInt64();
        if (INTEGER::defined(n) || n < 1 || n > 65535)
            n = 2;

        return new ZeroNPolynomialLMA(parameters.getDimension(0), y, (int) n, c);
    }

    if (Util::equal_insensitive(type, "CDF", "NormalCDF")) {
        if (inputs.size() > 1 || outputs.size() > 1) {
            return new ModelSingleComposite(configuration, inputs, outputs, parameters,
                                            "normalcdf");
        }

        ModelOutputConstraints c;
        if (!outputs.isEmpty()) {
            c = outputs.at(0);
        }
        return new NormalCDFFit(parameters.getDimension(0), c);
    }

    if (Util::equal_insensitive(type, "Allan")) {
        if (inputs.size() > 1 || outputs.size() > 1) {
            return new ModelSingleComposite(configuration, inputs, outputs, parameters, "allan");
        }

        QVector<double> y(parameters.getDimension(1));
        ModelOutputConstraints c;
        if (!outputs.isEmpty()) {
            c = outputs.at(0);
            applyInterpolateConstraints(c, y.begin(), y.end());
        }

        double startInterval = 10.0;
        if (configuration["Start"].exists()) {
            startInterval = configuration["Start"].toDouble();
            if (FP::defined(startInterval) && startInterval < 0.0)
                startInterval = 10.0;
        }

        double endInterval = 100.0;
        if (configuration["End"].exists()) {
            endInterval = configuration["End"].toDouble();
            if (FP::defined(endInterval) && endInterval < 0.0)
                endInterval = 100.0;
        }

        double interceptOffset = configuration["Intercept"].toDouble();
        if (!FP::defined(interceptOffset) || interceptOffset <= 0.0)
            interceptOffset = 60.0;

        return new AllanFit(parameters.getDimension(0), y, startInterval, endInterval,
                            interceptOffset, c);
    }

    if (Util::equal_insensitive(type, "Density", "GaussianDensity")) {
        if (inputs.size() != 2) {
            return new ModelConstant(FP::undefined());
        }

        ModelOutputConstraints c;
        if (!outputs.isEmpty()) {
            c = outputs.at(0);
        }
        return new GaussianDensity2D(parameters.getDimension(0), parameters.getDimension(1),
                                     configuration["Density"].toDouble(),
                                     configuration["Limit"].toDouble(), c);
    }

    if (inputs.size() > 1 || outputs.size() > 1) {
        return new ModelSingleComposite(configuration, inputs, outputs, parameters, "constant");
    }

    double value = configuration["Value"].toDouble();
    if (!outputs.isEmpty()) {
        if (!configuration["Value"].exists()) {
            double sum = 0.0;
            int count = 0;
            QVector<double> y(parameters.getDimension(1));
            if (configuration["GeometricAverage"].toBool()) {
                for (int i = 0, max = y.size(); i < max; i++) {
                    if (!FP::defined(y.at(i)))
                        continue;
                    sum *= outputs.at(0).apply(y.at(i));
                    count++;
                }
                if (count != 0)
                    value = pow(sum, 1.0 / (double) count);
            } else {
                for (int i = 0, max = y.size(); i < max; i++) {
                    if (!FP::defined(y.at(i)))
                        continue;
                    sum += outputs.at(0).apply(y.at(i));
                    count++;
                }
                if (count != 0)
                    value = sum / (double) count;
            }
        }
        value = outputs.at(0).apply(value);
    }
    return new ModelConstant(value);
}

namespace Internal {
class DeferredModelWrapperRunnable : public QRunnable {
    Variant::Read configuration;
    QList<ModelInputConstraints> inputs;
    QList<ModelOutputConstraints> outputs;
    ModelParameters parameters;
    DeferredModelWrapper *wrapper;
public:
    DeferredModelWrapperRunnable(const Variant::Read &c,
                                 const QList<ModelInputConstraints> &i,
                                 const QList<ModelOutputConstraints> &o,
                                 const ModelParameters &p,
                                 DeferredModelWrapper *w) : configuration(c),
                                                            inputs(i),
                                                            outputs(o),
                                                            parameters(p),
                                                            wrapper(w)
    {
        configuration.detachFromRoot();
        setAutoDelete(true);
    }

    virtual void run()
    {
        if (wrapper == NULL)
            return;
        Model *model = Model::fromConfiguration(configuration, inputs, outputs, parameters);
        wrapper->notifyModelReady(model);
        wrapper = NULL;
    }
};
}

/**
 * Create a model from the given configuration value.  The wrapper retains 
 * ownership of the model unless takeModel() is used.
 * 
 * @param configuration the configuration of the model
 * @param parameters    the model parameters
 * @return              a newly allocated model
 */
DeferredModelWrapper::DeferredModelWrapper(const Variant::Read &configuration,
                                           const ModelParameters &parameters) : model(NULL),
                                                                                finished(0)
{
    ThreadPool::system()->run(
            new DeferredModelWrapperRunnable(configuration, QList<ModelInputConstraints>(),
                                             QList<ModelOutputConstraints>(), parameters, this));
}

/**
 * Create a model from the given configuration value that should support the
 * given number of inputs translated to the given number of outputs.  The 
 * wrapper retains ownership of the model unless takeModel() is used.
 * 
 * @param configuration the configuration of the model
 * @param parameters    the model parameters
 * @param nInput        the number of inputs
 * @param nOutput       the number of outputs
 * @return              a newly allocated model
 */
DeferredModelWrapper::DeferredModelWrapper(const Variant::Read &configuration,
                                           int nInput,
                                           int nOutput,
                                           const ModelParameters &parameters) : model(NULL),
                                                                                finished(0)
{
    QList<ModelInputConstraints> inputs;
    while (nInput-- > 0) { inputs.append(ModelInputConstraints()); }
    QList<ModelOutputConstraints> outputs;
    while (nOutput-- > 0) { outputs.append(ModelOutputConstraints()); }
    ThreadPool::system()->run(
            new DeferredModelWrapperRunnable(configuration, inputs, outputs, parameters, this));
}

/**
 * Create a model from the given configuration value that should support the
 * given outputs and inputs.  The wrapper retains ownership of the model unless
 * takeModel() is used.
 * 
 * @param configuration the configuration of the model
 * @param parameters    the model parameters
 * @param inputs        the inputs
 * @param outputs       the outputs
 * @return              a newly allocated model
 */
DeferredModelWrapper::DeferredModelWrapper(const Variant::Read &configuration,
                                           const QList<ModelInputConstraints> &inputs,
                                           const QList<ModelOutputConstraints> &outputs,
                                           const ModelParameters &parameters) : model(NULL),
                                                                                finished(0)
{
    ThreadPool::system()->run(
            new DeferredModelWrapperRunnable(configuration, inputs, outputs, parameters, this));
}

DeferredModelWrapper::~DeferredModelWrapper()
{
    waitForModelReady();
    std::unique_lock<std::mutex> lock(mutex);
    while (finished != 1) {
        lock.unlock();
        QThread::yieldCurrentThread();
        lock.lock();
    }
    if (model != NULL) {
        delete model;
        model = NULL;
    }
}

/**
 * Test if the model has finished creation.
 * @return true if the model is ready
 */
bool DeferredModelWrapper::isModelReady()
{
    std::lock_guard<std::mutex> lock(mutex);
    return finished != 0;
}

/**
 * Wait for the model to be ready.  If the time is ULONG_MAX then this will
 * never time out, otherwise it is the maximum amount of time to wait.
 * 
 * @param time  the maximum time to wait in milliseconds or ULONG_MAX to wait forever
 * @return      true if the model is ready
 */
bool DeferredModelWrapper::waitForModelReady(unsigned long time)
{
    std::unique_lock<std::mutex> lock(mutex);
    if (finished != 0)
        return true;
    if (time != ULONG_MAX) {
        cond.wait_for(lock, std::chrono::milliseconds(time));
    } else {
        cond.wait(lock);
    }
    return finished != 0;
}

/**
 * Get the model, or NULL if it isn't created yet or has been taken
 * @return the model or NULL if it isn't finished yet or has been taken
 */
Model *DeferredModelWrapper::getModel()
{
    std::lock_guard<std::mutex> lock(mutex);
    return model;
}

/**
 * Take the model from the wrapper, if it is available.  Unless the model is
 * taken the wrapper will retain ownership of it (and will delete it when
 * the wrapper is deleted).
 * 
 * @return the model or NULL if it isn't created yet or has already been taken
 */
Model *DeferredModelWrapper::takeModel()
{
    std::lock_guard<std::mutex> lock(mutex);
    Model *m = model;
    model = NULL;
    return m;
}

/**
 * Delete the model wrapper (and the model if it hasn't been taken) once
 * creation is done.  This will not block the caller or event loop while
 * waiting for the model to finish.
 */
void DeferredModelWrapper::deleteWhenFinished()
{
    disconnect(this);
    connect(this, SIGNAL(modelReady()), this, SLOT(deleteLater()), Qt::QueuedConnection);
    std::unique_lock<std::mutex> lock(mutex);
    if (finished != 0) {
        lock.unlock();
        disconnect(this);
        deleteLater();
        return;
    }
}

void DeferredModelWrapper::notifyModelReady(Model *m)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        finished = 2;
        model = m;
    }
    cond.notify_all();
    emit modelReady();

    std::lock_guard<std::mutex> lock(mutex);
    finished = 1;
}


ModelParameters::ModelParameters()
{ }

ModelParameters::ModelParameters(const ModelParameters &other) : parameters(other.parameters)
{ }

ModelParameters &ModelParameters::operator=(const ModelParameters &other)
{
    if (&other == this) return *this;
    parameters = other.parameters;
    return *this;
}

ModelParameters::ModelParameters(const QHash<QString, QVariant> &params) : parameters(params)
{ }

/**
 * Get the parameters of the given name.
 * 
 * @param name  the parameter name
 * @return      the parameter value, null if not found
 */
QVariant ModelParameters::getParameter(const QString &name) const
{
    QHash<QString, QVariant>::const_iterator it = parameters.constFind(name);
    if (it == parameters.constEnd()) return QVariant();
    return it.value();
}

/**
 * Set the parameter of the given name to the given value.
 * 
 * @param name  the name of the parameter to set
 * @param value the new value of the parameter
 */
void ModelParameters::setParameter(const QString &name, const QVariant &value)
{
    parameters.insert(name, value);
}

/**
 * Get a double value of the given name.
 * 
 * @param name  the name of the parameter to get
 * @return      the value or FP::undefined() if not found
 */
double ModelParameters::getDouble(const QString &name) const
{
    QVariant v(getParameter(name));
    if (v.isNull()) return FP::undefined();
    switch (v.type()) {
    case QVariant::Double:
        return v.toDouble();
    case QVariant::LongLong: {
        qint64 i(v.toLongLong());
        if (INTEGER::defined(i))
            return (double) i;
        return FP::undefined();
    }
    default:
        if (!v.canConvert(QVariant::Double)) FP::undefined();
        return v.toDouble();
    }
}

/**
 * Get the total number of data dimensions defined.
 */
int ModelParameters::totalDimensions() const
{
    return getParameter("dimensions").toList().size();
}

/**
 * Get the value list of a given dimension.
 * 
 * @param dimension the dimension to retrieve
 * @return          the values of the dimension
 */
QVector<double> ModelParameters::getDimension(int dimension) const
{
    QVector<double> result;
    QList<QVariant> dimensions(getParameter("dimensions").toList());
    if (dimension >= dimensions.size()) return result;
    dimensions = dimensions.at(dimension).toList();
    for (int i = 0, max = dimensions.size(); i < max; i++) {
        QVariant v(dimensions.at(i));
        if (v.isNull()) {
            result.append(FP::undefined());
            continue;
        }
        switch (v.type()) {
        case QVariant::Double:
            result.append(v.toDouble());
            break;
        case QVariant::LongLong: {
            qint64 iv(v.toLongLong());
            if (INTEGER::defined(iv)) {
                result.append((double) iv);
                break;
            }
            result.append(FP::undefined());
            break;
        }
        default:
            if (!v.canConvert(QVariant::Double)) {
                result.append(FP::undefined());
                break;
            }
            result.append(v.toDouble());
        }
    }
    return result;
}

/**
 * Set a single double valued parameter.
 * 
 * @param name  the name of the parameter
 * @param value the value of the parameter
 */
void ModelParameters::setDouble(const QString &name, double value)
{
    Q_ASSERT(name != "dimensions");
    if (!FP::defined(value)) {
        parameters.remove(name);
        return;
    }
    setParameter(name, QVariant(value));
}

/**
 * Set the values on a given dimension.
 * 
 * @param n         the dimension to set
 * @param values    the values of the dimension
 */
void ModelParameters::setDimension(int n, const QVector<double> &values)
{
    QList<QVariant> dimensions(getParameter("dimensions").toList());
    while (dimensions.size() <= n) {
        dimensions.append(QVariant(QList<QVariant>()));
    }
    QList<QVariant> set;
    for (int i = 0, max = values.size(); i < max; i++) {
        if (!FP::defined(values.at(i)))
            set.append(QVariant());
        else
            set.append(QVariant(values.at(i)));
    }
    dimensions[n] = set;
    parameters.insert("dimensions", dimensions);
}

/**
 * Add a given value to a dimension
 * 
 * @param n         the dimension to add to
 * @param value     the value to append
 */
void ModelParameters::appendDimension(int n, double value)
{
    QList<QVariant> dimensions(getParameter("dimensions").toList());
    while (dimensions.size() <= n) {
        dimensions.append(QVariant(QList<QVariant>()));
    }
    QList<QVariant> set(dimensions.at(n).toList());
    if (!FP::defined(value))
        set.append(QVariant());
    else
        set.append(QVariant(value));
    dimensions[n] = set;
    parameters.insert("dimensions", dimensions);
}

/**
 * Remove all dimensions.
 */
void ModelParameters::clearDimensions()
{
    parameters.remove("dimensions");
}


ModelInputConstraints::ModelInputConstraints() : min(FP::undefined()), max(FP::undefined())
{ }

ModelInputConstraints::ModelInputConstraints(const ModelInputConstraints &other) : min(other.min),
                                                                                   max(other.max)
{ }

ModelInputConstraints &ModelInputConstraints::operator=(const ModelInputConstraints &other)
{
    if (&other == this) return *this;
    min = other.min;
    max = other.max;
    return *this;
}

/**
 * Construct a new model input constraint specification.
 * 
 * @param setMin    the minimum value the input can take
 * @param setMax    the maximum value the input can take
 */
ModelInputConstraints::ModelInputConstraints(double setMin, double setMax) : min(setMin),
                                                                             max(setMax)
{ }

/**
 * Get the minimum value the input can take.
 * 
 * @return the minimum value
 */
double ModelInputConstraints::getMin() const
{ return min; }

/**
 * Get the maximum value the input can take.
 * 
 * @return the maximum value
 */
double ModelInputConstraints::getMax() const
{ return max; }


ModelOutputConstraints::ModelOutputConstraints() : min(FP::undefined()),
                                                   max(FP::undefined()),
                                                   wrap(false),
                                                   noop(true)
{ }

ModelOutputConstraints::ModelOutputConstraints(const ModelOutputConstraints &other) : min(
        other.min), max(other.max), wrap(other.wrap), noop(other.noop)
{ }

ModelOutputConstraints &ModelOutputConstraints::operator=(const ModelOutputConstraints &other)
{
    if (&other == this) return *this;
    min = other.min;
    max = other.max;
    wrap = other.wrap;
    noop = other.noop;
    return *this;
}

/**
 * Construct a new output constraints.
 * 
 * @param setMin    the minimum to apply
 * @param setMax    the maximum to apply (exclusive if wrapping)
 * @param setWrap   if true then wrap the value between min and max (instead of clipping)
 */
ModelOutputConstraints::ModelOutputConstraints(double setMin, double setMax, bool setWrap) : min(
        setMin), max(setMax), wrap(setWrap)
{
    if (!FP::defined(min) && !FP::defined(max)) {
        noop = true;
        wrap = false;
        return;
    }
    if (!FP::defined(min) || !FP::defined(max)) {
        wrap = false;
    } else if (min >= max) {
        wrap = false;
        min = FP::undefined();
        max = FP::undefined();
        noop = true;
        return;
    }
    noop = false;
}

/**
 * Get the minimum allowed value.
 * 
 * @return  the minimum allowed output value
 */
double ModelOutputConstraints::getMin() const
{ return min; }

/**
 * Get the maximum allowed value.
 * 
 * @return  the maximum allowed output value
 */
double ModelOutputConstraints::getMax() const
{ return max; }

/**
 * Test if the output should be wrapped between the minimum and maximum instead
 * of just clipped if it exceeds one.
 * 
 * @return  true if the value should wrap
 */
bool ModelOutputConstraints::isWrapping() const
{ return wrap; }

/**
 * Test if this constraint does anything.
 * 
 * @return  true if the constraint has no effect
 */
bool ModelOutputConstraints::isNoop() const
{ return noop; }

/**
 * Apply the constraint to the given proposed output value
 */
double ModelOutputConstraints::apply(double in) const
{
    if (noop || !FP::defined(in)) return in;
    if (wrap) {
        if (in >= min && in < max) return in;
        double range = max - min;
        in -= min;
        return in - floor(in / range) * range + min;
    }
    if (FP::defined(min) && in < min) return min;
    if (FP::defined(max) && in > max) return max;
    return in;
}

ModelLegendParameters::ModelLegendParameters() : confidence(0.0), showOffset(true)
{ }

ModelLegendParameters::ModelLegendParameters(const ModelLegendParameters &other) : confidence(
        other.confidence), showOffset(other.showOffset)
{ }

ModelLegendParameters &ModelLegendParameters::operator=(const ModelLegendParameters &other)
{
    if (&other == this) return *this;
    confidence = other.confidence;
    showOffset = other.showOffset;
    return *this;
}

/**
 * Get the confidence percent displayed in the legend line.
 * 
 * @return the confidence percent [0,1], zero disables
 */
double ModelLegendParameters::getConfidence() const
{ return confidence; }

/**
 * Set the confidence percent displayed in the legend line.
 * 
 * @param c the confidence percent [0,1], zero disables
 */
void ModelLegendParameters::setConfidence(double c)
{ confidence = c; }

/**
 * Get if constant offsets should be shown in the legend line.
 * 
 * @return true if offsets should be shown
 */
bool ModelLegendParameters::getShowOffset() const
{ return showOffset; }

/**
 * Enable or disable the display of constant offsets in the legend line.
 * 
 * @param s true to enable constant offset display
 */
void ModelLegendParameters::setShowOffset(bool s)
{ showOffset = s; }


ModelLegendEntry::ModelLegendEntry() : line()
{ }

ModelLegendEntry::ModelLegendEntry(const ModelLegendEntry &other) : line(other.line)
{ }

ModelLegendEntry &ModelLegendEntry::operator=(const ModelLegendEntry &other)
{
    if (&other == this) return *this;
    line = other.line;
    return *this;
}

/**
 * Construct a legend line with the given text.
 * 
 * @param   the text to show
 */
ModelLegendEntry::ModelLegendEntry(const QString &text) : line(text)
{ }

/**
 * Get the legend line text.
 * 
 * @return  the legend text
 */
QString ModelLegendEntry::getLine() const
{ return line; }

/**
 * Set the legend line text.
 * 
 * @param l the legend text
 */
void ModelLegendEntry::setLine(const QString &l)
{ line = l; }


Model::~Model()
{ }

QVector<double> Model::apply(const QVector<double> &inputs)
{ return applyConst(inputs); }

void Model::applyIOConst(QVector<double> &values) const
{ values = applyConst(values); }

void Model::applyIO(QVector<double> &values)
{ values = apply(values); }

double Model::oneToOneConst(double input) const
{
    QVector<double> result;
    result.append(input);
    applyIOConst(result);
    if (result.size() < 1) return FP::undefined();
    return result.at(0);
}

double Model::oneToOne(double input)
{
    QVector<double> result;
    result.append(input);
    applyIO(result);
    if (result.size() < 1) return FP::undefined();
    return result.at(0);
}

double Model::twoToOneConst(double a, double b) const
{
    QVector<double> result;
    result.append(a);
    result.append(b);
    applyIOConst(result);
    if (result.size() < 1) return FP::undefined();
    return result.at(0);
}

double Model::twoToOne(double a, double b)
{
    QVector<double> result;
    result.append(a);
    result.append(b);
    applyIO(result);
    if (result.size() < 1) return FP::undefined();
    return result.at(0);
}

double Model::threeToOneConst(double a, double b, double c) const
{
    QVector<double> result;
    result.append(a);
    result.append(b);
    result.append(c);
    applyIOConst(result);
    if (result.size() < 1) return FP::undefined();
    return result.at(0);
}

double Model::threeToOne(double a, double b, double c)
{
    QVector<double> result;
    result.append(a);
    result.append(b);
    result.append(c);
    applyIO(result);
    if (result.size() < 1) return FP::undefined();
    return result.at(0);
}

void Model::applyIOConst(double &a, double &b) const
{
    QVector<double> result;
    result.append(a);
    result.append(b);
    applyIOConst(result);
    if (result.size() < 1) a = FP::undefined(); else a = result.at(0);
    if (result.size() < 2) b = FP::undefined(); else b = result.at(1);
}

void Model::applyIO(double &a, double &b)
{
    QVector<double> result;
    result.append(a);
    result.append(b);
    applyIO(result);
    if (result.size() < 1) a = FP::undefined(); else a = result.at(0);
    if (result.size() < 2) b = FP::undefined(); else b = result.at(1);
}

void Model::applyIOConst(double &a, double &b, double &c) const
{
    QVector<double> result;
    result.append(a);
    result.append(b);
    result.append(c);
    applyIOConst(result);
    if (result.size() < 1) a = FP::undefined(); else a = result.at(0);
    if (result.size() < 2) b = FP::undefined(); else b = result.at(1);
    if (result.size() < 3) c = FP::undefined(); else c = result.at(2);
}

void Model::applyIO(double &a, double &b, double &c)
{
    QVector<double> result;
    result.append(a);
    result.append(b);
    result.append(c);
    applyIO(result);
    if (result.size() < 1) a = FP::undefined(); else a = result.at(0);
    if (result.size() < 2) b = FP::undefined(); else b = result.at(1);
    if (result.size() < 3) c = FP::undefined(); else c = result.at(2);
}

QString Model::describe() const
{ return QString(); }

ModelLegendEntry Model::legend(const ModelLegendParameters &parameters) const
{
    Q_UNUSED(parameters);
    return ModelLegendEntry();
}

void Model::printLog(QDebug &stream) const
{ stream << "Model(Unknown)"; }

QString Model::legendPolynomialString(const QVector<double> &coefficients,
                                      const QVector<double> &confidence,
                                      const ModelLegendParameters &parameters)
{
    if (coefficients.isEmpty())
        return QString();

    NumberFormat cf;
    NumberFormat ef(1, 0);

    QString result;
    for (int i = 0, max = coefficients.size(); i < max; i++) {
        if (i == 0 && !parameters.getShowOffset())
            continue;
        double p = coefficients.at(i);
        if (!FP::defined(p))
            continue;
        if (p == 0.0 &&
                (i < confidence.size() ||
                        !FP::defined(confidence.at(i)) ||
                        confidence.at(i) <= 0.0))
            continue;
        if (!result.isEmpty()) {
            if (p < 0.0) {
                p = -p;
                result.append(" - ");
            } else {
                result.append(" + ");
            }
        }

        result.append(cf.apply(p, QChar()));
        if (i < confidence.size() && FP::defined(confidence.at(i)) && confidence.at(i) > 0.0) {
            result.append('(');
            result.append(QChar(0x00B1));
            result.append(cf.apply(confidence.at(i), QChar()));
            result.append(')');
        }
        if (i > 0)
            result.append('x');
        if (i > 1)
            result.append(ef.superscript(i));
    }

    if (result.isEmpty())
        return cf.apply(0.0, QChar());
    return result;
}

ModelLegendEntry Model::legendPolynomial(const QVector<double> &coefficients,
                                         const ModelLegendParameters &parameters)
{
    return ModelLegendEntry(legendPolynomialString(coefficients, QVector<double>(), parameters));
}


ModelConstant::~ModelConstant()
{ }

ModelConstant::ModelConstant(double v) : value(v)
{ }

QVector<double> ModelConstant::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(value);
    }
    return result;
}

QVector<double> ModelConstant::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(value);
    }
    return result;
}

void ModelConstant::applyIOConst(QVector<double> &values) const
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = value;
    }
}

void ModelConstant::applyIO(QVector<double> &values)
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = value;
    }
}

double ModelConstant::oneToOneConst(double input) const
{
    Q_UNUSED(input);
    return value;
}

double ModelConstant::oneToOne(double input)
{
    Q_UNUSED(input);
    return value;
}

double ModelConstant::twoToOneConst(double a, double b) const
{
    Q_UNUSED(a);
    Q_UNUSED(b);
    return value;
}

double ModelConstant::twoToOne(double a, double b)
{
    Q_UNUSED(a);
    Q_UNUSED(b);
    return value;
}

double ModelConstant::threeToOneConst(double a, double b, double c) const
{
    Q_UNUSED(a);
    Q_UNUSED(b);
    Q_UNUSED(c);
    return value;
}

double ModelConstant::threeToOne(double a, double b, double c)
{
    Q_UNUSED(a);
    Q_UNUSED(b);
    Q_UNUSED(c);
    return value;
}

void ModelConstant::applyIOConst(double &a, double &b) const
{
    a = value;
    b = value;
}

void ModelConstant::applyIO(double &a, double &b)
{
    a = value;
    b = value;
}

void ModelConstant::applyIOConst(double &a, double &b, double &c) const
{
    a = value;
    b = value;
    c = value;
}

void ModelConstant::applyIO(double &a, double &b, double &c)
{
    a = value;
    b = value;
    c = value;
}

Model *ModelConstant::clone() const
{ return new ModelConstant(value); }

QString ModelConstant::describe() const
{ return FP::decimalFormat(value); }

ModelLegendEntry ModelConstant::legend(const ModelLegendParameters &parameters) const
{
    Q_UNUSED(parameters);
    return NumberFormat().apply(value);
}

ModelConstant::ModelConstant(QDataStream &stream)
{ stream >> value; }

void ModelConstant::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_Constant;
    stream << t << value;
}

void ModelConstant::printLog(QDebug &stream) const
{ stream << "ModelConstant(" << value << ")"; }


ModelSingleComposite::~ModelSingleComposite()
{ qDeleteAll(models); }

ModelSingleComposite::ModelSingleComposite(QVector<Model *> sm) : models(sm)
{ }

ModelSingleComposite::ModelSingleComposite(const Data::Variant::Read &configuration,
                                           const QList<ModelInputConstraints> &inputs,
                                           const QList<ModelOutputConstraints> &outputs,
                                           const ModelParameters &parameters,
                                           const std::string &overrideType)
{
    Variant::Container::ReadArray components;
    std::size_t max = std::max<std::size_t>(static_cast<std::size_t>(inputs.size()),
                                            static_cast<std::size_t>(outputs.size()));
    if (overrideType.empty()) {
        components = configuration["Components"].toArray();
        max = std::max(max, components.size());
    }
    for (std::size_t i = 0; i < max; i++) {
        QList<ModelInputConstraints> input;
        int nInput = 0;
        if (static_cast<int>(i) < inputs.size())
            nInput = static_cast<int>(i);
        if (nInput < inputs.size())
            input.append(inputs.at(nInput));
        else
            input.append(ModelInputConstraints());

        QList<ModelOutputConstraints> output;
        int nOutput = 0;
        if (static_cast<int>(i) < outputs.size())
            nOutput = static_cast<int>(i);
        if (nOutput < outputs.size())
            output.append(outputs.at(nOutput));
        else
            output.append(ModelOutputConstraints());

        ModelParameters reducedParameters(parameters);
        reducedParameters.clearDimensions();
        if (nInput < inputs.size() && nInput < parameters.totalDimensions()) {
            reducedParameters.setDimension(0, parameters.getDimension(nInput));
        }
        if (nOutput < outputs.size() && nOutput + inputs.size() < parameters.totalDimensions()) {
            reducedParameters.setDimension(1, parameters.getDimension(nOutput + inputs.size()));
        }

        Variant::Read modelConfiguration;
        if (!overrideType.empty()) {
            modelConfiguration = configuration;
        } else if (i >= components.size() || !components[i].exists()) {
            modelConfiguration = configuration["DefaultComponent"];
        } else {
            modelConfiguration = components[i];
        }

        models.append(Model::fromConfiguration(modelConfiguration, input, output, reducedParameters,
                                               overrideType));
    }
}

QVector<double> ModelSingleComposite::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    for (int i = 0, max = qMin(models.size(), inputs.size()); i < max; i++) {
        result.append(models.at(i)->oneToOneConst(inputs.at(i)));
    }
    return result;
}

QVector<double> ModelSingleComposite::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    for (int i = 0, max = qMin(models.size(), inputs.size()); i < max; i++) {
        result.append(models[i]->oneToOne(inputs.at(i)));
    }
    return result;
}

void ModelSingleComposite::applyIOConst(QVector<double> &values) const
{
    int max = qMin(models.size(), values.size());
    for (int i = 0; i < max; i++) {
        values[i] = models.at(i)->oneToOneConst(values.at(i));
    }
    if (max < values.size())
        values.erase(values.begin() + max, values.end());
}

void ModelSingleComposite::applyIO(QVector<double> &values)
{
    int max = qMin(models.size(), values.size());
    for (int i = 0; i < max; i++) {
        values[i] = models[i]->oneToOne(values.at(i));
    }
    if (max < values.size())
        values.erase(values.begin() + max, values.end());
}

double ModelSingleComposite::oneToOneConst(double input) const
{
    if (models.isEmpty()) return FP::undefined();
    return models.at(0)->oneToOneConst(input);
}

double ModelSingleComposite::oneToOne(double input)
{
    if (models.isEmpty()) return FP::undefined();
    return models[0]->oneToOne(input);
}

double ModelSingleComposite::twoToOneConst(double a, double b) const
{
    Q_UNUSED(a);
    Q_UNUSED(b);
    if (models.isEmpty()) return FP::undefined();
    return models.at(0)->oneToOneConst(a);
}

double ModelSingleComposite::twoToOne(double a, double b)
{
    Q_UNUSED(a);
    Q_UNUSED(b);
    if (models.isEmpty()) return FP::undefined();
    return models[0]->oneToOne(a);
}

double ModelSingleComposite::threeToOneConst(double a, double b, double c) const
{
    Q_UNUSED(a);
    Q_UNUSED(b);
    Q_UNUSED(c);
    if (models.isEmpty()) return FP::undefined();
    return models.at(0)->oneToOneConst(a);
}

double ModelSingleComposite::threeToOne(double a, double b, double c)
{
    Q_UNUSED(a);
    Q_UNUSED(b);
    Q_UNUSED(c);
    if (models.isEmpty()) return FP::undefined();
    return models[0]->oneToOne(a);
}

void ModelSingleComposite::applyIOConst(double &a, double &b) const
{
    if (models.size() < 1) a = FP::undefined(); else a = models.at(0)->oneToOneConst(a);
    if (models.size() < 2) b = FP::undefined(); else b = models.at(1)->oneToOneConst(b);
}

void ModelSingleComposite::applyIO(double &a, double &b)
{
    if (models.size() < 1) a = FP::undefined(); else a = models[0]->oneToOne(a);
    if (models.size() < 2) b = FP::undefined(); else b = models[1]->oneToOne(b);
}

void ModelSingleComposite::applyIOConst(double &a, double &b, double &c) const
{
    if (models.size() < 1) a = FP::undefined(); else a = models.at(0)->oneToOneConst(a);
    if (models.size() < 2) b = FP::undefined(); else b = models.at(1)->oneToOneConst(b);
    if (models.size() < 3) c = FP::undefined(); else c = models.at(2)->oneToOneConst(c);
}

void ModelSingleComposite::applyIO(double &a, double &b, double &c)
{
    if (models.size() < 1) a = FP::undefined(); else a = models[0]->oneToOne(a);
    if (models.size() < 2) b = FP::undefined(); else b = models[1]->oneToOne(b);
    if (models.size() < 2) c = FP::undefined(); else c = models[2]->oneToOne(c);
}

Model *ModelSingleComposite::clone() const
{
    QVector<Model *> m;
    for (int i = 0, max = models.size(); i < max; i++) {
        m.append(models.at(i)->clone());
    }
    return new ModelSingleComposite(m);
}

QString ModelSingleComposite::describe() const
{
    QString result("Composite(");
    for (int i = 0, max = models.size(); i < max; i++) {
        if (i != 0) result.append(",");
        result.append(models.at(i)->describe());
    }
    result.append(")");
    return result;
}

ModelLegendEntry ModelSingleComposite::legend(const ModelLegendParameters &parameters) const
{
    QString result;
    for (int i = 0, max = models.size(); i < max; i++) {
        if (i != 0) result.append(", ");
        result.append(models.at(i)->legend(parameters).getLine());
    }
    return ModelLegendEntry(result);
}

ModelSingleComposite::ModelSingleComposite(QDataStream &stream)
{ stream >> models; }

void ModelSingleComposite::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_Composite;
    stream << t << models;
}

void ModelSingleComposite::printLog(QDebug &stream) const
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "Composite(";
    for (int i = 0, max = models.size(); i < max; i++) {
        if (i != 0) stream << ',';
        models.at(i)->printLog(stream);
    }
    stream << ')';
}


QDataStream &operator<<(QDataStream &stream, const Model *model)
{
    if (model == NULL) {
        quint8 t = (quint8) Model::SerialType_NULL;
        stream << t;
        return stream;
    }
    model->serialize(stream);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, Model *&model)
{
    quint8 tRaw;
    stream >> tRaw;
    switch ((Model::ModelSerialType) tRaw) {
    case Model::SerialType_NULL:
        model = NULL;
        return stream;
    case Model::SerialType_Constant:
        model = new ModelConstant(stream);
        return stream;
    case Model::SerialType_Composite:
        model = new ModelSingleComposite(stream);
        return stream;
    case Model::SerialType_LogLog:
        model = new LogWrapper(stream);
        return stream;
    case Model::SerialType_LogIn:
        model = new LogWrapper::LogIn(stream);
        return stream;
    case Model::SerialType_LogOut:
        model = new LogWrapper::LogOut(stream);
        return stream;
    case Model::SerialType_LogLogMultiple:
        model = new LogWrapperMultiple(stream);
        return stream;
    case Model::SerialType_Linear:
        model = new LinearInterpolator(stream);
        return stream;
    case Model::SerialType_LinearSingle:
        model = new SingleLinearInterpolator(stream);
        return stream;
    case Model::SerialType_LinearTransfer:
        model = new LinearTransferInterpolator(stream);
        return stream;
    case Model::SerialType_CSpline:
        model = new CSplineInterpolator(stream);
        return stream;
    case Model::SerialType_CSplineSingle:
        model = new CSplineInterpolator::Single(stream);
        return stream;
    case Model::SerialType_BasicOrdinaryLSQ:
        model = new BasicOrdinaryLeastSquares(stream);
        return stream;
    case Model::SerialType_ZeroOrdinaryLSQ:
        model = new ZeroOrdinaryLeastSquares(stream);
        return stream;
    case Model::SerialType_NormalCDFFit:
        model = new NormalCDFFit(stream);
        return stream;
    case Model::SerialType_AllanFit:
        model = new AllanFit(stream);
        return stream;
    case Model::SerialType_GuassianDensity2D:
        model = new GaussianDensity2D(stream);
        return stream;
    case Model::SerialType_NPolynomialLMA:
        model = new NPolynomialLMA(stream);
        return stream;
    case Model::SerialType_ZeroNPolynomialLMA:
        model = new ZeroNPolynomialLMA(stream);
        return stream;
    }
    Q_ASSERT(false);
    model = NULL;
    return stream;
}

QDebug operator<<(QDebug stream, const Model *model)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << static_cast<const void *>(model) << ":";
    if (model != NULL) {
        model->printLog(stream);
    } else {
        stream << "Model(NULL)";
    }
    return stream;
}

}
}
