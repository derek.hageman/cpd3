/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QVector>

#include "algorithms/linearint.hxx"

#ifndef M_PI
#define M_PI    3.14159265358979
#endif

using namespace CPD3::Data;

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/linearint.hxx
 * Provides routines for generating and simple linear interpolators.
 */

SingleLinearInterpolator::~SingleLinearInterpolator()
{ }

/**
 * Create a new linear interpolator that is defined by a slope and intercept.
 * 
 * @param sm    the slope
 * @param sb    the intercept
 */
SingleLinearInterpolator::SingleLinearInterpolator(double sm,
                                                   double sb,
                                                   const ModelOutputConstraints &output) : m(sm),
                                                                                           b(sb),
                                                                                           constraints(
                                                                                                   output)
{ }

/**
 * Create a new linear interpolator that is defined by the line between
 * two points.
 * 
 * @param x0    the first X coordinate
 * @param y0    the first Y coordinate
 * @param x1    the second X coordinate
 * @param y1    the second Y coordinate
 */
SingleLinearInterpolator::SingleLinearInterpolator(double x0,
                                                   double y0,
                                                   double x1,
                                                   double y1,
                                                   const ModelOutputConstraints &output)
{
    constraints = output;
    if (x0 == x1) {
        m = 0;
        b = (y0 + y1) * 0.5;
        return;
    }
    m = (y1 - y0) / (x1 - x0);
    b = y0 - m * x0;
}

QVector<double> SingleLinearInterpolator::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

QVector<double> SingleLinearInterpolator::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

void SingleLinearInterpolator::applyIOConst(QVector<double> &values) const
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

void SingleLinearInterpolator::applyIO(QVector<double> &values)
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

double SingleLinearInterpolator::oneToOneConst(double input) const
{ return evaluate(input); }

double SingleLinearInterpolator::oneToOne(double input)
{ return evaluate(input); }

double SingleLinearInterpolator::twoToOneConst(double a, double b) const
{
    Q_UNUSED(b);
    return evaluate(a);
}

double SingleLinearInterpolator::twoToOne(double a, double b)
{
    Q_UNUSED(b);
    return evaluate(a);
}

double SingleLinearInterpolator::threeToOneConst(double a, double b, double c) const
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

double SingleLinearInterpolator::threeToOne(double a, double b, double c)
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

void SingleLinearInterpolator::applyIOConst(double &a, double &b) const
{
    a = evaluate(a);
    b = evaluate(b);
}

void SingleLinearInterpolator::applyIO(double &a, double &b)
{
    a = evaluate(a);
    b = evaluate(b);
}

void SingleLinearInterpolator::applyIOConst(double &a, double &b, double &c) const
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

void SingleLinearInterpolator::applyIO(double &a, double &b, double &c)
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

Model *SingleLinearInterpolator::clone() const
{ return new SingleLinearInterpolator(m, b, constraints); }

QString SingleLinearInterpolator::describe() const
{ return "Linear"; }

ModelLegendEntry SingleLinearInterpolator::legend(const ModelLegendParameters &parameters) const
{ return legendPolynomial(QVector<double>() << b << m, parameters); }

SingleLinearInterpolator::SingleLinearInterpolator(QDataStream &stream)
{ stream >> m >> b; }

void SingleLinearInterpolator::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_LinearSingle;
    stream << t << m << b;
}

void SingleLinearInterpolator::printLog(QDebug &stream) const
{ stream << "SingleLinearInterpolator(" << m << ',' << b << ')'; }


LinearInterpolator::~LinearInterpolator()
{
    free(components);
}

LinearInterpolator::LinearInterpolator(const LinearInterpolator &other) : constraints(
        other.constraints)
{
    if (other.nComponents <= 0) {
        components = (Component *) malloc(sizeof(Component));
        Q_ASSERT(components);
        lastHit = components;
        nComponents = 0;
    } else {
        nComponents = other.nComponents;
        components = (Component *) malloc(sizeof(Component) * nComponents);
        Q_ASSERT(components);
        lastHit = components;
        memcpy(components, other.components, sizeof(Component) * nComponents);
    }
}

bool LinearInterpolator::InputPoint::operator<(const InputPoint &other) const
{ return x < other.x; }

LinearInterpolator::InputPoint::InputPoint(double sx, double sy) : x(sx), y(sy)
{ }

LinearInterpolator::LinearInterpolator(const QVector<double> &x,
                                       const QVector<double> &y,
                                       const ModelOutputConstraints &output) : constraints(output)
{
    Q_ASSERT(x.size() == y.size());
    std::vector<InputPoint> sorted;
    sorted.reserve(x.size());
    for (int i = 0, max = x.size(); i < max; i++) {
        if (!FP::defined(x.at(i)) || !FP::defined(y.at(i)))
            continue;
        sorted.push_back(InputPoint(x.at(i), y.at(i)));
    }
    std::sort(sorted.begin(), sorted.end());
    if (sorted.begin() != sorted.end()) {
        for (std::vector<InputPoint>::iterator i = sorted.begin() + 1; i != sorted.end();) {
            if ((i - 1)->x == i->x) {
                i = sorted.erase(i);
                continue;
            }
            ++i;
        }
    }

    if (sorted.size() < 2) {
        components = (Component *) malloc(sizeof(Component));
        Q_ASSERT(components);
        lastHit = components;
        nComponents = 0;
        return;
    }

    nComponents = sorted.size() - 1;
    components = (Component *) malloc(sizeof(Component) * nComponents);
    Q_ASSERT(components);
    lastHit = components;

    for (int i = 0; i < nComponents; i++) {
        components[i].start = sorted[i].x;
        components[i].b = (sorted[i + 1].y - sorted[i].y) / (sorted[i + 1].x - sorted[i].x);
        components[i].a = sorted[i].y - components[i].b * sorted[i].x;
    }
}

bool LinearInterpolator::Component::operator<(double d) const
{ return start < d; }

double LinearInterpolator::evaluate(double input)
{
    if (nComponents == 0) return FP::undefined();
    const Component *end = components + nComponents;
    const Component *c = lastHit;
    if (input < c->start || (c != end - 1 && input > (c + 1)->start)) {
        c = std::lower_bound<const Component *, double>(components, end, input);
        if (c == end || (c != components && c->start > input)) c--;
        lastHit = c;
    }
    return constraints.apply(c->a + c->b * input);
}

double LinearInterpolator::evaluate(double input) const
{
    if (nComponents == 0) return constraints.apply(FP::undefined());
    const Component *end = components + nComponents;
    const Component *c = std::lower_bound<const Component *, double>(components, end, input);
    if (c == end || (c != components && c->start > input)) c--;
    return constraints.apply(c->a + c->b * input);
}

QVector<double> LinearInterpolator::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

QVector<double> LinearInterpolator::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

void LinearInterpolator::applyIOConst(QVector<double> &values) const
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

void LinearInterpolator::applyIO(QVector<double> &values)
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

double LinearInterpolator::oneToOneConst(double input) const
{ return evaluate(input); }

double LinearInterpolator::oneToOne(double input)
{ return evaluate(input); }

double LinearInterpolator::twoToOneConst(double a, double b) const
{
    Q_UNUSED(b);
    return evaluate(a);
}

double LinearInterpolator::twoToOne(double a, double b)
{
    Q_UNUSED(b);
    return evaluate(a);
}

double LinearInterpolator::threeToOneConst(double a, double b, double c) const
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

double LinearInterpolator::threeToOne(double a, double b, double c)
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

void LinearInterpolator::applyIOConst(double &a, double &b) const
{
    a = evaluate(a);
    b = evaluate(b);
}

void LinearInterpolator::applyIO(double &a, double &b)
{
    a = evaluate(a);
    b = evaluate(b);
}

void LinearInterpolator::applyIOConst(double &a, double &b, double &c) const
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

void LinearInterpolator::applyIO(double &a, double &b, double &c)
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

Model *LinearInterpolator::clone() const
{ return new LinearInterpolator(*this); }

QString LinearInterpolator::describe() const
{ return QString("%1-Linear").arg(nComponents); }

ModelLegendEntry LinearInterpolator::legend(const ModelLegendParameters &parameters) const
{
    Q_UNUSED(parameters);
    return ModelLegendEntry(
            QObject::tr("Linear Interpolation (%n segments)", "model linear legend line",
                        nComponents));
}

LinearInterpolator::LinearInterpolator(QDataStream &stream)
{
    quint32 n;
    stream >> n;
    nComponents = (int) n;
    if (nComponents == 0) {
        components = (Component *) malloc(sizeof(Component));
        Q_ASSERT(components);
        lastHit = components;
    } else {
        components = (Component *) malloc(sizeof(Component) * nComponents);
        Q_ASSERT(components);
        lastHit = components;
        for (int i = 0; i < nComponents; i++) {
            stream >> components[i].start >> components[i].a >> components[i].b;
        }
    }
}

void LinearInterpolator::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_Linear;
    stream << t << (quint32) nComponents;
    for (int i = 0; i < nComponents; i++) {
        stream << components[i].start << components[i].a << components[i].b;
    }
}

void LinearInterpolator::printLog(QDebug &stream) const
{
    stream << "LinearInterpolator(";
    for (int i = 0, max = nComponents; i < max; i++) {
        if (i != 0) stream << ',';
        stream << "[x0=" << components[i].start << ",{" << components[i].a << ',' << components[i].b
               << "}]";
    }
    stream << ')';
}

Model *LinearInterpolator::create(const QVector<double> &x,
                                  const QVector<double> &y,
                                  const ModelOutputConstraints &output,
                                  LinearModelType type,
                                  double k)
{
    if (x.size() != y.size() || x.isEmpty())
        return new ModelConstant(output.apply(FP::undefined()));
    if (x.size() == 1)
        return new ModelConstant(output.apply(y.at(0)));

    switch (type) {
    case Type_Linear:
        break;
    case Type_Sin:
        return new LinearTransferInterpolator(x, y, LinearTransferInterpolator::Transfer_Sin, k,
                                              output);
    case Type_Cos:
        return new LinearTransferInterpolator(x, y, LinearTransferInterpolator::Transfer_Cos, k,
                                              output);
    case Type_Sigmoid:
        if (!FP::defined(k) || k == 0.0 || k <= -1.0 || k >= 1.0)
            break;
        return new LinearTransferInterpolator(x, y,
                                              LinearTransferInterpolator::Transfer_NormalizedSigmoid,
                                              k, output);
    }

    if (x.size() == 2) {
        return new SingleLinearInterpolator(x.at(0), y.at(0), x.at(1), y.at(1), output);
    }

    LinearInterpolator *si = new LinearInterpolator(x, y, output);
    if (si->nComponents == 0) {
        delete si;
        double avg = 0;
        int count = 0;
        for (int i = 0, max = y.size(); i < max; i++) {
            if (!FP::defined(y.at(i)) || !FP::defined(x.at(i)))
                continue;
            avg += y.at(i);
            count++;
        }
        if (count == 0)
            return new ModelConstant(output.apply(FP::undefined()));
        return new ModelConstant(output.apply(avg / (double) count));
    }
    if (si->nComponents == 1) {
        Model *result =
                new SingleLinearInterpolator(si->components[0].b, si->components[0].a, output);
        delete si;
        return result;
    }

    return si;
}


static double transferSin(double x, double k)
{
    return ::pow(::sin(x * M_PI / 2.0), k);
}

static double transferCos(double x, double k)
{
    return ::pow(1.0 - ::cos(x * M_PI / 2.0), k);
}

static double transferTunableSigmoid(double x, double k)
{
    x = x * 2.0 - 1.0;
    x = (x - x * k) / (k - ::fabs(x) * 2 * k + 1);
    return x / 2.0 + 0.5;
}

void LinearTransferInterpolator::setTransfer()
{
    switch (type) {
    case Transfer_Sin:
        if (!FP::defined(k) || k <= 0.0)
            k = 1.0;
        transfer = transferSin;
        break;
    case Transfer_Cos:
        if (!FP::defined(k) || k <= 0.0)
            k = 1.0;
        transfer = transferCos;
        break;
    case Transfer_NormalizedSigmoid:
        if (!FP::defined(k) || k <= -1.0 || k >= 1.0)
            k = 0.0;
        transfer = transferTunableSigmoid;
        break;
    }
}

LinearTransferInterpolator::~LinearTransferInterpolator()
{
    free(components);
}

LinearTransferInterpolator::LinearTransferInterpolator(const LinearTransferInterpolator &other)
        : constraints(other.constraints), type(other.type), k(other.type)
{
    setTransfer();

    if (other.nComponents <= 0) {
        components = (Component *) malloc(sizeof(Component));
        Q_ASSERT(components);
        lastHit = components;
        nComponents = 0;
    } else {
        nComponents = other.nComponents;
        components = (Component *) malloc(sizeof(Component) * nComponents);
        Q_ASSERT(components);
        lastHit = components;
        memcpy(components, other.components, sizeof(Component) * nComponents);
    }
}

bool LinearTransferInterpolator::InputPoint::operator<(const InputPoint &other) const
{ return x < other.x; }

LinearTransferInterpolator::InputPoint::InputPoint(double sx, double sy) : x(sx), y(sy)
{ }

LinearTransferInterpolator::LinearTransferInterpolator(const QVector<double> &x,
                                                       const QVector<double> &y,
                                                       TransferFunctionType t,
                                                       double sk,
                                                       const ModelOutputConstraints &output)
        : constraints(output), type(t), k(sk)
{
    setTransfer();

    Q_ASSERT(x.size() == y.size());
    std::vector<InputPoint> sorted;
    sorted.reserve(x.size());
    for (int i = 0, max = x.size(); i < max; i++) {
        if (!FP::defined(x.at(i)) || !FP::defined(y.at(i)))
            continue;
        sorted.push_back(InputPoint(x.at(i), y.at(i)));
    }
    std::sort(sorted.begin(), sorted.end());
    if (sorted.begin() != sorted.end()) {
        for (std::vector<InputPoint>::iterator i = sorted.begin() + 1; i != sorted.end();) {
            if ((i - 1)->x == i->x) {
                i = sorted.erase(i);
                continue;
            }
            ++i;
        }
    }

    if (sorted.size() < 2) {
        components = (Component *) malloc(sizeof(Component));
        Q_ASSERT(components);
        lastHit = components;
        nComponents = 0;
        return;
    }

    nComponents = sorted.size() - 1;
    components = (Component *) malloc(sizeof(Component) * nComponents);
    Q_ASSERT(components);
    lastHit = components;

    for (int i = 0; i < nComponents; i++) {
        components[i].start = sorted[i].x;
        components[i].delta = 1.0 / (sorted[i + 1].x - sorted[i].x);
        components[i].offset = sorted[i].y;
        components[i].scale = (sorted[i + 1].y - sorted[i].y);
    }
}

bool LinearTransferInterpolator::Component::operator<(double d) const
{ return start < d; }

double LinearTransferInterpolator::evaluate(double input)
{
    if (nComponents == 0) return FP::undefined();
    const Component *end = components + nComponents;
    const Component *c = lastHit;
    if (input < c->start || (c != end - 1 && input > (c + 1)->start)) {
        c = std::lower_bound<const Component *, double>(components, end, input);
        if (c == end || (c != components && c->start > input)) c--;
        lastHit = c;
    }

    input = (input - c->start) * c->delta;
    if (input < 0.0)
        input = 0.0;
    else if (input > 1.0)
        input = 1.0;
    input = (*transfer)(input, k);
    return constraints.apply(input * c->scale + c->offset);
}

double LinearTransferInterpolator::evaluate(double input) const
{
    if (nComponents == 0) return constraints.apply(FP::undefined());
    const Component *end = components + nComponents;
    const Component *c = std::lower_bound<const Component *, double>(components, end, input);
    if (c == end || (c != components && c->start > input)) c--;

    input = (input - c->start) * c->delta;
    if (input < 0.0)
        input = 0.0;
    else if (input > 1.0)
        input = 1.0;
    input = (*transfer)(input, k);
    return constraints.apply(input * c->scale + c->offset);
}

QVector<double> LinearTransferInterpolator::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

QVector<double> LinearTransferInterpolator::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

void LinearTransferInterpolator::applyIOConst(QVector<double> &values) const
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

void LinearTransferInterpolator::applyIO(QVector<double> &values)
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

double LinearTransferInterpolator::oneToOneConst(double input) const
{ return evaluate(input); }

double LinearTransferInterpolator::oneToOne(double input)
{ return evaluate(input); }

double LinearTransferInterpolator::twoToOneConst(double a, double b) const
{
    Q_UNUSED(b);
    return evaluate(a);
}

double LinearTransferInterpolator::twoToOne(double a, double b)
{
    Q_UNUSED(b);
    return evaluate(a);
}

double LinearTransferInterpolator::threeToOneConst(double a, double b, double c) const
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

double LinearTransferInterpolator::threeToOne(double a, double b, double c)
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

void LinearTransferInterpolator::applyIOConst(double &a, double &b) const
{
    a = evaluate(a);
    b = evaluate(b);
}

void LinearTransferInterpolator::applyIO(double &a, double &b)
{
    a = evaluate(a);
    b = evaluate(b);
}

void LinearTransferInterpolator::applyIOConst(double &a, double &b, double &c) const
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

void LinearTransferInterpolator::applyIO(double &a, double &b, double &c)
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

Model *LinearTransferInterpolator::clone() const
{ return new LinearTransferInterpolator(*this); }

QString LinearTransferInterpolator::describe() const
{ return QString("%1-LinearTransfer").arg(nComponents); }

ModelLegendEntry LinearTransferInterpolator::legend(const ModelLegendParameters &parameters) const
{
    Q_UNUSED(parameters);
    return ModelLegendEntry(
            QObject::tr("Linear Transfer (%n segments)", "model linear transfer legend line",
                        nComponents));
}

LinearTransferInterpolator::LinearTransferInterpolator(QDataStream &stream)
{
    quint8 t = 0;
    stream >> t;
    type = (TransferFunctionType) t;
    stream >> k;

    quint32 n;
    stream >> n;
    nComponents = (int) n;
    if (nComponents == 0) {
        components = (Component *) malloc(sizeof(Component));
        Q_ASSERT(components);
        lastHit = components;
    } else {
        components = (Component *) malloc(sizeof(Component) * nComponents);
        Q_ASSERT(components);
        lastHit = components;
        for (int i = 0; i < nComponents; i++) {
            stream >> components[i].start >> components[i].delta >> components[i].offset
                   >> components[i].scale;
        }
    }

    setTransfer();
}

void LinearTransferInterpolator::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_LinearTransfer;
    stream << t << (quint8) type << k << (quint32) nComponents;
    for (int i = 0; i < nComponents; i++) {
        stream << components[i].start << components[i].delta << components[i].offset
               << components[i].scale;
    }
}

void LinearTransferInterpolator::printLog(QDebug &stream) const
{
    stream << "LinearTransferInterpolator(";
    switch (type) {
    case Transfer_Sin:
        stream << "Sin[" << k << ']';
        break;
    case Transfer_Cos:
        stream << "Cos[" << k << ']';
        break;
    case Transfer_NormalizedSigmoid:
        stream << "Sigmoid[" << k << ']';
        break;
    }
    for (int i = 0, max = nComponents; i < max; i++) {
        if (i != 0) stream << ',';
        stream << "[x0=" << components[i].start << ",d=" << components[i].delta << ",{"
               << components[i].offset << ',' << components[i].scale << "}]";
    }
    stream << ')';
}


}
}
