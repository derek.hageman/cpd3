/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "algorithms/dewpoint.hxx"
#include "algorithms/numericsolve.hxx"

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/dewpoint.hxx
 * Dew point and RH calculations.
 */

/* 
 * See http://www.decatur.de/javascript/dew/dew.js
 * Saturation Vapor Pressure formula for range -100..0 Deg. C.
 * This is taken from
 *   ITS-90 Formulations for Vapor Pressure, Frostpoint Temperature,
 *   Dewpoint Temperature, and Enhancement Factors in the Range 100 to +100 C
 * by Bob Hardy
 * as published in "The Proceedings of the Third International Symposium on Humidity & Moisture",
 * Teddington, London, England, April 1998
 */
static double svpIce(double t)
{
    if (t <= 0.0)
        return FP::undefined();
    return exp(-5.8666426e3 / t +
                       2.232870244e1 +
                       (1.39387003e-2 + (-3.4262402e-5 + (2.7040955e-8 * t)) * t) * t +
                       6.7063522e-1 * log(t));
}

/*
 * See http://www.decatur.de/javascript/dew/dew.js
 * Saturation Vapor Pressure formula for range 273..678 Deg. K.
 * This is taken from the
 *   Release on the IAPWS Industrial Formulation 1997
 *   for the Thermodynamic Properties of Water and Steam
 * by IAPWS (International Association for the Properties of Water and Steam),
 * Erlangen, Germany, September 1997.
 *
 * This is Equation (30) in Section 8.1 "The Saturation-Pressure Equation (Basic Equation)"
 */
static double svpWater(double t)
{
    if (t <= 0.0)
        return FP::undefined();
    double th = t - 0.23855557567849 / (t - 0.65017534844798e3);
    double A = (th + 0.11670521452767e4) * th - 0.72421316703206e6;
    double B = (-0.17073846940092e2 * th + 0.12020824702470e5) * th - 0.32325550322333e7;
    double C = (0.14915108613530e2 * th - 0.48232657361591e4) * th + 0.40511340542057e6;

    double p = 2.0 * C / (-B + sqrt(B * B - 4 * A * C));
    p *= p;
    p *= p;
    return p * 1e6;
}

static double svp(double t, bool forceWater = false)
{
    if (!forceWater && t < 273.15)
        return svpIce(t);
    return svpWater(t);
}

namespace {
struct SVPSolver : public NewtonRaphson<SVPSolver> {
    double t;
    bool forceWater;

    SVPSolver(double it, bool fw) : t(it), forceWater(fw)
    { }

    double evaluate(double x) const
    {
        return svp(x, forceWater);
    }

    double initial(double yTarget) const
    {
        Q_UNUSED(yTarget);
        return t;
    }

    double firstStep(double x0) const
    {
        return x0 + 1.0;
    }
};
}

static double svpSolve(double yTarget, double t, bool forceWater = false)
{
    double result = SVPSolver(t, forceWater).solve(yTarget);
    if (FP::defined(result))
        result -= 273.15;
    return result;
}

/**
 * Calculate the dew point given a temperature and RH.
 * 
 * @param t             the temperature (C)
 * @param rh            the relative humidity
 * @param forceWater    force the calculation to assume liquid water, even if the temperature is less than zero C
 * @return              the dewpoint (C)
 */
double Dewpoint::dewpoint(double t, double rh, bool forceWater)
{
    if (!FP::defined(t) || !FP::defined(rh))
        return FP::undefined();
    if (rh <= 0.0 || rh > 100.0 || t < -100.0 || t > 400.0)
        return FP::undefined();
    t += 273.15;

    double yTarget = svp(t, forceWater);
    if (!FP::defined(yTarget))
        return FP::undefined();
    yTarget *= (rh / 100.0);

    return svpSolve(yTarget, t, forceWater);
}

/**
 * Calculate the RH given a temperature and dew point.
 * 
 * @param t             the temperature (C)
 * @param td            the dew point (C)
 * @param forceWater    force the calculation to assume liquid water, even if the temperature is less than zero C
 * @return              the RH (percent)
 */
double Dewpoint::rh(double t, double td, bool forceWater)
{
    if (!FP::defined(t) || !FP::defined(td))
        return FP::undefined();
    if (t < -100.0 || t > 400.0 || td < -100.0 || td > 400.0)
        return FP::undefined();
    t += 273.15;
    td += 273.15;

    double svpT = svp(t, forceWater);
    if (!FP::defined(svpT) || svpT == 0.0)
        return FP::undefined();
    double svpTD = svp(td, forceWater);
    if (!FP::defined(svpTD))
        return FP::undefined();
    return (svpTD / svpT) * 100.0;
}

/**
 * Calculate the temperature given a dewpoint and RH.
 * 
 * @param rh            the relative humidity
 * @param td            the dewpoint (C)
 * @param forceWater    force the calculation to assume liquid water, even if the temperature is less than zero C
 * @return              the temperature (C)
 */
double Dewpoint::t(double rh, double td, bool forceWater)
{
    if (!FP::defined(rh) || !FP::defined(td))
        return FP::undefined();
    if (rh <= 0.0 || rh > 100.0 || td < -100 || td > 400.0)
        return FP::undefined();
    td += 273.15;

    double yTarget = svp(td, forceWater);
    if (!FP::defined(yTarget))
        return FP::undefined();
    yTarget /= (rh / 100.0);

    return svpSolve(yTarget, td, forceWater);
}

/**
 * Extrapolate the RH at a temperature given the temperature and RH at another
 * point, assuming the dew point is constant between those two points.
 * 
 * @param t1            the temperature at the first point (C)
 * @param rh            the RH at the first point (percent)
 * @param t2            the temperature to calculate the RH at (C)
 * @param forceWater    force the calculation to assume liquid water, even if the temperature is less than zero C
 * @return              the RH at the second point (percent)
 */
double Dewpoint::rhExtrapolate(double t1, double rh1, double t2, bool forceWater)
{
    return rh(t2, dewpoint(t1, rh1, forceWater), forceWater);
}

/* http://www.srh.noaa.gov/epz/?n=wxcalc_dewpoint */
static double eSubX(double t)
{
    if (t == -243.5)
        return FP::undefined();
    return 6.112 * exp((17.67 * t) / (t + 243.5));
}

static double eWet(double t, double tw, double P)
{
    double ew = eSubX(tw);
    if (!FP::defined(ew))
        return FP::undefined();
    return ew - P * (t - tw) * 0.00066 * (1.0 + (0.00115 * tw));
}

/**
 * Calculate the wet bulb temperature.
 * 
 * @param t     the dry bulb temperature (C)
 * @param td    the dewpoint (C)
 * @param P     the ambient pressure (hPa)
 * @return      the the wet bulb temperature (C)
 */
double Dewpoint::wetBulb(double t, double td, double P)
{
    if (!FP::defined(t) || !FP::defined(td) || !FP::defined(P))
        return FP::undefined();
    if (t < -100.0 || t > 400.0 || td < -100.0 || td > 400.0)
        return FP::undefined();
    if (P < 300.0 || P > 1100.0)
        return FP::undefined();

    double yTarget = eSubX(td);
    if (!FP::defined(yTarget))
        return FP::undefined();

    double x0 = t;
    double y0 = eWet(t, x0, P);
    if (!FP::defined(y0))
        return FP::undefined();

    double x1 = td;
    if (x1 == x0)
        x1 = x0 + 1.0;
    double y1;
    double dX;

    for (int itter = 0; itter < 20; itter++) {
        y1 = eWet(t, x1, P);
        if (!FP::defined(y1))
            return FP::undefined();
        if (y1 == y0)
            return x1;
        dX = ((x1 - x0) / (y1 - y0)) * (yTarget - y1);
        if (fabs(dX) < 1E-6)
            return x1 + dX;
        x0 = x1;
        y0 = y1;
        x1 += dX;
    }

    return FP::undefined();
}

}
}
