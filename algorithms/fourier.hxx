/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSFOURIER_H
#define CPD3ALGORITHMSFOURIER_H

#include "core/first.hxx"

#include <math.h>
#include <memory>
#include <QtGlobal>
#include <QList>
#include <QDebug>

#include "algorithms/algorithms.hxx"

#include "core/number.hxx"
#include "algorithms/model.hxx"
#include "algorithms/transformer.hxx"

#include <complex>


namespace CPD3 {
namespace Algorithms {


/**
 * A Fourier transformer (forward and reverse).
 */
class CPD3ALGORITHMS_EXPORT Fourier {
public:
    /**
     * Order the given list in bit reversed indexing.  The length of the
     * list must be a power of two.
     * 
     * @param points    the points to reorder
     */
    template<typename ListType>
    static void bitReverse(ListType &points)
    {
        const int n = (int) points.size();
        if (n <= 8) {
            switch (n) {
            case 8:
                qSwap(points[1], points[4]);
                qSwap(points[3], points[6]);
                break;
            case 4:
                qSwap(points[1], points[2]);
                break;
            case 2:
                break;
            default:
                Q_ASSERT(false);
            }
            return;
        }
        Q_ASSERT(((quint32) 1 << (31 - INTEGER::clz32(n))) == (quint32) n);

        /* A Fast FFT Bit-Reversal Algorithm, D Sundararajan - ‎1994 */

        const int n2 = n / 2;
        const int n4 = n / 4;
        const int n8 = n / 8;
        int i = 0;
        int j = -n4;
        while (i < n4) {
            if (j >= n4) {
                int k = n8;
                j -= n4;
                while (k <= j) {
                    j -= k;
                    k >>= 1;
                }
                j += k;
            } else {
                j += n4;
            }
            if (i < j) {
                qSwap(points[i], points[j]);
                qSwap(points[i + n2 + 1], points[j + n2 + 1]);
                if (i + n4 < j + 2) {
                    qSwap(points[i + n4], points[j + 2]);
                    qSwap(points[i + n4 + n2 + 1], points[j + n2 + 3]);
                }
            }
            i++;
            qSwap(points[i], points[j + n2]);
            qSwap(points[i + n4], points[j + n2 + 2]);
            i++;
        }
    }

    static void forward(QVector<std::complex<double> > &points);

    static QVector<std::complex<double> > forward(const QVector<double> &points);

    static void inverse(QVector<std::complex<double> > &points);

    static QVector<double> inverseReal(QVector<std::complex<double> > points);
};


/**
 * A transformer that takes the input puts and returns them in Fourier
 * power spectrum space.  Currently this only does a 1-d Fourier transform
 * (ignoring the first dimension, if available) on each dimension separately.
 */
class CPD3ALGORITHMS_EXPORT FourierPowerSpectrum : public Transformer {
public:
    FourierPowerSpectrum();

    ~FourierPowerSpectrum();

    virtual void applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                            TransformerAllocator *allocator = NULL) const;

    virtual void apply(QVector<std::shared_ptr<TransformerPoint> > &points,
                       TransformerAllocator *allocator = NULL);

    virtual void
            applyConst(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL)
            const;

    virtual void apply(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL);

    virtual Transformer *clone() const;

protected:
    friend class Transformer;

    FourierPowerSpectrum(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator<<(QDataStream &stream, const Transformer *transformer);

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator>>(QDataStream &stream, Transformer *&transformer);

    friend CPD3ALGORITHMS_EXPORT QDebug operator<<(QDebug stream, const Transformer *transformer);
};

}
}

#endif
