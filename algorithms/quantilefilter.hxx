/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSQUANTILEFILTER_H
#define CPD3ALGORITHMSQUANTILEFILTER_H

#include "core/first.hxx"

#include <math.h>
#include <memory>
#include <QtGlobal>
#include <QList>

#include "algorithms/algorithms.hxx"

#include "core/number.hxx"
#include "algorithms/model.hxx"
#include "algorithms/transformer.hxx"


namespace CPD3 {
namespace Algorithms {

namespace Internal {
struct QuantileFilterData;

typedef bool (*QuantileFilterTestFunction)(const QuantileFilterData &, double);
}

/**
 * A filter that accepts or rejects inside a range of the quantiles of that
 * data.
 */
class CPD3ALGORITHMS_EXPORT QuantileFilter : public Transformer {
    double min;
    double max;
    int flags;
    int dimension;
    Internal::QuantileFilterTestFunction function;

    void setFunction();

public:
    enum {
        Reject_Range = 0x1, Min_Inclusive = 0x2, Max_Inclusive = 0x4, Reject_Remove = 0x8,
    };

    QuantileFilter(double setMin, double setMax, int setFlags = 0, int onlyDimension = -1);

    ~QuantileFilter();

    virtual void applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                            TransformerAllocator *allocator = NULL) const;

    virtual void
            applyConst(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL)
            const;

    virtual Transformer *clone() const;

protected:
    friend class Transformer;

    QuantileFilter(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator<<(QDataStream &stream, const Transformer *transformer);

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator>>(QDataStream &stream, Transformer *&transformer);

    friend CPD3ALGORITHMS_EXPORT QDebug operator<<(QDebug stream, const Transformer *transformer);
};

}
}

#endif
