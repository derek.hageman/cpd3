/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVector>

#include "algorithms/npolynomial.hxx"
#include "algorithms/lma.hxx"
#include "core/qtcompat.hxx"

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/npolynomial.hxx
 * Provides routines for generating Nth order polynomial fits.
 */


static QDataStream &operator<<(QDataStream &stream, const std::vector<double> &vec)
{
    stream << (quint32) vec.size();
    for (std::vector<double>::const_iterator add = vec.begin(), endAdd = vec.end();
            add != endAdd;
            ++add) {
        stream << (*add);
    }
    return stream;
}

static QDataStream &operator>>(QDataStream &stream, std::vector<double> &vec)
{
    quint32 n;
    stream >> n;
    vec.resize(n);
    for (quint32 i = 0; i < n; i++) {
        stream >> vec[i];
    }
    return stream;
}

NPolynomialLMA::~NPolynomialLMA()
{ }

NPolynomialLMA::NPolynomialLMA(const NPolynomialLMA &other)
        : Model(), coefficients(other.coefficients), constraints(other.constraints)
{ }

static double evaluateNPolynomial(double x, const QVector<double> &p)
{
    double result = p[0];
    double value = x;
    for (QVector<double>::const_iterator add = p.begin() + 1, endAdd = p.end();
            add != endAdd;
            ++add, value *= x) {
        result += value * (*add);
    }
    return result;
}

static QVector<double> jacobianNPolynomial(double x, const QVector<double> &p)
{
    QVector<double> result(1, 1.0);
    result.reserve(p.size());
    double value = x;
    for (QVector<double>::const_iterator add = p.begin() + 1, endAdd = p.end();
            add != endAdd;
            ++add, value *= x) {
        result.append(value);
    }
    return result;
}

/**
 * Construct the Nth order polynomial fit.
 * 
 * @param x         the independent values
 * @param y         the dependent values
 * @param N         the number of coefficients
 * @param output    the output constraints to apply
 */
NPolynomialLMA::NPolynomialLMA(const QVector<double> &x,
                               const QVector<double> &y,
                               int N,
                               const ModelOutputConstraints &output)
        : coefficients(), constraints(output)
{
    QVector<double> result
            (LevenbergMarquardt::analytic(N + 1, evaluateNPolynomial, jacobianNPolynomial, x, y));
    coefficients = result.toStdVector();
}

QVector<double> NPolynomialLMA::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

QVector<double> NPolynomialLMA::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

void NPolynomialLMA::applyIOConst(QVector<double> &values) const
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

void NPolynomialLMA::applyIO(QVector<double> &values)
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

double NPolynomialLMA::oneToOneConst(double input) const
{ return evaluate(input); }

double NPolynomialLMA::oneToOne(double input)
{ return evaluate(input); }

double NPolynomialLMA::twoToOneConst(double a, double b) const
{
    Q_UNUSED(b);
    return evaluate(a);
}

double NPolynomialLMA::twoToOne(double a, double b)
{
    Q_UNUSED(b);
    return evaluate(a);
}

double NPolynomialLMA::threeToOneConst(double a, double b, double c) const
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

double NPolynomialLMA::threeToOne(double a, double b, double c)
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

void NPolynomialLMA::applyIOConst(double &a, double &b) const
{
    a = evaluate(a);
    b = evaluate(b);
}

void NPolynomialLMA::applyIO(double &a, double &b)
{
    a = evaluate(a);
    b = evaluate(b);
}

void NPolynomialLMA::applyIOConst(double &a, double &b, double &c) const
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

void NPolynomialLMA::applyIO(double &a, double &b, double &c)
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

Model *NPolynomialLMA::clone() const
{ return new NPolynomialLMA(*this); }

QString NPolynomialLMA::describe() const
{ return QString("%1Polynomial").arg(coefficients.size() - 1); }

ModelLegendEntry NPolynomialLMA::legend(const ModelLegendParameters &parameters) const
{
    QString result
            (legendPolynomialString(QVector<double>::fromStdVector(coefficients), QVector<double>(),
                                    parameters));

    return ModelLegendEntry(result);
}

NPolynomialLMA::NPolynomialLMA(QDataStream &stream)
{ stream >> coefficients; }

void NPolynomialLMA::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_NPolynomialLMA;
    stream << t << coefficients;
}

void NPolynomialLMA::printLog(QDebug &stream) const
{ stream << "NPolynomialLMA(" << coefficients << ")"; }


ZeroNPolynomialLMA::~ZeroNPolynomialLMA()
{ }

ZeroNPolynomialLMA::ZeroNPolynomialLMA(const ZeroNPolynomialLMA &other)
        : Model(), coefficients(other.coefficients), constraints(other.constraints)
{ }

static double evaluateZeroNPolynomial(double x, const QVector<double> &p)
{
    double result = 0.0;
    double value = x;
    for (QVector<double>::const_iterator add = p.begin(), endAdd = p.end();
            add != endAdd;
            ++add, value *= x) {
        result += value * (*add);
    }
    return result;
}

static QVector<double> jacobianZeroNPolynomial(double x, const QVector<double> &p)
{
    QVector<double> result;
    result.reserve(p.size());
    double value = x;
    for (QVector<double>::const_iterator add = p.begin(), endAdd = p.end();
            add != endAdd;
            ++add, value *= x) {
        result.append(value);
    }
    return result;
}

/**
 * Construct the Nth order polynomial fit with a fixed zero intercept.
 * 
 * @param x         the independent values
 * @param y         the dependent values
 * @param N         the number of coefficients
 * @param output    the output constraints to apply
 */
ZeroNPolynomialLMA::ZeroNPolynomialLMA(const QVector<double> &x,
                                       const QVector<double> &y,
                                       int N,
                                       const ModelOutputConstraints &output)
        : coefficients(), constraints(output)
{
    Q_ASSERT(N > 0);
    QVector<double> result
            (LevenbergMarquardt::analytic(N, evaluateZeroNPolynomial, jacobianZeroNPolynomial, x,
                                          y));
    coefficients = result.toStdVector();
}

QVector<double> ZeroNPolynomialLMA::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

QVector<double> ZeroNPolynomialLMA::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

void ZeroNPolynomialLMA::applyIOConst(QVector<double> &values) const
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

void ZeroNPolynomialLMA::applyIO(QVector<double> &values)
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

double ZeroNPolynomialLMA::oneToOneConst(double input) const
{ return evaluate(input); }

double ZeroNPolynomialLMA::oneToOne(double input)
{ return evaluate(input); }

double ZeroNPolynomialLMA::twoToOneConst(double a, double b) const
{
    Q_UNUSED(b);
    return evaluate(a);
}

double ZeroNPolynomialLMA::twoToOne(double a, double b)
{
    Q_UNUSED(b);
    return evaluate(a);
}

double ZeroNPolynomialLMA::threeToOneConst(double a, double b, double c) const
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

double ZeroNPolynomialLMA::threeToOne(double a, double b, double c)
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

void ZeroNPolynomialLMA::applyIOConst(double &a, double &b) const
{
    a = evaluate(a);
    b = evaluate(b);
}

void ZeroNPolynomialLMA::applyIO(double &a, double &b)
{
    a = evaluate(a);
    b = evaluate(b);
}

void ZeroNPolynomialLMA::applyIOConst(double &a, double &b, double &c) const
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

void ZeroNPolynomialLMA::applyIO(double &a, double &b, double &c)
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

Model *ZeroNPolynomialLMA::clone() const
{ return new ZeroNPolynomialLMA(*this); }

QString ZeroNPolynomialLMA::describe() const
{ return QString("%1ZeroPolynomial").arg(coefficients.size()); }

ModelLegendEntry ZeroNPolynomialLMA::legend(const ModelLegendParameters &parameters) const
{
    QVector<double> cf(QVector<double>::fromStdVector(coefficients));
    cf.prepend(0.0);
    QString result(legendPolynomialString(cf, QVector<double>(), parameters));

    return ModelLegendEntry(result);
}

ZeroNPolynomialLMA::ZeroNPolynomialLMA(QDataStream &stream)
{ stream >> coefficients; }

void ZeroNPolynomialLMA::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_ZeroNPolynomialLMA;
    stream << t << coefficients;
}

void ZeroNPolynomialLMA::printLog(QDebug &stream) const
{ stream << "ZeroNPolynomialLMA(" << coefficients << ")"; }

}
}
