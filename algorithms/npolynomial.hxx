/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSNPOLYNOMIAL_H
#define CPD3ALGORITHMSNPOLYNOMIAL_H

#include "core/first.hxx"

#include <vector>

#include <QtGlobal>
#include <QList>
#include <QDebug>

#include "algorithms/algorithms.hxx"

#include "algorithms/model.hxx"


namespace CPD3 {
namespace Algorithms {

/**
 * A simple two dimensional N-Polynomial fit using Levenberg-Marquardt fitting.
 */
class CPD3ALGORITHMS_EXPORT NPolynomialLMA : public Model {
    std::vector<double> coefficients;
    ModelOutputConstraints constraints;

    inline double evaluate(double input) const
    {
        double result = coefficients[0];
        double value = input;
        for (std::vector<double>::const_iterator add = coefficients.begin() + 1,
                endAdd = coefficients.end(); add != endAdd; ++add, value *= input) {
            result += value * (*add);
        }
        return constraints.apply(result);
    }

    NPolynomialLMA(const NPolynomialLMA &other);

public:
    virtual ~NPolynomialLMA();

    NPolynomialLMA(const QVector<double> &x,
                   const QVector<double> &y,
                   int N,
                   const ModelOutputConstraints &output = ModelOutputConstraints());

    virtual QVector<double> applyConst(const QVector<double> &inputs) const;

    virtual QVector<double> apply(const QVector<double> &inputs);

    virtual void applyIOConst(QVector<double> &values) const;

    virtual void applyIO(QVector<double> &values);

    virtual double oneToOneConst(double input) const;

    virtual double oneToOne(double input);

    virtual double twoToOneConst(double a, double b) const;

    virtual double twoToOne(double a, double b);

    virtual double threeToOneConst(double a, double b, double c) const;

    virtual double threeToOne(double a, double b, double c);

    virtual void applyIOConst(double &a, double &b) const;

    virtual void applyIO(double &a, double &b);

    virtual void applyIOConst(double &a, double &b, double &c) const;

    virtual void applyIO(double &a, double &b, double &c);

    virtual Model *clone() const;

    virtual QString describe() const;

    virtual ModelLegendEntry
            legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    /**
     * Get the order of the polynomial.
     * 
     * @return the polynomial order
     */
    inline int order() const
    { return (int) coefficients.size() - 1; }

    /**
     * Get the N'th coefficient in the polynomial.  Zero is the intercept.
     * 
     * @param n the order of the coefficient to fetch
     * @return  the N'th order coefficient
     */
    inline double coefficient(int n)
    {
        Q_ASSERT(n >= 0 && n < (int) coefficients.size());
        return coefficients[n];
    }

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

protected:
    NPolynomialLMA(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

/**
 * A simple two dimensional N-Polynomial fit using Levenberg-Marquardt fitting
 * with a fixed intercept at zero.
 */
class CPD3ALGORITHMS_EXPORT ZeroNPolynomialLMA : public Model {
    std::vector<double> coefficients;
    ModelOutputConstraints constraints;

    inline double evaluate(double input) const
    {
        double result = 0.0;
        double value = input;
        for (std::vector<double>::const_iterator add = coefficients.begin(),
                endAdd = coefficients.end(); add != endAdd; ++add, value *= input) {
            result += value * (*add);
        }
        return constraints.apply(result);
    }

    ZeroNPolynomialLMA(const ZeroNPolynomialLMA &other);

public:
    virtual ~ZeroNPolynomialLMA();

    ZeroNPolynomialLMA(const QVector<double> &x,
                       const QVector<double> &y,
                       int N,
                       const ModelOutputConstraints &output = ModelOutputConstraints());

    virtual QVector<double> applyConst(const QVector<double> &inputs) const;

    virtual QVector<double> apply(const QVector<double> &inputs);

    virtual void applyIOConst(QVector<double> &values) const;

    virtual void applyIO(QVector<double> &values);

    virtual double oneToOneConst(double input) const;

    virtual double oneToOne(double input);

    virtual double twoToOneConst(double a, double b) const;

    virtual double twoToOne(double a, double b);

    virtual double threeToOneConst(double a, double b, double c) const;

    virtual double threeToOne(double a, double b, double c);

    virtual void applyIOConst(double &a, double &b) const;

    virtual void applyIO(double &a, double &b);

    virtual void applyIOConst(double &a, double &b, double &c) const;

    virtual void applyIO(double &a, double &b, double &c);

    virtual Model *clone() const;

    virtual QString describe() const;

    virtual ModelLegendEntry
            legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    /**
     * Get the order of the polynomial.
     * 
     * @return the polynomial order
     */
    inline int order() const
    { return (int) coefficients.size(); }

    /**
     * Get the N'th coefficient in the polynomial.  Zero is the intercept,
     * which is always zero.
     * 
     * @param n the order of the coefficient to fetch
     * @return  the N'th order coefficient
     */
    inline double coefficient(int n)
    {
        if (n == 0)
            return 0.0;
        n--;
        Q_ASSERT(n >= 0 && n < (int) coefficients.size());
        return coefficients[n];
    }

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

protected:
    ZeroNPolynomialLMA(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};


}
}

#endif
