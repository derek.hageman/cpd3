/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3ALGORITHMS_SOLAR_HXX
#define CPD3ALGORITHMS_SOLAR_HXX

#include "core/first.hxx"

#include "algorithms.hxx"

namespace CPD3 {
namespace Algorithms {

class CPD3ALGORITHMS_EXPORT Solar {
    double time;
    double latitude;
    double longitude;

    double julianDay;

    double fromMinuteOfDay(double minutes) const;

public:
    Solar();

    Solar(double time, double latitude, double longitude);

    void setLocation(double latitude, double longitude);

    void setTime(double time);

    class CPD3ALGORITHMS_EXPORT Position {
        double azimuthAngle;
        double elevationAngle;

    public:
        Position() = delete;

        Position(const Position &) = default;

        Position &operator=(const Position &) = default;

        Position(const Solar &solar);

        inline double azimuth() const
        { return azimuthAngle; }

        inline double elevation() const
        { return elevationAngle; }

        bool isDark(double angle = 18.0) const;
    };

    friend class Position;

    inline Position position() const
    { return Position(*this); }

    class CPD3ALGORITHMS_EXPORT Day {
        double julianDay;
        double longitude;
        double timeOffset;
        double equationOfTime;
        double hourAngleSunrise;

    public:
        Day() = delete;

        Day(const Day &) = default;

        Day &operator=(const Day &) = default;

        Day(const Solar &solar);

        double sunrise() const;

        double sunset() const;

        double noon() const;
    };

    friend class Day;

    inline Day day() const
    { return Day(*this); }

    double noon() const;

};

}
}

#endif //CPD3ALGORITHMS_SOLAR_HXX
