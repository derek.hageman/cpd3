/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "algorithms/solar.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;


class TestSolar : public QObject {
Q_OBJECT
private slots:

    void solar()
    {
        QFETCH(double, time);
        QFETCH(double, latitude);
        QFETCH(double, longitude);
        QFETCH(double, azimuth);
        QFETCH(double, elevation);
        QFETCH(double, sunrise);
        QFETCH(double, sunset);
        QFETCH(double, noon);

        Solar s1(time, latitude, longitude);

        Solar::Position p = s1.position();
        if (FP::defined(azimuth)) {
            QCOMPARE(p.azimuth(), azimuth);
        } else {
            QVERIFY(!FP::defined(p.azimuth()));
        }
        if (FP::defined(elevation)) {
            QCOMPARE(p.elevation(), elevation);
        } else {
            QVERIFY(!FP::defined(p.elevation()));
        }

        Solar::Day d = s1.day();
        if (FP::defined(sunrise)) {
            QCOMPARE(d.sunrise(), sunrise);
        } else {
            QVERIFY(!FP::defined(d.sunrise()));
        }
        if (FP::defined(sunset)) {
            QCOMPARE(d.sunset(), sunset);
        } else {
            QVERIFY(!FP::defined(d.sunset()));
        }
        if (FP::defined(noon)) {
            QCOMPARE(d.noon(), noon);
            QCOMPARE(s1.noon(), noon);
        } else {
            QVERIFY(!FP::defined(d.noon()));
            QVERIFY(!FP::defined(s1.noon()));
        }

        Solar s2;
        s2.setLocation(latitude, longitude);
        s2.setTime(time);

        p = s2.position();
        if (FP::defined(azimuth)) {
            QCOMPARE(p.azimuth(), azimuth);
        } else {
            QVERIFY(!FP::defined(p.azimuth()));
        }
        if (FP::defined(elevation)) {
            QCOMPARE(p.elevation(), elevation);
        } else {
            QVERIFY(!FP::defined(p.elevation()));
        }

        d = s2.day();
        if (FP::defined(sunrise)) {
            QCOMPARE(d.sunrise(), sunrise);
        } else {
            QVERIFY(!FP::defined(d.sunrise()));
        }
        if (FP::defined(sunset)) {
            QCOMPARE(d.sunset(), sunset);
        } else {
            QVERIFY(!FP::defined(d.sunset()));
        }
        if (FP::defined(noon)) {
            QCOMPARE(d.noon(), noon);
            QCOMPARE(s2.noon(), noon);
        } else {
            QVERIFY(!FP::defined(d.noon()));
            QVERIFY(!FP::defined(s2.noon()));
        }
    }

    void solar_data()
    {
        QTest::addColumn<double>("time");
        QTest::addColumn<double>("latitude");
        QTest::addColumn<double>("longitude");
        QTest::addColumn<double>("azimuth");
        QTest::addColumn<double>("elevation");
        QTest::addColumn<double>("sunrise");
        QTest::addColumn<double>("sunset");
        QTest::addColumn<double>("noon");

        QTest::newRow("Invalid") << FP::undefined() << FP::undefined() << FP::undefined()
                                 << FP::undefined() << FP::undefined() << FP::undefined()
                                 << FP::undefined() << FP::undefined();

        QTest::newRow("Simple") << 1505325600.0 << 40.77 << -111.89 << 147.7534296687195
                                << 48.303543870394904 << 1505307962.982032 << 1505353269.9855568
                                << 1505330599.3007696;

        QTest::newRow("At night") << 1505260800.0 << 0.0 << 0.0 << 14.546086011796177
                                  << -86.07167344550348 << 1505281962.5228379 << 1505325563.244751
                                  << 1505303752.314623;

        QTest::newRow("Antarctic near dark") << 1505260800.0 << -89.9 << 0.0 << 179.0118997103677
                                             << -3.8172151972020743 << FP::undefined()
                                             << FP::undefined() << 1505303752.314623;

        QTest::newRow("Arctic light") << 1505260800.0 << 89.9 << 0.0 << 0.98787112586606840026
                                      << 3.901394900656669 << FP::undefined() << FP::undefined()
                                      << 1505303752.314623;
    }

    void isDark()
    {
        QVERIFY(!Solar(1505325600, 40.77, -111.89).position().isDark());
        QVERIFY(Solar(1505260800.0, 0.0, 0.0).position().isDark());
        QVERIFY(!Solar(1505260800.0, -89.9, 0.0).position().isDark());
        QVERIFY(!Solar(1505260800.0, 89.9, 0.0).position().isDark());
        QVERIFY(Solar(1497312000.0, -89.9, 0.0).position().isDark());
    }
};

QTEST_APPLESS_MAIN(TestSolar)

#include "solar.moc"
