/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "algorithms/linearint.hxx"

using namespace CPD3::Algorithms;

class TestLinearInt : public QObject {
Q_OBJECT
private slots:

    void single()
    {
        Model *m = new SingleLinearInterpolator(0.75, 0.5);

        QVector<double> l;
        QVector<double> r;
        double a;
        double b;
        double c;
        l << 0.0 << 0.5 << 1.0;

        r = m->applyConst(l);
        QCOMPARE(r, QVector<double>() << 0.5 << 0.875 << 1.25);
        r = m->apply(l);
        QCOMPARE(r, QVector<double>() << 0.5 << 0.875 << 1.25);
        r = l;
        m->applyIOConst(r);
        QCOMPARE(r, QVector<double>() << 0.5 << 0.875 << 1.25);
        r = l;
        m->applyIO(r);
        QCOMPARE(r, QVector<double>() << 0.5 << 0.875 << 1.25);
        QCOMPARE(m->oneToOneConst(0.5), 0.875);
        QCOMPARE(m->oneToOne(0.5), 0.875);
        QCOMPARE(m->twoToOneConst(0.5, 1.0), 0.875);
        QCOMPARE(m->twoToOne(0.5, 1.0), 0.875);
        QCOMPARE(m->threeToOneConst(0.5, 1.0, 2.0), 0.875);
        QCOMPARE(m->threeToOne(0.5, 1.0, 2.0), 0.875);
        a = 0.5;
        b = 0.0;
        c = 1.0;
        m->applyIOConst(a, b);
        QCOMPARE(a, 0.875);
        QCOMPARE(b, 0.5);
        a = 0.5;
        b = 0.0;
        c = 1.0;
        m->applyIO(a, b);
        QCOMPARE(a, 0.875);
        QCOMPARE(b, 0.5);
        a = 0.5;
        b = 0.0;
        c = 1.0;
        m->applyIOConst(a, b, c);
        QCOMPARE(a, 0.875);
        QCOMPARE(b, 0.5);
        QCOMPARE(c, 1.25);
        a = 0.5;
        b = 0.0;
        c = 1.0;
        m->applyIO(a, b, c);
        QCOMPARE(a, 0.875);
        QCOMPARE(b, 0.5);
        QCOMPARE(c, 1.25);
        Model *n;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << m;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> n;
            }
        }
        delete m;
        QCOMPARE(n->oneToOne(0.5), 0.875);
        m = n->clone();
        delete n;
        QCOMPARE(m->oneToOne(0.5), 0.875);
        delete m;

        m = new SingleLinearInterpolator(0.0, 0.5, 1.0, 1.25);
        QCOMPARE(m->oneToOne(0.0), 0.5);
        QCOMPARE(m->oneToOne(0.5), 0.875);
        QCOMPARE(m->oneToOne(1.0), 1.25);
        delete m;
    }

    void multiple()
    {
        QVector<double> x;
        x << 0.0 << 0.5 << 1.0;
        QVector<double> y;
        y << 0.0 << 1.0 << 0.5;
        Model *m = new LinearInterpolator(x, y);

        QVector<double> l;
        QVector<double> r;
        double a;
        double b;
        double c;
        l << 0.0 << 0.25 << 0.5 << 0.75 << 1.0;

        r = m->applyConst(l);
        QCOMPARE(r, QVector<double>() << 0.0 << 0.5 << 1.0 << 0.75 << 0.5);
        r = m->apply(l);
        QCOMPARE(r, QVector<double>() << 0.0 << 0.5 << 1.0 << 0.75 << 0.5);
        r = l;
        m->applyIOConst(r);
        QCOMPARE(r, QVector<double>() << 0.0 << 0.5 << 1.0 << 0.75 << 0.5);
        r = l;
        m->applyIO(r);
        QCOMPARE(r, QVector<double>() << 0.0 << 0.5 << 1.0 << 0.75 << 0.5);
        QCOMPARE(m->oneToOneConst(0.25), 0.5);
        QCOMPARE(m->oneToOne(0.25), 0.5);
        QCOMPARE(m->twoToOneConst(0.25, 1.0), 0.5);
        QCOMPARE(m->twoToOne(0.25, 1.0), 0.5);
        QCOMPARE(m->threeToOneConst(0.25, 1.0, 2.0), 0.5);
        QCOMPARE(m->threeToOne(0.25, 1.0, 2.0), 0.5);
        a = 0.25;
        b = 0.0;
        c = 0.75;
        m->applyIOConst(a, b);
        QCOMPARE(a, 0.5);
        QCOMPARE(b, 0.0);
        a = 0.25;
        b = 0.0;
        c = 0.75;
        m->applyIO(a, b);
        QCOMPARE(a, 0.5);
        QCOMPARE(b, 0.0);
        a = 0.25;
        b = 0.0;
        c = 0.75;
        m->applyIOConst(a, b, c);
        QCOMPARE(a, 0.5);
        QCOMPARE(b, 0.0);
        QCOMPARE(c, 0.75);
        a = 0.25;
        b = 0.0;
        c = 0.75;
        m->applyIO(a, b, c);
        QCOMPARE(a, 0.5);
        QCOMPARE(b, 0.0);
        QCOMPARE(c, 0.75);
        Model *n;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << m;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> n;
            }
        }
        delete m;
        QCOMPARE(n->oneToOne(0.25), 0.5);
        m = n->clone();
        delete n;
        QCOMPARE(m->oneToOne(0.25), 0.5);
        delete m;

        QCOMPARE(LinearInterpolator::interpolate(x.begin(), x.end(), y.begin(), 0.25), 0.5);
        QCOMPARE(LinearInterpolator::interpolate(x.begin(), x.end(), y.begin(), 0.4), 0.8);
        QCOMPARE(LinearInterpolator::interpolate(x.begin(), x.end(), y.begin(), 0.75), 0.75);
        QCOMPARE(LinearInterpolator::interpolate(x.begin(), x.end(), y.begin(), -0.1), -0.2);
        QCOMPARE(LinearInterpolator::interpolate(x.begin(), x.end(), y.begin(), 1.1), 0.4);
    }

    void transfer()
    {
        QVector<double> x;
        x << 0.0 << 0.5 << 1.0;
        QVector<double> y;
        y << 0.0 << 1.0 << 0.5;
        Model *m = new LinearTransferInterpolator(x, y,
                                                  LinearTransferInterpolator::Transfer_NormalizedSigmoid,
                                                  0.5);

        QVector<double> l;
        QVector<double> r;
        double a;
        double b;
        double c;
        l << 0.0 << 0.25 << 0.5 << 0.75 << 1.0;

        r = m->applyConst(l);
        QCOMPARE(r, QVector<double>() << 0.0 << 0.5 << 1.0 << 0.75 << 0.5);
        r = m->apply(l);
        QCOMPARE(r, QVector<double>() << 0.0 << 0.5 << 1.0 << 0.75 << 0.5);
        r = l;
        m->applyIOConst(r);
        QCOMPARE(r, QVector<double>() << 0.0 << 0.5 << 1.0 << 0.75 << 0.5);
        r = l;
        m->applyIO(r);
        QCOMPARE(r, QVector<double>() << 0.0 << 0.5 << 1.0 << 0.75 << 0.5);
        QCOMPARE(m->oneToOneConst(0.25), 0.5);
        QCOMPARE(m->oneToOne(0.25), 0.5);
        QCOMPARE(m->twoToOneConst(0.25, 1.0), 0.5);
        QCOMPARE(m->twoToOne(0.25, 1.0), 0.5);
        QCOMPARE(m->threeToOneConst(0.25, 1.0, 2.0), 0.5);
        QCOMPARE(m->threeToOne(0.25, 1.0, 2.0), 0.5);
        a = 0.25;
        b = 0.0;
        c = 0.75;
        m->applyIOConst(a, b);
        QCOMPARE(a, 0.5);
        QCOMPARE(b, 0.0);
        a = 0.25;
        b = 0.0;
        c = 0.75;
        m->applyIO(a, b);
        QCOMPARE(a, 0.5);
        QCOMPARE(b, 0.0);
        a = 0.25;
        b = 0.0;
        c = 0.75;
        m->applyIOConst(a, b, c);
        QCOMPARE(a, 0.5);
        QCOMPARE(b, 0.0);
        QCOMPARE(c, 0.75);
        a = 0.25;
        b = 0.0;
        c = 0.75;
        m->applyIO(a, b, c);
        QCOMPARE(a, 0.5);
        QCOMPARE(b, 0.0);
        QCOMPARE(c, 0.75);
        Model *n;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << m;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> n;
            }
        }
        delete m;
        QCOMPARE(n->oneToOne(0.25), 0.5);
        m = n->clone();
        delete n;
        QCOMPARE(m->oneToOne(0.25), 0.5);
        delete m;
    }

    void modelCreate()
    {
        Model *m = LinearInterpolator::create(QVector<double>() << 0.0 << 0.5 << 1.0,
                                              QVector<double>() << 0.0 << 1.0 << 0.5);
        QCOMPARE(m->oneToOne(0.25), 0.5);
        QCOMPARE(m->oneToOne(0.75), 0.75);
        delete m;

        m = LinearInterpolator::create(QVector<double>() << 0.0 << 0.5,
                                       QVector<double>() << 0.0 << 1.0);
        QCOMPARE(m->oneToOne(0.25), 0.5);
        QCOMPARE(m->oneToOne(0.75), 1.5);
        delete m;

        m = LinearInterpolator::create(QVector<double>() << 0.0, QVector<double>() << 0.5);
        QCOMPARE(m->oneToOne(0.75), 0.5);
        delete m;
    }
};

QTEST_APPLESS_MAIN(TestLinearInt)

#include "linearint.moc"
