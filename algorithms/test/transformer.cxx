/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "algorithms/transformer.hxx"
#include "core/number.hxx"
#include "datacore/variant/root.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;
using namespace CPD3::Data;

class TestTransformer : public QObject {
Q_OBJECT
private slots:

    void parameters()
    {
        TransformerParameters p;

        p.setParameter("p1", QVariant(QString("v1")));
        QCOMPARE(p.getParameter("p1").toString(), QString("v1"));

        p.setDouble("p2", 2.0);
        QCOMPARE(p.getDouble("p2"), 2.0);
    }

    void noopTransformer()
    {
        Transformer *t = new TransformerNOOP;
        QVector<TransformerPoint *> points;
        points.append(new TransformerPointStatic<2>(1.0, 2.0));
        points.append(new TransformerPointStatic<2>(2.0, 3.0));
        QVector<std::shared_ptr<TransformerPoint> > pointsShared;
        for (QVector<TransformerPoint *>::const_iterator add = points.constBegin(),
                end = points.constEnd(); add != end; ++add) {
            pointsShared.append(std::shared_ptr<TransformerPoint>(*add));
        }

        QVector<TransformerPoint *> result;
        t->applyConst(result);
        QVERIFY(result.isEmpty());
        t->apply(result);
        QVERIFY(result.isEmpty());

        QVector<std::shared_ptr<TransformerPoint> > resultShared;
        t->applyConst(resultShared);
        QVERIFY(resultShared.isEmpty());
        t->apply(resultShared);
        QVERIFY(resultShared.isEmpty());

        result = points;
        t->applyConst(result);
        QCOMPARE(result, points);
        t->apply(result);
        QCOMPARE(result, points);

        resultShared = pointsShared;
        t->applyConst(resultShared);
        QCOMPARE(resultShared, pointsShared);
        t->apply(resultShared);
        QCOMPARE(resultShared, pointsShared);

        Transformer *r;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << t;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> r;
            }
        }

        delete t;
        resultShared = pointsShared;
        r->applyConst(resultShared);
        t = r->clone();
        delete r;
        resultShared = pointsShared;
        t->applyConst(resultShared);
        delete t;
    }

    void constantTransformer()
    {
        Transformer *t = new TransformerConstant(5.0);
        QVector<TransformerPoint *> points;
        points.append(new TransformerPointStatic<2>(1.0, 2.0));
        points.append(new TransformerPointStatic<2>(3.0, 4.0));
        QVector<std::shared_ptr<TransformerPoint> > pointsShared;
        for (QVector<TransformerPoint *>::const_iterator add = points.constBegin(),
                end = points.constEnd(); add != end; ++add) {
            pointsShared.append(std::shared_ptr<TransformerPoint>(*add));
        }

        QVector<TransformerPointStatic<2> > expected;
        expected.append(TransformerPointStatic<2>(5.0, 5.0));
        expected.append(TransformerPointStatic<2>(5.0, 5.0));

        QVector<TransformerPoint *> result;
        t->applyConst(result);
        QVERIFY(result.isEmpty());
        t->apply(result);
        QVERIFY(result.isEmpty());

        QVector<std::shared_ptr<TransformerPoint> > resultShared;
        t->applyConst(resultShared);
        QVERIFY(resultShared.isEmpty());
        t->apply(resultShared);
        QVERIFY(resultShared.isEmpty());

        result = points;
        t->applyConst(result);
        QCOMPARE(result.size(), expected.size());
        for (int i = 0; i < expected.size(); i++) {
            QCOMPARE(result.at(i)->totalDimensions(), expected.at(i).totalDimensions());
            for (int j = 0; j < expected.at(i).totalDimensions(); j++) {
                QCOMPARE(result.at(i)->get(j), expected.at(i).get(j));
            }
        }
        t->apply(result);
        QCOMPARE(result.size(), expected.size());
        for (int i = 0; i < expected.size(); i++) {
            QCOMPARE(result.at(i)->totalDimensions(), expected.at(i).totalDimensions());
            for (int j = 0; j < expected.at(i).totalDimensions(); j++) {
                QCOMPARE(result.at(i)->get(j), expected.at(i).get(j));
            }
        }

        resultShared = pointsShared;
        t->applyConst(resultShared);
        QCOMPARE(resultShared.size(), expected.size());
        for (int i = 0; i < expected.size(); i++) {
            QCOMPARE(resultShared.at(i)->totalDimensions(), expected.at(i).totalDimensions());
            for (int j = 0; j < expected.at(i).totalDimensions(); j++) {
                QCOMPARE(resultShared.at(i)->get(j), expected.at(i).get(j));
            }
        }
        t->apply(resultShared);
        QCOMPARE(resultShared.size(), expected.size());
        for (int i = 0; i < expected.size(); i++) {
            QCOMPARE(resultShared.at(i)->totalDimensions(), expected.at(i).totalDimensions());
            for (int j = 0; j < expected.at(i).totalDimensions(); j++) {
                QCOMPARE(resultShared.at(i)->get(j), expected.at(i).get(j));
            }
        }

        Transformer *r;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << t;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> r;
            }
        }

        delete t;
        resultShared = pointsShared;
        r->applyConst(resultShared);
        QCOMPARE(resultShared.size(), expected.size());
        for (int i = 0; i < expected.size(); i++) {
            QCOMPARE(resultShared.at(i)->totalDimensions(), expected.at(i).totalDimensions());
            for (int j = 0; j < expected.at(i).totalDimensions(); j++) {
                QCOMPARE(resultShared.at(i)->get(j), expected.at(i).get(j));
            }
        }
        t = r->clone();
        delete r;
        resultShared = pointsShared;
        t->applyConst(resultShared);
        QCOMPARE(resultShared.size(), expected.size());
        for (int i = 0; i < expected.size(); i++) {
            QCOMPARE(resultShared.at(i)->totalDimensions(), expected.at(i).totalDimensions());
            for (int j = 0; j < expected.at(i).totalDimensions(); j++) {
                QCOMPARE(resultShared.at(i)->get(j), expected.at(i).get(j));
            }
        }
        delete t;
    }

    void differedRun()
    {
        Variant::Write config = Variant::Write::empty();
        config["Type"].setString("NOOP");
        QVector<TransformerPoint *> points;
        points.append(new TransformerPointStatic<2>(QVector<double>() << 1.0 << 2.0));
        points.append(new TransformerPointStatic<2>(QVector<double>() << 2.0 << 3.0));

        DeferredTransformerWrapper wrapper(points, config);
        QVERIFY(wrapper.waitForReady());
        QVERIFY(wrapper.isReady());
        QVERIFY(!wrapper.getPoints().isEmpty());
        bool ready = false;
        QVector<TransformerPoint *> result(wrapper.takePoints(&ready));
        QCOMPARE(result, points);
        QVERIFY(ready);
        qDeleteAll(result);
        QVERIFY(wrapper.getPoints().isEmpty());
        QVERIFY(wrapper.takePoints().isEmpty());
    }
};

QTEST_APPLESS_MAIN(TestTransformer)

#include "transformer.moc"
