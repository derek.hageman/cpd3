/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "algorithms/allan.hxx"
#include "algorithms/statistics.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;

class TestAllan : public QObject {
Q_OBJECT

private slots:

    void general()
    {
        Transformer *t = new Allan;

        QVector<std::shared_ptr<TransformerPoint> > inputPoints;
        for (int i = 0; i < 3600 * 4; i++) {
            inputPoints.append(std::shared_ptr<TransformerPoint>(
                    new TransformerPointStatic<2>(i, (double) (i % 10) + (double) i / 10000.0)));
        }
        QVector<std::shared_ptr<TransformerPoint> > result;

        result = inputPoints;
        t->apply(result, &TransformerPointStatic<2>::allocator);
        QVERIFY(result.size() > 5);
        for (QVector<std::shared_ptr<TransformerPoint> >::const_iterator
                check = result.constBegin() + 1, endCheck = result.constEnd();
                check != endCheck;
                ++check) {
            QVERIFY((*(check - 1))->get(0) < (*check)->get(0));
        }
        for (QVector<std::shared_ptr<TransformerPoint> >::const_iterator
                check = result.constBegin() + 5, endCheck = result.constEnd();
                check != endCheck;
                ++check) {
            QVERIFY((*(check - 1))->get(0) < (*check)->get(0) ||
                            (*(check - 2))->get(0) < (*check)->get(0) ||
                            (*(check - 3))->get(0) < (*check)->get(0) ||
                            (*(check - 4))->get(0) < (*check)->get(0) ||
                            (*(check - 5))->get(0) < (*check)->get(0));
        }

        delete t;
    }
};

QTEST_APPLESS_MAIN(TestAllan)

#include "allan.moc"
