/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QSslSocket>
#include <QSslCertificate>
#include <QSslKey>
#include <QTemporaryFile>
#include <QBuffer>

#include "algorithms/cryptography.hxx"
#include "datacore/variant/root.hxx"
#include "io/access.hxx"

using namespace CPD3::Algorithms;
using namespace CPD3::Data;
using namespace CPD3;

static const char *cert1Data = "-----BEGIN CERTIFICATE-----\n"
        "MIICbTCCAdYCCQDZ6mj3BI+kCTANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
        "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
        "DU5PQUEvR01EL0VTUkwxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
        "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1MzkwNloXDTM5MTIxMjE1MzkwNlow\n"
        "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
        "ZGVyMRYwFAYDVQQKDA1OT0FBL0dNRC9FU1JMMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
        "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
        "jQAwgYkCgYEAn3nIKzjLLEY8KG6+hUvrkjDkl9VU8HFKnp8wA2RsMta/UvKG4ZIY\n"
        "bNVsKSiAGm1G0i6Mtftw8quyihoxuy/hElH/XJChU5lersdrhalocHXoipNx8BeW\n"
        "T8sXuJOrh+IoDKqVI4I6Sz42qAlqBU7bNm40TE8sBPAOefyTGavC3e0CAwEAATAN\n"
        "BgkqhkiG9w0BAQUFAAOBgQBU2hljZZpJJ+zK6eePUpdELQGPGHJEbzfReeSBhRau\n"
        "iDj4jEa7IE3yrMv8NqC3Q+6lEVuJg0NInvDYsNYLPjMSPS+jKldrmrJvg+TFrp1w\n"
        "Qr3JoznNxwcwuDyrKjOVux6jo2aype3Zayo5VUizJX9Zo+xLe2MBVR1D0MXmoq8L\n"
        "zA==\n"
        "-----END CERTIFICATE-----";
static const char *cert1SHA512 = "57:DF:E3:DF:BB:14:C8:E4:3A:2F:9E:95:FF:57:58:68:"
        "6C:B2:F1:D2:0A:6A:FD:E7:7E:2B:57:7F:D1:A9:FE:AD:"
        "97:2E:AF:F5:B4:38:96:3A:92:ED:7C:D2:A5:0F:C0:44:"
        "F6:DD:D0:20:13:E6:76:63:81:78:3A:03:DD:20:18:91";
static const char *key1Data = "-----BEGIN RSA PRIVATE KEY-----\n"
        "MIICXAIBAAKBgQCfecgrOMssRjwobr6FS+uSMOSX1VTwcUqenzADZGwy1r9S8obh\n"
        "khhs1WwpKIAabUbSLoy1+3Dyq7KKGjG7L+ESUf9ckKFTmV6ux2uFqWhwdeiKk3Hw\n"
        "F5ZPyxe4k6uH4igMqpUjgjpLPjaoCWoFTts2bjRMTywE8A55/JMZq8Ld7QIDAQAB\n"
        "AoGBAIQOd0f7PpsKCfS9R7zfklG7dP+Z4z07wzu4vCyC8uniVAoe1LxjmyA8VtV6\n"
        "OSIpDTUs4M4tSWlZ7n1XlYjY6/lNt3xsy5BBQMlWVI8BV/yXnWRCxkQGBvXYTUKe\n"
        "7LRsUcqyGpAPeuStO/V8GEM5H272yv5S4413D09C9cAhROMtAkEA07j2lnnSl/WR\n"
        "0NFEOT9+nbh6Z+yRTzybC86TBhLCN3bY4VvqSWi1PdcU3oiz81dcaAPTksdx18/Y\n"
        "cSfz295ewwJBAMDTq22Dt2HMPQymxNUn9/IdIPU34/fEqSHS3e9hAgrp2gDVeNHR\n"
        "ZEzMkW00rLgdukkW51PV9hVIhYXdxYOjZY8CQFsf/cnwLurGf/b/SrzVDjr1/oEi\n"
        "Obx/2j+vrmnrwvm6Rkhglir4TSGLo+jPr5vpmtUN6I8BFoeLZp31UyjrwZ8CQC3W\n"
        "Y2ruI7qozV5jimjNToCMchg4yAVPB5GVydIsskqb2onWNRlTeE9VVcCrA9/kmTLk\n"
        "serY8t2OVsdCt8AaKHsCQFMEhJCpXaeLnrzToSv0uqRYjcT+r3ZjSghhSepN0ZA+\n"
        "PPj1CMmq9JrQaQAv3rT3hCbDF38TPhKX64OYzbuvdxA=\n"
        "-----END RSA PRIVATE KEY-----\n";
static const char *cert2Data = "-----BEGIN CERTIFICATE-----\n"
        "MIICbTCCAdYCCQCr9EVbER/M4TANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
        "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
        "DU5PQUEvRVNSTC9HTUQxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
        "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1NDIxNloXDTM5MTIxMjE1NDIxNlow\n"
        "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
        "ZGVyMRYwFAYDVQQKDA1OT0FBL0VTUkwvR01EMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
        "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
        "jQAwgYkCgYEAqa4sVcN1M4mWVqTqZuG2VikkOTpYrHYgnD4jFKDfi5FTFE9OQ5oW\n"
        "xhrBdoEtiWLEPmPSQCtJWCbD0d/YQ1ITxOF0OSaAa4c6PkdeiDmfull8EGmuaTFo\n"
        "ikcFS0r2SVtvIy3eFEAg9GBF4iCMp2AvUo2eqNCLt4CdRi6LQs2xkmUCAwEAATAN\n"
        "BgkqhkiG9w0BAQUFAAOBgQCEvlrqC1zChgkFMq8Oi7YRQOdx7ikzDodey/uu0r/W\n"
        "DxCnpA+F2f3okHiR5+LfYqQ5fvCYnmZd7w69tvdpui8swa15aXWaxLVD9/woyM2y\n"
        "Ofce/9pw6UgoYLkh9KsW5+2EP2Sn44lWcU5Jf8U/p8cDdjNIDzDG4vw7UFWiRakm\n"
        "Tw==\n"
        "-----END CERTIFICATE-----\n";
static const char *key2Data = "-----BEGIN RSA PRIVATE KEY-----\n"
        "MIICWwIBAAKBgQCprixVw3UziZZWpOpm4bZWKSQ5OlisdiCcPiMUoN+LkVMUT05D\n"
        "mhbGGsF2gS2JYsQ+Y9JAK0lYJsPR39hDUhPE4XQ5JoBrhzo+R16IOZ+6WXwQaa5p\n"
        "MWiKRwVLSvZJW28jLd4UQCD0YEXiIIynYC9SjZ6o0Iu3gJ1GLotCzbGSZQIDAQAB\n"
        "AoGAUzE2Q4ZlfDNFJo4M7wxTXcMmI3jb6RKxwmkkwgRuFfvWg+quMK7n45FSsUt8\n"
        "jBOErCI8/4E5oKLA97GMUtV3IxAXT/hxQRe6qZS7PEUW4k6Op7evX1hSCfI/IAB/\n"
        "AS6OqUR1U4+AFArhPS3CA+vuY4I01mpWpvI3dfvq29oUW4ECQQDQ4ItViA/CjWiq\n"
        "peW2+cptQ8QqkNGscjsUTZyp3aNtunC1mP/HRKlKLnuhY6G+LQWbzDv7BSJ9UNO4\n"
        "Ro1y90jJAkEAz/Xdjqx/xZntCleuxvMACWdhpMBqfdgs7+maLDXbtc9G43ygbqIz\n"
        "Wj99k9lu0L3iHdRlYicwdfFFBbvd4jsmvQJATOF5J4AvHNLjpXvuc0y5n0IEIA6x\n"
        "viFFcZGnijZUAv1OouivrG6vSOiXBK4hSFhV6iRgJ2KacTmg1ADT627tUQJAK4hl\n"
        "W9OCX8QMGekm/iCqNk284/cfk75oEcTN8ElJ9/Iu/bn9/4rWwyKdUBDpIKtPJT1s\n"
        "B7L6cwYRk9Sy6wPE5QJAFmjRUr3GypvuC4haazaOMyIt/FEbC+kUs9kq6JZsiBuM\n"
        "LbZ02ko9IkhfOiUIewV6+Xb2j+1u/Itc737M6RZ+3Q==\n"
        "-----END RSA PRIVATE KEY-----";


static bool writeData(QIODevice *target, const char *data)
{
    const char *endData = data + strlen(data);
    while (data != endData) {
        qint64 n = target->write(data, endData - data);
        if (n == -1)
            return false;
        data += n;
    }
    return true;
}

class TestCryptography : public QObject {
Q_OBJECT

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void setupSocket()
    {
        QSslSocket socket;

        Variant::Write config;
        config.detachFromRoot();
        config["Key"].setString(key1Data);
        config["Certificate"].setString(cert1Data);

        QVERIFY(Cryptography::setupSocket(&socket, config));
    }

    void certificateDigest()
    {
        QByteArray cert1Raw(QByteArray::fromRawData(cert1Data, strlen(cert1Data)));
        QSslCertificate cert1(cert1Raw);
        QVERIFY(!cert1.isNull());

        QByteArray expected;
        QList<QByteArray> parts(QByteArray(cert1SHA512).split(':'));
        for (QList<QByteArray>::const_iterator hex = parts.constBegin(), end = parts.constEnd();
                hex != end;
                ++hex) {
            expected.append((char) hex->toUShort(0, 16));
        }

        QCOMPARE(Cryptography::sha512(cert1), expected);

        QCOMPARE(Cryptography::sha512(IO::Access::buffer(
                CPD3::Util::ByteArray(cert1.toDer()))->stream()).toQByteArrayRef(), expected);
        QCOMPARE(Cryptography::sha512(cert1.toDer()), expected);
    }

    void getCertificate()
    {
        QByteArray cert1Raw(QByteArray::fromRawData(cert1Data, strlen(cert1Data)));
        QByteArray cert2Raw(QByteArray::fromRawData(cert2Data, strlen(cert1Data)));
        QSslCertificate cert1(cert1Raw);
        QVERIFY(!cert1.isNull());
        QSslCertificate cert2(cert2Raw);
        QVERIFY(!cert2.isNull());
        QVERIFY(cert1 != cert2);

        Variant::Write v = Variant::Write::empty();
        v.setString(QString(cert1Data));
        QSslCertificate testCert(Cryptography::getCertificate(v));
        QCOMPARE(testCert, cert1);
        QVERIFY(testCert != cert2);

        v.setString(QString(cert2Data));
        testCert = Cryptography::getCertificate(v);
        QCOMPARE(testCert, cert2);
        QVERIFY(testCert != cert1);

        QTemporaryFile file;
        QVERIFY(file.open());
        QVERIFY(writeData(&file, cert1Data));
        file.close();
        v.setString(file.fileName());
        testCert = Cryptography::getCertificate(v);
        QCOMPARE(testCert, cert1);
        QVERIFY(testCert != cert2);

        QVERIFY(file.open());
        file.seek(0);
        file.resize(0);
        QVERIFY(writeData(&file, cert2Data));
        file.close();
        testCert = Cryptography::getCertificate(v);
        QCOMPARE(testCert, cert2);
        QVERIFY(testCert != cert1);
    }

    void getKey()
    {
        QByteArray key1Raw(QByteArray::fromRawData(key1Data, strlen(key1Data)));
        QByteArray key2Raw(QByteArray::fromRawData(key2Data, strlen(key1Data)));
        QSslKey key1(key1Raw, QSsl::Rsa);
        QVERIFY(!key1.isNull());
        QSslKey key2(key2Raw, QSsl::Rsa);
        QVERIFY(!key2.isNull());
        QVERIFY(key1 != key2);

        Variant::Write v = Variant::Write::empty();
        v.setString(QString(key1Data));
        QSslKey testCert(Cryptography::getKey(v));
        QCOMPARE(testCert, key1);
        QVERIFY(testCert != key2);

        v.setString(QString(key2Data));
        testCert = Cryptography::getKey(v);
        QCOMPARE(testCert, key2);
        QVERIFY(testCert != key1);

        QTemporaryFile file;
        QVERIFY(file.open());
        QVERIFY(writeData(&file, key1Data));
        file.close();
        v.setString(file.fileName());
        testCert = Cryptography::getKey(v);
        QCOMPARE(testCert, key1);
        QVERIFY(testCert != key2);

        QVERIFY(file.open());
        file.seek(0);
        file.resize(0);
        QVERIFY(writeData(&file, key2Data));
        file.close();
        testCert = Cryptography::getKey(v);
        QCOMPARE(testCert, key2);
        QVERIFY(testCert != key1);
    }

    void signData()
    {
        Util::ByteArray data("ABCD 12345");

        Variant::Write v = Variant::Write::empty();
        v.setString(QString(key1Data));
        auto sig1 = Cryptography::signData(v, IO::Access::buffer(data)->stream());
        QVERIFY(!sig1.empty());

        v.setString(QString(cert1Data));
        QVERIFY(Cryptography::checkSignature(v, sig1, IO::Access::buffer(data)->stream()));

        v.setString(QString(cert2Data));
        QVERIFY(!Cryptography::checkSignature(v, sig1, IO::Access::buffer(data)->stream()));

        QTemporaryFile file;
        QVERIFY(file.open());
        QVERIFY(writeData(&file, key1Data));
        file.close();
        v.setString(file.fileName());
        v.setString(QString(cert1Data));
        sig1 = Cryptography::signData(v, IO::Access::buffer(data)->stream());
        v.setString(QString(key1Data));
        QVERIFY(Cryptography::checkSignature(v, sig1, IO::Access::buffer(data)->stream()));

        v.setString(QString(key2Data));
        auto sig2 = Cryptography::signData(v, IO::Access::buffer(data)->stream());
        QVERIFY(!sig2.empty());
        v.setString(QString(cert1Data));
        QVERIFY(!Cryptography::checkSignature(v, sig2, IO::Access::buffer(data)->stream()));
        v.setString(QString(cert2Data));
        QVERIFY(Cryptography::checkSignature(v, sig2, IO::Access::buffer(data)->stream()));

        QVERIFY(file.open());
        file.seek(0);
        file.resize(0);
        QVERIFY(writeData(&file, key2Data));
        file.close();
        v.setString(file.fileName());
        sig2 = Cryptography::signData(v, IO::Access::buffer(data)->stream());
        v.setString(QString(cert2Data));
        QVERIFY(Cryptography::checkSignature(v, sig2, IO::Access::buffer(data)->stream()));

        QVERIFY(file.open());
        file.seek(0);
        file.resize(0);
        QVERIFY(writeData(&file, cert1Data));
        file.close();
        v.setString(file.fileName());
        QVERIFY(Cryptography::checkSignature(v, sig1, IO::Access::buffer(data)->stream(), true));

        static const char *dataSignatureMBEDTLS = "70:DF:AB:76:51:A7:F3:04:60:CD:6C:B0:68:24:42:6B:"
                                                  "F6:49:03:B4:64:4E:F6:A5:F3:65:58:38:5F:59:51:0E:"
                                                  "DB:A6:FE:D0:9A:29:1D:08:B3:53:BA:47:48:F3:9D:8C:"
                                                  "14:02:B7:29:9E:E5:2F:95:E3:09:AA:0F:79:35:8D:73:"
                                                  "E0:2A:2D:DF:FB:10:71:AE:50:42:22:5A:DC:A6:9A:E1:"
                                                  "62:9B:4C:40:E5:BE:07:41:D5:9A:80:5B:D4:1F:AE:DF:"
                                                  "D2:33:F7:29:0E:F8:46:2C:7A:18:D9:BA:D8:E4:16:E2:"
                                                  "D5:4C:DE:83:3C:27:80:3B:F8:A2:90:94:EB:9B:F7:CC";
        QByteArray expected;
        QList<QByteArray> parts(QByteArray(dataSignatureMBEDTLS).split(':'));
        for (QList<QByteArray>::const_iterator hex = parts.constBegin(), end = parts.constEnd();
                hex != end;
                ++hex) {
            expected.append((char) hex->toUShort(0, 16));
        }
        v.setString(QString(cert1Data));
        QVERIFY(Cryptography::checkSignature(v, expected, IO::Access::buffer(data)->stream()));
    }

    void dataEncryption()
    {
        Util::ByteArray data1("ABCD data1");
        Util::ByteArray data2 = Util::ByteArray::filled(32, 8192);
        Variant::Write v = Variant::Write::empty();

        v.setString(QString(key1Data));
        auto encrypted11 = Cryptography::encryptData(v, data1);
        QVERIFY(!encrypted11.empty());
        QVERIFY(encrypted11 != data1);

        auto encrypted12 = Cryptography::encryptData(v, data1);
        QVERIFY(!encrypted12.empty());
        QVERIFY(encrypted12 != data1);
        QVERIFY(encrypted11 != encrypted12);

        auto encrypted13 = Cryptography::encryptData(v, data2);
        QVERIFY(!encrypted13.empty());
        QVERIFY(encrypted13 != data2);

        auto encrypted14 = Cryptography::encryptData(v, data2);
        QVERIFY(!encrypted14.empty());
        QVERIFY(encrypted14 != data2);
        QVERIFY(encrypted14 != encrypted13);

        v.setString(QString(cert1Data));
        auto check = Cryptography::decryptData(v, encrypted11);
        QCOMPARE(check.toQByteArrayRef(), data1.toQByteArrayRef());
        check = Cryptography::decryptData(v, encrypted12);
        QCOMPARE(check.toQByteArrayRef(), data1.toQByteArrayRef());
        check = Cryptography::decryptData(v, encrypted13);
        QCOMPARE(check.toQByteArrayRef(), data2.toQByteArrayRef());
        check = Cryptography::decryptData(v, encrypted14);
        QCOMPARE(check.toQByteArrayRef(), data2.toQByteArrayRef());

        v.setString(QString(cert2Data));
        check = Cryptography::decryptData(v, encrypted11);
        QVERIFY(check != data1);
        check = Cryptography::decryptData(v, encrypted13);
        QVERIFY(check != data2);


        v.setString(QString(key2Data));
        auto encrypted21 = Cryptography::encryptData(v, data1);
        QVERIFY(!encrypted21.empty());
        QVERIFY(encrypted21 != data1);
        QVERIFY(encrypted21 != encrypted11);

        auto encrypted22 = Cryptography::encryptData(v, data1);
        QVERIFY(!encrypted22.empty());
        QVERIFY(encrypted22 != data1);
        QVERIFY(encrypted21 != encrypted22);
        QVERIFY(encrypted21 != encrypted12);

        auto encrypted23 = Cryptography::encryptData(v, data2);
        QVERIFY(!encrypted23.empty());
        QVERIFY(encrypted23 != data2);
        QVERIFY(encrypted23 != encrypted13);

        auto encrypted24 = Cryptography::encryptData(v, data2);
        QVERIFY(!encrypted24.empty());
        QVERIFY(encrypted24 != data2);
        QVERIFY(encrypted24 != encrypted23);
        QVERIFY(encrypted24 != encrypted14);

        v.setString(QString(cert2Data));
        check = Cryptography::decryptData(v, encrypted21);
        QCOMPARE(check.toQByteArrayRef(), data1.toQByteArrayRef());
        check = Cryptography::decryptData(v, encrypted22);
        QCOMPARE(check.toQByteArrayRef(), data1.toQByteArrayRef());
        check = Cryptography::decryptData(v, encrypted23);
        QCOMPARE(check.toQByteArrayRef(), data2.toQByteArrayRef());
        check = Cryptography::decryptData(v, encrypted24);
        QCOMPARE(check.toQByteArrayRef(), data2.toQByteArrayRef());

        QByteArray fixedData("193d3e1e534dba9525cb40b3949942cb8c9d25019fbf9fcdcb89a0241bb9580"
                             "50f4e4ed5847eee547ca9aea2df6382c66638c56d39d1b75cef301f"
                             "cf9e10cf6c9d169e978bdbf62132f5fbfb631e40561d1e19ff3a6df"
                             "c533e770727bcfd2d4e33ffb9991d997c1e7ea8d883ae7a05d5ba7d"
                             "e75949e870c1076ca82e5c293115bd85d33e4e6cf5d5b90a");
        fixedData = QByteArray::fromHex(fixedData);
        check = Cryptography::decryptData(v, Util::ByteArray(fixedData));
        QCOMPARE(check.toQByteArrayRef(), data1.toQByteArrayRef());

        v.setString(QString(cert1Data));
        check = Cryptography::decryptData(v, encrypted21);
        QVERIFY(check != data1);
        check = Cryptography::decryptData(v, encrypted23);
        QVERIFY(check != data2);
    }

    void generate()
    {
        auto pair1 =
                Cryptography::generateSelfSigned(QStringList() << "CN=Cert1" << "OU=CPD3").read();
        QVERIFY(pair1.hash("Key").exists());
        QVERIFY(pair1.hash("Certificate").exists());
        QSslCertificate checkCert1(Cryptography::getCertificate(pair1));
        QVERIFY(!checkCert1.isNull());
        QSslKey checkKey1(Cryptography::getKey(pair1));
        QVERIFY(!checkKey1.isNull());

        auto pair2 = Cryptography::generateSelfSigned().read();
        QVERIFY(pair2.hash("Key").exists());
        QVERIFY(pair2.hash("Certificate").exists());
        QSslCertificate checkCert2(Cryptography::getCertificate(pair2));
        QVERIFY(!checkCert2.isNull());
        QSslKey checkKey2(Cryptography::getKey(pair2));
        QVERIFY(!checkKey2.isNull());

        QVERIFY(checkCert1 != checkCert2);
        QVERIFY(checkKey1 != checkKey2);

        Util::ByteArray data("QWERTYUIOP 0987654321");
        auto encrypted1 = Cryptography::encryptData(pair1, data);
        QVERIFY(!encrypted1.empty());
        auto encrypted2 = Cryptography::encryptData(pair2, data);
        QVERIFY(!encrypted2.empty());
        QVERIFY(encrypted1 != encrypted2);

        auto decrypted = Cryptography::decryptData(pair1, encrypted1);
        QCOMPARE(decrypted.toQByteArrayRef(), data.toQByteArrayRef());
        decrypted = Cryptography::decryptData(pair2, encrypted2);
        QCOMPARE(decrypted.toQByteArrayRef(), data.toQByteArrayRef());


        QSslCertificate testCert(Cryptography::getCertificate(pair1));

        Variant::Write config = Variant::Write::empty();
        config["CheckSelfSigned"] = true;
        config["RequireName"] = "Cert1";
        QVERIFY(Cryptography::verifyCertificate(testCert, config));

        config.remove();
        config["RequireValid"] = true;
        QVERIFY(!Cryptography::verifyCertificate(testCert, config));

        config["Authority"].setString(QString::fromLatin1(testCert.toPem()));
        QVERIFY(Cryptography::verifyCertificate(testCert, config));
    }
};

QTEST_MAIN(TestCryptography)

#include "cryptography.moc"
