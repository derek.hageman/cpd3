/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "algorithms/cdf.hxx"
#include "algorithms/statistics.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;

Q_DECLARE_METATYPE(QVector<double>);

class TestCDF : public QObject {
Q_OBJECT

    static bool comparePoints(const QVector<std::shared_ptr<TransformerPoint> > &result,
                              const QVector<double> &expected,
                              const QVector<double> &input)
    {
        if (result.size() != expected.size())
            return false;
        for (int i = 0, max = result.size(); i < max; i++) {
            if (!FP::defined(expected.at(i)))
                continue;
            if (result.at(i)->totalDimensions() < 2) {
                if (!FP::defined(result.at(i)->get(0)))
                    return false;
                if (fabs(expected.at(i) - result.at(i)->get(0)) > 1E-6)
                    return false;
            } else {
                if (!FP::equal(input.at(i), result.at(i)->get(0)))
                    return false;
                if (!FP::defined(result.at(i)->get(1)))
                    return false;
                if (fabs(expected.at(i) - result.at(i)->get(1)) > 1E-6)
                    return false;
            }
        }
        return true;
    }

private slots:

    void normal()
    {
        QFETCH(QVector<double>, values);

        Transformer *t = new NormalCDF;

        QVector<std::shared_ptr<TransformerPoint> > inputPoints1;
        QVector<std::shared_ptr<TransformerPoint> > inputPoints2;
        QVector<double> transformed;

        std::sort(values.begin(), values.end());
        for (int i = 0, max = values.size(); i < max; i++) {
            inputPoints1.append(
                    std::shared_ptr<TransformerPoint>(new TransformerPointStatic<2>(values.at(i))));
            inputPoints2.append(std::shared_ptr<TransformerPoint>(
                    new TransformerPointStatic<2>(values.at(i), values.at(i))));
            transformed.append(Statistics::normalQuantile((double) (i + 1) / (double) (max + 2)));
        }

        QVector<std::shared_ptr<TransformerPoint> > points;

        points = inputPoints1;
        t->apply(points);
        QVERIFY(comparePoints(points, transformed, values));
        points = inputPoints1;
        t->applyConst(points);
        QVERIFY(comparePoints(points, transformed, values));
        points = inputPoints2;
        t->apply(points);
        QVERIFY(comparePoints(points, transformed, values));
        points = inputPoints2;
        t->applyConst(points);
        QVERIFY(comparePoints(points, transformed, values));

        Transformer *u;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << t;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> u;
            }
        }
        delete t;
        points = inputPoints1;
        u->apply(points);
        QVERIFY(comparePoints(points, transformed, values));
        points = inputPoints1;
        u->applyConst(points);
        QVERIFY(comparePoints(points, transformed, values));
        points = inputPoints2;
        u->apply(points);
        QVERIFY(comparePoints(points, transformed, values));
        points = inputPoints2;
        u->applyConst(points);
        QVERIFY(comparePoints(points, transformed, values));

        t = u->clone();
        delete u;

        points = inputPoints1;
        t->apply(points);
        QVERIFY(comparePoints(points, transformed, values));
        points = inputPoints1;
        t->applyConst(points);
        QVERIFY(comparePoints(points, transformed, values));
        points = inputPoints2;
        t->apply(points);
        QVERIFY(comparePoints(points, transformed, values));
        points = inputPoints2;
        t->applyConst(points);
        QVERIFY(comparePoints(points, transformed, values));

        delete t;
    }

    void normal_data()
    {
        QTest::addColumn<QVector<double> >("values");

        QTest::newRow("Empty") << (QVector<double>());
        QTest::newRow("Three") << (QVector<double>() << 0.25 << 0.75 << 0.5);
        QTest::newRow("Four") << (QVector<double>() << 0.25 << 0.75 << 0.5 << 1.0);
        QTest::newRow("Five") << (QVector<double>() << 0.25 << 0.75 << 0.5 << 1.0 << 2.0);
    }
};

QTEST_APPLESS_MAIN(TestCDF)

#include "cdf.moc"
