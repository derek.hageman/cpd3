/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "algorithms/npolynomial.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;

class TestNPolynomial : public QObject {
Q_OBJECT
private slots:

    void order2LMA()
    {
        QVector<double> x;
        QVector<double> y;

        x <<
                0.00 <<
                0.25 <<
                0.50 <<
                0.75 <<
                1.00 <<
                1.25 <<
                1.50 <<
                1.75 <<
                2.00 <<
                2.25 <<
                2.50 <<
                2.75 <<
                3.00 <<
                3.25 <<
                3.50 <<
                3.75 <<
                4.00;
        y <<
                0.25000 <<
                0.31875 <<
                0.37500 <<
                0.41875 <<
                0.45000 <<
                0.46875 <<
                0.47500 <<
                0.46875 <<
                0.45000 <<
                0.41875 <<
                0.37500 <<
                0.31875 <<
                0.25000 <<
                0.16875 <<
                0.07500 <<
                -0.03125 <<
                -0.15000;

        NPolynomialLMA poly(x, y, 2);

        QCOMPARE(poly.order(), 2);
        QVERIFY(fabs(poly.coefficient(0) - 0.25) < 0.001);
        QVERIFY(fabs(poly.coefficient(1) - 0.3) < 0.001);
        QVERIFY(fabs(poly.coefficient(2) + 0.1) < 0.001);

        QVERIFY(fabs(poly.oneToOne(0.1) - 0.279) < 0.001);
    }

    void order2ZeroLMA()
    {
        QVector<double> x;
        QVector<double> y;

        x <<
                0.00 <<
                0.25 <<
                0.50 <<
                0.75 <<
                1.00 <<
                1.25 <<
                1.50 <<
                1.75 <<
                2.00 <<
                2.25 <<
                2.50 <<
                2.75 <<
                3.00 <<
                3.25 <<
                3.50 <<
                3.75 <<
                4.00;
        y <<
                0.00000 <<
                0.10625 <<
                0.17500 <<
                0.20625 <<
                0.20000 <<
                0.15625 <<
                0.07500 <<
                -0.04375 <<
                -0.20000 <<
                -0.39375 <<
                -0.62500 <<
                -0.89375 <<
                -1.20000 <<
                -1.54375 <<
                -1.92500 <<
                -2.34375 <<
                -2.80000;

        ZeroNPolynomialLMA poly(x, y, 2);

        QCOMPARE(poly.order(), 2);
        QCOMPARE(poly.coefficient(0), 0.0);
        QVERIFY(fabs(poly.coefficient(1) - 0.5) < 0.001);
        QVERIFY(fabs(poly.coefficient(2) + 0.3) < 0.001);

        QVERIFY(fabs(poly.oneToOne(0.4) - 0.152) < 0.001);
    }
};

QTEST_APPLESS_MAIN(TestNPolynomial)

#include "npolynomial.moc"
