/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cmath>

#include <QObject>
#include <QTest>

#include "algorithms/rayleigh.hxx"

using namespace CPD3::Algorithms;

static bool scatteringEqual(double calc, double expected)
{
    return std::fabs(calc - expected) < (expected * 0.02);
}

class TestRayLeigh : public QObject {
Q_OBJECT
private slots:

    void basic()
    {
        /* Some selections Table 2 */
        QVERIFY(scatteringEqual(Rayleigh::scattering(230, Rayleigh::Air, 288.15), 469.1));
        QVERIFY(scatteringEqual(Rayleigh::scattering(350, Rayleigh::Air, 288.15), 74.50));
        QVERIFY(scatteringEqual(Rayleigh::scattering(450, Rayleigh::Air, 288.15), 26.16));
        QVERIFY(scatteringEqual(Rayleigh::scattering(510, Rayleigh::Air, 288.15), 15.64));
        QVERIFY(scatteringEqual(Rayleigh::scattering(550, Rayleigh::Air, 288.15), 11.49));
        QVERIFY(scatteringEqual(Rayleigh::scattering(600, Rayleigh::Air, 288.15), 8.053));
        QVERIFY(scatteringEqual(Rayleigh::scattering(700, Rayleigh::Air, 288.15), 4.310));
        QVERIFY(scatteringEqual(Rayleigh::scattering(780, Rayleigh::Air, 288.15), 2.781));

        /* The "standard" values for the TSI neph */
        QVERIFY(scatteringEqual(Rayleigh::scattering(450), 27.89));
        QVERIFY(scatteringEqual(Rayleigh::scattering(550), 12.26));
        QVERIFY(scatteringEqual(Rayleigh::scattering(700), 4.605));
        QVERIFY(scatteringEqual(Rayleigh::scattering(450, Rayleigh::CO2), 27.89 * 2.61));
        QVERIFY(scatteringEqual(Rayleigh::scattering(550, Rayleigh::CO2), 12.26 * 2.61));
        QVERIFY(scatteringEqual(Rayleigh::scattering(700, Rayleigh::CO2), 4.605 * 2.61));
    }

    void angular()
    {
        /* The "standard" values for the TSI neph */
        QVERIFY(scatteringEqual(Rayleigh::angleScattering(450), 27.89));
        QVERIFY(scatteringEqual(Rayleigh::angleScattering(550), 12.26));
        QVERIFY(scatteringEqual(Rayleigh::angleScattering(700), 4.605));
        QVERIFY(scatteringEqual(Rayleigh::angleScattering(450, 90), 27.89 / 2.0));
        QVERIFY(scatteringEqual(Rayleigh::angleScattering(550, 90), 12.26 / 2.0));
        QVERIFY(scatteringEqual(Rayleigh::angleScattering(700, 90), 4.605 / 2.0));
    }
};

QTEST_APPLESS_MAIN(TestRayLeigh)

#include "rayleigh.moc"
