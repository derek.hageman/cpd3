/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "algorithms/linearcluster.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;

Q_DECLARE_METATYPE(QVector<double>);

Q_DECLARE_METATYPE(QVector<int>);

namespace QTest {
template<>
char *toString(const QVector<double> &output)
{
    QByteArray ba;
    for (int i = 0, maxBin = output.size(); i < maxBin; i++) {
        if (i != 0) ba += ", ";
        if (FP::defined(output.at(i)))
            ba += QByteArray::number(output.at(i));
        else
            ba += "UNDEFINED";
    }
    return qstrdup(ba.data());
}

template<>
char *toString(const QVector<int> &output)
{
    QByteArray ba;
    for (int i = 0, maxBin = output.size(); i < maxBin; i++) {
        if (i != 0) ba += ", ";
        if (FP::defined(output.at(i)))
            ba += QByteArray::number(output.at(i));
        else
            ba += "UNDEFINED";
    }
    return qstrdup(ba.data());
}
};

class TestLinearCluster : public QObject {
Q_OBJECT

private slots:

    void cluster()
    {
        QFETCH(QVector<double>, input);
        QFETCH(QVector<int>, expectedBins);
        QFETCH(QVector<double>, expectedBreaks);

        QVector<double> resultBreaks;
        QVector<int> resultBins = LinearCluster::cluster(input, &resultBreaks);

        QCOMPARE(resultBins, expectedBins);
        QCOMPARE(resultBreaks, expectedBreaks);
    }

    void cluster_data()
    {
        QTest::addColumn<QVector<double> >("input");
        QTest::addColumn<QVector<int> >("expectedBins");
        QTest::addColumn<QVector<double> >("expectedBreaks");

        QTest::newRow("Empty") << (QVector<double>()) << (QVector<int>()) << (QVector<double>());
        QTest::newRow("All undefined") <<
                (QVector<double>() << FP::undefined() << FP::undefined()) <<
                (QVector<int>() << -1 << -1) <<
                (QVector<double>());

        QTest::newRow("Single point") <<
                (QVector<double>() << 1.0) <<
                (QVector<int>() << 0) <<
                (QVector<double>());
        QTest::newRow("Two identical") <<
                (QVector<double>() << 1.0 << 1.0) <<
                (QVector<int>() << 0 << 0) <<
                (QVector<double>());
        QTest::newRow("Three identical") <<
                (QVector<double>() << 1.0 << 1.0 << 1.0) <<
                (QVector<int>() << 0 << 0 << 0) <<
                (QVector<double>());

        QTest::newRow("Two bin") <<
                (QVector<double>() << 0.8 << 0.9 << 1.0 << 5.0 << 5.1) <<
                (QVector<int>() << 0 << 0 << 0 << 1 << 1) <<
                (QVector<double>() << 3.0);

        QTest::newRow("Three bin") <<
                (QVector<double>() << 0.8 << 0.9 << 1.0 << 2.0 << 5.0 << 5.1 << 2.0) <<
                (QVector<int>() << 0 << 0 << 0 << 1 << 2 << 2 << 1) <<
                (QVector<double>() << 1.5 << 3.5);

        QTest::newRow("Two bin zero") <<
                (QVector<double>() << 1.0 << 2.0 << 1.0 << 2.0 << 2.0 << 2.0) <<
                (QVector<int>() << 0 << 1 << 0 << 1 << 1 << 1) <<
                (QVector<double>() << 1.5);

        QTest::newRow("Three bin zero") <<
                (QVector<double>() << 1.0 << 2.0 << 1.0 << 2.0 << 2.0 << 2.0 << 0.0 << 0.0) <<
                (QVector<int>() << 1 << 2 << 1 << 2 << 2 << 2 << 0 << 0) <<
                (QVector<double>() << 0.5 << 1.5);

        QTest::newRow("Three bin start zero") <<
                (QVector<double>() <<
                        2.0 <<
                        2.0 <<
                        2.0 <<
                        1.0 <<
                        0.9 <<
                        0.8 <<
                        -1.05 <<
                        -1.1 <<
                        -1.15) <<
                (QVector<int>() << 2 << 2 << 2 << 1 << 1 << 1 << 0 << 0 << 0) <<
                (QVector<double>() << (-1.05 + 0.8) * 0.5 << (2.0 + 1.0) * 0.5); // Rounding
    }
};

QTEST_APPLESS_MAIN(TestLinearCluster)

#include "linearcluster.moc"
