/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "algorithms/cspline.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;

class TestCSpline : public QObject {
Q_OBJECT
private slots:

    void natural()
    {
        QVector<double> x;
        x << 0.0 << 0.5 << 1.0;
        QVector<double> y;
        y << 0.0 << 0.125 << 1.0;
        CSpline spline(x, y);

        QCOMPARE(spline.evaluate(0.0), 0.0);
        QCOMPARE(spline.evaluateConst(0.0), 0.0);
        QCOMPARE(spline.derivative(0.0), -0.125);
        QCOMPARE(spline.derivativeConst(0.0), -0.125);
        QCOMPARE(spline.evaluate(0.5), 0.125);
        QCOMPARE(spline.evaluateConst(0.5), 0.125);
        QCOMPARE(spline.derivative(0.5), 1.0);
        QCOMPARE(spline.derivativeConst(0.5), 1.0);
        QCOMPARE(spline.evaluate(1.0), 1.0);
        QCOMPARE(spline.evaluateConst(1.0), 1.0);

        double v = 0.0 + 0.25 * -0.125 + 0.25 * 0.25 * 0.0 + 0.25 * 0.25 * 0.25 * 1.5;
        QCOMPARE(spline.evaluate(0.25), v);
        QCOMPARE(spline.evaluateConst(0.25), v);

        v = 0.125 + 0.25 * 1.0 + 0.25 * 0.25 * 2.25 + 0.25 * 0.25 * 0.25 * -1.5;
        QCOMPARE(spline.evaluate(0.75), v);
        QCOMPARE(spline.evaluateConst(0.75), v);

        v = -0.125 + 0.25 * 0.0 * 2.0 + 0.25 * 0.25 * 1.5 * 3.0;
        QCOMPARE(spline.derivative(0.25), v);
        QCOMPARE(spline.derivativeConst(0.25), v);

        v = 1.0 + 0.25 * 2.25 * 2.0 + 0.25 * 0.25 * -1.5 * 3.0;
        QCOMPARE(spline.derivative(0.75), v);
        QCOMPARE(spline.derivativeConst(0.75), v);

        CSpline spline2;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << spline;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> spline2;
            }
        }
        QCOMPARE(spline2.derivative(0.75), v);
        QCOMPARE(spline2.derivativeConst(0.75), v);
    }

    void clamped()
    {
        QVector<double> x;
        x << 0.0 << 0.5 << 1.0;
        QVector<double> y;
        y << 0.0 << 0.125 << 1.0;
        CSpline spline(x, y, 0.0, 0.0);

        QCOMPARE(spline.evaluate(0.0), 0.0);
        QCOMPARE(spline.evaluateConst(0.0), 0.0);
        QCOMPARE(spline.evaluate(0.5), 0.125);
        QCOMPARE(spline.evaluateConst(0.5), 0.125);
        QCOMPARE(spline.evaluate(1.0), 1.0);
        QCOMPARE(spline.evaluateConst(1.0), 1.0);

        double v = 0.0 + 0.25 * 0.0 + 0.25 * 0.25 * -1.5 + 0.25 * 0.25 * 0.25 * 4.0;
        QCOMPARE(spline.evaluate(0.25), v);
        QCOMPARE(spline.evaluateConst(0.25), v);

        v = 0.125 + 0.25 * 1.5 + 0.25 * 0.25 * 4.5 + 0.25 * 0.25 * 0.25 * -8.0;
        QCOMPARE(spline.evaluate(0.75), v);
        QCOMPARE(spline.evaluateConst(0.75), v);

        v = 0.0 + 0.25 * -1.5 * 2.0 + 0.25 * 0.25 * 4.0 * 3.0;
        QCOMPARE(spline.derivative(0.25), v);
        QCOMPARE(spline.derivativeConst(0.25), v);

        v = 1.5 + 0.25 * 4.5 * 2.0 + 0.25 * 0.25 * -8.0 * 3.0;
        QCOMPARE(spline.derivative(0.75), v);
        QCOMPARE(spline.derivativeConst(0.75), v);

        CSpline spline2;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << spline;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> spline2;
            }
        }
        QCOMPARE(spline2.derivative(0.75), v);
        QCOMPARE(spline2.derivativeConst(0.75), v);
    }

    void model()
    {
        Model *m = CSplineInterpolator::create(QVector<double>() << 0.0 << 0.5 << 1.0,
                                               QVector<double>() << 0.0 << 0.125 << 1.0);

        double v = 0.125 + 0.25 * 1.0 + 0.25 * 0.25 * 2.25 + 0.25 * 0.25 * 0.25 * -1.5;
        QVector<double> l;
        QVector<double> r;
        double a;
        double b;
        double c;
        l << 0.75 << 0.0 << 0.5 << 1.0;

        r = m->applyConst(l);
        QCOMPARE(r, QVector<double>() << v << 0.0 << 0.125 << 1.0);
        r = m->apply(l);
        QCOMPARE(r, QVector<double>() << v << 0.0 << 0.125 << 1.0);
        r = l;
        m->applyIOConst(r);
        QCOMPARE(r, QVector<double>() << v << 0.0 << 0.125 << 1.0);
        r = l;
        m->applyIO(r);
        QCOMPARE(r, QVector<double>() << v << 0.0 << 0.125 << 1.0);
        QCOMPARE(m->oneToOneConst(0.75), v);
        QCOMPARE(m->oneToOne(0.75), v);
        QCOMPARE(m->twoToOneConst(0.75, 1.0), v);
        QCOMPARE(m->twoToOne(0.75, 1.0), v);
        QCOMPARE(m->threeToOneConst(0.75, 1.0, 2.0), v);
        QCOMPARE(m->threeToOne(0.75, 1.0, 2.0), v);
        a = 0.75;
        b = 0.5;
        c = 1.0;
        m->applyIOConst(a, b);
        QCOMPARE(a, v);
        QCOMPARE(b, 0.125);
        a = 0.75;
        b = 0.5;
        c = 1.0;
        m->applyIO(a, b);
        QCOMPARE(a, v);
        QCOMPARE(b, 0.125);
        a = 0.75;
        b = 0.5;
        c = 1.0;
        m->applyIOConst(a, b, c);
        QCOMPARE(a, v);
        QCOMPARE(b, 0.125);
        QCOMPARE(c, 1.0);
        a = 0.75;
        b = 0.5;
        c = 1.0;
        m->applyIO(a, b, c);
        QCOMPARE(a, v);
        QCOMPARE(b, 0.125);
        QCOMPARE(c, 1.0);
        Model *n;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << m;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> n;
            }
        }
        delete m;
        QCOMPARE(n->oneToOne(0.75), v);
        m = n->clone();
        delete n;
        QCOMPARE(m->oneToOne(0.75), v);
        delete m;

        m = CSplineInterpolator::create(QVector<double>() << 0.0, QVector<double>() << 0.5);
        QCOMPARE(m->oneToOne(0.75), 0.5);
        delete m;

        m = CSplineInterpolator::create(QVector<double>() << 0.0 << 0.0,
                                        QVector<double>() << 0.0 << 0.5);
        QCOMPARE(m->oneToOne(0.75), 0.25);
        delete m;

        m = CSplineInterpolator::create(QVector<double>() << 0.0 << 1.0,
                                        QVector<double>() << 0.0 << 0.5);
        QCOMPARE(m->oneToOne(0.5), 0.25);
        QCOMPARE(m->oneToOne(0.0), 0.0);
        QCOMPARE(m->oneToOne(1.0), 0.5);
        delete m;


        m = CSplineInterpolator::create(QVector<double>() << 0.0 << 1.0,
                                        QVector<double>() << 0.0 << 0.5, FP::undefined(), 0.0);
        v = 0 + 0.75 * 0.75 + 0.75 * 0.75 * 0.0 + 0.75 * 0.75 * 0.75 * -0.25;
        l.clear();
        l << 0.75 << 0.0 << 1.0;
        r = m->applyConst(l);
        QCOMPARE(r, QVector<double>() << v << 0.0 << 0.5);
        r = m->apply(l);
        QCOMPARE(r, QVector<double>() << v << 0.0 << 0.5);
        r = l;
        m->applyIOConst(r);
        QCOMPARE(r, QVector<double>() << v << 0.0 << 0.5);
        r = l;
        m->applyIO(r);
        QCOMPARE(r, QVector<double>() << v << 0.0 << 0.5);
        QCOMPARE(m->oneToOneConst(0.75), v);
        QCOMPARE(m->oneToOne(0.75), v);
        QCOMPARE(m->twoToOneConst(0.75, 2.0), v);
        QCOMPARE(m->twoToOne(0.75, 1.0), v);
        QCOMPARE(m->threeToOneConst(0.75, 2.0, 2.0), v);
        QCOMPARE(m->threeToOne(0.75, 2.0, 3.0), v);
        a = 0.75;
        b = 0.0;
        c = 1.0;
        m->applyIOConst(a, b);
        QCOMPARE(a, v);
        QCOMPARE(b, 0.0);
        a = 0.75;
        b = 0.0;
        c = 1.0;
        m->applyIO(a, b);
        QCOMPARE(a, v);
        QCOMPARE(b, 0.0);
        a = 0.75;
        b = 0.0;
        c = 1.0;
        m->applyIOConst(a, b, c);
        QCOMPARE(a, v);
        QCOMPARE(b, 0.0);
        QCOMPARE(c, 0.5);
        a = 0.75;
        b = 0.0;
        c = 1.0;
        m->applyIO(a, b, c);
        QCOMPARE(a, v);
        QCOMPARE(b, 0.0);
        QCOMPARE(c, 0.5);
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << m;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> n;
            }
        }
        delete m;
        QCOMPARE(n->oneToOne(0.75), v);
        m = n->clone();
        delete n;
        QCOMPARE(m->oneToOne(0.75), v);
        delete m;
    }
};

QTEST_APPLESS_MAIN(TestCSpline)

#include "cspline.moc"
