/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QVector>

#include "algorithms/fourier.hxx"

using namespace CPD3::Algorithms;
using namespace std;

typedef complex<double> CD;

Q_DECLARE_METATYPE(QVector<CD>);

Q_DECLARE_METATYPE(QVector<double>);


class TestFourier : public QObject {
Q_OBJECT


private slots:

    void bitReverse()
    {
        for (int nbits = 2; nbits < 12; nbits++) {
            QVector<int> result;
            QVector<int> input;
            for (int i = 0; i < (1 << nbits); i++) {
                input.append(i);

                /* Slow, safe and stupid bit reversing */
                int output = 0;
                for (int bit = 0; bit < nbits; bit++) {
                    if (!(i & (1 << bit)))
                        continue;
                    output |= (1 << ((nbits - 1) - bit));
                }
                result.append(output);
            }

            Fourier::bitReverse(input);
            QCOMPARE(input, result);
        }
    }

    void fullComplex()
    {
        QFETCH(QVector<CD>, input);
        QFETCH(QVector<CD>, expected);

        QVector<CD> original(input);
        Fourier::forward(input);
        QCOMPARE(input.size(), expected.size());
        for (int i = 0, max = input.size(); i < max; i++) {
            QVERIFY(fabs(real(input[i]) - real(expected[i])) < 1E-3);
            QVERIFY(fabs(imag(input[i]) - imag(expected[i])) < 1E-3);
        }

        Fourier::inverse(input);
        QCOMPARE(input.size(), original.size());
        for (int i = 0, max = input.size(); i < max; i++) {
            QVERIFY(fabs(real(input[i]) - real(original[i])) < 1E-3);
            QVERIFY(fabs(imag(input[i]) - imag(original[i])) < 1E-3);
        }
    }

    void fullComplex_data()
    {
        QTest::addColumn<QVector<CD> >("input");
        QTest::addColumn<QVector<CD> >("expected");

        QTest::newRow("Basic") <<
                (QVector<CD>() <<
                        CD(1, 32) <<
                        CD(2, 31) <<
                        CD(3, 30) <<
                        CD(4, 29) <<
                        CD(5, 28) <<
                        CD(6, 27) <<
                        CD(7, 26) <<
                        CD(8, 25) <<
                        CD(9, 24) <<
                        CD(10, 23) <<
                        CD(11, 22) <<
                        CD(12, 21) <<
                        CD(13, 20) <<
                        CD(14, 19) <<
                        CD(15, 18) <<
                        CD(16, 17) <<
                        CD(17, 16) <<
                        CD(18, 15) <<
                        CD(19, 14) <<
                        CD(20, 13) <<
                        CD(21, 12) <<
                        CD(22, 11) <<
                        CD(23, 10) <<
                        CD(24, 9) <<
                        CD(25, 8) <<
                        CD(26, 7) <<
                        CD(27, 6) <<
                        CD(28, 5) <<
                        CD(29, 4) <<
                        CD(30, 3) <<
                        CD(31, 2) <<
                        CD(32, 1)) <<
                (QVector<CD>() <<
                        CD(528, 528) <<
                        CD(146.4507, 178.4507) <<
                        CD(64.43743, 96.43743) <<
                        CD(36.74493, 68.74493) <<
                        CD(22.62742, 54.62742) <<
                        CD(13.93389, 45.93389) <<
                        CD(7.945692, 39.94569) <<
                        CD(3.496056, 35.49606) <<
                        CD(0, 32) <<
                        CD(-2.869139, 29.13086) <<
                        CD(-5.309142, 26.69086) <<
                        CD(-7.447822, 24.55218) <<
                        CD(-9.372583, 22.62742) <<
                        CD(-11.14645, 20.85355) <<
                        CD(-12.8174, 19.1826) <<
                        CD(-14.42414, 17.57586) <<
                        CD(-16, 16) <<
                        CD(-17.57586, 14.42414) <<
                        CD(-19.1826, 12.8174) <<
                        CD(-20.85355, 11.14645) <<
                        CD(-22.62742, 9.372583) <<
                        CD(-24.55218, 7.447822) <<
                        CD(-26.69086, 5.309142) <<
                        CD(-29.13086, 2.869139) <<
                        CD(-32, 0) <<
                        CD(-35.49606, -3.496056) <<
                        CD(-39.94569, -7.945692) <<
                        CD(-45.93389, -13.93389) <<
                        CD(-54.62742, -22.62742) <<
                        CD(-68.74493, -36.74493) <<
                        CD(-96.43743, -64.43743) <<
                        CD(-178.4507, -146.4507));

        QTest::newRow("Single Real") <<
                (QVector<CD>() <<
                        CD(0.3826834, 0) <<
                        CD(0.7071068, 0) <<
                        CD(0.9238795, 0) <<
                        CD(1, 0) <<
                        CD(0.9238795, 0) <<
                        CD(0.7071068, 0) <<
                        CD(0.3826834, 0) <<
                        CD(1.224647e-16, 0) <<
                        CD(-0.3826834, 0) <<
                        CD(-0.7071068, 0) <<
                        CD(-0.9238795, 0) <<
                        CD(-1, 0) <<
                        CD(-0.9238795, 0) <<
                        CD(-0.7071068, 0) <<
                        CD(-0.3826834, 0) <<
                        CD(-2.449294e-16, 0) <<
                        CD(0.3826834, 0) <<
                        CD(0.7071068, 0) <<
                        CD(0.9238795, 0) <<
                        CD(1, 0) <<
                        CD(0.9238795, 0) <<
                        CD(0.7071068, 0) <<
                        CD(0.3826834, 0) <<
                        CD(3.67394e-16, 0) <<
                        CD(-0.3826834, 0) <<
                        CD(-0.7071068, 0) <<
                        CD(-0.9238795, 0) <<
                        CD(-1, 0) <<
                        CD(-0.9238795, 0) <<
                        CD(-0.7071068, 0) <<
                        CD(-0.3826834, 0) <<
                        CD(-4.898587e-16, 0) <<
                        CD(0.3826834, 0) <<
                        CD(0.7071068, 0) <<
                        CD(0.9238795, 0) <<
                        CD(1, 0) <<
                        CD(0.9238795, 0) <<
                        CD(0.7071068, 0) <<
                        CD(0.3826834, 0) <<
                        CD(6.123234e-16, 0) <<
                        CD(-0.3826834, 0) <<
                        CD(-0.7071068, 0) <<
                        CD(-0.9238795, 0) <<
                        CD(-1, 0) <<
                        CD(-0.9238795, 0) <<
                        CD(-0.7071068, 0) <<
                        CD(-0.3826834, 0) <<
                        CD(-7.347881e-16, 0) <<
                        CD(0.3826834, 0) <<
                        CD(0.7071068, 0) <<
                        CD(0.9238795, 0) <<
                        CD(1, 0) <<
                        CD(0.9238795, 0) <<
                        CD(0.7071068, 0) <<
                        CD(0.3826834, 0) <<
                        CD(8.572528e-16, 0) <<
                        CD(-0.3826834, 0) <<
                        CD(-0.7071068, 0) <<
                        CD(-0.9238795, 0) <<
                        CD(-1, 0) <<
                        CD(-0.9238795, 0) <<
                        CD(-0.7071068, 0) <<
                        CD(-0.3826834, 0) <<
                        CD(-9.797174e-16, 0)) <<
                (QVector<CD>() <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(12.24587, -29.56415) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(12.24587, 29.56415) <<
                        CD(0, 0) <<
                        CD(0, 0) <<
                        CD(0, 0));

        QTest::newRow("Factorizing") <<
                (QVector<CD>() <<
                        CD(1, 0) <<
                        CD(2, 0) <<
                        CD(3, 0) <<
                        CD(4, 0) <<
                        CD(5, 0) <<
                        CD(6, 0) <<
                        CD(7, 0) <<
                        CD(8, 0) <<
                        CD(9, 0) <<
                        CD(10, 0) <<
                        CD(11, 0) <<
                        CD(12, 0) <<
                        CD(13, 0) <<
                        CD(14, 0) <<
                        CD(15, 0) <<
                        CD(16, 0) <<
                        CD(17, 0) <<
                        CD(18, 0) <<
                        CD(19, 0) <<
                        CD(20, 0) <<
                        CD(21, 0) <<
                        CD(22, 0) <<
                        CD(23, 0) <<
                        CD(24, 0) <<
                        CD(25, 0) <<
                        CD(26, 0) <<
                        CD(27, 0) <<
                        CD(28, 0) <<
                        CD(29, 0) <<
                        CD(30, 0)) <<
                (QVector<CD>() <<
                        CD(465, 0) <<
                        CD(-15, 142.7155) <<
                        CD(-15, 70.56945) <<
                        CD(-15, 46.16525) <<
                        CD(-15, 33.69055) <<
                        CD(-15, 25.98076) <<
                        CD(-15, 20.64573) <<
                        CD(-15, 16.65919) <<
                        CD(-15, 13.50606) <<
                        CD(-15, 10.89814) <<
                        CD(-15, 8.660254) <<
                        CD(-15, 6.67843) <<
                        CD(-15, 4.873795) <<
                        CD(-15, 3.188348) <<
                        CD(-15, 1.576564) <<
                        CD(-15, -3.552714e-15) <<
                        CD(-15, -1.576564) <<
                        CD(-15, -3.188348) <<
                        CD(-15, -4.873795) <<
                        CD(-15, -6.67843) <<
                        CD(-15, -8.660254) <<
                        CD(-15, -10.89814) <<
                        CD(-15, -13.50606) <<
                        CD(-15, -16.65919) <<
                        CD(-15, -20.64573) <<
                        CD(-15, -25.98076) <<
                        CD(-15, -33.69055) <<
                        CD(-15, -46.16525) <<
                        CD(-15, -70.56945) <<
                        CD(-15, -142.7155));
    }

    void realOnly()
    {
        QFETCH(QVector<double>, input);
        QFETCH(QVector<CD>, expected);

        QVector<complex<double> > result(Fourier::forward(input));
        QCOMPARE(result.size(), expected.size());
        for (int i = 0, max = result.size(); i < max; i++) {
            QVERIFY(fabs(real(result[i]) - real(expected[i])) < 1E-3);
            QVERIFY(fabs(imag(result[i]) - imag(expected[i])) < 1E-3);
        }

        QVector<double> recovered(Fourier::inverseReal(result));
        QCOMPARE(recovered.size(), input.size());
        for (int i = 0, max = result.size(); i < max; i++) {
            QVERIFY(fabs(recovered[i] - input[i]) < 1E-3);
        }
    }

    void realOnly_data()
    {
        QTest::addColumn<QVector<double> >("input");
        QTest::addColumn<QVector<CD> >("expected");

        QTest::newRow("Basic") <<
                (QVector<double>() <<
                        1 <<
                        2 <<
                        3 <<
                        4 <<
                        5 <<
                        6 <<
                        7 <<
                        8 <<
                        9 <<
                        10 <<
                        11 <<
                        12 <<
                        13 <<
                        14 <<
                        15 <<
                        16) <<
                (QVector<CD>() <<
                        CD(136, 0) <<
                        CD(-8, 40.21872) <<
                        CD(-8, 19.31371) <<
                        CD(-8, 11.97285) <<
                        CD(-8, 8) <<
                        CD(-8, 5.345429) <<
                        CD(-8, 3.313708) <<
                        CD(-8, 1.591299) <<
                        CD(-8, 0) <<
                        CD(-8, -1.591299) <<
                        CD(-8, -3.313708) <<
                        CD(-8, -5.345429) <<
                        CD(-8, -8) <<
                        CD(-8, -11.97285) <<
                        CD(-8, -19.31371) <<
                        CD(-8, -40.21872));

        QTest::newRow("Factorizing") <<
                (QVector<double>() << 1 << 2 << 3 << 4 << 5 << 6 << 7 << 8 << 9 << 10) <<
                (QVector<CD>() <<
                        CD(55, 0) <<
                        CD(-5, 15.38842) <<
                        CD(-5, 6.88191) <<
                        CD(-5, 3.632713) <<
                        CD(-5, 1.624598) <<
                        CD(-5, 0) <<
                        CD(-5, -1.624598) <<
                        CD(-5, -3.632713) <<
                        CD(-5, -6.88191) <<
                        CD(-5, -15.38842));
    }

#if 0
    void benchmarkForward() {
        QVector<CD> data;
        for (int i=0; i<(1<<18); i++) {
            data.append(CD(i, 0.0));
        }
        
        QBENCHMARK {
            Fourier::forward(data);
        }
    }
    
    void benchmarkInverse() {
        QVector<CD> data;
        for (int i=0; i<(1<<18); i++) {
            data.append(CD(i, 0.0));
        }
        
        QBENCHMARK {
            Fourier::inverse(data);
        }
    }
#endif
};

QTEST_APPLESS_MAIN(TestFourier)

#include "fourier.moc"
