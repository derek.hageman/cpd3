/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "algorithms/mie.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;

struct TestMieResult {
    double qsca;
    double qbsc;
    double qabs;
    double qext;
    double gsca;

    TestMieResult() : qsca(FP::undefined()),
                      qbsc(FP::undefined()),
                      qabs(FP::undefined()),
                      qext(FP::undefined()),
                      gsca(FP::undefined())
    { }
};

class TestMieTableResult : public MieTable::CalculateHandler {
public:
    TestMieResult *target;

    TestMieTableResult(TestMieResult *t) : target(t)
    { }

    virtual ~TestMieTableResult()
    { }

    virtual void mieResult(double qsca, double qbsc, double qabs, double qext, double gsca)
    {
        target->qsca = qsca;
        target->qbsc = qbsc;
        target->qabs = qabs;
        target->qext = qext;
        target->gsca = gsca;
    }
};

static const double roundoffLimit = 1E10;

class TestMie : public QObject {
Q_OBJECT

private slots:

    void bohrenHuffman()
    {
        QFETCH(double, riR);
        QFETCH(double, riI);
        QFETCH(int, nAng);
        QFETCH(double, x);
        QFETCH(double, expectedQsca);
        QFETCH(double, expectedQbsc);
        QFETCH(double, expectedQabs);
        QFETCH(double, expectedQext);
        QFETCH(double, expectedGsca);

        Mie mie(riR, riI, nAng, Mie::BohrenHuffman);
        double qsca = 0;
        double qbsc = 0;
        double qabs = 0;
        double qext = 0;
        double gsca = 0;
        mie.calculate(x, &qsca, &qbsc, &qabs, &qext, &gsca);

        /* Fix for order of operations differences */
        qsca = floor(qsca * roundoffLimit) / roundoffLimit;
        qbsc = floor(qbsc * roundoffLimit) / roundoffLimit;
        qabs = floor(qabs * roundoffLimit) / roundoffLimit;
        qext = floor(qext * roundoffLimit) / roundoffLimit;
        gsca = floor(gsca * roundoffLimit) / roundoffLimit;
        expectedQsca = floor(expectedQsca * roundoffLimit) / roundoffLimit;
        expectedQbsc = floor(expectedQbsc * roundoffLimit) / roundoffLimit;
        expectedQabs = floor(expectedQabs * roundoffLimit) / roundoffLimit;
        expectedQext = floor(expectedQext * roundoffLimit) / roundoffLimit;
        expectedGsca = floor(expectedGsca * roundoffLimit) / roundoffLimit;

        QCOMPARE(qext, expectedQext);
        QCOMPARE(qsca, expectedQsca);
        QCOMPARE(qbsc, expectedQbsc);
        QCOMPARE(qabs, expectedQabs);
        QCOMPARE(gsca, expectedGsca);

        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            mie.serialize(stream);
        }
        QDataStream stream(&data, QIODevice::ReadOnly);
        Mie mie2(stream);

        qsca = 0;
        qbsc = 0;
        qabs = 0;
        qext = 0;
        gsca = 0;
        mie.calculate(x, &qsca, &qbsc, &qabs, &qext, &gsca);

        /* Fix for order of operations differences */
        qsca = floor(qsca * roundoffLimit) / roundoffLimit;
        qbsc = floor(qbsc * roundoffLimit) / roundoffLimit;
        qabs = floor(qabs * roundoffLimit) / roundoffLimit;
        qext = floor(qext * roundoffLimit) / roundoffLimit;
        gsca = floor(gsca * roundoffLimit) / roundoffLimit;

        QCOMPARE(qext, expectedQext);
        QCOMPARE(qsca, expectedQsca);
        QCOMPARE(qbsc, expectedQbsc);
        QCOMPARE(qabs, expectedQabs);
        QCOMPARE(gsca, expectedGsca);
    }

    void bohrenHuffman_data()
    {
        QTest::addColumn<double>("riR");
        QTest::addColumn<double>("riI");
        QTest::addColumn<int>("nAng");
        QTest::addColumn<double>("x");
        QTest::addColumn<double>("expectedQsca");
        QTest::addColumn<double>("expectedQbsc");
        QTest::addColumn<double>("expectedQabs");
        QTest::addColumn<double>("expectedQext");
        QTest::addColumn<double>("expectedGsca");

        QTest::newRow("1.33 0.1 10 0.1") <<
                1.33 <<
                0.1 <<
                10 <<
                0.1 <<
                1.2134663158e-05 <<
                1.8121544093e-05 <<
                2.2562126004e-02 <<
                2.2574260667e-02 <<
                1.8284116889e-03;
        QTest::newRow("1.33 0.1 2 0.2") <<
                1.33 <<
                0.1 <<
                2 <<
                0.2 <<
                1.9361631879e-04 <<
                2.8531209142e-04 <<
                4.5605794325e-02 <<
                4.5799410643e-02 <<
                7.3063003419e-03;
        QTest::newRow("1.4 0.01 4 0.4") <<
                1.4 <<
                0.01 <<
                4 <<
                0.4 <<
                3.9947521078e-03 <<
                5.5672028184e-03 <<
                9.1680842503e-03 <<
                1.3162836358e-02 <<
                3.0042536009e-02;
        QTest::newRow("1.53 0.001 2 0.25") <<
                1.53 <<
                0.001 <<
                2 <<
                0.25 <<
                9.9937201581e-04 <<
                1.4548384520e-03 <<
                5.0509422016e-04 <<
                1.5044662360e-03 <<
                1.2513836598e-02;
    }

    void table()
    {
        MieTable mie(new Mie(1.33, 0.1, 10), true);

        double qsca = 0;
        double qbsc = 0;
        double qabs = 0;
        double qext = 0;
        double gsca = 0;

        double expectedQsca = 1.2134663158e-05;
        double expectedQbsc = 1.8121544093e-05;
        double expectedQabs = 2.2562126004e-02;
        double expectedQext = 2.2574260667e-02;
        double expectedGsca = 1.8284116889e-03;
        mie.calculate(0.1, &qsca, &qbsc, &qabs, &qext, &gsca);
        /* Fix for order of operations differences */
        qsca = floor(qsca * roundoffLimit) / roundoffLimit;
        qbsc = floor(qbsc * roundoffLimit) / roundoffLimit;
        qabs = floor(qabs * roundoffLimit) / roundoffLimit;
        qext = floor(qext * roundoffLimit) / roundoffLimit;
        gsca = floor(gsca * roundoffLimit) / roundoffLimit;
        expectedQsca = floor(expectedQsca * roundoffLimit) / roundoffLimit;
        expectedQbsc = floor(expectedQbsc * roundoffLimit) / roundoffLimit;
        expectedQabs = floor(expectedQabs * roundoffLimit) / roundoffLimit;
        expectedQext = floor(expectedQext * roundoffLimit) / roundoffLimit;
        expectedGsca = floor(expectedGsca * roundoffLimit) / roundoffLimit;

        QCOMPARE(qext, expectedQext);
        QCOMPARE(qsca, expectedQsca);
        QCOMPARE(qbsc, expectedQbsc);
        QCOMPARE(qabs, expectedQabs);
        QCOMPARE(gsca, expectedGsca);

        mie.calculate(0.1, &qsca, &qbsc, &qabs, &qext, &gsca);
        qsca = floor(qsca * roundoffLimit) / roundoffLimit;
        qbsc = floor(qbsc * roundoffLimit) / roundoffLimit;
        qabs = floor(qabs * roundoffLimit) / roundoffLimit;
        qext = floor(qext * roundoffLimit) / roundoffLimit;
        gsca = floor(gsca * roundoffLimit) / roundoffLimit;
        QCOMPARE(qext, expectedQext);
        QCOMPARE(qsca, expectedQsca);
        QCOMPARE(qbsc, expectedQbsc);
        QCOMPARE(qabs, expectedQabs);
        QCOMPARE(gsca, expectedGsca);

        mie.calculate(0.1 + 1E-12, &qsca, &qbsc, &qabs, &qext, &gsca);
        qsca = floor(qsca * roundoffLimit) / roundoffLimit;
        qbsc = floor(qbsc * roundoffLimit) / roundoffLimit;
        qabs = floor(qabs * roundoffLimit) / roundoffLimit;
        qext = floor(qext * roundoffLimit) / roundoffLimit;
        gsca = floor(gsca * roundoffLimit) / roundoffLimit;
        QCOMPARE(qext, expectedQext);
        QCOMPARE(qsca, expectedQsca);
        QCOMPARE(qbsc, expectedQbsc);
        QCOMPARE(qabs, expectedQabs);
        QCOMPARE(gsca, expectedGsca);

        mie.calculate(0.1 - 1E-12, &qsca, &qbsc, &qabs, &qext, &gsca);
        qsca = floor(qsca * roundoffLimit) / roundoffLimit;
        qbsc = floor(qbsc * roundoffLimit) / roundoffLimit;
        qabs = floor(qabs * roundoffLimit) / roundoffLimit;
        qext = floor(qext * roundoffLimit) / roundoffLimit;
        gsca = floor(gsca * roundoffLimit) / roundoffLimit;
        QCOMPARE(qext, expectedQext);
        QCOMPARE(qsca, expectedQsca);
        QCOMPARE(qbsc, expectedQbsc);
        QCOMPARE(qabs, expectedQabs);
        QCOMPARE(gsca, expectedGsca);

        expectedQsca = 1.9361631879e-04;
        expectedQbsc = 2.8531209142e-04;
        expectedQabs = 4.5605794325e-02;
        expectedQext = 4.5799410643e-02;
        expectedGsca = 7.3063003419e-03;
        mie.calculate(0.2, &qsca, &qbsc, &qabs, &qext, &gsca);
        qsca = floor(qsca * roundoffLimit) / roundoffLimit;
        qbsc = floor(qbsc * roundoffLimit) / roundoffLimit;
        qabs = floor(qabs * roundoffLimit) / roundoffLimit;
        qext = floor(qext * roundoffLimit) / roundoffLimit;
        gsca = floor(gsca * roundoffLimit) / roundoffLimit;
        expectedQsca = floor(expectedQsca * roundoffLimit) / roundoffLimit;
        expectedQbsc = floor(expectedQbsc * roundoffLimit) / roundoffLimit;
        expectedQabs = floor(expectedQabs * roundoffLimit) / roundoffLimit;
        expectedQext = floor(expectedQext * roundoffLimit) / roundoffLimit;
        expectedGsca = floor(expectedGsca * roundoffLimit) / roundoffLimit;

        QCOMPARE(qext, expectedQext);
        QCOMPARE(qsca, expectedQsca);
        QCOMPARE(qbsc, expectedQbsc);
        QCOMPARE(qabs, expectedQabs);
        QCOMPARE(gsca, expectedGsca);

        mie.calculate(0.2, &qsca, &qbsc, &qabs, &qext, &gsca);
        qsca = floor(qsca * roundoffLimit) / roundoffLimit;
        qbsc = floor(qbsc * roundoffLimit) / roundoffLimit;
        qabs = floor(qabs * roundoffLimit) / roundoffLimit;
        qext = floor(qext * roundoffLimit) / roundoffLimit;
        gsca = floor(gsca * roundoffLimit) / roundoffLimit;
        QCOMPARE(qext, expectedQext);
        QCOMPARE(qsca, expectedQsca);
        QCOMPARE(qbsc, expectedQbsc);
        QCOMPARE(qabs, expectedQabs);
        QCOMPARE(gsca, expectedGsca);

        mie.calculate(0.2 + 1E-12, &qsca, &qbsc, &qabs, &qext, &gsca);
        qsca = floor(qsca * roundoffLimit) / roundoffLimit;
        qbsc = floor(qbsc * roundoffLimit) / roundoffLimit;
        qabs = floor(qabs * roundoffLimit) / roundoffLimit;
        qext = floor(qext * roundoffLimit) / roundoffLimit;
        gsca = floor(gsca * roundoffLimit) / roundoffLimit;
        QCOMPARE(qext, expectedQext);
        QCOMPARE(qsca, expectedQsca);
        QCOMPARE(qbsc, expectedQbsc);
        QCOMPARE(qabs, expectedQabs);
        QCOMPARE(gsca, expectedGsca);

        mie.calculate(0.2 - 1E-12, &qsca, &qbsc, &qabs, &qext, &gsca);
        qsca = floor(qsca * roundoffLimit) / roundoffLimit;
        qbsc = floor(qbsc * roundoffLimit) / roundoffLimit;
        qabs = floor(qabs * roundoffLimit) / roundoffLimit;
        qext = floor(qext * roundoffLimit) / roundoffLimit;
        gsca = floor(gsca * roundoffLimit) / roundoffLimit;
        QCOMPARE(qext, expectedQext);
        QCOMPARE(qsca, expectedQsca);
        QCOMPARE(qbsc, expectedQbsc);
        QCOMPARE(qabs, expectedQabs);
        QCOMPARE(gsca, expectedGsca);

        mie.scanDone();


        mie.calculate(0.2, &qsca, &qbsc, &qabs, &qext, &gsca);
        qsca = floor(qsca * roundoffLimit) / roundoffLimit;
        qbsc = floor(qbsc * roundoffLimit) / roundoffLimit;
        qabs = floor(qabs * roundoffLimit) / roundoffLimit;
        qext = floor(qext * roundoffLimit) / roundoffLimit;
        gsca = floor(gsca * roundoffLimit) / roundoffLimit;
        QCOMPARE(qext, expectedQext);
        QCOMPARE(qsca, expectedQsca);
        QCOMPARE(qbsc, expectedQbsc);
        QCOMPARE(qabs, expectedQabs);
        QCOMPARE(gsca, expectedGsca);

        mie.calculate(0.195, &qsca, &qbsc, &qabs, &qext, &gsca);
        QVERIFY(qext < 4.5799410643e-02 && qext > 2.2574260667e-02);
        QVERIFY(qsca < 1.9361631879e-04 && qsca > 1.2134663158e-05);
        QVERIFY(qbsc < 2.8531209142e-04 && qbsc > 1.8121544093e-05);
        QVERIFY(qabs < 4.5605794325e-02 && qabs > 2.2562126004e-02);
        QVERIFY(gsca < 7.3063003419e-03 && gsca > 1.8284116889e-03);

        mie.calculate(0.105, &qsca, &qbsc, &qabs, &qext, &gsca);
        QVERIFY(qext < 4.5799410643e-02 && qext > 2.2574260667e-02);
        QVERIFY(qsca < 1.9361631879e-04 && qsca > 1.2134663158e-05);
        QVERIFY(qbsc < 2.8531209142e-04 && qbsc > 1.8121544093e-05);
        QVERIFY(qabs < 4.5605794325e-02 && qabs > 2.2562126004e-02);
        QVERIFY(gsca < 7.3063003419e-03 && gsca > 1.8284116889e-03);

        mie.scanDone();

        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            mie.serialize(stream);
        }
        QDataStream stream(&data, QIODevice::ReadOnly);
        MieTable mie2(stream);

        mie2.calculate(0.2, &qsca, &qbsc, &qabs, &qext, &gsca);
        qsca = floor(qsca * roundoffLimit) / roundoffLimit;
        qbsc = floor(qbsc * roundoffLimit) / roundoffLimit;
        qabs = floor(qabs * roundoffLimit) / roundoffLimit;
        qext = floor(qext * roundoffLimit) / roundoffLimit;
        gsca = floor(gsca * roundoffLimit) / roundoffLimit;
        QCOMPARE(qext, expectedQext);
        QCOMPARE(qsca, expectedQsca);
        QCOMPARE(qbsc, expectedQbsc);
        QCOMPARE(qabs, expectedQabs);
        QCOMPARE(gsca, expectedGsca);

        mie2.calculate(0.195, &qsca, &qbsc, &qabs, &qext, &gsca);
        QVERIFY(qext < 4.5799410643e-02 && qext > 2.2574260667e-02);
        QVERIFY(qsca < 1.9361631879e-04 && qsca > 1.2134663158e-05);
        QVERIFY(qbsc < 2.8531209142e-04 && qbsc > 1.8121544093e-05);
        QVERIFY(qabs < 4.5605794325e-02 && qabs > 2.2562126004e-02);
        QVERIFY(gsca < 7.3063003419e-03 && gsca > 1.8284116889e-03);
    }

    void tableDefer()
    {
        MieTable mieDirect(new Mie(1.33, 0.1, 10), true);
        QHash<double, TestMieResult> expectedFirst;
        QHash<double, TestMieResult> expectedSecond;
        for (double x = 0.1; x <= 0.25; x += 0.05) {
            TestMieResult add;
            mieDirect.calculate(x, &add.qsca, &add.qbsc, &add.qabs, &add.qext, &add.gsca);
            expectedFirst.insert(x, add);
        }
        mieDirect.scanDone();
        for (double x = 0.101; x <= 0.25; x += 0.05) {
            TestMieResult add;
            mieDirect.calculate(x, &add.qsca, &add.qbsc, &add.qabs, &add.qext, &add.gsca);
            expectedSecond.insert(x, add);
        }
        mieDirect.scanDone();

        QHash<double, TestMieResult *> results;
        MieTable mieDefer(new Mie(1.33, 0.1, 10), true);
        for (QHash<double, TestMieResult>::const_iterator it = expectedFirst.constBegin(),
                end = expectedFirst.constEnd(); it != end; ++it) {
            bool result = mieDefer.beginManualDeferCalculate(it.key());
            QVERIFY(result);

            TestMieResult *r = new TestMieResult;
            results.insert(it.key(), r);

            mieDefer.issueManualDeferCalculate(new TestMieTableResult(r));
            mieDefer.endManualDeferCalculate();
        }
        mieDefer.scanDone();

        for (QHash<double, TestMieResult *>::const_iterator it = results.constBegin(),
                end = results.constEnd(); it != end; ++it) {
            QHash<double, TestMieResult>::const_iterator check = expectedFirst.constFind(it.key());
            QVERIFY(check != expectedFirst.end());
            QCOMPARE(it.value()->qsca, check.value().qsca);
            QCOMPARE(it.value()->qbsc, check.value().qbsc);
            QCOMPARE(it.value()->qabs, check.value().qabs);
            QCOMPARE(it.value()->qext, check.value().qext);
            QCOMPARE(it.value()->gsca, check.value().gsca);
        }

        for (QHash<double, TestMieResult>::const_iterator it = expectedSecond.constBegin(),
                end = expectedSecond.constEnd(); it != end; ++it) {
            double qsca = 0;
            double qbsc = 0;
            double qabs = 0;
            double qext = 0;
            double gsca = 0;
            bool result =
                    mieDefer.beginManualDeferCalculate(it.key(), &qsca, &qbsc, &qabs, &qext, &gsca);
            QVERIFY(!result);
            mieDefer.endManualDeferCalculate();

            QCOMPARE(qsca, it.value().qsca);
            QCOMPARE(qbsc, it.value().qbsc);
            QCOMPARE(qabs, it.value().qabs);
            QCOMPARE(qext, it.value().qext);
            QCOMPARE(gsca, it.value().gsca);
        }

        for (QHash<double, TestMieResult *>::const_iterator it = results.constBegin(),
                end = results.constEnd(); it != end; ++it) {
            delete it.value();
        }
    }
};

QTEST_APPLESS_MAIN(TestMie)

#include "mie.moc"
