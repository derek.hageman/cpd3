/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "algorithms/leastsquares.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;

class TestLeastSquares : public QObject {
Q_OBJECT
private slots:

    void basicOrdinary()
    {
        QVector<double> x;
        QVector<double> y;

        x <<
                1.47 <<
                1.50 <<
                1.52 <<
                1.55 <<
                1.57 <<
                1.60 <<
                1.63 <<
                1.65 <<
                1.68 <<
                1.70 <<
                1.73 <<
                1.75 <<
                1.78 <<
                1.80 <<
                1.83;
        y <<
                52.21 <<
                53.12 <<
                54.48 <<
                55.84 <<
                57.20 <<
                58.57 <<
                59.93 <<
                61.29 <<
                63.11 <<
                64.47 <<
                66.28 <<
                68.10 <<
                69.92 <<
                72.19 <<
                74.46;

        BasicOrdinaryLeastSquares ls(x, y);

        QVERIFY(fabs(ls.slope() - 61.272) < 0.001);
        QVERIFY(fabs(ls.intercept() + 39.062) < 0.001);
        QVERIFY(FP::defined(ls.rSquared()));

        QVERIFY(FP::defined(ls.slopeConfidence(0.95)));
        QVERIFY(fabs(ls.slopeConfidence(0.95) - 3.836703) < 0.1);
        QVERIFY(FP::defined(ls.interceptConfidence(0.95)));
        QVERIFY(fabs(ls.interceptConfidence(0.95) - 6.347257) < 0.1);
    }

    void zeroOrdinary()
    {
        QVector<double> x;
        QVector<double> y;

        x << 60.0 << 61.0 << 62.0 << 63.0 << 64.0 << 65.0 << 66.0 << 67.0 << 68.0 << 69.0 << 70.0;
        y <<
                130.0 <<
                131.0 <<
                132.0 <<
                133.0 <<
                134.0 <<
                135.0 <<
                136.0 <<
                137.0 <<
                138.0 <<
                139.0 <<
                140.0;

        ZeroOrdinaryLeastSquares ls(x, y);

        QVERIFY(fabs(ls.slope() - 2.07438016528926) < 1E-6);
        QVERIFY(fabs(ls.rSquared() - 0.999365492298663) < 1E-6);

        QVERIFY(FP::defined(ls.slopeConfidence(0.8413447)));
    }
};

QTEST_APPLESS_MAIN(TestLeastSquares)

#include "leastsquares.moc"
