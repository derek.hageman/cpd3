/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "algorithms/rangewrap.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;

class TestRangeWrap : public QObject {
Q_OBJECT

private slots:

    void shared()
    {
        Transformer *t = new RangeWrap(0, 10);

        QVector<std::shared_ptr<TransformerPoint> > inputPoints;
        inputPoints.append(std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(0.0)));
        inputPoints.append(std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(1.0)));
        inputPoints.append(std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(2.0)));
        inputPoints.append(std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(10.0)));
        inputPoints.append(std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(9.0)));
        inputPoints.append(std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(1.0)));
        inputPoints.append(
                std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(FP::undefined())));
        inputPoints.append(std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(8.0)));

        QVector<std::shared_ptr<TransformerPoint> > result;
        result = inputPoints;
        t->apply(result, &TransformerPointStatic<1>::allocator);
        QCOMPARE(result.size(), 14);

        QCOMPARE(result.at(0)->get(0), 0.0);
        QCOMPARE(result.at(1)->get(0), 1.0);
        QCOMPARE(result.at(2)->get(0), 2.0);
        QCOMPARE(result.at(3)->get(0), 0.0);
        QVERIFY(!FP::defined(result.at(4)->get(0)));
        QCOMPARE(result.at(5)->get(0), 12.0);
        QCOMPARE(result.at(6)->get(0), 10.0);
        QCOMPARE(result.at(7)->get(0), 9.0);
        QCOMPARE(result.at(8)->get(0), 11.0);
        QVERIFY(!FP::defined(result.at(9)->get(0)));
        QCOMPARE(result.at(10)->get(0), -1.0);
        QCOMPARE(result.at(11)->get(0), 1.0);
        QVERIFY(!FP::defined(result.at(12)->get(0)));
        QCOMPARE(result.at(13)->get(0), 8.0);

        delete t;
    }

    void sharedSingle()
    {
        Transformer *t = new RangeWrap(0, 10, 0);

        QVector<std::shared_ptr<TransformerPoint> > inputPoints;
        inputPoints.append(std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(0.0)));
        inputPoints.append(std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(1.0)));
        inputPoints.append(std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(2.0)));
        inputPoints.append(std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(10.0)));
        inputPoints.append(std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(9.0)));
        inputPoints.append(std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(1.0)));
        inputPoints.append(
                std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(FP::undefined())));
        inputPoints.append(std::shared_ptr<TransformerPoint>(new TransformerPointStatic<1>(8.0)));

        QVector<std::shared_ptr<TransformerPoint> > result;
        result = inputPoints;
        t->apply(result, &TransformerPointStatic<1>::allocator);
        QCOMPARE(result.size(), 14);

        QCOMPARE(result.at(0)->get(0), 0.0);
        QCOMPARE(result.at(1)->get(0), 1.0);
        QCOMPARE(result.at(2)->get(0), 2.0);
        QCOMPARE(result.at(3)->get(0), 0.0);
        QVERIFY(!FP::defined(result.at(4)->get(0)));
        QCOMPARE(result.at(5)->get(0), 12.0);
        QCOMPARE(result.at(6)->get(0), 10.0);
        QCOMPARE(result.at(7)->get(0), 9.0);
        QCOMPARE(result.at(8)->get(0), 11.0);
        QVERIFY(!FP::defined(result.at(9)->get(0)));
        QCOMPARE(result.at(10)->get(0), -1.0);
        QCOMPARE(result.at(11)->get(0), 1.0);
        QVERIFY(!FP::defined(result.at(12)->get(0)));
        QCOMPARE(result.at(13)->get(0), 8.0);

        delete t;
    }

    void pointers()
    {
        Transformer *t = new RangeWrap(0, 10);

        QVector<TransformerPoint *> inputPoints;
        inputPoints.append(new TransformerPointStatic<1>(0.0));
        inputPoints.append(new TransformerPointStatic<1>(1.0));
        inputPoints.append(new TransformerPointStatic<1>(2.0));
        inputPoints.append(new TransformerPointStatic<1>(10.0));
        inputPoints.append(new TransformerPointStatic<1>(9.0));
        inputPoints.append(new TransformerPointStatic<1>(1.0));
        inputPoints.append(new TransformerPointStatic<1>(FP::undefined()));
        inputPoints.append(new TransformerPointStatic<1>(8.0));

        QVector<TransformerPoint *> result;
        result = inputPoints;
        t->apply(result, &TransformerPointStatic<1>::allocator);
        QCOMPARE(result.size(), 14);

        QCOMPARE(result.at(0)->get(0), 0.0);
        QCOMPARE(result.at(1)->get(0), 1.0);
        QCOMPARE(result.at(2)->get(0), 2.0);
        QCOMPARE(result.at(3)->get(0), 0.0);
        QVERIFY(!FP::defined(result.at(4)->get(0)));
        QCOMPARE(result.at(5)->get(0), 12.0);
        QCOMPARE(result.at(6)->get(0), 10.0);
        QCOMPARE(result.at(7)->get(0), 9.0);
        QCOMPARE(result.at(8)->get(0), 11.0);
        QVERIFY(!FP::defined(result.at(9)->get(0)));
        QCOMPARE(result.at(10)->get(0), -1.0);
        QCOMPARE(result.at(11)->get(0), 1.0);
        QVERIFY(!FP::defined(result.at(12)->get(0)));
        QCOMPARE(result.at(13)->get(0), 8.0);

        delete t;
        qDeleteAll(result);
    }

    void pointersSingle()
    {
        Transformer *t = new RangeWrap(0, 10, 0);

        QVector<TransformerPoint *> inputPoints;
        inputPoints.append(new TransformerPointStatic<1>(0.0));
        inputPoints.append(new TransformerPointStatic<1>(1.0));
        inputPoints.append(new TransformerPointStatic<1>(2.0));
        inputPoints.append(new TransformerPointStatic<1>(10.0));
        inputPoints.append(new TransformerPointStatic<1>(9.0));
        inputPoints.append(new TransformerPointStatic<1>(1.0));
        inputPoints.append(new TransformerPointStatic<1>(FP::undefined()));
        inputPoints.append(new TransformerPointStatic<1>(8.0));

        QVector<TransformerPoint *> result;
        result = inputPoints;
        t->apply(result, &TransformerPointStatic<1>::allocator);
        QCOMPARE(result.size(), 14);

        QCOMPARE(result.at(0)->get(0), 0.0);
        QCOMPARE(result.at(1)->get(0), 1.0);
        QCOMPARE(result.at(2)->get(0), 2.0);
        QCOMPARE(result.at(3)->get(0), 0.0);
        QVERIFY(!FP::defined(result.at(4)->get(0)));
        QCOMPARE(result.at(5)->get(0), 12.0);
        QCOMPARE(result.at(6)->get(0), 10.0);
        QCOMPARE(result.at(7)->get(0), 9.0);
        QCOMPARE(result.at(8)->get(0), 11.0);
        QVERIFY(!FP::defined(result.at(9)->get(0)));
        QCOMPARE(result.at(10)->get(0), -1.0);
        QCOMPARE(result.at(11)->get(0), 1.0);
        QVERIFY(!FP::defined(result.at(12)->get(0)));
        QCOMPARE(result.at(13)->get(0), 8.0);

        delete t;
        qDeleteAll(result);
    }
};

QTEST_APPLESS_MAIN(TestRangeWrap)

#include "rangewrap.moc"
