/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QObject>
#include <QTest>

#include "algorithms/logwrapper.hxx"
#include "algorithms/linearint.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;

class TestLogWrapper : public QObject {
Q_OBJECT
private slots:

    void logLog()
    {
        Model *m = LogWrapper::create(new SingleLinearInterpolator(1.0, 0.0), 3.0, 4.0);
        double expected1 = pow(4.0, log(1.0) / log(3.0));
        double expected2 = pow(4.0, log(2.0) / log(3.0));
        double expected3 = pow(4.0, log(3.0) / log(3.0));

        QVector<double> l;
        QVector<double> r;
        double a;
        double b;
        double c;
        l << 1.0 << 2.0;

        r = m->applyConst(l);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        r = m->apply(l);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        r = l;
        m->applyIOConst(r);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        r = l;
        m->applyIO(r);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        QCOMPARE(m->oneToOneConst(1.0), expected1);
        QCOMPARE(m->oneToOne(1.0), expected1);
        QCOMPARE(m->twoToOneConst(1.0, 2.0), expected1);
        QCOMPARE(m->twoToOne(1.0, 2.0), expected1);
        QCOMPARE(m->threeToOneConst(1.0, 2.0, 3.0), expected1);
        QCOMPARE(m->threeToOne(1.0, 2.0, 3.0), expected1);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIOConst(a, b);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIO(a, b);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIOConst(a, b, c);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        QCOMPARE(c, expected3);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIO(a, b, c);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        QCOMPARE(c, expected3);

        Model *n;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << m;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> n;
            }
        }
        delete m;
        QCOMPARE(n->oneToOne(1.0), expected1);
        m = n->clone();
        delete n;
        QCOMPARE(m->oneToOne(1.0), expected1);
        delete m;
    }

    void logIn()
    {
        Model *m = LogWrapper::create(new SingleLinearInterpolator(1.0, 0.0), 3.0, FP::undefined());
        double expected1 = log(1.0) / log(3.0);
        double expected2 = log(2.0) / log(3.0);
        double expected3 = log(3.0) / log(3.0);

        QVector<double> l;
        QVector<double> r;
        double a;
        double b;
        double c;
        l << 1.0 << 2.0;

        r = m->applyConst(l);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        r = m->apply(l);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        r = l;
        m->applyIOConst(r);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        r = l;
        m->applyIO(r);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        QCOMPARE(m->oneToOneConst(1.0), expected1);
        QCOMPARE(m->oneToOne(1.0), expected1);
        QCOMPARE(m->twoToOneConst(1.0, 2.0), expected1);
        QCOMPARE(m->twoToOne(1.0, 2.0), expected1);
        QCOMPARE(m->threeToOneConst(1.0, 2.0, 3.0), expected1);
        QCOMPARE(m->threeToOne(1.0, 2.0, 3.0), expected1);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIOConst(a, b);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIO(a, b);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIOConst(a, b, c);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        QCOMPARE(c, expected3);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIO(a, b, c);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        QCOMPARE(c, expected3);

        Model *n;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << m;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> n;
            }
        }
        delete m;
        QCOMPARE(n->oneToOne(1.0), expected1);
        m = n->clone();
        delete n;
        QCOMPARE(m->oneToOne(1.0), expected1);
        delete m;
    }

    void logOut()
    {
        Model *m = LogWrapper::create(new SingleLinearInterpolator(1.0, 0.0), FP::undefined(), 4.0);
        double expected1 = pow(4.0, 1.0);
        double expected2 = pow(4.0, 2.0);
        double expected3 = pow(4.0, 3.0);

        QVector<double> l;
        QVector<double> r;
        double a;
        double b;
        double c;
        l << 1.0 << 2.0;

        r = m->applyConst(l);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        r = m->apply(l);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        r = l;
        m->applyIOConst(r);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        r = l;
        m->applyIO(r);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        QCOMPARE(m->oneToOneConst(1.0), expected1);
        QCOMPARE(m->oneToOne(1.0), expected1);
        QCOMPARE(m->twoToOneConst(1.0, 2.0), expected1);
        QCOMPARE(m->twoToOne(1.0, 2.0), expected1);
        QCOMPARE(m->threeToOneConst(1.0, 2.0, 3.0), expected1);
        QCOMPARE(m->threeToOne(1.0, 2.0, 3.0), expected1);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIOConst(a, b);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIO(a, b);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIOConst(a, b, c);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        QCOMPARE(c, expected3);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIO(a, b, c);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        QCOMPARE(c, expected3);

        Model *n;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << m;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> n;
            }
        }
        delete m;
        QCOMPARE(n->oneToOne(1.0), expected1);
        m = n->clone();
        delete n;
        QCOMPARE(m->oneToOne(1.0), expected1);
        delete m;
    }

    void logLogMultiple()
    {
        Model *m = LogWrapperMultiple::create(new SingleLinearInterpolator(1.0, 0.0),
                                              QVector<double>() << 4.0 << 5.0 << 6.0,
                                              QVector<double>() << 3.0 << 4.0 << 5.0);
        double expected1 = pow(3.0, log(1.0) / log(4.0));
        double expected2 = pow(4.0, log(2.0) / log(5.0));
        double expected3 = pow(5.0, log(3.0) / log(6.0));

        QVector<double> l;
        QVector<double> r;
        double a;
        double b;
        double c;
        l << 1.0 << 2.0;

        r = m->applyConst(l);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        r = m->apply(l);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        r = l;
        m->applyIOConst(r);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        r = l;
        m->applyIO(r);
        QCOMPARE(r, QVector<double>() << expected1 << expected2);
        QCOMPARE(m->oneToOneConst(1.0), expected1);
        QCOMPARE(m->oneToOne(1.0), expected1);
        QCOMPARE(m->twoToOneConst(1.0, 2.0), expected1);
        QCOMPARE(m->twoToOne(1.0, 2.0), expected1);
        QCOMPARE(m->threeToOneConst(1.0, 2.0, 3.0), expected1);
        QCOMPARE(m->threeToOne(1.0, 2.0, 3.0), expected1);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIOConst(a, b);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIO(a, b);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIOConst(a, b, c);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        QCOMPARE(c, expected3);
        a = 1.0;
        b = 2.0;
        c = 3.0;
        m->applyIO(a, b, c);
        QCOMPARE(a, expected1);
        QCOMPARE(b, expected2);
        QCOMPARE(c, expected3);

        Model *n;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << m;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> n;
            }
        }
        delete m;
        QCOMPARE(n->oneToOne(1.0), expected1);
        m = n->clone();
        delete n;
        QCOMPARE(m->oneToOne(1.0), expected1);
        delete m;
    }

};

QTEST_APPLESS_MAIN(TestLogWrapper)

#include "logwrapper.moc"
