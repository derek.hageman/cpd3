/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QtAlgorithms>

#include "algorithms/lma.hxx"

using namespace CPD3::Algorithms;

static double expFunc(double x, const QVector<double> &p)
{
    return p[0] * exp(p[1] * x);
}

static QVector<double> expJac(double x, const QVector<double> &p)
{
    QVector<double> result(2);
    result[0] = exp(p[1] * x);
    result[1] = p[0] * x * exp(p[1] * x);
    return result;
}

class TestLevMar : public QObject {
Q_OBJECT
private slots:

    void general()
    {
        QVector<double> result;

        QVector<double> expPoints;
        expPoints <<
                0.10 <<
                0.15 <<
                0.20 <<
                0.25 <<
                0.30 <<
                0.35 <<
                0.40 <<
                0.45 <<
                0.50 <<
                0.55 <<
                0.60 <<
                0.65 <<
                0.70 <<
                0.75 <<
                0.80 <<
                0.85 <<
                0.90 <<
                0.95 <<
                1.00 <<
                1.05 <<
                1.10 <<
                1.15 <<
                1.20 <<
                1.25 <<
                1.30 <<
                1.35 <<
                1.40 <<
                1.45 <<
                1.50 <<
                1.55 <<
                1.60 <<
                1.65 <<
                1.70 <<
                1.75 <<
                1.80 <<
                1.85 <<
                1.90 <<
                1.95 <<
                2.00;

        QVector<double> expData;
        expData <<
                1.525387 <<
                2.044798 <<
                1.209845 <<
                1.136713 <<
                4.107137 <<
                2.202680 <<
                2.377605 <<
                2.781010 <<
                3.229551 <<
                1.883611 <<
                2.977371 <<
                2.964262 <<
                5.376170 <<
                5.354330 <<
                3.704304 <<
                4.849650 <<
                3.605111 <<
                3.516307 <<
                6.125783 <<
                3.932602 <<
                7.772172 <<
                7.408276 <<
                7.204501 <<
                7.808810 <<
                7.119072 <<
                9.668462 <<
                9.641200 <<
                10.967625 <<
                10.914843 <<
                12.241030 <<
                14.325656 <<
                14.628069 <<
                17.214741 <<
                18.145624 <<
                17.008485 <<
                19.489141 <<
                19.792586 <<
                24.606883 <<
                25.851197;

        result = LevenbergMarquardt::analytic(2, expFunc, expJac, expPoints, expData,
                                              QVector<double>() << 0.5 << 0.5);
        QCOMPARE(result.size(), 2);
        QVERIFY(fabs(result[0] - 1.25) < 1E-1);
        QVERIFY(fabs(result[1] - 1.5) < 1E-1);

        result = LevenbergMarquardt::numeric(2, expFunc, expPoints, expData,
                                             QVector<double>() << 0.5 << 0.5);
        QCOMPARE(result.size(), 2);
        QVERIFY(fabs(result[0] - 1.25) < 1E-1);
        QVERIFY(fabs(result[1] - 1.5) < 1E-1);
    }
};

QTEST_APPLESS_MAIN(TestLevMar)

#include "levmar.moc"
