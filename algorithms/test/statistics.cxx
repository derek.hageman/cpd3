/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QtAlgorithms>

#include "algorithms/statistics.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;

Q_DECLARE_METATYPE(QVector<double>);

class TestStatistics : public QObject {
Q_OBJECT
private slots:

    void meanSD()
    {
        QFETCH(QVector<double>, values);
        QFETCH(double, expectedMean);
        QFETCH(double, expectedSD);

        double mean = -1;
        double sd = -1;
        Statistics::meanSD(values, mean, sd);
        if (!FP::defined(expectedMean)) {
            QVERIFY(!FP::defined(mean));
        } else {
            QVERIFY(FP::defined(mean));
            QCOMPARE(mean, expectedMean);
        }

        if (!FP::defined(expectedSD)) {
            QVERIFY(!FP::defined(sd));
        } else {
            QVERIFY(FP::defined(sd));
            QCOMPARE(sd, expectedSD);
        }

        if (FP::defined(expectedMean)) {
            mean = -1;
            sd = -1;
            Statistics::meanSDDefined(values, mean, sd);

            QCOMPARE(mean, expectedMean);
            if (!FP::defined(expectedSD)) {
                QVERIFY(!FP::defined(sd));
            } else {
                QVERIFY(FP::defined(sd));
                QCOMPARE(sd, expectedSD);
            }

            QCOMPARE(Statistics::meanDefined(values), mean);
        }
    }

    void meanSD_data()
    {
        QTest::addColumn<QVector<double> >("values");
        QTest::addColumn<double>("expectedMean");
        QTest::addColumn<double>("expectedSD");

        QTest::newRow("Empty") << (QVector<double>()) << FP::undefined() << FP::undefined();
        QTest::newRow("Single") << (QVector<double>() << 1.0) << 1.0 << FP::undefined();
        QTest::newRow("Same") << (QVector<double>() << 1.0 << 1.0) << 1.0 << 0.0;
        QTest::newRow("Simple") << (QVector<double>() << 1.0 << 3.0 << 5.0) << 3.0 << 2.0;
    }

    void sortRemoveUndefined()
    {
        QFETCH(QVector<double>, values);
        QFETCH(QVector<double>, expected);

        Statistics::sortRemoveUndefined(values);
        QCOMPARE(values, expected);
    }

    void sortRemoveUndefined_data()
    {
        QTest::addColumn<QVector<double> >("values");
        QTest::addColumn<QVector<double> >("expected");

        QTest::newRow("Empty") << (QVector<double>()) << (QVector<double>());
        QTest::newRow("Single Defined") << (QVector<double>() << 1.0) << (QVector<double>() << 1.0);
        QTest::newRow("Single Undefined") <<
                (QVector<double>() << FP::undefined()) <<
                (QVector<double>());
        QTest::newRow("Two Undefined") <<
                (QVector<double>() << FP::undefined() << FP::undefined()) <<
                (QVector<double>());
        QTest::newRow("Three Undefined") <<
                (QVector<double>() << FP::undefined() << FP::undefined() << FP::undefined()) <<
                (QVector<double>());
        QTest::newRow("Two Defined") <<
                (QVector<double>() << 1.0 << 2.0) <<
                (QVector<double>() << 1.0 << 2.0);
        QTest::newRow("Two Defined Reversed") <<
                (QVector<double>() << 2.0 << 1.0) <<
                (QVector<double>() << 1.0 << 2.0);
        QTest::newRow("Two Defined Equal") <<
                (QVector<double>() << 2.0 << 2.0) <<
                (QVector<double>() << 2.0 << 2.0);
        QTest::newRow("Three Defined") <<
                (QVector<double>() << 1.0 << 2.0 << 1.5) <<
                (QVector<double>() << 1.0 << 1.5 << 2.0);
        QTest::newRow("Three One Undefined") <<
                (QVector<double>() << 1.0 << FP::undefined() << 1.5) <<
                (QVector<double>() << 1.0 << 1.5);
        QTest::newRow("Three Two Undefined Start") <<
                (QVector<double>() << 1.0 << FP::undefined() << FP::undefined()) <<
                (QVector<double>() << 1.0);
        QTest::newRow("Three Two Undefined Middle") <<
                (QVector<double>() << FP::undefined() << 1.0 << FP::undefined()) <<
                (QVector<double>() << 1.0);
        QTest::newRow("Three Two Undefined Middle End") <<
                (QVector<double>() << FP::undefined() << FP::undefined() << 1.0) <<
                (QVector<double>() << 1.0);
    }

    void quantile()
    {
        QFETCH(QVector<double>, values);
        QFETCH(double, quantile);
        QFETCH(double, expected);

        std::sort(values.begin(), values.end());
        double result = Statistics::quantile(values, quantile);
        if (!FP::defined(expected)) {
            QVERIFY(!FP::defined(result));
        } else {
            QVERIFY(FP::defined(result));
            QCOMPARE(result, expected);
        }
    }

    void quantile_data()
    {
        QTest::addColumn<QVector<double> >("values");
        QTest::addColumn<double>("quantile");
        QTest::addColumn<double>("expected");

        QTest::newRow("Empty") << (QVector<double>()) << 0.5 << FP::undefined();
        QTest::newRow("Single") << (QVector<double>() << 1.0) << 0.5 << 1.0;

        QTest::newRow("Median two") << (QVector<double>() << 1.0 << 2.0) << 0.5 << 1.5;
        QTest::newRow("Median three") << (QVector<double>() << 1.0 << 2.0 << 3.0) << 0.5 << 2.0;
        QTest::newRow("Weighted lower two") << (QVector<double>() << 1.0 << 2.0) << 0.25 << 1.25;
        QTest::newRow("Weighted upper two") << (QVector<double>() << 1.0 << 2.0) << 0.75 << 1.75;
        QTest::newRow("Weighted lower three middle") <<
                (QVector<double>() << 1.0 << 2.0 << 3.0) <<
                0.25 <<
                1.5;
        QTest::newRow("Weighted upper three middle") <<
                (QVector<double>() << 1.0 << 2.0 << 3.0) <<
                0.75 <<
                2.5;
        QTest::newRow("Weighted lower three") <<
                (QVector<double>() << 1.0 << 2.0 << 3.0) <<
                0.1 <<
                1.2;
        QTest::newRow("Weighted upper three") <<
                (QVector<double>() << 1.0 << 2.0 << 3.0) <<
                0.9 <<
                2.8;
    }

    void normalQuantile()
    {
        QFETCH(double, quantile);
        QFETCH(double, expected);

        double result = Statistics::normalQuantile(quantile);
        QVERIFY(fabs(result - expected) < 0.00001);
    }

    void normalQuantile_data()
    {
        QTest::addColumn<double>("quantile");
        QTest::addColumn<double>("expected");

        QTest::newRow("0.5") << 0.5 << 0.0;
        QTest::newRow("0.75") << 0.75 << 0.6744898;
        QTest::newRow("0.25") << 0.25 << -0.6744898;
        QTest::newRow("0.85") << 0.85 << 1.036433;
        QTest::newRow("0.15") << 0.15 << -1.036433;
        QTest::newRow("0.9") << 0.9 << 1.281552;
        QTest::newRow("0.1") << 0.1 << -1.281552;
        QTest::newRow("0.999") << 0.999 << 3.090232;
        QTest::newRow("0.001") << 0.001 << -3.090232;
    }

    void normalCDF()
    {
        QFETCH(double, position);
        QFETCH(double, expected);

        double result = Statistics::normalCDF(position);
        QVERIFY(result >= 0.0);
        QVERIFY(result <= 1.0);
        QVERIFY(fabs(result - expected) < 0.00001);
    }

    void normalCDF_data()
    {
        QTest::addColumn<double>("position");
        QTest::addColumn<double>("expected");

        QTest::newRow("0.0") << 0.0 << 0.5;
        QTest::newRow("-1.0") << -1.0 << 0.1586553;
        QTest::newRow("1.0") << 1.0 << 0.8413447;
        QTest::newRow("-1.5") << -1.5 << 0.0668072;
        QTest::newRow("1.5") << 1.5 << 0.9331928;
    }

    void studentQuantile()
    {
        QFETCH(double, df);
        QFETCH(double, quantile);
        QFETCH(double, expected);

        double result = Statistics::studentQuantile(df, quantile);
        QVERIFY(fabs(result - expected) < 0.001);
    }

    void studentQuantile_data()
    {
        QTest::addColumn<double>("df");
        QTest::addColumn<double>("quantile");
        QTest::addColumn<double>("expected");

        QTest::newRow("1 0.75") << 1.0 << 0.75 << 1.000;
        QTest::newRow("1 0.9") << 1.0 << 0.9 << 3.078;
        QTest::newRow("2 0.75") << 2.0 << 0.75 << 0.816;
        QTest::newRow("2 0.9") << 2.0 << 0.9 << 1.886;
        QTest::newRow("4 0.75") << 4.0 << 0.75 << 0.741;
        QTest::newRow("4 0.9") << 4.0 << 0.9 << 1.533;
        QTest::newRow("5 0.75") << 5.0 << 0.75 << 0.727;
        QTest::newRow("5 0.9") << 5.0 << 0.9 << 1.476;
        QTest::newRow("13 0.975") << 13.0 << 0.975 << 2.1604;
        QTest::newRow("50 0.75") << 50.0 << 0.75 << 0.679;
        QTest::newRow("50 0.9") << 50.0 << 0.9 << 1.299;
        QTest::newRow("50 0.9995") << 50.0 << 0.9995 << 3.496;
    }

    void chi2CDF()
    {
        QFETCH(double, df);
        QFETCH(double, gamma);
        QFETCH(double, position);
        QFETCH(double, expected);

        double result = Statistics::chi2CDF(df, gamma, position);
        QVERIFY(fabs(result - expected) < 0.001);
    }

    void chi2CDF_data()
    {
        QTest::addColumn<double>("df");
        QTest::addColumn<double>("gamma");
        QTest::addColumn<double>("position");
        QTest::addColumn<double>("expected");

        QTest::newRow("20 0.0 30.0") << 20.0 << 0.0 << 30.0 << 0.9301463;
        QTest::newRow("5 0.0 10.0") << 5.0 << 0.0 << 10.0 << 0.9247648;
    }

    void chi2Quantile()
    {
        QFETCH(double, df);
        QFETCH(double, gamma);
        QFETCH(double, quantile);
        QFETCH(double, expected);

        double result = Statistics::chi2Quantile(df, gamma, quantile);
        QVERIFY(fabs(result - expected) < 0.01);
    }

    void chi2Quantile_data()
    {
        QTest::addColumn<double>("df");
        QTest::addColumn<double>("gamma");
        QTest::addColumn<double>("quantile");
        QTest::addColumn<double>("expected");

        QTest::newRow("13 0.0 0.975") << 13.0 << 0.0 << 0.975 << 24.73560;
        QTest::newRow("13 0.0 0.125") << 13.0 << 0.0 << 0.125 << 7.492872;
        QTest::newRow("20 0.0 0.125") << 20.0 << 0.0 << 0.125 << 13.05527;
        QTest::newRow("20 0.0 0.5") << 20.0 << 0.0 << 0.5 << 19.33743;
        QTest::newRow("100 0.0 0.2") << 100.0 << 0.0 << 0.2 << 87.94534;
        QTest::newRow("100 0.0 0.7") << 100.0 << 0.0 << 0.7 << 106.9058;
    }
};

QTEST_APPLESS_MAIN(TestStatistics)

#include "statistics.moc"
