/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "algorithms/crc.hxx"

using namespace CPD3::Algorithms;


class TestCRC : public QObject {
Q_OBJECT
private slots:

    void crc()
    {
        QFETCH(QByteArray, data);
        QFETCH(quint16, expected);

        CRC16 crc;
        QCOMPARE(crc.calculate(data), expected);
        QCOMPARE(crc.calculate(data.constData(), data.size()), expected);

        crc.reset();
        QCOMPARE(crc.add(data), expected);
        crc.reset();
        QCOMPARE(crc.add(data.constData(), data.size()), expected);
        crc.reset();
        quint16 result = 0;
        for (int i = 0; i < data.size(); i++) {
            result = crc.add(data.at(i));
        }
        QCOMPARE(result, expected);

        if (data.size() > 2) {
            crc.reset();
            crc.add(data.mid(0, data.size() / 2));
            QCOMPARE(crc.add(data.mid(data.size() / 2)), expected);
        }
    }

    void crc_data()
    {
        QTest::addColumn<QByteArray>("data");
        QTest::addColumn<quint16>("expected");

        QTest::newRow("a") << QByteArray("a") << (quint16) 0xE8C1;
        QTest::newRow("ab") << QByteArray("ab") << (quint16) 0x79A8;
        QTest::newRow("abc") << QByteArray("abc") << (quint16) 0x9738;
        QTest::newRow("abcd") << QByteArray("abcd") << (quint16) 0x3997;
        QTest::newRow("345gfldf84jf") << QByteArray("345gfldf84jf") << (quint16) 0xA5F8;

        QTest::newRow("uMAC packet 1") <<
                QByteArray("\x05\x01\x07\x00\x00\x02\x00\x00\x80", 9) <<
                (quint16) 0x4488;
        QTest::newRow("uMAC packet 2") << QByteArray("\x05\x01\x0E\x00\x01\x00\x33\x23"
                                                             "\x32\x23\x33\x23\x33\x23\x33\x23",
                                                     16) << (quint16) 0x3376;
    }
};

QTEST_APPLESS_MAIN(TestCRC)

#include "crc.moc"
