/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QtAlgorithms>

#include "algorithms/numericsolve.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;

struct TestNewtonRaphson : public NewtonRaphson<TestNewtonRaphson> {
    double evaluate(double x) const
    {
        return x * 0.5 + 1.0;
    }
};

class TestNumericSolve : public QObject {
Q_OBJECT
private slots:

    void newtonRaphson()
    {
        QCOMPARE(TestNewtonRaphson().solve(3.5), 5.0);
    }
};

QTEST_APPLESS_MAIN(TestNumericSolve)

#include "numericsolve.moc"
