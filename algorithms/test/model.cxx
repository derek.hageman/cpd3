/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "algorithms/model.hxx"
#include "core/number.hxx"
#include "datacore/variant/root.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;
using namespace CPD3::Data;

class TestModel : public QObject {
Q_OBJECT
private slots:

    void parameters()
    {
        ModelParameters p;

        p.setParameter("p1", QVariant(QString("v1")));
        QCOMPARE(p.getParameter("p1").toString(), QString("v1"));

        p.setDouble("p2", 2.0);
        QCOMPARE(p.getDouble("p2"), 2.0);

        QCOMPARE(p.totalDimensions(), 0);
        QVERIFY(p.getDimension(0).isEmpty());
        QVector<double> d;
        d << 1.0 << 2.0 << 3.0;
        p.setDimension(0, d);
        QCOMPARE(p.totalDimensions(), 1);
        QCOMPARE(p.getDimension(0), d);
        d << 4.0;
        p.setDimension(1, d);
        QCOMPARE(p.totalDimensions(), 2);
        QCOMPARE(p.getDimension(1), d);
        d.pop_back();
        QCOMPARE(p.getDimension(0), d);
        d << 5.0;
        QCOMPARE(p.totalDimensions(), 2);
        p.appendDimension(0, 5.0);
        QCOMPARE(p.getDimension(0), d);
        d.pop_back();
        d << 4.0;
        QCOMPARE(p.getDimension(1), d);

        p.clearDimensions();
        QCOMPARE(p.totalDimensions(), 0);
    }

    void constraints()
    {
        ModelInputConstraints ic;
        QVERIFY(!FP::defined(ic.getMin()));
        QVERIFY(!FP::defined(ic.getMax()));
        ic = ModelInputConstraints(1.0, 2.0);
        QCOMPARE(ic.getMin(), 1.0);
        QCOMPARE(ic.getMax(), 2.0);

        ModelOutputConstraints oc;
        QVERIFY(oc.isNoop());
        QVERIFY(!FP::defined(oc.getMin()));
        QVERIFY(!FP::defined(oc.getMax()));
        QVERIFY(!oc.isWrapping());
        QCOMPARE(oc.apply(5.0), 5.0);

        oc = ModelOutputConstraints(1.0, 2.0);
        QVERIFY(!oc.isNoop());
        QCOMPARE(oc.getMin(), 1.0);
        QCOMPARE(oc.getMax(), 2.0);
        QVERIFY(!oc.isWrapping());
        QCOMPARE(oc.apply(0.5), 1.0);
        QCOMPARE(oc.apply(1.0), 1.0);
        QCOMPARE(oc.apply(1.5), 1.5);
        QCOMPARE(oc.apply(2.0), 2.0);
        QCOMPARE(oc.apply(2.5), 2.0);
        QVERIFY(!FP::defined(oc.apply(FP::undefined())));

        oc = ModelOutputConstraints(2.0, 5.0, true);
        QVERIFY(!oc.isNoop());
        QCOMPARE(oc.getMin(), 2.0);
        QCOMPARE(oc.getMax(), 5.0);
        QVERIFY(oc.isWrapping());
        QCOMPARE(oc.apply(0.0), 3.0);
        QCOMPARE(oc.apply(0.5), 3.5);
        QCOMPARE(oc.apply(1.0), 4.0);
        QCOMPARE(oc.apply(1.25), 4.25);
        QCOMPARE(oc.apply(1.75), 4.75);
        QCOMPARE(oc.apply(1.75), 4.75);
        QCOMPARE(oc.apply(2.0), 2.0);
        QCOMPARE(oc.apply(3.0), 3.0);
        QCOMPARE(oc.apply(4.75), 4.75);
        QCOMPARE(oc.apply(5.0), 2.0);
        QCOMPARE(oc.apply(5.25), 2.25);
        QCOMPARE(oc.apply(5.5), 2.5);
        QCOMPARE(oc.apply(6.0), 3.0);
        QCOMPARE(oc.apply(20.0), 2.0);
        QCOMPARE(oc.apply(20.25), 2.25);
        QCOMPARE(oc.apply(20.5), 2.5);
        QCOMPARE(oc.apply(-19.0), 2.0);
        QCOMPARE(oc.apply(-19.25), 4.75);
        QCOMPARE(oc.apply(-19.5), 4.5);
        QCOMPARE(oc.apply(-19.75), 4.25);
        QVERIFY(!FP::defined(oc.apply(FP::undefined())));
    }

    void constantModel()
    {
        Model *m = new ModelConstant(12.0);
        QCOMPARE(m->oneToOne(1.0), 12.0);

        QVector<double> l;
        QVector<double> r;
        double a;
        double b;
        double c;
        l << 1.0 << 2.0;

        r = m->applyConst(l);
        QCOMPARE(r, QVector<double>() << 12.0 << 12.0);
        r = m->apply(l);
        QCOMPARE(r, QVector<double>() << 12.0 << 12.0);
        r = l;
        m->applyIOConst(r);
        QCOMPARE(r, QVector<double>() << 12.0 << 12.0);
        r = l;
        m->applyIO(r);
        QCOMPARE(r, QVector<double>() << 12.0 << 12.0);
        QCOMPARE(m->oneToOneConst(1.0), 12.0);
        QCOMPARE(m->oneToOne(1.0), 12.0);
        QCOMPARE(m->twoToOneConst(1.0, 2.0), 12.0);
        QCOMPARE(m->twoToOne(1.0, 2.0), 12.0);
        QCOMPARE(m->threeToOneConst(1.0, 2.0, 3.0), 12.0);
        QCOMPARE(m->threeToOne(1.0, 2.0, 3.0), 12.0);
        a = b = c = 1.0;
        m->applyIOConst(a, b);
        QCOMPARE(a, 12.0);
        QCOMPARE(b, 12.0);
        a = b = c = 1.0;
        m->applyIO(a, b);
        QCOMPARE(a, 12.0);
        QCOMPARE(b, 12.0);
        a = b = c = 1.0;
        m->applyIOConst(a, b, c);
        QCOMPARE(a, 12.0);
        QCOMPARE(b, 12.0);
        QCOMPARE(c, 12.0);
        a = b = c = 1.0;
        m->applyIO(a, b, c);
        QCOMPARE(a, 12.0);
        QCOMPARE(b, 12.0);
        QCOMPARE(c, 12.0);

        Model *n;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << m;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> n;
            }
        }
        delete m;
        QCOMPARE(n->oneToOne(1.0), 12.0);
        m = n->clone();
        delete n;
        QCOMPARE(m->oneToOne(1.0), 12.0);
        delete m;
    }

    void compositeModel()
    {
        Model *m = new ModelSingleComposite(QVector<Model *>() <<
                                                    (new ModelConstant(11.0)) <<
                                                    (new ModelConstant(12.0)) <<
                                                    (new ModelConstant(13.0)));

        QVector<double> l;
        QVector<double> r;
        double a;
        double b;
        double c;
        l << 1.0 << 2.0 << 3.0;

        r = m->applyConst(l);
        QCOMPARE(r, QVector<double>() << 11.0 << 12.0 << 13.0);
        r = m->apply(l);
        QCOMPARE(r, QVector<double>() << 11.0 << 12.0 << 13.0);
        r = l;
        m->applyIOConst(r);
        QCOMPARE(r, QVector<double>() << 11.0 << 12.0 << 13.0);
        r = l;
        m->applyIO(r);
        QCOMPARE(r, QVector<double>() << 11.0 << 12.0 << 13.0);
        QCOMPARE(m->oneToOneConst(1.0), 11.0);
        QCOMPARE(m->oneToOne(1.0), 11.0);
        QCOMPARE(m->twoToOneConst(1.0, 2.0), 11.0);
        QCOMPARE(m->twoToOne(1.0, 2.0), 11.0);
        QCOMPARE(m->threeToOneConst(1.0, 2.0, 3.0), 11.0);
        QCOMPARE(m->threeToOne(1.0, 2.0, 3.0), 11.0);
        a = b = c = 1.0;
        m->applyIOConst(a, b);
        QCOMPARE(a, 11.0);
        QCOMPARE(b, 12.0);
        a = b = c = 1.0;
        m->applyIO(a, b);
        QCOMPARE(a, 11.0);
        QCOMPARE(b, 12.0);
        a = b = c = 1.0;
        m->applyIOConst(a, b, c);
        QCOMPARE(a, 11.0);
        QCOMPARE(b, 12.0);
        QCOMPARE(c, 13.0);
        a = b = c = 1.0;
        m->applyIO(a, b, c);
        QCOMPARE(a, 11.0);
        QCOMPARE(b, 12.0);
        QCOMPARE(c, 13.0);

        Model *n;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << m;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> n;
            }
        }
        delete m;
        r = n->applyConst(l);
        QCOMPARE(r, QVector<double>() << 11.0 << 12.0 << 13.0);
        m = n->clone();
        delete n;
        r = m->applyConst(l);
        QCOMPARE(r, QVector<double>() << 11.0 << 12.0 << 13.0);
        delete m;
    }

    void differedModelCreation()
    {
        Variant::Write config = Variant::Write::empty();
        config["Type"].setString("Constant");
        config["Value"].setDouble(12.0);
        DeferredModelWrapper wrapper(config);
        QVERIFY(wrapper.waitForModelReady());
        QVERIFY(wrapper.isModelReady());
        QVERIFY(wrapper.getModel() != NULL);
        Model *m = wrapper.takeModel();
        QCOMPARE(m->oneToOne(1.0), 12.0);
        delete m;
        QVERIFY(wrapper.getModel() == NULL);
        QVERIFY(wrapper.takeModel() == NULL);
    }
};

QTEST_APPLESS_MAIN(TestModel)

#include "model.moc"
