/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>

#include "algorithms/rangefilter.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;

Q_DECLARE_METATYPE(QVector<double>);

class TestRangeFilter : public QObject {
Q_OBJECT

    static void constructPoints(const QVector<double> &input,
                                QVector<std::shared_ptr<TransformerPoint> > &result)
    {
        result.clear();
        for (QVector<double>::const_iterator add = input.constBegin(), end = input.constEnd();
                add != end;
                ++add) {
            result.append(
                    std::shared_ptr<TransformerPoint>(new TransformerPointStatic<2>(*add, *add)));
        }
    }

    static void constructPoints(const QVector<double> &input, QVector<TransformerPoint *> &result)
    {
        qDeleteAll(result);
        result.clear();
        for (QVector<double>::const_iterator add = input.constBegin(), end = input.constEnd();
                add != end;
                ++add) {
            result.append(new TransformerPointStatic<2>(*add, *add));
        }
    }

    static bool comparePoints(const QVector<std::shared_ptr<TransformerPoint> > &result,
                              const QVector<double> &expected)
    {
        if (result.size() != expected.size())
            return false;
        for (int i = 0, max = result.size(); i < max; i++) {
            if (!FP::equal(result.at(i)->get(0), expected.at(i)))
                return false;
        }
        return true;
    }

    static bool comparePoints(const QVector<TransformerPoint *> &result,
                              const QVector<double> &expected)
    {
        if (result.size() != expected.size())
            return false;
        for (int i = 0, max = result.size(); i < max; i++) {
            if (!FP::equal(result.at(i)->get(0), expected.at(i)))
                return false;
        }
        return true;
    }

private slots:

    void filter()
    {
        QFETCH(QVector<double>, values);
        QFETCH(double, min);
        QFETCH(double, max);
        QFETCH(int, flags);

        QVector<double> passed;
        for (QVector<double>::const_iterator v = values.constBegin(), endV = values.constEnd();
                v != endV;
                ++v) {
            double add = *v;
            if (!FP::defined(add)) {
                if (!(flags & RangeFilter::Reject_Remove))
                    passed.append(FP::undefined());
                continue;
            }
            if (flags & RangeFilter::Reject_Range) {
                if (FP::defined(min) && FP::defined(max)) {
                    if ((add > min || (add == min && !(flags & RangeFilter::Min_Inclusive))) &&
                            (add < max || (add == max && !(flags & RangeFilter::Max_Inclusive)))) {
                        if (!(flags & RangeFilter::Reject_Remove))
                            passed.append(FP::undefined());
                        continue;
                    }
                } else if (FP::defined(min) &&
                        (add > min || (add == min && !(flags & RangeFilter::Min_Inclusive)))) {
                    if (!(flags & RangeFilter::Reject_Remove))
                        passed.append(FP::undefined());
                    continue;
                } else if (FP::defined(max) &&
                        (add < max || (add == max && !(flags & RangeFilter::Max_Inclusive)))) {
                    if (!(flags & RangeFilter::Reject_Remove))
                        passed.append(FP::undefined());
                    continue;
                }
            } else {
                if (FP::defined(min) &&
                        (add < min || (add == min && !(flags & RangeFilter::Min_Inclusive)))) {
                    if (!(flags & RangeFilter::Reject_Remove))
                        passed.append(FP::undefined());
                    continue;
                }
                if (FP::defined(max) &&
                        (add > max || (add == max && !(flags & RangeFilter::Max_Inclusive)))) {
                    if (!(flags & RangeFilter::Reject_Remove))
                        passed.append(FP::undefined());
                    continue;
                }
            }
            passed.append(add);
        }

        Transformer *t1 = new RangeFilter(min, max, flags);
        Transformer *t2 = new RangeFilter(min, max, flags, 0);
        QVector<std::shared_ptr<TransformerPoint> > p1;
        QVector<TransformerPoint *> p2;

        constructPoints(values, p1);
        constructPoints(values, p2);
        t1->apply(p1);
        t1->applyConst(p2);
        QVERIFY(comparePoints(p1, passed));
        QVERIFY(comparePoints(p2, passed));
        constructPoints(values, p1);
        constructPoints(values, p2);
        t2->apply(p1);
        t2->applyConst(p2);
        QVERIFY(comparePoints(p1, passed));
        QVERIFY(comparePoints(p2, passed));


        Transformer *u1;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << t1;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> u1;
            }
        }
        delete t1;
        Transformer *u2;
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << t2;
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                stream >> u2;
            }
        }
        delete t2;

        constructPoints(values, p1);
        constructPoints(values, p2);
        u1->apply(p1);
        u1->applyConst(p2);
        QVERIFY(comparePoints(p1, passed));
        QVERIFY(comparePoints(p2, passed));
        constructPoints(values, p1);
        constructPoints(values, p2);
        u2->apply(p1);
        u2->applyConst(p2);
        QVERIFY(comparePoints(p1, passed));
        QVERIFY(comparePoints(p2, passed));


        t1 = u1->clone();
        delete u1;
        t2 = u2->clone();
        delete u2;

        constructPoints(values, p1);
        constructPoints(values, p2);
        t1->applyConst(p1);
        t1->apply(p2);
        QVERIFY(comparePoints(p1, passed));
        QVERIFY(comparePoints(p2, passed));
        constructPoints(values, p1);
        constructPoints(values, p2);
        t2->applyConst(p1);
        t2->apply(p2);
        QVERIFY(comparePoints(p1, passed));
        QVERIFY(comparePoints(p2, passed));

        delete t1;
        delete t2;

        qDeleteAll(p2);
    }

    void filter_data()
    {
        QTest::addColumn<QVector<double> >("values");
        QTest::addColumn<double>("min");
        QTest::addColumn<double>("max");
        QTest::addColumn<int>("flags");

        QTest::newRow("Empty") << (QVector<double>()) << FP::undefined() << FP::undefined() << 0;

        QTest::newRow("Min accept") <<
                (QVector<double>() << 0.5 << 1.0 << 2.0) <<
                1.0 <<
                FP::undefined() <<
                0;
        QTest::newRow("Min reject") <<
                (QVector<double>() << 0.5 << 1.0 << 2.0) <<
                1.0 <<
                FP::undefined() <<
                (int) RangeFilter::Reject_Range;
        QTest::newRow("Min inclusive accept") <<
                (QVector<double>() << 0.5 << 1.0 << 2.0) <<
                1.0 <<
                FP::undefined() <<
                (int) RangeFilter::Min_Inclusive;
        QTest::newRow("Min inclusive reject") <<
                (QVector<double>() << 0.5 << 1.0 << 2.0) <<
                1.0 <<
                FP::undefined() <<
                (int) (RangeFilter::Reject_Range | RangeFilter::Min_Inclusive);


        QTest::newRow("Min accept remove") <<
                (QVector<double>() << 0.5 << 1.0 << 2.0) <<
                1.0 <<
                FP::undefined() <<
                (int) RangeFilter::Reject_Remove;
        QTest::newRow("Min reject remove") <<
                (QVector<double>() << 0.5 << 1.0 << 2.0) <<
                1.0 <<
                FP::undefined() <<
                (int) (RangeFilter::Reject_Range | RangeFilter::Reject_Remove);
        QTest::newRow("Min inclusive accept remove") <<
                (QVector<double>() << 0.5 << 1.0 << 2.0) <<
                1.0 <<
                FP::undefined() <<
                (int) (RangeFilter::Min_Inclusive | RangeFilter::Reject_Remove);
        QTest::newRow("Min inclusive reject remove") <<
                (QVector<double>() << 0.5 << 1.0 << 2.0) <<
                1.0 <<
                FP::undefined() <<
                (int) (RangeFilter::Reject_Range |
                        RangeFilter::Min_Inclusive |
                        RangeFilter::Reject_Remove);


        QTest::newRow("Max accept") <<
                (QVector<double>() << 0.5 << 1.0 << 2.0) <<
                FP::undefined() <<
                1.0 <<
                0;
        QTest::newRow("Max reject") <<
                (QVector<double>() << 0.5 << 1.0 << 2.0) <<
                FP::undefined() <<
                1.0 <<
                (int) RangeFilter::Reject_Range;
        QTest::newRow("Max inclusive accept") <<
                (QVector<double>() << 0.5 << 1.0 << 2.0) <<
                FP::undefined() <<
                1.0 <<
                (int) RangeFilter::Max_Inclusive;
        QTest::newRow("Max inclusive reject") <<
                (QVector<double>() << 0.5 << 1.0 << 2.0) <<
                FP::undefined() <<
                1.0 <<
                (int) (RangeFilter::Reject_Range | RangeFilter::Max_Inclusive);


        QTest::newRow("Min max accept") <<
                (QVector<double>() << 0.25 << 0.5 << 1.0 << 2.0 << 4.0) <<
                0.5 <<
                2.0 <<
                0;
        QTest::newRow("Min max reject") <<
                (QVector<double>() << 0.25 << 0.5 << 1.0 << 2.0 << 4.0) <<
                0.5 <<
                2.0 <<
                (int) RangeFilter::Reject_Range;
        QTest::newRow("Min inclusive max accept") <<
                (QVector<double>() << 0.25 << 0.5 << 1.0 << 2.0 << 4.0) <<
                0.5 <<
                2.0 <<
                (int) RangeFilter::Min_Inclusive;
        QTest::newRow("Min inclusive max reject") <<
                (QVector<double>() << 0.25 << 0.5 << 1.0 << 2.0 << 4.0) <<
                0.5 <<
                2.0 <<
                (int) (RangeFilter::Reject_Range | RangeFilter::Min_Inclusive);
        QTest::newRow("Min max inclusive accept") <<
                (QVector<double>() << 0.25 << 0.5 << 1.0 << 2.0 << 4.0) <<
                0.5 <<
                2.0 <<
                (int) RangeFilter::Max_Inclusive;
        QTest::newRow("Min max inclusive reject") <<
                (QVector<double>() << 0.25 << 0.5 << 1.0 << 2.0 << 4.0) <<
                0.5 <<
                2.0 <<
                (int) (RangeFilter::Reject_Range | RangeFilter::Max_Inclusive);
        QTest::newRow("Min inclusive max inclusive accept") <<
                (QVector<double>() << 0.25 << 0.5 << 1.0 << 2.0 << 4.0) <<
                0.5 <<
                2.0 <<
                (int) (RangeFilter::Max_Inclusive | RangeFilter::Min_Inclusive);
        QTest::newRow("Min inclusive max inclusive reject") <<
                (QVector<double>() << 0.25 << 0.5 << 1.0 << 2.0 << 4.0) <<
                0.5 <<
                2.0 <<
                (int) (RangeFilter::Reject_Range |
                        RangeFilter::Min_Inclusive |
                        RangeFilter::Max_Inclusive);
    }
};

QTEST_APPLESS_MAIN(TestRangeFilter)

#include "rangefilter.moc"
