/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QTest>
#include <QtAlgorithms>

#include "algorithms/dewpoint.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Algorithms;

class TestDewpoint : public QObject {
Q_OBJECT
private slots:

    void dewpoint()
    {
        QFETCH(double, t);
        QFETCH(double, rh);
        QFETCH(bool, forceWater);
        QFETCH(double, expected);

        double result = Dewpoint::dewpoint(t, rh, forceWater);
        QVERIFY(fabs(result - expected) <= 1E-6);
    }

    void dewpoint_data()
    {
        QTest::addColumn<double>("t");
        QTest::addColumn<double>("rh");
        QTest::addColumn<bool>("forceWater");
        QTest::addColumn<double>("expected");

        QTest::newRow("20 10") << 20.0 << 10.0 << false << -11.1798699958809;
        QTest::newRow("30 10") << 30.0 << 10.0 << false << -4.34958706769009;
        QTest::newRow("30 80") << 30.0 << 80.0 << false << 26.1684418167009;
        QTest::newRow("30 80 water") << 30.0 << 80.0 << true << 26.1684418167009;
        QTest::newRow("30 10 water") << 30.0 << 10.0 << true << -4.90916662962104;
        QTest::newRow("-10 10") << -10.0 << 10.0 << false << -33.597234223073;
        QTest::newRow("-10 10 water") << -10.0 << 10.0 << true << -35.9333319940692;
    }

    void rh()
    {
        QFETCH(double, t);
        QFETCH(double, td);
        QFETCH(bool, forceWater);
        QFETCH(double, expected);

        double result = Dewpoint::rh(t, td, forceWater);
        QVERIFY(fabs(result - expected) <= 1E-6);
    }

    void rh_data()
    {
        QTest::addColumn<double>("t");
        QTest::addColumn<double>("td");
        QTest::addColumn<bool>("forceWater");
        QTest::addColumn<double>("expected");

        QTest::newRow("30 28") << 30.0 << 28.0 << false << 89.0767330629172;
        QTest::newRow("30 30") << 30.0 << 30.0 << false << 100.0;
        QTest::newRow("30 -10") << 30.0 << -10.0 << false << 6.11960074076457;
        QTest::newRow("30 -10 water") << 30.0 << -10.0 << true << 6.74489883084801;
        QTest::newRow("-5 -10") << -5.0 << -10.0 << false << 64.686228666108;
        QTest::newRow("-5 -10 water") << -5.0 << -10.0 << true << 67.9145715862388;
    }

    void temperature()
    {
        QFETCH(double, rh);
        QFETCH(double, td);
        QFETCH(bool, forceWater);
        QFETCH(double, expected);

        double result = Dewpoint::t(rh, td, forceWater);
        QVERIFY(fabs(result - expected) <= 1E-6);
    }

    void temperature_data()
    {
        QTest::addColumn<double>("rh");
        QTest::addColumn<double>("td");
        QTest::addColumn<bool>("forceWater");
        QTest::addColumn<double>("expected");

        QTest::newRow("30 28") << 89.0767330629172 << 28.0 << false << 30.0;
        QTest::newRow("30 30") << 100.0 << 30.0 << false << 30.0;
        QTest::newRow("30 -10") << 6.11960074076457 << -10.0 << false << 30.0;
        QTest::newRow("30 -10 water") << 6.74489883084801 << -10.0 << true << 30.0;
        QTest::newRow("-5 -10") << 64.686228666108 << -10.0 << false << -5.0;
        QTest::newRow("-5 -10 water") << 67.9145715862388 << -10.0 << true << -5.0;
    }

    void rhExtrapolate()
    {
        QFETCH(double, t1);
        QFETCH(double, rh1);
        QFETCH(double, t2);
        QFETCH(bool, forceWater);
        QFETCH(double, expected);

        double result = Dewpoint::rhExtrapolate(t1, rh1, t2, forceWater);
        QVERIFY(fabs(result - expected) <= 1E-6);
    }

    void rhExtrapolate_data()
    {
        QTest::addColumn<double>("t1");
        QTest::addColumn<double>("rh1");
        QTest::addColumn<double>("t2");
        QTest::addColumn<bool>("forceWater");
        QTest::addColumn<double>("expected");

        QTest::newRow("10 20 15") << 10.0 << 20.0 << 15.0 << false << 14.4005416242256;
        QTest::newRow("-10 20 -15") << -10.0 << 20.0 << -15.0 << false << 31.448302796248;
        QTest::newRow("-10 20 -15 water") << -10.0 << 20.0 << -15.0 << true << 29.9503799100084;
    }

    void wetBulb()
    {
        QFETCH(double, t);
        QFETCH(double, td);
        QFETCH(double, P);
        QFETCH(double, expected);

        double result = Dewpoint::wetBulb(t, td, P);
        QVERIFY(fabs(result - expected) <= 1E-2);
    }

    void wetBulb_data()
    {
        QTest::addColumn<double>("t");
        QTest::addColumn<double>("td");
        QTest::addColumn<double>("P");
        QTest::addColumn<double>("expected");

        QTest::newRow("30 25 850") << 30.0 << 25.0 << 850.0 << 26.1440;
        QTest::newRow("30 29 920") << 30.0 << 29.0 << 920.0 << 29.2119;
        QTest::newRow("35 10 920") << 35.0 << 10.0 << 900.0 << 19.0118;
    }
};

QTEST_APPLESS_MAIN(TestDewpoint)

#include "dewpoint.moc"
