/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "levmar/levmar.h"

#include "algorithms/lma.hxx"

#define MAXIMUM_ITERATIONS  1000

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/lma.hxx
 * General routines for the Levenberg-Marquardt Algorithm.
 */

bool LevenbergMarquardt::runAnalytic(void (*func)(double *p, double *hx, int m, int n, void *adata),
                                     void (*jacf)(double *p, double *j, int m, int n, void *adata),
                                     double *p,
                                     const double *x,
                                     int m,
                                     int n,
                                     void *adata)
{
    return ::dlevmar_der(func, jacf, p, const_cast<double *>(x), m, n, MAXIMUM_ITERATIONS, NULL,
                         NULL, NULL, NULL, adata) != LM_ERROR;
}

bool LevenbergMarquardt::runFDA(void (*func)(double *p, double *hx, int m, int n, void *adata),
                                double *p,
                                const double *x,
                                int m,
                                int n,
                                void *adata)
{
    return ::dlevmar_dif(func, p, const_cast<double *>(x), m, n, MAXIMUM_ITERATIONS, NULL, NULL,
                         NULL, NULL, adata) != LM_ERROR;
}

}
}
