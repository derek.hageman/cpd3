/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSCSPLINE_H
#define CPD3ALGORITHMSCSPLINE_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QList>
#include <QPointF>
#include <QPair>
#include <QDataStream>
#include "algorithms/algorithms.hxx"

#include "core/number.hxx"
#include "algorithms/model.hxx"

namespace CPD3 {
namespace Algorithms {

class CSpline;

class CSplineInterpolator;

CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const CSpline &spline);

CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, CSpline &spline);

CPD3ALGORITHMS_EXPORT QDebug operator<<(QDebug stream, const CSpline &spline);


/**
 * A simple stand alone cubic spline interpolator.
 */
class CPD3ALGORITHMS_EXPORT CSpline {
    struct Component {
        double start;
        double a;
        double b;
        double c;
        double d;

        bool operator<(double d) const;
    };

    int nComponents;
    const Component *lastHit;
    Component *components;

    struct InputPoint {
        double x;
        double y;

        InputPoint(double x, double y);

        bool operator<(const InputPoint &other) const;
    };

    void generate(const std::vector<InputPoint> &points, double dX0, double dXN);

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator<<(QDataStream &stream, const CSpline &spline);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, CSpline &spline);

    friend CPD3ALGORITHMS_EXPORT QDebug operator<<(QDebug stream, const CSpline &spline);

    friend class CSplineInterpolator;

public:
    ~CSpline();

    CSpline();

    CSpline(const CSpline &other);

    CSpline &operator=(const CSpline &other);

    CSpline(const QVector<double> &x,
            const QVector<double> &y,
            double dX0 = FP::undefined(),
            double dXN = FP::undefined());

    CSpline(const QList<QPointF> &points,
            double dX0 = FP::undefined(),
            double dXN = FP::undefined());

    CSpline(const QList<QPair<double, double> > &points,
            double dX0 = FP::undefined(),
            double dXN = FP::undefined());

    double evaluateConst(double input) const;

    double evaluate(double input);

    inline double evaluate(double input) const
    { return evaluateConst(input); }

    double derivativeConst(double input) const;

    double derivative(double input);

    inline double derivative(double input) const
    { return derivativeConst(input); }
};


/**
 * A cubic spline interpolator.  This will produce interpolated values for
 * each input value.  Note that the same spline is used to evaluate all inputs.
 * That is, this is only a two dimensional spline.
 */
class CPD3ALGORITHMS_EXPORT CSplineInterpolator : public Model {
    CSpline spline;
    ModelOutputConstraints constraints;

    inline double evaluate(double input)
    { return constraints.apply(spline.evaluate(input)); }

    inline double evaluate(double input) const
    { return constraints.apply(spline.evaluate(input)); }

    class Single : public Model {
        double start;
        double a;
        double b;
        double c;
        double d;
        ModelOutputConstraints constraints;

        inline double evaluate(double input) const
        {
            double v = input - start;
            return constraints.apply(a + v * (b + v * (c + v * d)));
        }

    public:
        virtual ~Single();

        Single(double start,
               double a,
               double b,
               double c,
               double d,
               const ModelOutputConstraints &output);

        virtual QVector<double> applyConst(const QVector<double> &inputs) const;

        virtual QVector<double> apply(const QVector<double> &inputs);

        virtual void applyIOConst(QVector<double> &values) const;

        virtual void applyIO(QVector<double> &values);

        virtual double oneToOneConst(double input) const;

        virtual double oneToOne(double input);

        virtual double twoToOneConst(double a, double b) const;

        virtual double twoToOne(double a, double b);

        virtual double threeToOneConst(double a, double b, double c) const;

        virtual double threeToOne(double a, double b, double c);

        virtual void applyIOConst(double &a, double &b) const;

        virtual void applyIO(double &a, double &b);

        virtual void applyIOConst(double &a, double &b, double &c) const;

        virtual void applyIO(double &a, double &b, double &c);

        virtual Model *clone() const;

        virtual QString describe() const;

        virtual ModelLegendEntry legend(const ModelLegendParameters &parameters) const;

        friend CPD3ALGORITHMS_EXPORT QDataStream
                &operator<<(QDataStream &stream, const Model *model);

        friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

    protected:
        Single(QDataStream &stream);

        virtual void serialize(QDataStream &stream) const;

        virtual void printLog(QDebug &stream) const;
    };

public:
    virtual ~CSplineInterpolator();

    CSplineInterpolator(const CSpline &spline,
                        const ModelOutputConstraints &output = ModelOutputConstraints());

    CSplineInterpolator(const QVector<double> &x,
                        const QVector<double> &y,
                        double dX0 = FP::undefined(),
                        double dXN = FP::undefined(),
                        const ModelOutputConstraints &output = ModelOutputConstraints());

    virtual QVector<double> applyConst(const QVector<double> &inputs) const;

    virtual QVector<double> apply(const QVector<double> &inputs);

    virtual void applyIOConst(QVector<double> &values) const;

    virtual void applyIO(QVector<double> &values);

    virtual double oneToOneConst(double input) const;

    virtual double oneToOne(double input);

    virtual double twoToOneConst(double a, double b) const;

    virtual double twoToOne(double a, double b);

    virtual double threeToOneConst(double a, double b, double c) const;

    virtual double threeToOne(double a, double b, double c);

    virtual void applyIOConst(double &a, double &b) const;

    virtual void applyIO(double &a, double &b);

    virtual void applyIOConst(double &a, double &b, double &c) const;

    virtual void applyIO(double &a, double &b, double &c);

    virtual Model *clone() const;

    virtual QString describe() const;

    virtual ModelLegendEntry
            legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

    static Model *create(const QVector<double> &x,
                         const QVector<double> &y,
                         double dX0 = FP::undefined(),
                         double dXN = FP::undefined(),
                         const ModelOutputConstraints &output = ModelOutputConstraints());

protected:
    CSplineInterpolator(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

}
}

#endif
