/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QSslSocket>
#include <QDir>
#include <QBuffer>
#include <QCoreApplication>
#include <QSslKey>
#include <QHostInfo>
#include <QSslConfiguration>
#include <QThread>
#include <QTimer>
#include <QEventLoop>
#include <QLoggingCategory>

#include <openssl/sha.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/evp.h>
#include <openssl/crypto.h>
#include <openssl/err.h>
#include <openssl/x509v3.h>

#include "algorithms/cryptography.hxx"
#include "core/openssl.hxx"
#include "core/waitutils.hxx"
#include "io/drivers/file.hxx"


Q_LOGGING_CATEGORY(log_algorithms_cryptography, "cpd3.algorithms.cryptography", QtWarningMsg)

using namespace CPD3::Data;

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/cryptography.hxx
 * General cryptography routines used by CPD3.
 */

static std::unique_ptr<QIODevice> tryFileLoad(const Variant::Read &source)
{
    if (source.getType() != Variant::Type::String)
        return {};
    QString path(source.toQString());
    if (path.isEmpty())
        return {};

    {
        QFileInfo info(path);
        if (info.exists()) {
            if (auto result = IO::Access::file(info, IO::File::Mode::readOnly())->qioBlock()) {
                if (result->open(QIODevice::ReadOnly)) {
                    return std::move(result);
                }
            }
        }
    }

    QByteArray check(qgetenv("CPD3"));
    if (!check.isEmpty()) {
        QFileInfo info(QDir(check).filePath(path));
        if (info.exists()) {
            if (auto result = IO::Access::file(info, IO::File::Mode::readOnly())->qioBlock()) {
                if (result->open(QIODevice::ReadOnly)) {
                    return std::move(result);
                }
            }
        }
    }

    return {};
}

static bool tryDirectoryLoad(const Variant::Read &source, QDir &target)
{
    if (source.getType() != Variant::Type::String)
        return false;
    QString path(source.toQString());
    if (path.isEmpty())
        return false;

    target.setPath(path);
    if (target.exists())
        return true;

    QByteArray check(qgetenv("CPD3"));
    if (!check.isEmpty()) {
        QDir cpd3Path(check);
        target.setPath(cpd3Path.filePath(path));
        if (target.exists())
            return true;
    }

    return false;
}

static QString passwordOrPasswordFile(const Variant::Read &config, bool loadExternal)
{
    QString password(config.hash("Password").toQString());
    if (password.isEmpty() && loadExternal) {
        QString fileName(config.hash("PasswordFile").toQString());
        if (!fileName.isEmpty() && QFile::exists(fileName)) {
            if (auto file = IO::Access::file(fileName, IO::File::Mode::readOnly())->block()) {
                QByteArray contents;
                file->read(contents);
                password = QString::fromUtf8(contents);
            } else {
                qCDebug(log_algorithms_cryptography) << "Failed to open password file" << fileName;
            }
        }
    }
    return password;
}

QSslCertificate Cryptography::getCertificate(const Variant::Read &config, bool loadExternal)
{
    if (loadExternal) {
        if (auto file = tryFileLoad(config)) {
            QSslCertificate result(file.get(), QSsl::Pem);
            if (!result.isNull())
                return result;

            file->seek(0);
            result = QSslCertificate(file.get(), QSsl::Der);
            if (!result.isNull())
                return result;
        }
        QDir dir;
        if (tryDirectoryLoad(config, dir)) {
            QList<QSslCertificate> result
                    (QSslCertificate::fromPath(dir.absolutePath() + "/*.pem", QSsl::Pem,
                                               QRegExp::Wildcard));
            result.append(QSslCertificate::fromPath(dir.absolutePath() + "/*.der", QSsl::Der,
                                                    QRegExp::Wildcard));
            if (!result.isEmpty())
                return result.first();
        }
    }

    switch (config.getType()) {
    case Variant::Type::String: {
        QSslCertificate result(QByteArray::fromStdString(config.toString()), QSsl::Pem);
        if (!result.isNull())
            return result;
        /* Fall through */
    }

    case Variant::Type::Bytes: {
        QSslCertificate result(config.toBytes(), QSsl::Der);
        if (!result.isNull())
            return result;
        break;
    }

    case Variant::Type::Array: {
        for (auto add : config.toArray()) {
            QSslCertificate check(getCertificate(add, loadExternal));
            if (check.isNull())
                continue;
            return check;
        }
        break;
    }

    case Variant::Type::Hash:
        return getCertificate(config.hash("Certificate"), loadExternal);

    default:
        break;
    }

    return QSslCertificate();
}

QList<QSslCertificate> Cryptography::getMultipleCertificates(const Variant::Read &config,
                                                             bool loadExternal)
{
    if (loadExternal) {
        if (auto file = tryFileLoad(config)) {
            auto result = QSslCertificate::fromDevice(file.get());
            if (!result.isEmpty())
                return result;
            file->seek(0);
            result = QSslCertificate::fromDevice(file.get(), QSsl::Der);
            if (!result.isEmpty())
                return result;
        }
        QDir dir;
        if (tryDirectoryLoad(config, dir)) {
            QList<QSslCertificate> result
                    (QSslCertificate::fromPath(dir.absolutePath() + "/*.pem", QSsl::Pem,
                                               QRegExp::Wildcard));
            result.append(QSslCertificate::fromPath(dir.absolutePath() + "/*.der", QSsl::Der,
                                                    QRegExp::Wildcard));
            if (!result.isEmpty())
                return result;
        }
    }

    switch (config.getType()) {
    case Variant::Type::String: {
        QList<QSslCertificate> result
                (QSslCertificate::fromData(QByteArray::fromStdString(config.toString()),
                                           QSsl::Pem));
        if (!result.isEmpty())
            return result;
        /* Fall through */
    }

    case Variant::Type::Bytes: {
        QList<QSslCertificate> result(QSslCertificate::fromData(config.toBytes(), QSsl::Der));
        if (!result.isEmpty())
            return result;
        break;
    }

    case Variant::Type::Array: {
        QList<QSslCertificate> result;
        for (auto add : config.toArray()) {
            result.append(getMultipleCertificates(add, loadExternal));
        }
        return result;
    }

    case Variant::Type::Hash:
        return getMultipleCertificates(config.hash("Certificate"), loadExternal);

    default:
        break;
    }

    return QList<QSslCertificate>();
}

QSslKey Cryptography::getKey(const Variant::Read &config,
                             bool loadExternal,
                             const QString &password)
{
    QByteArray passData;
    if (!password.isEmpty())
        passData = password.toUtf8();
    if (loadExternal) {
        if (auto file = tryFileLoad(config)) {
            QSslKey result(file.get(), QSsl::Rsa, QSsl::Pem, QSsl::PrivateKey, passData);
            if (!result.isNull())
                return result;

            file->seek(0);
            result = QSslKey(file.get(), QSsl::Rsa, QSsl::Der, QSsl::PrivateKey, passData);
            if (!result.isNull())
                return result;

            file->seek(0);
            result = QSslKey(file.get(), QSsl::Ec, QSsl::Pem, QSsl::PrivateKey, passData);
            if (!result.isNull())
                return result;

            file->seek(0);
            result = QSslKey(file.get(), QSsl::Ec, QSsl::Der, QSsl::PrivateKey, passData);
            if (!result.isNull())
                return result;

            file->seek(0);
            result = QSslKey(file.get(), QSsl::Dsa, QSsl::Pem, QSsl::PrivateKey, passData);
            if (!result.isNull())
                return result;

            file->seek(0);
            result = QSslKey(file.get(), QSsl::Dsa, QSsl::Der, QSsl::PrivateKey, passData);
            if (!result.isNull())
                return result;
        }
    }

    switch (config.getType()) {
    case Variant::Type::String: {
        QSslKey result(QByteArray::fromStdString(config.toString()), QSsl::Rsa, QSsl::Pem,
                       QSsl::PrivateKey, passData);
        if (!result.isNull())
            return result;
        result = QSslKey(QByteArray::fromStdString(config.toString()), QSsl::Ec, QSsl::Pem,
                         QSsl::PrivateKey, passData);
        if (!result.isNull())
            return result;
        result = QSslKey(QByteArray::fromStdString(config.toString()), QSsl::Dsa, QSsl::Pem,
                         QSsl::PrivateKey, passData);
        if (!result.isNull())
            return result;
        /* Fall through */
    }

    case Variant::Type::Bytes: {
        QSslKey result(config.toBytes(), QSsl::Rsa, QSsl::Der, QSsl::PrivateKey, passData);
        if (!result.isNull())
            return result;
        result = QSslKey(config.toBytes(), QSsl::Ec, QSsl::Der, QSsl::PrivateKey, passData);
        if (!result.isNull())
            return result;
        result = QSslKey(config.toBytes(), QSsl::Dsa, QSsl::Der, QSsl::PrivateKey, passData);
        if (!result.isNull())
            return result;
        break;
    }

    case Variant::Type::Hash:
        if (passData.isEmpty()) {
            return getKey(config.hash("Key"), loadExternal,
                          passwordOrPasswordFile(config, loadExternal));
        } else {
            return getKey(config.hash("Key"), loadExternal, password);
        }

    default:
        break;
    }

    return QSslKey();
}

bool Cryptography::configureSSL(QSslConfiguration &ssl,
                                const Variant::Read &config,
                                bool loadExternal)
{
    QSslCertificate cert(getCertificate(config.hash("Certificate"), loadExternal));
    if (cert.isNull())
        cert = getCertificate(config.hash("Cert"), loadExternal);

    QSslKey key(getKey(config.hash("Key"), loadExternal));

    if (config.hash("Authority").exists()) {
        ssl.setCaCertificates(getMultipleCertificates(config.hash("Authority"), loadExternal));
    }

    if (!config.hash("NoLocal").toBool()) {
        if (cert.isNull() || key.isNull())
            return false;

        ssl.setLocalCertificate(cert);
        ssl.setPrivateKey(key);
    }

    qint64 version = config.hash("MinimumVersion").toInt64();
    /* Not ideal, but the older Qt versions only support TLS 1.0, and Qt5 defaults to 1.1
     * minimum required.  So we have to set 1.0 as the baseline explicitly and just
     * allow upgrading if possible. */
    if (!INTEGER::defined(version))
        version = 0;
    switch (version) {
    case -1:
        break;
    case 0:
        ssl.setProtocol(QSsl::TlsV1_0OrLater);
        break;
    case 1:
        ssl.setProtocol(QSsl::TlsV1_1OrLater);
        break;
    case 2:
    default:
        ssl.setProtocol(QSsl::TlsV1_2OrLater);
        break;
    }

    return true;
}

bool Cryptography::setupSocket(QSslSocket *socket, const Variant::Read &config, bool loadExternal)
{
    QSslConfiguration ssl;
    if (!configureSSL(ssl, config, loadExternal))
        return false;
    socket->setSslConfiguration(ssl);

    if (config.hash("RequireValid").toBool()) {
        socket->setPeerVerifyMode(QSslSocket::VerifyPeer);
    } else {
        socket->setPeerVerifyMode(QSslSocket::QueryPeer);
        socket->ignoreSslErrors(QList<QSslError>() << QSslError::SelfSignedCertificate
                                                   << QSslError::SelfSignedCertificateInChain
                                                   << QSslError::HostNameMismatch);
    }

    return true;
}

QByteArray Cryptography::sha512(const QByteArray &data)
{
    QCryptographicHash hash(QCryptographicHash::Sha512);
    hash.addData(data);
    return hash.result();
}

QByteArray Cryptography::sha512(const QSslCertificate &certificate)
{
    return certificate.digest(QCryptographicHash::Sha512);
}

static int openSSLPasswordCallback(char *buf, int size, int rwflag, void *userdata)
{
    Q_UNUSED(rwflag);
    std::string result(*(reinterpret_cast<const std::string *>(userdata)));
    if (result.size() >= size)
        result.resize(size - 1);
    memcpy(buf, result.c_str(), (size_t) result.size() + 1);
    return result.size();
}

static RSA *loadOpenSSLKey(const QByteArray &data, const std::string &password)
{
    BIO *bio = BIO_new_mem_buf((void *) data.constData(), data.length());
    RSA *key = PEM_read_bio_RSAPrivateKey(bio, NULL, openSSLPasswordCallback, (void *) &password);
    if (!key) {
        BIO_vfree(bio);
        bio = BIO_new_mem_buf((void *) data.constData(), data.length());
        key = d2i_RSAPrivateKey_bio(bio, NULL);
    }
    BIO_vfree(bio);
    return key;
}

static X509 *loadOpenSSLCert(const QByteArray &data)
{
    BIO *bio = BIO_new_mem_buf((void *) data.constData(), data.length());
    X509 *cert = PEM_read_bio_X509(bio, NULL, 0, NULL);
    if (!cert) {
        BIO_vfree(bio);
        bio = BIO_new_mem_buf((void *) data.constData(), data.length());
        cert = d2i_X509_bio(bio, NULL);
    }
    BIO_vfree(bio);
    return cert;
}

static bool loadOpenSSLCert(const QByteArray &data, X509_STORE *store)
{
    BIO *bio = BIO_new_mem_buf((void *) data.constData(), data.length());
    X509 *add = PEM_read_bio_X509(bio, NULL, 0, NULL);
    bool any = false;
    if (!add) {
        BIO_vfree(bio);
        bio = BIO_new_mem_buf((void *) data.constData(), data.length());
        add = d2i_X509_bio(bio, NULL);
        while (add) {
            X509_STORE_add_cert(store, add);
            X509_free(add);
            any = true;
            add = d2i_X509_bio(bio, NULL);
        }
    } else {
        while (add) {
            X509_STORE_add_cert(store, add);
            X509_free(add);
            any = true;
            add = PEM_read_bio_X509(bio, NULL, 0, NULL);
        }
    }
    BIO_vfree(bio);
    return any;
}

static bool loadOpenSSLCRL(const QByteArray &data, X509_STORE *store)
{
    BIO *bio = BIO_new_mem_buf((void *) data.constData(), data.length());
    X509_CRL *add = PEM_read_bio_X509_CRL(bio, NULL, 0, NULL);
    bool any = false;
    if (!add) {
        BIO_vfree(bio);
        bio = BIO_new_mem_buf((void *) data.constData(), data.length());
        add = d2i_X509_CRL_bio(bio, NULL);
        while (add) {
            X509_STORE_add_crl(store, add);
            any = true;
            add = d2i_X509_CRL_bio(bio, NULL);
        }
    } else {
        while (add) {
            X509_STORE_add_crl(store, add);
            any = true;
            add = PEM_read_bio_X509_CRL(bio, NULL, 0, NULL);
        }
    }
    BIO_vfree(bio);
    return any;
}

static ::RSA *loadSSLKey(const Variant::Read &config,
                         bool loadExternal,
                         const std::string &password)
{
    if (loadExternal) {
        if (auto file = tryFileLoad(config)) {
            RSA *result = loadOpenSSLKey(file->readAll(), password);
            if (result)
                return result;
        }
    }

    switch (config.getType()) {
    case Variant::Type::String: {
        QByteArray keyData = QByteArray::fromStdString(config.toString());
        keyData.append((char) 0);
        RSA *result = loadOpenSSLKey(keyData, password);
        if (result)
            return result;
        /* Fall through */
    }

    case Variant::Type::Bytes: {
        QByteArray keyData(config.toBinary());
        RSA *result = loadOpenSSLKey(keyData, password);
        if (result)
            return result;
        break;
    }

    case Variant::Type::Hash:
        if (password.empty()) {
            return loadSSLKey(config.hash("Key"), loadExternal,
                              passwordOrPasswordFile(config, loadExternal).toStdString());
        } else {
            return loadSSLKey(config.hash("Key"), loadExternal, password);
        }

    default:
        break;
    }

    return 0;
}

static X509 *loadSSLCert(const Variant::Read &config, bool loadExternal)
{
    if (loadExternal) {
        if (auto file = tryFileLoad(config)) {
            X509 *result = loadOpenSSLCert(file->readAll());
            if (result)
                return result;
        }
    }

    switch (config.getType()) {
    case Variant::Type::String: {
        QByteArray certData = QByteArray::fromStdString(config.toString());
        certData.append((char) 0);
        X509 *result = loadOpenSSLCert(certData);
        if (result)
            return result;
        /* Fall through */
    }

    case Variant::Type::Bytes: {
        QByteArray certData(config.toBinary());
        X509 *result = loadOpenSSLCert(certData);
        if (result)
            return result;
        break;
    }

    case Variant::Type::Array: {
        for (auto add : config.toArray()) {
            X509 *result = loadSSLCert(add, loadExternal);
            if (result)
                return result;
        }
        break;
    }

    case Variant::Type::Hash:
        return loadSSLCert(config.hash("Certificate"), loadExternal);

    default:
        break;
    }

    return 0;
}

static bool loadSSLCertChain(const Variant::Read &config, X509_STORE *cert, bool loadExternal)
{
    if (loadExternal) {
        if (auto file = tryFileLoad(config)) {
            if (loadOpenSSLCert(file->readAll(), cert))
                return true;
        }
    }

    switch (config.getType()) {
    case Variant::Type::String: {
        QByteArray certData = QByteArray::fromStdString(config.toString());
        certData.append((char) 0);
        if (loadOpenSSLCert(certData, cert))
            return true;
        /* Fall through */
    }

    case Variant::Type::Bytes: {
        QByteArray certData(config.toBinary());
        if (loadOpenSSLCert(certData, cert))
            return true;
        break;
    }

    case Variant::Type::Array: {
        bool ok = false;
        for (auto add : config.toArray()) {
            if (loadSSLCertChain(add, cert, loadExternal))
                ok = true;
        }
        return ok;
    }

    case Variant::Type::Hash:
        return loadSSLCertChain(config.hash("Certificate"), cert, loadExternal);

    default:
        break;
    }

    return false;
}

static bool loadSSLRevoke(const Variant::Read &config, X509_STORE *crl, bool loadExternal)
{
    if (loadExternal) {
        if (auto file = tryFileLoad(config)) {
            if (loadOpenSSLCRL(file->readAll(), crl))
                return true;
        }
    }

    switch (config.getType()) {
    case Variant::Type::String: {
        QByteArray crlData = QByteArray::fromStdString(config.toString());
        crlData.append((char) 0);
        if (loadOpenSSLCRL(crlData, crl))
            return true;
        /* Fall through */
    }

    case Variant::Type::Bytes: {
        QByteArray crlData(config.toBinary());
        if (loadOpenSSLCRL(crlData, crl))
            return true;
        break;
    }

    case Variant::Type::Array: {
        bool ok = false;
        for (auto add : config.toArray()) {
            if (loadSSLRevoke(add, crl, loadExternal))
                ok = true;
        }
        return ok;
    }

    case Variant::Type::Hash:
        return loadSSLRevoke(config.hash("Revoke"), crl, loadExternal);

    default:
        break;
    }

    return false;
}


/*
 * id-sha512  OBJECT IDENTIFIER  ::=  { joint-iso-itu-t(2) country(16) us(840) organization(1)
 * gov(101) csor(3)nistalgorithm(4) hashalgs(2) 3 }
 */
static Util::ByteArray asn1SignatureHash(const Util::ByteView &hash,
                                         const Util::ByteView &oid = Util::ByteView(
                                                 "\x60\x86\x48\x01\x65\x03\x04\x02\x03"))
{
    /*
     * asn1 = SEQUENCE:digest
     *
     * [digest]
     * algorithm=SEQUENCE:digest_algorithm
     * hash=OCTETSTRING:...
     *
     * [digest_algorithm]
     * algorithm=OID:digestAlgorithm
     * parameter=NULL
     */
    Util::ByteArray result;
    result.push_back(0x30);
    result.push_back((0x08 + oid.size() + hash.size()));
    result.push_back(0x30);
    result.push_back((0x04 + oid.size()));
    result.push_back(0x06);
    result.push_back(oid.size());
    result += oid;
    result.push_back(0x05);
    result.push_back(0x00);
    result.push_back(0x04);
    result.push_back(hash.size());
    result += hash;
    return result;
}

Util::ByteArray Cryptography::signHash(const Variant::Read &key,
                                       const Util::ByteView &hash,
                                       bool loadExternal,
                                       const std::string &password)
{
    OpenSSL::initialize();

    auto rsa = loadSSLKey(key, loadExternal, password);
    if (!rsa)
        return {};

    auto data = asn1SignatureHash(hash);

    int length = ::RSA_size(rsa);
    if (length < static_cast<int>(data.size())) {
        ::RSA_free(rsa);
        return {};
    }

    Util::ByteArray result;
    result.resize(length, 0);
    if (::RSA_private_encrypt(data.size(), data.data<const unsigned char *>(),
                              result.data<unsigned char *>(), rsa, RSA_PKCS1_PADDING) <= 0) {
        ::RSA_free(rsa);
        return {};
    }

    ::RSA_free(rsa);
    return result;
}

Util::ByteArray Cryptography::sha512(std::unique_ptr<IO::Generic::Stream> &&input)
{
    static constexpr std::size_t stallAcquireThreshold = 2 * 1024 * 1024;
    static constexpr std::size_t stallReleaseThreshold = 1 * 1024 * 1024;

    QCryptographicHash hash(QCryptographicHash::Sha512);
    {
        std::mutex mutex;
        std::condition_variable cv;
        Util::ByteArray pending;
        bool ended = false;

        input->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                pending += data;
            }
            cv.notify_all();
        });
        input->ended.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ended = true;
            }
            cv.notify_all();
        });

        input->start();

        std::unique_lock<std::mutex> lock(mutex);
        bool stalled = false;
        for (;;) {
            if (!lock)
                lock.lock();
            if (!pending.empty()) {
                auto process = std::move(pending);
                pending.clear();
                lock.unlock();

                if (!stalled && process.size() > stallAcquireThreshold) {
                    stalled = true;
                    input->readStall(stalled);
                } else if (stalled && process.size() < stallReleaseThreshold) {
                    stalled = false;
                    input->readStall(stalled);
                }

                hash.addData(process.toQByteArrayRef());
                continue;
            }

            if (ended)
                break;

            if (stalled) {
                stalled = false;
                lock.unlock();
                input->readStall(false);
                continue;
            }
            cv.wait(lock);
        }

        input.reset();
    }
    return Util::ByteArray(hash.result());
}

Util::ByteArray Cryptography::signData(const Data::Variant::Read &key,
                                       std::unique_ptr<IO::Generic::Stream> &&data,
                                       bool loadExternal,
                                       const std::string &password)
{
    return signHash(key, sha512(std::move(data)), loadExternal, password);
}

bool Cryptography::checkHashSignature(const Data::Variant::Read &certificate,
                                      const Util::ByteView &signature,
                                      const Util::ByteView &hash,
                                      bool loadExternal)
{
    if (signature.empty())
        return true;
    OpenSSL::initialize();

    auto cert = loadSSLCert(certificate, loadExternal);
    if (!cert)
        return false;

    auto pkey = ::X509_get_pubkey(cert);
    if (!pkey) {
        ::X509_free(cert);
        return false;
    }

    auto rsa = ::EVP_PKEY_get1_RSA(pkey);
    if (!rsa) {
        ::EVP_PKEY_free(pkey);
        ::X509_free(cert);
        return false;
    }

    auto data = asn1SignatureHash(hash);

    int length = ::RSA_size(rsa);
    if (length < data.size()) {
        ::RSA_free(rsa);
        ::EVP_PKEY_free(pkey);
        ::X509_free(cert);
        return false;
    }

    Util::ByteArray result;
    result.resize(length, 0);
    if (::RSA_public_decrypt(signature.size(), signature.data<const unsigned char *>(),
                             result.data<unsigned char *>(), rsa, RSA_PKCS1_PADDING) <= 0) {
        ::RSA_free(rsa);
        ::EVP_PKEY_free(pkey);
        ::X509_free(cert);
        return false;
    }

    ::RSA_free(rsa);
    ::EVP_PKEY_free(pkey);
    ::X509_free(cert);

    return ::CRYPTO_memcmp(result.data(), data.data(), static_cast<::size_t>(data.size())) == 0;
}

bool Cryptography::checkSignature(const Data::Variant::Read &certificate,
                                  const Util::ByteView &signature,
                                  std::unique_ptr<IO::Generic::Stream> &&data,
                                  bool loadExternal)
{
    return checkHashSignature(certificate, signature, sha512(std::move(data)), loadExternal);
}

static bool runCFB(std::unique_ptr<IO::Generic::Stream> &&input,
                   IO::Generic::Writable &output,
                   const unsigned char *aesKey,
                   const unsigned char *aesIV,
                   bool encrypt)
{
    static constexpr std::size_t stallAcquireThreshold = 2 * 1024 * 1024;
    static constexpr std::size_t stallReleaseThreshold = 1 * 1024 * 1024;

    class Context {
        ::EVP_CIPHER_CTX *ctx;
        std::size_t bufferSize;
    public:
        Context(const unsigned char *aesKey, const unsigned char *aesIV, bool encrypt) : ctx(
                nullptr), bufferSize(0)
        {
            ctx = ::EVP_CIPHER_CTX_new();
            if (!ctx)
                return;

            if (::EVP_CipherInit_ex(ctx, EVP_aes_256_cfb(), NULL, aesKey, aesIV, encrypt ? 1 : 0) <=
                    0) {
                ::EVP_CIPHER_CTX_free(ctx);
                ctx = nullptr;
                return;
            }
        }

        ~Context()
        {
            if (!ctx)
                return;
            ::EVP_CIPHER_CTX_free(ctx);
        }

        bool ready() const
        { return ctx != nullptr; }

        bool step(const Util::ByteView &input, Util::ByteArray &output)
        {
            Q_ASSERT(output.empty());
            if (input.empty())
                return true;
            auto add = input.size();

            output.resize(add + 65);
            bufferSize = output.size();
            int wlen = 0;
            if (::EVP_CipherUpdate(ctx, output.data<unsigned char *>(), &wlen,
                                   input.data<const unsigned char *>(), static_cast<int>(add)) <=
                    0) {
                return false;
            }
            Q_ASSERT(wlen >= 0);
            output.resize(wlen);
            return true;
        }

        bool final(Util::ByteArray &output)
        {
            output.resize(bufferSize);
            int wlen = 0;
            if (::EVP_CipherFinal(ctx, output.data<unsigned char *>(), &wlen) <= 0) {
                return false;
            }
            Q_ASSERT(wlen >= 0);
            output.resize(wlen);
            return true;
        }
    };

    Context cfb(aesKey, aesIV, encrypt);
    if (!cfb.ready())
        return false;
    {
        std::mutex mutex;
        std::condition_variable cv;
        Util::ByteArray pending;
        bool ended = false;

        input->read.connect([&](const Util::ByteArray &data) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                pending += data;
            }
            cv.notify_all();
        });
        input->ended.connect([&]() {
            {
                std::lock_guard<std::mutex> lock(mutex);
                ended = true;
            }
            cv.notify_all();
        });

        input->start();

        std::unique_lock<std::mutex> lock(mutex);
        bool stalled = false;
        for (;;) {
            if (!lock)
                lock.lock();
            if (!pending.empty()) {
                auto process = std::move(pending);
                pending.clear();
                lock.unlock();

                if (!stalled && process.size() > stallAcquireThreshold) {
                    stalled = true;
                    input->readStall(stalled);
                } else if (stalled && process.size() < stallReleaseThreshold) {
                    stalled = false;
                    input->readStall(stalled);
                }

                Util::ByteArray result;
                if (!cfb.step(process, result)) {
                    input.reset();
                    return false;
                }
                output.write(std::move(result));
                continue;
            }

            if (ended)
                break;

            if (stalled) {
                stalled = false;
                lock.unlock();
                input->readStall(false);
                continue;
            }
            cv.wait(lock);
        }
    }

    input.reset();

    Util::ByteArray result;
    if (!cfb.final(result))
        return false;
    output.write(std::move(result));
    return true;
}

bool Cryptography::encrypt(const Data::Variant::Read &origin,
                           std::unique_ptr<IO::Generic::Stream> &&input,
                           IO::Generic::Writable &output,
                           bool usePrivate,
                           bool loadExternal,
                           const std::string &password)
{
    OpenSSL::initialize();

    struct Context {
        ::X509 *cert;
        ::EVP_PKEY *pkey;
        ::RSA *rsa;
        Util::ByteArray header;
        Util::ByteArray encryptedHeader;

        Context() : cert(nullptr), pkey(nullptr), rsa(nullptr)
        {
            header.resize(1 + 32 + 16, 0);
        }

        ~Context()
        {
            if (rsa)
                ::RSA_free(rsa);
            if (pkey)
                ::EVP_PKEY_free(pkey);
            if (cert)
                ::X509_free(cert);
            header.filled(0, header.size());
            encryptedHeader.filled(0, encryptedHeader.size());
        }
    };

    bool ok = false;
    Context ctx;
    unsigned char *aesKey = nullptr;
    unsigned char *aesIV = nullptr;

    if (usePrivate) {
        ctx.rsa = loadSSLKey(origin, loadExternal, password);
        if (!ctx.rsa) {
            qCDebug(log_algorithms_cryptography) << "Error loading key";
            return false;
        }
    } else {
        ctx.cert = loadSSLCert(origin, loadExternal);
        if (!ctx.cert) {
            qCDebug(log_algorithms_cryptography) << "Error loading certificate";
            return false;
        }
        ctx.pkey = ::X509_get_pubkey(ctx.cert);
        if (!ctx.pkey) {
            qCDebug(log_algorithms_cryptography) << "Unable get get public key";
            return false;
        }
        ctx.rsa = ::EVP_PKEY_get1_RSA(ctx.pkey);
        if (!ctx.rsa) {
            qCDebug(log_algorithms_cryptography) << "Unable to extract RSA key";
            return false;
        }
    }

    int blockLength = ::RSA_size(ctx.rsa);
    if (blockLength < ctx.header.size())
        return false;

    unsigned char *ptr = ctx.header.data<unsigned char *>();
    /* Version */
    *ptr = 1;
    ptr++;

    aesKey = ptr;
    if (!Random::fill(ptr, 32)) {
        qCDebug(log_algorithms_cryptography) << "Random number generation failure";
        return false;
    }
    ptr += 32;

    aesIV = ptr;
    if (!Random::fill(ptr, 16)) {
        qCDebug(log_algorithms_cryptography) << "Random number generation failure";
        return false;
    }
    ptr += 16;

    ctx.encryptedHeader.resize(blockLength, 0);
    if (usePrivate) {
        if (::RSA_private_encrypt(ctx.header.size(), ctx.header.data<const unsigned char *>(),
                                  ctx.encryptedHeader.data<unsigned char *>(), ctx.rsa,
                                  RSA_PKCS1_PADDING) <= 0) {
            qCDebug(log_algorithms_cryptography) << "RSA private key encryption failure";
            return false;
        }
    } else {
        if (::RSA_public_encrypt(ctx.header.size(), ctx.header.data<const unsigned char *>(),
                                 ctx.encryptedHeader.data<unsigned char *>(), ctx.rsa,
                                 RSA_PKCS1_PADDING) <= 0) {
            qCDebug(log_algorithms_cryptography) << "RSA public key encryption failure";
            return false;
        }
    }

    output.write(ctx.encryptedHeader);

    return runCFB(std::move(input), output, aesKey, aesIV, true);
}

Util::ByteArray Cryptography::encryptData(const Data::Variant::Read &key,
                                          const Util::ByteView &data,
                                          bool loadExternal,
                                          const std::string &password)
{
    std::unique_ptr<IO::Generic::Stream> input(new IO::Generic::Stream::ByteView(data));
    Util::ByteArray result;
    IO::Generic::Block::ByteArray output(result);
    if (!encrypt(key, std::move(input), output, true, loadExternal, password))
        return {};
    return result;
}

bool Cryptography::decrypt(const Data::Variant::Read &origin,
                           std::unique_ptr<IO::Generic::Stream> &&input,
                           IO::Generic::Writable &output,
                           bool usePrivate,
                           bool loadExternal,
                           const std::string &password)
{
    OpenSSL::initialize();

    static constexpr std::size_t headerSize = 1 + 32 + 16;

    struct Context {
        X509 *cert;
        EVP_PKEY *pkey;
        RSA *rsa;
        Util::ByteArray encryptedHeader;
        Util::ByteArray header;

        Context() : cert(nullptr), pkey(nullptr), rsa(nullptr)
        { }

        ~Context()
        {
            if (rsa)
                ::RSA_free(rsa);
            if (pkey)
                ::EVP_PKEY_free(pkey);
            if (cert)
                ::X509_free(cert);
            header.filled(0, header.size());
            encryptedHeader.filled(0, encryptedHeader.size());
        }
    };

    Context ctx;

    if (usePrivate) {
        ctx.rsa = loadSSLKey(origin, loadExternal, password);
        if (!ctx.rsa) {
            qCDebug(log_algorithms_cryptography) << "Error loading key";
            return false;
        }
    } else {
        ctx.cert = loadSSLCert(origin, loadExternal);
        if (!ctx.cert) {
            qCDebug(log_algorithms_cryptography) << "Error loading certificate";
            return false;
        }
        ctx.pkey = X509_get_pubkey(ctx.cert);
        if (!ctx.pkey) {
            qCDebug(log_algorithms_cryptography) << "Unable get get public key";
            return false;
        }
        ctx.rsa = EVP_PKEY_get1_RSA(ctx.pkey);
        if (!ctx.rsa) {
            qCDebug(log_algorithms_cryptography) << "Unable to extract RSA key";
            return false;
        }
    }

    int blockLength = ::RSA_size(ctx.rsa);
    if (blockLength < static_cast<int>(headerSize))
        return false;

    auto withoutHeader =
            IO::Generic::Stream::detachHeader(std::move(input), [&](Util::ByteArray &buffer) {
                if (buffer.size() < blockLength)
                    return false;
                ctx.encryptedHeader = buffer.mid(0, blockLength);
                buffer.pop_front(blockLength);
                return true;
            });
    if (!withoutHeader) {
        qCDebug(log_algorithms_cryptography) << "Error reading encrypted header";
        return false;
    }

    ctx.header.resize(blockLength, 0);
    if (usePrivate) {
        if (::RSA_private_decrypt(ctx.encryptedHeader.size(),
                                  ctx.encryptedHeader.data<const unsigned char *>(),
                                  ctx.header.data<unsigned char *>(), ctx.rsa, RSA_PKCS1_PADDING) <=
                0) {
            qCDebug(log_algorithms_cryptography) << "RSA private key decryption failure";
            return false;
        }
    } else {
        if (::RSA_public_decrypt(ctx.encryptedHeader.size(),
                                 ctx.encryptedHeader.data<const unsigned char *>(),
                                 ctx.header.data<unsigned char *>(), ctx.rsa, RSA_PKCS1_PADDING) <=
                0) {
            qCDebug(log_algorithms_cryptography) << "RSA public key decryption failure";
            return false;
        }
    }

    unsigned char *aesKey = nullptr;
    unsigned char *aesIV = nullptr;
    unsigned char *ptr = ctx.header.data<unsigned char *>();
    /* Version */
    if (*ptr != 1) {
        qCDebug(log_algorithms_cryptography) << "Invalid encryption header";
        return false;
    }
    ptr++;

    aesKey = ptr;
    ptr += 32;

    aesIV = ptr;
    ptr += 16;

    return runCFB(std::move(withoutHeader), output, aesKey, aesIV, false);
}

Util::ByteArray Cryptography::decryptData(const Data::Variant::Read &certificate,
                                          const Util::ByteArray &data,
                                          bool loadExternal)
{
    std::unique_ptr<IO::Generic::Stream> input(new IO::Generic::Stream::ByteView(data));
    Util::ByteArray result;
    IO::Generic::Block::ByteArray output(result);
    if (!decrypt(certificate, std::move(input), output, false, loadExternal))
        return {};
    return result;
}

static QString readWholeBIO(BIO *bio)
{
    QByteArray buffer;
    for (;;) {
        int original = buffer.size();
        buffer.resize(original + 4096);
        int n = BIO_read(bio, buffer.data() + original, 4096);
        if (n <= 0)
            break;
        buffer.resize(original + n);
    }
    return QString::fromLatin1(buffer);
}

Variant::Root Cryptography::generateSelfSigned(const QStringList &subject, int bits)
{
    EVP_PKEY *pkey;
    pkey = EVP_PKEY_new();
    if (!pkey)
        return Variant::Root();

    RSA *rsa = RSA_generate_key(bits, RSA_F4, NULL, NULL);
    if (!rsa) {
        EVP_PKEY_free(pkey);
        return Variant::Root();
    }

    if (EVP_PKEY_assign_RSA(pkey, rsa) <= 0) {
        RSA_free(rsa);
        EVP_PKEY_free(pkey);
        return Variant::Root();
    }

    Variant::Root result;
    {
        BIO *bio = BIO_new(BIO_s_mem());
        if (!bio) {
            EVP_PKEY_free(pkey);
            return Variant::Root();
        }
        if (PEM_write_bio_RSAPrivateKey(bio, rsa, NULL, NULL, 0, NULL, NULL) <= 0) {
            BIO_free(bio);
            EVP_PKEY_free(pkey);
            return Variant::Root();
        }
        QString key(readWholeBIO(bio));
        BIO_free(bio);
        if (key.isEmpty()) {
            EVP_PKEY_free(pkey);
            return Variant::Root();
        }
        result.write().hash("Key").setString(key);
    }

    X509 *x509;
    x509 = X509_new();
    if (!x509) {
        EVP_PKEY_free(pkey);
        return Variant::Root();
    }

    ASN1_INTEGER_set(X509_get_serialNumber(x509), 1);
    X509_gmtime_adj(X509_get_notBefore(x509), -300);
    X509_gmtime_adj(X509_get_notAfter(x509), 20 * 365 * 86400);

    {
        X509_EXTENSION *ex = X509V3_EXT_conf_nid(NULL, NULL, NID_key_usage,
                                                 (char *) "digitalSignature, nonRepudiation, keyEncipherment, keyAgreement, keyCertSign");
        X509_add_ext(x509, ex, -1);
        X509_EXTENSION_free(ex);
    }
    {
        X509_EXTENSION *ex = X509V3_EXT_conf_nid(NULL, NULL, NID_netscape_cert_type,
                                                 (char *) "client, server, objsign");
        X509_add_ext(x509, ex, -1);
        X509_EXTENSION_free(ex);
    }

    if (X509_set_pubkey(x509, pkey) <= 0) {
        X509_free(x509);
        EVP_PKEY_free(pkey);
        return Variant::Root();
    }

    X509_NAME *name = X509_get_subject_name(x509);
    for (QStringList::const_iterator v = subject.constBegin(), endV = subject.constEnd();
            v != endV;
            ++v) {
        QString field(v->section('=', 0, 0));
        QString value(v->section('=', 1));

        X509_NAME_add_entry_by_txt(name, field.toUtf8().constData(), MBSTRING_UTF8,
                                   reinterpret_cast<const unsigned char *>(value.toUtf8()
                                                                                .constData()), -1,
                                   -1, 0);
    }
    X509_set_issuer_name(x509, name);


    if (X509_sign(x509, pkey, EVP_sha512()) <= 0) {
        X509_free(x509);
        EVP_PKEY_free(pkey);
        return Variant::Root();
    }

    {
        BIO *bio = BIO_new(BIO_s_mem());
        if (!bio) {
            X509_free(x509);
            EVP_PKEY_free(pkey);
            return Variant::Root();
        }
        if (PEM_write_bio_X509(bio, x509) <= 0) {
            X509_free(x509);
            EVP_PKEY_free(pkey);
            return Variant::Root();
        }
        QString cert(readWholeBIO(bio));
        BIO_free(bio);
        if (cert.isEmpty()) {
            X509_free(x509);
            EVP_PKEY_free(pkey);
            return Variant::Root();
        }
        result.write().hash("Certificate").setString(cert);
    }

    X509_free(x509);
    EVP_PKEY_free(pkey);
    return result;
}


bool Cryptography::verifyCertificate(const QSslCertificate &certificate,
                                     const Data::Variant::Read &config,
                                     bool loadExternal)
{
    if (!config.exists())
        return true;
    if (certificate.isNull())
        return false;
    OpenSSL::initialize();

    X509 *cert;
    {
        QByteArray data(certificate.toDer());
        BIO *bio = BIO_new_mem_buf((void *) data.constData(), data.length());
        cert = d2i_X509_bio(bio, NULL);
        BIO_vfree(bio);
    }
    if (!cert)
        return false;

    if (config.hash("RequireValid").toBool()) {
        X509_STORE *store = X509_STORE_new();
        if (!store) {
            X509_free(cert);
            return false;
        }

        X509_STORE_CTX *ctx = X509_STORE_CTX_new();
        if (!ctx) {
            X509_STORE_free(store);
            X509_free(cert);
            return false;
        }

        X509_STORE_set_flags(store, 0);

        loadSSLCertChain(config.hash("Authority"), store, loadExternal);
        loadSSLRevoke(config.hash("Revoke"), store, loadExternal);

        if (X509_STORE_CTX_init(ctx, store, cert, NULL) <= 0) {
            X509_STORE_CTX_free(ctx);
            X509_STORE_free(store);
            X509_free(cert);
            return false;
        }

        if (X509_verify_cert(ctx) <= 0) {
            X509_STORE_CTX_free(ctx);
            X509_STORE_free(store);
            X509_free(cert);
            return false;
        }

        X509_STORE_CTX_free(ctx);
        X509_STORE_free(store);

    } else if (config.hash("CheckSelfSigned").toBool()) {
        X509_STORE *store = X509_STORE_new();
        if (!store) {
            X509_free(cert);
            return false;
        }

        X509_STORE_CTX *ctx = X509_STORE_CTX_new();
        if (!ctx) {
            X509_STORE_free(store);
            X509_free(cert);
            return false;
        }

        X509_STORE_set_flags(store, 0);
        X509_STORE_add_cert(store, cert);

        if (X509_STORE_CTX_init(ctx, store, cert, NULL) <= 0) {
            X509_STORE_CTX_free(ctx);
            X509_STORE_free(store);
            X509_free(cert);
            return false;
        }

        if (X509_verify_cert(ctx) <= 0) {
            X509_STORE_CTX_free(ctx);
            X509_STORE_free(store);
            X509_free(cert);
            return false;
        }

        X509_STORE_CTX_free(ctx);
        X509_STORE_free(store);
    }


    {
        QByteArray cnData = QByteArray::fromStdString(config.hash("RequireName").toString());
        if (!cnData.isEmpty()) {
            QByteArray check(cnData.length() + 2, 0);
            X509_NAME *name = X509_get_subject_name(cert);
            int n = X509_NAME_get_text_by_NID(name, NID_commonName, check.data(), check.length());
            if (n <= 0) {
                X509_free(cert);
                return false;
            }
            check.resize(n);
            if (check != cnData) {
                X509_free(cert);
                return false;
            }
        }
    }

    X509_free(cert);
    return true;
}

bool Cryptography::verifyCertificate(const Variant::Read &certificate,
                                     const Data::Variant::Read &config,
                                     bool loadExternal)
{
    if (!config.exists())
        return true;
    return verifyCertificate(getCertificate(certificate, loadExternal), config, loadExternal);
}


SSLAuthenticationOption::SSLAuthenticationOption(const QString &argumentName,
                                                 const QString &displayName,
                                                 const QString &description,
                                                 const QString &defaultBehavior,
                                                 int sortPriority) : ComponentOptionBase(
        argumentName, displayName, description, defaultBehavior, sortPriority)
{ }

SSLAuthenticationOption::SSLAuthenticationOption(const SSLAuthenticationOption &other)
        : ComponentOptionBase(other), key(other.key), certificate(other.certificate)
{ }

void SSLAuthenticationOption::reset()
{
    optionSet = false;
    key.clear();
    certificate.clear();
}

ComponentOptionBase *SSLAuthenticationOption::clone() const
{ return new SSLAuthenticationOption(*this); }

CPD3::Data::Variant::Root SSLAuthenticationOption::getLocalConfiguration() const
{
    if (!optionSet)
        return CPD3::Data::Variant::Root();
    CPD3::Data::Variant::Root result;
    result.write().hash("Key").setString(key);
    result.write().hash("Certificate").setString(key);
    return result;
}

void SSLAuthenticationOption::setKey(const QString &key)
{
    optionSet = true;
    this->key = key;
}

void SSLAuthenticationOption::setCertificate(const QString &certificate)
{
    optionSet = true;
    this->certificate = certificate;
}

}
}
