/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSCRC_H
#define CPD3ALGORITHMSCRC_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QByteArray>
#include "algorithms/algorithms.hxx"
#include "core/util.hxx"


namespace CPD3 {
namespace Algorithms {

/**
 * 16-bit CRC calculation
 */
class CPD3ALGORITHMS_EXPORT CRC16 {
    quint16 table[256];
    quint16 accumulator;

    quint16 calculate(const quint8 *data, size_t n, quint16 ac) const;

public:
    /**
     * Create a new CRC calculator.
     * 
     * @param polynomial    the polynomial
     */
    CRC16(quint16 polynomial = 0xA001);

    /**
     * Reset the internal CRC calculation.
     */
    void reset();

    /**
     * Add data to the internal accumulator and return the current result.
     * 
     * @param data      the data to add
     * @return          the current CRC
     */
    quint16 add(const QByteArray &data);

    /**
     * Add data to the internal accumulator and return the current result.
     * 
     * @param data      the data to add
     * @param n         the number of bytes
     * @return          the current CRC
     */
    quint16 add(const void *data, size_t n);

    /**
     * Add data to the internal accumulator and return the current result.
     * 
     * @param data      the data to add
     * @return          the current CRC
     */
    quint16 add(const quint8 data);

    /**
     * Calculate the CRC of the given data.  This does not use the internal
     * state.
     * 
     * @param data      the data to calculate
     * @return          the CRC of the data
     */
    quint16 calculate(const QByteArray &data) const;

    quint16 calculate(const Util::ByteView &data) const;

    /**
     * Calculate the CRC of the given data.  This does not use the internal
     * state.
     * 
     * @param data      the data to calculate
     * @param n         the number of bytes
     * @return          the CRC of the data
     */
    quint16 calculate(const void *data, size_t n) const;
};

}
}

#endif
