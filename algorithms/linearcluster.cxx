/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <vector>
#include <QVector>

#include "algorithms/linearcluster.hxx"
#include "algorithms/statistics.hxx"

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/linearcluster.hxx
 * Provides routines for simple 1-D linear clustering.
 */

namespace {
struct LinearClusterPoint {
    double point;
    int originalIndex;
};

static inline bool clusterSortCompare(const LinearClusterPoint &a, const LinearClusterPoint &b)
{
    return a.point < b.point;
}
}

static const double zeroEpsilon = 1E-10;

QVector<int> LinearCluster::cluster(const QVector<double> &input,
                                    QVector<double> *breaks,
                                    double minimumCombineDistance,
                                    double maximumClusterWidth,
                                    double expandCombineDistanceFactor)
{
    Q_ASSERT(FP::defined(minimumCombineDistance) && minimumCombineDistance >= 0.0);
    Q_ASSERT(!FP::defined(maximumClusterWidth) ||
                     (maximumClusterWidth >= 0.0 || maximumClusterWidth <= -1.0));
    Q_ASSERT(FP::defined(expandCombineDistanceFactor) && expandCombineDistanceFactor >= 1.0);

    if (breaks != NULL)
        breaks->clear();
    std::vector<LinearClusterPoint> sorted;
    sorted.reserve(input.size());
    for (QVector<double>::const_iterator add = input.constBegin(), end = input.constEnd();
            add != end;
            ++add) {
        if (!FP::defined(*add))
            continue;
        LinearClusterPoint p;
        p.point = *add;
        p.originalIndex = add - input.constBegin();
        sorted.push_back(p);
    }
    std::sort(sorted.begin(), sorted.end(), clusterSortCompare);
    QVector<int> result(input.size(), -1);
    if (sorted.empty())
        return result;
    if (sorted.size() == 1) {
        result[sorted[0].originalIndex] = 0;
        return result;
    }

    if (!FP::defined(maximumClusterWidth)) {
        maximumClusterWidth = sorted.end()->point - sorted.begin()->point;
    } else if (maximumClusterWidth < 0.0) {
        maximumClusterWidth = minimumCombineDistance * (-maximumClusterWidth);
    }

    std::vector<LinearClusterPoint>::iterator endPoints = sorted.end();
    std::vector<LinearClusterPoint>::iterator clusterPoint = sorted.begin();
    int binID = 0;
    while (clusterPoint != endPoints) {
        double maximumAllowedDistance = minimumCombineDistance;
        double totalDistance = 0;
        double epsilon;
        double distance;

        epsilon = fabs(clusterPoint->point);
        if (epsilon == 0.0)
            epsilon = zeroEpsilon;
        else
            epsilon *= zeroEpsilon;

        result[clusterPoint->originalIndex] = binID;
        for (++clusterPoint; clusterPoint != endPoints; ++clusterPoint) {
            distance = clusterPoint->point - (clusterPoint - 1)->point;
            if (distance < epsilon)
                distance = 0.0;
            totalDistance += distance;
            if (distance > maximumAllowedDistance || totalDistance > maximumClusterWidth) {
                ++binID;
                if (breaks != NULL) {
                    breaks->append((clusterPoint->point + (clusterPoint - 1)->point) * 0.5);
                }
                break;
            }

            result[clusterPoint->originalIndex] = binID;
            maximumAllowedDistance =
                    qMax(maximumAllowedDistance, distance * expandCombineDistanceFactor);
        }
    }

    return result;
}

}
}
