/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVector>
#include <QLoggingCategory>

#include "algorithms/fourier.hxx"


#ifndef M_PI
#define M_PI    3.14159265358979
#endif


Q_LOGGING_CATEGORY(log_algorithms_fourier, "cpd3.algorithms.fourier", QtWarningMsg)

extern "C" {
int CPD3ALGORITHMS_internal_fft_work(double *a,
                                     double *b,
                                     int nseg,
                                     int n,
                                     int nspn,
                                     int isn,
                                     double *work,
                                     int *iwork,
                                     int m_fac,
                                     int kt,
                                     int maxf,
                                     int *nfac);
void CPD3ALGORITHMS_internal_fft_factor
        (int n, int *pmaxf, int *pmaxp, int nfac[20], int *pm_fac, int *pkt);
}

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/fourier.hxx
 * Provides routines for generating Fourier transforms.
 */

static inline void butterflyRadix2(std::complex<double> &i,
                                   std::complex<double> &j,
                                   const std::complex<double> &w)
{
    std::complex<double> t(w * j);
    j = i - t;
    i += t;
}

static void danielsonLanzcos(QVector<std::complex<double> > &points)
{
    for (int N = 1, max = points.size(); N < max; N *= 2) {
        for (int k = 0; k < N; k++) {
            double t = -1.0 * M_PI * (double) k / (double) N;
            std::complex<double> w(cos(t), sin(t));
            for (int instance = k, inc = 2 * N; instance < max; instance += inc) {
                butterflyRadix2(points[instance], points[instance + N], w);
            }
        }
    }
}

static void callRFFT(QVector<std::complex<double> > &points, bool inverse)
{
    int maxf = 0;
    int maxp = 0;
    int nfac[20];
    int m_fac = 0;
    int kt = 0;
    CPD3ALGORITHMS_internal_fft_factor(points.size(), &maxf, &maxp, nfac, &m_fac, &kt);
    if (maxf == 0) {
        qCDebug(log_algorithms_fourier) << "Factorization of" << points.size() << "failed";
        points.clear();
        return;
    }

    double *work = (double *) malloc(4 * maxf * sizeof(double));
    Q_ASSERT(work);
    int *iwork = (int *) malloc(maxp * sizeof(int));
    Q_ASSERT(iwork);

    /* C++11 guarantees memory ordering of std::complex types, so we can avoid a 
     * duplication if we have it. */
#if !defined(__cplusplus) || __cplusplus < 201103L
    double *r = (double *) malloc(points.size() * sizeof(double));
    Q_ASSERT(r);
    double *i = (double *) malloc(points.size() * sizeof(double));
    Q_ASSERT(i);

    {
        double *rptr = r;
        double *iptr = i;
        for (QVector<std::complex<double> >::const_iterator add = points.constBegin(),
                end = points.constEnd(); add != end; ++add, rptr++, iptr++) {
            *rptr = std::real(*add);
            *iptr = std::imag(*add);
        }
    }

    CPD3ALGORITHMS_internal_fft_work(r, i, 1, points.size(), 1, inverse ? 1 : -1, work, iwork,
                                     m_fac, kt, maxf, nfac);
    {
        double *rptr = r;
        double *iptr = i;
        for (QVector<std::complex<double> >::iterator add = points.begin(), end = points.end();
                add != end;
                ++add, rptr++, iptr++) {
            *add = std::complex<double>(*rptr, *iptr);
        }
    }

    free(r);
    free(i);
#else
    double *r = reinterpret_cast<double *>(&points[0]);
    double *i = reinterpret_cast<double *>(&points[0]) + 1;
    CPD3ALGORITHMS_internal_fft_work(r, i, 1, points.size(), 1, 
            inverse ? 2 : -2, work, iwork, m_fac, kt, maxf, nfac);
#endif

    free(work);
    free(iwork);
}

/**
 * Compute the forward FFT of the given set of std::complex points.
 * 
 * @param points    the input and output points
 */
void Fourier::forward(QVector<std::complex<double> > &points)
{
    if (points.size() <= 1)
        return;
    /* R implementation is still faster even with memory allocation. */
    if (0 && points.size() == 1 << (int) (31 - INTEGER::clz32(points.size()))) {
        bitReverse(points);
        danielsonLanzcos(points);
    } else {
        callRFFT(points, false);
    }
}

/**
 * Compute the forward FFT of std::real valued data.
 * 
 * @param points    the input points
 * @return          the computed std::complex points
 */
QVector<std::complex<double> > Fourier::forward(const QVector<double> &points)
{
    if (points.size() <= 1)
        return QVector<std::complex<double> >();
    QVector<std::complex<double> > seq;
    seq.reserve(points.size());
    for (QVector<double>::const_iterator add = points.constBegin(), end = points.constEnd();
            add != end;
            ++add) {
        seq.append(std::complex<double>(*add, 0));
    }
    forward(seq);
    return seq;
}

/**
 * Compute the inverse FFT of the given set of std::complex points.
 * 
 * @param points    the input and output points
 */
void Fourier::inverse(QVector<std::complex<double> > &points)
{
    if (points.size() <= 1)
        return;
    /* R implementation is still faster even with memory allocation. */
    if (0 && points.size() == 1 << (int) (31 - INTEGER::clz32(points.size()))) {
        for (QVector<std::complex<double> >::iterator p = points.begin(), endP = points.end();
                p != endP;
                ++p) {
            *p = std::complex<double>(std::imag(*p), std::real(*p));
        }
        bitReverse(points);
        danielsonLanzcos(points);
        double d = (double) points.size();
        for (QVector<std::complex<double> >::iterator p = points.begin(), endP = points.end();
                p != endP;
                ++p) {
            *p = std::complex<double>(std::imag(*p) / d, std::real(*p) / d);
        }
    } else {
        callRFFT(points, true);
        double d = (double) points.size();
        for (QVector<std::complex<double> >::iterator p = points.begin(), endP = points.end();
                p != endP;
                ++p) {
            *p /= d;
        }
    }
}

/**
 * Compute the inverse FFT with a pure std::real output.  Note that this does NOT
 * enforce the symmetry mirroring that produces std::real output.
 * 
 * @param points    the input points
 * @return          the inverse
 */
QVector<double> Fourier::inverseReal(QVector<std::complex<double> > points)
{
    inverse(points);
    QVector<double> result;
    result.reserve(points.size());
    for (QVector<std::complex<double> >::const_iterator p = points.constBegin(),
            endP = points.constEnd(); p != endP; ++p) {
        result.append(std::real(*p));
    }
    return result;
}

template<typename PointType>
static QVector<std::complex<double> > powerSpectrumFFT(const QVector<PointType> &points,
                                                       double &nyquist)
{
    QVector<std::complex<double> > result;
    if (points.size() <= 2)
        return result;

    result.reserve(points.size());
    double sumInterval = 0.0;
    int countInterval = 0;
    double lastTime = FP::undefined();
    for (typename QVector<PointType>::const_iterator p = points.constBegin(),
            endP = points.constEnd(); p != endP; ++p) {
        if ((*p)->totalDimensions() < 2) {
            double v = (*p)->get(0);
            if (!FP::defined(v))
                continue;
            result.append(std::complex<double>(v, 0.0));
        } else {
            double t = (*p)->get(0);
            if (FP::defined(t) && FP::defined(lastTime)) {
                sumInterval += fabs(t - lastTime);
                countInterval++;
            }
            lastTime = t;

            double v = (*p)->get(1);
            if (!FP::defined(v))
                continue;
            result.append(std::complex<double>(v, 0.0));
        }
    }
    Fourier::forward(result);

    if (countInterval > 0 && sumInterval > 0.0) {
        double interval = sumInterval / (double) countInterval;
        nyquist = 1.0 / (2.0 * interval);
    }

    return result;
}

static inline double toSpectralLine(const std::complex<double> &input)
{ return std::abs(input); }

static inline double toFrequency(int freq, int maxFreq, double nyquist)
{ return (nyquist * maxFreq) / freq; }

template<typename PointType>
static void transformToSpectrumInner(QVector<PointType> &points,
                                     TransformerAllocator *allocator,
                                     const Transformer *tr,
                                     typename QVector<PointType>::iterator &eraseBegin)
{
    double nyquist = 0.0;
    QVector<std::complex<double> > pwr(powerSpectrumFFT(points, nyquist));

    int freq = 1;
    int maxFreq = pwr.size() / 2;
    typename QVector<PointType>::iterator p = points.begin();
    QVector<std::complex<double> >::const_iterator sp = pwr.constBegin();
    for (typename QVector<PointType>::iterator endPoints = points.end();
            p != endPoints && freq <= maxFreq;
            ++p, ++freq, ++sp) {

        double spectral = toSpectralLine(*sp);

        if ((*p)->totalDimensions() < 2) {
            (*p)->set(0, spectral);
        } else {
            (*p)->set(0, toFrequency(freq, maxFreq, nyquist));
            (*p)->set(1, spectral);
        }
    }

    for (; freq <= maxFreq; ++freq, ++sp) {
        double spectral = toSpectralLine(*sp);

        PointType np(allocator->create(tr));
        if (np->totalDimensions() < 2) {
            np->set(0, spectral);
        } else {
            np->set(0, toFrequency(freq, maxFreq, nyquist));
            np->set(1, spectral);
        }
        points.append(np);
    }

    eraseBegin = p;
}

static void transformToSpectrum(QVector<std::shared_ptr<TransformerPoint> > &points,
                                TransformerAllocator *allocator,
                                const Transformer *tr)
{
    QVector<std::shared_ptr<TransformerPoint> >::iterator eraseBegin;
    transformToSpectrumInner(points, allocator, tr, eraseBegin);
    if (eraseBegin != points.end()) {
        points.erase(eraseBegin, points.end());
    }
}

static void transformToSpectrum(QVector<TransformerPoint *> &points,
                                TransformerAllocator *allocator,
                                const Transformer *tr)
{
    QVector<TransformerPoint *>::iterator eraseBegin;
    transformToSpectrumInner(points, allocator, tr, eraseBegin);
    if (eraseBegin != points.end()) {
        for (QVector<TransformerPoint *>::iterator endPoints = points.end(), delP = eraseBegin;
                delP != endPoints;
                ++delP) {
            delete *delP;
        }
        points.erase(eraseBegin, points.end());
    }
}

FourierPowerSpectrum::FourierPowerSpectrum()
{ }

FourierPowerSpectrum::~FourierPowerSpectrum()
{ }

void FourierPowerSpectrum::applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                                      TransformerAllocator *allocator) const
{
    transformToSpectrum(points, allocator, this);
}

void FourierPowerSpectrum::apply(QVector<std::shared_ptr<TransformerPoint> > &points,
                                 TransformerAllocator *allocator)
{
    transformToSpectrum(points, allocator, this);
}

void FourierPowerSpectrum::applyConst(QVector<TransformerPoint *> &points,
                                      TransformerAllocator *allocator) const
{
    transformToSpectrum(points, allocator, this);
}

void FourierPowerSpectrum::apply(QVector<TransformerPoint *> &points,
                                 TransformerAllocator *allocator)
{
    transformToSpectrum(points, allocator, this);
}

Transformer *FourierPowerSpectrum::clone() const
{ return new FourierPowerSpectrum; }

FourierPowerSpectrum::FourierPowerSpectrum(QDataStream &stream)
{ Q_UNUSED(stream); }

void FourierPowerSpectrum::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Transformer::SerialType_FourierPowerSpectrum;
    stream << t;
}

void FourierPowerSpectrum::printLog(QDebug &stream) const
{ stream << "FourierPowerSpectrum"; }


}
}
