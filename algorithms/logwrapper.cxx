/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QStringList>

#include "algorithms/logwrapper.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/logwrapper.hxx
 * Provides routines for wrapping other models for log-log input/output.
 */

LogWrapper::~LogWrapper()
{ delete child; }

/**
 * Create a new wrapper for the given model.  This takes ownership of the model.
 * 
 * @param wrap      the model to wrap
 * @param inBase    the input base
 * @param outBase   the output base
 */
LogWrapper::LogWrapper(Model *wrap, double inBase, double outBase) : child(wrap),
                                                                     inputBase(inBase),
                                                                     outputBase(outBase)
{
    Q_ASSERT(child != NULL);
    Q_ASSERT(FP::defined(inputBase) && inputBase > 0.0);
    Q_ASSERT(FP::defined(outputBase) && outputBase > 0.0);
    lInputBase = log(inputBase);
}

double LogWrapper::convertInput(double input) const
{
    if (!FP::defined(input)) return input;
    if (input > 0.0)
        return log(input) / lInputBase;
    return FP::undefined();
}

double LogWrapper::convertOutput(double output) const
{
    if (!FP::defined(output)) return output;
    return pow(outputBase, output);
}

QVector<double> LogWrapper::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    result.reserve(inputs.size());
    for (QVector<double>::const_iterator it = inputs.constBegin(), end = inputs.constEnd();
            it != end;
            ++it) {
        result.append(convertInput(*it));
    }
    result = child->applyConst(result);
    for (QVector<double>::iterator it = result.begin(), end = result.end(); it != end; ++it) {
        *it = convertOutput(*it);
    }
    return result;
}

QVector<double> LogWrapper::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    result.reserve(inputs.size());
    for (QVector<double>::const_iterator it = inputs.constBegin(), end = inputs.constEnd();
            it != end;
            ++it) {
        result.append(convertInput(*it));
    }
    result = child->apply(result);
    for (QVector<double>::iterator it = result.begin(), end = result.end(); it != end; ++it) {
        *it = convertOutput(*it);
    }
    return result;
}

void LogWrapper::applyIOConst(QVector<double> &values) const
{
    for (QVector<double>::iterator it = values.begin(), end = values.end(); it != end; ++it) {
        *it = convertInput(*it);
    }
    child->applyIOConst(values);
    for (QVector<double>::iterator it = values.begin(), end = values.end(); it != end; ++it) {
        *it = convertOutput(*it);
    }
}

void LogWrapper::applyIO(QVector<double> &values)
{
    for (QVector<double>::iterator it = values.begin(), end = values.end(); it != end; ++it) {
        *it = convertInput(*it);
    }
    child->applyIO(values);
    for (QVector<double>::iterator it = values.begin(), end = values.end(); it != end; ++it) {
        *it = convertOutput(*it);
    }
}

double LogWrapper::oneToOneConst(double input) const
{ return convertOutput(child->oneToOneConst(convertInput(input))); }

double LogWrapper::oneToOne(double input)
{ return convertOutput(child->oneToOne(convertInput(input))); }

double LogWrapper::twoToOneConst(double a, double b) const
{
    return convertOutput(child->twoToOneConst(convertInput(a), convertInput(b)));
}

double LogWrapper::twoToOne(double a, double b)
{
    return convertOutput(child->twoToOne(convertInput(a), convertInput(b)));
}

double LogWrapper::threeToOneConst(double a, double b, double c) const
{
    return convertOutput(child->threeToOneConst(convertInput(a), convertInput(b), convertInput(c)));
}

double LogWrapper::threeToOne(double a, double b, double c)
{
    return convertOutput(child->threeToOne(convertInput(a), convertInput(b), convertInput(c)));
}

void LogWrapper::applyIOConst(double &a, double &b) const
{
    a = convertInput(a);
    b = convertInput(b);
    child->applyIOConst(a, b);
    a = convertOutput(a);
    b = convertOutput(b);
}

void LogWrapper::applyIO(double &a, double &b)
{
    a = convertInput(a);
    b = convertInput(b);
    child->applyIO(a, b);
    a = convertOutput(a);
    b = convertOutput(b);
}

void LogWrapper::applyIOConst(double &a, double &b, double &c) const
{
    a = convertInput(a);
    b = convertInput(b);
    c = convertInput(c);
    child->applyIOConst(a, b, c);
    a = convertOutput(a);
    b = convertOutput(b);
    c = convertOutput(c);
}

void LogWrapper::applyIO(double &a, double &b, double &c)
{
    a = convertInput(a);
    b = convertInput(b);
    c = convertInput(c);
    child->applyIO(a, b, c);
    a = convertOutput(a);
    b = convertOutput(b);
    c = convertOutput(c);
}

Model *LogWrapper::clone() const
{ return new LogWrapper(child->clone(), inputBase, outputBase); }

QString LogWrapper::describe() const
{
    return QString("log%1-log%2-%3").arg(QString::number(inputBase), QString::number(outputBase),
                                         child->describe());
}

ModelLegendEntry LogWrapper::legend(const ModelLegendParameters &parameters) const
{
    ModelLegendEntry le(child->legend(parameters));
    QString line(le.getLine());
    if (!line.isEmpty()) {
        le.setLine(QObject::tr("%1 (%2^f[log%3(x)])", "model log-log legend").arg(line,
                                                                                  NumberFormat(1, 0)
                                                                                          .apply(outputBase),
                                                                                  NumberFormat(1, 0)
                                                                                          .subscript(
                                                                                                  inputBase)));
    }
    return le;
}

LogWrapper::LogWrapper(QDataStream &stream)
{
    stream >> inputBase >> outputBase >> child;
    Q_ASSERT(FP::defined(inputBase) && inputBase > 0.0);
    Q_ASSERT(FP::defined(outputBase) && outputBase > 0.0);
    lInputBase = log(inputBase);
}

void LogWrapper::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_LogLog;
    stream << t << inputBase << outputBase << child;
}

void LogWrapper::printLog(QDebug &stream) const
{
    stream << "LogWrapper(" << inputBase << ',' << outputBase << ',' << child << ')';
}

Model *LogWrapper::create(Model *model, double inBase, double outBase)
{
    if (!FP::defined(inBase) || inBase <= 0.0) {
        if (!FP::defined(outBase) || outBase <= 0.0)
            return model;
        return new LogOut(model, outBase);
    } else if (!FP::defined(outBase) || outBase <= 0.0) {
        return new LogIn(model, inBase);
    } else {
        return new LogWrapper(model, inBase, outBase);
    }
}


LogWrapper::LogIn::~LogIn()
{ delete child; }

LogWrapper::LogIn::LogIn(Model *wrap, double inBase) : child(wrap), inputBase(inBase)
{
    Q_ASSERT(child != NULL);
    Q_ASSERT(FP::defined(inputBase) && inputBase > 0.0);
    lInputBase = log(inputBase);
}

double LogWrapper::LogIn::convertInput(double input) const
{
    if (!FP::defined(input)) return input;
    if (input > 0.0)
        return log(input) / lInputBase;
    return FP::undefined();
}

QVector<double> LogWrapper::LogIn::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    result.reserve(inputs.size());
    for (QVector<double>::const_iterator it = inputs.constBegin(), end = inputs.constEnd();
            it != end;
            ++it) {
        result.append(convertInput(*it));
    }
    return child->applyConst(result);
}

QVector<double> LogWrapper::LogIn::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    result.reserve(inputs.size());
    for (QVector<double>::const_iterator it = inputs.constBegin(), end = inputs.constEnd();
            it != end;
            ++it) {
        result.append(convertInput(*it));
    }
    return child->apply(result);
}

void LogWrapper::LogIn::applyIOConst(QVector<double> &values) const
{
    for (QVector<double>::iterator it = values.begin(), end = values.end(); it != end; ++it) {
        *it = convertInput(*it);
    }
    child->applyIOConst(values);
}

void LogWrapper::LogIn::applyIO(QVector<double> &values)
{
    for (QVector<double>::iterator it = values.begin(), end = values.end(); it != end; ++it) {
        *it = convertInput(*it);
    }
    child->applyIO(values);
}

double LogWrapper::LogIn::oneToOneConst(double input) const
{ return child->oneToOneConst(convertInput(input)); }

double LogWrapper::LogIn::oneToOne(double input)
{ return child->oneToOne(convertInput(input)); }

double LogWrapper::LogIn::twoToOneConst(double a, double b) const
{
    return child->twoToOneConst(convertInput(a), convertInput(b));
}

double LogWrapper::LogIn::twoToOne(double a, double b)
{
    return child->twoToOne(convertInput(a), convertInput(b));
}

double LogWrapper::LogIn::threeToOneConst(double a, double b, double c) const
{
    return child->threeToOneConst(convertInput(a), convertInput(b), convertInput(c));
}

double LogWrapper::LogIn::threeToOne(double a, double b, double c)
{
    return child->threeToOne(convertInput(a), convertInput(b), convertInput(c));
}

void LogWrapper::LogIn::applyIOConst(double &a, double &b) const
{
    a = convertInput(a);
    b = convertInput(b);
    child->applyIOConst(a, b);
}

void LogWrapper::LogIn::applyIO(double &a, double &b)
{
    a = convertInput(a);
    b = convertInput(b);
    child->applyIO(a, b);
}

void LogWrapper::LogIn::applyIOConst(double &a, double &b, double &c) const
{
    a = convertInput(a);
    b = convertInput(b);
    c = convertInput(c);
    child->applyIOConst(a, b, c);
}

void LogWrapper::LogIn::applyIO(double &a, double &b, double &c)
{
    a = convertInput(a);
    b = convertInput(b);
    c = convertInput(c);
    child->applyIO(a, b, c);
}

Model *LogWrapper::LogIn::clone() const
{ return new LogIn(child->clone(), inputBase); }

QString LogWrapper::LogIn::describe() const
{
    return QString("log%1-%2").arg(QString::number(inputBase), child->describe());
}

ModelLegendEntry LogWrapper::LogIn::legend(const ModelLegendParameters &parameters) const
{
    ModelLegendEntry le(child->legend(parameters));
    QString line(le.getLine());
    if (!line.isEmpty()) {
        le.setLine(QObject::tr("%1 (f[log%2(x)])", "model log input legend").arg(line,
                                                                                 NumberFormat(1,
                                                                                              0).subscript(
                                                                                         inputBase)));
    }
    return le;
}

LogWrapper::LogIn::LogIn(QDataStream &stream)
{
    stream >> inputBase >> child;
    Q_ASSERT(FP::defined(inputBase) && inputBase > 0.0);
    lInputBase = log(inputBase);
}

void LogWrapper::LogIn::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_LogIn;
    stream << t << inputBase << child;
}

void LogWrapper::LogIn::printLog(QDebug &stream) const
{
    stream << "LogWrapper(" << inputBase << ",," << child << ")";
}


LogWrapper::LogOut::~LogOut()
{ delete child; }

LogWrapper::LogOut::LogOut(Model *wrap, double outBase) : child(wrap), outputBase(outBase)
{
    Q_ASSERT(child != NULL);
    Q_ASSERT(FP::defined(outputBase) && outputBase > 0.0);
}

double LogWrapper::LogOut::convertOutput(double output) const
{
    if (!FP::defined(output)) return output;
    return pow(outputBase, output);
}

QVector<double> LogWrapper::LogOut::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result(child->applyConst(inputs));
    for (QVector<double>::iterator it = result.begin(), end = result.end(); it != end; ++it) {
        *it = convertOutput(*it);
    }
    return result;
}

QVector<double> LogWrapper::LogOut::apply(const QVector<double> &inputs)
{
    QVector<double> result(child->apply(inputs));
    for (QVector<double>::iterator it = result.begin(), end = result.end(); it != end; ++it) {
        *it = convertOutput(*it);
    }
    return result;
}

void LogWrapper::LogOut::applyIOConst(QVector<double> &values) const
{
    child->applyIOConst(values);
    for (QVector<double>::iterator it = values.begin(), end = values.end(); it != end; ++it) {
        *it = convertOutput(*it);
    }
}

void LogWrapper::LogOut::applyIO(QVector<double> &values)
{
    child->applyIO(values);
    for (QVector<double>::iterator it = values.begin(), end = values.end(); it != end; ++it) {
        *it = convertOutput(*it);
    }
}

double LogWrapper::LogOut::oneToOneConst(double input) const
{ return convertOutput(child->oneToOneConst(input)); }

double LogWrapper::LogOut::oneToOne(double input)
{ return convertOutput(child->oneToOne(input)); }

double LogWrapper::LogOut::twoToOneConst(double a, double b) const
{ return convertOutput(child->twoToOneConst(a, b)); }

double LogWrapper::LogOut::twoToOne(double a, double b)
{ return convertOutput(child->twoToOne(a, b)); }

double LogWrapper::LogOut::threeToOneConst(double a, double b, double c) const
{ return convertOutput(child->threeToOneConst(a, b, c)); }

double LogWrapper::LogOut::threeToOne(double a, double b, double c)
{ return convertOutput(child->threeToOne(a, b, c)); }

void LogWrapper::LogOut::applyIOConst(double &a, double &b) const
{
    child->applyIOConst(a, b);
    a = convertOutput(a);
    b = convertOutput(b);
}

void LogWrapper::LogOut::applyIO(double &a, double &b)
{
    child->applyIO(a, b);
    a = convertOutput(a);
    b = convertOutput(b);
}

void LogWrapper::LogOut::applyIOConst(double &a, double &b, double &c) const
{
    child->applyIOConst(a, b, c);
    a = convertOutput(a);
    b = convertOutput(b);
    c = convertOutput(c);
}

void LogWrapper::LogOut::applyIO(double &a, double &b, double &c)
{
    child->applyIO(a, b, c);
    a = convertOutput(a);
    b = convertOutput(b);
    c = convertOutput(c);
}

Model *LogWrapper::LogOut::clone() const
{ return new LogOut(child->clone(), outputBase); }

QString LogWrapper::LogOut::describe() const
{
    return QString("exp%1-%2").arg(QString::number(outputBase), child->describe());
}

ModelLegendEntry LogWrapper::LogOut::legend(const ModelLegendParameters &parameters) const
{
    ModelLegendEntry le(child->legend(parameters));
    QString line(le.getLine());
    if (!line.isEmpty()) {
        le.setLine(QObject::tr("%1 (%2^f[x])", "model log out legend").arg(line,
                                                                           NumberFormat(1, 0).apply(
                                                                                   outputBase)));
    }
    return le;
}

LogWrapper::LogOut::LogOut(QDataStream &stream)
{
    stream >> outputBase >> child;
    Q_ASSERT(FP::defined(outputBase) && outputBase > 0.0);
}

void LogWrapper::LogOut::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_LogOut;
    stream << t << outputBase << child;
}

void LogWrapper::LogOut::printLog(QDebug &stream) const
{
    stream << "LogWrapper(," << outputBase << ',' << child << ')';
}


LogWrapperMultiple::~LogWrapperMultiple()
{ delete child; }

/**
 * Create a new wrapper for the given model.  This takes ownership of the model.
 * 
 * @param wrap      the model to wrap
 * @param inBase    the input bases
 * @param outBase   the output bases
 */
LogWrapperMultiple::LogWrapperMultiple(Model *wrap,
                                       const QVector<double> &inBase,
                                       const QVector<double> &outBase) : child(wrap),
                                                                         inputBases(inBase),
                                                                         outputBases(outBase)
{
    Q_ASSERT(child != NULL);
    for (QVector<double>::const_iterator it = inputBases.constBegin(), end = inputBases.constEnd();
            it != end;
            ++it) {
        if (FP::defined(*it) && *it > 0.0)
            lInputBases.append(log(*it));
        else
            lInputBases.append(FP::undefined());
    }
}

double LogWrapperMultiple::convertInput(double input, int dim) const
{
    if (!FP::defined(input)) return input;
    if (dim >= lInputBases.size()) return input;
    double lBase = lInputBases.at(dim);
    if (!FP::defined(lBase)) return input;
    if (input > 0.0)
        return log(input) / lBase;
    return FP::undefined();
}

double LogWrapperMultiple::convertOutput(double output, int dim) const
{
    if (!FP::defined(output)) return output;
    if (dim >= outputBases.size()) return output;
    double lOutput = outputBases.at(dim);
    if (!FP::defined(lOutput)) return output;
    return pow(lOutput, output);
}

QVector<double> LogWrapperMultiple::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    result.reserve(inputs.size());
    for (QVector<double>::const_iterator it = inputs.constBegin(), end = inputs.constEnd();
            it != end;
            ++it) {
        result.append(convertInput(*it, result.size()));
    }
    result = child->applyConst(result);
    int i = 0;
    for (QVector<double>::iterator it = result.begin(), end = result.end(); it != end; ++it) {
        *it = convertOutput(*it, i++);
    }
    return result;
}

QVector<double> LogWrapperMultiple::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    result.reserve(inputs.size());
    for (QVector<double>::const_iterator it = inputs.constBegin(), end = inputs.constEnd();
            it != end;
            ++it) {
        result.append(convertInput(*it, result.size()));
    }
    result = child->apply(result);
    int i = 0;
    for (QVector<double>::iterator it = result.begin(), end = result.end(); it != end; ++it) {
        *it = convertOutput(*it, i++);
    }
    return result;
}

void LogWrapperMultiple::applyIOConst(QVector<double> &values) const
{
    int i = 0;
    for (QVector<double>::iterator it = values.begin(), end = values.end(); it != end; ++it) {
        *it = convertInput(*it, i++);
    }
    child->applyIOConst(values);
    i = 0;
    for (QVector<double>::iterator it = values.begin(), end = values.end(); it != end; ++it) {
        *it = convertOutput(*it, i++);
    }
}

void LogWrapperMultiple::applyIO(QVector<double> &values)
{
    int i = 0;
    for (QVector<double>::iterator it = values.begin(), end = values.end(); it != end; ++it) {
        *it = convertInput(*it, i++);
    }
    child->applyIO(values);
    i = 0;
    for (QVector<double>::iterator it = values.begin(), end = values.end(); it != end; ++it) {
        *it = convertOutput(*it, i++);
    }
}

double LogWrapperMultiple::oneToOneConst(double input) const
{ return convertOutput(child->oneToOneConst(convertInput(input, 0)), 0); }

double LogWrapperMultiple::oneToOne(double input)
{ return convertOutput(child->oneToOne(convertInput(input, 0)), 0); }

double LogWrapperMultiple::twoToOneConst(double a, double b) const
{
    return convertOutput(child->twoToOneConst(convertInput(a, 0), convertInput(b, 1)), 0);
}

double LogWrapperMultiple::twoToOne(double a, double b)
{
    return convertOutput(child->twoToOne(convertInput(a, 0), convertInput(b, 1)), 0);
}

double LogWrapperMultiple::threeToOneConst(double a, double b, double c) const
{
    return convertOutput(
            child->threeToOneConst(convertInput(a, 0), convertInput(b, 1), convertInput(c, 2)), 0);
}

double LogWrapperMultiple::threeToOne(double a, double b, double c)
{
    return convertOutput(
            child->threeToOne(convertInput(a, 0), convertInput(b, 1), convertInput(c, 2)), 0);
}

void LogWrapperMultiple::applyIOConst(double &a, double &b) const
{
    a = convertInput(a, 0);
    b = convertInput(b, 1);
    child->applyIOConst(a, b);
    a = convertOutput(a, 0);
    b = convertOutput(b, 1);
}

void LogWrapperMultiple::applyIO(double &a, double &b)
{
    a = convertInput(a, 0);
    b = convertInput(b, 1);
    child->applyIO(a, b);
    a = convertOutput(a, 0);
    b = convertOutput(b, 1);
}

void LogWrapperMultiple::applyIOConst(double &a, double &b, double &c) const
{
    a = convertInput(a, 0);
    b = convertInput(b, 1);
    c = convertInput(c, 2);
    child->applyIOConst(a, b, c);
    a = convertOutput(a, 0);
    b = convertOutput(b, 1);
    c = convertOutput(c, 2);
}

void LogWrapperMultiple::applyIO(double &a, double &b, double &c)
{
    a = convertInput(a, 0);
    b = convertInput(b, 1);
    c = convertInput(c, 2);
    child->applyIO(a, b, c);
    a = convertOutput(a, 0);
    b = convertOutput(b, 1);
    c = convertOutput(c, 2);
}

Model *LogWrapperMultiple::clone() const
{ return new LogWrapperMultiple(child->clone(), inputBases, outputBases); }

QString LogWrapperMultiple::describe() const
{
    QStringList listInBases;
    for (QVector<double>::const_iterator it = inputBases.constBegin(), end = inputBases.constEnd();
            it != end;
            ++it) {
        if (FP::defined(*it))
            listInBases.append(QString::number(*it));
        else
            listInBases.append("");
    }
    QStringList listOutBases;
    for (QVector<double>::const_iterator it = outputBases.constBegin(),
            end = outputBases.constEnd(); it != end; ++it) {
        if (FP::defined(*it))
            listOutBases.append(QString::number(*it));
        else
            listOutBases.append("");
    }
    return QString("log%1-log%2-%3").arg(listInBases.join(","), listOutBases.join(","),
                                         child->describe());
}

ModelLegendEntry LogWrapperMultiple::legend(const ModelLegendParameters &parameters) const
{
    ModelLegendEntry le(child->legend(parameters));
    QString line(le.getLine());
    if (!line.isEmpty()) {
        for (int i = 0, max = qMax(inputBases.size(), outputBases.size()); i < max; i++) {
            if (i < inputBases.size() &&
                    i < outputBases.size() &&
                    FP::defined(inputBases.at(i)) &&
                    FP::defined(outputBases.at(i))) {
                line.append(QObject::tr(" (%1^f[log%2(x)])", "model log-log multiple legend").arg(
                        NumberFormat(1, 0).apply(outputBases.at(i)),
                        NumberFormat(1, 0).subscript(inputBases.at(i))));
            } else if (i < inputBases.size() && FP::defined(inputBases.at(i))) {
                line.append(QObject::tr(" (f[log%1(x)])", "model log input multiple legend").arg(
                        NumberFormat(1, 0).subscript(inputBases.at(i))));
            } else if (FP::defined(outputBases.at(i))) {
                line.append(QObject::tr(" (%1^f[x])", "model log output multiple legend").arg(
                        NumberFormat(1, 0).apply(outputBases.at(i))));
            }
        }
        le.setLine(line);
    }
    return le;
}

LogWrapperMultiple::LogWrapperMultiple(QDataStream &stream)
{
    stream >> inputBases >> outputBases >> child;
    lInputBases.reserve(inputBases.size());
    for (QVector<double>::const_iterator it = inputBases.constBegin(), end = inputBases.constEnd();
            it != end;
            ++it) {
        if (FP::defined(*it) && *it > 0.0)
            lInputBases.append(log(*it));
        else
            lInputBases.append(FP::undefined());
    }
}

void LogWrapperMultiple::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_LogLogMultiple;
    stream << t << inputBases << outputBases << child;
}

void LogWrapperMultiple::printLog(QDebug &stream) const
{
    stream << "LogWrapperMultiple([" << inputBases << "],[" << outputBases << "]," << child << ')';
}

Model *LogWrapperMultiple::create(Model *model,
                                  const QVector<double> &inputBases,
                                  const QVector<double> &outputBases)
{
    bool anyValid = false;
    for (QVector<double>::const_iterator check = inputBases.constBegin(),
            endCheck = inputBases.constEnd(); check != endCheck; ++check) {
        if (!FP::defined(*check) || *check <= 0.0)
            continue;
        anyValid = true;
        break;
    }
    if (!anyValid) {
        for (QVector<double>::const_iterator check = outputBases.constBegin(),
                endCheck = outputBases.constEnd(); check != endCheck; ++check) {
            if (!FP::defined(*check) || *check <= 0.0)
                continue;
            anyValid = true;
            break;
        }
        if (!anyValid)
            return model;
    }

    if (inputBases.size() == 1 && outputBases.size() == 1)
        return LogWrapper::create(model, inputBases.at(0), outputBases.at(0));
    if (inputBases.size() == 1 && outputBases.size() == 0)
        return LogWrapper::create(model, inputBases.at(0), FP::undefined());
    if (inputBases.size() == 0 && outputBases.size() == 1)
        return LogWrapper::create(model, FP::undefined(), outputBases.at(0));
    if (inputBases.size() == 0 && outputBases.size() == 0)
        return model;

    if (inputBases.size() == outputBases.size()) {
        bool missed = false;
        for (QVector<double>::const_iterator in = inputBases.constBegin() + 1,
                out = outputBases.constBegin() + 1, end = inputBases.constEnd();
                in != end;
                ++in, ++out) {
            if (!FP::equal(*in, *(in - 1)) || !FP::equal(*out, *(out - 1))) {
                missed = true;
                break;
            }
        }
        if (!missed) {
            return LogWrapper::create(model, inputBases.at(0), outputBases.at(0));
        }
    }

    return new LogWrapperMultiple(model, inputBases, outputBases);
}


}
}
