/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "algorithms/rayleigh.hxx"
#include "algorithms/linearint.hxx"

#ifndef M_PI
#define M_PI    3.14159265358979
#endif

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/rayleigh.hxx
 * Rayleigh scattering calculations.
 * <br>
 * @see http://www.opticsinfobase.org/ao/abstract.cfm?uri=ao-34-15-2765 (Applied Optics, Vol. 34, Issue 15, pp. 2765-2773 (1995) )
 */

/* Constants are from Table 3 in Bucholtz 1995 with conversion to Mm-1 and 
 * changed to 0C, used with eq (8) */
static double airRayleigh(double wavelength)
{
    wavelength *= 1E-3;
    if (wavelength < 0.5) {
        static const double A = 7.68246E-4 * 1E3 * (288.15 / 273.15);
        static const double B = 3.55212;
        static const double C = 1.35579;
        static const double D = 0.11563;
        return A * pow(wavelength, -1.0 * (B + C * wavelength + D / wavelength));
    } else {
        static const double A = 10.21675E-4 * 1E3 * (288.15 / 273.15);
        static const double B = 3.99668;
        static const double C = 1.10298E-3;
        static const double D = 2.71393E-2;
        return A * pow(wavelength, -1.0 * (B + C * wavelength + D / wavelength));
    }
}

/* Constants from Table 1 in Bucholtz 1995.  No analytic form for gamma 
 * given, so just do linear interpolation on them. */
static double wavelengthGamma(double wavelength)
{
    static const double gammaWL[] =
            {200, 205, 210, 215, 220, 225, 230, 240, 250, 260, 270, 280, 290, 300, 310, 320, 330,
             340, 350, 360, 370, 380, 390, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900,
             950, 1000};
    static const double gammaValues[] =
            {2.326E-2, 2.241E-2, 2.156E-2, 2.100E-2, 2.043E-2, 1.986E-2, 1.930E-2, 1.872E-2,
             1.815E-2, 1.758E-2, 1.729E-2, 1.672E-2, 1.643E-2, 1.614E-2, 1.614E-2, 1.586E-2,
             1.557E-2, 1.557E-2, 1.528E-2, 1.528E-2, 1.528E-2, 1.499E-2, 1.499E-2, 1.499E-2,
             1.471E-2, 1.442E-2, 1.442E-2, 1.413E-2, 1.413E-2, 1.413E-2, 1.413E-2, 1.384E-2,
             1.384E-2, 1.384E-2, 1.384E-2, 1.384E-2};
    Q_ASSERT((sizeof(gammaWL) / sizeof(gammaWL[0])) ==
                     (sizeof(gammaValues) / sizeof(gammaValues[0])));
    return LinearInterpolator::interpolate(gammaWL, &gammaWL[sizeof(gammaWL) / sizeof(gammaWL[0])],
                                           gammaValues, wavelength);
}

/* This is eq (12) in Bucholtz 1995. */
static double phaseFunction(double gamma, double angle)
{
    double den = 4.0 * (1.0 + 2.0 * gamma);
    Q_ASSERT(den != 0.0);
    double cs = cos(angle);
    return (3.0 / den) * ((1.0 + 3.0 * gamma) + (1.0 - gamma) * cs * cs);
}

static double stpFactor(double t, double p)
{
    if (!FP::defined(t) || !FP::defined(p))
        return FP::undefined();
    if (p < 10.0 || p > 2000.0)
        return FP::undefined();
    if (t < -100.0)
        return FP::undefined();
    else if (t < 150.0)
        t += 273.15;
    else if (t > 400.0)
        return FP::undefined();
    return (p / 1013.25) * (273.15 / t);
}

/* I'm not actually sure of the origin of these, but I got them from the
 * Ecotech Neph manual */
#define F_CO2       2.61
#define F_FM200     15.3
#define F_SF6       6.74
#define F_R12       15.31
#define F_R22       7.53
#define F_R134      7.35

double Rayleigh::scattering(double wavelength, Gas type, double t, double p)
{
    if (!FP::defined(wavelength))
        return FP::undefined();
    double stp = stpFactor(t, p);
    if (!FP::defined(stp))
        return FP::undefined();

    double bs = airRayleigh(wavelength);
    if (!FP::defined(bs))
        return FP::undefined();

    switch (type) {
    case Air:
        return bs * stp;
    case CO2:
        return bs * stp * F_CO2;
    case FM200:
        return bs * stp * F_FM200;
    case SF6:
        return bs * stp * F_SF6;
    case R12:
        return bs * stp * F_R12;
    case R22:
        return bs * stp * F_R22;
    case R134:
        return bs * stp * F_R134;
    }
    return FP::undefined();
}

double Rayleigh::angleScattering(double wavelength,
                                 double startAngle,
                                 double endAngle,
                                 Gas type,
                                 double t,
                                 double p)
{
    if (!FP::defined(wavelength))
        return FP::undefined();
    if (!FP::defined(startAngle) || !FP::defined(endAngle))
        return FP::undefined();
    if (startAngle >= endAngle)
        return FP::undefined();
    double stp = stpFactor(t, p);
    if (!FP::defined(stp))
        return FP::undefined();

    double bs = airRayleigh(wavelength);
    if (!FP::defined(bs))
        return FP::undefined();
    double gamma = wavelengthGamma(wavelength);
    if (!FP::defined(gamma))
        return FP::undefined();

    startAngle *= M_PI / 180.0;
    endAngle *= M_PI / 180.0;
    double deltaAngle = endAngle - startAngle;
    int nSteps = (int) ceil(deltaAngle / ((M_PI / 180.0)));
    if (nSteps < 100)
        nSteps = 100;
    double angleStep = deltaAngle / (double) nSteps;

    /* Trapezoidal integration */
    double sum = phaseFunction(gamma, startAngle) + phaseFunction(gamma, endAngle);
    for (int i = 1; i < nSteps; i++) {
        double angle = startAngle + angleStep * (double) i;
        sum += 2.0 * phaseFunction(gamma, angle) * sin(angle);
    }
    sum *= deltaAngle / ((double) (2 * nSteps));

    /* Eq (14) canceled with the 2*pi from the phase integration */
    sum *= bs / 2.0;

    switch (type) {
    case Air:
        return sum * stp;
    case CO2:
        return sum * stp * F_CO2;
    case FM200:
        return sum * stp * F_FM200;
    case SF6:
        return sum * stp * F_SF6;
    case R12:
        return sum * stp * F_R12;
    case R22:
        return sum * stp * F_R22;
    case R134:
        return sum * stp * F_R134;
    }
    return FP::undefined();
}


Rayleigh::Gas Rayleigh::gasFromString(const QString &string)
{
    QString test(string.toLower());
    if (test == "co2") {
        return CO2;
    } else if (test == "fm200") {
        return FM200;
    } else if (test == "sf6") {
        return SF6;
    } else if (test == "r12") {
        return R12;
    } else if (test == "r22") {
        return R22;
    } else if (test == "r134") {
        return R134;
    }
    return Air;
}

QString Rayleigh::gasToString(Gas gas)
{
    switch (gas) {
    case Air:
        return "Air";
    case CO2:
        return "CO2";
    case FM200:
        return "FM200";
    case SF6:
        return "SF6";
    case R12:
        return "R12";
    case R22:
        return "R22";
    case R134:
        return "R134";
    }
    Q_ASSERT(false);
    return QString();
}

}
}
