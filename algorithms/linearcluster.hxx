/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSLINEARCLUSTER_H
#define CPD3ALGORITHMSLINEARCLUSTER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QVector>
#include "algorithms/algorithms.hxx"


namespace CPD3 {
namespace Algorithms {


/**
 * A simple linear point clustering implementation.  This clusters points
 * in a single dimension into groups, deciding the number of groups needed
 * on the fly.
 */
class CPD3ALGORITHMS_EXPORT LinearCluster {
public:
    /**
     * Apply point clustering to given set of input points.
     * 
     * @param input     the input point list
     * @param breaks    the output list of midpoint breaks in the same order as the bin IDs or NULL
     * @param minimumCombineDistance the minimum distance to always combine points into a cluster
     * @param maximumClusterWidth the total maximum width of a cluster, undefined for none or negative for a multiplier of the minimum width
     * @param expandCombineDistanceFactor the factor to expand the minimum distance by
     * @return          the remapped list of cluster IDs (starting from zero) that the points map to, undefined points are mapped to bin -1
     */
    static QVector<int> cluster(const QVector<double> &input,
                                QVector<double> *breaks = NULL,
                                double minimumCombineDistance = 0.1,
                                double maximumClusterWidth = -3.0,
                                double expandCombineDistanceFactor = 2.0);
};


}
}

#endif
