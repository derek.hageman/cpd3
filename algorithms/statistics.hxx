/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSSTATISTICS_H
#define CPD3ALGORITHMSSTATISTICS_H

#include "core/first.hxx"

#include <math.h>
#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QVector>
#include "algorithms/algorithms.hxx"
#include "core/number.hxx"


namespace CPD3 {
namespace Algorithms {

class CPD3ALGORITHMS_EXPORT Statistics {
public:
    /**
     * Return the mean of the given points.
     * 
     * @param begin     the start of the points
     * @param end       the end of the points
     * @return          the mean
     */
    template<typename ForwardIterator>
    static double mean(ForwardIterator begin, ForwardIterator end)
    {
        double sum = 0;
        size_t count = 0;
        for (; begin != end; ++begin) {
            if (!FP::defined(*begin))
                continue;
            sum += *begin;
            ++count;
        }
        if (count == 0)
            return FP::undefined();
        return sum / (double) count;
    }

    /**
     * Return the mean of the given points.  This requires all points to
     * be defined.
     * 
     * @param begin     the start of the points
     * @param end       the end of the points
     * @return          the mean
     */
    template<typename ForwardIterator>
    static double meanDefined(ForwardIterator begin, ForwardIterator end)
    {
        double sum = 0;
        size_t count = 0;
        for (; begin != end; ++begin) {
            Q_ASSERT(FP::defined(*begin));
            sum += *begin;
            ++count;
        }
        if (count == 0)
            return FP::undefined();
        return sum / (double) count;
    }

    /**
     * Return the mean of the given points.
     * 
     * @param points    the points
     * @return          the mean
     */
    template<typename ListType>
    static inline double mean(const ListType &points)
    { return mean(points.begin(), points.end()); }

    /**
     * Return the mean of the given points.  This requires all points to
     * be defined.
     * 
     * @param points    the points
     * @return          the mean
     */
    template<typename ListType>
    static inline double meanDefined(const ListType &points)
    { return meanDefined(points.begin(), points.end()); }

    /**
     * Calculate standard deviation, given the mean and the sum of squares.
     * 
     * @param mean      the mean
     * @param sumSq     the sum of squares
     * @param n         the number of points
     * @return          the standard deviation
     */
    static double calculateSD(double mean, double sumSq, size_t n);

    /**
     * Get the mean and standard deviation of the given points.
     * 
     * @param begin     the start of the points
     * @param end       the end of the points
     * @param m         the output mean
     * @param s         the output standard deviation
     */
    template<typename ForwardIterator>
    static void meanSD(ForwardIterator begin, ForwardIterator end, double &m, double &s)
    {
        size_t n = 0;
        double sum = 0;
        double sumSq = 0;
        for (; begin != end; ++begin) {
            if (!FP::defined(*begin))
                continue;
            ++n;
            sum += *begin;
            sumSq += (*begin) * (*begin);
        }
        if (n == 0) {
            m = FP::undefined();
            s = FP::undefined();
            return;
        }
        m = sum / (double) n;
        s = calculateSD(m, sumSq, n);
    }

    /**
     * Get the mean and standard deviation of the given points.  This requires
     * that all points in the list are defined.
     * 
     * @param begin     the start of the points
     * @param end       the end of the points
     * @param m         the output mean
     * @param s         the output standard deviation
     */
    template<typename ForwardIterator>
    static void meanSDDefined(ForwardIterator begin, ForwardIterator end, double &m, double &s)
    {
        if (begin == end) {
            m = FP::undefined();
            s = FP::undefined();
            return;
        }

        size_t n = 0;
        double sum = 0;
        double sumSq = 0;
        for (; begin != end; ++begin, ++n) {
            Q_ASSERT(FP::defined(*begin));
            sum += *begin;
            sumSq += (*begin) * (*begin);
        }
        Q_ASSERT(n > 0);

        m = sum / (double) n;
        s = calculateSD(m, sumSq, n);
    }

    /**
     * Get the mean and standard deviation of the given points.
     * 
     * @param points    the points
     * @param m         the output mean
     * @param s         the output standard deviation
     */
    template<typename ListType>
    static inline void meanSD(const ListType &points, double &m, double &s)
    { meanSD(points.begin(), points.end(), m, s); }

    /**
     * Get the mean and standard deviation of the given points.  This requires
     * that all points in the list are defined.
     * 
     * @param points    the points
     * @param m         the output mean
     * @param s         the output standard deviation
     */
    template<typename ListType>
    static inline void meanSDDefined(const ListType &points, double &m, double &s)
    { meanSDDefined(points.begin(), points.end(), m, s); }

    /**
     * Get the standard deviation of the given points.
     * 
     * @param begin     the start of the points
     * @param end       the end of the points
     * @return          the standard deviation
     */
    template<typename ForwardIterator>
    static inline double sd(ForwardIterator begin, ForwardIterator end)
    {
        double m = 0;
        double s = 0;
        meanSD(begin, end, m, s);
        return s;
    }

    /**
     * Get the standard deviation of the given points.
     * 
     * @param points    the points
     * @return          the standard deviation
     */
    template<typename ListType>
    static inline double sd(const ListType &points)
    {
        double m = 0;
        double s = 0;
        meanSD(points, m, s);
        return s;
    }


    static void sortRemoveUndefined(QVector<double> &list);

    static double quantile(const QVector<double> &sorted, double q);

    static double quantile(const std::vector<double> &sorted, double q);

    static double normalPDF(double x);

    static double normalPDF(double x, double v, double u = 0.0);

    static double normalQuantile(double q);

    static double normalCDF(double x);

    static double studentQuantile(double df, double q);

    static double chi2CDF(double df, double gamma, double x);

    /**
     * The CDF of the centered chi^2 distribution with the given 
     * number of degrees of freedom.
     * <br>
     * Low accuracy with a small number of degrees of freedom.
     * 
     * @param df        the degrees of freedom
     * @param x         the position
     * @return          the quantile to the distribution
     */
    static inline double chi2CDF(double df, double x)
    { return chi2CDF(df, 0.0, x); }

    static double chi2Quantile(double df, double gamma, double q);

    /**
     * Get the given quantile of the centered chi^2 distribution with the given 
     * number of degrees of freedom.
     * <br>
     * Low accuracy with a small number of degrees of freedom.
     * 
     * @param df        the degrees of freedom
     * @param q         the quantile
     * @return          the quantile to the distribution
     */
    static inline double chi2Quantile(double df, double q)
    { return chi2Quantile(df, 0.0, q); }

    /**
     * The error function erf.  This only exists as a separate definition
     * because older versions of MSVC didn't include it.
     * 
     * @param x         the value to evaluate
     * @return          erf(x)
     */
    static inline double erf(double x)
    {
#if !defined(Q_CC_MSVC) || (defined(_MSC_VER) && _MSC_VER >= 1700)
        return ::erf(x);
#else
        double sign = 1.0;
        if (t < 0.0) {
            sign = -1.0;
            t = -t;
        }
        double t = 1.0 / (1.0 + 0.47047 * x);
        return sign * (1.0 - (0.3480242*t - 0.0958798*t*t + 0.7478556*t*t*t) *
            exp(-(x*x)));
#endif
    }
};

}
}

#endif
