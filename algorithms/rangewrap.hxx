/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSRANGEWRAP_H
#define CPD3ALGORITHMSRANGEWRAP_H

#include "core/first.hxx"

#include <math.h>
#include <memory>
#include <QtGlobal>
#include <QList>
#include "algorithms/algorithms.hxx"

#include "core/number.hxx"
#include "algorithms/model.hxx"
#include "algorithms/transformer.hxx"


namespace CPD3 {
namespace Algorithms {

/**
 * A specialized transformer that inserted simulated wrap around values to
 * restore information that was lost during averaging.
 */
class CPD3ALGORITHMS_EXPORT RangeWrap : public Transformer {
    double min;
    double max;
    int dimension;

    double mid;
    double range;

    double applyWrap(double value) const;

public:
    RangeWrap(double setMin, double setMax, int onlyDimension = -1);

    ~RangeWrap();

    virtual void applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                            TransformerAllocator *allocator = NULL) const;

    virtual void
            applyConst(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL)
            const;

    virtual Transformer *clone() const;

    /**
     * Get the filter minimum.
     * @return the minimum
     */
    inline double getMin() const
    { return min; }

    /**
     * Get the filter maximum.
     * @return the maximum
     */
    inline double getMax() const
    { return max; }

protected:
    friend class Transformer;

    RangeWrap(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator<<(QDataStream &stream, const Transformer *transformer);

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator>>(QDataStream &stream, Transformer *&transformer);

    friend CPD3ALGORITHMS_EXPORT QDebug operator<<(QDebug stream, const Transformer *transformer);
};

}
}

#endif
