/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVector>

#include "algorithms/leastsquares.hxx"
#include "algorithms/statistics.hxx"

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/leastsquares.hxx
 * Provides routines for generating least squares regression fits.
 */

BasicOrdinaryLeastSquares::~BasicOrdinaryLeastSquares()
{ }

BasicOrdinaryLeastSquares::BasicOrdinaryLeastSquares(const BasicOrdinaryLeastSquares &other)
        : Model(),
          n(other.n),
          m(other.m),
          b(other.b),
          r2(other.r2),
          sm(other.sm),
          sb(other.sb),
          constraints(other.constraints)
{ }

/**
 * Construct the ordinary least squares fit from the given points.
 * 
 * @param x         the independent values
 * @param y         the dependent values
 * @param output    the output constraints to apply
 */
BasicOrdinaryLeastSquares::BasicOrdinaryLeastSquares(const QVector<double> &x,
                                                     const QVector<double> &y,
                                                     const ModelOutputConstraints &output)
        : constraints(output)
{
    n = 0;
    double sxy = 0;
    double sx = 0;
    double sy = 0;
    double sxx = 0;
    double syy = 0;
    for (int i = 0, max = qMin(x.size(), y.size()); i < max; i++) {
        double xv = x.at(i);
        double yv = y.at(i);
        if (!FP::defined(xv) || !FP::defined(yv))
            continue;
        n++;

        sxy += xv * yv;
        sx += xv;
        sy += yv;
        sxx += xv * xv;
        syy += yv * yv;
    }

    if (n == 1) {
        m = 0;
        b = sy;
        r2 = 1.0;
        sm = FP::undefined();
        sb = FP::undefined();
        return;
    }
    if (n < 2) {
        m = FP::undefined();
        b = FP::undefined();
        r2 = FP::undefined();
        sm = FP::undefined();
        sb = FP::undefined();
        return;
    }

    double div = ((double) n * sxx - sx * sx);
    if (div == 0.0) {
        m = FP::undefined();
        b = FP::undefined();
        r2 = FP::undefined();
        sm = FP::undefined();
        sb = FP::undefined();
        return;
    }

    m = ((double) n * sxy - (sx * sy)) / div;
    b = (sy - m * sx) / (double) n;

    if (n < 3) {
        r2 = FP::undefined();
        sm = FP::undefined();
        sb = FP::undefined();
        return;
    }

    double see = ((double) n * syy - sy * sy - m * m * ((double) n * sxx - sx * sx)) /
            (((double) n * (double) (n - 2)));

    sm = sqrt(((double) n * see) / ((double) n * sxx - sx * sx));
    sb = sm * sqrt(sxx / (double) n);

    double tsq = 0.0;
    double rsq = 0.0;
    double ymean = sy / (double) n;
    for (int i = 0, max = qMin(x.size(), y.size()); i < max; i++) {
        double xv = x.at(i);
        double yv = y.at(i);
        if (!FP::defined(xv) || !FP::defined(yv))
            continue;

        double d = yv - ymean;
        tsq += d * d;

        d = yv - evaluate(xv);
        rsq += d * d;
    }
    r2 = 1.0 - rsq / tsq;
}

QVector<double> BasicOrdinaryLeastSquares::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

QVector<double> BasicOrdinaryLeastSquares::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

void BasicOrdinaryLeastSquares::applyIOConst(QVector<double> &values) const
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

void BasicOrdinaryLeastSquares::applyIO(QVector<double> &values)
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

double BasicOrdinaryLeastSquares::oneToOneConst(double input) const
{ return evaluate(input); }

double BasicOrdinaryLeastSquares::oneToOne(double input)
{ return evaluate(input); }

double BasicOrdinaryLeastSquares::twoToOneConst(double a, double b) const
{
    Q_UNUSED(b);
    return evaluate(a);
}

double BasicOrdinaryLeastSquares::twoToOne(double a, double b)
{
    Q_UNUSED(b);
    return evaluate(a);
}

double BasicOrdinaryLeastSquares::threeToOneConst(double a, double b, double c) const
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

double BasicOrdinaryLeastSquares::threeToOne(double a, double b, double c)
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

void BasicOrdinaryLeastSquares::applyIOConst(double &a, double &b) const
{
    a = evaluate(a);
    b = evaluate(b);
}

void BasicOrdinaryLeastSquares::applyIO(double &a, double &b)
{
    a = evaluate(a);
    b = evaluate(b);
}

void BasicOrdinaryLeastSquares::applyIOConst(double &a, double &b, double &c) const
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

void BasicOrdinaryLeastSquares::applyIO(double &a, double &b, double &c)
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

Model *BasicOrdinaryLeastSquares::clone() const
{ return new BasicOrdinaryLeastSquares(*this); }

QString BasicOrdinaryLeastSquares::describe() const
{ return "SimpleLeastSquares"; }

double BasicOrdinaryLeastSquares::slopeConfidence(double confidence) const
{
    if (FP::defined(sm) && n > 2 && FP::defined(confidence)) {
        return sm * Statistics::studentQuantile(n - 2, 1.0 - (1.0 - confidence) / 2.0);
    } else {
        return FP::undefined();
    }
}

double BasicOrdinaryLeastSquares::interceptConfidence(double confidence) const
{
    if (FP::defined(sb) && n > 2 && FP::defined(confidence)) {
        return sb * Statistics::studentQuantile(n - 2, 1.0 - (1.0 - confidence) / 2.0);
    } else {
        return FP::undefined();
    }
}

ModelLegendEntry BasicOrdinaryLeastSquares::legend(const ModelLegendParameters &parameters) const
{
    QVector<double> coefficients;
    coefficients << b << m;

    QVector<double> confidence;
    confidence <<
            interceptConfidence(parameters.getConfidence()) <<
            slopeConfidence(parameters.getConfidence());

    QString result(legendPolynomialString(coefficients, confidence, parameters));

    if (FP::defined(r2)) {
        if (!result.isEmpty())
            result.append(", ");
        result.append('r');
        result.append(NumberFormat(1, 0).superscript(2));
        result.append('=');
        result.append(NumberFormat().apply(r2, QChar()));
    }

    return ModelLegendEntry(result);
}

BasicOrdinaryLeastSquares::BasicOrdinaryLeastSquares(QDataStream &stream)
{ stream >> m >> b >> r2 >> sm >> sb; }

void BasicOrdinaryLeastSquares::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_BasicOrdinaryLSQ;
    stream << t << m << b << r2 << sm << sb;
}

void BasicOrdinaryLeastSquares::printLog(QDebug &stream) const
{ stream << "BasicOrdinaryLeastSquares(" << m << ',' << b << ')'; }


ZeroOrdinaryLeastSquares::~ZeroOrdinaryLeastSquares()
{ }

ZeroOrdinaryLeastSquares::ZeroOrdinaryLeastSquares(const ZeroOrdinaryLeastSquares &other)
        : Model(),
          n(other.n),
          m(other.m),
          r2(other.r2),
          sm(other.sm),
          constraints(other.constraints)
{ }

/**
 * Construct the ordinary least squares fit from the given points.
 * 
 * @param x         the independent values
 * @param y         the dependent values
 * @param output    the output constraints to apply
 */
ZeroOrdinaryLeastSquares::ZeroOrdinaryLeastSquares(const QVector<double> &x,
                                                   const QVector<double> &y,
                                                   const ModelOutputConstraints &output)
        : constraints(output)
{
    n = 0;
    double sxy = 0;
    double sxx = 0;
    double syy = 0;
    double sx = 0;
    for (int i = 0, max = qMin(x.size(), y.size()); i < max; i++) {
        double xv = x.at(i);
        double yv = y.at(i);
        if (!FP::defined(xv) || !FP::defined(yv))
            continue;
        n++;

        sxy += xv * yv;
        sxx += xv * xv;
        syy += yv * yv;
        sx += xv;
    }

    if (n < 1) {
        m = FP::undefined();
        r2 = FP::undefined();
        sm = FP::undefined();
        return;
    }

    if (sxx == 0.0) {
        m = FP::undefined();
        r2 = FP::undefined();
        sm = FP::undefined();
        return;
    }

    m = sxy / sxx;

    if (n < 3) {
        m = FP::undefined();
        r2 = FP::undefined();
        sm = FP::undefined();
        return;
    }

    r2 = 0;
    double see = 0.0;
    double xdsq = 0.0;
    double mean = sx / n;
    for (int i = 0, max = qMin(x.size(), y.size()); i < max; i++) {
        double xv = x.at(i);
        double yv = y.at(i);
        if (!FP::defined(xv) || !FP::defined(yv))
            continue;
        double xd = xv - mean;
        xdsq += xd * xd;
        xv *= m;
        r2 += xv * xv;
        yv -= xv;
        see += yv * yv;
    }

    if (syy != 0.0) {
        r2 /= syy;
    } else {
        r2 = FP::undefined();
    }

    sm = sqrt(see / (xdsq * (n - 2)));
}

QVector<double> ZeroOrdinaryLeastSquares::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

QVector<double> ZeroOrdinaryLeastSquares::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

void ZeroOrdinaryLeastSquares::applyIOConst(QVector<double> &values) const
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

void ZeroOrdinaryLeastSquares::applyIO(QVector<double> &values)
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

double ZeroOrdinaryLeastSquares::oneToOneConst(double input) const
{ return evaluate(input); }

double ZeroOrdinaryLeastSquares::oneToOne(double input)
{ return evaluate(input); }

double ZeroOrdinaryLeastSquares::twoToOneConst(double a, double b) const
{
    Q_UNUSED(b);
    return evaluate(a);
}

double ZeroOrdinaryLeastSquares::twoToOne(double a, double b)
{
    Q_UNUSED(b);
    return evaluate(a);
}

double ZeroOrdinaryLeastSquares::threeToOneConst(double a, double b, double c) const
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

double ZeroOrdinaryLeastSquares::threeToOne(double a, double b, double c)
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

void ZeroOrdinaryLeastSquares::applyIOConst(double &a, double &b) const
{
    a = evaluate(a);
    b = evaluate(b);
}

void ZeroOrdinaryLeastSquares::applyIO(double &a, double &b)
{
    a = evaluate(a);
    b = evaluate(b);
}

void ZeroOrdinaryLeastSquares::applyIOConst(double &a, double &b, double &c) const
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

void ZeroOrdinaryLeastSquares::applyIO(double &a, double &b, double &c)
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

Model *ZeroOrdinaryLeastSquares::clone() const
{ return new ZeroOrdinaryLeastSquares(*this); }

QString ZeroOrdinaryLeastSquares::describe() const
{ return "ZeroLeastSquares"; }

double ZeroOrdinaryLeastSquares::slopeConfidence(double confidence) const
{
    if (FP::defined(sm) && n > 2 && FP::defined(confidence)) {
        return sm * Statistics::studentQuantile(n - 2, 1.0 - (1.0 - confidence) / 2.0);
    } else {
        return FP::undefined();
    }
}

ModelLegendEntry ZeroOrdinaryLeastSquares::legend(const ModelLegendParameters &parameters) const
{
    QVector<double> coefficients;
    coefficients << m;

    QVector<double> confidence;
    confidence << slopeConfidence(parameters.getConfidence());

    QString result(legendPolynomialString(coefficients, confidence, parameters));

    if (FP::defined(r2)) {
        if (!result.isEmpty())
            result.append(", ");
        result.append('r');
        result.append(NumberFormat(1, 0).superscript(2));
        result.append('=');
        result.append(NumberFormat().apply(r2, QChar()));
    }

    return ModelLegendEntry(result);
}

ZeroOrdinaryLeastSquares::ZeroOrdinaryLeastSquares(QDataStream &stream)
{ stream >> m >> r2 >> sm; }

void ZeroOrdinaryLeastSquares::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_ZeroOrdinaryLSQ;
    stream << t << m << r2 << sm;
}

void ZeroOrdinaryLeastSquares::printLog(QDebug &stream) const
{ stream << "ZeroOrdinaryLeastSquares(" << m << ')'; }


}
}
