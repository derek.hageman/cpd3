/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSNUMERICSOLVE_H
#define CPD3ALGORITHMSNUMERICSOLVE_H

#include "core/first.hxx"

#include <math.h>
#include <QtGlobal>
#include <QObject>

#include "algorithms/algorithms.hxx"
#include "core/number.hxx"


namespace CPD3 {
namespace Algorithms {

/**
 * This class provides a general solver using Newton-Raphson iteration.
 * <br>
 * The derived class (inheriting from NewtonRaphson<T>, as per CRTP) must
 * provide an evaluate(double) method.
 */
template<class T>
class NewtonRaphson {
public:
    /**
     * Solve for a given target.
     * 
     * @param yTarget   the target value to solve for
     * @return          the resulting input value
     */
    double solve(double yTarget = 0.0) const
    {
        double x0 = static_cast<const T *>(this)->initial(yTarget);
        if (!FP::defined(x0))
            return FP::undefined();
        double y0 = static_cast<const T *>(this)->evaluate(x0);
        if (!FP::defined(y0))
            return FP::undefined();

        double x1 = static_cast<const T *>(this)->firstStep(x0);
        double y1;
        double dX;
        for (int itter = 0; itter < 20; itter++) {
            y1 = static_cast<const T *>(this)->evaluate(x1);
            if (!FP::defined(y1))
                return FP::undefined();
            if (y1 == y0)
                return x1;
            dX = ((x1 - x0) / (y1 - y0)) * (yTarget - y1);
            if (static_cast<const T *>(this)->completed(dX))
                return x1 + dX;
            x0 = x1;
            y0 = y1;
            x1 += dX;
        }
        return FP::undefined();
    }

    /**
     * Called to get the initial value used to begin the iteration.
     * <br>
     * The default implementation returns the input target.
     * 
     * @param yTarget   the target value being solved for
     * @return          the initial guess value
     */
    double initial(double yTarget) const
    {
        return yTarget;
    }

    /**
     * Called to get the first step of the iteration.
     * <br>
     * The default returns the input times 0.9 unless it's zero then it returns
     * 0.1.
     * 
     * @param x0    the initial guess value
     * @return      the second guess value
     */
    double firstStep(double x0) const
    {
        if (x0 == 0.0)
            return 0.1;
        return x0 * 0.9;
    }

    /**
     * Called at each step of iteration to test if the answer is good enough.
     * <br>
     * The default returns true when the step size is less than 1E-6.
     * 
     * @param dX    the current step size
     * @return      true to stop the iteration
     */
    bool completed(double dX) const
    {
        return fabs(dX) < 1E-6;
    }
};

}
}

#endif
