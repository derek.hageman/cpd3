/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <algorithm>
#include <QVector>

#include "algorithms/statistics.hxx"

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/statistics.hxx
 * General statistics utilities.
 */

double Statistics::calculateSD(double mean, double sumSq, size_t n)
{
    if (n < 2)
        return FP::undefined();
    double stddevSq = sumSq / (double) n - mean * mean;
    if (stddevSq < 0.0)
        return FP::undefined();
    return sqrt(stddevSq * ((double) n / (double) (n - 1)));
}

static inline bool sortRemoveUndefinedComparator(double a, double b)
{
    if (!FP::defined(b))
        return FP::defined(a);
    if (!FP::defined(a))
        return false;
    return a < b;
}

static QVector<double>::iterator findFirstUndefined(QVector<double> &list)
{
    if (list.empty() || FP::defined(list.last()))
        return list.end();
    QVector<double>::iterator begin = list.begin();
    QVector<double>::iterator middle;
    int n = list.size();
    int half;
    while (n > 0) {
        half = n >> 1;
        middle = begin + half;
        if (FP::defined(*middle)) {
            begin = middle + 1;
            n -= half + 1;
        } else {
            n = half;
        }
    }
    return begin;
}

/**
 * Sort the list of values and remove all undefined values from it.
 * 
 * @param list  the input unsorted list and the output sorted with no undefined values
 */
void Statistics::sortRemoveUndefined(QVector<double> &list)
{
    std::sort(list.begin(), list.end(), sortRemoveUndefinedComparator);
    QVector<double>::iterator end = list.end();
    QVector<double>::iterator begin = findFirstUndefined(list);
    if (begin != end)
        list.erase(begin, end);
}

/**
 * Get the quantile at the given percentage.
 * 
 * @param sorted    the sorted set of points
 * @param q         the quantile [0-1].
 */
double Statistics::quantile(const QVector<double> &sorted, double q)
{
    Q_ASSERT(FP::defined(q) && q >= 0.0 && q <= 1.0);
    if (sorted.isEmpty())
        return FP::undefined();
    if (sorted.size() == 1)
        return sorted.first();
    double center = q * (double) (sorted.size() - 1);
    double upper = ceil(center);
    double lower = floor(center);
    int iUpper = (int) upper;
    int iLower = (int) lower;
    if (iUpper == iLower)
        return sorted.at(iLower);
    center -= lower;
    return sorted.at(iUpper) * (center) + sorted.at(iLower) * (1.0 - center);
}

/**
 * Get the quantile at the given percentage.
 * 
 * @param sorted    the sorted set of points
 * @param q         the quantile [0-1].
 */
double Statistics::quantile(const std::vector<double> &sorted, double q)
{
    Q_ASSERT(FP::defined(q) && q >= 0.0 && q <= 1.0);
    if (sorted.empty())
        return FP::undefined();
    if (sorted.size() == 1)
        return sorted.front();
    double center = q * (double) (sorted.size() - 1);
    double upper = std::ceil(center);
    double lower = std::floor(center);
    std::size_t iUpper = static_cast<std::size_t>(upper);
    std::size_t iLower = static_cast<std::size_t>(lower);
    if (iUpper == iLower)
        return sorted[iLower];
    center -= lower;
    return sorted[iUpper] * (center) + sorted[iLower] * (1.0 - center);
}

/**
 * Get the given quantile of the standard normal distribution.
 * 
 * @param q the quantile
 * @return  the quantile of the normal distribution
 */
double Statistics::normalQuantile(double q)
{
    Q_ASSERT(q > 0.0 && q < 1.0);

    /*
     * Loosely based off of 
     * http://root.cern.ch/root/html/src/TMath.cxx.html#NN0FOC
     * ALGORITHM AS241  APPL. STATIST. (1988) VOL. 37, NO. 3, 477-484.
     */
    static const double s1 = 0.425;
    static const double s2 = 5.0;
    static const double k1 = 0.180625;
    static const double k2 = 1.6;
    static const double a[8] =
            {3.3871328727963666080, 1.3314166789178437745E2, 1.9715909503065514427E3,
             1.3731693765509461125E4, 4.5921953931549871457E4, 6.7265770927008700853E4,
             3.3430575583588128105E4, 2.5090809287301226727E3};
    static const double b[7] =
            {4.2313330701600911252E1, 6.8718700749205790830E2, 5.3941960214247511077E3,
             2.1213794301586595867E4, 3.9307895800092710610E4, 2.8729085735721942674E4,
             5.2264952788528545610E3};
    static const double c[8] =
            {1.42343711074968357734, 4.63033784615654529590, 5.76949722146069140550,
             3.64784832476320460504, 1.27045825245236838258, 2.41780725177450611770E-1,
             2.27238449892691845833E-2, 7.74545014278341407640E-4};
    static const double d[7] =
            {2.05319162663775882187, 1.67638483018380384940, 6.89767334985100004550E-1,
             1.48103976427480074590E-1, 1.51986665636164571966E-2, 5.47593808499534494600E-4,
             1.05075007164441684324E-9};
    static const double e[8] =
            {6.65790464350110377720, 5.46378491116411436990, 1.78482653991729133580,
             2.96560571828504891230E-1, 2.65321895265761230930E-2, 1.24266094738807843860E-3,
             2.71155556874348757815E-5, 2.01033439929228813265E-7};
    static const double f[7] =
            {5.99832206555887937690E-1, 1.36929880922735805310E-1, 1.48753612908506148525E-2,
             7.86869131145613259100E-4, 1.84631831751005468180E-5, 1.42151175831644588870E-7,
             2.04426310338993978564E-15};

    double qq = q - 0.5;
    if (qq == 0.0) {
        return 0;
    }
    if (fabs(qq) < s1) {
        double r = k1 - qq * qq;
        double accum = r;
        double c1 = a[0];
        for (int i = 1; i < 8; i++, accum *= r) {
            c1 += accum * a[i];
        }
        double c2 = 1.0;
        accum = r;
        for (int i = 0; i < 7; i++, accum *= r) {
            c2 += accum * b[i];
        }
        return qq * c1 / c2;
    } else {
        double r;
        if (qq < 0.0)
            r = q;
        else
            r = 1.0 - q;
        r = sqrt(-log(r));
        if (r <= s2) {
            r = r - k2;
            double accum = r;
            double c1 = c[0];
            for (int i = 1; i < 8; i++, accum *= r) {
                c1 += accum * c[i];
            }
            double c2 = 1.0;
            accum = r;
            for (int i = 0; i < 7; i++, accum *= r) {
                c2 += accum * d[i];
            }
            r = c1 / c2;
        } else {
            r = r - s2;
            double accum = r;
            double c1 = e[0];
            for (int i = 1; i < 8; i++, accum *= r) {
                c1 += accum * e[i];
            }
            double c2 = 1.0;
            accum = r;
            for (int i = 0; i < 7; i++, accum *= r) {
                c2 += accum * f[i];
            }
            r = c1 / c2;
        }
        if (qq < 0.0)
            return -r;
        return r;
    }
}

/**
 * The PDF of the standard normal distribution.
 * 
 * @param x     the position
 * @return      the probability
 */
double Statistics::normalPDF(double x)
{
    return 0.398942280401433 * exp((x * x) / -2.0);
}

/**
 * The PDF of the a normal distribution.
 * 
 * @param x     the position
 * @param v     the variance (sd^2)
 * @param u     the center (mean/median)
 * @return      the probability
 */
double Statistics::normalPDF(double x, double v, double u)
{
    Q_ASSERT(v > 0.0);
    x -= u;
    return exp((x * x) / (-2.0 * v)) / sqrt(6.28318530717959 * v);
}

/**
 * The CDF of the standard normal distribution.
 * 
 * @param x the position
 * @return  the cumulative probability
 */
double Statistics::normalCDF(double x)
{
    static const double p = 0.2316419;
    static const double b[5] = {0.319381530, -0.356563782, 1.781477937, -1.821255978, 1.330274429};
    double t;
    if (x >= 0.0)
        t = 1.0 / (1.0 + p * x);
    else
        t = 1.0 / (1.0 - p * x);
    double accum = t;
    double sum = 0;
    for (int i = 0; i < 5; i++, accum *= t) {
        sum += accum * b[i];
    }
    if (x >= 0.0)
        return 1.0 - normalPDF(x) * sum;
    else
        return normalPDF(-x) * sum;
}

/**
 * Get the given quantile of Student's t-distribution with the given 
 * number of degrees of freedom.
 * 
 * @param df        the degrees of freedom
 * @param q         the quantile
 * @return          the quantile to the distribution
 */
double Statistics::studentQuantile(double df, double q)
{
    Q_ASSERT(df >= 1.0 && q > 0.0 && q < 1.0);

    /*
     * Based off of http://root.cern.ch/root/html/src/TMath.cxx.html#E_45cC
     * G.W.Hill, "Algorithm 396, Student's t-quantiles"
     * "Communications of the ACM", 13(10), October 1970
     */

    static const double studentIntegerEpsilon = 1E-8;

    if (df < 1.0 + studentIntegerEpsilon)
        return tan(3.14159265358979323846 * (q - 0.5));
    if (fabs(df - 2.0) < studentIntegerEpsilon)
        return 2.0 * (q - 0.5) * sqrt(2.0 / (4.0 * q * (1.0 - q)));
    if (fabs(df - 4.0) < studentIntegerEpsilon) {
        if (q > 0.5) {
            double alpha = sqrt(4.0 * q * (1.0 - q));
            return 2.0 * sqrt(cos(acos(alpha) / 3.0) / alpha - 1.0);
        } else if (q != 0.5) {
            double alpha = sqrt(4.0 * q * (1.0 - q));
            return -2.0 * sqrt(cos(acos(alpha) / 3.0) / alpha - 1.0);
        } else {
            return 0.0;
        }
    }

    double qq;
    if (q > 0.5) {
        qq = 2.0 * (1.0 - q);
    } else {
        qq = 2.0 * q;
    }

    double a = 1.0 / (df - 0.5);
    double b = 48.0 / (a * a);
    double c = ((20700.0 * a / b - 98.0) * a - 16.0) * a + 96.36;
    double d = ((94.5 / (b + c) - 3.0) / b + 1.0) * sqrt(a * 1.57079632679490) * df;
    double x = d * d;
    double y = pow(x, 2.0 / df);

    if (y > a + 0.05) {
        x = normalQuantile(qq * 0.5);
        y = x * x;
        if (df < 5.0)
            c += 0.3 * (df - 4.5) * (x + 0.6);
        c += (((0.05 * d * x - 5.0) * x - 7.0) * x - 2.0) * x + b;
        y = (((((0.4 * y + 6.3) * y + 36.0) * y + 94.5) / c - y - 3.0) / b + 1.0) * x;
        y = a * y * y;
        if (y > 0.002)
            y = exp(y) - 1.0;
        else
            y += 0.5 * y * y;
    } else {
        y = ((1.0 / (((df + 6.0) / (df * y) - 0.089 * d - 0.822) * (df + 2.0) * 3.0) +
                0.5 / (df + 4.0)) * y - 1.0) * (df + 1.0) / (df + 2.0) + 1.0 / y;
    }
    if (q <= 0.5)
        return -sqrt(df * y);
    return sqrt(df * y);
}

/**
 * the CDF of the chi^2 distribution with the given number of degrees of 
 * freedom and centrality parameter.
 * <br>
 * Low accuracy with a small number of degrees of freedom.
 * 
 * @param df        the degrees of freedom
 * @param gamma     the non-centrality parameter
 * @param x         the position
 * @return          the probability
 */
double Statistics::chi2CDF(double df, double gamma, double x)
{
    Q_ASSERT(df > 0.0 && gamma >= 0.0);
    double h = 1.0 -
            (2.0 * (df + gamma) * (df + gamma * 3.0)) /
                    (3.0 * (df + 2.0 * gamma) * (df + 2.0 * gamma));
    double p = (df + 2.0 * gamma) / ((df + gamma) * (df + gamma));
    double m = (h - 1.0) * (1.0 - 3.0 * h);
    return normalCDF(
            (pow(x / (df + gamma), h) - (1.0 + h * p * (h - 1.0 - 0.5 * (2.0 - h) * m * p))) /
                    (h * sqrt(2.0 * p) * (1.0 + 0.5 * m * p)));
}

/**
 * Get the given quantile of the chi^2 distribution with the given number of
 * degrees of freedom.
 * <br>
 * Low accuracy with a small number of degrees of freedom.
 * 
 * @param df        the degrees of freedom
 * @param gamma     the non-centrality parameter
 * @param q         the quantile
 * @return          the quantile to the distribution
 */
double Statistics::chi2Quantile(double df, double gamma, double q)
{
    Q_ASSERT(df > 0.0 && gamma >= 0.0 && q >= 0.0 && q <= 1.0);
    double h = 1.0 -
            (2.0 * (df + gamma) * (df + gamma * 3.0)) /
                    (3.0 * (df + 2.0 * gamma) * (df + 2.0 * gamma));
    double p = (df + 2.0 * gamma) / ((df + gamma) * (df + gamma));
    double m = (h - 1.0) * (1.0 - 3.0 * h);
    return pow(normalQuantile(q) * h * sqrt(2.0 * p) * (1.0 + 0.5 * m * p) +
                       (1.0 + h * p * (h - 1.0 - 0.5 * (2.0 - h) * m * p)), 1.0 / h) * (df + gamma);
}

}
}
