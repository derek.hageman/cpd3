/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/numericsolve.hxx
 * Numeric solving utilities.
 */

}
}
