/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSLMA_H
#define CPD3ALGORITHMSLMA_H

#include "core/first.hxx"

#include <stdlib.h>
#include <QtGlobal>
#include <QVector>

#include "algorithms/algorithms.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Algorithms {

/**
 * Provides an interface to levmar for the Levenberg-Marquardt Algorithm for
 * curve fitting.
 */
class CPD3ALGORITHMS_EXPORT LevenbergMarquardt {
    static bool runAnalytic(void (*func)(double *p, double *hx, int m, int n, void *adata),
                            void (*jacf)(double *p, double *j, int m, int n, void *adata),
                            double *p,
                            const double *x,
                            int m,
                            int n,
                            void *adata);

    static bool runFDA(void (*func)(double *p, double *hx, int m, int n, void *adata),
                       double *p,
                       const double *x,
                       int m,
                       int n,
                       void *adata);

    template<typename Functor>
    class WrapperFDA {
        QVector<double> positions;
        QVector<double> localP;
        Functor *functor;
    public:
        WrapperFDA(int N, const QVector<double> &p, Functor *f) : positions(p),
                                                                  localP(N, FP::undefined()),
                                                                  functor(f)
        { }

        void func(double *p, double *hx, int m, int n)
        {
            Q_ASSERT(m == localP.size());
            Q_ASSERT(n == positions.size());

            for (int i = 0; i < n; i++) {
                memcpy(localP.data(), p, m * sizeof(double));
                hx[i] = (*functor)(positions.at(i), localP);
            }
        }
    };

    template<typename Functor, typename FunctorJac>
    class WrapperAnalytic {
        QVector<double> positions;
        QVector<double> localP;
        Functor *functor;
        FunctorJac *jacfunctor;
    public:
        WrapperAnalytic(int N, const QVector<double> &p, Functor *f, FunctorJac *jf) : positions(p),
                                                                                       localP(N,
                                                                                              FP::undefined()),
                                                                                       functor(f),
                                                                                       jacfunctor(
                                                                                               jf)
        { }

        void func(double *p, double *hx, int m, int n)
        {
            Q_ASSERT(m == localP.size());
            Q_ASSERT(n == positions.size());

            for (int i = 0; i < n; i++) {
                memcpy(localP.data(), p, m * sizeof(double));
                hx[i] = (*functor)(positions.at(i), localP);
            }
        }

        void jac(double *p, double *hx, int m, int n)
        {
            Q_ASSERT(m == localP.size());
            Q_ASSERT(n == positions.size());

            for (int i = 0; i < n; i++) {
                memcpy(localP.data(), p, m * sizeof(double));
                localP = (*jacfunctor)(positions.at(i), localP);
                Q_ASSERT(m == localP.size());

                memcpy(hx + i * m, localP.constData(), m * sizeof(double));
            }
        }
    };

    template<typename Wrapper>
    static void wrapperEvaluatorFunc(double *p, double *hx, int m, int n, void *adata)
    {
        Wrapper *w = (Wrapper *) adata;
        w->func(p, hx, m, n);
    }

    template<typename Wrapper>
    static void wrapperEvaluatorJac(double *p, double *hx, int m, int n, void *adata)
    {
        Wrapper *w = (Wrapper *) adata;
        w->jac(p, hx, m, n);
    }

public:
    /**
     * Execute a Levenberg-Marquardt fit on the given function, using
     * finite difference approximation to derive the Jacobian.
     * <br>
     * The functor is called like: value = Functor(x, QVector<double>)
     * 
     * @param N         the number of independent parameters
     * @param f         the functor to evaluate the function
     * @param positions the positions of the data
     * @param data      the data
     * @param initial   the initial values, if empty assumed to be zero
     * @return          the fitted values
     */
    template<typename Functor>
    static QVector<double> numeric(int N,
                                   Functor &f,
                                   const QVector<double> &positions,
                                   const QVector<double> &data,
                                   const QVector<double> &initial = QVector<double>())
    {
        Q_ASSERT(N > 0);
        if (data.size() < N)
            return QVector<double>(N, FP::undefined());

        WrapperFDA<Functor> wrapper(N, positions, &f);

        QVector<double> parameters(initial);
        if (parameters.size() != N) {
            parameters = QVector<double>(N, 0.0);
        } else {
            for (QVector<double>::iterator check = parameters.begin(), endCheck = parameters.end();
                    check != endCheck;
                    ++check) {
                if (!FP::defined(*check))
                    *check = 0.0;
            }
        }

        if (!runFDA(wrapperEvaluatorFunc<WrapperFDA<Functor> >, parameters.data(), data.data(), N,
                    data.size(), &wrapper))
            return QVector<double>(N, FP::undefined());

        return parameters;
    }

    template<typename Functor>
    static QVector<double> numeric(int N,
                                   const Functor &f,
                                   const QVector<double> &positions,
                                   const QVector<double> &data,
                                   const QVector<double> &initial = QVector<double>())
    {
        Q_ASSERT(N > 0);
        if (data.size() < N)
            return QVector<double>(N, FP::undefined());

        WrapperFDA<const Functor> wrapper(N, positions, &f);

        QVector<double> parameters(initial);
        if (parameters.size() != N) {
            parameters = QVector<double>(N, 0.0);
        } else {
            for (QVector<double>::iterator check = parameters.begin(), endCheck = parameters.end();
                    check != endCheck;
                    ++check) {
                if (!FP::defined(*check))
                    *check = 0.0;
            }
        }

        if (!runFDA(wrapperEvaluatorFunc<WrapperFDA<Functor> >, parameters.data(), data.data(), N,
                    data.size(), &wrapper))
            return QVector<double>(N, FP::undefined());

        return parameters;
    }

    /**
     * Execute a Levenberg-Marquardt fit on the given function, using an
     * analytic Jacobian.
     * <br>
     * The functor is called like: value = Functor(double, QVector<double>) and 
     * the Jacobian is called like QVector<double> = Jacobian(double, 
     * Vector<double>)
     * 
     * @param N         the number of independent parameters
     * @param f         the functor to evaluate the function
     * @param j         the functor to evaluate the jacobian
     * @param positions the positions of the data
     * @param data      the data
     * @param initial   the initial values, if empty assumed to be zero
     * @return          the fitted values
     */
    template<typename Functor, typename Jacobian>
    static QVector<double> analytic(int N,
                                    Functor &f,
                                    Jacobian &j,
                                    const QVector<double> &positions,
                                    const QVector<double> &data,
                                    const QVector<double> &initial = QVector<double>())
    {
        Q_ASSERT(N > 0);
        if (data.size() < N)
            return QVector<double>(N, FP::undefined());

        WrapperAnalytic<Functor, Jacobian> wrapper(N, positions, &f, &j);

        QVector<double> parameters(initial);
        if (parameters.size() != N) {
            parameters = QVector<double>(N, 0.0);
        } else {
            for (QVector<double>::iterator check = parameters.begin(), endCheck = parameters.end();
                    check != endCheck;
                    ++check) {
                if (!FP::defined(*check))
                    *check = 0.0;
            }
        }

        if (!runAnalytic(wrapperEvaluatorFunc<WrapperAnalytic<Functor, Jacobian> >,
                         wrapperEvaluatorJac<WrapperAnalytic<Functor, Jacobian> >,
                         parameters.data(), data.data(), N, data.size(), &wrapper))
            return QVector<double>(N, FP::undefined());

        return parameters;
    }

    template<typename Functor, typename Jacobian>
    static QVector<double> analytic(int N,
                                    const Functor &f,
                                    const Jacobian &j,
                                    const QVector<double> &positions,
                                    const QVector<double> &data,
                                    const QVector<double> &initial = QVector<double>())
    {
        Q_ASSERT(N > 0);
        if (data.size() < N)
            return QVector<double>(N, FP::undefined());

        WrapperAnalytic<const Functor, const Jacobian> wrapper(N, positions, &f, &j);

        QVector<double> parameters(initial);
        if (parameters.size() != N) {
            parameters = QVector<double>(N, 0.0);
        } else {
            for (QVector<double>::iterator check = parameters.begin(), endCheck = parameters.end();
                    check != endCheck;
                    ++check) {
                if (!FP::defined(*check))
                    *check = 0.0;
            }
        }

        if (!runAnalytic(wrapperEvaluatorFunc<WrapperAnalytic<Functor, Jacobian> >,
                         wrapperEvaluatorJac<WrapperAnalytic<Functor, Jacobian> >,
                         parameters.data(), data.data(), N, data.size(), &wrapper))
            return QVector<double>(N, FP::undefined());

        return parameters;
    }
};

}
}

#endif
