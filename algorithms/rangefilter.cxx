/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVector>

#include "algorithms/rangefilter.hxx"

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/rangefilter.hxx
 * Provides a filter that adds or removes data based on a fixed range.
 */

static bool acceptMin(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value > filter->getMin();
}

static bool acceptMinInclusive(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value >= filter->getMin();
}

static bool acceptMax(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value < filter->getMax();
}

static bool acceptMaxInclusive(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value <= filter->getMax();
}

static bool acceptMinMax(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value > filter->getMin() && value < filter->getMax();
}

static bool acceptMinInclusiveMax(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value >= filter->getMin() && value < filter->getMax();
}

static bool acceptMinMaxInclusive(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value > filter->getMin() && value <= filter->getMax();
}

static bool acceptMinInclusiveMaxInclusive(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value >= filter->getMin() && value <= filter->getMax();
}

static bool rejectMin(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value < filter->getMin();
}

static bool rejectMinInclusive(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value <= filter->getMin();
}

static bool rejectMax(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value > filter->getMax();
}

static bool rejectMaxInclusive(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value >= filter->getMax();
}

static bool rejectMinMax(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value < filter->getMin() || value > filter->getMax();
}

static bool rejectMinInclusiveMax(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value <= filter->getMin() || value > filter->getMax();
}

static bool rejectMinMaxInclusive(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value < filter->getMin() || value >= filter->getMax();
}

static bool rejectMinInclusiveMaxInclusive(const RangeFilter *filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value <= filter->getMin() || value >= filter->getMax();
}


void RangeFilter::setFunction()
{
    if (!FP::defined(max)) {
        if (!FP::defined(min)) {
            function = NULL;
        } else if (flags & Min_Inclusive) {
            if (flags & Reject_Range)
                function = &rejectMinInclusive;
            else
                function = &acceptMinInclusive;
        } else {
            if (flags & Reject_Range)
                function = &rejectMin;
            else
                function = &acceptMin;
        }
    } else if (!FP::defined(min)) {
        if (flags & Max_Inclusive) {
            if (flags & Reject_Range)
                function = &rejectMaxInclusive;
            else
                function = &acceptMaxInclusive;
        } else {
            if (flags & Reject_Range)
                function = &rejectMax;
            else
                function = &acceptMax;
        }
    } else {
        if ((flags & Min_Inclusive) && (flags & Max_Inclusive)) {
            if (flags & Reject_Range)
                function = &rejectMinInclusiveMaxInclusive;
            else
                function = &acceptMinInclusiveMaxInclusive;
        } else if (flags & Min_Inclusive) {
            if (flags & Reject_Range)
                function = &rejectMinInclusiveMax;
            else
                function = &acceptMinInclusiveMax;
        } else if (flags & Max_Inclusive) {
            if (flags & Reject_Range)
                function = &rejectMinMaxInclusive;
            else
                function = &acceptMinMaxInclusive;
        } else {
            if (flags & Reject_Range)
                function = &rejectMinMax;
            else
                function = &acceptMinMax;
        }
    }
}

RangeFilter::RangeFilter(double setMin, double setMax, int setFlags, int onlyDimension) : min(
        setMin), max(setMax), flags(setFlags), dimension(onlyDimension), function(NULL)
{
    setFunction();
}

RangeFilter::~RangeFilter()
{ }

template<typename PointType>
static bool allDimensionsPassed(const RangeFilter *filter,
                                Internal::RangeFilterTestFunction function,
                                const PointType &point)
{
    for (int i = 0, nDims = point->totalDimensions(); i < nDims; ++i) {
        if ((*function)(filter, point->get(i)))
            continue;
        return false;
    }
    return true;
}

void RangeFilter::applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                             TransformerAllocator *allocator) const
{
    Q_UNUSED(allocator);
    if (function == NULL)
        return;

    if (flags & Reject_Remove) {
        if (dimension == -1) {
            for (QVector<std::shared_ptr<TransformerPoint> >::iterator it = points.begin();
                    it != points.end();) {
                if (allDimensionsPassed(this, function, *it)) {
                    ++it;
                    continue;
                }
                it = points.erase(it);
            }
        } else {
            for (QVector<std::shared_ptr<TransformerPoint> >::iterator it = points.begin();
                    it != points.end();) {
                if ((*it)->totalDimensions() <= dimension) {
                    it = points.erase(it);
                    continue;
                }
                if ((*function)(this, (*it)->get(dimension))) {
                    ++it;
                    continue;
                }
                it = points.erase(it);
            }
        }
    } else {
        if (dimension == -1) {
            for (QVector<std::shared_ptr<TransformerPoint> >::const_iterator
                    it = points.constBegin(), end = points.constEnd(); it != end; ++it) {
                for (int i = 0, nDims = (*it)->totalDimensions(); i < nDims; ++i) {
                    if ((*function)(this, (*it)->get(i)))
                        continue;
                    (*it)->set(i, FP::undefined());
                }
            }
        } else {
            for (QVector<std::shared_ptr<TransformerPoint> >::const_iterator
                    it = points.constBegin(), end = points.constEnd(); it != end; ++it) {
                if ((*it)->totalDimensions() <= dimension)
                    continue;
                if ((*function)(this, (*it)->get(dimension)))
                    continue;
                (*it)->set(dimension, FP::undefined());
            }
        }
    }
}

void RangeFilter::applyConst(QVector<TransformerPoint *> &points,
                             TransformerAllocator *allocator) const
{
    Q_UNUSED(allocator);
    if (function == NULL)
        return;

    if (flags & Reject_Remove) {
        if (dimension == -1) {
            for (QVector<TransformerPoint *>::iterator it = points.begin(); it != points.end();) {
                if (allDimensionsPassed(this, function, *it)) {
                    ++it;
                    continue;
                }
                delete *it;
                it = points.erase(it);
            }
        } else {
            for (QVector<TransformerPoint *>::iterator it = points.begin(); it != points.end();) {
                if ((*it)->totalDimensions() <= dimension) {
                    delete *it;
                    it = points.erase(it);
                    continue;
                }
                if ((*function)(this, (*it)->get(dimension))) {
                    ++it;
                    continue;
                }
                delete *it;
                it = points.erase(it);
            }
        }
    } else {
        if (dimension == -1) {
            for (QVector<TransformerPoint *>::const_iterator it = points.constBegin(),
                    end = points.constEnd(); it != end; ++it) {
                for (int i = 0, nDims = (*it)->totalDimensions(); i < nDims; ++i) {
                    if ((*function)(this, (*it)->get(i)))
                        continue;
                    (*it)->set(i, FP::undefined());
                }
            }
        } else {
            for (QVector<TransformerPoint *>::const_iterator it = points.constBegin(),
                    end = points.constEnd(); it != end; ++it) {
                if ((*it)->totalDimensions() <= dimension)
                    continue;
                if ((*function)(this, (*it)->get(dimension)))
                    continue;
                (*it)->set(dimension, FP::undefined());
            }
        }
    }
}

Transformer *RangeFilter::clone() const
{ return new RangeFilter(min, max, flags, dimension); }

RangeFilter::RangeFilter(QDataStream &stream)
{
    quint8 f = 0;
    qint32 d = 0;
    stream >> min >> max >> f >> d;
    flags = (int) f;
    dimension = (int) d;
    setFunction();
}

void RangeFilter::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Transformer::SerialType_RangeFilter;
    quint8 f = (quint32) flags;
    qint32 d = (qint32) dimension;
    stream << t << min << max << f << d;
}

void RangeFilter::printLog(QDebug &stream) const
{
    stream << "RangeFilter(" << min << ',' << max << ',' << hex << flags << ',' << dimension << ')';
}


}
}
