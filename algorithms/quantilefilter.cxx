/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVector>

#include "algorithms/quantilefilter.hxx"
#include "algorithms/statistics.hxx"

using namespace CPD3::Algorithms::Internal;

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/quantilefilter.hxx
 * Provides a filter that accepts or removes data based on the quantiles
 * of the data set.
 */

namespace Internal {
struct QuantileFilterData {
    double min;
    double max;
};
}

static bool acceptMin(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value > filter.min;
}

static bool acceptMinInclusive(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value >= filter.min;
}

static bool acceptMax(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value < filter.max;
}

static bool acceptMaxInclusive(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value <= filter.max;
}

static bool acceptMinMax(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value > filter.min && value < filter.max;
}

static bool acceptMinInclusiveMax(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value >= filter.min && value < filter.max;
}

static bool acceptMinMaxInclusive(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value > filter.min && value <= filter.max;
}

static bool acceptMinInclusiveMaxInclusive(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value >= filter.min && value <= filter.max;
}

static bool rejectMin(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value < filter.min;
}

static bool rejectMinInclusive(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value <= filter.min;
}

static bool rejectMax(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value > filter.max;
}

static bool rejectMaxInclusive(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value >= filter.max;
}

static bool rejectMinMax(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value < filter.min || value > filter.max;
}

static bool rejectMinInclusiveMax(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value <= filter.min || value > filter.max;
}

static bool rejectMinMaxInclusive(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value < filter.min || value >= filter.max;
}

static bool rejectMinInclusiveMaxInclusive(const QuantileFilterData &filter, double value)
{
    if (!FP::defined(value))
        return false;
    return value <= filter.min || value >= filter.max;
}


void QuantileFilter::setFunction()
{
    if (!FP::defined(max) || max < 0.0 || max > 1.0) {
        if (!FP::defined(min) || min < 0.0 || min > 1.0) {
            function = NULL;
        } else if (flags & Min_Inclusive) {
            if (flags & Reject_Range)
                function = &rejectMinInclusive;
            else
                function = &acceptMinInclusive;
        } else {
            if (flags & Reject_Range)
                function = &rejectMin;
            else
                function = &acceptMin;
        }
    } else if (!FP::defined(min) || min < 0.0 || min > 1.0) {
        if (flags & Max_Inclusive) {
            if (flags & Reject_Range)
                function = &rejectMaxInclusive;
            else
                function = &acceptMaxInclusive;
        } else {
            if (flags & Reject_Range)
                function = &rejectMax;
            else
                function = &acceptMax;
        }
    } else {
        if ((flags & Min_Inclusive) && (flags & Max_Inclusive)) {
            if (flags & Reject_Range)
                function = &rejectMinInclusiveMaxInclusive;
            else
                function = &acceptMinInclusiveMaxInclusive;
        } else if (flags & Min_Inclusive) {
            if (flags & Reject_Range)
                function = &rejectMinInclusiveMax;
            else
                function = &acceptMinInclusiveMax;
        } else if (flags & Max_Inclusive) {
            if (flags & Reject_Range)
                function = &rejectMinMaxInclusive;
            else
                function = &acceptMinMaxInclusive;
        } else {
            if (flags & Reject_Range)
                function = &rejectMinMax;
            else
                function = &acceptMinMax;
        }
    }
}

QuantileFilter::QuantileFilter(double setMin, double setMax, int setFlags, int onlyDimension) : min(
        setMin), max(setMax), flags(setFlags), dimension(onlyDimension), function(NULL)
{
    setFunction();
}

QuantileFilter::~QuantileFilter()
{ }

template<typename PointType>
static bool allDimensionsPassed(const std::vector<QuantileFilterData> &filter,
                                QuantileFilterTestFunction function,
                                const PointType &point)
{
    for (int i = 0, nDims = point->totalDimensions(); i < nDims; ++i) {
        Q_ASSERT((size_t) i < filter.size());
        if ((*function)(filter[i], point->get(i)))
            continue;
        return false;
    }
    return true;
}

template<typename PointType>
static bool buildSingleDimension(const QVector<PointType> &points,
                                 int dimension,
                                 double min,
                                 double max,
                                 QuantileFilterData &target)
{
    std::vector<double> values;
    values.reserve(points.size());
    for (typename QVector<PointType>::const_iterator it = points.constBegin(),
            end = points.constEnd(); it != end; ++it) {
        if ((*it)->totalDimensions() <= dimension)
            continue;
        double add = (*it)->get(dimension);
        if (!FP::defined(add))
            continue;
        values.push_back(add);
    }
    if (values.empty())
        return false;
    std::sort(values.begin(), values.end());
    if (FP::defined(min)) {
        target.min = Statistics::quantile(values, min);
        Q_ASSERT(FP::defined(target.min));
    }
    if (FP::defined(max)) {
        target.max = Statistics::quantile(values, max);
        Q_ASSERT(FP::defined(target.max));
    }
    return true;
}

template<typename PointType>
static bool buildAllDimensions(const QVector<PointType> &points,
                               double min,
                               double max,
                               std::vector<QuantileFilterData> &target)
{
    std::vector<std::vector<double> > values;
    for (typename QVector<PointType>::const_iterator it = points.constBegin(),
            end = points.constEnd(); it != end; ++it) {
        int nDims = (*it)->totalDimensions();
        while ((size_t) nDims > values.size()) {
            values.push_back(std::vector<double>());
            values.back().reserve(points.size());
        }
        for (int i = 0; i < nDims; ++i) {
            double add = (*it)->get(i);
            if (!FP::defined(add))
                continue;
            values[i].push_back(add);
        }
    }
    if (values.empty())
        return false;
    target.clear();
    target.reserve(values.size());
    for (std::vector<std::vector<double> >::iterator dim = values.begin(), endDim = values.end();
            dim != endDim;
            ++dim) {
        if (dim->empty())
            return false;
        std::sort(dim->begin(), dim->end());

        target.push_back(QuantileFilterData());
        if (FP::defined(min)) {
            target.back().min = Statistics::quantile(*dim, min);
            Q_ASSERT(FP::defined(target.back().min));
        }
        if (FP::defined(max)) {
            target.back().max = Statistics::quantile(*dim, max);
            Q_ASSERT(FP::defined(target.back().max));
        }
    }
    return true;
}

template<typename PointType>
static void invalidateDimension(QVector<PointType> &points, int dimension)
{
    for (typename QVector<PointType>::const_iterator it = points.constBegin(),
            end = points.constEnd(); it != end; ++it) {
        if (dimension >= (*it)->totalDimensions())
            continue;
        (*it)->set(dimension, FP::undefined());
    }
}

template<typename PointType>
static void invalidateAllDimensions(QVector<PointType> &points)
{
    for (typename QVector<PointType>::const_iterator it = points.constBegin(),
            end = points.constEnd(); it != end; ++it) {
        for (int i = 0, nDims = (*it)->totalDimensions(); i < nDims; ++i) {
            (*it)->set(i, FP::undefined());
        }
    }
}

void QuantileFilter::applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                                TransformerAllocator *allocator) const
{
    Q_UNUSED(allocator);
    if (function == NULL)
        return;

    if (flags & Reject_Remove) {
        if (dimension == -1) {
            std::vector<QuantileFilterData> data;
            if (!buildAllDimensions(points, min, max, data)) {
                points.clear();
                return;
            }
            for (QVector<std::shared_ptr<TransformerPoint> >::iterator it = points.begin();
                    it != points.end();) {
                if (allDimensionsPassed(data, function, *it)) {
                    ++it;
                    continue;
                }
                it = points.erase(it);
            }
        } else {
            QuantileFilterData data;
            if (!buildSingleDimension(points, dimension, min, max, data)) {
                points.clear();
                return;
            }
            for (QVector<std::shared_ptr<TransformerPoint> >::iterator it = points.begin();
                    it != points.end();) {
                if ((*it)->totalDimensions() <= dimension) {
                    it = points.erase(it);
                    continue;
                }
                if ((*function)(data, (*it)->get(dimension))) {
                    ++it;
                    continue;
                }
                it = points.erase(it);
            }
        }
    } else {
        if (dimension == -1) {
            std::vector<QuantileFilterData> data;
            if (!buildAllDimensions(points, min, max, data)) {
                invalidateAllDimensions(points);
                return;
            }
            for (QVector<std::shared_ptr<TransformerPoint> >::const_iterator
                    it = points.constBegin(), end = points.constEnd(); it != end; ++it) {
                for (int i = 0, nDims = (*it)->totalDimensions(); i < nDims; ++i) {
                    Q_ASSERT((size_t) i < data.size());
                    if ((*function)(data[i], (*it)->get(i)))
                        continue;
                    (*it)->set(i, FP::undefined());
                }
            }
        } else {
            QuantileFilterData data;
            if (!buildSingleDimension(points, dimension, min, max, data)) {
                invalidateDimension(points, dimension);
                return;
            }
            for (QVector<std::shared_ptr<TransformerPoint> >::const_iterator
                    it = points.constBegin(), end = points.constEnd(); it != end; ++it) {
                if ((*it)->totalDimensions() <= dimension)
                    continue;
                if ((*function)(data, (*it)->get(dimension)))
                    continue;
                (*it)->set(dimension, FP::undefined());
            }
        }
    }
}

void QuantileFilter::applyConst(QVector<TransformerPoint *> &points,
                                TransformerAllocator *allocator) const
{
    Q_UNUSED(allocator);
    if (function == NULL)
        return;


    if (flags & Reject_Remove) {
        if (dimension == -1) {
            std::vector<QuantileFilterData> data;
            if (!buildAllDimensions(points, min, max, data)) {
                points.clear();
                return;
            }
            for (QVector<TransformerPoint *>::iterator it = points.begin(); it != points.end();) {
                if (allDimensionsPassed(data, function, *it)) {
                    ++it;
                    continue;
                }
                delete *it;
                it = points.erase(it);
            }
        } else {
            QuantileFilterData data;
            if (!buildSingleDimension(points, dimension, min, max, data)) {
                points.clear();
                return;
            }
            for (QVector<TransformerPoint *>::iterator it = points.begin(); it != points.end();) {
                if ((*it)->totalDimensions() <= dimension) {
                    delete *it;
                    it = points.erase(it);
                    continue;
                }
                if ((*function)(data, (*it)->get(dimension))) {
                    ++it;
                    continue;
                }
                delete *it;
                it = points.erase(it);
            }
        }
    } else {
        if (dimension == -1) {
            std::vector<QuantileFilterData> data;
            if (!buildAllDimensions(points, min, max, data)) {
                invalidateAllDimensions(points);
                return;
            }
            for (QVector<TransformerPoint *>::const_iterator it = points.constBegin(),
                    end = points.constEnd(); it != end; ++it) {
                for (int i = 0, nDims = (*it)->totalDimensions(); i < nDims; ++i) {
                    Q_ASSERT((size_t) i < data.size());
                    if ((*function)(data[i], (*it)->get(i)))
                        continue;
                    (*it)->set(i, FP::undefined());
                }
            }
        } else {
            QuantileFilterData data;
            if (!buildSingleDimension(points, dimension, min, max, data)) {
                invalidateDimension(points, dimension);
                return;
            }
            for (QVector<TransformerPoint *>::const_iterator it = points.constBegin(),
                    end = points.constEnd(); it != end; ++it) {
                if ((*it)->totalDimensions() <= dimension)
                    continue;
                if ((*function)(data, (*it)->get(dimension)))
                    continue;
                (*it)->set(dimension, FP::undefined());
            }
        }
    }
}

Transformer *QuantileFilter::clone() const
{ return new QuantileFilter(min, max, flags, dimension); }

QuantileFilter::QuantileFilter(QDataStream &stream)
{
    quint8 f = 0;
    qint32 d = 0;
    stream >> min >> max >> f >> d;
    flags = (int) f;
    dimension = (int) d;
    setFunction();
}

void QuantileFilter::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Transformer::SerialType_QuantileFilter;
    quint8 f = (quint32) flags;
    qint32 d = (qint32) dimension;
    stream << t << min << max << f << d;
}

void QuantileFilter::printLog(QDebug &stream) const
{
    stream << "QuantileFilter(" << min << ',' << max << ',' << hex << flags << ',' << dimension
           << ")";
}


}
}
