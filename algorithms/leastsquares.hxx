/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSLEASTSQUARES_H
#define CPD3ALGORITHMSLEASTSQUARES_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QList>
#include <QDebug>

#include "algorithms/algorithms.hxx"

#include "algorithms/model.hxx"


namespace CPD3 {
namespace Algorithms {

/**
 * A simple two dimensional (f(x) = m * x + b) ordinary least squares
 * fit.
 */
class CPD3ALGORITHMS_EXPORT BasicOrdinaryLeastSquares : public Model {
    int n;
    double m;
    double b;
    double r2;
    double sm;
    double sb;
    ModelOutputConstraints constraints;

    inline double evaluate(double input) const
    { return constraints.apply(m * input + b); }

    BasicOrdinaryLeastSquares(const BasicOrdinaryLeastSquares &other);

public:
    virtual ~BasicOrdinaryLeastSquares();

    BasicOrdinaryLeastSquares(const QVector<double> &x,
                              const QVector<double> &y,
                              const ModelOutputConstraints &output = ModelOutputConstraints());

    virtual QVector<double> applyConst(const QVector<double> &inputs) const;

    virtual QVector<double> apply(const QVector<double> &inputs);

    virtual void applyIOConst(QVector<double> &values) const;

    virtual void applyIO(QVector<double> &values);

    virtual double oneToOneConst(double input) const;

    virtual double oneToOne(double input);

    virtual double twoToOneConst(double a, double b) const;

    virtual double twoToOne(double a, double b);

    virtual double threeToOneConst(double a, double b, double c) const;

    virtual double threeToOne(double a, double b, double c);

    virtual void applyIOConst(double &a, double &b) const;

    virtual void applyIO(double &a, double &b);

    virtual void applyIOConst(double &a, double &b, double &c) const;

    virtual void applyIO(double &a, double &b, double &c);

    virtual Model *clone() const;

    virtual QString describe() const;

    virtual ModelLegendEntry
            legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    inline double slope() const
    { return m; }

    inline double intercept() const
    { return b; }

    inline double rSquared() const
    { return r2; }

    double slopeConfidence(double confidence = 0.95) const;

    double interceptConfidence(double confidence = 0.95) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

protected:
    BasicOrdinaryLeastSquares(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

/**
 * A simple two dimensional (f(x) = m * x + 0) ordinary least squares
 * fit.  That is, the intercept is forced to be zero.
 */
class CPD3ALGORITHMS_EXPORT ZeroOrdinaryLeastSquares : public Model {
    int n;
    double m;
    double r2;
    double sm;
    ModelOutputConstraints constraints;

    inline double evaluate(double input) const
    { return constraints.apply(m * input); }

    ZeroOrdinaryLeastSquares(const ZeroOrdinaryLeastSquares &other);

public:
    virtual ~ZeroOrdinaryLeastSquares();

    ZeroOrdinaryLeastSquares(const QVector<double> &x,
                             const QVector<double> &y,
                             const ModelOutputConstraints &output = ModelOutputConstraints());

    virtual QVector<double> applyConst(const QVector<double> &inputs) const;

    virtual QVector<double> apply(const QVector<double> &inputs);

    virtual void applyIOConst(QVector<double> &values) const;

    virtual void applyIO(QVector<double> &values);

    virtual double oneToOneConst(double input) const;

    virtual double oneToOne(double input);

    virtual double twoToOneConst(double a, double b) const;

    virtual double twoToOne(double a, double b);

    virtual double threeToOneConst(double a, double b, double c) const;

    virtual double threeToOne(double a, double b, double c);

    virtual void applyIOConst(double &a, double &b) const;

    virtual void applyIO(double &a, double &b);

    virtual void applyIOConst(double &a, double &b, double &c) const;

    virtual void applyIO(double &a, double &b, double &c);

    virtual Model *clone() const;

    virtual QString describe() const;

    virtual ModelLegendEntry
            legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    inline double slope() const
    { return m; }

    inline double rSquared() const
    { return r2; }

    double slopeConfidence(double confidence = 0.95) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

protected:
    ZeroOrdinaryLeastSquares(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};


}
}

#endif
