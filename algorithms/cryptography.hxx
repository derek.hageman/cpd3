/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSCRPTOGRAPHY_H
#define CPD3ALGORITHMSCRPTOGRAPHY_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QString>
#include <QByteArray>
#include <QList>
#include <QStringList>
#include <QTimer>
#include <QCryptographicHash>

class QSslSocket;

class QSslCertificate;

class QSslKey;

class QSslConfiguration;

#include "algorithms/algorithms.hxx"
#include "datacore/variant/root.hxx"
#include "core/component.hxx"
#include "io/access.hxx"

namespace CPD3 {
namespace Algorithms {

/**
 * Provides general routines for cryptography used in CPD3.
 */
class CPD3ALGORITHMS_EXPORT Cryptography {
public:
    /**
     * Setup an SSL configuration.
     *
     * @param socket    the target socket
     * @param config    the source configuration
     * @param loadExternal  set to allow loading of externally stored certificates and keys
     * @return          true on success
     */
    static bool configureSSL(QSslConfiguration &ssl,
                             const Data::Variant::Read &config,
                             bool loadExternal = true);

    /**
     * Setup an SSL socket from a given configuration.
     *
     * @param socket    the target socket
     * @param config    the source configuration
     * @param loadExternal  set to allow loading of externally stored certificates and keys
     * @return          true on success
     */
    static bool setupSocket(QSslSocket *socket,
                            const Data::Variant::Read &config,
                            bool loadExternal = true);

    /**
     * Get the certificate associated with the given configuration.
     *
     * @param config    the source configuration
     * @param loadExternal  set to allow loading of external files
     * @return          the certificate
     */
    static QSslCertificate getCertificate(const Data::Variant::Read &config,
                                          bool loadExternal = true);

    /**
     * Get the certificates associated with the given configuration.
     *
     * @param config    the source configuration
     * @param loadExternal  set to allow loading of external files
     * @return          the list of certificates
     */
    static QList<QSslCertificate> getMultipleCertificates(const Data::Variant::Read &config,
                                                          bool loadExternal = true);

    /**
     * Get the key associated with the given configuration.
     *
     * @param config        the source configuration
     * @param loadExternal  set to allow loading of external files
     * @param password      the password for the key
     */
    static QSslKey getKey(const Data::Variant::Read &config,
                          bool loadExternal = true,
                          const QString &password = QString());


    /**
     * Verify a certificate against the given configuration.
     *
     * @param certificate   the certificate to verify
     * @param config        the configuration
     * @param loadExternal  set to allow loading of external files
     * @return              true if the certificate is valid
     */
    static bool verifyCertificate(const QSslCertificate &certificate,
                                  const Data::Variant::Read &config,
                                  bool loadExternal = true);

    /**
     * Verify a certificate against the given configuration.
     *
     * @param certificate   the certificate to verify
     * @param config        the configuration
     * @param loadExternal  set to allow loading of external files
     * @return              true if the certificate is valid
     */
    static bool verifyCertificate(const Data::Variant::Read &certificate,
                                  const Data::Variant::Read &config,
                                  bool loadExternal = true);

    /**
     * Return the SHA-512 digest of the given data.
     *
     * @param data      the input data
     * @return          the SHA-512 digest
     */
    static QByteArray sha512(const QByteArray &data);

    /**
     * Return the SHA-512 digest of the given device.
     *
     * @param uboyt     the input data
     * @return          the SHA-512 digest
     */
    static Util::ByteArray sha512(std::unique_ptr<IO::Generic::Stream> &&input);

    /**
     * Perform a certificate digest (SHA2-512).
     *
     * @param certificate   the certificate
     * @return              the digest
     */
    static QByteArray sha512(const QSslCertificate &certificate);

    /**
     * Sign the given hash with the given private key.
     *
     * @param key       the private key
     * @param data      the data to sign
     * @param loadExternal  set to allow loading of externally stored keys
     * @param password      the password for the key
     * @return          the signature data
     */
    static Util::ByteArray signHash(const Data::Variant::Read &key,
                                    const Util::ByteView &hash,
                                    bool loadExternal,
                                    const std::string &password = {});

    /**
     * Sign the given data with the given private key.
     *
     * @param key       the private key
     * @param data      the data to sign
     * @param loadExternal  set to allow loading of externally stored keys
     * @param password      the password for the key
     * @return          the signature data
     */
    static Util::ByteArray signData(const Data::Variant::Read &key,
                                    std::unique_ptr<IO::Generic::Stream> &&data,
                                    bool loadExternal = true,
                                    const std::string &password = {});

    /**
     * Check the signature of the given hash.
     *
     * @param certificate   the certificate of the signer
     * @param signature     the signature data
     * @param data          the data to check
     * @param loadExternal  set to allow loading of externally stored certificates
     * @return              true if the signature matches
     */
    static bool checkHashSignature(const Data::Variant::Read &certificate,
                                   const Util::ByteView &signature,
                                   const Util::ByteView &hash,
                                   bool loadExternal = false);

    /**
     * Check the signature of the given data.
     *
     * @param certificate   the certificate of the signer
     * @param signature     the signature data
     * @param data          the data to check
     * @param loadExternal  set to allow loading of externally stored certificates
     * @return              true if the signature matches
     */
    static bool checkSignature(const Data::Variant::Read &certificate,
                               const Util::ByteView &signature,
                               std::unique_ptr<IO::Generic::Stream> &&data,
                               bool loadExternal = false);

    /**
     * Encrypt data.
     *
     * @param origin        the key or certificate to use
     * @param input         the input to read from
     * @param output        the output to write to
     * @param usePrivate    if set then encrypt using the origin as a private key
     * @param loadExternal  set to allow loading of externally stored origins
     * @param password      the key password, if required
     */
    static bool encrypt(const Data::Variant::Read &origin,
                        std::unique_ptr<IO::Generic::Stream> &&input,
                        IO::Generic::Writable &output,
                        bool usePrivate = false,
                        bool loadExternal = false,
                        const std::string &password = {});

    /**
     * Encrypt the given buffer of data with a private key.
     *
     * @param key       the private key
     * @param data      the input data to encrypt
     * @param loadExternal  set to allow loading of externally stored keys
     * @param password      the password for the key
     * @return          the encrypted data
     */
    static Util::ByteArray encryptData(const Data::Variant::Read &key,
                                       const Util::ByteView &data,
                                       bool loadExternal = true,
                                       const std::string &password = {});

    /**
     * Decrypt data.
     *
     * @param origin        the key or certificate to use
     * @param input         the input to read from
     * @param output        the output to write to
     * @param usePrivate    if set then decrypt using the origin as a private key
     * @param loadExternal  set to allow loading of externally stored origins
     * @param password      the key password, if required
     */
    static bool decrypt(const Data::Variant::Read &origin,
                        std::unique_ptr<IO::Generic::Stream> &&input,
                        IO::Generic::Writable &output,
                        bool usePrivate = true,
                        bool loadExternal = true,
                        const std::string &password = {});

    /**
     * Decrypt the given buffer of data with a certificate.
     *
     * @param certificate   the certificate of corresponding to the private key used in encryption
     * @param data          the input data to decrypt
     * @param loadExternal  set to allow loading of externally stored certificates
     * @return              the decrypted plain text
     */
    static Util::ByteArray decryptData(const Data::Variant::Read &certificate,
                                       const Util::ByteArray &data,
                                       bool loadExternal = true);

    /**
     * Generate a self signed certificate and key pair.
     *
     * @param subject   the list of subject fields
     * @param bits      the number of private key bits
     * @return          true on success
     */
    static CPD3::Data::Variant::Root generateSelfSigned(const QStringList &subject = QStringList(
            "CN=CPD3"), int bits = 2048);
};

/** 
 * An option that specifies the private key and public certificate for 
 * a SSL socket.
 */
class CPD3ALGORITHMS_EXPORT SSLAuthenticationOption : public ComponentOptionBase {
Q_OBJECT

private:
    QString key;
    QString certificate;
public:
    SSLAuthenticationOption(const QString &argumentName,
                            const QString &displayName,
                            const QString &description,
                            const QString &defaultBehavior,
                            int sortPriority = 0);

    SSLAuthenticationOption(const SSLAuthenticationOption &other);

    virtual void reset();

    virtual ComponentOptionBase *clone() const;

    CPD3::Data::Variant::Root getLocalConfiguration() const;

    void setKey(const QString &key);

    void setCertificate(const QString &certificate);
};

}
}

#endif
