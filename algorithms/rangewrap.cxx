/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <vector>
#include <QVector>

#include "algorithms/rangewrap.hxx"

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/rangewrap.hxx
 * Provides a transformer that inserts wrapped extended values at predicted
 * wrap around points.
 */

RangeWrap::RangeWrap(double setMin, double setMax, int onlyDimension) : min(setMin),
                                                                        max(setMax),
                                                                        dimension(onlyDimension)
{
    Q_ASSERT(FP::defined(min));
    Q_ASSERT(FP::defined(max));
    Q_ASSERT(min < max);

    mid = (min + max) * 0.5;
    range = (max - min);
}

RangeWrap::~RangeWrap()
{ }

double RangeWrap::applyWrap(double value) const
{
    if (!FP::defined(value))
        return value;
    if (value < mid) {
        value += range;
    } else {
        value -= range;
    }
    return value;
}

void RangeWrap::applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                           TransformerAllocator *allocator) const
{
    if (points.size() < 2)
        return;

    auto prior = points.first();
    if (dimension == -1) {
        for (QVector<std::shared_ptr<TransformerPoint> >::iterator it = points.begin() + 1;
                it != points.end();
                prior = *it, ++it) {
            int nDim = prior->totalDimensions();
            if ((*it)->totalDimensions() != nDim)
                continue;

            double originalDistance = 0;
            double wrappedDistance = 0;
            for (int i = 0; i < nDim; i++) {
                double v = (*it)->get(i);
                double p = prior->get(i);
                if (!FP::defined(v) || !FP::defined(p))
                    continue;

                double d = v - p;
                originalDistance += d * d;

                v = applyWrap(v);
                d = v - p;
                wrappedDistance += d * d;
            }

            if (originalDistance <= wrappedDistance)
                continue;

            std::shared_ptr<TransformerPoint> wrapCurrent(allocator->copy(it->get()));
            std::shared_ptr<TransformerPoint> wrapPrior(allocator->copy(prior.get()));
            std::shared_ptr<TransformerPoint> wrapBreak(allocator->copy(it->get()));

            for (int i = 0; i < nDim; i++) {
                wrapCurrent->set(i, applyWrap(wrapCurrent->get(i)));
                wrapPrior->set(i, applyWrap(wrapPrior->get(i)));
                wrapBreak->set(i, FP::undefined());
            }

            it = points.insert(it, wrapCurrent);
            it = points.insert(it + 1, wrapBreak);
            it = points.insert(it + 1, wrapPrior);
            ++it;
        }
    } else {
        for (QVector<std::shared_ptr<TransformerPoint> >::iterator it = points.begin() + 1;
                it != points.end();
                prior = *it, ++it) {
            int nDim = prior->totalDimensions();
            if (nDim <= dimension)
                continue;
            if ((*it)->totalDimensions() <= dimension)
                continue;

            double v = (*it)->get(dimension);
            double p = prior->get(dimension);
            if (!FP::defined(v) || !FP::defined(p))
                continue;
            double w = applyWrap(v);
            if (fabs(w - p) >= fabs(v - p))
                continue;

            std::shared_ptr<TransformerPoint> wrapCurrent(allocator->copy(it->get()));
            std::shared_ptr<TransformerPoint> wrapPrior(allocator->copy(prior.get()));
            std::shared_ptr<TransformerPoint> wrapBreak(allocator->copy(it->get()));

            wrapCurrent->set(dimension, w);
            wrapPrior->set(dimension, applyWrap(wrapPrior->get(dimension)));
            for (int i = 0; i < nDim; i++) {
                wrapBreak->set(i, FP::undefined());
            }

            it = points.insert(it, wrapCurrent);
            it = points.insert(it + 1, wrapBreak);
            it = points.insert(it + 1, wrapPrior);
            ++it;
        }
    }
}

void RangeWrap::applyConst(QVector<TransformerPoint *> &points,
                           TransformerAllocator *allocator) const
{
    if (points.size() < 2)
        return;

    TransformerPoint *prior = points.first();
    if (dimension == -1) {
        for (QVector<TransformerPoint *>::iterator it = points.begin() + 1;
                it != points.end();
                prior = *it, ++it) {
            int nDim = prior->totalDimensions();
            if ((*it)->totalDimensions() != nDim)
                continue;

            double originalDistance = 0;
            double wrappedDistance = 0;
            for (int i = 0; i < nDim; i++) {
                double v = (*it)->get(i);
                double p = prior->get(i);
                if (!FP::defined(v) || !FP::defined(p))
                    continue;

                double d = v - p;
                originalDistance += d * d;

                v = applyWrap(v);
                d = v - p;
                wrappedDistance += d * d;
            }

            if (originalDistance <= wrappedDistance)
                continue;

            TransformerPoint *wrapCurrent = allocator->copy(*it);
            TransformerPoint *wrapPrior = allocator->copy(prior);
            TransformerPoint *wrapBreak = allocator->copy(*it);

            for (int i = 0; i < nDim; i++) {
                wrapCurrent->set(i, applyWrap(wrapCurrent->get(i)));
                wrapPrior->set(i, applyWrap(wrapPrior->get(i)));
                wrapBreak->set(i, FP::undefined());
            }

            it = points.insert(it, wrapCurrent);
            it = points.insert(it + 1, wrapBreak);
            it = points.insert(it + 1, wrapPrior);
            ++it;
        }
    } else {
        for (QVector<TransformerPoint *>::iterator it = points.begin() + 1;
                it != points.end();
                prior = *it, ++it) {
            int nDim = prior->totalDimensions();
            if (nDim <= dimension)
                continue;
            if ((*it)->totalDimensions() <= dimension)
                continue;

            double v = (*it)->get(dimension);
            double p = prior->get(dimension);
            if (!FP::defined(v) || !FP::defined(p))
                continue;
            double w = applyWrap(v);
            if (fabs(w - p) >= fabs(v - p))
                continue;

            TransformerPoint *wrapCurrent = allocator->copy(*it);
            TransformerPoint *wrapPrior = allocator->copy(prior);
            TransformerPoint *wrapBreak = allocator->copy(*it);

            wrapCurrent->set(dimension, w);
            wrapPrior->set(dimension, applyWrap(wrapPrior->get(dimension)));
            for (int i = 0; i < nDim; i++) {
                wrapBreak->set(i, FP::undefined());
            }

            it = points.insert(it, wrapCurrent);
            it = points.insert(it + 1, wrapBreak);
            it = points.insert(it + 1, wrapPrior);
            ++it;
        }
    }
}

Transformer *RangeWrap::clone() const
{ return new RangeWrap(min, max, dimension); }

RangeWrap::RangeWrap(QDataStream &stream)
{
    qint32 d = 0;
    stream >> min >> max >> d;
    dimension = (int) d;

    mid = (min + max) * 0.5;
    range = (max - min);
}

void RangeWrap::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Transformer::SerialType_RangeWrap;
    qint32 d = (qint32) dimension;
    stream << t << min << max << d;
}

void RangeWrap::printLog(QDebug &stream) const
{
    stream << "RangeWrap(" << min << ',' << max << ',' << dimension << ')';
}


}
}
