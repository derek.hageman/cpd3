/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <QVector>

#include "algorithms/allan.hxx"
#include "algorithms/statistics.hxx"

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/allan.hxx
 * Provides routines for generating Allan plot transforms.
 */

/* Need fast and repeatable random numbers, so just use a simple XOR-shift */

template<bool doDelete, typename PointType>
static QVector<PointType> transformToAllan(const QVector<PointType> &points,
                                           TransformerAllocator *allocator,
                                           const Transformer *tr,
                                           int nSamplePoints = 500,
                                           double logBase = 60.0,
                                           int pointsPerBase = 60)
{
    if (points.size() < 2)
        return QVector<PointType>();

    double minimumInterval;
    double maximumInterval;
    double totalInterval = (points.last()->get(0) - points.first()->get(0));
    int maximumDimensions = points.first()->totalDimensions();
    double meanInterval = 0.0;
    for (typename QVector<PointType>::const_iterator it = points.constBegin() + 1,
            end = points.constEnd(); it != end; ++it) {
        maximumDimensions = qMax(maximumDimensions, (*it)->totalDimensions());
        meanInterval += (*it)->get(0) - (*(it - 1))->get(0);
    }
    if (maximumDimensions < 2)
        return QVector<PointType>();
    meanInterval /= (double) points.size();
    if (meanInterval < 1E-3)
        meanInterval = 1E-3;

    minimumInterval = totalInterval / (double) points.size();
    if (minimumInterval <= 1E-3)
        minimumInterval = 1E-3;
    maximumInterval = totalInterval;
    if (maximumInterval <= minimumInterval)
        maximumInterval = minimumInterval;

    QVector<PointType> result;
    std::vector<double> samplePoints;
    std::vector<double> addValues;
    samplePoints.reserve(nSamplePoints);
    addValues.reserve(maximumDimensions - 1);

    std::vector<int> previousSignature;
    std::vector<int> addSignature;
    std::vector<bool> skipDimensions(maximumDimensions, false);
    previousSignature.reserve(nSamplePoints * maximumDimensions * 2 + maximumDimensions);
    addSignature.reserve(nSamplePoints * maximumDimensions * 2 + maximumDimensions);

    double intervalOffset = log(minimumInterval) / log(logBase);
    double sampleInterval = minimumInterval;
    for (int itter = 1;
            itter <= pointsPerBase * 128 && sampleInterval <= maximumInterval;
            ++itter) {
        bool anyValid = false;

        int nExpectedPoints = (int) floor(sampleInterval / meanInterval);
        if (nExpectedPoints < 1)
            nExpectedPoints = 1;

        addSignature.clear();
        addValues.clear();
        for (int dimension = 1; dimension < maximumDimensions; dimension++) {
            addSignature.push_back(-dimension);
            if (skipDimensions[dimension])
                continue;

            samplePoints.clear();
            typename QVector<PointType>::const_iterator startPoints = points.constBegin();
            typename QVector<PointType>::const_iterator endPoints = points.constEnd();
            int maximumOffset = 0;
            {
                typename QVector<PointType>::const_iterator addPoint = endPoints - 1;
                typename QVector<PointType>::const_iterator endAdd = startPoints - 1;

                double breakTime = (*addPoint)->get(0) - sampleInterval;
                double sum = 0.0;
                int count = 0;
                for (--addPoint; addPoint != endAdd; --addPoint, ++maximumOffset) {
                    if ((*addPoint)->get(0) < breakTime)
                        break;
                    if (dimension >= (*addPoint)->totalDimensions())
                        continue;
                    double v = (*addPoint)->get(dimension);
                    if (!FP::defined(v))
                        continue;
                    sum += v;
                    ++count;
                }
                /* Consumed the entire list, so can't possibly add anything */
                if (addPoint == endAdd) {
                    skipDimensions[dimension] = true;
                    continue;
                }
                if (count > 0) {
                    samplePoints.push_back(sum / (double) count);
                }
            }

            maximumOffset = (endPoints - startPoints) - maximumOffset;
            if (maximumOffset < nSamplePoints / 2) {
                skipDimensions[dimension] = true;
                continue;
            }

            /* Sample the same starting points for the same expected number
             * per interval, so that we remove duplicate intervals.  This
             * will get a different signature when the threshold is enough
             * to push it to adding another point to a bin, so we'll generate
             * a new output point then. */
            quint32 randomOffset = dimension + maximumOffset + nExpectedPoints;
            randomOffset = Random::xorshift32(randomOffset);
            randomOffset = Random::xorshift32(randomOffset);
            randomOffset = Random::xorshift32(randomOffset);

            for (int indexSample = 0; indexSample < nSamplePoints; ++indexSample) {
                randomOffset = Random::xorshift32(randomOffset);
                int startSample = (int) (((quint64) (randomOffset - 1) * (quint64) maximumOffset) /
                        Q_UINT64_C(0xFFFFFFFF));
                typename QVector<PointType>::const_iterator addPoint = startPoints + startSample;
                if (addPoint == endPoints)
                    continue;

                double breakTime = (*addPoint)->get(0) + sampleInterval;
                double sum = 0.0;
                int count = 0;
                for (; addPoint != endPoints; ++addPoint) {
                    if ((*addPoint)->get(0) > breakTime)
                        break;
                    if (dimension >= (*addPoint)->totalDimensions())
                        continue;
                    double v = (*addPoint)->get(dimension);
                    if (!FP::defined(v))
                        continue;
                    sum += v;
                    ++count;
                }
                /* Ran out of points before the end of the interval, skip */
                if (addPoint == endPoints)
                    continue;

                if (count > 0) {
                    addSignature.push_back(startSample);
                    addSignature.push_back(addPoint - startPoints);
                    samplePoints.push_back(sum / (double) count);
                }
            }

            if ((int) samplePoints.size() < nSamplePoints / 4) {
                addValues.push_back(FP::undefined());
                continue;
            }

            double sd = Statistics::sd(samplePoints);
            if (!FP::defined(sd)) {
                addValues.push_back(FP::undefined());
                continue;
            }
            addValues.push_back(sd);
            anyValid = true;
        }

        if (!anyValid) {
            sampleInterval = pow(logBase, intervalOffset + (double) itter / (double) pointsPerBase);
            continue;
        }
        if (!addSignature.empty() &&
                (previousSignature.size() != addSignature.size() ||
                        memcmp(&previousSignature[0], &addSignature[0],
                               sizeof(int) * previousSignature.size()))) {

            PointType add(allocator->create(tr));
            if (add->totalDimensions() > 1) {
                add->set(0, sampleInterval);
                for (int i = 1; i <= maximumDimensions; i++) {
                    if (i >= add->totalDimensions())
                        break;
                    add->set(i, addValues[i - 1]);
                }
            } else {
                for (int i = 0; i <= maximumDimensions; i++) {
                    if (i >= add->totalDimensions())
                        break;
                    add->set(i, addValues[i]);
                }
            }

            result.append(add);
            previousSignature.resize(addSignature.size());
            memcpy(&previousSignature[0], &addSignature[0], sizeof(int) * previousSignature.size());
        } else if (itter > 1) {
            int nPossible = 1;
            if (sampleInterval > 0.0)
                nPossible = (int) ceil(totalInterval / sampleInterval);
            if (nPossible < 1)
                nPossible = 1;
            if (nPossible < nSamplePoints / 2 && nExpectedPoints > points.size() / 2) {
                /* Should we even be checking the number of possible
                 * unique intervals? Or just leave it at the number of
                 * points per interval? */
                break;
            }
        }

        sampleInterval = pow(logBase, intervalOffset + (double) itter / (double) pointsPerBase);
    }

    return result;
}

Allan::Allan()
{ }

Allan::~Allan()
{ }

void Allan::applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                       TransformerAllocator *allocator) const
{
    Q_ASSERT(allocator != NULL);
    sortAndRemoveUndefined<0>(points);
    points = transformToAllan<false>(points, allocator, this);
}

void Allan::apply(QVector<std::shared_ptr<TransformerPoint> > &points,
                  TransformerAllocator *allocator)
{
    Q_ASSERT(allocator != NULL);
    sortAndRemoveUndefined<0>(points);
    points = transformToAllan<false>(points, allocator, this);
}

void Allan::applyConst(QVector<TransformerPoint *> &points, TransformerAllocator *allocator) const
{
    Q_ASSERT(allocator != NULL);
    sortAndRemoveUndefined<0>(points);
    QVector<TransformerPoint *> result(transformToAllan<true>(points, allocator, this));
    qDeleteAll(points);
    points = result;
}

void Allan::apply(QVector<TransformerPoint *> &points, TransformerAllocator *allocator)
{
    Q_ASSERT(allocator != NULL);
    sortAndRemoveUndefined<0>(points);
    QVector<TransformerPoint *> result(transformToAllan<true>(points, allocator, this));
    qDeleteAll(points);
    points = result;
}

Transformer *Allan::clone() const
{ return new Allan; }

Allan::Allan(QDataStream &stream)
{ Q_UNUSED(stream); }

void Allan::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Transformer::SerialType_Allan;
    stream << t;
}

void Allan::printLog(QDebug &stream) const
{ stream << "Allan"; }


AllanFit::~AllanFit()
{ }

AllanFit::AllanFit(const AllanFit &other)
        : Model(),
          m(other.m),
          b(other.b),
          interceptPoint(other.interceptPoint),
          constraints(other.constraints)
{ }

/**
 * Construct the Allan fit for the given points
 * 
 * @param points    the data points
 * @param output    the output constraints to apply
 * @param setInterceptPoint the point to display the intercept at
 */
AllanFit::AllanFit(const QVector<double> &intervals,
                   const QVector<double> &sds,
                   double startInterval,
                   double endInterval,
                   double setInterceptPoint,
                   const ModelOutputConstraints &output) : interceptPoint(setInterceptPoint),
                                                           constraints(output)
{
    Q_ASSERT(FP::defined(interceptPoint) && interceptPoint > 0.0);

    double sxy = 0;
    double sx = 0;
    double sy = 0;
    double sxx = 0;
    int nPoints = 0;
    for (int i = 0, max = qMin(intervals.size(), sds.size()); i < max; i++) {
        double xv = intervals.at(i);
        if (!FP::defined(xv) || xv <= 0.0)
            continue;
        if (FP::defined(startInterval) && xv < startInterval)
            continue;
        if (FP::defined(endInterval) && xv > endInterval)
            continue;
        double yv = sds.at(i);
        if (!FP::defined(yv) || yv <= 0.0)
            continue;
        xv = log10(xv);
        yv = log10(yv);

        sxy += xv * yv;
        sx += xv;
        sy += yv;
        sxx += xv * xv;
        ++nPoints;
    }

    double div = ((double) nPoints * sxx - sx * sx);
    if (nPoints == 0 || div == 0.0) {
        m = FP::undefined();
        b = FP::undefined();
        return;
    }
    m = ((double) nPoints * sxy - (sx * sy)) / div;
    b = (sy - m * sx) / (double) nPoints;
}

QVector<double> AllanFit::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

QVector<double> AllanFit::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

void AllanFit::applyIOConst(QVector<double> &values) const
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

void AllanFit::applyIO(QVector<double> &values)
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

double AllanFit::oneToOneConst(double input) const
{ return evaluate(input); }

double AllanFit::oneToOne(double input)
{ return evaluate(input); }

double AllanFit::twoToOneConst(double a, double b) const
{
    Q_UNUSED(b);
    return evaluate(a);
}

double AllanFit::twoToOne(double a, double b)
{
    Q_UNUSED(b);
    return evaluate(a);
}

double AllanFit::threeToOneConst(double a, double b, double c) const
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

double AllanFit::threeToOne(double a, double b, double c)
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

void AllanFit::applyIOConst(double &a, double &b) const
{
    a = evaluate(a);
    b = evaluate(b);
}

void AllanFit::applyIO(double &a, double &b)
{
    a = evaluate(a);
    b = evaluate(b);
}

void AllanFit::applyIOConst(double &a, double &b, double &c) const
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

void AllanFit::applyIO(double &a, double &b, double &c)
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

Model *AllanFit::clone() const
{ return new AllanFit(*this); }

QString AllanFit::describe() const
{ return QString("AllanFit(%1,%2)").arg(m).arg(b); }

ModelLegendEntry AllanFit::legend(const ModelLegendParameters &parameters) const
{
    Q_UNUSED(parameters);
    if (!FP::defined(m) || !FP::defined(b))
        return ModelLegendEntry();

    NumberFormat fmt(1, 3);

    QString result(QString::fromUtf8("\xCF\x83("));
    if (floor(interceptPoint) == ceil(interceptPoint)) {
        result.append(QString::number((int) floor(interceptPoint)));
    } else {
        result.append(fmt.apply(interceptPoint, QChar()));
    }
    result.append(") = ");
    result.append(fmt.apply(pow(10.0, m * log10(interceptPoint) + b)));

    result.append(" m(log");
    result.append(NumberFormat(1, 0).subscript(10));
    result.append(") = ");
    result.append(fmt.apply(m));

    return ModelLegendEntry(result);
}

AllanFit::AllanFit(QDataStream &stream)
{
    stream >> m >> b >> interceptPoint;
}

void AllanFit::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_AllanFit;
    stream << t << m << b << interceptPoint;
}

void AllanFit::printLog(QDebug &stream) const
{ stream << "AllanFit(" << m << ',' << b << ')'; }


}
}
