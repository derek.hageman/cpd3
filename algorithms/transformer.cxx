/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <QVector>
#include <QRunnable>
#include <QDebugStateSaver>

#include "core/threadpool.hxx"
#include "core/util.hxx"
#include "algorithms/transformer.hxx"
#include "algorithms/cdf.hxx"
#include "algorithms/allan.hxx"
#include "algorithms/rangefilter.hxx"
#include "algorithms/rangewrap.hxx"
#include "algorithms/quantilefilter.hxx"
#include "algorithms/fourier.hxx"

using namespace CPD3::Data;
using namespace CPD3::Algorithms::Internal;

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/transformer.hxx
 * This is the general interface to general purpose transformers.  A transformer
 * takes an input set of points and produces an output
 */

Transformer *Transformer::fromConfiguration(const Variant::Read &configuration,
                                            const TransformerParameters &parameters)
{
    const auto &type = configuration["Type"].toString();

    if (Util::equal_insensitive(type, "CDF", "NormalCDF"))
        return new NormalCDF;

    if (Util::equal_insensitive(type, "Allan", "AllanPlot"))
        return new Allan;

    if (Util::equal_insensitive(type, "Fourier", "FFT", "PowerSpectrum"))
        return new FourierPowerSpectrum;

    if (Util::equal_insensitive(type, "Range", "AcceptRange")) {
        int flags = 0;
        if (configuration["MinInclusive"].toBool())
            flags |= RangeFilter::Min_Inclusive;
        if (configuration["MaxInclusive"].toBool())
            flags |= RangeFilter::Max_Inclusive;
        if (configuration["Remove"].toBool())
            flags |= RangeFilter::Reject_Remove;
        qint64 dimension = configuration["Dimension"].toInt64();
        if (!INTEGER::defined(dimension) || dimension < 0)
            dimension = -1;
        return new RangeFilter(configuration["Minimum"].toDouble(),
                               configuration["Maximum"].toDouble(), flags, (int) dimension);
    }
    if (Util::equal_insensitive(type, "RejectRange")) {
        int flags = RangeFilter::Reject_Range;
        if (configuration["MinInclusive"].toBool())
            flags |= RangeFilter::Min_Inclusive;
        if (configuration["MaxInclusive"].toBool())
            flags |= RangeFilter::Max_Inclusive;
        if (configuration["Remove"].toBool())
            flags |= RangeFilter::Reject_Remove;
        qint64 dimension = configuration["Dimension"].toInt64();
        if (!INTEGER::defined(dimension) || dimension < 0)
            dimension = -1;
        return new RangeFilter(configuration["Minimum"].toDouble(),
                               configuration["Maximum"].toDouble(), flags, (int) dimension);
    }
    if (Util::equal_insensitive(type, "QuantileFilter", "AcceptQuantile")) {
        int flags = 0;
        if (configuration["MinInclusive"].toBool())
            flags |= QuantileFilter::Min_Inclusive;
        if (configuration["MaxInclusive"].toBool())
            flags |= QuantileFilter::Max_Inclusive;
        if (configuration["Remove"].toBool())
            flags |= QuantileFilter::Reject_Remove;
        qint64 dimension = configuration["Dimension"].toInt64();
        if (!INTEGER::defined(dimension) || dimension < 0)
            dimension = -1;
        return new QuantileFilter(configuration["Minimum"].toDouble(),
                                  configuration["Maximum"].toDouble(), flags, (int) dimension);
    }
    if (Util::equal_insensitive(type, "RejectQuantile")) {
        int flags = QuantileFilter::Reject_Range;
        if (configuration["MinInclusive"].toBool())
            flags |= QuantileFilter::Min_Inclusive;
        if (configuration["MaxInclusive"].toBool())
            flags |= QuantileFilter::Max_Inclusive;
        if (configuration["Remove"].toBool())
            flags |= QuantileFilter::Reject_Remove;
        qint64 dimension = configuration["Dimension"].toInt64();
        if (!INTEGER::defined(dimension) || dimension < 0)
            dimension = -1;
        return new QuantileFilter(configuration["Minimum"].toDouble(),
                                  configuration["Maximum"].toDouble(), flags, (int) dimension);
    }

    if (Util::equal_insensitive(type, "Wrap", "RangeWrap", "WrapRange")) {
        qint64 dimension = configuration["Dimension"].toInt64();
        if (!INTEGER::defined(dimension) || dimension < 0)
            dimension = -1;
        double min = configuration["Minimum"].toDouble();
        double max = configuration["Maximum"].toDouble();
        if (!FP::defined(min) || !FP::defined(max) || min >= max)
            return new TransformerNOOP();
        return new RangeWrap(min, max, (int) dimension);
    }

    if (Util::equal_insensitive(type, "Constant"))
        return new TransformerConstant(configuration["Value"].toDouble());

    if (Util::equal_insensitive(type, "Chain")) {
        auto chain = configuration["Chain"].toArray();
        if (chain.empty())
            chain = configuration["Transformers"].toArray();
        if (!chain.empty()) {
            QVector<Transformer *> trs;
            for (auto add : chain) {
                trs.append(fromConfiguration(add, parameters));
            }
            if (trs.size() == 1)
                return trs[0];
            return new TransformerChain(trs);
        }
    }

    if (configuration.getType() == Variant::Type::Array) {
        auto chain = configuration.toArray();
        if (!chain.empty()) {
            QVector<Transformer *> trs;
            for (auto add : chain) {
                trs.append(fromConfiguration(add, parameters));
            }
            if (trs.size() == 1)
                return trs[0];
            return new TransformerChain(trs);
        }
    }

    return new TransformerNOOP();
}


TransformerPoint::~TransformerPoint()
{ }

TransformerParameters::TransformerParameters()
{ }

TransformerParameters::TransformerParameters(const TransformerParameters &other) : parameters(
        other.parameters)
{ }

TransformerParameters &TransformerParameters::operator=(const TransformerParameters &other)
{
    if (&other == this) return *this;
    parameters = other.parameters;
    return *this;
}

TransformerParameters::TransformerParameters(const QHash<QString, QVariant> &params) : parameters(
        params)
{ }

QVariant TransformerParameters::getParameter(const QString &name) const
{ return parameters.value(name, QVariant()); }

void TransformerParameters::setParameter(const QString &name, const QVariant &value)
{ parameters.insert(name, value); }

double TransformerParameters::getDouble(const QString &name) const
{
    QVariant v(parameters.value(name, QVariant()));
    if (!v.isValid())
        return FP::undefined();
    return v.toDouble();
}

void TransformerParameters::setDouble(const QString &name, double value)
{
    if (!FP::defined(value))
        parameters.remove(name);
    else
        parameters.insert(name, QVariant(value));
}

void Transformer::printLog(QDebug &stream) const
{ stream << "Transformer(UNKNOWN)"; }

Transformer::~Transformer()
{ }

void Transformer::apply(QVector<std::shared_ptr<TransformerPoint> > &points,
                        TransformerAllocator *allocator)
{ applyConst(points, allocator); }

void Transformer::apply(QVector<TransformerPoint *> &points, TransformerAllocator *allocator)
{ applyConst(points, allocator); }


TransformerNOOP::TransformerNOOP()
{ }

TransformerNOOP::~TransformerNOOP()
{ }

void TransformerNOOP::applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                                 TransformerAllocator *allocator) const
{
    Q_UNUSED(points);
    Q_UNUSED(allocator);
}

void TransformerNOOP::apply(QVector<std::shared_ptr<TransformerPoint> > &points,
                            TransformerAllocator *allocator)
{
    Q_UNUSED(points);
    Q_UNUSED(allocator);
}

void TransformerNOOP::applyConst(QVector<TransformerPoint *> &points,
                                 TransformerAllocator *allocator) const
{
    Q_UNUSED(points);
    Q_UNUSED(allocator);
}

void TransformerNOOP::apply(QVector<TransformerPoint *> &points, TransformerAllocator *allocator)
{
    Q_UNUSED(points);
    Q_UNUSED(allocator);
}

Transformer *TransformerNOOP::clone() const
{ return new TransformerNOOP; }

TransformerNOOP::TransformerNOOP(QDataStream &stream)
{ Q_UNUSED(stream); }

void TransformerNOOP::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Transformer::SerialType_NOOP;
    stream << t;
}

void TransformerNOOP::printLog(QDebug &stream) const
{ stream << "TransformerNOOP"; }


TransformerConstant::TransformerConstant(double sv) : value(sv)
{ }

TransformerConstant::~TransformerConstant()
{ }

void TransformerConstant::applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                                     TransformerAllocator *allocator) const
{
    Q_UNUSED(allocator);
    for (QVector<std::shared_ptr<TransformerPoint> >::iterator it = points.begin(),
            end = points.end(); it != end; ++it) {
        for (int i = 0, max = (*it)->totalDimensions(); i < max; i++) {
            (*it)->set(i, value);
        }
    }
}

void TransformerConstant::apply(QVector<std::shared_ptr<TransformerPoint> > &points,
                                TransformerAllocator *allocator)
{
    Q_UNUSED(allocator);
    for (QVector<std::shared_ptr<TransformerPoint> >::iterator it = points.begin(),
            end = points.end(); it != end; ++it) {
        for (int i = 0, max = (*it)->totalDimensions(); i < max; i++) {
            (*it)->set(i, value);
        }
    }
}

void TransformerConstant::applyConst(QVector<TransformerPoint *> &points,
                                     TransformerAllocator *allocator) const
{
    Q_UNUSED(allocator);
    for (QVector<TransformerPoint *>::iterator it = points.begin(), end = points.end();
            it != end;
            ++it) {
        for (int i = 0, max = (*it)->totalDimensions(); i < max; i++) {
            (*it)->set(i, value);
        }
    }
}

void TransformerConstant::apply(QVector<TransformerPoint *> &points,
                                TransformerAllocator *allocator)
{
    Q_UNUSED(allocator);
    for (QVector<TransformerPoint *>::iterator it = points.begin(), end = points.end();
            it != end;
            ++it) {
        for (int i = 0, max = (*it)->totalDimensions(); i < max; i++) {
            (*it)->set(i, value);
        }
    }
}

Transformer *TransformerConstant::clone() const
{ return new TransformerConstant(value); }

TransformerConstant::TransformerConstant(QDataStream &stream)
{ stream >> value; }

void TransformerConstant::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Transformer::SerialType_Constant;
    stream << t << value;
}

void TransformerConstant::printLog(QDebug &stream) const
{ stream << "TransformerConstant(" << value << ")"; }


TransformerChain::TransformerChain(const QVector<Transformer *> &chain) : transformers(chain)
{ }

TransformerChain::~TransformerChain()
{ qDeleteAll(transformers); }

void TransformerChain::applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                                  TransformerAllocator *allocator) const
{
    for (QVector<Transformer *>::const_iterator it = transformers.constBegin(),
            end = transformers.constEnd(); it != end; ++it) {
        (*it)->applyConst(points, allocator);
    }
}

void TransformerChain::apply(QVector<std::shared_ptr<TransformerPoint> > &points,
                             TransformerAllocator *allocator)
{
    for (QVector<Transformer *>::const_iterator it = transformers.constBegin(),
            end = transformers.constEnd(); it != end; ++it) {
        (*it)->apply(points, allocator);
    }
}

void TransformerChain::applyConst(QVector<TransformerPoint *> &points,
                                  TransformerAllocator *allocator) const
{
    for (QVector<Transformer *>::const_iterator it = transformers.constBegin(),
            end = transformers.constEnd(); it != end; ++it) {
        (*it)->applyConst(points, allocator);
    }
}

void TransformerChain::apply(QVector<TransformerPoint *> &points, TransformerAllocator *allocator)
{
    for (QVector<Transformer *>::const_iterator it = transformers.constBegin(),
            end = transformers.constEnd(); it != end; ++it) {
        (*it)->apply(points, allocator);
    }
}

Transformer *TransformerChain::clone() const
{
    QVector<Transformer *> copy;
    copy.reserve(transformers.size());
    for (QVector<Transformer *>::const_iterator it = transformers.constBegin(),
            end = transformers.constEnd(); it != end; ++it) {
        copy.append((*it)->clone());
    }
    return new TransformerChain(copy);
}

TransformerChain::TransformerChain(QDataStream &stream)
{
    quint32 n;
    stream >> n;
    transformers.reserve((int) n);
    for (quint32 i = 0; i < n; i++) {
        Transformer *add = NULL;
        stream >> add;
        transformers.append(add);
    }
}

void TransformerChain::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Transformer::SerialType_Chain;
    stream << t << (quint32) transformers.size();
    for (QVector<Transformer *>::const_iterator it = transformers.constBegin() + 1,
            end = transformers.constEnd(); it != end; ++it) {
        stream << (*it);
    }
}

void TransformerChain::printLog(QDebug &stream) const
{
    if (transformers.isEmpty()) {
        stream << "TransformerChain()";
        return;
    }
    stream << "TransformerChain(";
    transformers.first()->printLog(stream);
    for (QVector<Transformer *>::const_iterator it = transformers.constBegin() + 1,
            end = transformers.constEnd(); it != end; ++it) {
        stream << ',';
        (*it)->printLog(stream);
    }
    stream << ')';
}


namespace Internal {
class DeferredTransformerWrapperRunnable : public QRunnable {
    DeferredTransformerWrapper *wrapper;
    QVector<TransformerPoint *> points;
    QVector<std::shared_ptr<TransformerPoint> > pointsShared;
    TransformerAllocator *allocator;
protected:
    Transformer *transformer;
public:
    DeferredTransformerWrapperRunnable(DeferredTransformerWrapper *setWrapper,
                                       const QVector<TransformerPoint *> &setPoints,
                                       Transformer *setTransformer,
                                       TransformerAllocator *setAllocator) : wrapper(setWrapper),
                                                                             points(setPoints),
                                                                             pointsShared(),
                                                                             allocator(
                                                                                     setAllocator),
                                                                             transformer(
                                                                                     setTransformer)
    {
        setAutoDelete(true);
    }

    DeferredTransformerWrapperRunnable(DeferredTransformerWrapper *setWrapper,
                                       const QVector<std::shared_ptr<TransformerPoint> > &setPoints,
                                       Transformer *setTransformer,
                                       TransformerAllocator *setAllocator) : wrapper(setWrapper),
                                                                             points(),
                                                                             pointsShared(
                                                                                     setPoints),
                                                                             allocator(
                                                                                     setAllocator),
                                                                             transformer(
                                                                                     setTransformer)
    {
        setAutoDelete(true);
    }

    ~DeferredTransformerWrapperRunnable()
    {
        if (transformer != NULL)
            delete transformer;
    }

    virtual void run()
    {
        if (wrapper == NULL || transformer == NULL)
            return;
        if (points.isEmpty()) {
            transformer->apply(pointsShared, allocator);
            wrapper->notifyReady(pointsShared);
        } else {
            transformer->apply(points, allocator);
            wrapper->notifyReady(points);
        }
        wrapper = NULL;
    }
};
}

namespace {
class DeferredTransformerWrapperRunnableTransformer : public DeferredTransformerWrapperRunnable {
    Variant::Read configuration;
    TransformerParameters parameters;
public:
    DeferredTransformerWrapperRunnableTransformer(DeferredTransformerWrapper *wrapper,
                                                  const QVector<TransformerPoint *> &points,
                                                  const Variant::Read &setConfiguration,
                                                  const TransformerParameters &setParameters,
                                                  TransformerAllocator *allocator)
            : DeferredTransformerWrapperRunnable(wrapper, points, NULL, allocator),
              configuration(setConfiguration),
              parameters(setParameters)
    {
        configuration.detachFromRoot();
    }

    DeferredTransformerWrapperRunnableTransformer(DeferredTransformerWrapper *wrapper,
                                                  const QVector<std::shared_ptr<
                                                          TransformerPoint> > &points,
                                                  const Variant::Read &setConfiguration,
                                                  const TransformerParameters &setParameters,
                                                  TransformerAllocator *allocator)
            : DeferredTransformerWrapperRunnable(wrapper, points, NULL, allocator),
              configuration(setConfiguration),
              parameters(setParameters)
    {
        configuration.detachFromRoot();
    }

    virtual void run()
    {
        if (transformer == NULL) {
            transformer = Transformer::fromConfiguration(configuration, parameters);
        }
        DeferredTransformerWrapperRunnable::run();
    }
};
}

DeferredTransformerWrapper::~DeferredTransformerWrapper()
{
    waitForReady();
    std::unique_lock<std::mutex> lock(mutex);
    while (finished != 1) {
        lock.unlock();
        QThread::yieldCurrentThread();
        lock.lock();
    }
    lock.unlock();
    qDeleteAll(points);
}

void DeferredTransformerWrapper::notifyReady(const QVector<TransformerPoint *> &points)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        finished = 2;
        this->points = points;
        this->pointsShared.clear();
    }
    cond.notify_all();
    emit pointsReady();

    std::lock_guard<std::mutex> lock(mutex);
    finished = 1;
}

void DeferredTransformerWrapper::notifyReady(const QVector<
        std::shared_ptr<TransformerPoint> > &points)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
    finished = 2;
    this->pointsShared = points;
        this->points.clear();
    }
    cond.notify_all();
    emit pointsReady();

    std::lock_guard<std::mutex> lock(mutex);
    finished = 1;
}

/**
 * Transform the given points with with the transformer specified in the
 * configuration.  This takes ownership of the points and will retain that
 * ownership unless they are taken back.  That is, this may delete points
 * in the given list as necessary and will delete them all when the
 * wrapper is deleted.
 * 
 * @param points        the input points
 * @param configuration the configuration
 * @param parameters    the transformer parameters
 * @param allocator     the optional allocator for new points
 */
DeferredTransformerWrapper::DeferredTransformerWrapper(const QVector<TransformerPoint *> &points,
                                                       const Data::Variant::Read &configuration,
                                                       const TransformerParameters &parameters,
                                                       TransformerAllocator *allocator) : finished(
        0)
{
    ThreadPool::system()->run(
            new DeferredTransformerWrapperRunnableTransformer(this, points, configuration,
                                                              parameters, allocator));
}

/**
 * Transform the given points with with the transformer specified in the
 * configuration.
 * 
 * @param points        the input points
 * @param configuration the configuration
 * @param parameters    the transformer parameters
 * @param allocator     the optional allocator for new points
 */
DeferredTransformerWrapper::DeferredTransformerWrapper(const QVector<
        std::shared_ptr<TransformerPoint> > &points, const Data::Variant::Read &configuration,
                                                       const TransformerParameters &parameters,
                                                       TransformerAllocator *allocator) : finished(
        0)
{
    ThreadPool::system()->run(
            new DeferredTransformerWrapperRunnableTransformer(this, points, configuration,
                                                              parameters, allocator));
}

/**
 * Transform the given points with with the transformer.  This takes
 * ownership of the transformer and will delete it when finished.
 * This also takes ownership of the points and will retain that
 * ownership unless they are taken back.  That is, this may delete points
 * in the given list as necessary and will delete them all when the
 * wrapper is deleted.
 * 
 * @param points        the input points
 * @param transformer   the transformer
 * @param allocator     the optional allocator for new points
 */
DeferredTransformerWrapper::DeferredTransformerWrapper(const QVector<TransformerPoint *> &points,
                                                       Transformer *transformer,
                                                       TransformerAllocator *allocator) : finished(
        0)
{
    ThreadPool::system()->run(
            new DeferredTransformerWrapperRunnable(this, points, transformer, allocator));
}

/**
 * Transform the given points with with the transformer.  This takes
 * ownership of the transformer and will delete it when finished.
 * 
 * @param points        the input points
 * @param transformer   the transformer
 * @param allocator     the optional allocator for new points
 */
DeferredTransformerWrapper::DeferredTransformerWrapper(const QVector<
        std::shared_ptr<TransformerPoint> > &points,
                                                       Transformer *transformer,
                                                       TransformerAllocator *allocator) : finished(
        0)
{
    ThreadPool::system()->run(
            new DeferredTransformerWrapperRunnable(this, points, transformer, allocator));
}

/**
 * Test if the transformer is finished.
 * 
 * @return true if the transformer is finished
 */
bool DeferredTransformerWrapper::isReady()
{
    std::lock_guard<std::mutex> lock(mutex);
    return finished != 0;
}

/**
 * Wait for the transformer to finish.
 * 
 * @param time  the maximum time to wait in milliseconds or ULONG_MAX to wait forever
 */
bool DeferredTransformerWrapper::waitForReady(unsigned long time)
{
    std::unique_lock<std::mutex> lock(mutex);
    if (finished != 0)
        return true;
    if (time != ULONG_MAX) {
        cond.wait_for(lock, std::chrono::milliseconds(time));
    } else {
        cond.wait(lock);
    }
    return finished != 0;
}

/**
 * Get the points.
 * 
 * @param ready if not NULL then set to true if ready
 * @return      the points
 */
QVector<TransformerPoint *> DeferredTransformerWrapper::getPoints(bool *ready)
{
    if (ready != NULL) *ready = false;
    std::lock_guard<std::mutex> lock(mutex);
    if (finished == 0)
        return QVector<TransformerPoint *>();
    if (ready != NULL) *ready = true;
    return points;
}

/**
 * Get the shared point list.
 * 
 * @param ready if not NULL then set to true if ready
 * @return      the shared points
 */
QVector<std::shared_ptr<TransformerPoint> > DeferredTransformerWrapper::getSharedPoints(bool *ready)
{
    if (ready != NULL) *ready = false;
    std::lock_guard<std::mutex> lock(mutex);
    if (finished == 0)
        return QVector<std::shared_ptr<TransformerPoint> >();
    if (ready != NULL) *ready = true;
    return pointsShared;
}

/**
 * Get the points and pass ownership of them to the caller.
 * 
 * @param ready if not NULL then set to true if ready
 * @return      the points
 */
QVector<TransformerPoint *> DeferredTransformerWrapper::takePoints(bool *ready)
{
    if (ready != NULL) *ready = false;
    std::lock_guard<std::mutex> lock(mutex);
    if (finished == 0)
        return QVector<TransformerPoint *>();
    if (ready != NULL) *ready = true;
    QVector<TransformerPoint *> p(points);
    points.clear();
    return p;
}

/**
 * Get the shared points and pass ownership of them to the caller.
 * 
 * @param ready if not NULL then set to true if ready
 * @return      the shared points
 */
QVector<std::shared_ptr<
        TransformerPoint> > DeferredTransformerWrapper::takeSharedPoints(bool *ready)
{
    if (ready != NULL) *ready = false;
    std::lock_guard<std::mutex> lock(mutex);
    if (finished == 0)
        return QVector<std::shared_ptr<TransformerPoint> >();
    if (ready != NULL) *ready = true;
    QVector<std::shared_ptr<TransformerPoint> > p(pointsShared);
    pointsShared.clear();
    return p;
}

/**
 * Delete the transformer wrapper once transformer is done.  This will not 
 * block the caller or event loop while waiting for the model to finish.
 */
void DeferredTransformerWrapper::deleteWhenFinished()
{
    disconnect(this);
    connect(this, SIGNAL(pointsReady()), this, SLOT(deleteLater()), Qt::QueuedConnection);
    std::unique_lock<std::mutex> lock(mutex);
    if (finished != 0) {
        lock.unlock();
        disconnect(this);
        deleteLater();
        return;
    }
}


QDataStream &operator>>(QDataStream &stream, Transformer *&transformer)
{
    quint8 tRaw;
    stream >> tRaw;
    switch ((Transformer::TransformerSerialType) tRaw) {
    case Transformer::SerialType_NULL:
        transformer = NULL;
        return stream;
    case Transformer::SerialType_NOOP:
        transformer = new TransformerNOOP(stream);
        return stream;
    case Transformer::SerialType_Constant:
        transformer = new TransformerConstant(stream);
        return stream;
    case Transformer::SerialType_Chain:
        transformer = new TransformerChain(stream);
        return stream;
    case Transformer::SerialType_NormalCDF:
        transformer = new NormalCDF(stream);
        return stream;
    case Transformer::SerialType_Allan:
        transformer = new Allan(stream);
        return stream;
    case Transformer::SerialType_RangeFilter:
        transformer = new RangeFilter(stream);
        return stream;
    case Transformer::SerialType_RangeWrap:
        transformer = new RangeFilter(stream);
        return stream;
    case Transformer::SerialType_QuantileFilter:
        transformer = new QuantileFilter(stream);
        return stream;
    case Transformer::SerialType_FourierPowerSpectrum:
        transformer = new FourierPowerSpectrum(stream);
        return stream;
    }
    Q_ASSERT(false);
    transformer = NULL;
    return stream;
}

QDataStream &operator<<(QDataStream &stream, const Transformer *transformer)
{
    if (transformer == NULL) {
        quint8 t = (quint8) Transformer::SerialType_NULL;
        stream << t;
        return stream;
    }
    transformer->serialize(stream);
    return stream;
}

QDebug operator<<(QDebug stream, const Transformer *transformer)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << static_cast<const void *>(transformer) << ":";
    if (transformer != NULL) {
        transformer->printLog(stream);
    } else {
        stream << "Transformer(NULL)";
    }
    return stream;
}

TransformerAllocator::TransformerAllocator()
{ }

TransformerAllocator::~TransformerAllocator()
{ }

}
}
