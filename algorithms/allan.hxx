/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSALLAN_H
#define CPD3ALGORITHMSALLAN_H

#include "core/first.hxx"

#include <math.h>
#include <memory>
#include <QtGlobal>
#include <QList>
#include "algorithms/algorithms.hxx"

#include "core/number.hxx"
#include "algorithms/model.hxx"
#include "algorithms/transformer.hxx"


namespace CPD3 {
namespace Algorithms {


/**
 * A transformer transforms points into an sampled points for an Allan plot.
 */
class CPD3ALGORITHMS_EXPORT Allan : public Transformer {
public:
    Allan();

    ~Allan();

    virtual void applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                            TransformerAllocator *allocator = NULL) const;

    virtual void apply(QVector<std::shared_ptr<TransformerPoint> > &points,
                       TransformerAllocator *allocator = NULL);

    virtual void
            applyConst(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL)
            const;

    virtual void apply(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL);

    virtual Transformer *clone() const;

protected:
    friend class Transformer;

    Allan(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator<<(QDataStream &stream, const Transformer *transformer);

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator>>(QDataStream &stream, Transformer *&transformer);

    friend CPD3ALGORITHMS_EXPORT QDebug operator<<(QDebug stream, const Transformer *transformer);
};


/**
 * A fit for the linearization of a normal CDF.
 */
class CPD3ALGORITHMS_EXPORT AllanFit : public Model {
    double m;
    double b;
    double interceptPoint;
    ModelOutputConstraints constraints;

    inline double evaluate(double input) const
    {
        if (!FP::defined(m))
            return FP::undefined();
        return constraints.apply(pow(10.0, m * log10(input) + b));
    }

    void calculateLine();

    AllanFit(const AllanFit &other);

public:
    virtual ~AllanFit();

    AllanFit(const QVector<double> &intervals,
             const QVector<double> &sds,
             double startInterval = 10.0,
             double endInterval = 100.0,
             double setInterceptPoint = 60.0,
             const ModelOutputConstraints &output = ModelOutputConstraints());

    virtual QVector<double> applyConst(const QVector<double> &inputs) const;

    virtual QVector<double> apply(const QVector<double> &inputs);

    virtual void applyIOConst(QVector<double> &values) const;

    virtual void applyIO(QVector<double> &values);

    virtual double oneToOneConst(double input) const;

    virtual double oneToOne(double input);

    virtual double twoToOneConst(double a, double b) const;

    virtual double twoToOne(double a, double b);

    virtual double threeToOneConst(double a, double b, double c) const;

    virtual double threeToOne(double a, double b, double c);

    virtual void applyIOConst(double &a, double &b) const;

    virtual void applyIO(double &a, double &b);

    virtual void applyIOConst(double &a, double &b, double &c) const;

    virtual void applyIO(double &a, double &b, double &c);

    virtual Model *clone() const;

    virtual QString describe() const;

    virtual ModelLegendEntry
            legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

protected:
    AllanFit(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

}
}

#endif
