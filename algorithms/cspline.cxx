/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <QDebugStateSaver>

#include "algorithms/cspline.hxx"
#include "algorithms/linearint.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/cspline.hxx
 * Provides routines for generating and using cubic spline interpolators.
 */


CSpline::~CSpline()
{ free(components); }

CSpline::CSpline() : nComponents(0)
{
    components = (Component *) malloc(sizeof(Component));
    Q_ASSERT(components);
    lastHit = components;
}

CSpline::CSpline(const CSpline &other) : nComponents(other.nComponents), lastHit(other.lastHit)
{
    if (nComponents == 0) {
        components = (Component *) malloc(sizeof(Component));
        Q_ASSERT(components);
    } else {
        components = (Component *) malloc(nComponents * sizeof(Component));
        Q_ASSERT(components);
        memcpy(components, other.components, nComponents * sizeof(Component));
    }
    Q_ASSERT(components);
    lastHit = components;
}

CSpline &CSpline::operator=(const CSpline &other)
{
    if (&other == this) return *this;
    nComponents = other.nComponents;

    free(components);
    if (nComponents == 0) {
        components = (Component *) malloc(sizeof(Component));
        Q_ASSERT(components);
    } else {
        components = (Component *) malloc(nComponents * sizeof(Component));
        Q_ASSERT(components);
        memcpy(components, other.components, nComponents * sizeof(Component));
    }
    lastHit = components;
    return *this;
}

bool CSpline::InputPoint::operator<(const InputPoint &other) const
{ return x < other.x; }

CSpline::InputPoint::InputPoint(double sx, double sy) : x(sx), y(sy)
{ }

void CSpline::generate(const std::vector<InputPoint> &points, double dX0, double dXN)
{
    int n = points.size();
    if (n < 2) {
        nComponents = 1;
        components = (Component *) malloc(sizeof(Component));
        Q_ASSERT(components);
        lastHit = components;
        return;
    }
    int n1 = n - 1;

    std::vector<double> h;
    h.reserve(n1);
    for (int i = 0; i < n1; i++) {
        h.push_back(points[i + 1].x - points[i].x);
    }

    std::vector<double> a;
    a.reserve(n);
    if (FP::defined(dX0)) {
        a.push_back(3.0 * (points[1].y - points[0].y) / h[0] - 3.0 * dX0);
    } else {
        a.push_back(0);
    }
    for (int i = 1; i < n1; i++) {
        a.push_back((3.0 / h[i]) * (points[i + 1].y - points[i].y) -
                            (3.0 / h[i - 1]) * (points[i].y - points[i - 1].y));
    }

    std::vector<double> l;
    l.reserve(n1);
    std::vector<double> u;
    u.reserve(n1);
    std::vector<double> z;
    z.reserve(n1);
    if (FP::defined(dX0)) {
        l.push_back(2.0 * h[0]);
        u.push_back(0.5);
        z.push_back(a[0] / l[0]);
    } else {
        l.push_back(1.0);
        u.push_back(0.0);
        z.push_back(0.0);
    }

    for (int i = 1; i < n1; i++) {
        l.push_back(2.0 * (points[i + 1].x - points[i - 1].x) - h[i - 1] * u[i - 1]);
        u.push_back(h[i] / l[i]);
        z.push_back((a[i] - h[i - 1] * z[i - 1]) / l[i]);
    }

    int n2 = n - 2;
    double cl;
    if (FP::defined(dXN)) {
        double lN = h[n2] * (2.0 - u[n2]);
        double aN = 3.0 * dXN - 3.0 * (points[n1].y - points[n2].y) / h[n2];
        cl = (aN - h[n2] * z[n2]) / lN;
    } else {
        cl = 0.0;
    }

    nComponents = n - 1;
    components = (Component *) malloc(nComponents * sizeof(Component));
    Q_ASSERT(components);
    lastHit = components;
    for (int i = n2; i >= 0; i--) {
        components[i].start = points[i].x;
        components[i].a = points[i].y;
        components[i].c = z[i] - u[i] * cl;
        components[i].b =
                (points[i + 1].y - points[i].y) / h[i] - h[i] * (cl + 2.0 * components[i].c) / 3.0;;
        components[i].d = (cl - components[i].c) / (3.0 * h[i]);

        cl = components[i].c;
    }
}

/**
 * Construct a cubic spline from the given x and y points.  If the derivatives
 * are undefined then that end is taken to be natural (f''(x) == 0) otherwise
 * it is clamped at the given value.
 * 
 * @param x     the set of X (input) values
 * @param y     the set of Y (output) values
 * @param dX0   the derivative at the first point
 * @param dXN   the derivative at the last point
 */
CSpline::CSpline(const QVector<double> &x, const QVector<double> &y, double dX0, double dXN)
{
    Q_ASSERT(x.size() == y.size());
    std::vector<InputPoint> sorted;
    sorted.reserve(x.size());
    for (int i = 0, max = x.size(); i < max; i++) {
        if (!FP::defined(x.at(i)) || !FP::defined(y.at(i)))
            continue;
        sorted.push_back(InputPoint(x.at(i), y.at(i)));
    }
    std::sort(sorted.begin(), sorted.end());
    if (sorted.begin() != sorted.end()) {
        for (std::vector<InputPoint>::iterator i = sorted.begin() + 1; i != sorted.end();) {
            if ((i - 1)->x == i->x) {
                i = sorted.erase(i);
                continue;
            }
            ++i;
        }
    }
    generate(sorted, dX0, dXN);
}

/** 
 * Construct a cubic spline with the given points.
 * 
 * @param points    the list of points (x = input, y = output)
 * @param dX0   the derivative at the first point
 * @param dXN   the derivative at the last point
 * @see CSpline( const QVector<double> &, const QVector<double> &, double, double )
 */
CSpline::CSpline(const QList<QPointF> &points, double dX0, double dXN)
{
    std::vector<InputPoint> sorted;
    sorted.reserve(points.size());
    for (int i = 0, max = points.size(); i < max; i++) {
        if (!FP::defined(points.at(i).x()) || !FP::defined(points.at(i).y()))
            continue;
        sorted.push_back(InputPoint(points.at(i).x(), points.at(i).y()));
    }
    std::sort(sorted.begin(), sorted.end());
    if (sorted.begin() != sorted.end()) {
        for (std::vector<InputPoint>::iterator i = sorted.begin() + 1; i != sorted.end();) {
            if ((i - 1)->x == i->x) {
                i = sorted.erase(i);
                continue;
            }
            ++i;
        }
    }
    generate(sorted, dX0, dXN);
}

/**
 * Construct a cubic spline with the given points
 * 
 * @param points    the list of points (first = input, second = output)
 * @see CSpline( const QVector<double> &, const QVector<double> &, double, double )
 */
CSpline::CSpline(const QList<QPair<double, double> > &points, double dX0, double dXN)
{
    std::vector<InputPoint> sorted;
    sorted.reserve(points.size());
    for (int i = 0, max = points.size(); i < max; i++) {
        if (!FP::defined(points.at(i).first) || !FP::defined(points.at(i).second))
            continue;
        sorted.push_back(InputPoint(points.at(i).first, points.at(i).second));
    }
    std::sort(sorted.begin(), sorted.end());
    if (sorted.begin() != sorted.end()) {
        for (std::vector<InputPoint>::iterator i = sorted.begin() + 1; i != sorted.end();) {
            if ((i - 1)->x == i->x) {
                i = sorted.erase(i);
                continue;
            }
            ++i;
        }
    }
    generate(sorted, dX0, dXN);
}

bool CSpline::Component::operator<(double d) const
{ return start < d; }

/**
 * Apply the spline to the given input.
 * 
 * @param input the input value
 * @return      the interpolated output
 */
double CSpline::evaluateConst(double input) const
{
    if (nComponents == 0) return FP::undefined();
    const Component *end = components + nComponents;
    const Component *c = std::lower_bound<const Component *, double>(components, end, input);
    if (c == end || (c != components && c->start > input)) c--;
    double d = input - c->start;
    return c->a + d * (c->b + d * (c->c + d * c->d));
}

/**
 * Apply the spline to the given input.
 * 
 * @param input the input value
 * @return      the interpolated output
 */
double CSpline::evaluate(double input)
{
    if (nComponents == 0) return FP::undefined();
    const Component *end = components + nComponents;
    const Component *c = lastHit;
    if (input < c->start || (c != end - 1 && input > (c + 1)->start)) {
        c = std::lower_bound<const Component *, double>(components, end, input);
        if (c == end || (c != components && c->start > input)) c--;
        lastHit = c;
    }

    double d = input - c->start;
    return c->a + d * (c->b + d * (c->c + d * c->d));
}

/**
 * Get the first derivative of the spline at the given input.
 * 
 * @param input the input value
 * @return      dY/dX
 */
double CSpline::derivativeConst(double input) const
{
    if (nComponents == 0) return FP::undefined();
    const Component *end = components + nComponents;
    const Component *c = std::lower_bound<const Component *, double>(components, end, input);
    if (c == end || (c != components && c->start > input)) c--;
    double d = input - c->start;
    return c->b + d * (c->c * 2.0 + c->d * 3.0 * d);
}

/**
 * Get the first derivative of the spline at the given input.
 * 
 * @param input the input value
 * @return      dY/dX
 */
double CSpline::derivative(double input)
{
    if (nComponents == 0) return FP::undefined();
    const Component *end = components + nComponents;
    const Component *c = lastHit;
    if (input < c->start || (c != end - 1 && input > (c + 1)->start)) {
        c = std::lower_bound<const Component *, double>(components, end, input);
        if (c == end || (c != components && c->start > input)) c--;
        lastHit = c;
    }

    double d = input - c->start;
    return c->b + d * (c->c * 2.0 + c->d * 3.0 * d);
}

QDataStream &operator<<(QDataStream &stream, const CSpline &spline)
{
    stream << (quint32) spline.nComponents;
    for (int i = 0; i < spline.nComponents; i++) {
        stream << spline.components[i].start;
        stream << spline.components[i].a;
        stream << spline.components[i].b;
        stream << spline.components[i].c;
        stream << spline.components[i].d;
    }
    return stream;
}

QDataStream &operator>>(QDataStream &stream, CSpline &spline)
{
    free(spline.components);
    quint32 n;
    stream >> n;
    spline.nComponents = (int) n;
    if (spline.nComponents == 0) {
        spline.components = (CPD3::Algorithms::CSpline::Component *) malloc(
                sizeof(CPD3::Algorithms::CSpline::Component));
        Q_ASSERT(spline.components);
    } else {
        spline.components = (CPD3::Algorithms::CSpline::Component *) malloc(
                spline.nComponents * sizeof(CPD3::Algorithms::CSpline::Component));
        Q_ASSERT(spline.components);
        for (int i = 0; i < spline.nComponents; i++) {
            stream >> spline.components[i].start;
            stream >> spline.components[i].a;
            stream >> spline.components[i].b;
            stream >> spline.components[i].c;
            stream >> spline.components[i].d;
        }
    }
    spline.lastHit = spline.components;
    return stream;
}

QDebug operator<<(QDebug stream, const CSpline &spline)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    stream << "CSpline(";
    for (int i = 0, max = spline.nComponents; i < max; i++) {
        if (i != 0) stream << ',';
        stream << "[x0=" << spline.components[i].start << ",{" << spline.components[i].a << ','
               << spline.components[i].b << ',' << spline.components[i].c << ','
               << spline.components[i].d << "}]";
    }
    stream << ")";
    return stream;
}


CSplineInterpolator::~CSplineInterpolator()
{ }

CSplineInterpolator::CSplineInterpolator(const CSpline &ss, const ModelOutputConstraints &output)
        : spline(ss), constraints(output)
{ }

CSplineInterpolator::CSplineInterpolator(const QVector<double> &x,
                                         const QVector<double> &y,
                                         double dX0,
                                         double dXN,
                                         const ModelOutputConstraints &output) : spline(x, y, dX0,
                                                                                        dXN),
                                                                                 constraints(output)
{ }

QVector<double> CSplineInterpolator::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

QVector<double> CSplineInterpolator::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

void CSplineInterpolator::applyIOConst(QVector<double> &values) const
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

void CSplineInterpolator::applyIO(QVector<double> &values)
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

double CSplineInterpolator::oneToOneConst(double input) const
{ return evaluate(input); }

double CSplineInterpolator::oneToOne(double input)
{ return evaluate(input); }

double CSplineInterpolator::twoToOneConst(double a, double b) const
{
    Q_UNUSED(b);
    return evaluate(a);
}

double CSplineInterpolator::twoToOne(double a, double b)
{
    Q_UNUSED(b);
    return evaluate(a);
}

double CSplineInterpolator::threeToOneConst(double a, double b, double c) const
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

double CSplineInterpolator::threeToOne(double a, double b, double c)
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

void CSplineInterpolator::applyIOConst(double &a, double &b) const
{
    a = evaluate(a);
    b = evaluate(b);
}

void CSplineInterpolator::applyIO(double &a, double &b)
{
    a = evaluate(a);
    b = evaluate(b);
}

void CSplineInterpolator::applyIOConst(double &a, double &b, double &c) const
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

void CSplineInterpolator::applyIO(double &a, double &b, double &c)
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

Model *CSplineInterpolator::clone() const
{ return new CSplineInterpolator(spline, constraints); }

QString CSplineInterpolator::describe() const
{ return QString("%1-CSpline").arg(spline.nComponents); }

ModelLegendEntry CSplineInterpolator::legend(const ModelLegendParameters &parameters) const
{
    Q_UNUSED(parameters);
    return ModelLegendEntry(QObject::tr("Cubic Spline (%n segments)", "model cspline legend line",
                                        spline.nComponents));
}

CSplineInterpolator::CSplineInterpolator(QDataStream &stream)
{ stream >> spline; }

void CSplineInterpolator::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_CSpline;
    stream << t << spline;
}

void CSplineInterpolator::printLog(QDebug &stream) const
{ stream << "CSplineInterpolator(" << spline << ")"; }

Model *CSplineInterpolator::create(const QVector<double> &x,
                                   const QVector<double> &y,
                                   double dX0,
                                   double dXN,
                                   const ModelOutputConstraints &output)
{
    if (x.size() != y.size() || x.isEmpty())
        return new ModelConstant(output.apply(FP::undefined()));
    if (x.size() == 1)
        return new ModelConstant(output.apply(y.at(0)));
    if (x.size() == 2) {
        /* A natural cubic spline with just two points is always a line */
        if (!FP::defined(dX0) && !FP::defined(dXN)) {
            if (!FP::defined(x.at(1)) ||
                    !FP::defined(x.at(0)) ||
                    !FP::defined(y.at(1)) ||
                    !FP::defined(y.at(0)) ||
                    x.at(0) == x.at(1) ||
                    y.at(0) == y.at(1)) {
                if (FP::defined(y.at(1)) && FP::defined(y.at(0)))
                    return new ModelConstant(output.apply((y.at(0) + y.at(1)) * 0.5));
                else if (FP::defined(y.at(0)))
                    return new ModelConstant(output.apply(y.at(0)));
                return new ModelConstant(output.apply(y.at(1)));
            }
            return new SingleLinearInterpolator(x.at(0), y.at(0), x.at(1), y.at(1), output);
        }
    }

    CSplineInterpolator *si = new CSplineInterpolator(x, y, dX0, dXN, output);
    if (si->spline.nComponents == 0) {
        delete si;
        double avg = 0;
        int count = 0;
        for (int i = 0, max = y.size(); i < max; i++) {
            if (!FP::defined(y.at(i)) || !FP::defined(x.at(i)))
                continue;
            avg += y.at(i);
            count++;
        }
        if (count == 0)
            return new ModelConstant(output.apply(FP::undefined()));
        return new ModelConstant(output.apply(avg / (double) count));
    }
    if (si->spline.nComponents == 1) {
        Model *result = new CSplineInterpolator::Single(si->spline.components[0].start,
                                                        si->spline.components[0].a,
                                                        si->spline.components[0].b,
                                                        si->spline.components[0].c,
                                                        si->spline.components[0].d, output);
        delete si;
        return result;
    }

    return si;
}


CSplineInterpolator::Single::~Single()
{ }

CSplineInterpolator::Single::Single(double ss,
                                    double sa,
                                    double sb,
                                    double sc,
                                    double sd,
                                    const ModelOutputConstraints &output) : start(ss),
                                                                            a(sa),
                                                                            b(sb),
                                                                            c(sc),
                                                                            d(sd),
                                                                            constraints(output)
{ }

QVector<double> CSplineInterpolator::Single::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

QVector<double> CSplineInterpolator::Single::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

void CSplineInterpolator::Single::applyIOConst(QVector<double> &values) const
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

void CSplineInterpolator::Single::applyIO(QVector<double> &values)
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

double CSplineInterpolator::Single::oneToOneConst(double input) const
{ return evaluate(input); }

double CSplineInterpolator::Single::oneToOne(double input)
{ return evaluate(input); }

double CSplineInterpolator::Single::twoToOneConst(double a, double b) const
{
    Q_UNUSED(b);
    return evaluate(a);
}

double CSplineInterpolator::Single::twoToOne(double a, double b)
{
    Q_UNUSED(b);
    return evaluate(a);
}

double CSplineInterpolator::Single::threeToOneConst(double a, double b, double c) const
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

double CSplineInterpolator::Single::threeToOne(double a, double b, double c)
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

void CSplineInterpolator::Single::applyIOConst(double &a, double &b) const
{
    a = evaluate(a);
    b = evaluate(b);
}

void CSplineInterpolator::Single::applyIO(double &a, double &b)
{
    a = evaluate(a);
    b = evaluate(b);
}

void CSplineInterpolator::Single::applyIOConst(double &a, double &b, double &c) const
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

void CSplineInterpolator::Single::applyIO(double &a, double &b, double &c)
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

Model *CSplineInterpolator::Single::clone() const
{ return new CSplineInterpolator::Single(start, a, b, c, d, constraints); }

QString CSplineInterpolator::Single::describe() const
{ return "1-CSpline"; }

ModelLegendEntry CSplineInterpolator::Single::legend(const ModelLegendParameters &parameters) const
{
    return legendPolynomial(QVector<double>() << a << b << c << d, parameters);
}

CSplineInterpolator::Single::Single(QDataStream &stream)
{ stream >> start >> a >> b >> c >> d; }

void CSplineInterpolator::Single::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_CSplineSingle;
    stream << t << start << a << b << c << d;
}

void CSplineInterpolator::Single::printLog(QDebug &stream) const
{
    stream << "CSplineInterpolatorSingle(x0=" << start << ",{" << a << ',' << b << ',' << c << ','
           << d << "})";
}

}
}
