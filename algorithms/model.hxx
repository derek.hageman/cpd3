/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSMODEL_H
#define CPD3ALGORITHMSMODEL_H

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <QtGlobal>
#include <QObject>
#include <QWaitCondition>
#include <QList>
#include <QVector>
#include <QHash>
#include <QDataStream>
#include <QVariant>
#include "algorithms/algorithms.hxx"

#include "datacore/variant/root.hxx"


namespace CPD3 {
namespace Algorithms {
class Model;

CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

CPD3ALGORITHMS_EXPORT QDebug operator<<(QDebug stream, const Model *model);


/**
 * A general wrapper for model parameters.
 */
class CPD3ALGORITHMS_EXPORT ModelParameters {
    QHash<QString, QVariant> parameters;
public:
    ModelParameters();

    ModelParameters(const ModelParameters &other);

    ModelParameters &operator=(const ModelParameters &other);

    ModelParameters(const QHash<QString, QVariant> &params);

    QVariant getParameter(const QString &name) const;

    void setParameter(const QString &name, const QVariant &value);

    double getDouble(const QString &name) const;

    int totalDimensions() const;

    QVector<double> getDimension(int dimension) const;

    void setDouble(const QString &name, double value);

    void setDimension(int n, const QVector<double> &values);

    void appendDimension(int n, double value);

    void clearDimensions();
};

/**
 * Defines constraints that are guaranteed on model inputs.
 */
class CPD3ALGORITHMS_EXPORT ModelInputConstraints {
    double min;
    double max;
public:
    ModelInputConstraints();

    ModelInputConstraints(const ModelInputConstraints &other);

    ModelInputConstraints &operator=(const ModelInputConstraints &other);

    ModelInputConstraints(double setMin, double setMax);

    double getMin() const;

    double getMax() const;
};

/**
 * Defines constraints that are applied to model outputs.
 */
class CPD3ALGORITHMS_EXPORT ModelOutputConstraints {
    double min;
    double max;
    bool wrap;
    bool noop;
public:
    ModelOutputConstraints();

    ModelOutputConstraints(const ModelOutputConstraints &other);

    ModelOutputConstraints &operator=(const ModelOutputConstraints &other);

    ModelOutputConstraints(double setMin, double setMax, bool setWrap = false);

    double getMin() const;

    double getMax() const;

    bool isWrapping() const;

    bool isNoop() const;

    double apply(double in) const;
};

/**
 * Defines the parameters used to generate a legend for a model.
 */
class CPD3ALGORITHMS_EXPORT ModelLegendParameters {
    double confidence;
    bool showOffset;
public:
    ModelLegendParameters();

    ModelLegendParameters(const ModelLegendParameters &other);

    ModelLegendParameters &operator=(const ModelLegendParameters &other);

    double getConfidence() const;

    void setConfidence(double c);

    bool getShowOffset() const;

    void setShowOffset(bool s);
};

/**
 * A legend entry for a model
 */
class CPD3ALGORITHMS_EXPORT ModelLegendEntry {
    QString line;
public:
    ModelLegendEntry();

    ModelLegendEntry(const ModelLegendEntry &other);

    ModelLegendEntry &operator=(const ModelLegendEntry &other);

    ModelLegendEntry(const QString &text);

    QString getLine() const;

    void setLine(const QString &l);
};

class ModelSingleComposite;

/**
 * A general purpose model.  This takes potentially multiple input double
 * values and produces potentially multiple output double values.
 */
class CPD3ALGORITHMS_EXPORT Model {
protected:
    enum ModelSerialType {
        SerialType_NULL = 0,
        SerialType_Constant,
        SerialType_Composite,
        SerialType_LogLog,
        SerialType_LogIn,
        SerialType_LogOut,
        SerialType_LogLogMultiple,
        SerialType_Linear,
        SerialType_LinearSingle, SerialType_LinearTransfer,
        SerialType_CSpline,
        SerialType_CSplineSingle,
        SerialType_BasicOrdinaryLSQ,
        SerialType_ZeroOrdinaryLSQ,
        SerialType_NormalCDFFit,
        SerialType_AllanFit,
        SerialType_GuassianDensity2D,
        SerialType_NPolynomialLMA,
        SerialType_ZeroNPolynomialLMA,
    };

    virtual void serialize(QDataStream &stream) const = 0;

    virtual void printLog(QDebug &stream) const;

    friend class ModelSingleComposite;

    static ModelLegendEntry legendPolynomial
            (const QVector<double> &coefficients, const ModelLegendParameters &parameters);

    static QString legendPolynomialString(const QVector<double> &coefficients,
                                          const QVector<double> &confidence,
                                          const ModelLegendParameters &parameters);


    static Model *fromConfiguration(const Data::Variant::Read &configuration,
                                    const QList<ModelInputConstraints> &inputs,
                                    const QList<ModelOutputConstraints> &outputs,
                                    const ModelParameters &parameters,
                                    const std::string &overrideType);

public:
    virtual ~Model();

    /**
     * Apply the model to the given set of inputs, producing a set of outputs.
     * 
     * @param inputs    the input values
     * @return          the result of the model
     */
    virtual QVector<double> applyConst(const QVector<double> &inputs) const = 0;

    /**
     * Apply the model to the given set of inputs, producing a set of outputs.
     * This may alter some internal state to speed later applications.
     * 
     * @param inputs    the input values
     * @return          the result of the model
     */
    virtual QVector<double> apply(const QVector<double> &inputs);

    /**
     * Apply the model to the given inputs, replacing the list with the
     * output.
     * 
     * @param values    the input values, replaced with the output
     */
    virtual void applyIOConst(QVector<double> &values) const;

    /**
     * Apply the model to the given inputs, replacing the list with the
     * output.  This may alter some internal state to speed later applications.
     * 
     * @param values    the input values, replaced with the output
     */
    virtual void applyIO(QVector<double> &values);

    /**
     * Apply the model to a single input, producing a single output.
     * 
     * @param input the input value
     * @return      the resulting value
     */
    virtual double oneToOneConst(double input) const;

    /**
     * Apply the model to a single input, producing a single output.
     * This may alter some internal state to speed later applications.
     * 
     * @param input the input value
     * @return      the resulting value
     */
    virtual double oneToOne(double input);

    /**
     * Apply the model to two inputs, producing a single output.
     * 
     * @param a     the first input
     * @param b     the second input
     * @return      the resulting value
     */
    virtual double twoToOneConst(double a, double b) const;

    /**
     * Apply the model to two inputs, producing a single output.
     * This may alter some internal state to speed later applications.
     * 
     * @param a     the first input
     * @param b     the second input
     * @return      the resulting value
     */
    virtual double twoToOne(double a, double b);

    /**
     * Apply the model to three inputs, producing a single output.
     * 
     * @param a     the first input
     * @param b     the second input
     * @param c     the third input
     * @return      the resulting value
     */
    virtual double threeToOneConst(double a, double b, double c) const;

    /**
     * Apply the model to three inputs, producing a single output.
     * This may alter some internal state to speed later applications.
     * 
     * @param a     the first input
     * @param b     the second input
     * @param c     the third input
     * @return      the resulting value
     */
    virtual double threeToOne(double a, double b, double c);

    /**
     * Apply the model to two inputs producing two outputs that replace
     * the input values.
     * 
     * @param a     the first input and output
     * @param b     the second input and output
     */
    virtual void applyIOConst(double &a, double &b) const;

    /**
     * Apply the model to two inputs producing two outputs that replace
     * the input values.  This may alter some internal state to speed later 
     * applications.
     * 
     * @param a     the first input and output
     * @param b     the second input and output
     */
    virtual void applyIO(double &a, double &b);

    /**
     * Apply the model to three inputs producing three outputs that replace
     * the input values.
     * 
     * @param a     the first input and output
     * @param b     the second input and output
     * @param c     the third input and output
     */
    virtual void applyIOConst(double &a, double &b, double &c) const;

    /**
     * Apply the model to three inputs producing three outputs that replace
     * the input values.  This may alter some internal state to speed later 
     * applications.
     * 
     * @param a     the first input and output
     * @param b     the second input and output
     * @param c     the third input and output
     */
    virtual void applyIO(double &a, double &b, double &c);


    /**
     * Duplicate this object, including polymorphism.  Allocates a new object
     * that must be deleted by the caller.
     * 
     * @return      the copy
     */
    virtual Model *clone() const = 0;

    /**
     * Get a short logable description of the model.
     * 
     * @return a log description of the model
     */
    virtual QString describe() const;

    /**
     * Get the legend entry for the model.
     * 
     * @param parameters    the parameters for the legend entry
     * @return              the legend entry
     */
    virtual ModelLegendEntry
            legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

    friend CPD3ALGORITHMS_EXPORT QDebug operator<<(QDebug stream, const Model *model);

    static Model *fromConfiguration(const Data::Variant::Read &configuration,
                                    const ModelParameters &parameters = ModelParameters());

    static Model *fromConfiguration(const Data::Variant::Read &configuration,
                                    int nInput,
                                    int nOutput,
                                    const ModelParameters &parameters = ModelParameters());

    static Model *fromConfiguration(const Data::Variant::Read &configuration,
                                    const QList<ModelInputConstraints> &inputs,
                                    const QList<ModelOutputConstraints> &outputs,
                                    const ModelParameters &parameters = ModelParameters());


    /** @see applyConst( const QVector<double> & ) */
    inline QVector<double> apply(const QVector<double> &inputs) const
    { return applyConst(inputs); }

    /** @see applyIOConst( QVector<double> & ) */
    inline void applyIO(QVector<double> &values) const
    { return applyIOConst(values); }

    /** @see oneToOneConst(double) */
    inline double oneToOne(double input) const
    { return oneToOneConst(input); }

    /** @see oneToOneConst(double) */
    inline double apply(double input) const
    { return oneToOneConst(input); }

    /** @see oneToOne(double) */
    inline double apply(double input)
    { return oneToOne(input); }

    /** @see twoToOneConst(double, double) */
    inline double twoToOne(double a, double b) const
    { return twoToOneConst(a, b); }

    /** @see threeToOneConst(double, double, double) */
    inline double threeToOne(double a, double b, double c) const
    { return threeToOneConst(a, b, c); }

    /** @see applyIOConst( double &, double & ) */
    inline void applyIO(double &a, double &b) const
    { applyIOConst(a, b); }

    /** @see applyIOConst( double &, double &, double & ) */
    inline void applyIO(double &a, double &b, double &c) const
    { applyIOConst(a, b, c); }


    /**
     * Handles wrapping for interpolated values and clips the input to the
     * possible range otherwise.  That is, this will move values to the other 
     * end of the range if the wrapped distance would be less.
     * 
     * @param c         the constraints to enforce
     * @param begin     a read/write iterator to the start of values
     * @param end       a read/write iterator to the end of values
     */
    template<typename AccessIterator>
    static void applyInterpolateConstraints(const ModelOutputConstraints &c,
                                            AccessIterator begin,
                                            AccessIterator end)
    {
        if (c.isNoop())
            return;
        if (begin == end)
            return;
        *begin = c.apply(*begin);
        double offset = c.getMin();
        for (AccessIterator i = begin + 1; i != end; ++i) {
            if (!c.isWrapping()) {
                *i = c.apply(*i);
                continue;
            }
            double v = c.apply(*i) - offset;
            double prior = c.apply(*(i - 1)) - offset;
            double delta = c.getMax() - c.getMin();
            if (fabs(v - prior) <= delta * 0.5) {
                *i = v + offset;
                continue;
            }
            if (v > prior) {
                offset -= delta;
            } else {
                offset += delta;
            }
            *i = v + offset;
        }
    }
};

namespace Internal {
class DeferredModelWrapperRunnable;
}

/**
 * A class that handles deffered (threaded) model creation.  On construction
 * this will queue a runnable with the global thread pool that starts model
 * creation.  That means that it is necessary to check that the model was
 * not created successfully before a signal connection, after the signal
 * is connected.
 */
class CPD3ALGORITHMS_EXPORT DeferredModelWrapper : public QObject {
Q_OBJECT

    Model *model;
    int finished;
    std::mutex mutex;
    std::condition_variable cond;

    void notifyModelReady(Model *m);

    friend class Internal::DeferredModelWrapperRunnable;

public:
    DeferredModelWrapper(const Data::Variant::Read &configuration,
                         const ModelParameters &parameters = ModelParameters());

    DeferredModelWrapper(const Data::Variant::Read &configuration,
                         int nInput,
                         int nOutput,
                         const ModelParameters &parameters = ModelParameters());

    DeferredModelWrapper(const Data::Variant::Read &configuration,
                         const QList<ModelInputConstraints> &inputs,
                         const QList<ModelOutputConstraints> &outputs,
                         const ModelParameters &parameters = ModelParameters());

    ~DeferredModelWrapper();

    bool isModelReady();

    bool waitForModelReady(unsigned long time = ULONG_MAX);

    Model *getModel();

    Model *takeModel();

    void deleteWhenFinished();

signals:

    void modelReady();
};

/**
 * A model that produces a constant output value.  The number of outputs
 * is equal to the number of inputs.
 */
class CPD3ALGORITHMS_EXPORT ModelConstant : public Model {
    double value;
public:
    virtual ~ModelConstant();

    ModelConstant(double value);

    virtual QVector<double> applyConst(const QVector<double> &inputs) const;

    virtual QVector<double> apply(const QVector<double> &inputs);

    virtual void applyIOConst(QVector<double> &values) const;

    virtual void applyIO(QVector<double> &values);

    virtual double oneToOneConst(double input) const;

    virtual double oneToOne(double input);

    virtual double twoToOneConst(double a, double b) const;

    virtual double twoToOne(double a, double b);

    virtual double threeToOneConst(double a, double b, double c) const;

    virtual double threeToOne(double a, double b, double c);

    virtual void applyIOConst(double &a, double &b) const;

    virtual void applyIO(double &a, double &b);

    virtual void applyIOConst(double &a, double &b, double &c) const;

    virtual void applyIO(double &a, double &b, double &c);

    virtual Model *clone() const;

    virtual QString describe() const;

    virtual ModelLegendEntry
            legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

protected:
    ModelConstant(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

/**
 * A model that applies a number of single input/single output sub-models
 * independently to each input, producing an equal number of outputs.
 */
class CPD3ALGORITHMS_EXPORT ModelSingleComposite : public Model {
    QVector<Model *> models;

    ModelSingleComposite(const Data::Variant::Read &configuration,
                         const QList<ModelInputConstraints> &inputs,
                         const QList<ModelOutputConstraints> &outputs,
                         const ModelParameters &parameters = ModelParameters(),
                         const std::string &overrideType = std::string());

    friend class Model;

public:
    virtual ~ModelSingleComposite();

    ModelSingleComposite(QVector<Model *> models);

    virtual QVector<double> applyConst(const QVector<double> &inputs) const;

    virtual QVector<double> apply(const QVector<double> &inputs);

    virtual void applyIOConst(QVector<double> &values) const;

    virtual void applyIO(QVector<double> &values);

    virtual double oneToOneConst(double input) const;

    virtual double oneToOne(double input);

    virtual double twoToOneConst(double a, double b) const;

    virtual double twoToOne(double a, double b);

    virtual double threeToOneConst(double a, double b, double c) const;

    virtual double threeToOne(double a, double b, double c);

    virtual void applyIOConst(double &a, double &b) const;

    virtual void applyIO(double &a, double &b);

    virtual void applyIOConst(double &a, double &b, double &c) const;

    virtual void applyIO(double &a, double &b, double &c);

    virtual Model *clone() const;

    virtual QString describe() const;

    virtual ModelLegendEntry
            legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

protected:
    ModelSingleComposite(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

}
}

#endif
