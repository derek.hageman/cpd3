/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSDEWPOINT_H
#define CPD3ALGORITHMSDEWPOINT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QVector>
#include "algorithms/algorithms.hxx"


namespace CPD3 {
namespace Algorithms {

/**
 * General routines for working with dewpoints and RHs.
 */
class CPD3ALGORITHMS_EXPORT Dewpoint {
public:
    static double dewpoint(double t, double rh, bool forceWater = false);

    static double rh(double t, double td, bool forceWater = false);

    static double t(double rh, double td, bool forceWater = false);

    static double rhExtrapolate(double t1, double rh1, double t2, bool forceWater = false);

    static double wetBulb(double t, double td, double P);
};

}
}

#endif
