/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSLINEARINT_H
#define CPD3ALGORITHMSLINEARINT_H

#include "core/first.hxx"

#include <functional>
#include <QtGlobal>
#include <QList>
#include <QtAlgorithms>
#include <QDebug>

#include "algorithms/algorithms.hxx"

#include "algorithms/model.hxx"


namespace CPD3 {
namespace Algorithms {


/**
 * A single constant slope linear interpolator.
 */
class CPD3ALGORITHMS_EXPORT SingleLinearInterpolator : public Model {
    double m;
    double b;
    ModelOutputConstraints constraints;

    inline double evaluate(double input) const
    { return constraints.apply(m * input + b); }

public:
    virtual ~SingleLinearInterpolator();

    SingleLinearInterpolator(double m,
                             double b = 0.0,
                             const ModelOutputConstraints &output = ModelOutputConstraints());

    SingleLinearInterpolator(double x0,
                             double y0,
                             double x1,
                             double y1,
                             const ModelOutputConstraints &output = ModelOutputConstraints());

    virtual QVector<double> applyConst(const QVector<double> &inputs) const;

    virtual QVector<double> apply(const QVector<double> &inputs);

    virtual void applyIOConst(QVector<double> &values) const;

    virtual void applyIO(QVector<double> &values);

    virtual double oneToOneConst(double input) const;

    virtual double oneToOne(double input);

    virtual double twoToOneConst(double a, double b) const;

    virtual double twoToOne(double a, double b);

    virtual double threeToOneConst(double a, double b, double c) const;

    virtual double threeToOne(double a, double b, double c);

    virtual void applyIOConst(double &a, double &b) const;

    virtual void applyIO(double &a, double &b);

    virtual void applyIOConst(double &a, double &b, double &c) const;

    virtual void applyIO(double &a, double &b, double &c);

    virtual Model *clone() const;

    virtual QString describe() const;

    virtual ModelLegendEntry legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

protected:
    SingleLinearInterpolator(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

/**
 * A piecewise linear interpolator.
 */
class CPD3ALGORITHMS_EXPORT LinearInterpolator : public Model {
    ModelOutputConstraints constraints;

    struct Component {
        double start;
        double a;
        double b;

        bool operator<(double d) const;
    };

    int nComponents;
    const Component *lastHit;
    Component *components;

    double evaluate(double input);

    double evaluate(double input) const;

    struct InputPoint {
        double x;
        double y;

        InputPoint(double x, double y);

        bool operator<(const InputPoint &other) const;
    };

    LinearInterpolator(const LinearInterpolator &other);

public:
    virtual ~LinearInterpolator();

    LinearInterpolator(const QVector<double> &x,
                       const QVector<double> &y,
                       const ModelOutputConstraints &output = ModelOutputConstraints());

    virtual QVector<double> applyConst(const QVector<double> &inputs) const;

    virtual QVector<double> apply(const QVector<double> &inputs);

    virtual void applyIOConst(QVector<double> &values) const;

    virtual void applyIO(QVector<double> &values);

    virtual double oneToOneConst(double input) const;

    virtual double oneToOne(double input);

    virtual double twoToOneConst(double a, double b) const;

    virtual double twoToOne(double a, double b);

    virtual double threeToOneConst(double a, double b, double c) const;

    virtual double threeToOne(double a, double b, double c);

    virtual void applyIOConst(double &a, double &b) const;

    virtual void applyIO(double &a, double &b);

    virtual void applyIOConst(double &a, double &b, double &c) const;

    virtual void applyIO(double &a, double &b, double &c);

    virtual Model *clone() const;

    virtual QString describe() const;

    virtual ModelLegendEntry legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

    enum LinearModelType {
        Type_Linear, Type_Sin, Type_Cos, Type_Sigmoid,
    };

    static Model *create(const QVector<double> &x,
                         const QVector<double> &y,
                         const ModelOutputConstraints &output = ModelOutputConstraints(),
                         LinearModelType type = Type_Linear,
                         double k = FP::undefined());


    /**
     * Evaluate linear interpolation over a given series of points.
     * 
     * @param xBegin    the start of the X points
     * @param xEnd      the end of the X points
     * @param yBegin    the start of the Y points
     * @param xTarget   the target value to evaluate at
     * @param compare   the X comparator
     * @return          the interpolated Y value
     */
    template<typename XIterator, typename YIterator, typename ValueType, typename XComparator>
    static ValueType interpolate(XIterator xBegin,
                                 XIterator xEnd,
                                 YIterator yBegin,
                                 ValueType xTarget,
                                 XComparator compare)
    {
        Q_ASSERT((xEnd - xBegin) > 1);

        XIterator upper = std::upper_bound(xBegin, xEnd, xTarget, compare);
        XIterator lower;
        if (upper == xEnd) {
            upper = xEnd - 1;
            lower = upper - 1;
        } else if (upper == xBegin) {
            lower = upper;
            upper = lower + 1;
        } else {
            lower = upper - 1;
        }

        yBegin += (lower - xBegin);
        YIterator yEnd = yBegin + 1;

        Q_ASSERT((*upper) != (*lower));
        return (*yBegin) + (xTarget - (*lower)) * ((*yEnd) - (*yBegin)) / ((*upper) - (*lower));
    }

    /**
     * Evaluate linear interpolation over a given series of points.
     * 
     * @param xBegin    the start of the X points
     * @param xEnd      the end of the X points
     * @param yBegin    the start of the Y points
     * @param xTarget   the target value to evaluate at
     * @return          the interpolated Y value
     */
    template<typename XIterator, typename YIterator, typename ValueType>
    static inline ValueType interpolate(XIterator xBegin,
                                        XIterator xEnd,
                                        YIterator yBegin,
                                        ValueType xTarget)
    {
        return interpolate(xBegin, xEnd, yBegin, xTarget, std::less<ValueType>());
    }

protected:
    LinearInterpolator(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

/**
 * A piecewise linear interpolator with a transfer function (usually a form of sigmoid) applied.
 */
class CPD3ALGORITHMS_EXPORT LinearTransferInterpolator : public Model {
    ModelOutputConstraints constraints;
public:
    /**
     * The available types of transfer functions.
     */
    enum TransferFunctionType {
        /** sin(x*pi/2)^k */
                Transfer_Sin,

        /** 1-cos(x*pi/2)^k */
                Transfer_Cos,

        /** A normalized and tunable sigmoid of the form x/(1+|x|) */
                Transfer_NormalizedSigmoid,
    };
private:

    struct Component {
        double start;
        double delta;
        double offset;
        double scale;

        bool operator<(double d) const;
    };

    TransferFunctionType type;
    double k;

    int nComponents;
    const Component *lastHit;
    Component *components;

    typedef double (*TransferFunction)(double, double);

    TransferFunction transfer;

    double evaluate(double input);

    double evaluate(double input) const;

    struct InputPoint {
        double x;
        double y;

        InputPoint(double x, double y);

        bool operator<(const InputPoint &other) const;
    };

    LinearTransferInterpolator(const LinearTransferInterpolator &other);

    void setTransfer();

public:
    virtual ~LinearTransferInterpolator();

    LinearTransferInterpolator(const QVector<double> &x,
                               const QVector<double> &y,
                               TransferFunctionType type,
                               double k = FP::undefined(),
                               const ModelOutputConstraints &output = ModelOutputConstraints());

    virtual QVector<double> applyConst(const QVector<double> &inputs) const;

    virtual QVector<double> apply(const QVector<double> &inputs);

    virtual void applyIOConst(QVector<double> &values) const;

    virtual void applyIO(QVector<double> &values);

    virtual double oneToOneConst(double input) const;

    virtual double oneToOne(double input);

    virtual double twoToOneConst(double a, double b) const;

    virtual double twoToOne(double a, double b);

    virtual double threeToOneConst(double a, double b, double c) const;

    virtual double threeToOne(double a, double b, double c);

    virtual void applyIOConst(double &a, double &b) const;

    virtual void applyIO(double &a, double &b);

    virtual void applyIOConst(double &a, double &b, double &c) const;

    virtual void applyIO(double &a, double &b, double &c);

    virtual Model *clone() const;

    virtual QString describe() const;

    virtual ModelLegendEntry legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

protected:
    LinearTransferInterpolator(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};


}
}

#endif
