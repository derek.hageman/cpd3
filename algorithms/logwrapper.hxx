/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSLOGWRAPPER_H
#define CPD3ALGORITHMSLOGWRAPPER_H

#include "core/first.hxx"

#include <QtGlobal>
#include "algorithms/algorithms.hxx"

#include "algorithms/model.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Algorithms {

/**
 * A wrapper that handles log/exponent input/output wrapping for another
 * model.
 */
class CPD3ALGORITHMS_EXPORT LogWrapper : public Model {
    Model *child;
    double inputBase;
    double outputBase;
    double lInputBase;

    double convertInput(double input) const;

    double convertOutput(double output) const;

    class LogIn : public Model {
        Model *child;
        double inputBase;
        double lInputBase;

        double convertInput(double input) const;

    public:
        virtual ~LogIn();

        LogIn(Model *wrap, double inBase);

        virtual QVector<double> applyConst(const QVector<double> &inputs) const;

        virtual QVector<double> apply(const QVector<double> &inputs);

        virtual void applyIOConst(QVector<double> &values) const;

        virtual void applyIO(QVector<double> &values);

        virtual double oneToOneConst(double input) const;

        virtual double oneToOne(double input);

        virtual double twoToOneConst(double a, double b) const;

        virtual double twoToOne(double a, double b);

        virtual double threeToOneConst(double a, double b, double c) const;

        virtual double threeToOne(double a, double b, double c);

        virtual void applyIOConst(double &a, double &b) const;

        virtual void applyIO(double &a, double &b);

        virtual void applyIOConst(double &a, double &b, double &c) const;

        virtual void applyIO(double &a, double &b, double &c);

        virtual Model *clone() const;

        virtual QString describe() const;

        virtual ModelLegendEntry
                legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

        friend CPD3ALGORITHMS_EXPORT QDataStream
                &operator<<(QDataStream &stream, const Model *model);

        friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

    protected:
        LogIn(QDataStream &stream);

        virtual void serialize(QDataStream &stream) const;

        virtual void printLog(QDebug &stream) const;
    };

    class LogOut : public Model {
        Model *child;
        double outputBase;

        double convertOutput(double output) const;

    public:
        virtual ~LogOut();

        LogOut(Model *wrap, double outBase);

        virtual QVector<double> applyConst(const QVector<double> &inputs) const;

        virtual QVector<double> apply(const QVector<double> &inputs);

        virtual void applyIOConst(QVector<double> &values) const;

        virtual void applyIO(QVector<double> &values);

        virtual double oneToOneConst(double input) const;

        virtual double oneToOne(double input);

        virtual double twoToOneConst(double a, double b) const;

        virtual double twoToOne(double a, double b);

        virtual double threeToOneConst(double a, double b, double c) const;

        virtual double threeToOne(double a, double b, double c);

        virtual void applyIOConst(double &a, double &b) const;

        virtual void applyIO(double &a, double &b);

        virtual void applyIOConst(double &a, double &b, double &c) const;

        virtual void applyIO(double &a, double &b, double &c);

        virtual Model *clone() const;

        virtual QString describe() const;

        virtual ModelLegendEntry
                legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

        friend CPD3ALGORITHMS_EXPORT QDataStream
                &operator<<(QDataStream &stream, const Model *model);

        friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

    protected:
        LogOut(QDataStream &stream);

        virtual void serialize(QDataStream &stream) const;

        virtual void printLog(QDebug &stream) const;
    };

public:
    virtual ~LogWrapper();

    LogWrapper(Model *wrap, double inBase, double outBase);

    virtual QVector<double> applyConst(const QVector<double> &inputs) const;

    virtual QVector<double> apply(const QVector<double> &inputs);

    virtual void applyIOConst(QVector<double> &values) const;

    virtual void applyIO(QVector<double> &values);

    virtual double oneToOneConst(double input) const;

    virtual double oneToOne(double input);

    virtual double twoToOneConst(double a, double b) const;

    virtual double twoToOne(double a, double b);

    virtual double threeToOneConst(double a, double b, double c) const;

    virtual double threeToOne(double a, double b, double c);

    virtual void applyIOConst(double &a, double &b) const;

    virtual void applyIO(double &a, double &b);

    virtual void applyIOConst(double &a, double &b, double &c) const;

    virtual void applyIO(double &a, double &b, double &c);

    virtual Model *clone() const;

    virtual QString describe() const;

    virtual ModelLegendEntry
            legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

    static Model *create
            (Model *model, double inBase = FP::undefined(), double outBase = FP::undefined());

protected:
    LogWrapper(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

/**
 * A wrapper that handles log/exponent input/output wrapping for another
 * model.  This version supports multiple different bases for each dimension.
 */
class CPD3ALGORITHMS_EXPORT LogWrapperMultiple : public Model {
    Model *child;
    QVector<double> inputBases;
    QVector<double> outputBases;
    QVector<double> lInputBases;

    double convertInput(double input, int dim) const;

    double convertOutput(double output, int dim) const;

public:
    virtual ~LogWrapperMultiple();

    LogWrapperMultiple(Model *wrap, const QVector<double> &inBase, const QVector<double> &outBase);

    virtual QVector<double> applyConst(const QVector<double> &inputs) const;

    virtual QVector<double> apply(const QVector<double> &inputs);

    virtual void applyIOConst(QVector<double> &values) const;

    virtual void applyIO(QVector<double> &values);

    virtual double oneToOneConst(double input) const;

    virtual double oneToOne(double input);

    virtual double twoToOneConst(double a, double b) const;

    virtual double twoToOne(double a, double b);

    virtual double threeToOneConst(double a, double b, double c) const;

    virtual double threeToOne(double a, double b, double c);

    virtual void applyIOConst(double &a, double &b) const;

    virtual void applyIO(double &a, double &b);

    virtual void applyIOConst(double &a, double &b, double &c) const;

    virtual void applyIO(double &a, double &b, double &c);

    virtual Model *clone() const;

    virtual QString describe() const;

    virtual ModelLegendEntry
            legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    static Model *create
            (Model *model, const QVector<double> &inputBases, const QVector<double> &outputBases);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

protected:
    LogWrapperMultiple(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

}
}

#endif
