/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "algorithms/crc.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/crc.hxx
 * CRC calculation functions.
 */

/*
 * 1990  Gary P. Mussar
 * This code is released to the public domain. There are no restrictions,
 * however, acknowledging the author by keeping this comment around
 * would be appreciated.
 *
 * CRC calculations are performed one byte at a time using a table lookup
 * mechanism.  Two routines are provided: one to initialize the CRC table;
 * and one to perform the CRC calculation over an array of bytes.
 *
 * A CRC is the remainder produced by dividing a generator polynomial into
 * a data polynomial using binary arthimetic (XORs). The data polynomial
 * is formed by using each bit of the data as a coefficient of a term in
 * the polynomial. These utilities assume the data communications ordering
 * of bits for the data polynomial, ie. the LSB of the first byte of data
 * is the coefficient of the highest term of the polynomial, etc..
 */

CRC16::CRC16(quint16 polynomial) : table(), accumulator(0)
{
    for (int i = 0; i < 256; i++) {
        quint16 crc = i;
        for (int j = 0; j < 8; j++) {
            crc = (crc >> 1) ^ ((crc & 1) ? polynomial : 0);
        }
        table[i] = crc;
    }
}

void CRC16::reset()
{
    accumulator = 0;
}

quint16 CRC16::calculate(const quint8 *data, size_t n, quint16 ac) const
{
    for (; n != 0; --n, data++) {
        ac = (ac >> 8) ^ table[(ac & 0xFF) ^ *data];
    }
    return ac;
}

quint16 CRC16::add(const QByteArray &data)
{
    accumulator = calculate((const quint8 *) (data.constData()), data.size(), accumulator);
    return accumulator;
}

quint16 CRC16::add(const void *data, size_t n)
{
    accumulator = calculate((const quint8 *) data, n, accumulator);
    return accumulator;
}

quint16 CRC16::add(const quint8 data)
{
    accumulator = calculate(&data, 1, accumulator);
    return accumulator;
}

quint16 CRC16::calculate(const QByteArray &data) const
{
    return calculate((const quint8 *) (data.constData()), data.size(), 0);
}

quint16 CRC16::calculate(const Util::ByteView &data) const
{
    return calculate(data.data<const quint8 *>(), data.size(), 0);
}

quint16 CRC16::calculate(const void *data, size_t n) const
{
    return calculate((const quint8 *) data, n, 0);
}

}
}
