/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSMIE_H
#define CPD3ALGORITHMSMIE_H

#include "core/first.hxx"

#include <vector>
#include <mutex>
#include <condition_variable>
#include <list>
#include <QDataStream>

#include "algorithms/algorithms.hxx"
#include "core/threading.hxx"


namespace CPD3 {
namespace Algorithms {

/**
 * Mei code calculation interface.
 */
class CPD3ALGORITHMS_EXPORT Mie {
    class Implementation;

    std::unique_ptr<Implementation> p;
public:
    /**
     * Type of Mie code being used.
     */
    enum Type {
        /**
         * Bohren and Huffman.
         */
        BohrenHuffman,
    };

    /**
     * Construct a Mie code calculator.
     * 
     * @param nAng  the number of angles to calculator between 0 and 90 degrees
     * @param riR   the refractive index real part
     * @param riI   the refractive index imaginary part
     * @param type  the type of code to use
     */
    Mie(double riR = 1.53, double riI = 0.001, int nAng = 2, Type t = BohrenHuffman);

    ~Mie();

    /**
     * Restore the Mie calculator from saved state.
     * 
     * @param stream the stream to restore from.
     */
    Mie(QDataStream &stream);

    /**
     * Save the Mie calculator for later restore.
     * 
     * @param stream the stream to save to
     */
    void serialize(QDataStream &stream) const;

    /**
     * Set the refractive index.
     * @param riR   the refractive index real part
     * @param riI   the refractive index complex part
     */
    void setRI(double riR, double riI);

    /**
     * Set the number of angles between 0 and 90 degrees.
     * @param nAng  the number of angles to calculator between 0 and 90 degrees
     */
    void setAngles(int nAng);

    /**
     * Run the Mie calculation for a given "X" (2*pi*a/lambda).
     * 
     * @param x     the X value to calculate
     * @param qsca  the output scattering efficiency or NULL
     * @param qbsc  the output back scattering efficiency or NULL
     * @param qabs  the output absorption efficiency or NULL
     * @param qext  the output extinction efficiency or NULL
     * @param gsca  the output asymmetry parameter or NULL
     */
    void calculate(double x,
                   double *qsca = NULL,
                   double *qbsc = NULL,
                   double *qabs = NULL,
                   double *qext = NULL,
                   double *gsca = NULL);

    /**
     * Create a copy of the calculator.
     * 
     * @return a new copy of the calculator.
     */
    Mie *clone() const;
};

/**
 * A class wrapping a Mie calculator that does linear interpolation between
 * output results to accelerate the run speed.
 * <br>
 * This table also allows for deferred calculation and partially exposes its
 * mutex to allow that mutex to be used to protect the callers data as well.
 * That is, during a deferred sequence if data is only accessed before the
 * release call and from the callback handler then it will always be accessed
 * while the table's mutex is held.
 */
class CPD3ALGORITHMS_EXPORT MieTable {
public:
    /**
     * A handler callback for deferred calculations.
     */
    class CPD3ALGORITHMS_EXPORT CalculateHandler {
    public:
        virtual ~CalculateHandler();

        /**
         * Process the result of the calculation.  This is only called while
         * the mutex for the table is held, so the table itself should not
         * be referenced but that mutex can be used to protect data accessed
         * by the handler.
         * 
         * @param qsca  the output scattering efficiency
         * @param qbsc  the output back scattering efficiency
         * @param qabs  the output absorption efficiency
         * @param qext  the output extinction efficiency
         * @param gsca  the output asymmetry parameter
         */
        virtual void mieResult(double qsca, double qbsc, double qabs, double qext, double gsca) = 0;
    };

private:
    std::unique_ptr<Mie> mie;

    struct TableEntry {
        double x;
        double qsca;
        double qbsc;
        double qabs;
        double qext;
        double gsca;
    };
    std::vector<TableEntry> table;
    bool first;

    struct DeferredLookup {
        double x;
        std::vector<CalculateHandler *> handlers;
    };
    std::vector<DeferredLookup> deferred;
    std::vector<std::unique_ptr<Mie>> deferredCalculatorPool;
    double lastManualDeferredX;

    std::mutex mutex;
    std::condition_variable internalWait;

    struct TableSort {
        inline bool operator()(const TableEntry &a, const TableEntry &b) const
        { return a.x < b.x; }

        inline bool operator()(const TableEntry &a, double b) const
        { return a.x < b; }

        inline bool operator()(double a, const TableEntry &b) const
        { return a < b.x; }

        inline bool operator()(const DeferredLookup &a, const DeferredLookup &b) const
        { return a.x < b.x; }

        inline bool operator()(const DeferredLookup &a, double b) const
        { return a.x < b; }

        inline bool operator()(double a, const DeferredLookup &b) const
        { return a < b.x; }
    };

    Threading::PoolRun pool;

    void runDeferred(double x);

    void startDeferred(double x, CalculateHandler *handler);

    int lookup(double x,
               std::vector<TableEntry>::iterator &lower,
               std::vector<TableEntry>::iterator &upper);

    void interpolate(double x,
                     std::vector<TableEntry>::const_iterator lower,
                     std::vector<TableEntry>::const_iterator upper,
                     double *qsca,
                     double *qbsc,
                     double *qabs,
                     double *qext,
                     double *gsca);

public:
    /**
     * Construct a new Mie table lookup executor.  This takes ownership
     * of the Mie calculator.
     * 
     * @param m     the Mie calculator to use
     * @param generateInitial   generate all points until the first call to scanDone()
     */
    MieTable(Mie *m, bool generateInitial = false);

    ~MieTable();

    /**
     * De-serialize the Mie table.
     * 
     * @param stream    the stream to restore from
     */
    MieTable(QDataStream &stream);

    /**
     * Serialize the Mie table.
     * 
     * @param stream    the stream to write to
     */
    void serialize(QDataStream &stream) const;

    /**
     * Run the Mie calculation for a given "X" (2*pi*a/lambda).  Note that this
     * can not be used while there are any outstanding deferred calculations.
     * 
     * @param x     the X value to calculate
     * @param qsca  the output scattering efficiency or NULL
     * @param qbsc  the output back scattering efficiency or NULL
     * @param qabs  the output absorption efficiency or NULL
     * @param qext  the output extinction efficiency or NULL
     * @param gsca  the output asymmetry parameter or NULL
     */
    void calculate(double x,
                   double *qsca = NULL,
                   double *qbsc = NULL,
                   double *qabs = NULL,
                   double *qext = NULL,
                   double *gsca = NULL);

    /**
     * Run a (potentially) deferred calculation.  Ownership of the handler
     * is taken by the the table.
     * 
     * @param x         the X value to calculate
     * @param hanlder   the callback handler to use
     */
    void deferredCalculate(double x, CalculateHandler *handler);

    /**
     * Begin a manually controlled deferred calculation.  This allows the
     * caller to explicitly handle the creation of the callback handler to
     * ensure no more than are needed are allocated.  endManualDeferCalculate()
     * must be called after handling as the internal mutex will be held until
     * then.
     * 
     * @param x     the X value to calculate
     * @param qsca  the output scattering efficiency or NULL
     * @param qbsc  the output back scattering efficiency or NULL
     * @param qabs  the output absorption efficiency or NULL
     * @param qext  the output extinction efficiency or NULL
     * @param gsca  the output asymmetry parameter or NULL
     * @return      true if the calculation needs to be deferred, which means issueManualDeferCalculate(CalculateHandler *) should be used as the results from this function will not be valid
     */
    bool beginManualDeferCalculate(double x,
                                   double *qsca = NULL,
                                   double *qbsc = NULL,
                                   double *qabs = NULL,
                                   double *qext = NULL,
                                   double *gsca = NULL);

    /**
     * Issue the manually deferred calculate for the X value used in the last
     * call to beginManualDeferCalculate( double, double *, double *, 
     * double *, double *, double *)
     * 
     * @param hanlder   the callback handler to use
     */
    void issueManualDeferCalculate(CalculateHandler *handler);

    /**
     * End a manual calculate call.  This releases the table.
     */
    void endManualDeferCalculate();

    /**
     * Inform the table lookup that the current scan is done.  This is used
     * in initial table calculation to generate the intermediate points.
     * <br>
     * This will also wait for all deferred calculations to complete.
     */
    void scanDone();
};

}
}

#endif
