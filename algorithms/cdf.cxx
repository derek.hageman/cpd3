/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVector>

#include "algorithms/cdf.hxx"
#include "algorithms/statistics.hxx"

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/cdf.hxx
 * Provides routines for generating CDF transforms.
 */

NormalCDF::NormalCDF()
{ }

NormalCDF::~NormalCDF()
{ }

void NormalCDF::applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                           TransformerAllocator *allocator) const
{
    Q_UNUSED(allocator);
    sortAndRemoveUndefined<0>(points);

    int n = points.size() + 2;
    int index = 1;
    for (QVector<std::shared_ptr<TransformerPoint> >::iterator it = points.begin(),
            end = points.end(); it != end; ++it, ++index) {
        if ((*it)->totalDimensions() < 2) {
            (*it)->set(0, Statistics::normalQuantile((double) index / (double) n));
        } else {
            (*it)->set(1, Statistics::normalQuantile((double) index / (double) n));
        }
    }
}

void NormalCDF::apply(QVector<std::shared_ptr<TransformerPoint> > &points,
                      TransformerAllocator *allocator)
{
    Q_UNUSED(allocator);
    sortAndRemoveUndefined<0>(points);

    int n = points.size() + 2;
    int index = 1;
    for (QVector<std::shared_ptr<TransformerPoint> >::iterator it = points.begin(),
            end = points.end(); it != end; ++it, ++index) {
        if ((*it)->totalDimensions() < 2) {
            (*it)->set(0, Statistics::normalQuantile((double) index / (double) n));
        } else {
            (*it)->set(1, Statistics::normalQuantile((double) index / (double) n));
        }
    }
}

void NormalCDF::applyConst(QVector<TransformerPoint *> &points,
                           TransformerAllocator *allocator) const
{
    Q_UNUSED(allocator);
    sortAndRemoveUndefined<0>(points);

    int n = points.size() + 2;
    int index = 1;
    for (QVector<TransformerPoint *>::iterator it = points.begin(), end = points.end();
            it != end;
            ++it, ++index) {
        if ((*it)->totalDimensions() < 2) {
            (*it)->set(0, Statistics::normalQuantile((double) index / (double) n));
        } else {
            (*it)->set(1, Statistics::normalQuantile((double) index / (double) n));
        }
    }
}

void NormalCDF::apply(QVector<TransformerPoint *> &points, TransformerAllocator *allocator)
{
    Q_UNUSED(allocator);
    sortAndRemoveUndefined<0>(points);

    int n = points.size() + 1;
    int index = 1;
    for (QVector<TransformerPoint *>::iterator it = points.begin(), end = points.end();
            it != end;
            ++it, ++index) {
        if ((*it)->totalDimensions() < 2) {
            (*it)->set(0, Statistics::normalQuantile((double) index / (double) n));
        } else {
            (*it)->set(1, Statistics::normalQuantile((double) index / (double) n));
        }
    }
}

Transformer *NormalCDF::clone() const
{ return new NormalCDF; }

NormalCDF::NormalCDF(QDataStream &stream)
{ Q_UNUSED(stream); }

void NormalCDF::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Transformer::SerialType_NormalCDF;
    stream << t;
}

void NormalCDF::printLog(QDebug &stream) const
{ stream << "NormalCDF"; }


NormalCDFFit::~NormalCDFFit()
{ }

NormalCDFFit::NormalCDFFit(const NormalCDFFit &other)
        : Model(),
          n(other.n),
          m(other.m),
          b(other.b),
          mean(other.mean),
          sd(other.sd),
          constraints(other.constraints)
{ }

/**
 * Construct the CDF fit for the given points
 * 
 * @param points    the data points
 * @param output    the output constraints to apply
 */
NormalCDFFit::NormalCDFFit(const QVector<double> &points, const ModelOutputConstraints &output)
        : constraints(output)
{
    Statistics::meanSD(points, mean, sd);
    calculateLine();
    n = 0;
    for (QVector<double>::const_iterator it = points.constBegin(), end = points.constEnd();
            it != end;
            ++it) {
        if (!FP::defined(*it) || *it <= 0.0)
            continue;
        ++n;
    }
}

void NormalCDFFit::calculateLine()
{
    if (FP::defined(mean) && FP::defined(sd) && sd > 0.0) {
        b = mean / -sd;
        m = 1.0 / sd;
    } else {
        m = FP::undefined();
        b = FP::undefined();
    }
}

QVector<double> NormalCDFFit::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

QVector<double> NormalCDFFit::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    for (int i = 0, max = inputs.size(); i < max; i++) {
        result.append(evaluate(inputs.at(i)));
    }
    return result;
}

void NormalCDFFit::applyIOConst(QVector<double> &values) const
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

void NormalCDFFit::applyIO(QVector<double> &values)
{
    for (int i = 0, max = values.size(); i < max; i++) {
        values[i] = evaluate(values.at(i));
    }
}

double NormalCDFFit::oneToOneConst(double input) const
{ return evaluate(input); }

double NormalCDFFit::oneToOne(double input)
{ return evaluate(input); }

double NormalCDFFit::twoToOneConst(double a, double b) const
{
    Q_UNUSED(b);
    return evaluate(a);
}

double NormalCDFFit::twoToOne(double a, double b)
{
    Q_UNUSED(b);
    return evaluate(a);
}

double NormalCDFFit::threeToOneConst(double a, double b, double c) const
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

double NormalCDFFit::threeToOne(double a, double b, double c)
{
    Q_UNUSED(b);
    Q_UNUSED(c);
    return evaluate(a);
}

void NormalCDFFit::applyIOConst(double &a, double &b) const
{
    a = evaluate(a);
    b = evaluate(b);
}

void NormalCDFFit::applyIO(double &a, double &b)
{
    a = evaluate(a);
    b = evaluate(b);
}

void NormalCDFFit::applyIOConst(double &a, double &b, double &c) const
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

void NormalCDFFit::applyIO(double &a, double &b, double &c)
{
    a = evaluate(a);
    b = evaluate(b);
    c = evaluate(c);
}

Model *NormalCDFFit::clone() const
{ return new NormalCDFFit(*this); }

QString NormalCDFFit::describe() const
{ return QString("NormalCDFFit(%1,%2)").arg(mean).arg(sd); }

/**
 * Get the confidence bound for the mean.
 * 
 * @param confidence    the confidence interval
 * @return              the mean confidence bound
 */
double NormalCDFFit::meanConfidence(double confidence) const
{
    if (FP::defined(mean) && FP::defined(sd) && sd > 0.0 && n > 1 && FP::defined(confidence)) {
        return Statistics::studentQuantile(n - 1, 1.0 - (1.0 - confidence) / 2.0) *
                (sd / sqrt((double) n));
    } else {
        return FP::undefined();
    }
}

/**
 * Get the confidence interval for the standard deviation.
 * 
 * @param lower         the output lower bound
 * @param upper         the output upper bound
 * @param confidence    the confidence interval
 */
void NormalCDFFit::sdConfidence(double &lower, double &upper, double confidence) const
{
    if (FP::defined(sd) && sd > 0.0 && n > 2 && FP::defined(confidence)) {
        double alpha = 1.0 - confidence;
        upper = sd * sqrt((double) (n - 1) / Statistics::chi2Quantile(n - 1, alpha / 2.0));
        lower = sd * sqrt((double) (n - 1) / Statistics::chi2Quantile(n - 1, 1.0 - alpha / 2.0));
    } else {
        lower = FP::undefined();
        upper = FP::undefined();
    }
}

/**
 * Return the number of points involved in the fit.
 * 
 * @return the number of points
 */
int NormalCDFFit::nPoints() const
{ return n; }

static bool requiresExponentialFormat(double v)
{
    if (!FP::defined(v))
        return false;
    if (fabs(v) < 0.002 || fabs(v) >= 1000.0)
        return true;
    return false;
}

//#define SHOW_SD_CONFIDENCE

ModelLegendEntry NormalCDFFit::legend(const ModelLegendParameters &parameters) const
{
    if (!FP::defined(mean) || !FP::defined(sd))
        return ModelLegendEntry();

    NumberFormat fmt;
    double mc = meanConfidence(parameters.getConfidence());

#ifdef SHOW_SD_CONFIDENCE
    double sdL = 0;
    double sdU = 0;
    sdConfidence(sdL, sdU, parameters.getConfidence());
    if (requiresExponentialFormat(mean) ||
            requiresExponentialFormat(sd) ||
            requiresExponentialFormat(mc) ||
            requiresExponentialFormat(sdL) ||
            requiresExponentialFormat(sdU)) {
        fmt = NumberFormat(2);
    }
#else
    if (requiresExponentialFormat(mean) ||
            requiresExponentialFormat(sd) ||
            requiresExponentialFormat(mc)) {
        fmt = NumberFormat(2);
    }
#endif

    QString result(QString::fromUtf8("x\xE2\x80\xBE = "));

    result.append(fmt.apply(mean));
    if (FP::defined(mc)) {
        result.append('(');
        result.append(QChar(0x00B1));
        result.append(fmt.apply(mc, QChar()));
        result.append(')');
    }

    result.append(QString::fromUtf8(" \xCF\x83 = "));
    result.append(fmt.apply(sd));
#ifdef SHOW_SD_CONFIDENCE
    if (FP::defined(sdL) && FP::defined(sdU)) {
        result.append('(');
        result.append(fmt.apply(sdL, QChar()));
        result.append(',');
        result.append(fmt.apply(sdU, QChar()));
        result.append(')');
    }
#else
    if (n > 0 && FP::defined(parameters.getConfidence())) {
        result.append(" N = ");
        result.append(QString::number(n));
    }
#endif

    return ModelLegendEntry(result);
}

NormalCDFFit::NormalCDFFit(QDataStream &stream)
{
    qint32 nr = 0;
    stream >> nr >> mean >> sd;
    n = (int) nr;
    calculateLine();
}

void NormalCDFFit::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_NormalCDFFit;
    stream << t << (qint32) n << mean << sd;
}

void NormalCDFFit::printLog(QDebug &stream) const
{ stream << "NormalCDFFit(" << mean << ',' << sd << ')'; }


}
}
