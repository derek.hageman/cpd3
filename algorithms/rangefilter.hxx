/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSRANGEFILTER_H
#define CPD3ALGORITHMSRANGEFILTER_H

#include "core/first.hxx"

#include <math.h>
#include <QtGlobal>
#include <QList>
#include <QDebug>

#include "algorithms/algorithms.hxx"

#include "core/number.hxx"
#include "algorithms/model.hxx"
#include "algorithms/transformer.hxx"


namespace CPD3 {
namespace Algorithms {

class RangeFilter;
namespace Internal {
typedef bool (*RangeFilterTestFunction)(const RangeFilter *, double);
}

/**
 * A filter that either accepts or rejects a range of data.
 */
class CPD3ALGORITHMS_EXPORT RangeFilter : public Transformer {
    double min;
    double max;
    int flags;
    int dimension;
    Internal::RangeFilterTestFunction function;

    void setFunction();

public:
    enum {
        Reject_Range = 0x1, Min_Inclusive = 0x2, Max_Inclusive = 0x4, Reject_Remove = 0x8,
    };

    RangeFilter(double setMin, double setMax, int setFlags = 0, int onlyDimension = -1);

    ~RangeFilter();

    virtual void applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                            TransformerAllocator *allocator = NULL) const;

    virtual void
            applyConst(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL)
            const;

    virtual Transformer *clone() const;

    /**
     * Get the filter minimum.
     * @return the minimum
     */
    inline double getMin() const
    { return min; }

    /**
     * Get the filter maximum.
     * @return the maximum
     */
    inline double getMax() const
    { return max; }

protected:
    friend class Transformer;

    RangeFilter(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator<<(QDataStream &stream, const Transformer *transformer);

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator>>(QDataStream &stream, Transformer *&transformer);

    friend CPD3ALGORITHMS_EXPORT QDebug operator<<(QDebug stream, const Transformer *transformer);
};

}
}

#endif
