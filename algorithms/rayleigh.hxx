/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSRAYLEIGH_H
#define CPD3ALGORITHMSRAYLEIGH_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "algorithms/algorithms.hxx"


namespace CPD3 {
namespace Algorithms {

/**
 * Mei code calculation interface.
 */
class CPD3ALGORITHMS_EXPORT Rayleigh {
public:
    /** The type of gas to calculate for. */
    enum Gas {
        Air, CO2, FM200, SF6, R12, R22, R134
    };

    /**
     * Calculate Rayleigh scattering at the given wavelength for a type of
     * gas.
     * 
     * @param wavelength        the wavelength to calculate at in nm
     * @param type              the type of gas
     * @param t                 the temperature
     * @param p                 the pressure
     * @return                  the Rayleigh scattering of the gas in Mm-1
     */
    static double scattering(double wavelength, Gas type = Air, double t = 0.0, double p = 1013.25);

    /**
     * Calculate the Rayleigh scattering over a given angle sweep.  This
     * calculation considers depolorization that the simpler form does
     * not even when calculating total (0-180) scattering.
     * <br>
     * For example, backscattering starts at 90 degrees and ends at 180.
     * 
     * @param wavelength        the wavelength to calculate at in nm
     * @param startAngle        the start of the angle sweep
     * @param endAngle          the end of the angle sweep
     * @param type              the type of gas
     * @param t                 the temperature
     * @param p                 the pressure
     * @return                  the Rayleigh scattering of the gas in Mm-1 over the given angles
     */
    static double angleScattering(double wavelength,
                                  double startAngle = 0.0,
                                  double endAngle = 180.0,
                                  Gas type = Air,
                                  double t = 0.0,
                                  double p = 1013.25);

    /**
     * Get a gas type from a string name.
     * 
     * @param string        the string name of the gas
     * @return              the gas type
     */
    static Gas gasFromString(const QString &string);

    /**
     * Return the string name of a gas.
     * 
     * @param gas           the gas type
     * @return              the string name
     */
    static QString gasToString(Gas gas);
};

}
}

#endif
