/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSTRANSFORMER_H
#define CPD3ALGORITHMSTRANSFORMER_H

#include "core/first.hxx"

#include <string.h>
#include <algorithm>
#include <mutex>
#include <condition_variable>
#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QVector>
#include <QHash>
#include <QDataStream>
#include <QVariant>
#include <QDebug>
#include "algorithms/algorithms.hxx"

#include "core/number.hxx"
#include "datacore/variant/root.hxx"


namespace CPD3 {
namespace Algorithms {

class Transformer;

class TransformerPoint;

class TransformerChain;

CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Transformer *transformer);

CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Transformer *&transformer);

CPD3ALGORITHMS_EXPORT QDebug operator<<(QDebug stream, const Transformer *transformer);


/**
 * The allocator used for adding points in a transformer.
 */
class CPD3ALGORITHMS_EXPORT TransformerAllocator {
public:
    TransformerAllocator();

    virtual ~TransformerAllocator();

    virtual TransformerPoint *create(const Transformer *tr) = 0;

    virtual TransformerPoint *copy(const TransformerPoint *origin) = 0;
};

/**
 * A general wrapper for transformer parameters.
 */
class CPD3ALGORITHMS_EXPORT TransformerParameters {
    QHash<QString, QVariant> parameters;
public:
    TransformerParameters();

    TransformerParameters(const TransformerParameters &other);

    TransformerParameters &operator=(const TransformerParameters &other);

    TransformerParameters(const QHash<QString, QVariant> &params);

    QVariant getParameter(const QString &name) const;

    void setParameter(const QString &name, const QVariant &value);

    double getDouble(const QString &name) const;

    void setDouble(const QString &name, double value);
};


/**
 * The base class for transformer points.
 */
class CPD3ALGORITHMS_EXPORT TransformerPoint {
public:
    virtual ~TransformerPoint();

    /**
     * Get the value of the given dimension.
     * 
     * @param dim   the dimension index
     * @return      the value
     */
    virtual double get(int dim) const = 0;

    /**
     * Set the value of the given dimension.
     * 
     * @param dim   the dimension index
     * @param value the new value
     */
    virtual void set(int dim, double value) = 0;

    /**
     * Get the total number of dimensions.
     * 
     * @return the total number of dimensions
     */
    virtual int totalDimensions() const = 0;
};

/**
 * A statically sized point.
 * 
 * @param N the number of dimensions
 */
template<int N>
class TransformerPointStatic : public TransformerPoint {
    double values[N];
public:
    virtual ~TransformerPointStatic()
    { }

    TransformerPointStatic()
    {
        for (int i = 0; i < N; i++) {
            values[i] = FP::undefined();
        }
    }

    TransformerPointStatic(double v)
    {
        values[0] = v;
        if (N > 1) {
            for (int i = 1; i < N; i++) {
                values[i] = FP::undefined();
            }
        }
    }

    TransformerPointStatic(double v1, double v2)
    {
        Q_ASSERT(N > 1);
        values[0] = v1;
        values[1] = v2;
        if (N > 2) {
            for (int i = 2; i < N; i++) {
                values[i] = FP::undefined();
            }
        }
    }

    TransformerPointStatic(double v1, double v2, double v3)
    {
        if (N < 3) {
            Q_ASSERT(false);
            return;
        }
        values[0] = v1;
        values[1] = v2;
        values[2] = v3;
        if (N > 3) {
            for (int i = 3; i < N; i++) {
                values[i] = FP::undefined();
            }
        }
    }

    TransformerPointStatic(const double *input)
    {
        memcpy(this->values, input, N * sizeof(double));
    }

    TransformerPointStatic(const QVector<double> &values)
    {
        if (values.size() >= N) {
            memcpy(this->values, values.data(), N * sizeof(double));
        } else {
            int split = qMin(values.size(), N);
            memcpy(this->values, values.data(), split * sizeof(double));
            for (int i = split; i < N; i++) {
                this->values[i] = FP::undefined();
            }
        }
    }

    TransformerPointStatic(const QList<double> &values)
    {
        int split = qMin(values.size(), N);
        for (int i = 0; i < split; i++) {
            this->values[i] = values.at(i);
        }
        for (int i = split; i < N; i++) {
            this->values[i] = FP::undefined();
        }
    }

    TransformerPointStatic(const TransformerPointStatic<N> &other) : TransformerPoint()
    {
        memcpy(this->values, other.values, N * sizeof(double));
    }

    virtual double get(int dim) const
    {
        Q_ASSERT(dim >= 0 && dim <= N);
        return values[dim];
    }

    virtual void set(int dim, double value)
    {
        Q_ASSERT(dim >= 0 && dim <= N);
        values[dim] = value;
    }

    virtual int totalDimensions() const
    { return N; }

    inline double &operator[](int dim)
    { return values[dim]; }

    inline const double &operator[](int dim) const
    { return values[dim]; }

    inline bool operator==(const TransformerPointStatic<N> &other) const
    {
        for (int i = 0; i < N; i++) {
            if (!FP::equal(values[i], other.values[i]))
                return false;
        }
        return true;
    }

    inline bool operator!=(const TransformerPointStatic<N> &other) const
    {
        for (int i = 0; i < N; i++) {
            if (!FP::equal(values[i], other.values[i]))
                return true;
        }
        return false;
    }

    /**
     * The basic implementation for the allocator of static points. 
     */
    class Allocator : public Algorithms::TransformerAllocator {
    public:
        Allocator()
        { }

        virtual ~Allocator()
        { }

        virtual TransformerPoint *create(const Transformer *tr)
        {
            Q_UNUSED(tr);
            return new TransformerPointStatic<N>();
        }

        virtual TransformerPoint *copy(const TransformerPoint *origin)
        {
            return new TransformerPointStatic<N>(
                    *(static_cast<const TransformerPointStatic<N> *>(origin)));
        }
    };

    /**
     * A static instance of the simple allocator for general use.
     */
    static Allocator allocator;
};

template<int M> typename TransformerPointStatic<M>::Allocator
        TransformerPointStatic<M>::allocator;

/**
 * A general transformer model.  This transforms a set of input points to
 * a set of outputs.
 */
class CPD3ALGORITHMS_EXPORT Transformer {
protected:
    enum TransformerSerialType {
        SerialType_NULL = 0,
        SerialType_NOOP,
        SerialType_Constant,
        SerialType_Chain,
        SerialType_NormalCDF,
        SerialType_Allan,
        SerialType_RangeFilter,
        SerialType_RangeWrap,
        SerialType_QuantileFilter,
        SerialType_FourierPowerSpectrum,
    };

    virtual void serialize(QDataStream &stream) const = 0;

    virtual void printLog(QDebug &stream) const;

    friend class TransformerChain;

    /**
     * A sorting comparator that sorts transformer points (either direct or
     * wrapped pointers) based on the given dimension, but puts all undefined
     * values at the end of the list.
     * 
     * @param a         the first point
     * @param b         the second point
     * @param PointType the point type to inspect
     * @param Dimension the dimension index to sort by
     * @return          true if a is less than b
     */
    template<int Dimension, typename PointType>
    static bool undefinedSortComparator(const PointType &a, const PointType &b)
    {
        if (!FP::defined(b->get(Dimension)))
            return FP::defined(a->get(Dimension));
        if (!FP::defined(a->get(Dimension)))
            return false;
        return a->get(Dimension) < b->get(Dimension);
    }

    /**
     * Find the first undefined value in a list sorted such that all undefined
     * values are at the end.
     * 
     * @param list      the list to search
     * @param ListType  the type of the list, generally a QVector of pointers
     * @param Dimension the dimension index to search in
     */
    template<int Dimension, typename ListType>
    static typename ListType::iterator findFirstUndefined(ListType &list)
    {
        if (list.empty() || FP::defined(list.last()->get(Dimension)))
            return list.end();
        typename ListType::iterator begin = list.begin();
        typename ListType::iterator middle;
        int n = list.size();
        int half;
        while (n > 0) {
            half = n >> 1;
            middle = begin + half;
            if (FP::defined((*middle)->get(Dimension))) {
                begin = middle + 1;
                n -= half + 1;
            } else {
                n = half;
            }
        }
        return begin;
    }

    /**
     * Sort a list of points by the given dimension and remove all undefined
     * values from the list.
     * 
     * @param list      the list to sort
     * @param Dimension the dimension index to sort by
     * @param PointType the type of the points
     * @Param ListType  the type of the list
     */
    template<int Dimension, typename PointType, template<typename ListTemplateType> class ListType>
    static void sortAndRemoveUndefined(ListType<PointType> &list)
    {
        std::sort(list.begin(), list.end(), undefinedSortComparator<Dimension, PointType>);
        typename ListType<PointType>::iterator end = list.end();
        typename ListType<PointType>::iterator begin = findFirstUndefined<Dimension>(list);
        if (begin != end) {
            list.erase(begin, end);
        }
    }

public:
    virtual ~Transformer();

    /**
     * Apply the transform to the given inputs. 
     * 
     * @param points    the input and output points
     * @param allocator the allocator used to add points or NULL to disable
     */
    virtual void applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                            TransformerAllocator *allocator = NULL) const = 0;

    /**
     * Apply the transform to the given inputs.  
     * 
     * @param points the input and output points
     * @param allocator the allocator used to add points or NULL to disable
     */
    virtual void apply(QVector<std::shared_ptr<TransformerPoint> > &points,
                       TransformerAllocator *allocator = NULL);

    /** @see applyConst( QVector<std::shared_ptr<TransformerPoint> > &, 
     * TransformerAllocator ) const */
    inline void apply(QVector<std::shared_ptr<TransformerPoint> > &points,
                      TransformerAllocator *allocator = NULL) const
    { applyConst(points, allocator); }

    /**
     * Apply the transform to the given inputs.  This will delete any
     * entries that are removed from the list.
     * 
     * @param points the input and output points
     * @param allocator the allocator used to add points or NULL to disable
     */
    virtual void
            applyConst(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL)
            const = 0;

    /**
     * Apply the transform to the given inputs.  This will delete any
     * entries that are removed from the list.
     * 
     * @param points the input and output points
     * @param allocator the allocator used to add points or NULL to disable
     */
    virtual void apply(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL);

    /** @see applyConst( QVector<TransformerPoint *> & ) const */
    inline void apply(QVector<TransformerPoint *> &points,
                      TransformerAllocator *allocator = NULL) const
    { applyConst(points, allocator); }

    /**
     * Create a copy of the transformer, including polymorphism.
     * 
     * @return a copy of the transformer
     */
    virtual Transformer *clone() const = 0;


    /**
     * Create a transformer from the given configuration.
     * 
     * @param configuration the configuration
     * @param parameters    the parameters
     * @return              a new transformer
     */
    static Transformer *fromConfiguration(const Data::Variant::Read &configuration,
                                          const TransformerParameters &parameters = TransformerParameters());

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator<<(QDataStream &stream, const Transformer *transformer);

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator>>(QDataStream &stream, Transformer *&transformer);

    friend CPD3ALGORITHMS_EXPORT QDebug operator<<(QDebug stream, const Transformer *transformer);
};

/**
 * A NO-OP transformer.  This does nothing.
 */
class CPD3ALGORITHMS_EXPORT TransformerNOOP : public Transformer {
public:
    TransformerNOOP();

    ~TransformerNOOP();

    virtual void applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                            TransformerAllocator *allocator = NULL) const;

    virtual void apply(QVector<std::shared_ptr<TransformerPoint> > &points,
                       TransformerAllocator *allocator = NULL);

    virtual void
            applyConst(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL)
            const;

    virtual void apply(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL);

    virtual Transformer *clone() const;

protected:
    friend class Transformer;

    TransformerNOOP(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator<<(QDataStream &stream, const Transformer *transformer);

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator>>(QDataStream &stream, Transformer *&transformer);

    friend CPD3ALGORITHMS_EXPORT QDebug operator<<(QDebug stream, const Transformer *transformer);
};

/**
 * A transformer that sets all input values to a constant.
 */
class CPD3ALGORITHMS_EXPORT TransformerConstant : public Transformer {
    double value;
public:
    TransformerConstant(double sv);

    ~TransformerConstant();

    virtual void applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                            TransformerAllocator *allocator = NULL) const;

    virtual void apply(QVector<std::shared_ptr<TransformerPoint> > &points,
                       TransformerAllocator *allocator = NULL);

    virtual void
            applyConst(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL)
            const;

    virtual void apply(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL);

    virtual Transformer *clone() const;

protected:
    friend class Transformer;

    TransformerConstant(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator<<(QDataStream &stream, const Transformer *transformer);

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator>>(QDataStream &stream, Transformer *&transformer);

    friend CPD3ALGORITHMS_EXPORT QDebug operator<<(QDebug stream, const Transformer *transformer);
};

/**
 * A chain of transformers.  Each is applied one after the other to
 * produce the result.
 */
class CPD3ALGORITHMS_EXPORT TransformerChain : public Transformer {
    QVector<Transformer *> transformers;
public:
    /**
     * Create a new chain.  This takes ownership of the components of it.
     * 
     * @param chain     the chain, in order of application
     */
    TransformerChain(const QVector<Transformer *> &chain);

    ~TransformerChain();

    virtual void applyConst(QVector<std::shared_ptr<TransformerPoint> > &points,
                            TransformerAllocator *allocator = NULL) const;

    virtual void apply(QVector<std::shared_ptr<TransformerPoint> > &points,
                       TransformerAllocator *allocator = NULL);

    virtual void
            applyConst(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL)
            const;

    virtual void apply(QVector<TransformerPoint *> &points, TransformerAllocator *allocator = NULL);

    virtual Transformer *clone() const;

protected:
    friend class Transformer;

    TransformerChain(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator<<(QDataStream &stream, const Transformer *transformer);

    friend CPD3ALGORITHMS_EXPORT QDataStream
            &operator>>(QDataStream &stream, Transformer *&transformer);

    friend CPD3ALGORITHMS_EXPORT QDebug operator<<(QDebug stream, const Transformer *transformer);
};

namespace Internal {
class DeferredTransformerWrapperRunnable;
}

/**
 * A class that handles deffered (threaded) transformer application.  
 * On construction this will queue a runnable with the global thread pool 
 * that starts transformer creation.  That means that it is necessary to check 
 * that the transformer was not finished before a signal connection, 
 * after the signal is connected.
 */
class CPD3ALGORITHMS_EXPORT DeferredTransformerWrapper : public QObject {
Q_OBJECT

    int finished;
    QVector<TransformerPoint *> points;
    QVector<std::shared_ptr<TransformerPoint> > pointsShared;
    std::mutex mutex;
    std::condition_variable cond;

    void notifyReady(const QVector<TransformerPoint *> &points);

    void notifyReady(const QVector<std::shared_ptr<TransformerPoint> > &points);

    friend class Internal::DeferredTransformerWrapperRunnable;

public:
    DeferredTransformerWrapper(const QVector<TransformerPoint *> &points,
                               const Data::Variant::Read &configuration,
                               const TransformerParameters &parameters = TransformerParameters(),
                               TransformerAllocator *allocator = NULL);

    DeferredTransformerWrapper(const QVector<std::shared_ptr<TransformerPoint> > &points,
                               const Data::Variant::Read &configuration,
                               const TransformerParameters &parameters = TransformerParameters(),
                               TransformerAllocator *allocator = NULL);

    DeferredTransformerWrapper(const QVector<TransformerPoint *> &points,
                               Transformer *transformer,
                               TransformerAllocator *allocator = NULL);

    DeferredTransformerWrapper(const QVector<std::shared_ptr<TransformerPoint> > &points,
                               Transformer *transformer,
                               TransformerAllocator *allocator = NULL);

    ~DeferredTransformerWrapper();

    bool isReady();

    bool waitForReady(unsigned long time = ULONG_MAX);

    QVector<TransformerPoint *> getPoints(bool *ready = NULL);

    QVector<std::shared_ptr<TransformerPoint> > getSharedPoints(bool *ready = NULL);

    QVector<TransformerPoint *> takePoints(bool *ready = NULL);

    QVector<std::shared_ptr<TransformerPoint> > takeSharedPoints(bool *ready = NULL);

    void deleteWhenFinished();

signals:

    void pointsReady();
};

}
}

#endif
