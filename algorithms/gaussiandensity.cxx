/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <vector>
#include <QVector>

#include "algorithms/gaussiandensity.hxx"

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/gaussiandensity.hxx
 * Provides a model that generates a density field from Gaussian kernels.
 */

GaussianDensity2D::~GaussianDensity2D()
{ }

static double densityContribution(double pointX,
                                  double pointY,
                                  double x,
                                  double y,
                                  double scaleX,
                                  double scaleY)
{
    double dX = x - pointX;
    double dY = y - pointY;
    dX = dX * dX;
    dY = dY * dY;
    return exp((dX * scaleX) + (dY * scaleY));
}

#define N_CELLS         512
#define N_CELL_POINTS   4
static const double kernelInfluenceDistance = 4.0;

/**
 * Create a new Gaussian density field from the given points.
 * 
 * @param x             the X coordinates
 * @param y             the X coordinates
 * @param kernelDensity the density of the kernels, lower gives larger "spots"
 * @param weightLimit   the maximum weight limit as a fraction of the number of points
 * @param output        the output constraints
 */
GaussianDensity2D::GaussianDensity2D(const QVector<double> &x,
                                     const QVector<double> &y,
                                     double kernelDensity,
                                     double weightLimit, const ModelOutputConstraints &output)
        : constraints(output),
          minX(FP::undefined()),
          maxX(FP::undefined()),
          minY(FP::undefined()),
          maxY(FP::undefined()),
          scaleX(0),
          scaleY(0),
          maximumResult(0)
{
    if (!FP::defined(kernelDensity) || kernelDensity <= 0.0)
        kernelDensity = 100.0;

    for (int i = 0, max = qMin(x.size(), y.size()); i < max; i++) {
        double xv = x.at(i);
        double yv = y.at(i);
        if (!FP::defined(xv) || !FP::defined(yv))
            continue;

        xPoints.push_back(xv);
        yPoints.push_back(yv);

        if (!FP::defined(minX)) {
            minX = xv;
            maxX = minX;
            minY = yv;
            maxY = minY;
        } else {
            if (xv < minX) minX = xv;
            if (xv > maxX) maxX = xv;
            if (yv < minY) minY = yv;
            if (yv > maxY) maxY = yv;
        }
    }

    if (xPoints.size() == 0)
        return;

    if (!FP::defined(weightLimit) || weightLimit <= 0.0 || weightLimit >= 1.0)
        maximumResult = (double) xPoints.size() * 2.0;
    else
        maximumResult = qMax((double) xPoints.size() * weightLimit, 1.0);

    scaleX = -1.0;
    scaleY = -1.0;
    double influenceX = 1.0;
    if (minX != maxX) {
        scaleX = 2.0 * (maxX - minX) / kernelDensity;
        scaleX = -1.0 / (scaleX * scaleX);
        influenceX = ((maxX - minX) / kernelDensity) * kernelInfluenceDistance;
    }
    minX -= influenceX;
    maxX += influenceX;

    double influenceY = 1.0;
    if (minY != maxY) {
        scaleY = 2.0 * (maxY - minY) / kernelDensity;
        scaleY = -1.0 / (scaleY * scaleY);
        influenceY = ((maxY - minY) / kernelDensity) * kernelInfluenceDistance;
    }
    minY -= influenceY;
    maxY += influenceY;

    matrix.resize(N_CELLS * N_CELLS, 0);
    matrixPoints.resize(N_CELLS * N_CELLS * N_CELL_POINTS, -1);

    cellXScale = (double) N_CELLS / (maxX - minX);
    cellYScale = (double) N_CELLS / (maxY - minY);
    for (int pointIndex = 0, countPoints = xPoints.size(); pointIndex < countPoints; pointIndex++) {
        double pointX = xPoints[pointIndex];
        double pointY = yPoints[pointIndex];

        int minCellX = (int) floor(((pointX - influenceX) - minX) * cellXScale);
        if (minCellX < 0) minCellX = 0;
        int maxCellX = (int) ceil(((pointX + influenceX) - minX) * cellXScale);
        if (maxCellX >= N_CELLS) maxCellX = N_CELLS - 1;

        int minCellY = (int) floor(((pointY - influenceY) - minY) * cellYScale);
        if (minCellY < 0) minCellY = 0;
        int maxCellY = (int) ceil(((pointY + influenceY) - minY) * cellYScale);
        if (maxCellY >= N_CELLS) maxCellY = N_CELLS - 1;

        for (int cellY = minCellY; cellY <= maxCellY; cellY++) {
            double yv = ((double) cellY / cellYScale) + minY;
            size_t matrixBase = cellY * N_CELLS;
            for (int cellX = minCellX; cellX <= maxCellX; cellX++) {
                double xv = ((double) cellX / cellXScale) + minX;
                size_t matrixIndex = matrixBase + cellX;

                double worstDistance = 0;
                size_t worstIndex = (size_t) -1;
                for (size_t matrixPointIndex = matrixIndex * N_CELL_POINTS,
                        matrixPointEnd = (matrixIndex + 1) * N_CELL_POINTS;
                        matrixPointIndex < matrixPointEnd;
                        ++matrixPointIndex) {
                    int mpIdx = matrixPoints[matrixPointIndex];
                    if (mpIdx == -1) {
                        worstIndex = matrixPointIndex;
                        break;
                    }
                    double dX = xPoints[mpIdx] - xv;
                    double dY = yPoints[mpIdx] - yv;
                    dX = dX * dX;
                    dY = dY * dY;
                    double distance = dX + dY;

                    if (worstIndex == (size_t) -1 || distance > worstDistance) {
                        worstIndex = matrixPointIndex;
                        worstDistance = distance;
                    }
                }
                Q_ASSERT(worstIndex != (size_t) -1);

                int mpIdx = matrixPoints[worstIndex];
                if (mpIdx != -1) {
                    matrix[matrixIndex] +=
                            densityContribution(xPoints[mpIdx], yPoints[mpIdx], xv, yv, scaleX,
                                                scaleY);
                }
                matrixPoints[worstIndex] = pointIndex;
            }
        }
    }
}

double GaussianDensity2D::cellValue(size_t cell, double x, double y) const
{
    double value = matrix[cell];
    for (size_t pointIndex = cell * N_CELL_POINTS, pointEnd = (cell + 1) * N_CELL_POINTS;
            pointIndex < pointEnd;
            ++pointIndex) {
        int mpIdx = matrixPoints[pointIndex];
        if (mpIdx == -1)
            break;
        value += densityContribution(xPoints[mpIdx], yPoints[mpIdx], x, y, scaleX, scaleY);
    }
    return qMin(value, maximumResult);
}

double GaussianDensity2D::twoToOneConst(double a, double b) const
{
    if (!FP::defined(minX))
        return FP::undefined();

    int cellLowerX;
    int cellUpperX;
    if (a <= minX) {
        cellLowerX = 0;
        cellUpperX = -1;
    } else if (a >= maxX) {
        cellLowerX = N_CELLS - 1;
        cellUpperX = -1;
    } else {
        cellLowerX = (int) floor((a - minX) * cellXScale);
        if (cellLowerX < 0) cellLowerX = 0;
        cellUpperX = (int) ceil((a - minX) * cellXScale);
        if (cellUpperX >= N_CELLS) cellUpperX = N_CELLS - 1;
    }

    int cellLowerY;
    int cellUpperY;
    if (b <= minY) {
        cellLowerY = 0;
        cellUpperY = -1;
    } else if (b >= maxY) {
        cellLowerY = N_CELLS - 1;
        cellUpperY = -1;
    } else {
        cellLowerY = (int) floor((b - minY) * cellYScale);
        if (cellLowerY < 0) cellLowerY = 0;
        cellUpperY = (int) ceil((b - minY) * cellYScale);
        if (cellUpperY >= N_CELLS) cellUpperY = N_CELLS - 1;
    }

    if (cellUpperX == -1 || cellLowerX == cellUpperX) {
        Q_ASSERT(cellLowerX != -1);
        if (cellUpperY == -1 || cellLowerY == cellUpperY) {
            Q_ASSERT(cellLowerY != -1);
            return cellValue((size_t) cellLowerY * N_CELLS + (size_t) cellLowerX, a, b);
        }

        double lowerValue = cellValue((size_t) cellLowerY * N_CELLS + (size_t) cellLowerX, a, b);
        double upperValue = cellValue((size_t) cellUpperY * N_CELLS + (size_t) cellLowerX, a, b);

        double yvLower = ((double) cellLowerY / cellYScale) + minY;
        double yvUpper = ((double) cellUpperY / cellYScale) + minY;
        double weight = (b - yvLower) / (yvUpper - yvLower);
        return constraints.apply(lowerValue * (1.0 - weight) + upperValue * weight);
    } else if (cellUpperY == -1 || cellLowerY == cellUpperY) {
        Q_ASSERT(cellLowerY != -1);

        double lowerValue = cellValue((size_t) cellLowerY * N_CELLS + (size_t) cellLowerX, a, b);
        double upperValue = cellValue((size_t) cellLowerY * N_CELLS + (size_t) cellUpperX, a, b);

        double xvLower = ((double) cellLowerX / cellXScale) + minX;
        double xvUpper = ((double) cellUpperX / cellXScale) + minX;
        double weight = (a - xvLower) / (xvUpper - xvLower);
        return constraints.apply(lowerValue * (1.0 - weight) + upperValue * weight);
    } else {
        double ll = cellValue((size_t) cellLowerY * N_CELLS + (size_t) cellLowerX, a, b);
        double lu = cellValue((size_t) cellUpperY * N_CELLS + (size_t) cellLowerX, a, b);
        double ul = cellValue((size_t) cellLowerY * N_CELLS + (size_t) cellUpperX, a, b);
        double uu = cellValue((size_t) cellUpperY * N_CELLS + (size_t) cellUpperX, a, b);

        double xvLower = ((double) cellLowerX / cellXScale) + minX;
        double xvUpper = ((double) cellUpperX / cellXScale) + minX;
        double yvLower = ((double) cellLowerY / cellYScale) + minY;
        double yvUpper = ((double) cellUpperY / cellYScale) + minY;
        double scale = (xvUpper - xvLower) * (yvUpper - yvLower);
        return constraints.apply(((ll * (xvUpper - a) * (yvUpper - b)) / scale) +
                                        ((ul * (a - xvLower) * (yvUpper - b)) / scale) +
                                        ((lu * (xvUpper - a) * (b - yvLower)) / scale) +
                                        ((uu * (a - xvLower) * (b - yvLower)) / scale));
    }
}

double GaussianDensity2D::twoToOne(double a, double b)
{ return twoToOneConst(a, b); }

QVector<double> GaussianDensity2D::applyConst(const QVector<double> &inputs) const
{
    QVector<double> result;
    if (inputs.size() < 2)
        return result;
    result.append(twoToOneConst(inputs.at(0), inputs.at(1)));
    return result;
}

QVector<double> GaussianDensity2D::apply(const QVector<double> &inputs)
{
    QVector<double> result;
    if (inputs.size() < 2)
        return result;
    result.append(twoToOne(inputs.at(0), inputs.at(1)));
    return result;
}

void GaussianDensity2D::applyIOConst(QVector<double> &values) const
{
    if (values.size() < 2) {
        values.clear();
        return;
    }
    double v = twoToOneConst(values.at(0), values.at(1));
    values.clear();
    values.append(v);
}

void GaussianDensity2D::applyIO(QVector<double> &values)
{
    if (values.size() < 2) {
        values.clear();
        return;
    }
    double v = twoToOne(values.at(0), values.at(1));
    values.clear();
    values.append(v);
}

double GaussianDensity2D::threeToOneConst(double a, double b, double c) const
{
    Q_UNUSED(c);
    return twoToOneConst(a, b);
}

double GaussianDensity2D::threeToOne(double a, double b, double c)
{
    Q_UNUSED(c);
    return twoToOne(a, b);
}

void GaussianDensity2D::applyIOConst(double &a, double &b) const
{ a = b = twoToOneConst(a, b); }

void GaussianDensity2D::applyIO(double &a, double &b)
{ a = b = twoToOne(a, b); }

void GaussianDensity2D::applyIOConst(double &a, double &b, double &c) const
{
    a = b = c = twoToOneConst(a, b);
}

void GaussianDensity2D::applyIO(double &a, double &b, double &c)
{
    a = b = c = twoToOneConst(a, b);
}

GaussianDensity2D::GaussianDensity2D(const GaussianDensity2D &other)
        : Model(), constraints(other.constraints),
          minX(other.minX),
          maxX(other.maxX),
          minY(other.minY),
          maxY(other.maxY),
          scaleX(other.scaleX),
          scaleY(other.scaleY),
          cellXScale(other.cellXScale),
          cellYScale(other.cellYScale),
          matrix(other.matrix),
          xPoints(other.xPoints),
          yPoints(other.yPoints),
          matrixPoints(other.matrixPoints)
{ }

Model *GaussianDensity2D::clone() const
{ return new GaussianDensity2D(*this); }

QString GaussianDensity2D::describe() const
{ return "GaussianDensityField"; }

ModelLegendEntry GaussianDensity2D::legend(const ModelLegendParameters &parameters) const
{
    Q_UNUSED(parameters);
    return ModelLegendEntry();
}

GaussianDensity2D::GaussianDensity2D(QDataStream &stream)
{
    stream >> minX >> maxX >> minY >> maxY >> scaleX >> scaleY >> cellXScale >> cellYScale;

    matrix.reserve(N_CELLS * N_CELLS);
    matrixPoints.reserve(N_CELLS * N_CELLS * N_CELL_POINTS);
    for (size_t i = 0; i < N_CELLS * N_CELLS; i++) {
        double v;
        stream >> v;
        matrix.push_back(v);
        for (size_t j = 0; j < N_CELL_POINTS; j++) {
            qint32 i32;
            stream >> i32;
            matrixPoints.push_back((int) i32);
        }
    }

    quint32 n;
    stream >> n;
    xPoints.reserve(n);
    yPoints.reserve(n);
    for (size_t i = 0; i < n; i++) {
        double v;
        stream >> v;
        xPoints.push_back(v);
        stream >> v;
        yPoints.push_back(v);
    }
}

void GaussianDensity2D::serialize(QDataStream &stream) const
{
    quint8 t = (quint8) Model::SerialType_GuassianDensity2D;
    stream << t << minX << maxX << minY << maxY << scaleX << scaleY << cellXScale << cellYScale;
    for (size_t i = 0; i < N_CELLS * N_CELLS; i++) {
        stream << matrix[i];
        for (size_t j = i * N_CELL_POINTS, end = (i + 1) * N_CELL_POINTS; j < end; j++) {
            stream << (quint32) (matrixPoints[j]);
        }
    }
    stream << (quint32) xPoints.size();
    for (quint32 i = 0, max = (quint32) xPoints.size(); i < max; i++) {
        stream << xPoints[i] << yPoints[i];
    }
}

void GaussianDensity2D::printLog(QDebug &stream) const
{ stream << "GaussianDensity2D"; }


}
}
