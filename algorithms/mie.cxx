/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <vector>
#include <algorithm>
#include <QRunnable>

#include "algorithms/mie.hxx"
#include "core/number.hxx"

/* Note, have to include this later, or it causes definition conflicts with
 * definitions */
#include <complex>

#ifndef M_PI
#define M_PI    3.14159265358979
#endif

namespace CPD3 {
namespace Algorithms {

/** @file algorithms/mie.hxx
 * Mie scatter/absorption efficiency calculations.
 * <br>
 * @see http://atol.ucsd.edu/scatlib/scatterlib.htm
 */

class Mie::Implementation {
    std::vector<double> amu;
    int nAng;
    std::complex<double> ri;
    std::vector<double> tau;
    std::vector<double> pi;
    std::vector<double> pi0;
    std::vector<double> pi1;
    std::vector<std::complex<double> > s1;
    std::vector<std::complex<double> > s2;
    std::vector<std::complex<double> > D;
public:
    Implementation(int snAng, const std::complex<double> &sri)
            : amu(),
              nAng(snAng),
              ri(sri),
              tau(snAng),
              pi(snAng),
              pi0(snAng),
              pi1(snAng),
              s1(snAng * 2 - 1),
              s2(snAng * 2 - 1)
    {
        Q_ASSERT(nAng >= 2);
        Q_ASSERT(FP::defined(std::real(ri)));
        Q_ASSERT(FP::defined(std::imag(ri)));

        amu.reserve(nAng);
        for (int i = 0; i < nAng; i++) {
            amu.push_back(cos((double) i * (M_PI / 2.0) / (double) (nAng - 1)));
        }
    }

    void setRI(const std::complex<double> &sri)
    {
        ri = sri;
    }

    void setAngles(int snAng)
    {
        nAng = snAng;
        amu.clear();
        amu.reserve(nAng);
        for (int i = 0; i < nAng; i++) {
            amu.push_back(cos((double) i * (M_PI / 2.0) / (double) (nAng - 1)));
        }
    }

    void calculate(double x,
                   double *outQsca,
                   double *outQbsc,
                   double *outQabs,
                   double *outQext,
                   double *outGsca)
    {
        Q_ASSERT(FP::defined(x) && x >= 0.0);

        std::complex<double> y = x * ri;
        int nstop = (int) (x + 4.0 * pow(x, 1 / 3.0) + 2.0);
        int nmx = qMax((int) abs(y), nstop) + 15;
        Q_ASSERT(nmx > 0);

        tau.assign(nAng, 0.0);
        pi.assign(nAng, 0.0);
        pi0.assign(nAng, 0.0);
        pi1.assign(nAng, 1.0);
        s1.assign(nAng * 2 - 1, std::complex<double>(0.0, 0.0));
        s2.assign(nAng * 2 - 1, std::complex<double>(0.0, 0.0));

        double psi0 = cos(x);
        double psi1 = sin(x);
        double chi0 = -psi1;
        double chi1 = psi0;
        std::complex<double> xi1(psi1, -chi1);
        double qsca = 0;
        double gsca = 0;
        double p = -1;

        double fn = 0;
        double psi = 0;
        double chi = 0;
        std::complex<double> xi(0, 0);
        std::complex<double> an(0, 0);
        std::complex<double> bn(0, 0);
        std::complex<double> an1(0, 0);
        std::complex<double> bn1(0, 0);
        double anA = 0;
        double bnA = 0;
        int j, k, en, en21, en2m1;

        D.resize(nmx);
        D[nmx - 1] = std::complex<double>(0.0, 0.0);
        for (int en = nmx; en > 1; en--) {
            an = (double) en / y;
            D[en - 2] = an - (1.0 / (D[en - 1] + an));
        }
        for (int n = 0; n < nstop; n++) {
            en = n + 1;
            en21 = 2 * en + 1;
            en2m1 = 2 * en - 1;

            fn = (double) en21 / (double) (en * (en + 1));
            psi = (double) en2m1 * psi1 / x - psi0;
            chi = (double) en2m1 * chi1 / x - chi0;
            xi = std::complex<double>(psi, -chi);

            if (n != 0) {
                an1 = an;
                bn1 = bn;
            }
            an = (D[n] / ri + (double) en / x) * psi - psi1;
            an /= (D[n] / ri + (double) en / x) * xi - xi1;
            bn = (ri * D[n] + (double) en / x) * psi - psi1;
            bn /= (ri * D[n] + (double) en / x) * xi - xi1;

            anA = abs(an);
            bnA = abs(bn);

            qsca += (double) en21 * (anA * anA + bnA * bnA);
            gsca += fn * (std::real(an) * std::real(bn) + std::imag(an) * std::imag(bn));

            if (n > 0) {
                gsca += ((double) ((en - 1) * (en + 1)) / (double) en) *
                        (std::real(an1) * std::real(an) +
                                std::imag(an1) * std::imag(an) +
                                std::real(bn1) * std::real(bn) +
                                std::imag(bn1) * std::imag(bn));
            }

            for (j = 0; j < nAng; j++) {
                pi[j] = pi1[j];
                tau[j] = (double) en * amu[j] * pi[j] - (double) (en + 1) * pi0[j];
                s1[j] += fn * (an * pi[j] + bn * tau[j]);
                s2[j] += fn * (an * tau[j] + bn * pi[j]);
            }

            p = -p;

            for (j = 0, k = nAng * 2 - 2; j < nAng - 1; j++, k--) {
                s1[k] += fn * p * (an * pi[j] - bn * tau[j]);
                s2[k] += fn * p * (bn * pi[j] - an * tau[j]);
            }

            psi0 = psi1;
            psi1 = psi;
            chi0 = chi1;
            chi1 = chi;
            xi1 = xi;

            for (j = 0; j < nAng; j++) {
                pi1[j] =
                        ((double) en21 * amu[j] * pi[j] - (double) (en + 1) * pi0[j]) / (double) en;
                pi0[j] = pi[j];
            }
        }

        if (outGsca != NULL)
            *outGsca = 2.0 * gsca / qsca;
        if (outQsca != NULL || outQabs != NULL) {
            qsca = (2.0 / (x * x)) * qsca;
            if (outQsca != NULL)
                *outQsca = qsca;
        }
        double qext = 0;
        if (outQext != NULL || outQabs != NULL) {
            qext = (4.0 / (x * x)) * std::real(s1[0]);
            if (outQext != NULL)
                *outQext = qext;
        }
        if (outQbsc != NULL) {
            double qbsc = abs(s1[2 * nAng - 2]);
            *outQbsc = (4.0 / (x * x)) * qbsc * qbsc;
        }
        if (outQabs != NULL) {
            *outQabs = qext - qsca;
        }
    }

    inline int getAngles() const
    { return nAng; }

    inline std::complex<double> getRI() const
    { return ri; }
};

Mie::Mie(double riR, double riI, int nAng, Type)
{
    p.reset(new Implementation(nAng, std::complex<double>(riR, riI)));
}

Mie::~Mie() = default;

Mie *Mie::clone() const
{
    std::complex<double> cri(p->getRI());
    return new Mie(std::real(cri), std::imag(cri), p->getAngles());
}

Mie::Mie(QDataStream &stream)
{
    quint32 nAng;
    stream >> nAng;
    double riR;
    stream >> riR;
    double riI;
    stream >> riI;
    p.reset(new Implementation((int) nAng, std::complex<double>(riR, riI)));
}

void Mie::serialize(QDataStream &stream) const
{
    stream << (quint32) p->getAngles();
    std::complex<double> cri(p->getRI());
    stream << std::real(cri) << std::imag(cri);
}

void Mie::setRI(double riR, double riI)
{ p->setRI(std::complex<double>(riR, riI)); }

void Mie::setAngles(int nAng)
{ p->setAngles(nAng); }

void Mie::calculate(double x, double *qsca, double *qbsc, double *qabs, double *qext, double *gsca)
{ p->calculate(x, qsca, qbsc, qabs, qext, gsca); }

MieTable::MieTable(Mie *m, bool generateInitial) : mie(m),
                                                   table(),
                                                   first(generateInitial),
                                                   deferred(),
                                                   deferredCalculatorPool(),
                                                   lastManualDeferredX(FP::undefined()),
                                                   mutex(),
                                                   internalWait()
{
    Q_ASSERT(mie);
    deferredCalculatorPool.emplace_back(mie->clone());
}

MieTable::~MieTable()
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        while (!deferred.empty()) {
            internalWait.wait(lock);
        }
    }
    pool.wait();
}

MieTable::MieTable(QDataStream &stream) : mie(new Mie(stream)),
                                          table(),
                                          first(false),
                                          deferred(),
                                          lastManualDeferredX(FP::undefined()),
                                          mutex(),
                                          internalWait()
{
    deferredCalculatorPool.emplace_back(mie->clone());

    stream >> first;
    quint32 n;
    stream >> n;
    table.reserve(n);
    for (quint32 i = 0; i < n; i++) {
        TableEntry add;
        stream >> add.x >> add.qsca >> add.qbsc >> add.qabs >> add.qext >> add.gsca;
        table.push_back(add);
    }
}

void MieTable::serialize(QDataStream &stream) const
{
    mie->serialize(stream);
    stream << first;
    stream << (quint32) table.size();
    for (std::vector<TableEntry>::const_iterator it = table.begin(), end = table.end();
            it != end;
            ++it) {
        stream << it->x << it->qsca << it->qbsc << it->qabs << it->qext << it->gsca;
    }
}

static const double zeroEpsilon = 1E-10;

int MieTable::lookup(double x,
                     std::vector<TableEntry>::iterator &lower,
                     std::vector<TableEntry>::iterator &upper)
{
    Q_ASSERT(FP::defined(x) && x >= 0.0);

    double epsilon = x * zeroEpsilon;

    lower = std::lower_bound(table.begin(), table.end(), x, TableSort());
    upper = table.end();
    if (lower != table.end() && fabs(lower->x - x) <= epsilon) {
        return 0;
    }
    if (lower != table.begin()) {
        if (fabs((lower - 1)->x - x) <= epsilon) {
            --lower;
            return 0;
        }
    } else if (lower != table.end() &&
            lower + 1 != table.end() &&
            fabs((lower + 1)->x - x) <= epsilon) {
        ++lower;
        return 0;
    }

    if (first || table.size() < 2) {
        return -1;
    }

    std::vector<TableEntry>::iterator originalTarget = lower;

    if (lower == table.end()) {
        lower--;
    }

    if (lower != table.begin()) {
        upper = lower;
        --lower;
    } else {
        upper = lower + 1;
    }

    double maximumDelta = x * 0.1;
    if (fabs(x - lower->x) > maximumDelta && fabs(upper->x - x) > maximumDelta) {
        lower = originalTarget;
        return -1;
    }

    return 1;
}

void MieTable::interpolate(double x,
                           std::vector<TableEntry>::const_iterator lower,
                           std::vector<TableEntry>::const_iterator upper,
                           double *qsca,
                           double *qbsc,
                           double *qabs,
                           double *qext,
                           double *gsca)
{
    double scalar = ((x - lower->x) / (upper->x - lower->x));

    if (qsca != NULL) {
        if (FP::defined(lower->qsca) && FP::defined(upper->qsca))
            *qsca = lower->qsca + scalar * (upper->qsca - lower->qsca);
        else
            *qsca = FP::undefined();
    }

    if (qbsc != NULL) {
        if (FP::defined(lower->qbsc) && FP::defined(upper->qbsc))
            *qbsc = lower->qbsc + scalar * (upper->qbsc - lower->qbsc);
        else
            *qbsc = FP::undefined();
    }

    if (qabs != NULL) {
        if (FP::defined(lower->qabs) && FP::defined(upper->qabs))
            *qabs = lower->qabs + scalar * (upper->qabs - lower->qabs);
        else
            *qabs = FP::undefined();
    }

    if (qext != NULL) {
        if (FP::defined(lower->qext) && FP::defined(upper->qext))
            *qext = lower->qext + scalar * (upper->qext - lower->qext);
        else
            *qext = FP::undefined();
    }

    if (gsca != NULL) {
        if (FP::defined(lower->gsca) && FP::defined(upper->gsca))
            *gsca = lower->gsca + scalar * (upper->gsca - lower->gsca);
        else
            *gsca = FP::undefined();
    }
}

void MieTable::calculate(double x,
                         double *qsca,
                         double *qbsc,
                         double *qabs,
                         double *qext,
                         double *gsca)
{
    Q_ASSERT(FP::defined(x) && x >= 0.0);

    std::vector<TableEntry>::iterator lower;
    std::vector<TableEntry>::iterator upper;
    int rc = lookup(x, lower, upper);
    if (rc == -1) {
        TableEntry add;
        add.x = x;
        mie->calculate(x, &add.qsca, &add.qbsc, &add.qabs, &add.qext, &add.gsca);
        lower = table.insert(lower, add);
    }
    if (rc != 1) {
        if (qsca != NULL)
            *qsca = lower->qsca;
        if (qbsc != NULL)
            *qbsc = lower->qbsc;
        if (qabs != NULL)
            *qabs = lower->qabs;
        if (qext != NULL)
            *qext = lower->qext;
        if (gsca != NULL)
            *gsca = lower->gsca;
        return;
    }

    interpolate(x, lower, upper, qsca, qbsc, qabs, qext, gsca);
}

MieTable::CalculateHandler::~CalculateHandler()
{ }


void MieTable::runDeferred(double x)
{
    double qsca = 0;
    double qbsc = 0;
    double qabs = 0;
    double qext = 0;
    double gsca = 0;

    std::unique_ptr<Mie> mie;
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (!deferredCalculatorPool.empty()) {
            mie = std::move(deferredCalculatorPool.back());
            deferredCalculatorPool.pop_back();
        } else {
            mie.reset(this->mie->clone());
        }
    }

    mie->calculate(x, &qsca, &qbsc, &qabs, &qext, &gsca);

    double epsilon = x * zeroEpsilon;

    std::lock_guard<std::mutex> lock(mutex);

    std::vector<MieTable::DeferredLookup>::iterator
            source = std::lower_bound(deferred.begin(), deferred.end(), x, TableSort());
    if (source == deferred.end()) {
        Q_ASSERT(source != deferred.begin());
        --source;
    } else if (fabs(x - source->x) > epsilon) {
        if ((source + 1) != deferred.end() && fabs(x - (source + 1)->x) <= epsilon) {
            ++source;
        } else {
            Q_ASSERT(source != deferred.begin());
            Q_ASSERT(fabs(x - (source - 1)->x) <= epsilon);
            --source;
        }
    }
    Q_ASSERT(fabs(x - source->x) <= epsilon);

    std::vector<MieTable::TableEntry>::iterator
            target = std::lower_bound(table.begin(), table.end(), x, TableSort());
    if (target == table.end()) {
        if (target != table.begin()) {
            if (fabs(x - (target - 1)->x) <= epsilon) {
                --target;
            } else {
                target = table.insert(target, MieTable::TableEntry());
            }
        } else {
            target = table.insert(target, MieTable::TableEntry());
        }
    } else if (fabs(x - target->x) > epsilon) {
        if ((target + 1) != table.end() && fabs(x - (target + 1)->x) <= epsilon) {
            ++target;
        } else {
            if (target != table.begin()) {
                if (fabs(x - (target - 1)->x) <= epsilon) {
                    --target;
                } else {
                    target = table.insert(target, MieTable::TableEntry());
                }
            } else {
                target = table.insert(target, MieTable::TableEntry());
            }
        }
    }

    target->x = x;
    target->qsca = qsca;
    target->qbsc = qbsc;
    target->qabs = qabs;
    target->qext = qext;
    target->gsca = gsca;

    for (std::vector<MieTable::CalculateHandler *>::iterator cb = source->handlers.begin(),
            endCB = source->handlers.end(); cb != endCB; ++cb) {
        Q_ASSERT((*cb) != NULL);
        (*cb)->mieResult(qsca, qbsc, qabs, qext, gsca);
        delete *cb;
    }

    deferred.erase(source);
    deferredCalculatorPool.emplace_back(std::move(mie));
    internalWait.notify_all();
}

void MieTable::startDeferred(double x, CalculateHandler *handler)
{
    Q_ASSERT(FP::defined(x) && x >= 0.0);
    Q_ASSERT(!mutex.try_lock());

    double epsilon = x * zeroEpsilon;
    std::vector<DeferredLookup>::iterator
            lower = std::lower_bound(deferred.begin(), deferred.end(), x, TableSort());
    if (lower != deferred.begin()) {
        if (fabs((lower - 1)->x - x) <= epsilon) {
            if (handler != NULL)
                (lower - 1)->handlers.push_back(handler);
            return;
        }
        if (lower != deferred.end()) {
            if (fabs(lower->x - x) <= epsilon) {
                if (handler != NULL)
                    lower->handlers.push_back(handler);
                return;
            }
            if ((lower + 1) != deferred.end() && fabs((lower + 1)->x - x) <= epsilon) {
                if (handler != NULL)
                    (lower + 1)->handlers.push_back(handler);
                return;
            }
        }
    } else if (lower != deferred.end()) {
        if (fabs(lower->x - x) <= epsilon) {
            if (handler != NULL)
                lower->handlers.push_back(handler);
            return;
        }
        if ((lower + 1) != deferred.end() && fabs((lower + 1)->x - x) <= epsilon) {
            if (handler != NULL)
                (lower + 1)->handlers.push_back(handler);
            return;
        }
    }

    DeferredLookup add;
    add.x = x;
    lower = deferred.insert(lower, add);
    if (handler)
        lower->handlers.push_back(handler);
    pool.run(std::bind(&MieTable::runDeferred, this, x));
}

static const int subdivideTable = 4;

void MieTable::scanDone()
{
    std::unique_lock<std::mutex> lock(mutex);
    while (!deferred.empty()) {
        internalWait.wait(lock);
    }
    if (!first || table.size() < 2)
        return;
    first = false;

    double startX = table.begin()->x;
    double startDeltaX = ((table.begin() + 1)->x - startX) / (double) (subdivideTable + 1);
    double endX = (table.end() - 1)->x;
    double endDeltaX = (endX + (table.end() - 2)->x) / (double) (subdivideTable + 1);

    table.reserve(table.size() * (subdivideTable + 1) + subdivideTable * 2);
    deferred.reserve((table.size() + 2) * subdivideTable);

    for (int i = subdivideTable; i >= 1; i--) {
        startDeferred(startX - startDeltaX * (double) i, NULL);
    }

    for (std::vector<TableEntry>::iterator add = table.begin(); add + 1 != table.end(); ++add) {
        double deltaX = ((add + 1)->x - add->x) / (double) (subdivideTable + 1);
        for (int i = 1; i <= subdivideTable; i++) {
            startDeferred(add->x + deltaX * (double) i, NULL);
        }
    }

    for (int i = 1; i <= subdivideTable; i++) {
        startDeferred(endX + endDeltaX * (double) i, NULL);
    }

    while (!deferred.empty()) {
        internalWait.wait(lock);
    }
}


void MieTable::deferredCalculate(double x, CalculateHandler *handler)
{
    std::lock_guard<std::mutex> lock(mutex);

    std::vector<TableEntry>::iterator lower;
    std::vector<TableEntry>::iterator upper;
    int rc = lookup(x, lower, upper);
    if (rc == -1) {
        startDeferred(x, handler);
        return;
    }
    if (handler == NULL)
        return;
    if (rc == 0) {
        handler->mieResult(lower->qsca, lower->qbsc, lower->qabs, lower->qext, lower->gsca);
        delete handler;
        return;
    }

    double qsca = 0;
    double qbsc = 0;
    double qabs = 0;
    double qext = 0;
    double gsca = 0;
    interpolate(x, lower, upper, &qsca, &qbsc, &qabs, &qext, &gsca);
    handler->mieResult(qsca, qbsc, qabs, qext, gsca);
    delete handler;
}

bool MieTable::beginManualDeferCalculate(double x,
                                         double *qsca,
                                         double *qbsc,
                                         double *qabs,
                                         double *qext,
                                         double *gsca)
{
    Q_ASSERT(FP::defined(x) && x >= 0.0);
    mutex.lock();
#ifndef NDEBUG
    Q_ASSERT(!FP::defined(lastManualDeferredX));
#endif

    std::vector<TableEntry>::iterator lower;
    std::vector<TableEntry>::iterator upper;
    int rc = lookup(x, lower, upper);
    if (rc == -1) {
        lastManualDeferredX = x;
        return true;
    }
    if (rc != 1) {
        if (qsca != NULL)
            *qsca = lower->qsca;
        if (qbsc != NULL)
            *qbsc = lower->qbsc;
        if (qabs != NULL)
            *qabs = lower->qabs;
        if (qext != NULL)
            *qext = lower->qext;
        if (gsca != NULL)
            *gsca = lower->gsca;
        return false;
    }

    interpolate(x, lower, upper, qsca, qbsc, qabs, qext, gsca);
    return false;
}

void MieTable::issueManualDeferCalculate(CalculateHandler *handler)
{
    Q_ASSERT(!mutex.try_lock());
    Q_ASSERT(FP::defined(lastManualDeferredX));
    startDeferred(lastManualDeferredX, handler);
#ifndef NDEBUG
    lastManualDeferredX = FP::undefined();
#endif
}

void MieTable::endManualDeferCalculate()
{
    Q_ASSERT(!mutex.try_lock());
    mutex.unlock();
}

}
}
