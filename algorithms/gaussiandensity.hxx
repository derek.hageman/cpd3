/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3ALGORITHMSGAUSSIANDENSITY_H
#define CPD3ALGORITHMSGAUSSIANDENSITY_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QList>
#include <QDebug>

#include "algorithms/algorithms.hxx"

#include "algorithms/model.hxx"


namespace CPD3 {
namespace Algorithms {

struct GaussianDensity2DNode;

/**
 * A density field generator that uses Gaussian kernels, with the given
 * number of input dimensions.
 */
class CPD3ALGORITHMS_EXPORT GaussianDensity2D : public Model {
    ModelOutputConstraints constraints;
    double minX;
    double maxX;
    double minY;
    double maxY;
    double scaleX;
    double scaleY;
    double cellXScale;
    double cellYScale;
    double maximumResult;
    std::vector<double> matrix;
    std::vector<double> xPoints;
    std::vector<double> yPoints;
    std::vector<int> matrixPoints;

    double cellValue(size_t cell, double x, double y) const;

public:
    virtual ~GaussianDensity2D();

    GaussianDensity2D(const GaussianDensity2D &other);

    GaussianDensity2D(const QVector<double> &x,
                      const QVector<double> &y,
                      double kernelDensity = 100.0,
                      double weightLimit = FP::undefined(),
                      const ModelOutputConstraints &output = ModelOutputConstraints());

    virtual QVector<double> applyConst(const QVector<double> &inputs) const;

    virtual QVector<double> apply(const QVector<double> &inputs);

    virtual void applyIOConst(QVector<double> &values) const;

    virtual void applyIO(QVector<double> &values);

    virtual double twoToOneConst(double a, double b) const;

    virtual double twoToOne(double a, double b);

    virtual double threeToOneConst(double a, double b, double c) const;

    virtual double threeToOne(double a, double b, double c);

    virtual void applyIOConst(double &a, double &b) const;

    virtual void applyIO(double &a, double &b);

    virtual void applyIOConst(double &a, double &b, double &c) const;

    virtual void applyIO(double &a, double &b, double &c);

    virtual Model *clone() const;

    virtual QString describe() const;

    virtual ModelLegendEntry
            legend(const ModelLegendParameters &parameters = ModelLegendParameters()) const;

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator<<(QDataStream &stream, const Model *model);

    friend CPD3ALGORITHMS_EXPORT QDataStream &operator>>(QDataStream &stream, Model *&model);

protected:
    GaussianDensity2D(QDataStream &stream);

    virtual void serialize(QDataStream &stream) const;

    virtual void printLog(QDebug &stream) const;
};

}
}

#endif
