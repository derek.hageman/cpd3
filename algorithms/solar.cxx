/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <cmath>

#include "solar.hxx"
#include "core/number.hxx"
#include "core/timeutils.hxx"

#ifndef M_PI
#define M_PI    3.14159265358979
#endif

namespace CPD3 {
namespace Algorithms {

/*
 * https://www.esrl.noaa.gov/gmd/grad/solcalc/
 */

static double toRadians(double degrees)
{ return degrees * (M_PI / 180.0); }

static double toDegrees(double radians)
{ return radians * (180.0 / M_PI); }

static double toJulianCentury(double julianDay)
{ return (julianDay - 2451545.0) / 36525.0; }


static double calculateGeomMeanLongSun(double julianCentury)
{
    double L0 = 280.46646 + julianCentury * (36000.76983 + julianCentury * (0.0003032));
    L0 -= std::floor(L0 / 360.0) * 360.0;
    return toRadians(L0);
}

static double calculateGeomMeanAnomalySun(double julianCentury)
{
    double M = 357.52911 + julianCentury * (35999.05029 - 0.0001537 * julianCentury);
    return toRadians(M);
}

static double calculateEccentricityEarthOrbit(double julianCentury)
{
    return 0.016708634 - julianCentury * (0.000042037 + 0.0000001267 * julianCentury);
}

static double calculateSunEqOfCenter(double julianCentury, double meanAnomalySun)
{
    double sinm = std::sin(meanAnomalySun);
    double sin2m = std::sin(meanAnomalySun * 2.0);
    double sin3m = std::sin(meanAnomalySun * 3.0);
    double C = sinm * (1.914602 - julianCentury * (0.004817 + 0.000014 * julianCentury)) +
            sin2m * (0.019993 - 0.000101 * julianCentury) +
            sin3m * 0.000289;
    return toRadians(C);
}

static double calculateSunTrueLong(double geomMeanLongSun, double sunEqOfCenter)
{
    return geomMeanLongSun + sunEqOfCenter;
}

/*
static double calculateSunTrueAnomaly(double geomMeanLongSun, double sunEqOfCenter)
{
    return geomMeanLongSun + sunEqOfCenter;
}

static double calculateSunRadVector(double sunTrueAnomaly, double eccentricityEarthOrbit)
{
    return (1.000001018 * (1 - eccentricityEarthOrbit * eccentricityEarthOrbit)) /
            (1 + eccentricityEarthOrbit * std::cos(sunTrueAnomaly));
}
*/

static double calculateSunApparentLong(double julianCentury, double sunTrueLong)
{
    double o = toDegrees(sunTrueLong);
    double omega = 125.04 - 1934.136 * julianCentury;
    double lambda = o - 0.00569 - 0.00478 * std::sin(toRadians(omega));
    return toRadians(lambda);
}

/*static double calculateMeanObliquityOfEcliptic(double julianCentury)
{
    double seconds = 21.448 - julianCentury * (46.8150 + julianCentury * (0.00059 - julianCentury * (0.001813)));
    double e0 = 23.0 + (26.0 + (seconds / 60.0)) / 60.0;
    return toRadians(e0);
}*/

static double calculateObliquityCorrection(double julianCentury)
{
    double seconds = 21.448 -
            julianCentury * (46.8150 + julianCentury * (0.00059 - julianCentury * (0.001813)));
    double e0 = 23.0 + (26.0 + (seconds / 60.0)) / 60.0;

    double omega = 125.04 - 1934.136 * julianCentury;
    double e = e0 + 0.00256 * std::cos(toRadians(omega));
    return toRadians(e);
}

/*
static double calcSunRtAscension(double obliquityCorrection, double sunApparentLong)
{
    double tananum = (std::cos(obliquityCorrection) * std::sin(sunApparentLong));
    double tanadenom = (std::cos(sunApparentLong));
    return std::atan2(tananum, tanadenom);
}
 */

static double calculateSunDeclination(double obliquityCorrection, double sunApparentLong)
{
    double sint = std::sin(obliquityCorrection) * std::sin(sunApparentLong);
    return std::asin(sint);
}

static double calculateEquationOfTime(double eccentricityEarthOrbit,
                                      double obliquityCorrection,
                                      double geomMeanLongSun,
                                      double geomMeanAnomalySun)
{
    double y = std::tan(obliquityCorrection / 2.0);
    y *= y;

    double sin2l0 = std::sin(2.0 * geomMeanLongSun);
    double sinm = std::sin(geomMeanAnomalySun);
    double cos2l0 = std::cos(2.0 * geomMeanLongSun);
    double sin4l0 = std::sin(4.0 * geomMeanLongSun);
    double sin2m = std::sin(2.0 * geomMeanAnomalySun);

    double Etime = y * sin2l0 -
            2.0 * eccentricityEarthOrbit * sinm +
            4.0 * eccentricityEarthOrbit * y * sinm * cos2l0 -
            0.5 * y * y * sin4l0 -
            1.25 * eccentricityEarthOrbit * eccentricityEarthOrbit * sin2m;
    return toDegrees(Etime) * 4.0;
}

static double calculateEquationOfTime(double julianCentury)
{
    return calculateEquationOfTime(calculateEccentricityEarthOrbit(julianCentury),
                                   calculateObliquityCorrection(julianCentury),
                                   calculateGeomMeanLongSun(julianCentury),
                                   calculateGeomMeanAnomalySun(julianCentury));
}

static double calculateHourAngleSunrise(double latitudeRadians, double sunDeclination)
{
    double HAarg =
            (std::cos(toRadians(90.833)) / (std::cos(latitudeRadians) * std::cos(sunDeclination)) -
                    std::tan(latitudeRadians) * std::tan(sunDeclination));
    if (HAarg < -1.0 || HAarg > 1.0)
        return FP::undefined();
    return std::acos(HAarg);
}

static double calculateSolarTime(double minutes, double equationOfTime, double longitude)
{
    double solarTimeFix = equationOfTime + 4.0 * longitude;
    double trueSolarTime = minutes + solarTimeFix;
    trueSolarTime -= std::floor(minutes / 1440.0) * 1440.0;
    return trueSolarTime;
}

static std::pair<double, double> calculateAzimuthElevation(double sunDeclination,
                                                           double trueSolarTime,
                                                           double latitudeRadians)
{
    double hourAngle = trueSolarTime / 4.0 - 180.0;
    if (hourAngle < -180.0)
        hourAngle += 360.0;
    double haRad = toRadians(hourAngle);
    double csz = std::sin(latitudeRadians) * std::sin(sunDeclination) +
            std::cos(latitudeRadians) * std::cos(sunDeclination) * std::cos(haRad);
    if (csz > 1.0)
        csz = 1.0;
    else if (csz < -1.0)
        csz = -1.0;
    double zenith = std::acos(csz);
    double azDenom = std::cos(latitudeRadians) * std::sin(zenith);
    double azimuth;
    if (std::fabs(azDenom) > 0.001) {
        double azRad = ((std::sin(latitudeRadians) * std::cos(zenith)) - std::sin(sunDeclination)) /
                azDenom;
        if (azRad > 1.0)
            azRad = 1.0;
        else if (azRad < -1.0)
            azRad = -1.0;
        azimuth = M_PI - std::acos(azRad);
        if (hourAngle > 0.0)
            azimuth = -azimuth;
    } else {
        if (latitudeRadians > 0.0) {
            azimuth = M_PI;
        } else {
            azimuth = 0.0;
        }
    }
    if (azimuth < 0.0)
        azimuth += M_PI * 2;

    azimuth = toDegrees(azimuth);
    zenith = toDegrees(zenith);

    double exoatmElevation = 90.0 - zenith;

    // Atmospheric Refraction correction
    double refractionCorrection;
    if (exoatmElevation > 85.0) {
        refractionCorrection = 0.0;
    } else {
        double te = std::tan(toRadians(exoatmElevation));
        if (exoatmElevation > 5.0) {
            refractionCorrection =
                    58.1 / te - 0.07 / (te * te * te) + 0.000086 / (te * te * te * te * te);
        } else if (exoatmElevation > -0.575) {
            refractionCorrection = 1735.0 +
                    exoatmElevation *
                            (-518.2 +
                                    exoatmElevation *
                                            (103.4 +
                                                    exoatmElevation *
                                                            (-12.79 + exoatmElevation * 0.711)));
        } else {
            refractionCorrection = -20.774 / te;
        }
        refractionCorrection = refractionCorrection / 3600.0;
    }

    double solarZenith = zenith - refractionCorrection;

    return std::pair<double, double>(azimuth, 90.0 - solarZenith);
}

static double calculateSolarNoon(double julianDay, double longitude)
{
    double tnoon = toJulianCentury(julianDay - longitude / 360.0);
    double eqTime = calculateEquationOfTime(tnoon);
    double solNoonOffset = 720.0 - (longitude * 4) - eqTime;
    double newt = toJulianCentury(julianDay + solNoonOffset / 1440.0);
    eqTime = calculateEquationOfTime(newt);
    double solNoon = 720 - (longitude * 4) - eqTime;
    solNoon -= std::floor(solNoon / 1440.0) * 1440.0;
    return solNoon;
}

static double calculateSunRiseSetTime(double equationOfTime, double hourAngle, double longitude)
{
    double delta = longitude + toDegrees(hourAngle);
    return 720.0 - (4.0 * delta) - equationOfTime;
}

Solar::Position::Position(const Solar &solar)
{
    if (!FP::defined(solar.time) ||
            !FP::defined(solar.longitude) ||
            !FP::defined(solar.longitude)) {
        azimuthAngle = FP::undefined();
        elevationAngle = FP::undefined();
        return;
    }

    double julianDay = solar.julianDay;
    double secondsAfterMidnight = solar.time - std::floor(solar.time / 86400.0) * 86400.0;
    julianDay += secondsAfterMidnight / 86400.0;
    double julianCentury = toJulianCentury(julianDay);

    double geomMeanLongSun = calculateGeomMeanLongSun(julianCentury);
    double geomMeanAnomalySun = calculateGeomMeanAnomalySun(julianCentury);
    double sunEqOfCenter = calculateSunEqOfCenter(julianCentury, geomMeanAnomalySun);
    double sunTrueLong = calculateSunTrueLong(geomMeanLongSun, sunEqOfCenter);
    double sunApparentLong = calculateSunApparentLong(julianCentury, sunTrueLong);
    double obliquityCorrection = calculateObliquityCorrection(julianCentury);
    double sunDeclination = calculateSunDeclination(obliquityCorrection, sunApparentLong);

    double eccentricityEarthOrbit = calculateEccentricityEarthOrbit(julianCentury);
    double equationOfTime =
            calculateEquationOfTime(eccentricityEarthOrbit, obliquityCorrection, geomMeanLongSun,
                                    geomMeanAnomalySun);

    double solarTime =
            calculateSolarTime(secondsAfterMidnight / 60.0, equationOfTime, solar.longitude);

    std::pair<double, double>
            pos = calculateAzimuthElevation(sunDeclination, solarTime, toRadians(solar.latitude));
    azimuthAngle = pos.first;
    elevationAngle = pos.second;
}

bool Solar::Position::isDark(double angle) const
{
    if (!FP::defined(elevationAngle))
        return false;
    if (!FP::defined(angle))
        return false;
    return elevationAngle < -angle;
}

Solar::Day::Day(const Solar &solar)
{
    if (!FP::defined(solar.time) || !FP::defined(solar.latitude) || !FP::defined(solar.longitude)) {
        julianDay = 0;
        longitude = FP::undefined();
        timeOffset = FP::undefined();
        equationOfTime = FP::undefined();
        hourAngleSunrise = FP::undefined();
        return;
    }
    longitude = solar.longitude;

    timeOffset = std::floor(solar.time / 86400.0) * 86400.0;

    julianDay = solar.julianDay;
    double julianCentury = toJulianCentury(julianDay);

    double geomMeanLongSun = calculateGeomMeanLongSun(julianCentury);
    double geomMeanAnomalySun = calculateGeomMeanAnomalySun(julianCentury);
    double sunEqOfCenter = calculateSunEqOfCenter(julianCentury, geomMeanAnomalySun);
    double sunTrueLong = calculateSunTrueLong(geomMeanLongSun, sunEqOfCenter);
    double sunApparentLong = calculateSunApparentLong(julianCentury, sunTrueLong);
    double obliquityCorrection = calculateObliquityCorrection(julianCentury);
    double sunDeclination = calculateSunDeclination(obliquityCorrection, sunApparentLong);

    double eccentricityEarthOrbit = calculateEccentricityEarthOrbit(julianCentury);
    equationOfTime =
            calculateEquationOfTime(eccentricityEarthOrbit, obliquityCorrection, geomMeanLongSun,
                                    geomMeanAnomalySun);

    hourAngleSunrise = calculateHourAngleSunrise(toRadians(solar.latitude), sunDeclination);
}

double Solar::Day::sunrise() const
{
    if (!FP::defined(hourAngleSunrise))
        return FP::undefined();
    double minutes = calculateSunRiseSetTime(equationOfTime, hourAngleSunrise, longitude);
    return minutes * 60.0 + timeOffset;
}

double Solar::Day::sunset() const
{
    if (!FP::defined(hourAngleSunrise))
        return FP::undefined();
    double minutes = calculateSunRiseSetTime(equationOfTime, -hourAngleSunrise, longitude);
    return minutes * 60.0 + timeOffset;
}

double Solar::Day::noon() const
{
    if (julianDay <= 0 || !FP::defined(longitude))
        return FP::undefined();
    double minutes = calculateSolarNoon(julianDay, longitude);
    return minutes * 60.0 + timeOffset;
}

double Solar::fromMinuteOfDay(double minutes) const
{
    return minutes * 60.0 + std::floor(time / 86400.0) * 86400.0;
}

double Solar::noon() const
{
    if (!FP::defined(time) || !FP::defined(longitude))
        return FP::undefined();
    return fromMinuteOfDay(calculateSolarNoon(julianDay, longitude));
}

Solar::Solar() : time(FP::undefined()),
                 latitude(FP::undefined()),
                 longitude(FP::undefined()),
                 julianDay(0)
{ }

Solar::Solar(double time, double latitude, double longitude) : time(time),
                                                               latitude(latitude),
                                                               longitude(longitude),
                                                               julianDay(0)
{
    if (FP::defined(time))
        julianDay = Time::julianDay(time) - 0.5;
}

void Solar::setLocation(double latitude, double longitude)
{
    this->latitude = latitude;
    this->longitude = longitude;
}

void Solar::setTime(double time)
{
    this->time = time;
    if (FP::defined(time))
        julianDay = Time::julianDay(time) - 0.5;
}

}
}