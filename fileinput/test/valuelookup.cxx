/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>

#include "fileinput/valuelookup.hxx"

using namespace CPD3;
using namespace CPD3::Input;
using namespace CPD3::Data;

Q_DECLARE_METATYPE(QList<ValueLookupPointer>);

class TestValueLookup : public QObject {
Q_OBJECT

private slots:

    void lookup()
    {
        QFETCH(Variant::Root, key);
        QFETCH(Variant::Root, convert);
        QFETCH(Variant::Root, expected);
        QFETCH(QList<ValueLookupPointer>, layers);

        ValueLookup *l = new ValueLookup(layers);
        l->context()->substitutionStack().push();
        l->context()->substitutionStack().setString("SUB", "SubVal");

        if (expected.read().exists()) {
            QCOMPARE(l->lookup(key, convert), expected.read());
        } else {
            QVERIFY(!l->lookup(key, convert).exists());
        }
        ValueLookup *c = l->clone();
        delete l;
        if (expected.read().exists()) {
            QCOMPARE(c->lookup(key, convert), expected.read());
        } else {
            QVERIFY(!c->lookup(key, convert).exists());
        }
        delete c;

        l = new ValueLookup;
        l->context()->substitutionStack().push();
        l->context()->substitutionStack().setString("SUB", "SubVal");
        for (QList<ValueLookupPointer>::const_iterator add = layers.constBegin(),
                end = layers.constEnd(); add != end; ++add) {
            l->add(*add);
        }
        if (expected.read().exists()) {
            QCOMPARE(l->lookup(key, convert), expected.read());
        } else {
            QVERIFY(!l->lookup(key, convert).exists());
        }
        delete l;
    }

    void lookup_data()
    {
        QTest::addColumn<Variant::Root>("key");
        QTest::addColumn<Variant::Root>("convert");
        QTest::addColumn<Variant::Root>("expected");
        QTest::addColumn<QList<ValueLookupPointer> >("layers");

        typedef Variant::Root V;
        typedef QList<ValueLookupPointer> LL;
        typedef ValueLookupPointer LP;
        typedef ValueLookupConstant L;

        V a;

        QTest::newRow("Empty") << V() << V() << V() << LL();

        QTest::newRow("Single") << V("KV") << V() << V("Val") << (LL() << LP(new L("KV", V("Val"))));
        QTest::newRow("Not found") << V("ZZZ") << V() << V() << (LL() << LP(new L("KV", V("Val"))));

        QTest::newRow("Overlay") <<
                V("KV") <<
                V() <<
                V("Val") <<
                (LL() <<
                        LP(new L("KV", V("In"))) <<
                        LP(new L("KV", V("Val"))) <<
                        LP(new L("Stuff", V("Out"))));

        a.write().hash("C").setDouble(3.0);
        QTest::newRow("Path") <<
                V("A/B/C") <<
                V() <<
                V(3.0) <<
                (LL() << LP(new L("A/B/C", V("1"))) << LP(new L("A/B/D", V("2"))) << LP(new L("A/B", a)));

        QTest::newRow("Override double") <<
                V("KV") <<
                V(2.0) <<
                V(2.0) <<
                (LL() << LP(new L("KV", V("Val"))));
        QTest::newRow("Override double not found") <<
                V("ZZZ") <<
                V(2.0) <<
                V(2.0) <<
                (LL() << LP(new L("KV", V("Val"))));
        QTest::newRow("Override integer") <<
                V("KV") <<
                V((qint64) 2) <<
                V((qint64) 2) <<
                (LL() << LP(new L("KV", V("Val"))));
        QTest::newRow("Override string") <<
                V("KV") <<
                V("Q") <<
                V("Q") <<
                (LL() << LP(new L("KV", V("Val"))));
        QTest::newRow("Override string sub") <<
                V("KV") <<
                V("Q_${SUB}") <<
                V("Q_SubVal") <<
                (LL() << LP(new L("KV", V("Val"))));

        a = V();
        a.write().applyFlag("Flag1");
        QTest::newRow("Override flags") <<
                V("KV") <<
                V(a) <<
                V(a) <<
                (LL() << LP(new L("KV", V("Val"))));

        a = V();
        a["Type"] = "Override";
        a["Value"] = "ABC";
        QTest::newRow("Override complex set") <<
                V("KV") <<
                a <<
                V("ABC") <<
                (LL() << LP(new L("KV", V("Val"))));

        a = V();
        a["Type"] = "String";
        a["Value"] = "${SUB}ABC";
        QTest::newRow("Override complex set") <<
                V("KV") <<
                a <<
                V("SubValABC") <<
                (LL() << LP(new L("KV", V("Val"))));

        a = V();
        a["Type"] = "Format";
        a["Format"] = "00.00";
        QTest::newRow("Override complex format") <<
                V("KV") <<
                a <<
                V("12.00") <<
                (LL() << LP(new L("KV", V(12.0))));

        a = V();
        a["Type"] = "Script";
        a["Code"] = "return '1234'";
        QTest::newRow("Script basic") <<
                V("KV") <<
                a <<
                V("1234") <<
                (LL() << LP(new L("KV", V(12.0))));
        QTest::newRow("Script undefined") <<
                V("KV2") <<
                a <<
                V() <<
                (LL() << LP(new L("KV", V(12.0))));

        a = V();
        a["Type"] = "Script";
        a["Code"] = "return VALUE.B.C";
        QTest::newRow("Script value lookup") <<
                V("A") <<
                a <<
                V("1") <<
                (LL() << LP(new L("A/B/C", V("1"))) << LP(new L("A/B/D", V("2"))));

        a = V();
        a["Type"] = "Script";
        a["Cnv"] = "qwerty";
        a["Code"] = "return CONVERTER.Cnv";
        QTest::newRow("Script converter lookup") <<
                V("A") <<
                a <<
                V("qwerty") <<
                (LL() << LP(new L("A/B/C", V("1"))) << LP(new L("A/B/D", V("2"))));

        QTest::newRow("Script string sub") <<
                                           V("KV") << V(
                "Q_${<a = VALUE.A+2; return 'stuff' .. tostring(a)>}") <<
                                           V("Q_stuff3") <<
                                           (LL() << LP(new L("KV/A", V(1))));
    }

    void lookupReference()
    {
        typedef Variant::Root V;
        typedef ValueLookupPointer LP;
        typedef ValueLookupConstant L;

        QList<ValueLookupPointer> layers;
        layers << LP(new L("K1", V("Val1")));
        layers << LP(new L("K2", V("Val2")));

        ValueLookup *l1 = new ValueLookup(layers);
        ValueLookup *l2 = new ValueLookup(layers);
        l2->context()->setReference(l1);

        QCOMPARE(l1->lookup(V("K1")).toString(), std::string("Val1"));
        QCOMPARE(l2->lookup(V("K1")).toString(), std::string("Val1"));
        QCOMPARE(l1->lookup(V("K2")).toString(), std::string("Val2"));
        QCOMPARE(l2->lookup(V("K2")).toString(), std::string("Val2"));

        V a;

        a["Reference/Key"] = "K2";
        QVERIFY(!l1->lookup(a).exists());
        QCOMPARE(l2->lookup(a).toString(), std::string("Val2"));

        l2->context()->setReference(NULL);
        QVERIFY(!l2->lookup(a).exists());

        delete l1;
        delete l2;
    }

    void condition()
    {
        QFETCH(Variant::Root, cond);
        QFETCH(bool, expected);
        QFETCH(QList<ValueLookupPointer>, layers);

        ValueLookup *l = new ValueLookup(layers);
        l->context()->substitutionStack().push();
        l->context()->substitutionStack().setString("SUB", "SubVal");
        QCOMPARE(l->isTrue(cond), expected);

        ValueLookup *c = l->clone();
        delete l;
        QCOMPARE(c->isTrue(cond), expected);
        delete c;

        l = new ValueLookup;
        l->context()->substitutionStack().push();
        l->context()->substitutionStack().setString("SUB", "SubVal");
        for (QList<ValueLookupPointer>::const_iterator add = layers.constBegin(),
                end = layers.constEnd(); add != end; ++add) {
            l->add(*add);
        }
        QCOMPARE(l->isTrue(cond), expected);
        delete l;
    }

    void condition_data()
    {
        QTest::addColumn<Variant::Root>("cond");
        QTest::addColumn<bool>("expected");
        QTest::addColumn<QList<ValueLookupPointer> >("layers");

        typedef Variant::Root V;
        typedef QList<ValueLookupPointer> LL;
        typedef ValueLookupPointer LP;
        typedef ValueLookupConstant L;

        QTest::newRow("Empty") << V() << true << LL();
        QTest::newRow("Override true") << V(true) << true << LL();
        QTest::newRow("Override false") << V(false) << false << LL();

        QTest::newRow("String defined") << V("KV") << true << (LL() << LP(new L("KV", V("Val"))));
        QTest::newRow("String undefined") << V("BV") << false << (LL() << LP(new L("KV", V("Val"))));

        V cond;
        cond["Value/Key"] = "K/1";
        QTest::newRow("Defined true") << cond << true << (LL() << LP(new L("K/1", V(1.0))));

        cond = V();
        cond["Value/Key"] = "K/1";
        QTest::newRow("Defined false") <<
                cond <<
                false <<
                (LL() << LP(new L("K/1", V(FP::undefined()))));

        cond = V();
        cond["Type"] = "Always";
        QTest::newRow("Always") << cond << true << LL();

        cond = V();
        cond["Type"] = "Always";
        cond["Invert"] = true;
        QTest::newRow("Always invert") << cond << false << LL();

        cond = V();
        cond["Type"] = "Never";
        QTest::newRow("Never") << cond << false << LL();

        cond = V();
        cond["Type"] = "Never";
        cond["Invert"] = true;
        QTest::newRow("Never invert") << cond << true << LL();

        cond = V();
        cond["Type"] = "OR";
        cond["Components/#0"] = false;
        cond["Components/#1"] = true;
        QTest::newRow("OR true") << cond << true << LL();

        cond = V();
        cond["Type"] = "OR";
        cond["Components/#0"] = false;
        cond["Components/#1"] = false;
        QTest::newRow("OR false") << cond << false << LL();

        cond = V();
        cond["Type"] = "AND";
        cond["Components/#0"] = true;
        cond["Components/#1"] = true;
        QTest::newRow("AND true") << cond << true << LL();

        cond = V();
        cond["Type"] = "AND";
        cond["Components/#0"] = false;
        cond["Components/#1"] = true;
        QTest::newRow("AND false") << cond << false << LL();

        cond = V();
        cond["Type"] = "Less";
        cond["Left/Key"] = "K/1";
        cond["Right/Key"] = "K/2";
        QTest::newRow("Less true") <<
                cond <<
                true <<
                (LL() << LP(new L("K/1", V(1.0))) << LP(new L("K/2", V(2.0))));

        cond = V();
        cond["Type"] = "Less";
        cond["Left/Constant"] = 3.0;
        cond["Right/Key"] = "K/2";
        QTest::newRow("Less false") <<
                cond <<
                false <<
                (LL() << LP(new L("K/1", V(1.0))) << LP(new L("K/2", V(2.0))));

        cond = V();
        cond["Type"] = "LessEqual";
        cond["Left/Key"] = "K/1";
        cond["Right/Key"] = "K/2";
        QTest::newRow("LessEqual true") <<
                cond <<
                true <<
                (LL() << LP(new L("K/1", V(1.0))) << LP(new L("K/2", V(1.0))));

        cond = V();
        cond["Type"] = "LessEqual";
        cond["Left/Constant"] = 3.0;
        cond["Right/Key"] = "K/2";
        QTest::newRow("LessEqual false") <<
                cond <<
                false <<
                (LL() << LP(new L("K/1", V(1.0))) << LP(new L("K/2", V(2.0))));

        cond = V();
        cond["Type"] = "Greater";
        cond["Left/Key"] = "K/2";
        cond["Right/Key"] = "K/1";
        QTest::newRow("Greater true") <<
                cond <<
                true <<
                (LL() << LP(new L("K/1", V(1.0))) << LP(new L("K/2", V(2.0))));

        cond = V();
        cond["Type"] = "Greater";
        cond["Left/Constant"] = 0.5;
        cond["Right/Key"] = "K/1";
        QTest::newRow("Greater false") <<
                cond <<
                false <<
                (LL() << LP(new L("K/1", V(1.0))) << LP(new L("K/2", V(2.0))));

        cond = V();
        cond["Type"] = "GreaterEqual";
        cond["Left/Key"] = "K/2";
        cond["Right/Key"] = "K/1";
        QTest::newRow("GreaterEqual true") <<
                cond <<
                true <<
                (LL() << LP(new L("K/1", V(1.0))) << LP(new L("K/2", V(1.0))));

        cond = V();
        cond["Type"] = "GreaterEqual";
        cond["Left/Constant"] = 0.5;
        cond["Right/Key"] = "K/1";
        QTest::newRow("GreaterEqual false") <<
                cond <<
                false <<
                (LL() << LP(new L("K/1", V(1.0))) << LP(new L("K/2", V(2.0))));

        cond = V();
        cond["Type"] = "Exact";
        cond["Left/Key"] = "K/2";
        cond["Right/Key"] = "K/1";
        QTest::newRow("Exact true") <<
                cond <<
                true <<
                (LL() << LP(new L("K/1", V(1.0))) << LP(new L("K/2", V(1.0))));

        cond = V();
        cond["Type"] = "Exact";
        cond["Left/Constant"] = "1.0";
        cond["Right/Key"] = "K/1";
        QTest::newRow("Exact false") <<
                cond <<
                false <<
                (LL() << LP(new L("K/1", V(1.0))) << LP(new L("K/2", V(1.0))));

        cond = V();
        cond["Type"] = "Equal";
        cond["Left/Key"] = "K/2";
        cond["Right/Key"] = "K/1";
        QTest::newRow("Equal true") <<
                cond <<
                true <<
                (LL() << LP(new L("K/1", V("A"))) << LP(new L("K/2", V("A"))));

        cond = V();
        cond["Type"] = "Equal";
        cond["Left/Key"] = "K/2";
        cond["Right/Key"] = "K/1";
        QTest::newRow("Equal false") <<
                cond <<
                false <<
                (LL() << LP(new L("K/1", V("a"))) << LP(new L("K/2", V("A"))));

        cond = V();
        cond["Type"] = "Text";
        cond["Left/Key"] = "K/1";
        cond["Right/Key"] = "K/2";
        QTest::newRow("Equal true") <<
                cond <<
                true <<
                (LL() << LP(new L("K/1", V("A"))) << LP(new L("K/2", V("a "))));

        cond = V();
        cond["Type"] = "Exists";
        cond["Value/Key"] = "K/1";
        QTest::newRow("Exists true") <<
                cond <<
                true <<
                (LL() << LP(new L("K/1", V(FP::undefined()))) << LP(new L("K/2", V("a"))));

        cond = V();
        cond["Type"] = "Exists";
        cond["Value/Key"] = "K/3";
        QTest::newRow("Exists false") <<
                cond <<
                false <<
                (LL() << LP(new L("K/1", V(FP::undefined()))) << LP(new L("K/2", V("a"))));

        cond = V();
        cond["Type"] = "Script";
        cond["Code"] = "return lookup('K').A == lookup('K/B'):toString()";
        QTest::newRow("Script true") <<
                cond <<
                true <<
                (LL() << LP(new L("K/A", V("a"))) << LP(new L("K/B", V("a"))));

        cond = V();
        cond["Type"] = "Script";
        cond["Code"] = "return lookup('K').A == lookup('K/B'):toString()";
        QTest::newRow("Script false") <<
                cond <<
                false <<
                (LL() << LP(new L("K/A", V("a"))) << LP(new L("K/B", V("b"))));
    }
};

QTEST_MAIN(TestValueLookup)

#include "valuelookup.moc"
