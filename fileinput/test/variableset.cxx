/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>

#include "fileinput/variableset.hxx"

using namespace CPD3;
using namespace CPD3::Input;
using namespace CPD3::Data;

class TestVariableSet : public QObject {
Q_OBJECT

    static bool valuesEqual(const Variant::Read &a, const Variant::Read &b)
    {
        if (a.getType() != Variant::Type::Real)
            return a == b;
        if (b.getType() != Variant::Type::Real)
            return a == b;
        double va = a.toDouble();
        double vb = b.toDouble();
        if (!FP::defined(va))
            return !FP::defined(vb);
        if (!FP::defined(vb))
            return false;
        double eps = qMax(va, vb) * 1E-6;
        if (eps == 0)
            return va == vb;
        return fabs(va - vb) < eps;
    }

    static bool valueListMatch(SequenceValue::Transfer values, SequenceValue::Transfer expected)
    {
        for (auto f = expected.begin(); f != expected.end();) {
            bool hit = false;
            for (auto c = values.begin(), endC = values.end(); c != endC; ++c) {
                if (!FP::equal(c->getStart(), f->getStart()))
                    continue;
                if (!FP::equal(c->getEnd(), f->getEnd()))
                    continue;
                if (c->getUnit() != f->getUnit())
                    continue;

                Variant::Root merged;
                if (c->getValue().getType() != Variant::Type::Array ||
                        f->getValue().getType() != Variant::Type::Array) {
                    merged = Variant::Root::overlay(c->root(), f->root());
                } else {
                    auto ca = c->read().toArray();
                    auto fa = f->read().toArray();
                    for (std::size_t i = 0, max = std::max(ca.size(), fa.size()); i < max; i++) {
                        merged.write()
                              .array(i)
                              .set(Variant::Root::overlay(Variant::Root(ca[i]),
                                                          Variant::Root(fa[i])));
                    }
                }
                if (!valuesEqual(merged, c->getValue())) {
                    qDebug() << "Value overlay mismatch for" << *f << "merged:" << merged
                             << "input:" << c->getValue();
                    return false;
                }

                values.erase(c);
                f = expected.erase(f);
                hit = true;
                break;
            }
            if (!hit)
                ++f;
        }
        if (!values.empty()) {
            qDebug() << "Unmatched values in result:" << values;
        }
        if (!expected.empty()) {
            qDebug() << "Unmatched values in expected:" << expected;
        }
        return values.empty() && expected.empty();
    }

private slots:

    void basic()
    {
        Variant::Root config;

        config["Groups/A/Type"] = "Number";
        config["Groups/A/Match/Type"] = "Exact";
        config["Groups/A/Match/Left/Key"] = "OtherValue";
        config["Groups/A/Match/Right/Constant"] = 1.0;
        config["Groups/A/Data/Calibration/#0"] = 1.0;
        config["Groups/A/Data/Calibration/#1"] = 1.0;
        config["Groups/A/Data/Station/Convert"] = "s1";
        config["Groups/A/Data/Archive/Convert"] = "arc";
        config["Groups/A/Data/Variable/Convert"] = "var${WAVELENGTHCODE}";
        config["Groups/A/Data/Metadata/Base/*dBaseline"] = "A";
        config["Groups/A/Data/Metadata/Overlay/1/Path"] = "^Wavelength";
        config["Groups/A/Data/Metadata/Overlay/1/Key"] = "Wavelength";

        config["Groups/B/Global/Type"] = "Greater";
        config["Groups/B/Global/Left/Key"] = "GlobalValue";
        config["Groups/B/Global/Right/Constant"] = 1.0;
        config["Groups/B/Match/Type"] = "GreaterEqual";
        config["Groups/B/Match/Left/Key"] = "OtherValue";
        config["Groups/B/Match/Right/Constant"] = 2.0;
        config["Groups/B/Data/Type"] = "Number";
        config["Groups/B/Data/Calibration/#0"] = 2.0;
        config["Groups/B/Data/Calibration/#1"] = 1.0;
        config["Groups/B/Data/Station/Convert"] = "s1";

        config["Groups/C/Global/Type"] = "Never";
        config["Groups/C/Match/Type"] = "Exact";
        config["Groups/C/Match/Left/Key"] = "OtherValue";
        config["Groups/C/Match/Right/Constant"] = 3.0;
        config["Groups/C/Data/Type"] = "Number";
        config["Groups/C/Data/Calibration/#0"] = 3.0;
        config["Groups/C/Data/Calibration/#1"] = 1.0;
        config["Groups/C/Data/Station/Convert"] = "s1";
        config["Groups/C/Data/Archive/Convert"] = "a1";

        config["Groups/D/Type"] = "Number";
        config["Groups/D/Match/Type"] = "Exact";
        config["Groups/D/Match/Left/Key"] = "OtherValue";
        config["Groups/D/Match/Right/Constant"] = 1.0;
        config["Groups/D/Auxiliary/AuxIn/Match/Type"] = "Exact";
        config["Groups/D/Auxiliary/AuxIn/Match/Left/Key"] = "OtherValue";
        config["Groups/D/Auxiliary/AuxIn/Match/Right/Constant"] = 2.0;
        config["Groups/D/Data/Type"] = "Script";
        config["Groups/D/Data/Code"] = "return VALUE * 1.0 + AuxIn * 1.0 + 10.0";
        config["Groups/D/Data/Station/Convert"] = "s1";
        config["Groups/D/Data/Archive/Convert"] = "arc";
        config["Groups/D/Data/Variable/Convert"] = "script";

        ValueLookup g;
        g.add(new ValueLookupConstant("GlobalValue", Variant::Root(2.0)));
        g.add(new ValueLookupConstant("OtherValue", Variant::Root(99.0)));

        OutputSet os(config);
        Record r1(&g);
        Record r2(&g);

        RecordVariable *r1v1 = r1.addVariable();
        RecordVariable *r1v2 = r1.addVariable();
        RecordVariable *r2v1 = r2.addVariable();

        r1v1->getLookup()->add(new ValueLookupConstant("OtherValue", Variant::Root(1.0)));
        r1v1->getLookup()->add(new ValueLookupConstant("Variable", Variant::Root("r1v1")));
        r1v1->getLookup()->add(new ValueLookupConstant("Wavelength", Variant::Root(550.0)));

        r1v2->getLookup()->add(new ValueLookupConstant("OtherValue", Variant::Root(2.0)));
        r1v2->getLookup()->add(new ValueLookupConstant("Variable", Variant::Root("r1v2")));
        r1v2->getLookup()->add(new ValueLookupConstant("Archive", Variant::Root("a2")));

        r2v1->getLookup()->add(new ValueLookupConstant("OtherValue", Variant::Root(3.0)));
        r2v1->getLookup()->add(new ValueLookupConstant("Variable", Variant::Root("r2v1")));
        r2v1->getLookup()->add(new ValueLookupConstant("Archive", Variant::Root("a2")));

        r1.defineTime(1000.0, 2000.0);
        r1v1->setValue(Variant::Root(0.1));
        r1v2->setValue(Variant::Root(0.2));

        r1.defineTime(2000.0, 3000.0);
        r1v1->setValue(Variant::Root(0.3));
        r1v2->setValue(Variant::Root(0.4));

        r2.defineTime(1000.0, 4000.0);
        r2v1->setValue(Variant::Root(0.5));

        os.activate(&g);
        os.integrateRecord(&r1);
        os.integrateRecord(&r2);
        SequenceValue::Transfer result(os.produceOutput());

        SequenceValue::Transfer expected;

        Variant::Root m;
        m["*dBaseline"] = "A";
        m["*dWavelength"] = 550.0;
        expected.emplace_back(SequenceName("s1", "arc_meta", "varG"), m, 1000.0, 3000.0);

        expected.emplace_back(SequenceName("s1", "arc", "varG"), Variant::Root(1.1), 1000.0,
                              2000.0);
        expected.emplace_back(SequenceName("s1", "arc", "varG"), Variant::Root(1.3), 2000.0,
                              3000.0);

        expected.emplace_back(SequenceName("s1", "a2", "r1v2"), Variant::Root(2.2), 1000.0, 2000.0);
        expected.emplace_back(SequenceName("s1", "a2", "r1v2"), Variant::Root(2.4), 2000.0, 3000.0);

        expected.emplace_back(SequenceName("s1", "a2", "r2v1"), Variant::Root(2.5), 1000.0, 4000.0);

        expected.emplace_back(SequenceName("s1", "arc", "script"), Variant::Root(10.3), 1000.0,
                              2000.0);
        expected.emplace_back(SequenceName("s1", "arc", "script"), Variant::Root(10.7), 2000.0,
                              3000.0);

        QVERIFY(valueListMatch(result, expected));
    }
};

QTEST_MAIN(TestVariableSet)

#include "variableset.moc"
