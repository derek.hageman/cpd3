/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>

#include "fileinput/inputengine.hxx"

using namespace CPD3;
using namespace CPD3::Input;
using namespace CPD3::Data;

class TestEngineLines : public InputLineBased {
public:
    bool ended;

    TestEngineLines() : ended(false)
    { }

    virtual ~TestEngineLines()
    { }

protected:
    bool process(QStringList &lines, bool ended) override
    {
        if (ended)
            this->ended = true;

        for (QList<QString>::const_iterator line = lines.constBegin(), end = lines.constEnd();
                line != end;
                ++line) {
            outputData(SequenceValue({"a", "b", "c"}, Variant::Root(*line)));
        }
        lines.clear();

        return !ended;
    }
};

class TestEngineHeaders : public InputLineHeaders {
public:
    bool ended;
    bool completed;

    TestEngineHeaders() : ended(false), completed(false)
    { }

    virtual ~TestEngineHeaders()
    { }

protected:
    bool processHeaders(QStringList &lines) override
    {
        while (!lines.isEmpty()) {
            if (!lines.first().startsWith('!'))
                return false;
            outputData(SequenceValue({"a", "b", "c"}, Variant::Root("H" + lines.first().mid(1))));
            lines.removeFirst();
        }
        return true;
    }

    void processData(QStringList &lines, bool ended) override
    {
        if (ended)
            this->ended = true;

        for (QList<QString>::const_iterator line = lines.constBegin(), end = lines.constEnd();
                line != end;
                ++line) {
            outputData(SequenceValue({"a", "b", "c"}, Variant::Root(*line)));
        }
        lines.clear();
    }

    bool completeFile() override
    {
        ended = true;
        completed = true;
        return false;
    }
};

class TestInputEngine : public QObject {
Q_OBJECT
private slots:

    void lines()
    {
        TestEngineLines engine;
        StreamSink::Buffer egress;

        engine.start();
        engine.setEgress(&egress);

        engine.incomingData(QByteArray("line1\n"));
        engine.incomingData(QByteArray("line2\r"));
        engine.incomingData(QByteArray("lin3\r\n"));
        engine.incomingData(QByteArray("finalline"));

        engine.endData();
        QVERIFY(engine.wait(30000));
        QVERIFY(engine.ended);

        QVERIFY(egress.ended());
        QCOMPARE((int) egress.values().size(), 4);
        QCOMPARE(egress.values()[0].read().toString(), std::string("line1"));
        QCOMPARE(egress.values()[1].read().toString(), std::string("line2"));
        QCOMPARE(egress.values()[2].read().toString(), std::string("lin3"));
        QCOMPARE(egress.values()[3].read().toString(), std::string("finalline"));
    }

    void headers()
    {
        TestEngineHeaders engine;
        StreamSink::Buffer egress;

        engine.start();
        engine.setEgress(&egress);

        engine.incomingData(QByteArray("!head1\r"));
        engine.incomingData(QByteArray("!head2\r\n"));
        engine.incomingData(QByteArray("line1\n"));
        engine.incomingData(QByteArray("line2"));

        engine.endData();
        QVERIFY(engine.wait(30000));
        QVERIFY(engine.ended);
        QVERIFY(engine.completed);

        QVERIFY(egress.ended());
        QCOMPARE((int) egress.values().size(), 4);
        QCOMPARE(egress.values()[0].read().toString(), std::string("Hhead1"));
        QCOMPARE(egress.values()[1].read().toString(), std::string("Hhead2"));
        QCOMPARE(egress.values()[2].read().toString(), std::string("line1"));
        QCOMPARE(egress.values()[3].read().toString(), std::string("line2"));
    }
};

QTEST_MAIN(TestInputEngine)

#include "inputengine.moc"
