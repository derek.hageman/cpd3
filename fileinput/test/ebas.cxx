/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>
#include <QSignalSpy>

#include "core/waitutils.hxx"
#include "fileinput/ebas.hxx"

using namespace CPD3;
using namespace CPD3::Input;
using namespace CPD3::Data;

class TestEBAS : public QObject {
Q_OBJECT

    static bool valuesEqual(const Variant::Read &a, const Variant::Read &b)
    {
        if (a.getType() != Variant::Type::Real || b.getType() != Variant::Type::Real)
            return a == b;
        double va = a.toDouble();
        double vb = b.toDouble();
        if (!FP::defined(va))
            return !FP::defined(vb);
        if (!FP::defined(vb))
            return false;
        double eps = qMax(va, vb) * 1E-6;
        if (eps == 0)
            return va == vb;
        return fabs(va - vb) < eps;
    }

    static bool valueListMatch(SequenceValue::Transfer values, SequenceValue::Transfer expected)
    {
        for (auto f = expected.begin(); f != expected.end();) {
            bool hit = false;
            for (auto c = values.begin(), endC = values.end(); c != endC; ++c) {
                if (!FP::equal(c->getStart(), f->getStart()))
                    continue;
                if (!FP::equal(c->getEnd(), f->getEnd()))
                    continue;
                if (c->getUnit() != f->getUnit())
                    continue;

                Variant::Root merged;
                if (c->getValue().getType() != Variant::Type::Array ||
                        f->getValue().getType() != Variant::Type::Array) {
                    merged = Variant::Root::overlay(c->root(), f->root());
                } else {
                    auto ca = c->read().toArray();
                    auto fa = f->read().toArray();
                    for (std::size_t i = 0, max = std::max(ca.size(), fa.size()); i < max; i++) {
                        merged.write()
                              .array(i)
                              .set(Variant::Root::overlay(Variant::Root(ca[i]),
                                                          Variant::Root(fa[i])));
                    }
                }
                if (!valuesEqual(merged, c->getValue())) {
                    qDebug() << "Value overlay mismatch for" << *f << "merged:" << merged
                             << "input:" << c->getValue();
                    return false;
                }

                values.erase(c);
                f = expected.erase(f);
                hit = true;
                break;
            }
            if (!hit)
                ++f;
        }
        if (!values.empty()) {
            qDebug() << "Unmatched values in result:" << values;
        }
        if (!expected.empty()) {
            qDebug() << "Unmatched values in expected:" << expected;
        }
        return values.empty() && expected.empty();
    }

    static Variant::Root mo(const Variant::Read &under,
                            const QString &path,
                            const Variant::Read &over)
    {
        Variant::Root ret(under);
        ret[path].set(over);
        return ret;
    }

    static Variant::Root mo(const Variant::Read &under, const QString &path, double over)
    { return mo(under, path, Variant::Root(over)); }

    static Variant::Root stats(double mean, double q15, double q84)
    {
        Variant::Root ret;
        ret["Mean"] = mean;
        ret["Quantiles"].keyframe(15.87 / 100.0) = q15;
        ret["Quantiles"].keyframe(84.13 / 100.0) = q84;
        return ret;
    }

private slots:

    void level2()
    {
        Variant::Root config;

        config["Groups/Neph_Flags/SortPriority"] = 1;
        config["Groups/Neph_Flags/Dependencies/Require"].applyFlag("Nephelometer");
        config["Groups/Neph_Flags/Match/Type"] = "Text";
        config["Groups/Neph_Flags/Match/Left/Key"] = "Component";
        config["Groups/Neph_Flags/Match/Right/Constant"] = "numflag";
        config["Groups/Neph_Flags/Data/Type"] = "Flags";
        config["Groups/Neph_Flags/Data/Variable/Convert"] = "F1_${SUFFIX}";
        config["Groups/Neph_Flags/Data/Flavors/Key"] = "Matrix";
        config["Groups/Neph_Flags/Data/Flavors/Convert/Type"] = "MatrixFlavors";
        config["Groups/Neph_Flags/Data/Metadata/Base/*fDescription"] = "System flags";
        config["Groups/Neph_Flags/Data/FlagTranslation/500/Flag"] = "SomeFlag";
        config["Groups/Neph_Flags/Data/FlagTranslation/500/Metadata/Description"] =
                "Flag description";

        config["Groups/Neph_T/Global/Type"] = "Text";
        config["Groups/Neph_T/Global/Left/Key"] = "Instrument/Type";
        config["Groups/Neph_T/Global/Right/Constant"] = "nephelometer";
        config["Groups/Neph_T/Match/Type"] = "Text";
        config["Groups/Neph_T/Match/Left/Key"] = "Component";
        config["Groups/Neph_T/Match/Right/Constant"] = "temperature";
        config["Groups/Neph_T/Data/Variable/Convert"] = "T_${SUFFIX}";
        config["Groups/Neph_T/Data/Flavors/Key"] = "Matrix";
        config["Groups/Neph_T/Data/Flavors/Convert/Type"] = "MatrixFlavors";
        config["Groups/Neph_T/Data/Calibration/#0"] = -273.15;
        config["Groups/Neph_T/Data/Calibration/#1"] = 1.0;
        config["Groups/Neph_T/Data/Metadata/Base/*dDescription"] = "Internal temperature";
        config["Groups/Neph_T/Data/Metadata/Base/*dUnits"] = "C";
        config["Groups/Neph_T/Data/Metadata/Overlay/SerialNumber/Path"] = "^Source/SerialNumber";
        config["Groups/Neph_T/Data/Metadata/Overlay/SerialNumber/Key"] = "Instrument/SerialNumber";

        config["Groups/Bs/Dependencies/Set"].applyFlag("Nephelometer");
        config["Groups/Bs/Match/Type"] = "And";
        config["Groups/Bs/Match/Components/#0/Type"] = "Text";
        config["Groups/Bs/Match/Components/#0/Left/Key"] = "Component";
        config["Groups/Bs/Match/Components/#0/Right/Constant"] =
                "aerosol_light_scattering_coefficient";
        config["Groups/Bs/Match/Components/#1/Type"] = "Defined";
        config["Groups/Bs/Match/Components/#1/Value/Key"] = "Quantile";
        config["Groups/Bs/Match/Components/#1/Invert"] = true;
        config["Groups/Bs/Data/Type"] = "Statistics";
        config["Groups/Bs/Data/Variable/Convert"] = "Bs${WAVELENGTHCODE}_${SUFFIX}";
        config["Groups/Bs/Data/Flavors/Key"] = "Matrix";
        config["Groups/Bs/Data/Flavors/Convert/Type"] = "MatrixFlavors";
        config["Groups/Bs/Data/Metadata/Base/*dDescription"] =
                "Aerosol light scattering coefficient";
        config["Groups/Bs/Data/Metadata/Overlay/Wavelength/Path"] = "^Wavelength";
        config["Groups/Bs/Data/Metadata/Overlay/Wavelength/Key"] = "Wavelength";
        config["Groups/Bs/Data/Metadata/Overlay/ReportT/Path"] = "^ReportT";
        config["Groups/Bs/Data/Metadata/Overlay/ReportT/Key"] = "ReportT";
        config["Groups/Bs/Data/Metadata/Overlay/Units/Path"] = "^Units";
        config["Groups/Bs/Data/Metadata/Overlay/Units/Key"] = "Units";
        config["Groups/Bs/Auxiliary/Quantile/Match/Type"] = "And";
        config["Groups/Bs/Auxiliary/Quantile/Match/Components/#0/Type"] = "Text";
        config["Groups/Bs/Auxiliary/Quantile/Match/Components/#0/Left/Key"] = "Component";
        config["Groups/Bs/Auxiliary/Quantile/Match/Components/#0/Right/Constant"] =
                "aerosol_light_scattering_coefficient";
        config["Groups/Bs/Auxiliary/Quantile/Match/Components/#1/Type"] = "Defined";
        config["Groups/Bs/Auxiliary/Quantile/Match/Components/#1/Value/Key"] = "Quantile";
        config["Groups/Bs/Auxiliary/Quantile/Match/Components/#2/Type"] = "Exact";
        config["Groups/Bs/Auxiliary/Quantile/Match/Components/#2/Left/Key/Reference/Key"] =
                "Wavelength";
        config["Groups/Bs/Auxiliary/Quantile/Match/Components/#2/Right/Key"] = "Wavelength";
        config["Groups/Bs/Auxiliary/Quantile/Name/Key"] = "Quantile";
        config["Groups/Bs/Auxiliary/Quantile/Name/Convert/Type"] = "Format";
        config["Groups/Bs/Auxiliary/Quantile/Name/Convert/Format"] = "000.00000";

        SequenceSegment seg;
        SequenceName unit("thd", "configuration", "ebasinput");
        seg.setValue(unit, config);
        StreamSink::Buffer egress;
        EBASEngine engine(SequenceSegment::Transfer{seg}, unit);
        engine.start();
        engine.setEgress(&egress);

        engine.incomingData(QByteArray("92 1001\n"
                                               "Ogren, John\n"
                                               "US06L, National Oceanic and Atmospheric Administration/Earth System Research Laboratory/Global Monitoring Division, NOAA/ESRL/GMD,, 325 Broadway,, 80305, \"Boulder, CO\", USA\n"
                                               "Ogren, John\n"
                                               "GAW-WDCA NOAA-ESRL\n"
                                               "1 1\n"
                                               "2012 01 01 2015 06 16\n"
                                               "0.041667\n"
                                               "Days from the file reference point (start_time)\n"
                                               "26\n"
                                               "1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
                                               "9999.999999 9999 9999 99999.99 9999.99 9999.99 999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9999.99999999 9.999999999999999999999999\n"
                                               "end_time of measurement, days from the file reference point\n"
                                               "start_time of measurement, year\n"
                                               "end_time of measurement, year\n"
                                               "pressure, hPa, Location=instrument internal, Matrix=instrument\n"
                                               "temperature, K, Location=instrument internal, Matrix=instrument\n"
                                               "relative_humidity, %, Location=instrument internal, Matrix=instrument\n"
                                               "number of wavelengths\n"
                                               "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=450nm\n"
                                               "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=550nm\n"
                                               "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=700nm\n"
                                               "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=450nm\n"
                                               "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=550nm\n"
                                               "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=700nm\n"
                                               "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=450nm, Statistics=percentile:15.87\n"
                                               "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=550nm, Statistics=percentile:15.87\n"
                                               "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=700nm, Statistics=percentile:15.87\n"
                                               "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=450nm, Statistics=percentile:15.87\n"
                                               "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=550nm, Statistics=percentile:15.87\n"
                                               "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=700nm, Statistics=percentile:15.87\n"
                                               "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=450nm, Statistics=percentile:84.13\n"
                                               "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=550nm, Statistics=percentile:84.13\n"
                                               "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=700nm, Statistics=percentile:84.13\n"
                                               "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=450nm, Statistics=percentile:84.13\n"
                                               "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=550nm, Statistics=percentile:84.13\n"
                                               "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=700nm, Statistics=percentile:84.13\n"
                                               "numflag\n"
                                               "0\n"
                                               "52\n"
                                               "Data definition:              EBAS_1.1\n"
                                               "Set type code:                TU\n"
                                               "Station code:                 US6005G\n"
                                               "Platform code:                US6005S\n"
                                               "Timezone:                     UTC\n"
                                               "Startdate:                    20120101000000\n"
                                               "Timeref:                      00_00\n"
                                               "Revision date:                20150616205734\n"
                                               "Component:                    aerosol_light_scattering_coefficient\n"
                                               "Unit:                         1/Mm\n"
                                               "Matrix:                       pm1\n"
                                               "Period code:                  6h\n"
                                               "Resolution code:              1h\n"
                                               "Sample duration:              1h\n"
                                               "Laboratory code:              US06L\n"
                                               "Instrument type:              nephelometer\n"
                                               "Instrument manufacturer:      TSI\n"
                                               "Instrument model:             3563\n"
                                               "Instrument name:              TSI_3563_THD\n"
                                               "Instrument serial number:     1070\n"
                                               "Method ref:                   US06L_scat_coef\n"
                                               "Add. qualifier:               1mn\n"
                                               "File name:                    US6005G.20120101000000.20150616205734.aerosol_light_scattering_coefficient.pm1.1d.1h.lev2.nas\n"
                                               "Station WDCA-ID:              GAWAUSCATHD\n"
                                               "Station WDCA-Name:            Trinidad Head, California\n"
                                               "Station GAW-ID:               THD\n"
                                               "Station state/province:       California\n"
                                               "Station latitude:             41.05000\n"
                                               "Station longitude:            -124.15000\n"
                                               "Station altitude:             213m\n"
                                               "Station land use:             Residential\n"
                                               "Station setting:              Coastal\n"
                                               "Station GAW type:             G\n"
                                               "Station WMO region:           4\n"
                                               "Originator:                   Ogren, John, John.A.Ogren@noaa.gov, National Oceanic and Atmospheric Administration/Earth System Research Laboratory/Global Monitoring Division, NOAA/ESRL/GMD,, 325 Broadway,, 80305, \"Boulder, CO\", USA\n"
                                               "Submitter:                    Ogren, John, John.A.Ogren@noaa.gov, National Oceanic and Atmospheric Administration/Earth System Research Laboratory/Global Monitoring Division, NOAA/ESRL/GMD,, 325 Broadway,, 80305, \"Boulder, CO\", USA\n"
                                               "Data level:                   2\n"
                                               "Version:                      1\n"
                                               "Version description:          Version numbering not tracked, generated by data.aggregate.ebas r4678\n"
                                               "Height AGL:                   10m\n"
                                               "Inlet type:                   Impactor--direct\n"
                                               "Inlet description:            Switched impactor at 1 um\n"
                                               "Humidity/temperature control: Heating to 40% RH, limit 40 deg. C\n"
                                               "Volume std. temperature:      273.15K\n"
                                               "Volume std. pressure:         1013.25hPa\n"
                                               "Detection limit:              0.4 1/Mm\n"
                                               "Detection limit expl.:        Determined by instrument noise on filtered air (2 standard deviations), no detection limit flag used\n"
                                               "Zero/negative values code:    zero/negative possible\n"
                                               "Zero/negative values:         Zero and neg. values may appear due to statistical variations at very low concentrations\n"
                                               "Standard method:              cal-gas=CO2+AIR_truncation-correction=Anderson1998\n"
                                               "Acknowledgement:              Request acknowledgement details from data originator\n"
                                               " start_time    end_time st_y ed_y    P_int   T_int  RH_int nWav         sc450         sc550         sc700        bsc450        bsc550        bsc700     lStdSc450     lStdSc550     lStdSc700    lStdBsc450    lStdBsc550    lStdBsc700     uStdSc450     uStdSc550     uStdSc700    uStdBsc450    uStdBsc550    uStdBsc700 numflag\n"
                                               "   0.000000    0.041667 2012 2012   992.68  301.38   20.67    3   24.40764706   17.94294118   11.56411765    2.64882353    1.88705882    1.64235294   19.42582400   13.88651200    9.14937600    2.04715200    1.52323200    1.13392000   29.07600000   21.56990400   13.71804800    3.36000000    2.39147200    2.11764800 0.500000000000000000000000\n"
                                               "   0.041667    0.083333 2012 2012   993.53  301.12   21.03    3   12.97352941    9.46058824    5.85470588    1.56176471    1.06529412    0.74176471    8.20488000    6.24438400    3.78547200    1.00696000    0.80539200    0.59000000   16.21441600   11.74912000    7.37598400    2.04372800    1.41608000    0.93921600 0.000000000000000000000000\n"
                                               "   0.083333    0.125000 2012 2012   993.83  300.86   17.30    3   16.20692308   11.60461538    7.63769231    1.82384615    1.44692308    1.16000000   12.60881600    9.32374800    6.42323200    1.34948400    1.13183600    0.95610000   20.15934800   13.57692000    9.03588800    2.26956000    1.85286800    1.40191200 0.000000000000000000000000\n"
                                               "   0.125000    0.166667 2012 2012   993.58  300.86   20.03    3   16.83176471   11.90764706    7.50764706    1.87411765    1.45823529    1.20058824   12.47235200    8.82156800    5.70166400    1.42539200    1.07470400    0.90940800   22.24896000   15.52276800    9.39648000    2.32686400    1.77137600    1.45833600 0.000000000000000000000000\n"
                                               "   0.166667    0.208333 2012 2012   992.85  300.92   20.31    3   14.38588235   10.83941176    6.49647059    1.48470588    1.40823529    0.94470588   11.42624000    8.37988800    5.01822400    1.10539200    1.01539200    0.66000000   17.72198400   13.11284800    7.75667200    1.80137600    1.88676800    1.23225600 0.000000000000000000000000\n"
                                               "   0.208333    0.250000 2012 2012   992.24 9999.99   19.46    3   11.43176471    8.11823529    5.16705882    1.23235294    1.06823529    0.79176471    4.94539200    2.83852800    2.09470400    0.38156800    0.33078400    0.34696000   20.09030400   15.10755200    9.31059200    2.15843200    1.64225600    1.37912000 0.000000000000000000000000\n"));

        engine.endData();
        QVERIFY(engine.wait(30000));

        typedef SequenceValue DV;

        SequenceName BsB("thd", "raw", "BsB_X1", {"pm1"});
        SequenceName BsG("thd", "raw", "BsG_X1", {"pm1"});
        SequenceName BsR("thd", "raw", "BsR_X1", {"pm1"});
        SequenceName T("thd", "raw", "T_X1", {"pm1"});
        SequenceName F1("thd", "raw", "F1_X1", {"pm1"});

        Variant::Root T_meta;
        T_meta.write().metadataReal("Description").setString("Internal temperature");
        T_meta.write().metadataReal("Units").setString("C");
        T_meta.write().metadataReal("Source").hash("SerialNumber").setInt64(1070);

        Variant::Root F1_meta;
        F1_meta.write().metadataFlags("Description").setString("System flags");
        F1_meta.write()
               .metadataSingleFlag("SomeFlag")
               .hash("Description")
               .setString("Flag description");
        F1_meta.write()
               .metadataSingleFlag("SomeFlag")
               .hash("Origin")
               .toArray()
               .after_back()
               .setString("ebas");

        Variant::Root Bs_meta;
        Bs_meta.write()
               .metadataReal("Description")
               .setString("Aerosol light scattering coefficient");
        Bs_meta.write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        Bs_meta.write().metadataReal("ReportT").setDouble(0);

        QVERIFY(egress.ended());
        QVERIFY(valueListMatch(egress.values(), (SequenceValue::Transfer{
                DV(F1.toMeta(), F1_meta, 1325376000, 1325397600),
                DV(T.toMeta(), T_meta, 1325376000, 1325397600),
                DV(BsB.toMeta(), mo(Bs_meta, "^Wavelength", 450.0), 1325376000, 1325397600),
                DV(BsG.toMeta(), mo(Bs_meta, "^Wavelength", 550.0), 1325376000, 1325397600),
                DV(BsR.toMeta(), mo(Bs_meta, "^Wavelength", 700.0), 1325376000, 1325397600),
                DV(F1, Variant::Root(Variant::Flags{"EBASFlag500", "SomeFlag"}), 1325376000,
                   1325379600), DV(F1, Variant::Root(Variant::Flags()), 1325379600, 1325383200),
                DV(F1, Variant::Root(Variant::Flags()), 1325383200, 1325386800),
                DV(F1, Variant::Root(Variant::Flags()), 1325386800, 1325390400),
                DV(F1, Variant::Root(Variant::Flags()), 1325390400, 1325394000),
                DV(F1, Variant::Root(Variant::Flags()), 1325394000, 1325397600),
                DV(T, Variant::Root(28.23), 1325376000, 1325379600),
                DV(T, Variant::Root(27.97), 1325379600, 1325383200),
                DV(T, Variant::Root(27.71), 1325383200, 1325386800),
                DV(T, Variant::Root(27.71), 1325386800, 1325390400),
                DV(T, Variant::Root(27.77), 1325390400, 1325394000),
                DV(T, Variant::Root(FP::undefined()), 1325394000, 1325397600),
                DV(BsB, Variant::Root(24.40764706), 1325376000, 1325379600),
                DV(BsB, Variant::Root(12.97352941), 1325379600, 1325383200),
                DV(BsB, Variant::Root(16.20692308), 1325383200, 1325386800),
                DV(BsB, Variant::Root(16.83176471), 1325386800, 1325390400),
                DV(BsB, Variant::Root(14.38588235), 1325390400, 1325394000),
                DV(BsB, Variant::Root(11.43176471), 1325394000, 1325397600),
                DV(BsB.withFlavor("stats"), stats(24.40764706, 19.42582400, 29.07600000),
                   1325376000, 1325379600),
                DV(BsB.withFlavor("stats"), stats(12.97352941, 8.20488000, 16.21441600), 1325379600,
                   1325383200),
                DV(BsB.withFlavor("stats"), stats(16.20692308, 12.60881600, 20.15934800),
                   1325383200, 1325386800),
                DV(BsB.withFlavor("stats"), stats(16.83176471, 12.47235200, 22.24896000),
                   1325386800, 1325390400),
                DV(BsB.withFlavor("stats"), stats(14.38588235, 11.42624000, 17.72198400),
                   1325390400, 1325394000),
                DV(BsB.withFlavor("stats"), stats(11.43176471, 4.94539200, 20.09030400), 1325394000,
                   1325397600), DV(BsG, Variant::Root(17.94294118), 1325376000, 1325379600),
                DV(BsG, Variant::Root(9.46058824), 1325379600, 1325383200),
                DV(BsG, Variant::Root(11.60461538), 1325383200, 1325386800),
                DV(BsG, Variant::Root(11.90764706), 1325386800, 1325390400),
                DV(BsG, Variant::Root(10.83941176), 1325390400, 1325394000),
                DV(BsG, Variant::Root(8.11823529), 1325394000, 1325397600),
                DV(BsG.withFlavor("stats"), stats(17.94294118, 13.88651200, 21.56990400),
                   1325376000, 1325379600),
                DV(BsG.withFlavor("stats"), stats(9.46058824, 6.24438400, 11.74912000), 1325379600,
                   1325383200),
                DV(BsG.withFlavor("stats"), stats(11.60461538, 9.32374800, 13.57692000), 1325383200,
                   1325386800),
                DV(BsG.withFlavor("stats"), stats(11.90764706, 8.82156800, 15.52276800), 1325386800,
                   1325390400),
                DV(BsG.withFlavor("stats"), stats(10.83941176, 8.37988800, 13.11284800), 1325390400,
                   1325394000),
                DV(BsG.withFlavor("stats"), stats(8.11823529, 2.83852800, 15.10755200), 1325394000,
                   1325397600), DV(BsR, Variant::Root(11.56411765), 1325376000, 1325379600),
                DV(BsR, Variant::Root(5.85470588), 1325379600, 1325383200),
                DV(BsR, Variant::Root(7.63769231), 1325383200, 1325386800),
                DV(BsR, Variant::Root(7.50764706), 1325386800, 1325390400),
                DV(BsR, Variant::Root(6.49647059), 1325390400, 1325394000),
                DV(BsR, Variant::Root(5.16705882), 1325394000, 1325397600),
                DV(BsR.withFlavor("stats"), stats(11.56411765, 9.14937600, 13.71804800), 1325376000,
                   1325379600),
                DV(BsR.withFlavor("stats"), stats(5.85470588, 3.78547200, 7.37598400), 1325379600,
                   1325383200),
                DV(BsR.withFlavor("stats"), stats(7.63769231, 6.42323200, 9.03588800), 1325383200,
                   1325386800),
                DV(BsR.withFlavor("stats"), stats(7.50764706, 5.70166400, 9.39648000), 1325386800,
                   1325390400),
                DV(BsR.withFlavor("stats"), stats(6.49647059, 5.01822400, 7.75667200), 1325390400,
                   1325394000),
                DV(BsR.withFlavor("stats"), stats(5.16705882, 2.09470400, 9.31059200), 1325394000,
                   1325397600)})));
    }

    void level0()
    {
        Variant::Root config;

        config["Groups/Neph_Flags/Match/Type"] = "Text";
        config["Groups/Neph_Flags/Match/Left/Key"] = "Component";
        config["Groups/Neph_Flags/Match/Right/Constant"] = "numflag";
        config["Groups/Neph_Flags/Data/Type"] = "Flags";
        config["Groups/Neph_Flags/Data/Variable/Convert"] = "F1_${SUFFIX}";
        config["Groups/Neph_Flags/Data/Flavors/Key"] = "Matrix";
        config["Groups/Neph_Flags/Data/Flavors/Convert/Type"] = "MatrixFlavors";
        config["Groups/Neph_Flags/Data/Metadata/Base/*fDescription"] = "System flags";

        config["Groups/Bs/Dependencies/Set"].applyFlag("Nephelometer");
        config["Groups/Bs/Match/Type"] = "Text";
        config["Groups/Bs/Match/Left/Key"] = "Component";
        config["Groups/Bs/Match/Right/Constant"] = "aerosol_light_scattering_coefficient";
        config["Groups/Bs/Data/Variable/Convert"] = "Bs${WAVELENGTHCODE}_${SUFFIX}";
        config["Groups/Bs/Data/Flavors/Key"] = "Matrix";
        config["Groups/Bs/Data/Flavors/Convert/Type"] = "MatrixFlavors";
        config["Groups/Bs/Data/Metadata/Base/*dDescription"] =
                "Aerosol light scattering coefficient";
        config["Groups/Bs/Data/Metadata/Overlay/Wavelength/Path"] = "^Wavelength";
        config["Groups/Bs/Data/Metadata/Overlay/Wavelength/Key"] = "Wavelength";

        config["Groups/Bsw/Match/Type"] = "Text";
        config["Groups/Bsw/Match/Left/Key"] = "Component";
        config["Groups/Bsw/Match/Right/Constant"] =
                "aerosol_light_scattering_coefficient_zero_measurement";
        config["Groups/Bsw/Data/Type"] = "NephZero";
        config["Groups/Bsw/Data/Variable/Convert"] = "Bsw${WAVELENGTHCODE}_${SUFFIX}";
        config["Groups/Bsw/Data/Metadata/Base/*dDescription"] =
                "Aerosol light scattering coefficient wall signal";
        config["Groups/Bsw/Data/Metadata/Overlay/Wavelength/Path"] = "^Wavelength";
        config["Groups/Bsw/Data/Metadata/Overlay/Wavelength/Key"] = "Wavelength";
        config["Groups/Bsw/Auxiliary/Rayleigh/Match/Type"] = "And";
        config["Groups/Bsw/Auxiliary/Rayleigh/Match/Components/#0/Type"] = "Text";
        config["Groups/Bsw/Auxiliary/Rayleigh/Match/Components/#0/Left/Key"] = "Component";
        config["Groups/Bsw/Auxiliary/Rayleigh/Match/Components/#0/Right/Constant"] =
                "aerosol_light_rayleighscattering_coefficient_zero_measurement";
        config["Groups/Bsw/Auxiliary/Rayleigh/Match/Components/#1/Type"] = "Exact";
        config["Groups/Bsw/Auxiliary/Rayleigh/Match/Components/#1/Left/Key/Reference/Key"] =
                "Wavelength";
        config["Groups/Bsw/Auxiliary/Rayleigh/Match/Components/#1/Right/Key"] = "Wavelength";

        SequenceSegment seg;
        SequenceName unit("bnd", "configuration", "ebasinput");
        seg.setValue(unit, config);
        StreamSink::Buffer egress;
        EBASEngine engine(SequenceSegment::Transfer
        { seg }, unit);
        engine.start();
        engine.setEgress(&egress);

        engine.incomingData(QByteArray("93 1001\n"
                                               "Ogren, John\n"
                                               "US06L, National Oceanic and Atmospheric Administration/Earth System Research Laboratory/Global Monitoring Division, NOAA/ESRL/GMD, , 325 Broadway, , , \"Boulder, CO\", USA\n"
                                               "Ogren, John\n"
                                               "GAW-WDCA_NRT NOAA-ESRL_NRT\n"
                                               "1 1\n"
                                               "2015 05 01 2015 07 09\n"
                                               "0.000694\n"
                                               "Days from the file reference point (start_time)\n"
                                               "29\n"
                                               "1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1\n"
                                               "9999.999999 9999 9999 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 0x9999 0000 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9.999999999999999\n"
                                               "end_time of measurement, days from the file reference point\n"
                                               "start_time of measurement, year\n"
                                               "end_time of measurement, year\n"
                                               "pressure, hPa, Location=instrument internal, Matrix=instrument\n"
                                               "temperature, K, Location=instrument inlet, Matrix=instrument\n"
                                               "temperature, K, Location=instrument outlet, Matrix=instrument\n"
                                               "relative_humidity, %, Location=instrument inlet, Matrix=instrument\n"
                                               "relative_humidity, %, Location=instrument outlet, Matrix=instrument\n"
                                               "flow_rate, l/min, Location=instrument internal, Matrix=instrument, Volume std. temperature=273.15 K, Volume std. pressure=1013.25 hPa\n"
                                               "electric_tension, V, Location=lamp supply, Matrix=instrument\n"
                                               "electric_current, A, Location=lamp supply, Matrix=instrument\n"
                                               "status, no unit, Status=overall instrument status, Matrix=instrument\n"
                                               "number of wavelengths\n"
                                               "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=450 nm\n"
                                               "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=550 nm\n"
                                               "aerosol_light_scattering_coefficient, 1/Mm, Wavelength=700 nm\n"
                                               "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=450 nm\n"
                                               "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=550 nm\n"
                                               "aerosol_light_backscattering_coefficient, 1/Mm, Wavelength=700 nm\n"
                                               "aerosol_light_scattering_coefficient_zero_measurement, 1/Mm, Wavelength=450 nm\n"
                                               "aerosol_light_scattering_coefficient_zero_measurement, 1/Mm, Wavelength=550 nm\n"
                                               "aerosol_light_scattering_coefficient_zero_measurement, 1/Mm, Wavelength=700 nm\n"
                                               "aerosol_light_backscattering_coefficient_zero_measurement, 1/Mm, Wavelength=450 nm\n"
                                               "aerosol_light_backscattering_coefficient_zero_measurement, 1/Mm, Wavelength=550 nm\n"
                                               "aerosol_light_backscattering_coefficient_zero_measurement, 1/Mm, Wavelength=700 nm\n"
                                               "aerosol_light_rayleighscattering_coefficient_zero_measurement, 1/Mm, Wavelength=450 nm\n"
                                               "aerosol_light_rayleighscattering_coefficient_zero_measurement, 1/Mm, Wavelength=550 nm\n"
                                               "aerosol_light_rayleighscattering_coefficient_zero_measurement, 1/Mm, Wavelength=700 nm\n"
                                               "numflag\n"
                                               "0\n"
                                               "50\n"
                                               "Data definition:              EBAS_1.1\n"
                                               "Set type code:                TI\n"
                                               "Station code:                 US0035R\n"
                                               "Platform code:                US0035S\n"
                                               "Timezone:                     UTC\n"
                                               "Startdate:                    20150501000000\n"
                                               "Revision date:                20150709201115\n"
                                               "Component:                    aerosol_light_scattering_coefficient\n"
                                               "Unit:                         1/Mm\n"
                                               "Matrix:                       pm10\n"
                                               "Period code:                  1h\n"
                                               "Resolution code:              1mn\n"
                                               "Sample duration:              1mn\n"
                                               "Laboratory code:              US06L\n"
                                               "Instrument type:              nephelometer\n"
                                               "Instrument manufacturer:      TSI\n"
                                               "Instrument model:             3563\n"
                                               "Instrument name:              TSI_3563_BND\n"
                                               "Instrument serial number:     1087\n"
                                               "Method ref:                   US06L_scat_coef\n"
                                               "File name:                    US0035R.20150501000000.20150709201115.aerosol_light_scattering_coefficient.pm10.1h.1mn.lev0.nas\n"
                                               "Station WDCA-ID:              GAWAUSILBND\n"
                                               "Station GAW-ID:               BND\n"
                                               "Station state/province:       Illinois\n"
                                               "Measurement latitude:         40.05000\n"
                                               "Measurement longitude:        -88.36700\n"
                                               "Measurement altitude:         213m\n"
                                               "Station land use:             Agricultural\n"
                                               "Station setting:              Rural\n"
                                               "Station GAW type:             R\n"
                                               "Station WMO region:           4\n"
                                               "Originator:                   Ogren, John, John.A.Ogren@noaa.gov, National Oceanic and Atmospheric Administration/Earth System Research Laboratory/Global Monitoring Division, NOAA/ESRL/GMD, , 325 Broadway, , , \"Boulder, CO\", USA\n"
                                               "Submitter:                    Ogren, John, John.A.Ogren@noaa.gov, National Oceanic and Atmospheric Administration/Earth System Research Laboratory/Global Monitoring Division, NOAA/ESRL/GMD, , 325 Broadway, , , \"Boulder, CO\", USA\n"
                                               "Data level:                   0\n"
                                               "Version:                      1\n"
                                               "Version description:          Version numbering not tracked, generated by CPD3 EBAS Export 4713 compiled at 2015-07-06T09:45:02 run as PID 29765 run by hageman(115) on aero with da.output.ebas --profile=nrtneph bnd 2015-05-01 1h\n"
                                               "Height AGL:                   10m\n"
                                               "Inlet type:                   Impactor--direct\n"
                                               "Inlet description:            Switched impactor at 10 um\n"
                                               "Humidity/temperature control: Heating to 40% RH, limit 40 deg. C\n"
                                               "Volume std. temperature:      instrument internal\n"
                                               "Volume std. pressure:         instrument internal\n"
                                               "Detection limit:              0.4 1/Mm\n"
                                               "Detection limit expl.:        Determined by instrument noise on filtered air (2 standard deviations), no detection limit flag used\n"
                                               "Zero/negative values code:    zero/negative possible\n"
                                               "Zero/negative values:         Zero and neg. values may appear due to statistical variations at very low concentrations\n"
                                               "Standard method:              cal-gas=CO2+AIR_truncation-correction=none\n"
                                               "Comment:                      calibration, but no truncation correction applied\n"
                                               "Acknowledgement:              Request acknowledgement details from data originator\n"
                                               " start_time    end_time st_y ed_y   p_int   T_int   T_int  RH_int  RH_out    flow  lamp_v  lamp_c  flags NWav   sc450   sc550   sc700  bsc450  bsc550  bsc700  sc450z  sc550z  sc700z bsc450z bsc550z bsc700z  sc450r  sc550r  sc700r           numflag\n"));

        SequenceName F1("bnd", "raw", "F1_X1", {"pm10"});
        SequenceName BsB("bnd", "raw", "BsB_X1", {"pm10"});
        SequenceName BsG("bnd", "raw", "BsG_X1", {"pm10"});
        SequenceName BsR("bnd", "raw", "BsR_X1", {"pm10"});
        SequenceName BswB("bnd", "raw", "BswB_X1");
        SequenceName BswG("bnd", "raw", "BswG_X1");
        SequenceName BswR("bnd", "raw", "BswR_X1");

        while (engine.predictedOutputs().empty()) {
            QTest::qSleep(50);
        }
        QCOMPARE(engine.predictedOutputs(),
                 (SequenceName::Set{F1, BsB, BsG, BsR, BswB, BswG, BswR}));

        engine.incomingData(QByteArray(
                "   0.000000    0.000000 2015 2015 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99   12.60    6.00 0x9999    3 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99   28.87   15.05   14.38   14.06    6.86    7.31   23.93   10.49    3.94 0.000000000000000\n"
                        "   0.000694    0.001389 2015 2015  985.93  302.34  307.72   15.52   11.45   28.14   12.60    6.00 0x0000    3   23.77   18.32   14.49    4.00    3.72    3.51 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 0.000000000000000\n"
                        "   0.001389    0.002083 2015 2015  985.94  302.30  307.71   15.48   11.40   28.14   12.60    6.00 0x0000    3   20.58   13.91    9.63    3.09    2.41    1.77 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99 0.000000000000000\n"
                        "   0.002083    0.002778 2015 2015 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99   12.60    6.00 0x9999    3 9999.99 9999.99 9999.99 9999.99 9999.99 9999.99   28.88   15.06   14.39   14.07    6.87    7.32   23.93   10.49    3.94 0.000000000000000\n"));

        engine.endData();
        QVERIFY(engine.wait(30000));

        typedef SequenceValue DV;

        Variant::Root F1_meta;
        F1_meta.write().metadataFlags("Description").setString("System flags");

        Variant::Root Bs_meta;
        Bs_meta.write()
               .metadataReal("Description")
               .setString("Aerosol light scattering coefficient");

        Variant::Root Bsw_meta;
        Bsw_meta.write().metadataReal("Description")
                .setString("Aerosol light scattering coefficient wall signal");

        QVERIFY(egress.ended());
        QVERIFY(valueListMatch(egress.values(), (SequenceValue::Transfer{
                DV(F1.toMeta(), F1_meta, 1430438400, 1430438640),
                DV(BsB.toMeta(), mo(Bs_meta, "^Wavelength", 450.0), 1430438400, 1430438640),
                DV(BsG.toMeta(), mo(Bs_meta, "^Wavelength", 550.0), 1430438400, 1430438640),
                DV(BsR.toMeta(), mo(Bs_meta, "^Wavelength", 700.0), 1430438400, 1430438640),
                DV(BswB.toMeta(), mo(Bsw_meta, "^Wavelength", 450.0), 1430438400, 1430438760),
                DV(BswG.toMeta(), mo(Bsw_meta, "^Wavelength", 550.0), 1430438400, 1430438760),
                DV(BswR.toMeta(), mo(Bsw_meta, "^Wavelength", 700.0), 1430438400, 1430438760),
                DV(F1, Variant::Root(Variant::Flags()), 1430438400, 1430438460),
                DV(F1, Variant::Root(Variant::Flags()), 1430438460, 1430438520),
                DV(F1, Variant::Root(Variant::Flags()), 1430438520, 1430438580),
                DV(F1, Variant::Root(Variant::Flags()), 1430438580, 1430438640),
                DV(BsB, Variant::Root(FP::undefined()), 1430438400, 1430438460),
                DV(BsB, Variant::Root(23.77), 1430438460, 1430438520),
                DV(BsB, Variant::Root(20.58), 1430438520, 1430438580),
                DV(BsB, Variant::Root(FP::undefined()), 1430438580, 1430438640),
                DV(BsG, Variant::Root(FP::undefined()), 1430438400, 1430438460),
                DV(BsG, Variant::Root(18.32), 1430438460, 1430438520),
                DV(BsG, Variant::Root(13.91), 1430438520, 1430438580),
                DV(BsG, Variant::Root(FP::undefined()), 1430438580, 1430438640),
                DV(BsR, Variant::Root(FP::undefined()), 1430438400, 1430438460),
                DV(BsR, Variant::Root(14.49), 1430438460, 1430438520),
                DV(BsR, Variant::Root(9.63), 1430438520, 1430438580),
                DV(BsR, Variant::Root(FP::undefined()), 1430438580, 1430438640),
                DV(BswB, Variant::Root(4.94), 1430438400, 1430438580),
                DV(BswB, Variant::Root(4.95), 1430438580, 1430438760),
                DV(BswG, Variant::Root(4.56), 1430438400, 1430438580),
                DV(BswG, Variant::Root(4.57), 1430438580, 1430438760),
                DV(BswR, Variant::Root(10.44), 1430438400, 1430438580),
                DV(BswR, Variant::Root(10.45), 1430438580, 1430438760)})));
    }
};

QTEST_MAIN(TestEBAS)

#include "ebas.moc"
