/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>

#include "fileinput/record.hxx"

using namespace CPD3;
using namespace CPD3::Input;
using namespace CPD3::Data;

class TestRecord : public QObject {
Q_OBJECT
private slots:

    void basic()
    {
        ValueLookup lookup;
        lookup.add(new ValueLookupConstant("Underlay", Variant::Root(1.0)));

        Record r(&lookup);
        RecordVariable *v1 = r.addVariable();
        RecordVariable *v2 = r.addVariable();
        RecordVariable *v3 = r.addVariable();

        v2->getLookup()->add(new ValueLookupConstant("Underlay", Variant::Root(2.0)));

        QCOMPARE(v1->getLookup()->lookup(Variant::Root("Underlay")).toDouble(), 1.0);
        QCOMPARE(v2->getLookup()->lookup(Variant::Root("Underlay")).toDouble(), 2.0);

        r.defineTime(1000, 2000);
        v1->setValue(Variant::Root(1.1));
        v2->setValue(Variant::Root(1.2));
        v3->setValue(Variant::Root(1.3));

        r.defineTime(2000, 3000);
        v1->setValue(Variant::Root(2.1));
        v2->setValue(Variant::Root(2.2));

        r.defineTime(3000, FP::undefined());
        v1->setValue(Variant::Root(3.1));

        QList<Record::DefinedTime> result(r.output());
        QCOMPARE(result.size(), 3);

        QCOMPARE(result[0].getStart(), 1000.0);
        QCOMPARE(result[0].getEnd(), 2000.0);
        QCOMPARE((int) result[0].values.size(), 3);
        QCOMPARE(result[0].values[v1->getIndex()].read().toDouble(), 1.1);
        QCOMPARE(result[0].values[v2->getIndex()].read().toDouble(), 1.2);
        QCOMPARE(result[0].values[v3->getIndex()].read().toDouble(), 1.3);

        QCOMPARE(result[1].getStart(), 2000.0);
        QCOMPARE(result[1].getEnd(), 3000.0);
        QCOMPARE((int) result[1].values.size(), 3);
        QCOMPARE(result[1].values[v1->getIndex()].read().toDouble(), 2.1);
        QCOMPARE(result[1].values[v2->getIndex()].read().toDouble(), 2.2);
        QVERIFY(!result[1].values[v3->getIndex()].read().exists());

        QCOMPARE(result[2].getStart(), 3000.0);
        QCOMPARE(result[2].getEnd(), 4000.0);
        QCOMPARE((int) result[2].values.size(), 3);
        QCOMPARE(result[2].values[v1->getIndex()].read().toDouble(), 3.1);
        QVERIFY(!result[2].values[v2->getIndex()].read().exists());
        QVERIFY(!result[2].values[v3->getIndex()].read().exists());
    }
};

QTEST_MAIN(TestRecord)

#include "record.moc"
