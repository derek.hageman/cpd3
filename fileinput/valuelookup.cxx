/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "fileinput/valuelookup.hxx"
#include "fileinput/inputengine.hxx"
#include "core/util.hxx"
#include "luascript/libs/variant.hxx"


Q_LOGGING_CATEGORY(log_fileinput_valuelookup, "cpd3.fileinput.valuelookup", QtWarningMsg)

using namespace CPD3::Data;

namespace CPD3 {
namespace Input {

/** @file fileinput/valuelookup.hxx
 * Value lookup handling.
 */

ValueLookupContext::ValueLookupContext() : substitutions(this), reference(nullptr)
{ }

ValueLookupContext::ValueLookupContext(const ValueLookupContext &other) : substitutions(this,
                                                                                        other.substitutions),
                                                                          reference(nullptr)
{ }

ValueLookupContext::~ValueLookupContext() = default;

Variant::Read ValueLookupContext::convert(const Variant::Read &input,
                                          const Variant::Read &converter,
                                          bool defined)
{
    switch (converter.getType()) {
    case Variant::Type::Real:
    case Variant::Type::Integer:
    case Variant::Type::Flags:
        return converter;
    case Variant::Type::String:
        return Variant::Root(substitutions.apply(converter.toQString(), input));
    default:
        break;

    }

    const auto &type = converter["Type"].toString();
    if (Util::equal_insensitive(type, "override")) {
        if (!defined && !converter["AllowUndefined"].toBool())
            return Variant::Root();
        return converter["Value"];
    }

    if (!defined)
        return Variant::Read::empty();

    if (Util::equal_insensitive(type, "string")) {
        return Variant::Root(substitutions.apply(converter["Value"].toQString(), input));
    } else if (Util::equal_insensitive(type, "format")) {
        NumberFormat fmt(converter["Format"].toQString());
        return Variant::Root(fmt.apply(input.toDouble()));
    } else if (Util::equal_insensitive(type, "realstring")) {
        bool ok = false;
        double v = input.toQString().toDouble(&ok);
        if (!ok)
            v = FP::undefined();
        return Variant::Root(v);
    } else if (Util::equal_insensitive(type, "integerstring")) {
        qint64 base = converter["Base"].toInt64();
        if (!INTEGER::defined(base) || base < 0 || base > 36)
            base = 10;
        bool ok = false;
        qint64 v = input.toQString().toLongLong(&ok, static_cast<int>(base));
        if (!ok)
            v = INTEGER::undefined();
        return Variant::Root(v);
    } else if (Util::equal_insensitive(type, "script")) {
        return evaluateConverter(converter["Code"].toQString(), input, converter);
    }

    return input;
}

void ValueLookupContext::prepareScript()
{
    if (scriptEngine)
        return;

    scriptEngine.reset(new Lua::Engine);

    Lua::Engine::Frame root(*scriptEngine);
    {
        Lua::Engine::Assign assign(root, root.registry(), "CPD3_libfileinput_print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_fileinput_valuelookup) << entry[i].toOutputString();
            }
        }));
    }
}

QString ValueLookupContext::evaluateReplacement(const QString &code,
                                                const Data::Variant::Read &value)
{
    prepareScript();

    Lua::Engine::Frame root(*scriptEngine);
    auto env = root.pushSandboxEnvironment();
    {
        Lua::Engine::Assign assign(root, env, "print");
        assign.push(root.registry(), "CPD3_libfileinput_print");
    }
    {
        Lua::Engine::Assign assign(root, env, "VALUE");
        assign.pushData<Lua::Libs::Variant>(Variant::Root(value).write());
    }

    Lua::Engine::Call call(root);
    if (!call.pushChunk(code.toStdString(), env)) {
#ifndef NDEBUG
        auto error = scriptEngine->errorDescription();
        scriptEngine->clearError();
        return QString::fromStdString("_SCRIPT_PARSE_ERROR(" + error + ")");
#else
        scriptEngine->clearError();
        return "_SCRIPT_PARSE_ERROR";
#endif
    }
    if (!call.execute(1)) {
#ifndef NDEBUG
        auto error = scriptEngine->errorDescription();
        scriptEngine->clearError();
        return QString::fromStdString("_SCRIPT_EXECUTE_ERROR(" + error + ")");
#else
        scriptEngine->clearError();
        return "_SCRIPT_EXECUTE_ERROR";
#endif
    }

    return call.back().toQString();
}

Variant::Read ValueLookupContext::evaluateConverter(const QString &code,
                                                    const Variant::Read &value,
                                                    const Variant::Read &converter)
{
    prepareScript();

    Lua::Engine::Frame root(*scriptEngine);
    auto env = root.pushSandboxEnvironment();
    {
        Lua::Engine::Assign assign(root, env, "print");
        assign.push(root.registry(), "CPD3_libfileinput_print");
    }
    {
        Lua::Engine::Assign assign(root, env, "VALUE");
        assign.pushData<Lua::Libs::Variant>(Variant::Root(value).write());
    }
    {
        Lua::Engine::Assign assign(root, env, "CONVERTER");
        assign.pushData<Lua::Libs::Variant>(Variant::Root(converter).write());
    }

    Lua::Engine::Call call(root);
    if (!call.pushChunk(code.toStdString(), env)) {
        scriptEngine->clearError();
        return Variant::Read::empty();
    }
    if (!call.execute(1)) {
        scriptEngine->clearError();
        return Variant::Read::empty();
    }

    return Lua::Libs::Variant::extract(call, call.back());
}

bool ValueLookupContext::evaluateCondition(const QString &code,
                                           ValueLookup *lookup,
                                           const Data::Variant::Read &condition)
{
    prepareScript();

    Lua::Engine::Frame root(*scriptEngine);
    auto env = root.pushSandboxEnvironment();
    {
        Lua::Engine::Assign assign(root, env, "print");
        assign.push(root.registry(), "CPD3_libfileinput_print");
    }
    {
        Lua::Engine::Assign assign(root, env, "CONDITION");
        assign.pushData<Lua::Libs::Variant>(Variant::Root(condition).write());
    }
    if (lookup) {
        Lua::Engine::Assign assign(root, env, "lookup");
        assign.push(std::function<void(Lua::Engine::Entry &)>([lookup](Lua::Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Invalid arguments");
                return;
            }

            auto key = Lua::Libs::Variant::extract(entry, entry[0]);
            auto converter = Variant::Read::empty();
            if (entry.size() > 1) {
                converter = Lua::Libs::Variant::extract(entry, entry[1]);
            }

            entry.pushData<Lua::Libs::Variant>(
                    Variant::Root(lookup->lookup(key, converter)).write());
            entry.replaceWithBack(0);
            entry.propagate(1);
        }));
    }

    Lua::Engine::Call call(root);
    if (!call.pushChunk(code.toStdString(), env)) {
        scriptEngine->clearError();
        return false;
    }
    if (!call.execute(1)) {
        scriptEngine->clearError();
        return false;
    }
    return call.back().toBoolean();
}


void ValueLookupContext::setReference(ValueLookup *ref)
{ reference = ref; }

ValueLookup *ValueLookupContext::getReference() const
{ return reference; }

ValueLookupContext::Substitutions::Substitutions(ValueLookupContext *p) : parent(p)
{ }

ValueLookupContext::Substitutions::Substitutions(ValueLookupContext *p, const Substitutions &other)
        : Data::Variant::Composite::SubstitutionStack(other),
          parent(p),
          context(Variant::Read::empty())
{ }

ValueLookupContext::Substitutions::~Substitutions() = default;

QString ValueLookupContext::Substitutions::literalCheck(QStringRef &key) const
{
    if (key.length() > 1 && key.at(0) == '<') {
        key = QStringRef(key.string(), key.position() + 1, key.length() - 1);
        return QString('>');
    }
    return QString();
}

QString ValueLookupContext::Substitutions::literalSubstitution(const QStringRef &key,
                                                               const QString &close) const
{
    if (!key.isEmpty()) {
        return parent->evaluateReplacement(key.toString(), context);
    }
    return QString();
}

QString ValueLookupContext::Substitutions::apply(const QString &input,
                                                 const Data::Variant::Read &context)
{
    this->context = context;
    auto result = Data::Variant::Composite::SubstitutionStack::apply(input);
    this->context = Variant::Read::empty();
    return result;
}


ValueLookupLayer::ValueLookupLayer() = default;

ValueLookupLayer::~ValueLookupLayer() = default;

qint64 ValueLookupLayer::sortPriority() const
{ return 0; }

const std::string &ValueLookupLayer::keyToString(ValueLookupContext *,
                                                 const Data::Variant::Read &key)
{
    if (key.getType() == Data::Variant::Type::Hash)
        return key["Origin"].toString();
    return key.toString();
}

ValueLookupConstant::ValueLookupConstant(const QString &key, const Variant::Read &v, qint64 p)
        : value(), priority(p)
{
    value[key].set(v);
}

ValueLookupConstant::~ValueLookupConstant() = default;

bool ValueLookupConstant::lookup(ValueLookupContext *context,
                                 const Variant::Read &key,
                                 Variant::Root &result)
{
    const auto &ks = keyToString(context, key);
    if (ks.empty())
        return false;
    Variant::Read v = value[ks];
    if (!v.exists())
        return false;
    result.write().set(v);
    return true;
}

qint64 ValueLookupConstant::sortPriority() const
{ return priority; }


static bool layerSort(const ValueLookupPointer &a, const ValueLookupPointer &b)
{
    qint64 pa = a->sortPriority();
    qint64 pb = b->sortPriority();
    if (!INTEGER::defined(pa)) {
        if (!INTEGER::defined(pb))
            return false;
        return true;
    } else if (!INTEGER::defined(pb)) {
        return false;
    }
    return pa < pb;
}

ValueLookup::ValueLookup(const QList<ValueLookupPointer> &l) : layers(l)
{
    std::stable_sort(layers.begin(), layers.end(), layerSort);
}

ValueLookup::~ValueLookup() = default;

void ValueLookup::add(const ValueLookupPointer &layer)
{
    layers.insert(std::upper_bound(layers.begin(), layers.end(), layer, layerSort), layer);
}

void ValueLookup::add(const QList<ValueLookupPointer> &l)
{
    if (l.isEmpty())
        return;
    layers.append(l);
    std::stable_sort(layers.begin(), layers.end(), layerSort);
}

Variant::Read ValueLookup::lookup(const Variant::Read &key, const Variant::Read &convert)
{
    if (context()->getReference() && key["Reference"].exists()) {
        auto rv = context()->getReference()->lookup(key["Reference/Key"], key["Reference/Convert"]);
        return context()->convert(rv, convert, rv.exists());
    }
    std::vector<Variant::Root> merge;
    for (QList<ValueLookupPointer>::iterator l = layers.begin(), endL = layers.end();
            l != endL;
            ++l) {
        Variant::Root v;
        if ((*l)->lookup(context(), key, v))
            merge.emplace_back(std::move(v));
    }
    if (merge.empty())
        return context()->convert(Variant::Read::empty(), convert, false);
    return context()->convert(Variant::Root::overlay(merge.begin(), merge.end()), convert, true);
}

bool ValueLookup::has(const Data::Variant::Read &key)
{
    for (QList<ValueLookupPointer>::iterator l = layers.begin(), endL = layers.end();
            l != endL;
            ++l) {
        Variant::Root v;
        if ((*l)->lookup(context(), key, v))
            return true;
    }
    return false;
}

static bool invertCondition(const Variant::Read &condition, bool truth)
{
    if (condition.hash("Invert").toBool())
        return !truth;
    return truth;
}

Variant::Read ValueLookup::conditionLookup(const Variant::Read &config)
{
    switch (config.getType()) {
    case Variant::Type::String:
        return lookup(config);
    default:
        break;
    }
    if (config["Constant"].exists())
        return config["Constant"];
    return lookup(config["Key"], config["Convert"]);
}

bool ValueLookup::isTrue(const Variant::Read &condition)
{
    switch (condition.getType()) {
    case Variant::Type::Boolean:
        return condition.toBool();
    case Variant::Type::Empty:
        return true;
    case Variant::Type::String:
        return conditionLookup(condition).exists();
    default:
        break;
    }

    const auto &type = condition.hash("Type").toString();

    if (Util::equal_insensitive(type, "always")) {
        return invertCondition(condition, true);
    } else if (Util::equal_insensitive(type, "never")) {
        return invertCondition(condition, false);
    } else if (Util::equal_insensitive(type, "or", "any")) {
        for (auto child : condition.hash("Components").toArray()) {
            if (isTrue(child))
                return invertCondition(condition, true);
        }
        return invertCondition(condition, false);
    } else if (Util::equal_insensitive(type, "and", "all")) {
        for (auto child : condition.hash("Components").toArray()) {
            if (!isTrue(child))
                return invertCondition(condition, false);
        }
        return invertCondition(condition, true);
    } else if (Util::equal_insensitive(type, "less", "lessthan")) {
        double lhs = conditionLookup(condition.hash("Left")).toDouble();
        double rhs = conditionLookup(condition.hash("Right")).toDouble();
        if (!FP::defined(lhs) || !FP::defined(rhs))
            return invertCondition(condition, false);
        return invertCondition(condition, lhs < rhs);
    } else if (Util::equal_insensitive(type, "lessequal", "lessthanorequal")) {
        double lhs = conditionLookup(condition.hash("Left")).toDouble();
        double rhs = conditionLookup(condition.hash("Right")).toDouble();
        if (!FP::defined(lhs) || !FP::defined(rhs))
            return invertCondition(condition, false);
        return invertCondition(condition, lhs <= rhs);
    } else if (Util::equal_insensitive(type, "greater", "greaterthan")) {
        double lhs = conditionLookup(condition.hash("Left")).toDouble();
        double rhs = conditionLookup(condition.hash("Right")).toDouble();
        if (!FP::defined(lhs) || !FP::defined(rhs))
            return invertCondition(condition, false);
        return invertCondition(condition, lhs > rhs);
    } else if (Util::equal_insensitive(type, "greaterequal", "greaterthanorequal")) {
        double lhs = conditionLookup(condition.hash("Left")).toDouble();
        double rhs = conditionLookup(condition.hash("Right")).toDouble();
        if (!FP::defined(lhs) || !FP::defined(rhs))
            return invertCondition(condition, false);
        return invertCondition(condition, lhs >= rhs);
    } else if (Util::equal_insensitive(type, "exact", "exactly", "numberequal")) {
        double lhs = conditionLookup(condition.hash("Left")).toDouble();
        double rhs = conditionLookup(condition.hash("Right")).toDouble();
        if (!FP::defined(lhs) || !FP::defined(rhs))
            return invertCondition(condition, false);
        return invertCondition(condition, lhs == rhs);
    } else if (Util::equal_insensitive(type, "equal")) {
        return invertCondition(condition, conditionLookup(condition.hash("Left")) ==
                conditionLookup(condition.hash("Right")));
    } else if (Util::equal_insensitive(type, "text", "stringequal")) {
        QString lhs(conditionLookup(condition.hash("Left")).toQString().trimmed().toLower());
        QString rhs(conditionLookup(condition.hash("Right")).toQString().trimmed().toLower());
        return invertCondition(condition, lhs == rhs);
    } else if (Util::equal_insensitive(type, "exists", "has")) {
        return invertCondition(condition, has(condition.hash("Value").hash("Key")));
    } else if (Util::equal_insensitive(type, "script")) {
        return invertCondition(condition,
                               context()->evaluateCondition(condition.hash("Code").toQString(),
                                                            this, condition));
    }

    auto result = conditionLookup(condition.hash("Value"));
    switch (result.getType()) {
    case Variant::Type::Real:
        return invertCondition(condition, FP::defined(result.toReal()));
    case Variant::Type::Integer:
        return invertCondition(condition, INTEGER::defined(result.toInteger()));
    default:
        break;
    }
    return invertCondition(condition, result.exists());
}

ValueLookup *ValueLookup::clone() const
{ return new ValueLookup(*this); }

ValueLookup::ValueLookup(const ValueLookup &other) : QObject(), layers(other.layers), ctx(other.ctx)
{ }

ValueLookupContext *ValueLookup::context()
{ return &ctx; }


}
}
