/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3FILEINPUTEBAS_H
#define CPD3FILEINPUTEBAS_H

#include "core/first.hxx"

#include <QtGlobal>

#include "fileinput/fileinput.hxx"
#include "fileinput/inputengine.hxx"
#include "fileinput/record.hxx"
#include "fileinput/valuelookup.hxx"
#include "fileinput/variableset.hxx"
#include "datacore/segment.hxx"

namespace CPD3 {
namespace Input {

class EBASEngine;

/**
 * The specialization of the value lookup context used in EBAS records.
 */
class CPD3FILEINPUT_EXPORT EBASValueLookupContext : public ValueLookupContext {
public:
    EBASValueLookupContext(EBASEngine *engine = NULL);

    EBASValueLookupContext(const EBASValueLookupContext &other);

    virtual ~EBASValueLookupContext();

    Data::Variant::Read convert(const Data::Variant::Read &input,
                                const Data::Variant::Read &converter,
                                bool defined = true) override;
};

/**
 * The specialization of the value lookup used in EBAS records.
 */
class CPD3FILEINPUT_EXPORT EBASValueLookup : public ValueLookup {
    EBASValueLookupContext ctx;
public:
    EBASValueLookup(const QList<ValueLookupPointer> &layers = QList<ValueLookupPointer>(),
                    EBASEngine *engine = NULL);

    virtual ~EBASValueLookup();

    ValueLookup *clone() const override;

    ValueLookupContext *context() override;

protected:
    EBASValueLookup(const EBASValueLookup &other);
};

/**
 * The specialization of a record used in EBAS files.
 */
class CPD3FILEINPUT_EXPORT EBASRecord : public Record {
    ValueLookup *global;
public:
    EBASRecord(ValueLookup *global);

    virtual ~EBASRecord();

protected:
    RecordVariable *createVariable() override;
};

/**
 * The specialization of a record variable used in EBAS files.
 */
class CPD3FILEINPUT_EXPORT EBASRecordVariable : public RecordVariable {
public:
    EBASRecordVariable(EBASRecord *record, ValueLookup *lookup);

    virtual ~EBASRecordVariable();
};

/**
 * The specialization of a variable set used in EBAS files.
 */
class CPD3FILEINPUT_EXPORT EBASVariableSet : public VariableSet {
public:
    EBASVariableSet(const Data::Variant::Read &config);

    virtual ~EBASVariableSet();

protected:
    OutputVariable *createOutput(const Data::Variant::Read &config,
                                 RecordVariable *variable) override;
};

/**
 * The specialization of an output variable set used in EBAS files.
 */
class CPD3FILEINPUT_EXPORT EBASOutputSet : public OutputSet {
    QObject *scriptParent;
public:
    EBASOutputSet(const Data::Variant::Read &config);

    virtual ~EBASOutputSet();

protected:
    VariableSet *createVariableSet(const Data::Variant::Read &config) override;
};

/**
 * The main engine for handling EBAS data.
 */
class CPD3FILEINPUT_EXPORT EBASEngine : public InputLineHeaders {
    std::unique_ptr<ValueLookup> global;
    QHash<QString, EBASRecord *> records;
    QList<QHash<QString, EBASRecordVariable *> > variables;
    QStringList mvcs;
    QList<QSet<QString> > flagsFor;
    QStringList priorValues;

    double referenceTime;
    int idxStartDOY;
    int idxEndDOY;
    double timeRoundingBase;

    Data::SequenceSegment::Transfer inputConfig;
    Data::SequenceName inputConfigUnit;
    QString inputConfigPath;
    Data::Variant::Root configuration;

    QString defaultStation;
    QString defaultArchive;
    QString defaultSuffix;

    std::unique_ptr<EBASOutputSet> outputSet;
    bool fileLockedCompleted;
    CPD3::Data::SequenceValue::Transfer completedOutput;

    bool interpretAuxiliaryComment(QString key, const QString &value);

    void prepareOutputSet();

public:
    /**
     * Create the EBAS conversion engine.
     * 
     * @param config    the configuration
     * @param unit      the configuration unit
     * @param path      the path within the configuration
     */
    EBASEngine(const Data::SequenceSegment::Transfer &config, const Data::SequenceName &unit,
               const QString &path = QString());

    virtual ~EBASEngine();

    /**
     * Set the station used by default.  This can still be overridden by
     * the configuration.
     * 
     * @param station   the station
     */
    inline void setStation(const QString &station)
    { defaultStation = station; }

    /**
     * Set the archive used by default.  This can still be overridden by
     * the configuration.
     * 
     * @param archive   the archive
     */
    inline void setArchive(const QString &archive)
    { defaultArchive = archive; }

    /**
     * Set the suffix used by default.  This can still be overridden by
     * the configuration.
     * 
     * @param suffix    the suffix
     */
    inline void setSuffix(const QString &suffix)
    { defaultSuffix = suffix; }

    Data::SequenceName::Set predictedOutputs() override;

protected:
    bool processHeaders(QStringList &lines) override;

    void processData(QStringList &lines, bool ended) override;

    bool completeFile() override;
};

}
}

#endif
