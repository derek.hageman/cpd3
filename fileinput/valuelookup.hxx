/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3FILEINPUTEVALUELOOKUP_H
#define CPD3FILEINPUTEVALUELOOKUP_H

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QObject>

#include "fileinput/fileinput.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/variant/composite.hxx"
#include "luascript/engine.hxx"

namespace CPD3 {
namespace Input {

class ValueLookup;

/**
 * A context provided for all value lookups.
 */
class CPD3FILEINPUT_EXPORT ValueLookupContext {
public:
    class Substitutions : public Data::Variant::Composite::SubstitutionStack {
        ValueLookupContext *parent;
        Data::Variant::Read context;
    public:
        Substitutions(ValueLookupContext *parent);

        Substitutions(ValueLookupContext *parent, const Substitutions &other);

        virtual ~Substitutions();

        QString apply(const QString &input, const Data::Variant::Read &context);

    protected:
        virtual QString literalCheck(QStringRef &key) const;

        virtual QString literalSubstitution(const QStringRef &key, const QString &close) const;
    };

    void prepareScript();

private:
    Substitutions substitutions;
    ValueLookup *reference;
    std::unique_ptr<Lua::Engine> scriptEngine;

public:
    /**
     * Create a new context.
     * 
     * @param scriptParent the parent object for the script environment
     */
    ValueLookupContext();

    ValueLookupContext(const ValueLookupContext &other);

    virtual ~ValueLookupContext();

    /**
     * Apply conversion.
     * 
     * @param input     the input value
     * @param converter the conversion
     * @param defined   true if the value is defined
     * @return          the converted value
     */
    virtual Data::Variant::Read convert(const Data::Variant::Read &input,
                                        const Data::Variant::Read &converter,
                                        bool defined = true);

    /**
     * Get a reference to the substitution stack.
     *
     * @return the substitution stack
     */
    inline Substitutions &substitutionStack()
    { return substitutions; }

    /**
     * Get a reference to the substitution stack.
     *
     * @return the substitution stack
     */
    inline const Substitutions &substitutionsStack() const
    { return substitutions; }

    /**
     * Set the external reference the value lookup is using.
     * 
     * @param reference the reference
     */
    void setReference(ValueLookup *reference);

    /**
     * Get the external reference the value lookup is using.
     * 
     * @return the reference of NULL if none is in use
     */
    ValueLookup *getReference() const;

    /**
     * Perform a script evaluation for code inside a ${< ... >} block.
     * <br>
     * The default creates an environment if needed then sets the global
     * "VALUE" to the reference value.
     * 
     * @param code      the code to evaluate
     * @param value     the looked up input value
     * @return          the replacement
     */
    virtual QString evaluateReplacement(const QString &code, const Data::Variant::Read &value);

    /**
     * Perform script evaluation for a script converter.
     * <br>
     * The default creates an environment if needed then sets the globals
     * "VALUE" and "CONVERTER".
     * 
     * @param code      the code to evaluate
     * @param value     the looked up input value
     * @param converter the full converter configuration
     * @return          the converted value
     */
    virtual Data::Variant::Read evaluateConverter(const QString &code,
                                                  const Data::Variant::Read &value,
                                                  const Data::Variant::Read &converter);

    /**
     * Perform script evaluation for a script condition.
     * <br>
     * The default creates an environment if needed, sets the global "CONDITION"
     * and the global "lookup" to perform value lookup.
     * 
     * @param code      the code to evaluate
     * @param lookup    the lookup to perform against
     * @param condition the full condition configuration
     * @return          the condition result
     */
    virtual bool evaluateCondition(const QString &code,
                                   ValueLookup *lookup,
                                   const Data::Variant::Read &condition);
};

/**
 * A single lookup layer in the stack of values.
 */
class CPD3FILEINPUT_EXPORT ValueLookupLayer {
public:
    ValueLookupLayer();

    virtual ~ValueLookupLayer();

    /**
     * Lookup a value in this layer.
     * 
     * @param key       the key to lookup
     * @param result    the resuling value
     * @return          true if the layer contains the key
     */
    virtual bool lookup(ValueLookupContext *context,
                        const Data::Variant::Read &key,
                        Data::Variant::Root &result) = 0;

    /**
     * Get the sort priority of the layer.
     * 
     * @return  the sort priority
     */
    virtual qint64 sortPriority() const;

protected:
    /**
     * Convert a key to a string
     */
    const std::string &keyToString(ValueLookupContext *context, const Data::Variant::Read &key);
};

typedef std::shared_ptr<ValueLookupLayer> ValueLookupPointer;

/**
 * A lookup for one or more constant values.
 */
class CPD3FILEINPUT_EXPORT ValueLookupConstant : public ValueLookupLayer {
    Data::Variant::Root value;
    qint64 priority;
public:
    ValueLookupConstant(const QString &key, const Data::Variant::Read &value, qint64 priority = 0);

    virtual ~ValueLookupConstant();

    bool lookup(ValueLookupContext *context,
                const Data::Variant::Read &key,
                Data::Variant::Root &result) override;

    virtual qint64 sortPriority() const;
};


/**
 * A lookup for a set of layers.
 */
class CPD3FILEINPUT_EXPORT ValueLookup : public QObject {
Q_OBJECT

    QList<ValueLookupPointer> layers;

    ValueLookupContext ctx;

    Data::Variant::Read conditionLookup(const Data::Variant::Read &config);

public:
    /**
     * Create the lookup.
     * 
     * @param layers        the layers of the lookup
     * @param scriptParent  the script parent for the context
     */
    ValueLookup(const QList<ValueLookupPointer> &layers = {});

    virtual ~ValueLookup();

    /**
     * Add a layer to the lookup.
     * 
     * @param layer the layer to add
     */
    void add(const ValueLookupPointer &layer);

    /** @see add( const ValueLookupPointer & ) */
    inline void add(ValueLookupLayer *layer)
    { add(ValueLookupPointer(layer)); }

    /**
     * Add layers to the lookup.
     * 
     * @param layers    the layers to add
     */
    void add(const QList<ValueLookupPointer> &layers);

    /**
     * Get the layers of the lookup.
     * 
     * @return the layers
     */
    inline QList<ValueLookupPointer> getLayers() const
    { return layers; }

    /**
     * Perform a value lookup on the layers.
     * 
     * @param key       the key to lookup
     * @param convert   the conversion
     * @return          the converted value
     */
    virtual Data::Variant::Read lookup(const Data::Variant::Read &key,
                                       const Data::Variant::Read &convert = Data::Variant::Read::empty());

    /**
     * Test if the lookup has a given key
     * 
     * @param key       the key to test
     * @return          true if the lookup contains the key
     */
    virtual bool has(const Data::Variant::Read &key);

    /**
     * Test if a condition is true on the variable set.
     * 
     * @param condition the condition to check
     * @return          true if the condition passes
     */
    virtual bool isTrue(const Data::Variant::Read &condition);

    /**
     * Create a copy of the lookup
     * 
     * @return          a copy of the lookup
     */
    virtual ValueLookup *clone() const;

    /**
     * Get the context used in the general lookup functions.
     * 
     * @return a context for lookup
     */
    virtual ValueLookupContext *context();

protected:
    ValueLookup(const ValueLookup &other);
};

}
}

#endif
