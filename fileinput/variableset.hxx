/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3FILEINPUTEVARIABLESET_H
#define CPD3FILEINPUTEVARIABLESET_H

#include "core/first.hxx"

#include <unordered_set>
#include <unordered_map>
#include <QtGlobal>
#include <QObject>

#include "fileinput/fileinput.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"
#include "fileinput/valuelookup.hxx"
#include "fileinput/record.hxx"
#include "core/threading.hxx"
#include "luascript/engine.hxx"

namespace CPD3 {
namespace Input {

class VariableSet;

/**
 * A single output variable.  The base implementation just returns the input
 * value.
 */
class CPD3FILEINPUT_EXPORT OutputVariable {
    Data::Variant::Root config;

    RecordVariable *input;
    QHash<QString, RecordVariable *> auxiliary;

    Data::SequenceName unit;

    friend class VariableSet;

public:
    /**
     * Create the output variable.
     * 
     * @param config        the configuration
     * @param input         the bound input
     */
    OutputVariable(const Data::Variant::Read &config, RecordVariable *input);

    virtual ~OutputVariable();

    /**
     * Add an auxiliary input to the the output
     * 
     * @param name          the name string of the input
     * @param variable      the input variable associated with the name
     */
    virtual void addAuxiliary(const QString &name, RecordVariable *variable);

    /**
     * Called at the beginning of value production with the absolute
     * limits of the data being produced.  This is normally used to
     * generate metadata.
     * 
     * @param start         the absolute start time of the output
     * @param end           the absolute end time of the output
     */
    virtual Data::SequenceValue::Transfer produceBegin(double start, double end);

    /**
     * Called for each time the output has a value in the merged data.
     * 
     * @param start         the effective start time
     * @param end           the effective end time
     * @param input         the value of the primary input
     * @param auxiliary     the values of the auxiliary inputs
     */
    virtual Data::SequenceValue::Transfer produceOutput(double start,
                                                        double end,
                                                        const Data::Variant::Read &input,
                                                        const std::unordered_map<std::string,
                                                                                 Data::Variant::Read> &auxiliary);

    /**
     * Get a unique key used for wavelength code assignment.  Variables
     * within the same group key are assigned unique wavelength codes, while
     * those using different ones receive different codes.
     * 
     * @return  the wavelength assignment group
     */
    virtual QString wavelengthAssignmentGroup();

    /**
     * Get the predicted outputs from the variable.
     * 
     * @return the predicted outputs
     */
    virtual Data::SequenceName::Set predictedOutputs();

    /**
     * Generate the unit of the data.
     * 
     * @return              the output unit
     */
    Data::SequenceName getUnit() const;

    /**
     * Get the bound input variable.
     * 
     * @return the input
     */
    inline RecordVariable *getInput()
    { return input; }

    /**
     * Get the input's lookup.
     * 
     * @return the lookup
     */
    inline ValueLookup *getLookup()
    { return input->getLookup(); }

protected:
    /**
     * Process the inputs into an output value.
     * 
     * @param input         the primary input
     * @param auxiliary     the auxiliary values
     * @return              the output value
     */
    virtual Data::Variant::Root processInput(const Data::Variant::Read &input,
                                             const std::unordered_map<std::string,
                                                                      Data::Variant::Read> &auxiliary);

    /**
     * Process the metadata of the output.
     * 
     * @return              the metadata for the output
     */
    virtual Data::Variant::Root processMetadata();
};

/**
 * An output variable representing a single real number.
 */
class CPD3FILEINPUT_EXPORT OutputVariableNumber : public OutputVariable {
    Calibration cal;
public:
    /**
     * Create the output variable.
     * 
     * @param config        the configuration
     * @param input         the bound input
     */
    OutputVariableNumber(const Data::Variant::Read &config, RecordVariable *input);

    virtual ~OutputVariableNumber();

protected:
    Data::Variant::Root processInput(const Data::Variant::Read &input,
                                     const std::unordered_map<std::string,
                                                              Data::Variant::Read> &auxiliary) override;
};

/**
 * An output variable that applies script processing.
 */
class CPD3FILEINPUT_EXPORT OutputVariableScript : public OutputVariable {
    Lua::Engine engine;
    Lua::Engine::Frame root;
    Lua::Engine::Reference environment;
    Lua::Engine::Reference invoke;
public:
    /**
     * Create the output variable.
     * 
     * @param config        the configuration
     * @param input         the bound input
     */
    OutputVariableScript(const Data::Variant::Read &config, RecordVariable *input);

    virtual ~OutputVariableScript();

protected:
    Data::Variant::Root processInput(const Data::Variant::Read &input,
                                     const std::unordered_map<std::string,
                                                              Data::Variant::Read> &auxiliary) override;
};

/**
 * A set of variables.
 */
class CPD3FILEINPUT_EXPORT VariableSet {
    Data::Variant::Root config;
    QList<OutputVariable *> variables;
    QList<RecordVariable *> seenVariables;

    QList<ValueLookupContext *> initializeSubstitutions();

public:
    /**
     * Create the variable set.
     * 
     * @param config    the configuration of the variable set
     */
    VariableSet(const Data::Variant::Read &config);

    virtual ~VariableSet();

    /**
     * Set if the variable set is active given a global lookup and
     * active dependencies set.
     * 
     * @param global        the global lookup
     * @param dependencies  the currently active dependency strings
     * @return              true if the variable set is active
     */
    virtual bool isActive(ValueLookup *global, CPD3::Data::Variant::Flags &dependencies);

    /**
     * Integrate a record into the variable set.
     * <br>
     * The record and all its variables must remain valid for the remainder
     * of the set's lifetime.
     * 
     * @param record        the record to add
     */
    virtual void integrateRecord(Record *record);

    /**
     * Calculate all output of the variable set.
     * 
     * @return              the output of the variable set
     */
    virtual Data::SequenceValue::Transfer produceOutput();

    /**
     * Get the predicted outputs from the set.
     * 
     * @return the predicted outputs
     */
    virtual Data::SequenceName::Set predictedOutputs();

    /**
     * Get the sort priority of the variable set (for dependency handling).
     * 
     * @return          the sort priority, undefined comes first
     */
    virtual qint64 sortPriority() const;

protected:
    /**
     * Test if the variable set applies to a variable as a primary initiator.
     * 
     * @param variable  the variable
     * @return          true if the variable creates an entry in the set
     */
    virtual bool appliesTo(RecordVariable *variable);

    /**
     * Create the output for a variable or NULL if it doesn't have an
     * output.
     * 
     * @param config        the configuration
     * @param variable      the variable
     * @return              the output or NULL for none
     */
    virtual OutputVariable *createOutput(const Data::Variant::Read &config,
                                         RecordVariable *variable);

    /**
     * Get the auxiliary name for a newly created output.
     * 
     * @param target        the target variable
     * @param input         the input variable to check
     * @return              the auxiliary target name or empty for none
     */
    virtual QString auxiliaryInput(OutputVariable *target, RecordVariable *input);
};

/**
 * A set of all outputs.
 */
class CPD3FILEINPUT_EXPORT OutputSet {
    Data::Variant::Root config;
    QList<VariableSet *> sets;
public:
    /**
     * Create a set of outputs.
     * 
     * @param config            the configuration
     */
    OutputSet(const Data::Variant::Read &config);

    virtual ~OutputSet();

    /**
     * Activate the output set.
     * 
     * @param global        the global lookup
     */
    virtual void activate(ValueLookup *global);

    /**
     * Integrate a record and all its variables into the output set.
     * <br>
     * The record and all its variables must remain valid for the remainder
     * of the set's lifetime.
     * 
     * @param record        the record to add
     */
    virtual void integrateRecord(Record *record);

    /**
     * Produce all output from the set.
     * 
     * @return              the set's output
     */
    virtual Data::SequenceValue::Transfer produceOutput();

    /**
     * Get the predicted outputs from the set.
     * 
     * @return the predicted outputs
     */
    virtual Data::SequenceName::Set predictedOutputs();

protected:
    /**
     * Create a variable set from the given configuration.
     * 
     * @param config        the set configuration
     * @return              a new variable set
     */
    virtual VariableSet *createVariableSet(const Data::Variant::Read &config);
};


}
}

#endif
