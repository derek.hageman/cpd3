/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "fileinput/variableset.hxx"
#include "fileinput/inputengine.hxx"
#include "core/environment.hxx"
#include "datacore/wavelength.hxx"
#include "datacore/variant/composite.hxx"
#include "luascript/libs/variant.hxx"


Q_LOGGING_CATEGORY(log_fileinput_variableset, "cpd3.fileinput.variableset", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Input {

/** @file fileinput/variableset.hxx
 * Variable set handling.
 */

OutputVariable::OutputVariable(const Variant::Read &cfg, RecordVariable *in) : config(cfg),
                                                                               input(in),
                                                                               auxiliary(),
                                                                               unit()
{ }

OutputVariable::~OutputVariable()
{ }

void OutputVariable::addAuxiliary(const QString &name, RecordVariable *variable)
{ auxiliary.insert(name, variable); }

SequenceValue::Transfer OutputVariable::produceBegin(double start, double end)
{
    unit = getUnit();
    if (!unit.isValid())
        return SequenceValue::Transfer();
    Variant::Root meta = processMetadata();
    if (!meta.read().exists())
        return SequenceValue::Transfer();
    SequenceValue::Transfer result;
    result.emplace_back(SequenceIdentity(unit.toMeta(), start, end), std::move(meta));
    return result;
}

SequenceValue::Transfer OutputVariable::produceOutput(double start,
                                                      double end,
                                                      const Variant::Read &input,
                                                      const std::unordered_map<std::string,
                                                                               Variant::Read> &auxiliary)
{
    if (!unit.isValid())
        return SequenceValue::Transfer();
    return SequenceValue::Transfer{
            SequenceValue({unit, start, end}, processInput(input, auxiliary))};
}

static Variant::Read lookupDefaultPath(ValueLookup *lookup,
                                       const Data::Variant::Read &config,
                                       const QString &defaultPath)
{
    Variant::Read key = config["Key"];
    if (!key.exists())
        key = Variant::Root(defaultPath).read();
    return lookup->lookup(key, config["Convert"]);
}

SequenceName OutputVariable::getUnit() const
{
    return SequenceName(
            lookupDefaultPath(input->getLookup(), config["Station"], "Station").toString(),
            lookupDefaultPath(input->getLookup(), config["Archive"], "Archive").toString(),
            lookupDefaultPath(input->getLookup(), config["Variable"], "Variable").toString(),
            SequenceName::toFlavors(
                    lookupDefaultPath(input->getLookup(), config["Flavors"], "Flavors")));
}

QString OutputVariable::wavelengthAssignmentGroup()
{
    QString result(lookupDefaultPath(input->getLookup(), config["Station"], "Station").toQString()
                                                                                      .toLower());
    result.append(':');
    result.append(lookupDefaultPath(input->getLookup(), config["Archive"], "Archive").toQString()
                                                                                     .toLower());
    result.append(':');
    result.append(
            lookupDefaultPath(input->getLookup(), config["Variable"], "Variable").toQString());
    result.append(':');

    QSet<QString> unique;
    for (const auto &add : SequenceName::toFlavors(
            lookupDefaultPath(input->getLookup(), config["Flavors"], "Flavors"))) {
        unique.insert(QString::fromStdString(add).toLower());
    }
    QStringList sorted(unique.values());
    std::sort(sorted.begin(), sorted.end());
    result.append(sorted.join(":"));
    return result;
}

SequenceName::Set OutputVariable::predictedOutputs()
{
    return SequenceName::Set{getUnit()};
}

Variant::Root OutputVariable::processInput(const Variant::Read &input,
                                           const std::unordered_map<std::string, Variant::Read> &)
{ return Variant::Root(input); }

Variant::Root OutputVariable::processMetadata()
{
    struct OutputMetadataOverlay {
        qint64 priority;
        std::string path;
        Variant::Read value;

        bool operator<(const OutputMetadataOverlay &other) const
        {
            if (!INTEGER::defined(priority)) {
                if (!INTEGER::defined(other.priority))
                    return false;
                return true;
            } else if (!INTEGER::defined(other.priority)) {
                return false;
            }
            return priority < other.priority;
        }
    };

    std::vector<OutputMetadataOverlay> toMerge;
    for (auto c : config["Metadata/Overlay"].toHash()) {
        if (c.first.empty())
            continue;
        if (!input->getLookup()->isTrue(c.second["Match"]))
            continue;

        OutputMetadataOverlay mo;
        mo.priority = c.second["Priority"].toInt64();
        if (c.second["Path"].exists())
            mo.path = c.second["Path"].toString();
        else
            mo.path = c.first;
        if (c.second["Constant"].exists()) {
            mo.value = c.second["Constant"];
        } else {
            mo.value = input->getLookup()->lookup(c.second["Key"], c.second["Convert"]);
        }
        toMerge.emplace_back(std::move(mo));
    }
    std::sort(toMerge.begin(), toMerge.end());

    std::vector<Variant::Root> mergeList;
    mergeList.emplace_back(config["Metadata/Base"]);
    for (const auto &mo : toMerge) {
        Variant::Root add(mergeList.front().read().getType());
        add[mo.path].set(mo.value);
        mergeList.emplace_back(std::move(add));
    }

    auto meta = Variant::Root::overlay(mergeList.begin(), mergeList.end());

    if (meta.read().isMetadata()) {
        auto processing = meta.write().metadata("Processing").toArray().after_back();
        processing.hash("By").setString("fileinput");
        processing.hash("At").setDouble(Time::time());
        processing.hash("Environment").setString(Environment::describe());
        processing.hash("Revision").setString(Environment::revision());
    }

    return meta;
}

OutputVariableNumber::OutputVariableNumber(const Variant::Read &config, RecordVariable *input)
        : OutputVariable(config, input),
          cal(Variant::Composite::toCalibration(config["Calibration"]))
{ }

OutputVariableNumber::~OutputVariableNumber() = default;

Variant::Root OutputVariableNumber::processInput(const Variant::Read &input,
                                                 const std::unordered_map<std::string,
                                                                          Variant::Read> &)
{ return Variant::Root(cal.apply(input.toDouble())); }

OutputVariableScript::OutputVariableScript(const Variant::Read &config, RecordVariable *input)
        : OutputVariable(config, input), engine(), root(engine)
{
    environment = root.pushSandboxEnvironment();
    {
        Lua::Engine::Assign assign(root, environment, "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_fileinput_variableset) << entry[i].toOutputString();
            }
        }));
    }
    if (!root.pushChunk(config["Code"].toString(), environment)) {
        qCDebug(log_fileinput_variableset) << "Error parsing output variable script:"
                                           << root.errorDescription();
        root.clearError();
        return;
    }
    invoke = root.back();
}

OutputVariableScript::~OutputVariableScript() = default;

Variant::Root OutputVariableScript::processInput(const Variant::Read &input,
                                                 const std::unordered_map<std::string,
                                                                          Variant::Read> &auxiliary)
{
    if (!invoke.isAssigned())
        return Variant::Root();

    {
        Lua::Engine::Assign assign(root, environment, "VALUE");
        assign.pushData<Lua::Libs::Variant>(Variant::Root(input).write());
    }
    for (const auto &add : auxiliary) {
        Lua::Engine::Assign assign(root, environment, add.first);
        assign.pushData<Lua::Libs::Variant>(Variant::Root(add.second).write());
    }

    Lua::Engine::Call call(root, invoke);
    if (!call.execute(1)) {
        engine.clearError();
        return Variant::Root();
    }

    return Variant::Root(Lua::Libs::Variant::extract(call, call.back()));
}

VariableSet::VariableSet(const Variant::Read &cfg) : config(cfg), variables()
{ }

VariableSet::~VariableSet()
{
    qDeleteAll(variables);
}

namespace Internal {
struct MergeOutputRecord : Time::Bounds {
    std::vector<Data::Variant::Read> values;
    QHash<RecordVariable *, int> indices;

    MergeOutputRecord(const Record::DefinedTime &record, const QHash<RecordVariable *, int> &idx)
            : Time::Bounds(record.getStart(), record.getEnd()), values(), indices(idx)
    {
        for (const auto &add : record.values) {
            this->values.emplace_back(add);
        }
    }
};

struct MergedOutputData : Time::Bounds {
    QHash<RecordVariable *, Data::Variant::Read> values;

    void mergeIndices(const MergeOutputRecord &merge)
    {
        for (QHash<RecordVariable *, int>::const_iterator idx = merge.indices.constBegin(),
                endIdx = merge.indices.constEnd(); idx != endIdx; ++idx) {
            int i = idx.value();
            if (i < 0 || i >= static_cast<int>(merge.values.size()))
                continue;
            values[idx.key()] = merge.values[i];
        }
    }

    MergedOutputData(const MergedOutputData &other, double start, double end) : Time::Bounds(start,
                                                                                             end),
                                                                                values(other.values)
    { }

    MergedOutputData(const MergeOutputRecord &other, double start, double end) : Time::Bounds(start,
                                                                                              end)
    {
        mergeIndices(other);
    }

    MergedOutputData(const MergedOutputData &under,
                     const MergeOutputRecord &over,
                     double start,
                     double end) : Time::Bounds(start, end), values(under.values)
    {
        mergeIndices(over);
    }
};
}

template<typename T>
static uint qHash(const QSet<T *> &set)
{
    uint hv = 0;
    for (typename QSet<T *>::const_iterator i = set.constBegin(), endI = set.constEnd();
            i != endI;
            ++i) {
        hv = INTEGER::rotl(hv, (uint) 8);
        hv ^= (uint) ((quintptr) (*i));
    }
    return hv;
}

QList<ValueLookupContext *> VariableSet::initializeSubstitutions()
{
    QList<ValueLookupContext *> result;

    /* Activate per-variable substitutions */
    QHash<QString, QMap<double, QList<OutputVariable *> > > wavelengthLookup;
    for (QList<OutputVariable *>::const_iterator ov = variables.constBegin(),
            endVariables = variables.constEnd(); ov != endVariables; ++ov) {
        ValueLookup *lookup = (*ov)->input->getLookup();
        ValueLookupContext *ctx = lookup->context();

        for (auto c : config["Lookup"].toHash()) {
            if (!lookup->isTrue(c.second["Match"]))
                continue;
            qint64 priority = c.second["Priority"].toInt64();
            if (!INTEGER::defined(priority))
                priority = 0;

            Variant::Write add = Variant::Write::empty();
            if (c.second["Constant"].exists()) {
                add.set(c.second["Constant"]);
            } else {
                add.set((*ov)->input->getLookup()->lookup(c.second["Key"], c.second["Convert"]));
            }
            lookup->add(new ValueLookupConstant(QString::fromStdString(c.first), add, priority));
        }

        ctx->substitutionStack().push();

        for (auto c : config["Substitutions"].toHash()) {
            if (!lookup->isTrue(c.second["Match"]))
                continue;
            if (c.second["Constant"].exists()) {
                ctx->substitutionStack()
                   .setVariant(QString::fromStdString(c.first), c.second["Constant"]);
            } else {
                ctx->substitutionStack()
                   .setVariant(QString::fromStdString(c.first), (*ov)->input
                                                                     ->getLookup()
                                                                     ->lookup(c.second["Key"],
                                                                              c.second["Convert"]));
            }
        }

        result.append(ctx);


        double wl = lookupDefaultPath(lookup, config["Wavelength"], "Wavelength").toReal();
        if (FP::defined(wl)) {
            /* Explicitly set it so the group will be unique */
            ctx->substitutionStack().push();
            ctx->substitutionStack().setString("WavelengthCode", "-");
            result.append(ctx);

            wavelengthLookup[(*ov)->wavelengthAssignmentGroup()][wl].append(*ov);
        }
    }

    for (QHash<QString, QMap<double, QList<OutputVariable *> > >::const_iterator
            group = wavelengthLookup.constBegin(), endGroup = wavelengthLookup.constEnd();
            group != endGroup;
            ++group) {
        /* Assign wavelength codes */
        bool canUseWavelengthCodes = true;
        {
            QSet<QString> usedCodes;
            for (QMap<double, QList<OutputVariable *> >::const_iterator
                    wl = group.value().constBegin(), endWL = group.value().constEnd();
                    wl != endWL;
                    ++wl) {
                if (wl.value().size() > 1) {
                    canUseWavelengthCodes = false;
                    break;
                }
                auto code = QString::fromStdString(Wavelength::code(wl.key()));
                if (code.isEmpty()) {
                    canUseWavelengthCodes = false;
                    break;
                }
                if (usedCodes.contains(code)) {
                    canUseWavelengthCodes = false;
                    break;
                }
                usedCodes.insert(code);
            }
        }
        int wavelengthCounter = 1;
        for (QMap<double, QList<OutputVariable *> >::const_iterator wl = group.value().constBegin(),
                endWL = group.value().constEnd(); wl != endWL; ++wl) {
            for (QList<OutputVariable *>::const_iterator ov = wl.value().constBegin(),
                    endOV = wl.value().constEnd(); ov != endOV; ++ov) {
                QString code;
                if (canUseWavelengthCodes) {
                    code = QString::fromStdString(Wavelength::code(wl.key()));
                    Q_ASSERT(!code.isEmpty());
                } else {
                    code = QString::number(wavelengthCounter);
                }
                ++wavelengthCounter;

                ValueLookupContext *ctx = (*ov)->getLookup()->context();
                ctx->substitutionStack().push();
                ctx->substitutionStack().setString("WavelengthCode", code);
                result.append(ctx);
            }
        }
    }

    return result;
}

SequenceValue::Transfer VariableSet::produceOutput()
{
    QList<ValueLookupContext *> popCtx(initializeSubstitutions());

    /* Map output variables to all the records they use */
    QHash<OutputVariable *, QSet<Record *> > outputMapping;
    for (QList<OutputVariable *>::const_iterator ov = variables.constBegin(),
            end = variables.constEnd(); ov != end; ++ov) {
        outputMapping[*ov] |= (*ov)->input->getRecord();

        for (QHash<QString, RecordVariable *>::const_iterator a = (*ov)->auxiliary.constBegin(),
                endA = (*ov)->auxiliary.constEnd(); a != endA; ++a) {
            outputMapping[*ov] |= a.value()->getRecord();
        }
    }

    /* Now the reverse mapping of sets of records to output variable lookups */
    QHash<QSet<Record *>, QSet<OutputVariable *> > outputReverseMapping;
    QHash<QSet<Record *>, QHash<Record *, QHash<RecordVariable *, int> > > outputSetMapping;
    for (QHash<OutputVariable *, QSet<Record *> >::const_iterator ms = outputMapping.constBegin(),
            endMS = outputMapping.constEnd(); ms != endMS; ++ms) {
        outputReverseMapping[ms.value()] |= ms.key();

        outputSetMapping[ms.value()][ms.key()->input->getRecord()][ms.key()->input] =
                ms.key()->input->getIndex();

        for (QHash<QString, RecordVariable *>::const_iterator a = ms.key()->auxiliary.constBegin(),
                endA = ms.key()->auxiliary.constEnd(); a != endA; ++a) {
            outputSetMapping[ms.value()][a.value()->getRecord()][a.value()] = a.value()->getIndex();
        }
    }

    SequenceValue::Transfer result;

    /* Finally merge records */
    for (QHash<QSet<Record *>, QHash<Record *, QHash<RecordVariable *, int> > >::const_iterator
            sm = outputSetMapping.constBegin(), endSM = outputSetMapping.constEnd();
            sm != endSM;
            ++sm) {
        QList<Internal::MergedOutputData> merged;

        for (QHash<Record *, QHash<RecordVariable *, int> >::const_iterator
                addSet = sm.value().constBegin(), endAddSet = sm.value().constEnd();
                addSet != endAddSet;
                ++addSet) {
            /* Merge the record's output */
            QList<Record::DefinedTime> rOut(addSet.key()->output());
            for (QList<Record::DefinedTime>::const_iterator add = rOut.constBegin(),
                    endAdd = rOut.constEnd(); add != endAdd; ++add) {
                Range::overlayFragmenting(merged,
                                          Internal::MergeOutputRecord(*add, addSet.value()));
            }
        }

        if (merged.isEmpty())
            continue;

        /* Now produce output for each variable from the merged data */
        QSet<OutputVariable *> rmsm(outputReverseMapping.value(sm.key()));
        for (QSet<OutputVariable *>::const_iterator ov = rmsm.constBegin(), endRM = rmsm.constEnd();
                ov != endRM;
                ++ov) {
            Util::append((*ov)->produceBegin(merged.first().getStart(), merged.last().getEnd()),
                         result);

            for (QList<Internal::MergedOutputData>::const_iterator seg = merged.constBegin(),
                    endSeg = merged.constEnd(); seg != endSeg; ++seg) {
                std::unordered_map<std::string, Variant::Read> auxiliary;
                for (QHash<QString, RecordVariable *>::const_iterator
                        a = (*ov)->auxiliary.constBegin(), endA = (*ov)->auxiliary.constEnd();
                        a != endA;
                        ++a) {
                    auto check = seg->values.find(a.value());
                    if (check == seg->values.end())
                        continue;
                    auxiliary.emplace(a.key().toStdString(), check.value());
                }

                Util::append((*ov)->produceOutput(seg->getStart(), seg->getEnd(),
                                                  seg->values.value((*ov)->input), auxiliary),
                             result);
            }
        }
    }

    for (QList<ValueLookupContext *>::const_iterator ctx = popCtx.constBegin(),
            end = popCtx.constEnd(); ctx != end; ++ctx) {
        (*ctx)->substitutionStack().pop();
    }

    return result;
}

SequenceName::Set VariableSet::predictedOutputs()
{
    QList<ValueLookupContext *> popCtx(initializeSubstitutions());

    SequenceName::Set result;
    for (auto add : variables) {
        Util::merge(add->predictedOutputs(), result);
    }

    for (QList<ValueLookupContext *>::const_iterator ctx = popCtx.constBegin(),
            end = popCtx.constEnd(); ctx != end; ++ctx) {
        (*ctx)->substitutionStack().pop();
    }
    return result;
}

void VariableSet::integrateRecord(Record *record)
{
    QList<RecordVariable *> recordVariables(record->getVariables());

    for (QList<OutputVariable *>::const_iterator ov = variables.constBegin(),
            endOV = variables.constEnd(); ov != endOV; ++ov) {
        for (QList<RecordVariable *>::const_iterator avar = recordVariables.constBegin(),
                endRV = recordVariables.constEnd(); avar != endRV; ++avar) {
            (*avar)->getLookup()->context()->setReference((*ov)->getLookup());
            QString name(auxiliaryInput(*ov, *avar));
            (*avar)->getLookup()->context()->setReference(NULL);
            if (name.isEmpty())
                continue;
            (*ov)->addAuxiliary(name, *avar);
        }
    }

    seenVariables.append(recordVariables);
    for (QList<RecordVariable *>::const_iterator pvar = recordVariables.constBegin(),
            endRV = recordVariables.constEnd(); pvar != endRV; ++pvar) {
        if (!appliesTo(*pvar))
            continue;
        OutputVariable *ov = createOutput(config["Data"], *pvar);
        if (ov == NULL)
            continue;

        for (QList<RecordVariable *>::const_iterator avar = seenVariables.constBegin(),
                endSV = seenVariables.constEnd(); avar != endSV; ++avar) {
            (*avar)->getLookup()->context()->setReference(ov->getLookup());
            QString name(auxiliaryInput(ov, *avar));
            (*avar)->getLookup()->context()->setReference(NULL);
            if (name.isEmpty())
                continue;
            ov->addAuxiliary(name, *avar);
        }

        variables.append(ov);
    }
}

bool VariableSet::appliesTo(RecordVariable *variable)
{
    return variable->getLookup()->isTrue(config["Match"]);
}

OutputVariable *VariableSet::createOutput(const Variant::Read &config, RecordVariable *variable)
{
    const auto &type = config["Type"].toString();

    if (Util::equal_insensitive(type, "raw", "direct")) {
        return new OutputVariable(config, variable);
    } else if (Util::equal_insensitive(type, "script")) {
        return new OutputVariableScript(config, variable);
    }

    return new OutputVariableNumber(config, variable);
}

QString VariableSet::auxiliaryInput(OutputVariable *target, RecordVariable *input)
{
    Q_UNUSED(target);

    for (auto c : config["Auxiliary"].toHash()) {
        if (c.first.empty())
            continue;
        if (!input->getLookup()->isTrue(c.second["Match"]))
            continue;
        if (c.second["Name"].exists()) {
            QString check(input->getLookup()
                               ->lookup(c.second["Name/Key"], c.second["Name/Convert"])
                               .toQString());
            if (!check.isEmpty())
                return check;
        }
        return QString::fromStdString(c.first);
    }
    return QString();
}

qint64 VariableSet::sortPriority() const
{ return config["SortPriority"].toInt64(); }

bool VariableSet::isActive(ValueLookup *global, Variant::Flags &dependencies)
{
    for (const auto &check : config["Dependencies/Require"].toFlags()) {
        if (dependencies.count(check) == 0)
            return false;
    }
    for (const auto &check : config["Dependencies/Exclude"].toFlags()) {
        if (dependencies.count(check) != 0)
            return false;
    }

    if (!global->isTrue(config["Global"]))
        return false;

    Util::merge(config["Dependencies/Set"].toFlags(), dependencies);
    for (const auto &rem : config["Dependencies/Clear"].toFlags()) {
        dependencies.erase(rem);
    }
    return true;
}


OutputSet::OutputSet(const Variant::Read &cfg) : config(cfg), sets()
{ }

OutputSet::~OutputSet()
{
    qDeleteAll(sets);
}

static bool variableSetSortCompare(const VariableSet *a, const VariableSet *b)
{
    qint64 pa = a->sortPriority();
    qint64 pb = b->sortPriority();
    if (!INTEGER::defined(pa)) {
        if (INTEGER::defined(pb))
            return true;
        return false;
    } else if (!INTEGER::defined(pb)) {
        return false;
    }
    return pa < pb;
}

void OutputSet::activate(ValueLookup *global)
{
    for (auto c : config["Groups"].toHash()) {
        VariableSet *set = createVariableSet(c.second);
        if (set == NULL)
            continue;
        sets.append(set);
    }
    std::stable_sort(sets.begin(), sets.end(), variableSetSortCompare);

    Variant::Flags
    dependencies;
    for (QList<VariableSet *>::iterator vs = sets.begin(); vs != sets.end();) {
        if ((*vs)->isActive(global, dependencies)) {
            ++vs;
            continue;
        }
        delete *vs;
        vs = sets.erase(vs);
    }
}

void OutputSet::integrateRecord(Record *record)
{
    for (QList<VariableSet *>::const_iterator set = sets.constBegin(), end = sets.constEnd();
            set != end;
            ++set) {
        (*set)->integrateRecord(record);
    }
}

SequenceValue::Transfer OutputSet::produceOutput()
{
    SequenceValue::Transfer result;
    for (auto set : sets) {
        Util::append(set->produceOutput(), result);
    }
    std::sort(result.begin(), result.end(), SequenceIdentity::OrderTime());
    return result;
}

SequenceName::Set OutputSet::predictedOutputs()
{
    SequenceName::Set result;
    for (auto add : sets) {
        Util::merge(add->predictedOutputs(), result);
    }
    return result;
}

VariableSet *OutputSet::createVariableSet(const Variant::Read &cfg)
{ return new VariableSet(cfg); }

}
}
