/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3FILEINPUTERECORD_H
#define CPD3FILEINPUTERECORD_H

#include "core/first.hxx"

#include <QtGlobal>

#include "fileinput/fileinput.hxx"
#include "datacore/variant/root.hxx"
#include "fileinput/valuelookup.hxx"
#include "core/timeutils.hxx"

namespace CPD3 {
namespace Input {

class Record;

/**
 * A variable within a record.
 */
class CPD3FILEINPUT_EXPORT RecordVariable {
    Record *record;
    ValueLookup *lookup;

    int index;

    friend class Record;

public:
    RecordVariable(Record *record, ValueLookup *lookup);

    virtual ~RecordVariable();

    /**
     * Get the value lookup for the variable.
     * 
     * @return the value lookup
     */
    inline ValueLookup *getLookup() const
    { return lookup; }

    /**
     * Set the value of the variable at the currently defined time.
     * 
     * @param value the value
     */
    void setValue(const Data::Variant::Read &value);

    /**
     * Get the originating record.
     * 
     * @return the record
     */
    inline Record *getRecord() const
    { return record; }

    /**
     * Get the index within the record.
     * 
     * @return the index
     */
    inline int getIndex() const
    { return index; }
};


/**
 * A definition of a record in a file
 */
class CPD3FILEINPUT_EXPORT Record {
public:
    struct CPD3FILEINPUT_EXPORT DefinedTime : public Time::Bounds {
        std::vector<Data::Variant::Root> values;

        inline DefinedTime(double start,
                           double end,
                           const std::vector<Data::Variant::Root> &v = std::vector<
                                   Data::Variant::Root>()) : Time::Bounds(
                start, end), values(v)
        { }

        inline DefinedTime(double start, double end, std::vector<Data::Variant::Root> &&v)
                : Time::Bounds(start, end), values(std::move(v))
        { }

        inline DefinedTime(const DefinedTime &other, double start, double end) : Time::Bounds(other,
                                                                                              start,
                                                                                              end),
                                                                                 values(other.values)
        { }
    };

private:
    ValueLookup *global;
    QList<RecordVariable *> variables;
    std::deque<DefinedTime> times;

    double nominalInterval;

    void setVariable(int index, const Data::Variant::Read &value);

    friend class RecordVariable;

public:
    /**
     * Create a record definition.
     * 
     * @param global    the global lookup
     */
    Record(ValueLookup *global);

    virtual ~Record();

    /**
     * Add a variable to the record.
     * 
     * @return the variable handle
     */
    virtual RecordVariable *addVariable();

    /**
     * Get the variables in the record.
     * 
     * @return the variables
     */
    inline QList<RecordVariable *> getVariables() const
    { return variables; }


    /**
     * Define a time when the record exists.  All handles are set
     * to update this time.
     * 
     * @param start     the start time of the record or undefined for automatic
     * @param end       the end time of the record or undefined for automatic
     */
    void defineTime(double start, double end = FP::undefined());

    /**
     * Set the nominal time between records.
     * 
     * @param interval  the interval in seconds
     */
    void setNominalInterval(double interval);

    /**
     * Calculate and output the defined times the record has.
     * 
     * @return          the record defined times
     */
    QList<DefinedTime> output();

protected:
    /**
     * Create a value lookup for a variable.
     * 
     * @param global    the gloval lookup
     * @return          a value lookup for a new variable
     */
    virtual ValueLookup *createVariableLookup(const ValueLookup *global);

    /**
     * Create a new record variable.
     * 
     * @return          a new record variable
     */
    virtual RecordVariable *createVariable();

    /**
     * Get the interval applied to record times.
     * 
     * @return the interval
     */
    virtual double getRecordInterval();
};

}
}

#endif
