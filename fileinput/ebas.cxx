/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QDateTime>
#include <QLoggingCategory>

#include "fileinput/ebas.hxx"


Q_LOGGING_CATEGORY(log_fileinput_ebas, "cpd3.fileinput.ebas", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Input {

/** @file fileinput/ebas.hxx
 * EBAS style file input.
 */


EBASValueLookupContext::EBASValueLookupContext(EBASEngine *)
{ }

EBASValueLookupContext::EBASValueLookupContext(const EBASValueLookupContext &other)
        : ValueLookupContext(other)
{ }

EBASValueLookupContext::~EBASValueLookupContext()
{ }

Variant::Read EBASValueLookupContext::convert(const Variant::Read &input,
                                              const Variant::Read &converter,
                                              bool defined)
{
    if (!defined)
        return ValueLookupContext::convert(input, converter, defined);

    const auto &type = converter["Type"].toString();
    if (Util::equal_insensitive(type, "MatrixFlavors")) {
        const auto &m = input.toString();

        Variant::Flags flavors;
        if (Util::equal_insensitive(m, "pm1"))
            flavors.insert("pm1");
        else if (Util::equal_insensitive(m, "pm10"))
            flavors.insert("pm10");
        else if (Util::equal_insensitive(m, "pm25"))
            flavors.insert("pm25");
        return Variant::Root(std::move(flavors));
    } else if (Util::equal_insensitive(type, "LevelArchive")) {
        bool ok = false;
        double l = input.toQString().toDouble(&ok);
        if (ok && l > 1.0)
            return Variant::Root("avgh");
        else if (ok && l >= 0.5)
            return Variant::Root("clean");
        return Variant::Root("raw");
    }

    return ValueLookupContext::convert(input, converter, defined);
}


EBASValueLookup::EBASValueLookup(const QList<ValueLookupPointer> &layers, EBASEngine *eng)
        : ValueLookup(layers), ctx(eng)
{ }

EBASValueLookup::EBASValueLookup(const EBASValueLookup &other) : ValueLookup(other), ctx(other.ctx)
{ }

EBASValueLookup::~EBASValueLookup()
{ }

ValueLookup *EBASValueLookup::clone() const
{ return new EBASValueLookup(*this); }

ValueLookupContext *EBASValueLookup::context()
{ return &ctx; }


EBASRecord::EBASRecord(ValueLookup *g) : Record(g), global(g)
{ }

EBASRecord::~EBASRecord()
{ }

RecordVariable *EBASRecord::createVariable()
{ return new EBASRecordVariable(this, createVariableLookup(global)); }

EBASRecordVariable::EBASRecordVariable(EBASRecord *record, ValueLookup *lookup) : RecordVariable(
        record, lookup)
{ }

EBASRecordVariable::~EBASRecordVariable()
{ }


namespace {

class VariableWithStatistics : public OutputVariableNumber {
    SequenceName statsUnit;
    SequenceName coverUnit;

    typedef std::pair<std::string, double> CoverFlag;
    std::vector<CoverFlag> coverLookup;
public:
    VariableWithStatistics(const Variant::Read &config, EBASRecordVariable *input)
            : OutputVariableNumber(config, input)
    {
        coverLookup.emplace_back("EBASFlag390", 0.5);
        coverLookup.emplace_back("EBASFlag391", 0.5);
        coverLookup.emplace_back("EBASFlag392", 0.75);
        coverLookup.emplace_back("EBASFlag393", 0.75);
        coverLookup.emplace_back("EBASFlag394", 0.9);
        coverLookup.emplace_back("EBASFlag395", 0.9);
    }

    virtual ~VariableWithStatistics() = default;

protected:

    SequenceValue::Transfer produceOutput(double start,
                                          double end,
                                          const Variant::Read &input,
                                          const std::unordered_map<std::string,
                                                                   Variant::Read> &auxiliary) override
    {
        SequenceValue::Transfer
                result(OutputVariableNumber::produceOutput(start, end, input, auxiliary));

        if (!input.exists())
            return result;

        Variant::Root add;
        auto stats = add.write();
        stats.hash("Mean").setDouble(input.toDouble());

        for (const auto &v : auxiliary) {
            double dv = v.second.toDouble();
            if (!FP::defined(dv))
                continue;

            double q = 0.0;
            try {
                q = std::stod(v.first);
            } catch (std::exception &) {
                continue;
            }
            if (!FP::defined(q) || q < 0.0 || q > 100.0)
                continue;

            stats.hash("Quantiles").keyframe(q / 100.0).setDouble(dv);
        }

        if (!statsUnit.isValid())
            statsUnit = getUnit().withFlavor(SequenceName::flavor_stats);
        result.emplace_back(statsUnit, std::move(add), start, end);

        if (!coverUnit.isValid())
            coverUnit = getUnit().withFlavor(SequenceName::flavor_cover);

        auto value = auxiliary.find("Flags");
        if (value != auxiliary.end()) {
            const auto &flags = value->second.toFlags();
            if (!flags.empty()) {
                for (const auto &check : coverLookup) {
                    if (flags.count(check.first) != 0) {
                        result.emplace_back(coverUnit, Variant::Root(check.second), start, end);
                        break;
                    }
                }
            }
        }

        return result;
    }
};

class VariableFlags : public OutputVariable {
    std::unordered_map<int, Variant::Flag> translate;
    std::unordered_map<Variant::Flag, Variant::Root> metadata;
public:
    VariableFlags(const Variant::Read &config, EBASRecordVariable *input) : OutputVariable(config,
                                                                                           input)
    {
        for (auto child : config["FlagTranslation"].toHash()) {
            int flag = -1;
            try {
                flag = std::stoi(child.first);
            } catch (std::exception &) {
                continue;
            }
            if (flag < 100 || flag > 999)
                continue;
            switch (child.second.getType()) {
            case Variant::Type::String: {
                const auto &name = child.second.toString();
                if (name.empty())
                    continue;
                translate.emplace(flag, name);
                Variant::Root meta;
                meta["Origin/#0"].setString("ebas");
                metadata.emplace(name, std::move(meta));
                continue;
            }
            default: {
                const auto &name = child.second["Flag"].toString();
                if (name.empty())
                    continue;
                translate.emplace(flag, name);
                Variant::Root meta(child.second["Metadata"]);
                if (!meta["Origin/#0"].exists())
                    meta["Origin/#0"].setString("ebas");
                metadata.emplace(name, std::move(meta));
                break;
            }
            }
        }
    }

    virtual ~VariableFlags() = default;

protected:
    Variant::Root processInput(const Variant::Read &input,
                               const std::unordered_map<std::string,
                                                        Variant::Read> &auxiliary) override
    {
        Variant::Root result = OutputVariable::processInput(input, auxiliary);
        if (result.read().getType() != Variant::Type::Flags)
            return result;
        auto flags = result.read().toFlags();
        for (const auto &raw : flags) {
            if (raw.length() <= 8)
                continue;
            if (raw.compare(0, 8, "EBASFlag") != 0)
                continue;
            long flag = -1;
            try {
                flag = std::strtol(raw.c_str() + 8, nullptr, 10);
            } catch (std::exception &) {
                continue;
            }
            if (flag < 100 || flag > 999)
                continue;
            auto check = translate.find(static_cast<int>(flag));
            if (check == translate.end())
                continue;
            if (check->second.empty())
                continue;
            result.write().applyFlag(check->second);
        }
        return result;
    }

    Variant::Root processMetadata() override
    {
        Variant::Root result = OutputVariable::processMetadata();
        if (result.read().getType() != Variant::Type::MetadataFlags)
            return result;
        for (const auto &add : metadata) {
            auto m = result.write().metadataSingleFlag(add.first);
            if (!m.exists()) {
                m.set(add.second);
                continue;
            }
            m["Origin"].toArray().after_back().set(add.second["Origin/#0"]);
        }
        return result;
    }
};

class VariableNephZero : public OutputVariableNumber {
    double fraction;
public:
    VariableNephZero(const Variant::Read &config, EBASRecordVariable *input, double f = 1.0)
            : OutputVariableNumber(config, input), fraction(f)
    {
        Q_ASSERT(FP::defined(fraction));
    }

    virtual ~VariableNephZero() = default;

protected:
    Variant::Root processInput(const Variant::Read &input,
                               const std::unordered_map<std::string,
                                                        Variant::Read> &auxiliary) override
    {
        double v = input.toDouble();
        double r = FP::undefined();
        {
            static const std::string key = "Rayleigh";
            auto check = auxiliary.find(key);
            if (check != auxiliary.end())
                r = check->second.toReal();
        }
        if (!FP::defined(v) || !FP::defined(r))
            return OutputVariableNumber::processInput(Variant::Read::empty(), auxiliary);
        return OutputVariableNumber::processInput(Variant::Root(v - r * fraction), auxiliary);
    }
};

class VariableTemperature : public OutputVariableNumber {
    double offset;
public:
    VariableTemperature(const Variant::Read &config, EBASRecordVariable *input)
            : OutputVariableNumber(config, input), offset(0.0)
    {
        QString units(input->getLookup()->lookup(Variant::Root("Units")).toQString());
        if (units == "K" || units == "k" || units == QString::fromUtf8("\xC2\xB0\x4B"))
            offset = -273.15;
    }

    virtual ~VariableTemperature() = default;

protected:
    Variant::Root processInput(const Variant::Read &input,
                               const std::unordered_map<std::string,
                                                        Variant::Read> &auxiliary) override
    {
        double v = input.toDouble();
        if (FP::defined(v))
            v += offset;
        return OutputVariableNumber::processInput(Variant::Root(v), auxiliary);
    }
};

}


EBASVariableSet::EBASVariableSet(const Variant::Read &config) : VariableSet(config)
{ }

EBASVariableSet::~EBASVariableSet() = default;

OutputVariable *EBASVariableSet::createOutput(const Variant::Read &config, RecordVariable *rv)
{
    EBASRecordVariable *variable = static_cast<EBASRecordVariable *>(rv);

    const auto &type = config["Type"].toString();

    if (Util::equal_insensitive(type, "statistics")) {
        return new VariableWithStatistics(config, variable);
    } else if (Util::equal_insensitive(type, "flags")) {
        return new VariableFlags(config, variable);
    } else if (Util::equal_insensitive(type, "temperature")) {
        return new VariableTemperature(config, variable);
    } else if (Util::equal_insensitive(type, "nephzero", "nephelometerzero")) {
        return new VariableNephZero(config, variable,
                                    config["Backscattering"].toBool() ? 0.5 : 1.0);
    }

    return VariableSet::createOutput(config, variable);
}

EBASOutputSet::EBASOutputSet(const Variant::Read &config) : OutputSet(config)
{ }

EBASOutputSet::~EBASOutputSet() = default;

VariableSet *EBASOutputSet::createVariableSet(const Variant::Read &config)
{ return new EBASVariableSet(config); }


EBASEngine::EBASEngine(const SequenceSegment::Transfer &config, const SequenceName &unit,
                       const QString &path) : global(),
                                              records(),
                                              variables(),
                                              referenceTime(FP::undefined()),
                                              idxStartDOY(-1),
                                              idxEndDOY(-1),
                                              timeRoundingBase(FP::undefined()),
                                              inputConfig(config),
                                              inputConfigUnit(unit),
                                              inputConfigPath(path),
                                              configuration(),
                                              defaultStation(),
                                              defaultArchive(),
                                              defaultSuffix(),
                                              fileLockedCompleted(false)
{ }

EBASEngine::~EBASEngine()
{
    qDeleteAll(records);
}

namespace {
struct VariableHeader {
    QString mvc;
    QString amesTitle;
    QString fieldName;
};
}

static QString remapUnits(const QString &units)
{
    if (units == "C")
        return QString::fromUtf8("\xC2\xB0\x43");
    else if (units == "K")
        return QString::fromUtf8("\xC2\xB0\x4B");
    else if (units == "1/Mm")
        return QString::fromUtf8("Mm\xE2\x81\xBB¹");
    else if (units == "1/cm3")
        return QString::fromUtf8("cm\xE2\x81\xBB³");
    else if (units == "l/min")
        return QString("lpm");
    return units;
}

static void setIfNotEmpty(Variant::Write &target, const char *path, const QString &value)
{
    if (value.isEmpty())
        return;
    target.getPath(path).setString(value);
}

static double okOrUndefined(const QString &value,
                            double scale = 1.0,
                            double min = FP::undefined(),
                            double max = FP::undefined(),
                            double offset = 0.0)
{
    bool ok = false;
    double v = value.trimmed().toDouble(&ok);
    if (!ok || !FP::defined(v))
        return FP::undefined();
    v *= scale;
    v += offset;
    if (FP::defined(min) && v < min)
        return FP::undefined();
    if (FP::defined(max) && v > max)
        return FP::undefined();
    return v;
}

static double toStandardUnits(QString value)
{
    if (value.endsWith("C")) {
        value.chop(1);
        return okOrUndefined(value, -273);
    } else if (value.endsWith("K")) {
        value.chop(1);
        return okOrUndefined(value, 1.0, -273, FP::undefined(), -273.15);
    } else if (value.endsWith("hPa")) {
        value.chop(3);
        return okOrUndefined(value);
    }
    return okOrUndefined(value);
}

static double toTimeInterval(QString value)
{
    if (value.endsWith("mn")) {
        value.chop(2);
        return okOrUndefined(value, 60, 0.0);
    } else if (value.endsWith("hr")) {
        value.chop(2);
        return okOrUndefined(value, 3600, 0.0);
    } else if (value.endsWith("h")) {
        value.chop(1);
        return okOrUndefined(value, 3600, 0.0);
    } else if (value.endsWith("dy")) {
        value.chop(2);
        return okOrUndefined(value, 86400, 0.0);
    } else if (value.endsWith("d")) {
        value.chop(1);
        return okOrUndefined(value, 86400, 0.0);
    } else if (value.endsWith("mo")) {
        value.chop(1);
        return okOrUndefined(value, 30 * 86400, 0.0);
    } else if (value.endsWith("y")) {
        value.chop(1);
        return okOrUndefined(value, 365 * 86400, 0.0);
    } else if (value.endsWith("s")) {
        value.chop(1);
        return okOrUndefined(value, 1, 0.0);
    }
    return okOrUndefined(value, 1, 0.0);
}

static double toAbsoluteTime(const QString &value)
{
    if (value.length() < 14)
        return FP::undefined();
    bool ok = false;
    int year = value.mid(0, 4).toInt(&ok);
    if (!ok || year < 1970 || year > 2999)
        return FP::undefined();
    int month = value.mid(4, 2).toInt(&ok);
    if (!ok || month < 1 || month > 12)
        return FP::undefined();
    int day = value.mid(6, 2).toInt(&ok);
    if (!ok || day < 1 || day > 31)
        return FP::undefined();
    int hour = value.mid(8, 2).toInt(&ok);
    if (!ok || hour < 0 || hour > 23)
        return FP::undefined();
    int minute = value.mid(10, 2).toInt(&ok);
    if (!ok || minute < 0 || minute > 59)
        return FP::undefined();
    int second = value.mid(12, 2).toInt(&ok);
    if (!ok || second < 0 || second > 60)
        return FP::undefined();

    QDateTime dt(QDate(year, month, day), QTime(hour, minute, second), Qt::UTC);
    if (!dt.isValid())
        return FP::undefined();
    return Time::fromDateTime(dt);
}

static bool keyMatches(const QString &key, const QString &check)
{
    return key.compare(check, Qt::CaseInsensitive) == 0;
}

bool EBASEngine::interpretAuxiliaryComment(QString key, const QString &value)
{
    if (key.isEmpty())
        return true;

    if (keyMatches(key, "Station code")) {
        key = "EBAS/Station";
    } else if (keyMatches(key, "Platform code")) {
        key = "EBAS/Platform";
    } else if (keyMatches(key, "Laboratory code")) {
        key = "EBAS/Laboratory";
    } else if (keyMatches(key, "Instrument type")) {
        key = "Instrument/Type";
    } else if (keyMatches(key, "Instrument manufacturer")) {
        key = "Instrument/Manufacturer";
    } else if (keyMatches(key, "Instrument model")) {
        key = "Instrument/Model";
    } else if (keyMatches(key, "Instrument serial number")) {
        key = "Instrument/SerialNumber";

        bool ok = false;
        qint64 i = value.toLongLong(&ok, 10);
        if (ok && i > 0) {
            global->add(new ValueLookupConstant(key, Variant::Root(i)));
            return true;
        }
    } else if (keyMatches(key, "Station GAW-ID")) {
        key = "Station";
    } else if (keyMatches(key, "Data level")) {
        key = "Level";
    } else if (keyMatches(key, "Volume std. temperature")) {
        if (value.trimmed().toLower() == "ambient")
            return true;
        global->add(new ValueLookupConstant("ReportT", Variant::Root(toStandardUnits(value))));
        return true;
    } else if (keyMatches(key, "Volume std. pressure")) {
        if (value.trimmed().toLower() == "ambient")
            return true;
        global->add(new ValueLookupConstant("ReportP", Variant::Root(toStandardUnits(value))));
        return true;
    } else if (keyMatches(key, "Period code")) {
        if (value.trimmed() == "0")
            return true;
        global->add(new ValueLookupConstant("Time/Period", Variant::Root(toTimeInterval(value))));
        return true;
    } else if (keyMatches(key, "Resolution code")) {
        if (value.trimmed() == "0")
            return true;
        double resolution = toTimeInterval(value);
        timeRoundingBase = resolution;
        global->add(new ValueLookupConstant("Time/Resolution", Variant::Root(resolution)));
        return true;
    } else if (keyMatches(key, "Sample duration")) {
        if (value.trimmed() == "0")
            return true;
        global->add(new ValueLookupConstant("Time/SampleDuration",
                                            Variant::Root(toTimeInterval(value))));
        return true;
    } else if (keyMatches(key, "Orig. time res")) {
        if (value.trimmed() == "0")
            return true;
        global->add(new ValueLookupConstant("Time/OriginalResolution",
                                            Variant::Root(toTimeInterval(value))));
        return true;
    } else if (keyMatches(key, "Startdate")) {
        global->add(new ValueLookupConstant("Time/Start", Variant::Root(toAbsoluteTime(value))));
        return true;
    } else if (keyMatches(key, "Revision date")) {
        global->add(new ValueLookupConstant("Time/Revision", Variant::Root(toAbsoluteTime(value))));
        return true;
    } else if (keyMatches(key, "Instrument name")) {
        QStringList components(value.split('_'));
        if (!components.isEmpty()) {
            global->add(new ValueLookupConstant("Instrument/Manufacturer",
                                                Variant::Root(components.takeFirst()), -1));
        }
        if (!components.isEmpty()) {
            global->add(new ValueLookupConstant("Instrument/Model",
                                                Variant::Root(components.takeFirst()), -1));
        }
        if (!components.isEmpty()) {
            QString check(components.takeFirst());
            if (check.length() == 3 &&
                    check.at(0).isLetter() &&
                    check.at(1).isLetter() &&
                    check.at(2).isLetter()) {
                global->add(new ValueLookupConstant("Station", Variant::Root(check.toUpper()), -2));
            }
        }
        return true;
    } else if (keyMatches(key, "Station WDCA-ID")) {
        if (value.length() > 3) {
            global->add(new ValueLookupConstant("Station", Variant::Root(value.right(3)), -1));
        }
    } else if (keyMatches(key, "File name")) {
        QString name(value.trimmed());
        global->add(new ValueLookupConstant("Filename", Variant::Root(name)));

        QStringList components(name.split('.'));
        if (!components.isEmpty()) {
            QString v(components.takeFirst().trimmed());
            if (!v.isEmpty()) {
                global->add(new ValueLookupConstant("EBAS/Station", Variant::Root(v), -1));
            }
        }


        if (!components.isEmpty() && components.last().trimmed() == "gz")
            components.removeLast();
        if (!components.isEmpty() && components.last().trimmed() == "nas")
            components.removeLast();
        if (!components.isEmpty()) {
            QString level(components.last().trimmed());
            if (level.startsWith("lev")) {
                components.removeLast();
                level = level.mid(3);
                global->add(new ValueLookupConstant("Level", Variant::Root(level), -1));
            }
        }
        return true;
    }

    if (value.isEmpty())
        return true;
    global->add(new ValueLookupConstant(key, Variant::Root(value)));
    return true;
}

static QSet<QString> recordTargets(const QString &componentName)
{
    /* Handled specially */
    if (componentName.startsWith("numflag"))
        return QSet<QString>();

    if (componentName.endsWith("_zero_measurement"))
        return QSet<QString>() << "zero";

    return QSet<QString>() << QString();
}

static int doyDigits(const QString &input)
{
    int pos = input.indexOf('.');
    if (pos < 0 || pos >= input.size() - 1)
        return -1;
    return (input.size() - 1) - pos;
}

static double doyRoundingLimit(int digits)
{
    if (digits <= 0)
        return 1.0;
    return 86400.0 * pow(10, -digits) * 0.5;
}

bool EBASEngine::processHeaders(QStringList &lines)
{
    Q_ASSERT(!lines.isEmpty());

    QStringList headers;
    {
        QStringList fields(lines.first().split(QRegExp("\\s+"), QString::SkipEmptyParts));
        if (fields.size() != 2) {
            qCDebug(log_fileinput_ebas) << "Invalid AMES header";
            return false;
        }
        if (fields.at(1).trimmed() != "1001") {
            qCDebug(log_fileinput_ebas) << "Invalid AMES version";
            return false;
        }
        bool ok = false;
        int n = fields.first().trimmed().toInt(&ok);
        if (!ok || n < 14) {
            qCDebug(log_fileinput_ebas) << "Insufficient AMES headers";
            return false;
        }
        if (lines.length() < n)
            return true;

        headers = lines.mid(0, n);
        lines.erase(lines.begin(), lines.begin() + n);
    }

    auto lock = processingRelock();

    global.reset(new EBASValueLookup(QList<ValueLookupPointer>(), this));
    if (!defaultStation.isEmpty()) {
        global->add(new ValueLookupConstant("Station", Variant::Root(defaultStation.toUpper()), 0));
    } else {
        global->add(new ValueLookupConstant("Station", Variant::Root("NIL"), -1000));
    }
    if (!defaultArchive.isEmpty()) {
        global->add(new ValueLookupConstant("Archive", Variant::Root(defaultArchive.toUpper()), 0));
    } else {
        global->add(new ValueLookupConstant("Archive", Variant::Root("RAW"), -1000));
    }
    if (!defaultSuffix.isEmpty()) {
        global->add(new ValueLookupConstant("Suffix", Variant::Root(defaultSuffix), 0));
    } else {
        global->add(new ValueLookupConstant("Suffix", Variant::Root("X1"), -1000));
    }
    {
        global->context()->substitutionStack().push();
        global->context()
              ->substitutionStack()
              .setVariant("Suffix",
                          Variant::Root(global->lookup(Variant::Root("Suffix")).toString()));
    }

    /* Header count and version number, handled above */
    headers.removeFirst();

    /* Data originator and PIs */
    {
        Q_ASSERT(!headers.isEmpty());
        QString line(headers.takeFirst().trimmed());
        int index = 0;
        Variant::Write pis = Variant::Write::empty();
        QStringList parts(line.split(';'));
        for (QStringList::const_iterator name = parts.constBegin(), endParts = parts.constEnd();
                name != endParts;
                ++name) {
            QString last(name->section(',', 0, 0).trimmed());
            QString first(name->section(',', 1).trimmed());

            Variant::Write v = Variant::Write::empty();
            v.toArray().after_back().setString(first);
            v.toArray().after_back().setString(last);

            if (index == 0) {
                global->add(new ValueLookupConstant("DataOriginator", v));
                global->add(new ValueLookupConstant("PrincipalInvestigator/#0", v));
            } else {
                pis.toArray().after_back().set(v);
            }

            ++index;
        }
        if (pis.exists()) {
            global->add(new ValueLookupConstant("PrincipalInvestigator", pis));
        }
    }

    /* Organization */
    {
        Q_ASSERT(!headers.isEmpty());
        QStringList fields(headers.takeFirst().trimmed());
        Variant::Write org = Variant::Write::empty();
        Variant::Write baseline = Variant::Write::empty();
        if (!fields.isEmpty()) {
            QString f(fields.takeFirst().trimmed());
            setIfNotEmpty(org, "Laboratory", f);
            setIfNotEmpty(baseline, "EBAS/Laboratory", f);
        }
        if (!fields.isEmpty())
            setIfNotEmpty(org, "Name", fields.takeFirst().trimmed());
        if (!fields.isEmpty())
            setIfNotEmpty(org, "Acronym", fields.takeFirst().trimmed());
        if (!fields.isEmpty())
            setIfNotEmpty(org, "Unit", fields.takeFirst().trimmed());
        if (!fields.isEmpty())
            setIfNotEmpty(org, "Address/#0", fields.takeFirst().trimmed());
        if (!fields.isEmpty())
            setIfNotEmpty(org, "Address/#1", fields.takeFirst().trimmed());
        if (!fields.isEmpty())
            setIfNotEmpty(org, "ZIP", fields.takeFirst().trimmed());
        if (!fields.isEmpty())
            setIfNotEmpty(org, "Town", fields.takeFirst().trimmed());
        if (!fields.isEmpty())
            setIfNotEmpty(org, "Country", fields.takeFirst().trimmed());

        if (org.exists())
            global->add(new ValueLookupConstant("Organization", org));
        if (baseline.exists())
            global->add(new ValueLookupConstant(QString(), baseline));
    }

    /* Data submitter */
    {
        Q_ASSERT(!headers.isEmpty());
        QString line(headers.takeFirst().trimmed());
        Variant::Write sub = Variant::Write::empty();
        QStringList parts(line.split(';'));
        for (QStringList::const_iterator name = parts.constBegin(), endParts = parts.constEnd();
                name != endParts;
                ++name) {
            QString last(name->section(',', 0, 0).trimmed());
            QString first(name->section(',', 1).trimmed());

            auto v = sub.toArray().after_back();
            v.toArray().after_back().setString(first);
            v.toArray().after_back().setString(last);
        }
        if (sub.exists())
            global->add(new ValueLookupConstant("DataSubmitter", sub));
    }

    /* Projects */
    {
        Q_ASSERT(!headers.isEmpty());
        QString line(headers.takeFirst().trimmed());
        if (!line.isEmpty())
            global->add(new ValueLookupConstant("Projects", Variant::Root(line)));
    }

    /* File splitting */
    Q_ASSERT(!headers.isEmpty());
    headers.removeFirst();

    /* Reference and revision dates */
    {
        Q_ASSERT(!headers.isEmpty());
        QStringList fields
                (headers.takeFirst().trimmed().split(QRegExp("\\s+"), QString::SkipEmptyParts));
        if (fields.size() < 6) {
            qCDebug(log_fileinput_ebas) << "Poorly formatted AMES reference times";
            return false;
        }
        bool ok = false;
        int year = fields.takeFirst().trimmed().toInt(&ok);
        if (!ok || year < 1970 || year > 2999) {
            qCDebug(log_fileinput_ebas) << "Invalid AMES reference year";
            return false;
        }
        int month = fields.takeFirst().trimmed().toInt(&ok);
        if (!ok || month < 1 || month > 12) {
            qCDebug(log_fileinput_ebas) << "Invalid AMES reference month";
            return false;
        }
        int day = fields.takeFirst().trimmed().toInt(&ok);
        if (!ok || day < 1 || day > 31) {
            qCDebug(log_fileinput_ebas) << "Invalid AMES reference day";
            return false;
        }
        QDateTime dt(QDate(year, month, day), QTime(0, 0, 0), Qt::UTC);
        if (!dt.isValid()) {
            qCDebug(log_fileinput_ebas) << "Invalid AMES reference time";
            return false;
        }
        referenceTime = Time::fromDateTime(dt);
        Q_ASSERT(FP::defined(referenceTime));
        global->add(new ValueLookupConstant("Time/Reference", Variant::Root(referenceTime)));

        year = fields.takeFirst().trimmed().toInt(&ok);
        if (!ok || year < 1970 || year > 2999) {
            qCDebug(log_fileinput_ebas) << "Invalid AMES revision year";
            return false;
        }
        month = fields.takeFirst().trimmed().toInt(&ok);
        if (!ok || month < 1 || month > 12) {
            qCDebug(log_fileinput_ebas) << "Invalid AMES revision month";
            return false;
        }
        day = fields.takeFirst().trimmed().toInt(&ok);
        if (!ok || day < 1 || day > 31) {
            qCDebug(log_fileinput_ebas) << "Invalid AMES revision day";
            return false;
        }
        dt = QDateTime(QDate(year, month, day), QTime(0, 0, 0), Qt::UTC);
        if (!dt.isValid()) {
            qCDebug(log_fileinput_ebas) << "Invalid AMES revision time";
            return false;
        }
        global->add(
                new ValueLookupConstant("Time/Revision", Variant::Root(Time::fromDateTime(dt))));
    }

    double interval = FP::undefined();
    int intervalDigits = -1;
    /* Interval between measurements */
    {
        Q_ASSERT(!headers.isEmpty());
        bool ok = false;
        QString intervalRaw(headers.takeFirst().trimmed());
        interval = intervalRaw.toDouble(&ok);
        if (ok && interval > 0.0) {
            interval *= 86400.0;
            intervalDigits = doyDigits(intervalRaw);
        } else {
            interval = FP::undefined();
        }
    }

    /* Time unit description */
    Q_ASSERT(!headers.isEmpty());
    headers.removeFirst();

    int nVariables = 0;
    QList<VariableHeader> variableHeaders;

    /* Number of variables */
    {
        Q_ASSERT(!headers.isEmpty());
        bool ok = false;
        nVariables = headers.takeFirst().trimmed().toInt(&ok);
        if (!ok || nVariables < 1) {
            qCDebug(log_fileinput_ebas) << "Invalid number of variables";
            return false;
        }
    }

    /* Scaling factors */
    Q_ASSERT(!headers.isEmpty());
    headers.removeFirst();

    /* MVCs */
    {
        Q_ASSERT(!headers.isEmpty());
        QStringList fields
                (headers.takeFirst().trimmed().split(QRegExp("\\s+"), QString::SkipEmptyParts));
        for (int i = 0, max = qMin(nVariables, fields.size()); i < max; i++) {
            while (variableHeaders.size() <= i) {
                variableHeaders.append(VariableHeader());
            }
            variableHeaders[i].mvc = fields.at(i).trimmed();
        }
    }

    if (headers.size() < nVariables) {
        qCDebug(log_fileinput_ebas) << "Insufficient headers for AMES titles";
        return false;
    }
    bool haveSeenQuantile = false;
    for (int i = 0; i < nVariables; i++) {
        Q_ASSERT(!headers.isEmpty());

        while (variableHeaders.size() <= i) {
            variableHeaders.append(VariableHeader());
        }
        QString title(headers.takeFirst().trimmed());
        variableHeaders[i].amesTitle = title;

        /* Nasty hack, but this should only exist in level 2 data,
         * so set it if we can't find the level otherwise */
        if (!haveSeenQuantile && title.contains("Statistics=percentile:", Qt::CaseInsensitive)) {
            global->add(new ValueLookupConstant("Level", Variant::Root("2"), -1000));
            haveSeenQuantile = true;
        }
    }

    int nSpecialComments = 0;
    /* Number of special comments */
    if (headers.isEmpty()) {
        qCDebug(log_fileinput_ebas) << "No auxiliary special definitions";
        return false;
    }
    {
        Q_ASSERT(!headers.isEmpty());
        bool ok = false;
        nSpecialComments = headers.takeFirst().trimmed().toInt(&ok);
        if (!ok || nSpecialComments < 0) {
            qCDebug(log_fileinput_ebas) << "Invalid number of special comments";
            return false;
        }
    }
    if (headers.size() < nSpecialComments) {
        qCDebug(log_fileinput_ebas) << "Insufficient headers for AMES special comments";
        return false;
    }
    /* Special comment lines */
    if (nSpecialComments > 0) {
        Variant::Write sc = Variant::Write::empty();
        while (nSpecialComments > 0) {
            Q_ASSERT(!headers.isEmpty());
            --nSpecialComments;
            sc.toArray().after_back().setString(headers.takeFirst().trimmed());
        }
        global->add(new ValueLookupConstant("SpecialComments", sc));
    }

    int nAuxiliaryComments = 0;
    /* Number of auxiliary comments */
    if (headers.isEmpty()) {
        qCDebug(log_fileinput_ebas) << "No auxiliary comment definitions";
        return false;
    }
    {
        Q_ASSERT(!headers.isEmpty());
        bool ok = false;
        nAuxiliaryComments = headers.takeFirst().trimmed().toInt(&ok);
        if (!ok || nAuxiliaryComments < 1) {
            qCDebug(log_fileinput_ebas) << "Invalid number of auxiliary comments";
            return false;
        }
    }
    if (headers.size() < nAuxiliaryComments) {
        qCDebug(log_fileinput_ebas) << "Insufficient headers for AMES auxiliary comments";
        return false;
    }
    /* Auxiliary comment lines */
    while (nAuxiliaryComments > 1) {
        Q_ASSERT(!headers.isEmpty());
        --nAuxiliaryComments;

        QString line(headers.takeFirst().trimmed());
        QString key(line.section(':', 0, 0).trimmed());
        QString value(line.section(':', 1).trimmed());
        if (key.endsWith('.'))
            key.chop(1);

        if (!FP::defined(interval) && key == "Resolution code")
            interval = toTimeInterval(value);

        if (!interpretAuxiliaryComment(key, value))
            return false;
    }
    /* Short variable names */
    {
        Q_ASSERT(nAuxiliaryComments == 1);
        Q_ASSERT(!headers.isEmpty());
        --nAuxiliaryComments;

        QStringList fields
                (headers.takeFirst().trimmed().split(QRegExp("\\s+"), QString::SkipEmptyParts));
        /* Skip independent variable header */
        if (!fields.isEmpty())
            fields.removeFirst();
        for (int i = 0, max = qMin(nVariables, fields.size()); i < max; i++) {
            while (variableHeaders.size() <= i) {
                variableHeaders.append(VariableHeader());
            }
            variableHeaders[i].fieldName = fields.at(i).trimmed();
        }
    }

    /* Round the interval to the rounding base, if it's close */
    if (FP::defined(interval) &&
            FP::defined(timeRoundingBase) &&
            interval > 0.0 &&
            timeRoundingBase > 0.0 &&
            (interval / timeRoundingBase) >= 0.5) {
        double rounded = floor(interval / timeRoundingBase + 0.5) * timeRoundingBase;
        if (fabs(rounded - interval) < doyRoundingLimit(intervalDigits)) {
            interval = rounded;
        }
    }

    /* Extract configuration */
    if (inputConfigUnit.getStation().empty()) {
        inputConfigUnit.setStation(global->lookup(Variant::Root("Station")).toString());
    }
    {
        double effectiveStart = global->lookup(Variant::Root("Time/Start")).toDouble();
        double effectiveEnd = global->lookup(Variant::Root("Time/Period")).toDouble();
        if (FP::defined(effectiveEnd) && FP::defined(effectiveStart)) {
            effectiveEnd += effectiveStart;
        } else {
            effectiveEnd = FP::undefined();
        }
        for (auto &c : inputConfig) {
            if (!Range::intersects(c.getStart(), c.getEnd(), effectiveStart, effectiveEnd))
                continue;
            configuration.write().set(c.getValue(inputConfigUnit).getPath(inputConfigPath));
            break;
        }
    }

    idxStartDOY = 0;
    int individualFlagsTargets = 0;
    QRegExp reNormalMVC("9+(?:\\.9*)?");
    for (int i = 0, max = variableHeaders.size(); i < max; i++) {
        VariableHeader vh = variableHeaders.at(i);

        if (vh.amesTitle == "end_time" ||
                (vh.amesTitle.startsWith("end_time") &&
                        vh.amesTitle.contains("days from the file reference point")) ||
                vh.fieldName == "end_time") {
            idxEndDOY = i;
            continue;
        } else if (vh.amesTitle.startsWith("start_time") ||
                vh.amesTitle.startsWith("end_time") ||
                vh.fieldName == "start_time" ||
                vh.fieldName == "end_time" ||
                vh.fieldName == "st_y" ||
                vh.fieldName == "ed_y") {
            continue;
        }

        QStringList amesComponents(vh.amesTitle.split(','));

        QString componentName;
        if (!amesComponents.isEmpty()) {
            componentName = amesComponents.takeFirst().trimmed();
        }

        QString units;
        if (!amesComponents.isEmpty()) {
            units = remapUnits(amesComponents.takeFirst().trimmed());
        }

        QHash<QString, Variant::Read> auxiliaryValues;
        while (!amesComponents.isEmpty()) {
            QString cmp(amesComponents.takeFirst().trimmed());
            QString name(cmp.section('=', 0, 0).trimmed());
            QString value(cmp.section('=', 1).trimmed());

            if (name.compare("Matrix", Qt::CaseInsensitive) == 0 &&
                    value.compare("instrument", Qt::CaseInsensitive) == 0)
                continue;

            if (value.isEmpty()) {
                auxiliaryValues.insert(name, Variant::Root(true));
                continue;
            } else if (name.compare("Wavelength", Qt::CaseInsensitive) == 0) {
                QRegExp re("^\\s*(\\d+)");
                if (re.indexIn(value) >= 0) {
                    bool ok = false;
                    double wl = re.cap(1).trimmed().toDouble(&ok);
                    if (!ok || !FP::defined(wl) || wl <= 0.0)
                        continue;
                    auxiliaryValues.insert("Wavelength", Variant::Root(wl));
                }
                continue;
            } else if (name.compare("Volume std. temperature", Qt::CaseInsensitive) == 0) {
                if (value.trimmed().toLower() == "ambient") {
                    auxiliaryValues.insert("ReportT", Variant::Root());
                } else {
                    auxiliaryValues.insert("ReportT", Variant::Root(toStandardUnits(value)));
                }
                continue;
            } else if (name.compare("Volume std. pressure", Qt::CaseInsensitive) == 0) {
                if (value.trimmed().toLower() == "ambient") {
                    auxiliaryValues.insert("ReportP", Variant::Root());
                } else {
                    auxiliaryValues.insert("ReportP", Variant::Root(toStandardUnits(value)));
                }
                continue;
            } else if (name.compare("Statistics", Qt::CaseInsensitive) == 0 &&
                    value.startsWith("percentile", Qt::CaseInsensitive)) {
                QRegExp re("(\\d+(?:\\.\\d*)?)\\s*$");
                if (re.indexIn(value.mid(10)) >= 0) {
                    bool ok = false;
                    double q = re.cap(1).trimmed().toDouble(&ok);
                    if (!ok || !FP::defined(q) || q < 0.0 || q > 100.0)
                        continue;
                    auxiliaryValues.insert("Quantile", Variant::Root(q));
                    continue;
                }
            }

            auxiliaryValues.insert(name, Variant::Root(value));
        }

        while (variables.size() <= i) {
            variables.append(QHash<QString, EBASRecordVariable *>());
        }
        while (mvcs.size() <= i) {
            mvcs.append(QString());
        }
        mvcs[i] = vh.mvc;
        while (flagsFor.size() <= i) {
            flagsFor.append(QSet<QString>());
        }
        bool isFlags = componentName.startsWith("numflag");
        while (priorValues.size() <= i) {
            priorValues.append(QString());
        }

        QSet<QString> targets;
        int flagsColumn = -1;
        if (isFlags) {
            if (i == 0)
                continue;
            if (i != max - 1) {
                targets = QSet<QString>::fromList(variables[i - 1].keys());
                flagsFor[i] = targets;
                flagsColumn = i - 1;
                ++individualFlagsTargets;
            } else if (individualFlagsTargets == 0 ||
                    variableHeaders.at(i - 1).fieldName.startsWith("numflag")) {
                targets = QSet<QString>::fromList(records.keys());
                targets.remove("zero");
                flagsFor[i] = targets;
            } else {
                targets = QSet<QString>::fromList(variables[i - 1].keys());
                flagsFor[i] = targets;
                flagsColumn = i - 1;
                ++individualFlagsTargets;
            }
        } else {
            targets = recordTargets(componentName);
        }

        for (QSet<QString>::const_iterator target = targets.constBegin(),
                endTargets = targets.constEnd(); target != endTargets; ++target) {
            EBASRecord *record = records.value(*target, NULL);
            if (record == NULL) {
                record = new EBASRecord(global.get());
                Q_ASSERT(record);
                records.insert(*target, record);

                if (target->isEmpty() && FP::defined(interval))
                    record->setNominalInterval(interval);
            }

            EBASRecordVariable *var = static_cast<EBASRecordVariable *>(
                    record->addVariable());
            variables[i][*target] = var;

            var->getLookup()->add(new ValueLookupConstant("Column", Variant::Root((qint64) i)));

            if (!vh.mvc.isEmpty()) {
                var->getLookup()->add(new ValueLookupConstant("MVC", Variant::Root(vh.mvc)));

                if (reNormalMVC.exactMatch(vh.mvc)) {
                    QString format(vh.mvc);
                    format.replace('9', '0');
                    var->getLookup()->add(new ValueLookupConstant("Format", Variant::Root(format)));
                }
            }

            if (flagsColumn >= 0) {
                var->getLookup()
                   ->add(new ValueLookupConstant("BaseColumn",
                                                 Variant::Root((qint64) flagsColumn)));
            }

            if (!componentName.isEmpty()) {
                var->getLookup()
                   ->add(new ValueLookupConstant("Component", Variant::Root(componentName)));
            }
            if (!units.isEmpty()) {
                var->getLookup()->add(new ValueLookupConstant("Units", Variant::Root(units)));
            }
            for (QHash<QString, Variant::Read>::const_iterator a = auxiliaryValues.constBegin(),
                    endA = auxiliaryValues.constEnd(); a != endA; ++a) {
                var->getLookup()->add(new ValueLookupConstant(a.key(), a.value()));
            }
        }
    }

    return false;
}

static bool compressUntilChanged(const QString &record)
{
    if (record.isEmpty())
        return false;
    return true;
}

void EBASEngine::processData(QStringList &lines, bool)
{
    /* Don't need the mutex here because the externally visible components
     * are not written to in a way that can be read from outside this
     * thread (i.e. this only updates the record contents and those are only
     * accessed via completeFile(), which is called from the same thread
     * as this */

    if (records.isEmpty())
        return;

    for (QStringList::const_iterator line = lines.constBegin(), endLines = lines.constEnd();
            line != endLines;
            ++line) {
        QStringList fields(line->split(QRegExp("\\s+"), QString::SkipEmptyParts));
        if (fields.isEmpty())
            continue;

        double start = FP::undefined();
        int startDigits = -1;
        if (idxStartDOY >= 0 && idxStartDOY < fields.size()) {
            bool ok = false;
            QString rawValue(fields.at(idxStartDOY));
            double days = rawValue.toDouble(&ok);
            if (ok && days >= 0.0) {
                start = referenceTime + days * 86400.0;
                startDigits = doyDigits(rawValue);
            }
        }
        double end = FP::undefined();
        int endDigits = -1;
        if (idxEndDOY >= 0 && idxEndDOY < fields.size()) {
            bool ok = false;
            QString rawValue(fields.at(idxEndDOY));
            double days = rawValue.toDouble(&ok);
            if (ok && days >= 0.0) {
                end = referenceTime + days * 86400.0;
                endDigits = doyDigits(rawValue);
            }
        }
        /* Round the bounds as required */
        if (FP::defined(timeRoundingBase) && timeRoundingBase > 0.0) {
            if (FP::defined(start)) {
                double rounded = floor(start / timeRoundingBase + 0.5) * timeRoundingBase;
                if (fabs(start - rounded) < doyRoundingLimit(startDigits))
                    start = rounded;
            }
            if (FP::defined(end)) {
                double rounded = floor(end / timeRoundingBase + 0.5) * timeRoundingBase;
                if (fabs(end - rounded) < doyRoundingLimit(endDigits))
                    end = rounded;
            }
        }

        if (FP::defined(start) && FP::defined(end) && start >= end) {
            end = FP::undefined();
        }

        /* First field is always the independent variable, which isn't
         * in the header definitions */
        fields.removeFirst();

        QHash<QString, bool> affected;
        for (int i = 0, max = qMin(variables.size(), fields.size()); i < max; i++) {
            bool changed = false;
            if (fields.at(i).trimmed() != mvcs.at(i)) {
                changed = changed || fields.at(i) != priorValues.at(i);
                priorValues[i] = fields.at(i);
            }

            for (QHash<QString, EBASRecordVariable *>::const_iterator v = variables[i].constBegin(),
                    endV = variables[i].constEnd(); v != endV; ++v) {
                affected[v.key()] = affected[v.key()] || changed;
            }
        }
        for (QHash<QString, bool>::iterator check = affected.begin(); check != affected.end();) {
            if (!compressUntilChanged(check.key())) {
                check.value() = true;
                ++check;
                continue;
            } else if (check.value()) {
                ++check;
                continue;
            }
            check = affected.erase(check);
        }

        /* First unset the affected when we have a 999 flag, so we don't
         * incorporate the values */
        for (int i = 0, maxI = qMin(variables.size(), fields.size()); i < maxI; i++) {
            QSet<QString> flagsTargets(flagsFor.at(i));
            if (flagsTargets.isEmpty())
                continue;
            QStringList parts(fields.at(i).trimmed().split('.'));
            if (parts.size() < 2)
                continue;

            QString base(parts.at(1));
            for (int offset = 0, len = base.length(); offset + 3 <= len; offset += 3) {
                if (base.mid(offset, 3) != "999")
                    continue;
                /* This might need better logic if a 999 shows up in individual
                 * variable flags columns */
                for (QSet<QString>::const_iterator r = flagsTargets.constBegin(),
                        endR = flagsTargets.constEnd(); r != endR; ++r) {
                    /* Check if any of the values are actually non-missing,
                     * in which case we keep it (i.e. there appear to be files
                     * that set 999 while data are valid for some reason) */
                    bool anyValid = false;
                    for (int j = 0, maxJ = qMin(variables.size(), fields.size()); j < maxJ; j++) {
                        if (!variables.at(j).contains(*r))
                            continue;
                        if (fields.at(j).trimmed() == mvcs.at(j))
                            continue;
                        anyValid = true;
                        break;
                    }
                    if (anyValid)
                        continue;

                    affected.remove(*r);
                }
            }
        }

        /* Define record timings */
        for (QHash<QString, bool>::const_iterator r = affected.constBegin(),
                endR = affected.constEnd(); r != endR; ++r) {
            if (compressUntilChanged(r.key())) {
                records[r.key()]->defineTime(start);
            } else {
                records[r.key()]->defineTime(start, end);
            }
        }

        /* Second pass we set the actual values */
        for (int i = 0, max = qMin(variables.size(), fields.size()); i < max; i++) {

            QString raw(fields.at(i).trimmed());
            QString mvc(mvcs.at(i));

            Variant::Write output = Variant::Write::empty();
            if (raw == mvc) {
                output.setEmpty();
            } else if (!flagsFor.at(i).isEmpty()) {
                QStringList parts(raw.split('.'));
                if (parts.size() < 2)
                    continue;
                QString base(parts.at(1));
                output.setType(Variant::Type::Flags);
                for (int offset = 0, len = base.length(); offset + 3 <= len; offset += 3) {
                    bool ok = false;
                    int flag = base.mid(offset, 3).toInt(&ok);
                    if (!ok || flag <= 0 || flag > 999)
                        continue;
                    std::string number = std::to_string(flag);
                    if (number.length() < 3) {
                        number.insert(0, 3 - number.length(), '0');
                    }

                    output.applyFlag(std::string("EBASFlag") + number);
                }
            } else {
                bool ok = false;
                double v = raw.toDouble(&ok);
                if (!ok) {
                    output.setDouble(FP::undefined());
                } else {
                    output.setDouble(v);
                }
            }

            for (QHash<QString, EBASRecordVariable *>::const_iterator v = variables[i].constBegin(),
                    endV = variables[i].constEnd(); v != endV; ++v) {
                if (!affected[v.key()])
                    continue;
                v.value()->setValue(output);
            }
        }
    }

    lines.clear();
}

void EBASEngine::prepareOutputSet()
{
    if (outputSet)
        return;
    outputSet.reset(new EBASOutputSet(configuration));
    Q_ASSERT(global);
    outputSet->activate(global.get());
    for (QHash<QString, EBASRecord *>::const_iterator add = records.constBegin(),
            endAdd = records.constEnd(); add != endAdd; ++add) {
        outputSet->integrateRecord(add.value());
    }
}

SequenceName::Set EBASEngine::predictedOutputs()
{
    auto lock = acquireLock();
    if (records.isEmpty())
        return SequenceName::Set();
    prepareOutputSet();
    return outputSet->predictedOutputs();
}

bool EBASEngine::completeFile()
{
    /* Need to acquire the main lock, which means releasing the egress lock, so we have
     * to do this in two steps */
    if (!fileLockedCompleted) {
        fileLockedCompleted = true;
        auto lock = processingRelock();
        if (records.isEmpty())
            return false;
        prepareOutputSet();
        completedOutput = outputSet->produceOutput();
        return true;
    }

    outputData(std::move(completedOutput));
    completedOutput.clear();
    return false;
}

}
}
