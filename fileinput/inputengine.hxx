/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3FILEINPUTENGINE_H
#define CPD3FILEINPUTENGINE_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QStringList>

#include "fileinput/fileinput.hxx"
#include "datacore/externalsource.hxx"

namespace CPD3 {
namespace Input {

/**
 * The base class for the engine that handles input processing.
 */
class CPD3FILEINPUT_EXPORT InputEngine : public Data::ExternalSourceProcessor {
public:
    InputEngine();

    virtual ~InputEngine();

    virtual void signalTerminate();

    Threading::Signal<> terminateRequested;

protected:
    virtual bool prepare() = 0;

    virtual bool process() = 0;
};

/**
 * An engine that handles input based on UTF-8 line delimited data.
 */
class CPD3FILEINPUT_EXPORT InputLineBased : public InputEngine {
    Util::ByteArray processingBuffer;
    bool processingEnded;
    QStringList lines;
public:
    InputLineBased();

    virtual ~InputLineBased();

protected:
    bool prepare() override;

    bool process() override;

    /**
     * Process the current lines.
     * 
     * @param lines     the current lines in the file
     * @param ended     true if this is the end of processing
     * @return true     to continue processing
     */
    virtual bool process(QStringList &lines, bool ended) = 0;
};


/**
 * An engine that handles input that is line based and divided up into a
 * headers section and a data section.
 */
class CPD3FILEINPUT_EXPORT InputLineHeaders : public InputLineBased {
    bool inHeaders;
public:
    InputLineHeaders();

    virtual ~InputLineHeaders();

protected:
    bool process(QStringList &lines, bool ended) override;

    /**
     * Process header lines.  This should consume any headers lines it can
     * and return false when the headers are finished.  The egress mutex
     * may be unlocked during the process.
     * 
     * @param lines     the current lines in the file
     * @return          true if there are more header lines
     */
    virtual bool processHeaders(QStringList &lines) = 0;

    /**
     * Process data lines.  This should consume any lines used.
     * 
     * @param lines     the current lines in the file
     * @param ended     true if this is the end of processing
     */
    virtual void processData(QStringList &lines, bool ended) = 0;

    /**
     * Complete the file.  This only called if there have been headers.
     *
     * @return true if more processing needs to be done
     */
    virtual bool completeFile();
};

}
}

#endif
