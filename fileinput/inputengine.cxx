/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "fileinput/inputengine.hxx"
#include "core/csv.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Input {

/** @file fileinput/inputengine.hxx
 * The main engine for handling file input.
 */

InputEngine::InputEngine()
{ }

InputEngine::~InputEngine()
{ }

void InputEngine::signalTerminate()
{
    ExternalSourceProcessor::signalTerminate();
    terminateRequested();
}

InputLineBased::InputLineBased() : processingBuffer(), processingEnded(false), lines()
{ }

InputLineBased::~InputLineBased()
{ }

bool InputLineBased::prepare()
{
    /* Don't copy out of buffers while we can't output, so the limiting
     * works while paused */
    if (egress == nullptr && !dataEnded)
        return false;

    if (!buffer.empty()) {
        processingBuffer += std::move(buffer);
        buffer.clear();
        processingEnded = dataEnded;
        return true;
    } else if (dataEnded || !lines.isEmpty()) {
        processingEnded = dataEnded;
        return true;
    }

    return false;
}

static constexpr std::size_t bufferProcessIterations = 2048;

bool InputLineBased::process()
{
    Util::ByteArray::LineIterator it(processingBuffer);
    if (processingEnded && processingBuffer.back() != '\n' && processingBuffer.back() != '\r') {
        processingBuffer.push_back('\n');
    }

    if (!lines.isEmpty()) {
        if (!process(lines, false))
            return false;
    }

    std::size_t counter = 0;
    while (auto line = it.next()) {
        lines += QString::fromStdString(line.toString());
        ++counter;

        if (counter >= bufferProcessIterations) {
            processingBuffer.pop_front(it.getCurrentOffset());
            return process(lines, false);
        }
    }

    processingBuffer.pop_front(it.getCurrentOffset());

    if (process(lines, processingEnded))
        return true;

    return !processingEnded;
}


InputLineHeaders::InputLineHeaders() : InputLineBased(), inHeaders(true)
{ }

InputLineHeaders::~InputLineHeaders()
{ }

bool InputLineHeaders::process(QStringList &lines, bool ended)
{
    if (lines.isEmpty()) {
        if (ended) {
            if (inHeaders)
                return false;
            return completeFile();
        }
        return true;
    }
    if (inHeaders) {
        if (!processHeaders(lines)) {
            inHeaders = false;
        }
        return true;
    }

    processData(lines, ended);
    if (ended)
        return completeFile();
    return true;
}

bool InputLineHeaders::completeFile()
{ return false; }


}
}
