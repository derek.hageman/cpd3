/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "fileinput/record.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Input {

/** @file fileinput/record.hxx
 * File record definition handling
 */


RecordVariable::RecordVariable(Record *r, ValueLookup *l) : record(r), lookup(l), index(-1)
{ }

RecordVariable::~RecordVariable()
{
    delete lookup;
}

void RecordVariable::setValue(const Variant::Read &value)
{
    Q_ASSERT(index >= 0);
    record->setVariable(index, value);
}

Record::Record(ValueLookup *g) : global(g), variables(), times(), nominalInterval(FP::undefined())
{ }

Record::~Record()
{
    qDeleteAll(variables);
}

void Record::setVariable(int index, const Variant::Read &value)
{
    Q_ASSERT(!times.empty());
    Q_ASSERT(index >= 0 && index <= static_cast<int>(times.back().values.size()));
    times.back().values[index].write().set(value);
}


RecordVariable *Record::addVariable()
{
    RecordVariable *v = createVariable();
    v->index = variables.size();
    variables.append(v);
    return v;
}

ValueLookup *Record::createVariableLookup(const ValueLookup *global)
{ return global->clone(); }

RecordVariable *Record::createVariable()
{
    return new RecordVariable(this, createVariableLookup(global));
}

void Record::defineTime(double start, double end)
{
    times.emplace_back(start, end, std::vector<Variant::Root>(variables.size(), Variant::Root()));
}

void Record::setNominalInterval(double interval)
{
    nominalInterval = interval;
}

static double roundInterval(double start, double end)
{
    if (start >= end)
        return 0.0;
    return floor((end - start) * 1000.0 + 0.5) / 1000.0;
}

double Record::getRecordInterval()
{
    if (FP::defined(nominalInterval) && nominalInterval > 0.0)
        return nominalInterval;

    QHash<double, int> intervalCounts;
    for (auto t = times.cbegin(), endT = times.cend(); t != endT; ++t) {
        if (FP::defined(t->getStart())) {
            if (FP::defined(t->getEnd())) {
                intervalCounts[roundInterval(t->getStart(), t->getEnd())] += 1;
                continue;
            }
            if (t + 1 != endT && FP::defined((t + 1)->getStart())) {
                intervalCounts[roundInterval(t->getStart(), (t + 1)->getStart())] += 1;
                continue;
            }
        } else if (FP::defined(t->getEnd())) {
            if (t + 1 != endT && FP::defined((t + 1)->getEnd())) {
                intervalCounts[roundInterval(t->getEnd(), (t + 1)->getEnd())] += 1;
                continue;
            }
        }
    }
    int bestCount = 0;
    double bestInterval = FP::undefined();
    for (QHash<double, int>::const_iterator i = intervalCounts.constBegin(),
            endI = intervalCounts.constEnd(); i != endI; ++i) {
        if (i.key() <= 0.0)
            continue;
        if (i.value() < bestCount)
            continue;
        bestCount = i.value();
        bestInterval = i.key();
    }
    return bestInterval;
}

static bool segmentClippedToInterval(double duration, double interval)
{
    if (!FP::defined(interval))
        return false;
    return duration > interval * 1.0;
}

QList<Record::DefinedTime> Record::output()
{
    double interval = getRecordInterval();

    QList<DefinedTime> output;
    for (auto t = times.cbegin(), endT = times.cend(); t != endT; ++t) {
        double start = t->getStart();
        double end = t->getEnd();
        if (!FP::defined(start)) {
            if (!FP::defined(end))
                continue;
            if (!output.isEmpty()) {
                start = output.last().getEnd();
                Q_ASSERT(FP::defined(start));
                if (segmentClippedToInterval(end - start, interval)) {
                    start = end - interval;
                }
            } else if (FP::defined(interval)) {
                start = end - interval;
            } else {
                continue;
            }
        } else if (!FP::defined(end)) {
            if (t + 1 == endT) {
                if (FP::defined(interval)) {
                    end = start + interval;
                } else {
                    continue;
                }
            } else if (FP::defined((t + 1)->getStart())) {
                end = (t + 1)->getStart();
                if (segmentClippedToInterval(end - start, interval)) {
                    end = start + interval;
                }
            } else if (FP::defined(interval)) {
                end = start + interval;
            } else {
                continue;
            }
        }
        Q_ASSERT(FP::defined(start));
        Q_ASSERT(FP::defined(end));

        if (end - start <= 0.0)
            continue;

        output.append(DefinedTime(*t, start, end));
    }

    return output;
}

}
}
