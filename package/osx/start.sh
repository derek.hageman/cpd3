#!/bin/sh

BUNDLE="`echo "$0" | sed -e 's/\/Contents\/MacOS\/CPD3//'`"
RESOURCES="${BUNDLE}/Contents/Resources"

export "PATH=${RESOURCES}:${PATH}"

DEFAULT_ARCHIVE="${HOME}/.cpd3.db"
# Not ideal, but I can't find a way to make the launcher ignore the first thing launched (or
# at least re-use the icon).  So we just don't run at all if we see the database already
# created.
if [ ! -e "${DEFAULT_ARCHIVE}" ]; then
    "${RESOURCES}/cpd3_initial_setup" --firstrun --archive="${DEFAULT_ARCHIVE}" --mode=synchronize
    touch "${DEFAULT_ARCHIVE}"
fi

exec "${RESOURCES}/cpd3_gui" "$@"
