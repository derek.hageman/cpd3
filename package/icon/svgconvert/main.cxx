#include <QSvgRenderer>
#include <QPainter>
#include <QStringList>
#include <QImage>
#include <QSize>

#include <QGuiApplication>
#include <QApplication>

int main(int argc, char **argv)
{
#if defined(Q_OS_UNIX) && !defined(Q_OS_OSX)
    qputenv("QT_QPA_PLATFORM", "offscreen");
#endif
    QGuiApplication app(argc, argv);

    QStringList arguments(app.arguments());
    if (arguments.size() < 5) {
        qFatal("Usage: input width height output");
        return 1;
    }

    arguments.removeFirst();

    QSvgRenderer renderer(arguments.takeFirst());
    int width = arguments.takeFirst().toInt();
    int height = arguments.takeFirst().toInt();

    QSize size(width, height);
    QImage img(size, QImage::Format_ARGB32);
    img.fill(0x00FFFFFF);

    QPainter painter;
    painter.begin(&img);
    renderer.render(&painter);
    painter.end();

    return img.save(arguments.takeFirst()) ? 0 : 2;
}