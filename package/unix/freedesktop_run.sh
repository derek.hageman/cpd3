#!/bin/sh

# Maybe check if the acquisition system is running?
if [ -r "/etc/default/cpd3-acquisition" ] && \
      ( [ -e "/lib/systemd/system/cpd3-acquisition.service" ] || \
        [ -e "/etc/systemd/system/cpd3-acquisition.service" ] ); then
    set -a
    . "/etc/default/cpd3-acquisition"
    set +a

    exec cpd3_gui --mode="realtime" "$@"
fi

if [ -z "${CPD3ARCHIVE}" ] && [ -z "${CPD3_ARCHIVE}" ]; then
    if [ -z "${XDG_DATA_HOME}" ]; then
        DEFAULT_ARCHIVE="${HOME}/.local/share/cpd3.db"
    else
        DEFAULT_ARCHIVE="${XDG_DATA_HOME}/cpd3.db"
    fi

    cpd3_initial_setup --firstrun --archive="${DEFAULT_ARCHIVE}" --mode=synchronize
fi

exec cpd3_gui "$@"
