#!/bin/sh

MODE=synchronize

if [ -r "/etc/default/cpd3-acquisition" ] && ( [ -e "/lib/systemd/system/cpd3-acquisition.service" ] || [ -e "/etc/systemd/system/cpd3-acquisition.service" ] ); then
    set -a
    . "/etc/default/cpd3-acquisition"
    set +a

    if [ ! -z "${CPD3ARCHIVE}" ] || [ ! -z "${CPD3_ARCHIVE}" ]; then
        exec cpd3_initial_setup --mode=acquisition "$@"
    fi
fi


if [ -z "${CPD3ARCHIVE}" ] && [ -z "${CPD3_ARCHIVE}" ]; then
    if [ -z "${XDG_DATA_HOME}" ]; then
        DEFAULT_ARCHIVE="${HOME}/.local/share/cpd3.db"
    else
        DEFAULT_ARCHIVE="${XDG_DATA_HOME}/cpd3.db"
    fi

    if [ ! -s "${DEFAULT_ARCHIVE}" ]; then
        exec cpd3_initial_setup --firstrun --archive="${DEFAULT_ARCHIVE}" --mode=synchronize "$@"
    fi
fi

exec cpd3_initial_setup "$@"
