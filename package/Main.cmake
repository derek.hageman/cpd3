set(CPACK_PACKAGE_NAME "cpd3")
set(CPACK_PACKAGE_CONTACT "Derek Hageman <derek.hageman@noaa.gov>")
set(CPACK_PACKAGE_VENDOR "NOAA-ESRL-GMD")
set(CPACK_PACKAGE_DESCRIPTION_FILE ${CMAKE_CURRENT_SOURCE_DIR}/package/description.txt)
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Data acquisition and processing")
set(CPACK_PACKAGE_HOMEPAGE_URL "https://gitlab.com/derek.hageman/cpd3")
set(CPACK_RESOURCE_FILE_LICENSE ${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.txt)
set(CPACK_MONOLITHIC_INSTALL 1)

set(CPACK_PACKAGE_VERSION_MAJOR "${CPD3_VERSION}")
set(CPACK_PACKAGE_VERSION_MINOR "0")
set(CPACK_PACKAGE_VERSION_PATCH "0")
#set(CPACK_PACKAGE_VERSION "${CPACK_PACKAGE_VERSION_MAJOR}")
set(CPACK_PACKAGE_FILE_NAME "${CPACK_PACKAGE_NAME}-v${CPACK_PACKAGE_VERSION_MAJOR}")

set(CPACK_DEBIAN_PACKAGE_SECTION "Science")
list(APPEND CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA "${CMAKE_CURRENT_SOURCE_DIR}/package/unix/deb/postinst")
list(APPEND CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA "${CMAKE_CURRENT_SOURCE_DIR}/package/unix/deb/postrm")

set(CPACK_RPM_PACKAGE_LICENSE "GPL3")
set(CPACK_RPM_PACKAGE_GROUP "Applications/Engineering")
set(CPACK_RPM_POST_INSTALL_SCRIPT_FILE ${CMAKE_CURRENT_SOURCE_DIR}/package/unix/rpm/postinst)

set(CPACK_BUNDLE_NAME "CPD3")
set(CPACK_BUNDLE_STARTUP_COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/package/osx/start.sh)
set(CPACK_BUNDLE_PLIST ${CMAKE_CURRENT_BINARY_DIR}/Info.plist)

set(CPACK_WIX_UPGRADE_GUID "E5E9C865-C189-4F75-BBBC-584DCA8A573D")
set(CPACK_WIX_PATCH_FILE "${CMAKE_CURRENT_BINARY_DIR}/wixpatch.xml")
set(CPACK_WIX_PROGRAM_MENU_FOLDER "CPD3")

if(APPLE)
    configure_file(package/osx/Info.plist.xml Info.plist)
elseif(WIN32)
    set(CPACK_PACKAGE_INSTALL_DIRECTORY "CPD3")
    set(CPACK_PACKAGE_NAME "CPD3")

    set(WIX_USE_VCREDIST "0")
    set(WIX_VCREDIST_REFID "1")
    foreach(ADD ${WIX_C_REDISTRIBUTABLE_FILES})
        if((NOT ADD STREQUAL "") AND (EXISTS "${ADD}"))
            set(WIX_USE_VCREDIST "1")

            set(WIX_MERGE_VCREDIST "${WIX_MERGE_VCREDIST}
        <DirectoryRef Id=\"TARGETDIR\">
            <Merge Id=\"VCRedist${WIX_VCREDIST_REFID}\" SourceFile=\"${ADD}\" DiskId=\"1\" Language=\"0\"/>
        </DirectoryRef>
        <Feature Id=\"VCRedist${WIX_VCREDIST_REFID}\" Title=\"Visual C++ Runtime ${WIX_VCREDIST_REFID}\" AllowAdvertise=\"no\" Display=\"hidden\" Level=\"1\">
            <MergeRef Id=\"VCRedist${WIX_VCREDIST_REFID}\"/>
        </Feature>
")

            math(EXPR WIX_VCREDIST_REFID "${WIX_VCREDIST_REFID}+1")
        endif((NOT ADD STREQUAL "") AND (EXISTS "${ADD}"))
    endforeach(ADD ${WIX_C_REDISTRIBUTABLE_FILES})

    configure_file(package/windows/patch.xml wixpatch.xml)
elseif(UNIX)
    install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/package/unix/freedesktop_run.sh
            RENAME cpd3_desktop_run.sh
            PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ
            GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
            DESTINATION ${BIN_INSTALL_DIR})
    install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/package/unix/freedesktop_setup.sh
            RENAME cpd3_desktop_setup.sh
            PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ
            GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
            DESTINATION ${BIN_INSTALL_DIR})

    configure_file(package/unix/freedesktop_main.desktop cpd3.desktop)
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/cpd3.desktop
            DESTINATION ${SHARE_INSTALL_DIR}/applications/)
    configure_file(package/unix/freedesktop_setup.desktop cpd3setup.desktop)
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/cpd3setup.desktop
            DESTINATION ${SHARE_INSTALL_DIR}/applications/)

    set(CPACK_RPM_EXCLUDE_FROM_AUTO_FILELIST_ADDITION ${CPACK_RPM_EXCLUDE_FROM_AUTO_FILELIST_ADDITION} /usr/share/applications)
endif(APPLE)

set(CPACK_DEBIAN_PACKAGE_DEPENDS "libqt5core5a, libqt5gui5, libqt5network5, libqt5sql5, libqt5svg5, libqt5xml5, libssl1.0.0 | libssl1.0.2 | libssl1.1 | libssl3, libc6, libstdc++6")
set(CPACK_DEBIAN_PACKAGE_RECOMMENDS "libqt5sql5-sqlite, xvfb")
set(CPACK_DEBIAN_PACKAGE_SUGGESTS "qt5-image-formats-plugins, libqt5sql5-mysql, libqt5sql5-psql")
if(Qt5Widgets_FOUND)
    set(CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}, libqt5widgets5")
endif(Qt5Widgets_FOUND)
if(Qt5PrintSupport_FOUND)
    set(CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}, libqt5printsupport5")
endif(Qt5PrintSupport_FOUND)

set(CPACK_RPM_PACKAGE_REQUIRES "qt5-qtbase >= 5.2, qt5-qtbase-gui >= 5.2, qt5-qtsvg >= 5.2, openssl >= 1.0.0")
# Causes issues on some versions of RPM building
#set(CPACK_RPM_PACKAGE_SUGGESTS "qt5-qtbase-mysql >= 5.2, qt5-qtbase-postgresql >= 5.2, xorg-x11-server-Xvfb >= 1.0.0")

#if(Boost_FOUND)
#    set(CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}, libboost (>= 1.36)")
#    set(CPACK_RPM_PACKAGE_REQUIRES "${CPACK_RPM_PACKAGE_REQUIRES}, boost >= 1.36")
#endif(Boost_FOUND)

if(HAVE_LUAJIT)
    set(CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}, libluajit-5.1-2")
    set(CPACK_RPM_PACKAGE_REQUIRES "${CPACK_RPM_PACKAGE_REQUIRES}, luajit >= 2.0.0")
else(HAVE_LUAJIT)
    set(CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}, liblua5.1-0")
    set(CPACK_RPM_PACKAGE_REQUIRES "${CPACK_RPM_PACKAGE_REQUIRES}, lua >= 5.1.0")
endif(HAVE_LUAJIT)

if(HAVE_NETCDF)
    set(CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}, libnetcdfc7 | libnetcdf11 | libnetcdf13 | libnetcdf15 | libnetcdf18 | libnetcdf19")
    set(CPACK_RPM_PACKAGE_REQUIRES "${CPACK_RPM_PACKAGE_REQUIRES}, netcdf >= 4.0.0")
endif(HAVE_NETCDF)

if(HAVE_SQLITE3)
    set(CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}, libsqlite3-0")
    set(CPACK_RPM_PACKAGE_REQUIRES "${CPACK_RPM_PACKAGE_REQUIRES}, sqlite >= 3.0.0")
endif(HAVE_SQLITE3)

if(PostgreSQL_FOUND)
    set(CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}, libpq5")
    set(CPACK_RPM_PACKAGE_REQUIRES "${CPACK_RPM_PACKAGE_REQUIRES}, postgresql-libs >= 8.0.0")
endif(PostgreSQL_FOUND)

if(HAVE_UDEV)
    set(CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}, libudev0 | libudev1")
    set(CPACK_RPM_PACKAGE_REQUIRES "${CPACK_RPM_PACKAGE_REQUIRES}, libudev >= 2.0")
endif(HAVE_UDEV)

if(ALLOCATE_TYPE EQUAL 2)
    set(CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}, libtbb2")
    set(CPACK_RPM_PACKAGE_REQUIRES "${CPACK_RPM_PACKAGE_REQUIRES}, tbb >= 4.1")
elseif(ALLOCATE_TYPE EQUAL 1)
    set(CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}, libtcmalloc-minimal4")
    set(CPACK_RPM_PACKAGE_REQUIRES "${CPACK_RPM_PACKAGE_REQUIRES}, gperftools-libs >= 2.5")
endif()

if(Fortran_FOUND)
    set(CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}, libgfortran3 | libgfortran4 | libgfortran5")
    set(CPACK_RPM_PACKAGE_REQUIRES "${CPACK_RPM_PACKAGE_REQUIRES}, libgfortran >= 3.0.0")
endif(Fortran_FOUND)


# Do this the weird way, so it gets run after all sub-directories.  Those are included
# after script injection during install, so we can't just put it inside this file.
add_subdirectory(package/standalone)

include(CPack)